%DECK TRINFO10 
      SUBROUTINE TRINFO10(SREF,SPOW,ITRVAR,ITRAFLX,ITRINIT,QMIN,
     1                    NTRACP,IAINDT)
C
C     * Aug 10/2017 - M.Lazare.    Add support for lakes.
C     * Aug 07/2017 - M.Lazare.    Initial GIT version.
C     * Apr 03/2015 - M.Lazare.    New version for gcm18:
C     *                            - ITCO2 always part of namelist.
C     *                            - Add support for tiles with namelists.
C     * JUL 13/2013 - K.VONSALZEN. Previous version TRINFO9 for gcm17:
C     *                            - Add support for PAM (different
C     *                              number of tracers).
C     *                            - Uses PSIZES_17 instead of PSIZES_16.
C     * MAY 08/2012 - M.LAZARE.    PREVIOUS VERSION TRINFO8 FOR GCM16:
C     *                            - USES PSIZES_16 INSTEAD OF PSIZES_15I.
C     * APR 26/2010 - M.LAZARE.    PREVIOUS VERSION TRINFO7 FOR GCM15I:
C     *                            - ADD CO2 TO LIST OF TRACERS.
C     *                            - ADD SUPPORT FOR ITSRF=2 (SURFACE
C     *                              FLUX FROM COUPLER).
C     *                            - ADD NEW NAMELIST FOR MOLECULAR WEIGHTS.
C     *                            - USES NEW PSIZES_15I MODULE.
C     * FEB 23/2009 - M.LAZARE.    PREVIOUS VERSION TRINFO6 FOR GCM15H:
C     *                            - REMOVAL OF QCTOFF (NO LONGER USED).
C     *                            - USES NEW PSIZES_15H INSTEAD OF
C     *                              PREVIOUS PSIZES_15G.
C     *                            - BUGFIX TO SPECIFICATION OF XMIN,
C     *                              QMIN AND TMIN.
C     * DEC 19/2007 - M.LAZARE.    PREVIOUS VERSION TRINFO5 FOR GCM15G:
C     *                            - QCTOFF SAME LARGE VALUE REGARDLESS OF
C     *                              32/64 BIT TO AVOID INACCURACY AT 32
C     *                              BITS.  
C     *                            - CALLS NEW PSIZES_15G.
C     * SEP 12/2006 - M.LAZARE.    PREVIOUS VERSION TRINFO4 FOR GCM15F:
C     *                            MODIFIED TO SUPPORT 32-BIT MODE FOR
C     *                            MODEL DRIVER:
C     *                            - QCTOFF DEFINED AND PASSED OUT
C     *                              INSTEAD OF IN GCM15F, SO CAN USE
C     *                              SAME METHODOLOGY AS QMIN,TMIN.
C     *                            - INCLUDE DEFINITION OF COMMON BLOCK
C     *                              CONTAINING "MACHINE" AND USE THIS
C     *                              TO INCREASE VALUE OF POWER FOR
C     *                              QMIN,TMIN,QCTOFF IN CASE OF 32-BIT
C     *                              MODE.
C     *                            - USES NEW PSIZES_15I MODULE.
C     * JUN 13/2006 - M.LAZARE.    - "USES" PSIZES_15F INSTEAD OF 
C     *                              PSIZES_15D.
C     *                            - DEFINE QMIN HERE INSTEAD OF SPWCON. 
C     * DEC 15/2005 - M.LAZARE.    PREVIOUS VERSION TRINFO3 FOR GCM15D/E:
C     *                            "USES" PSIZES_15D INSTEAD OF PSIZES_15B.
C     * APR 06, 2004 - M.LAZARE.   PREVIOUS VERSION TRINFO2 FOR GCM15C.
C
C     * TRACER SPECIFICATION ROUTINE FOR GCM15I.
C
      USE PSIZES_19

      IMPLICIT REAL (A-H,O-Z), 
     +INTEGER (I-N)            

      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE
C
C     * MUST HAVE ARRAYS IN NAMELISTS PASSED IN/OUT THROUGH COMMON BLOCKS.
C
      INTEGER ITRNAM,ITRADV,ITRPHS,ITRSRF,ITRIC,ITRWET,ITRCCH,ITRDRY,
     1        ITRLVS,ITRLVF
      REAL CONTRAC,MW
      COMMON /IXCONT/ ITRNAM (NTRAC), ITRADV (NTRAC),     
     1                ITRPHS (NTRAC), ITRSRF (NTRAC),     
     2                ITRIC  (NTRAC), ITRWET (NTRAC),     
     3                ITRCCH (NTRAC), ITRDRY (NTRAC),
     4                ITRLVS (NTRAC), ITRLVF (NTRAC),
     5                CONTRAC(NTRAC), MW     (NTRAC) 
C
C     * COMMON BLOCK FOR TRACER CONSERVATION (ITRVAR.NE."   Q")     
C                                                                   
      COMMON /CONSTR/XSRCRAT(NTRAC),XSRCRM1(NTRAC),XPP(NTRAC),      
     1               XP0(NTRAC),XPM(NTRAC),TFICXM(NTRAC),
     2               XREF(NTRAC),XPOW(NTRAC),XMIN(NTRAC),           
     3               TMIN(NTRAC),XSRC0(NTRAC),XSRCM(NTRAC),         
     4               XSCL0(NTRAC),XSCLM(NTRAC)                      

      CHARACTER*4 CHMNAM(NTRAC)
      CHARACTER*4 LTILNAM(NTLD), KTILNAM(NTLKD), WTILNAM(NTWT)
C
C     * HYBRID INFORMATION FOR MOISTURE.
C
      NAMELIST /INGEN/SREF,SPOW,ILAUN
      COMMON /NON_ORO/ILAUN
#if (defined(pla) && defined(pam))
      CHARACTER*4 CHTMP
      INTEGER IAINDT(NTRAC)
#endif
C
C     * SPECIFICATION OF EXTRA NAMELIST "NLINFOLIST".
C     * THIS MAY BE USED TO PASS INFORMATION FROM THE SUBMISSION JOB
C     * INTO THE MODEL RELATED TO THE PHYSICAL PARAMETERIZATIONS THAT
C     * EMPLOY THE TRACER LOGIC.
C
      namelist /nlinfolist/nlinfo_dum
c
c     * namelist for tracer index numbers.
c     * each physical package (including generic tracers) must be included.
c 
      namelist /indlist/
#if (defined(pla) && defined(pam))
     1           ITLWC,ITIWC,ITDMS,ITSO2,ITHPO,ITH2O,
     2           ITSO4,ITBCO,ITBCY,ITOCO,ITOCY,ITSSA,ITSSC,ITDUA,ITDUC,
     3           ITA01,ITA02,ITA03,ITA04,ITA05,
     4           ITA06,ITA07,ITA08,ITA09,ITA10,
     5           ITA11,ITA12,ITA13,ITA14,ITA15,
     6           ITA16,ITA17,ITA18,ITA19,ITA20,
     7           ITA21,ITA22,ITA23,ITA24,ITA25,
     8           ITA26,ITA27,ITA28,ITA29,ITA30,
     9           ITA31,ITA32,ITA33,ITA34,ITA35,
     *           ITA36,ITA37,ITA38,ITA39,ITA40,
     1           ITA41,ITA42,ITA43,ITA44,ITA45,
     2           ITA46,ITA47,ITA48,ITA49,ITA50,
     3           ITGS6,ITGSP
#else
     1           ITLWC,ITIWC,ITBCO,ITBCY,ITOCO,ITOCY,ITSSA,ITSSC,
     2           ITDUA,ITDUC,ITDMS,ITSO2,ITSO4,ITHPO,ITH2O
#endif
     3          ,ITCO2
      namelist /namlist/CHMNAM
      namelist /advlist/ITRADV
      namelist /phslist/ITRPHS
      namelist /srflist/ITRSRF
      namelist /conlist/CONTRAC
      namelist /reflist/XREF
      namelist /powlist/XPOW
      namelist /wetlist/ITRWET
      namelist /cchlist/ITRCCH
      namelist /drylist/ITRDRY
      namelist /iclist/ITRIC 
      namelist /mwlist/MW
C
C     * COMMON BLOCK TO HOLD INDEX OF TRACERS.
C
#if (defined(pla) && defined(pam))
      COMMON /ITRIND/ ITLWC,ITIWC,ITDMS,ITSO2,ITHPO,ITH2O 
      COMMON /ITRIND/ ITSO4,ITBCO,ITBCY,ITOCO,ITOCY,ITSSA,ITSSC,ITDUA,
     1                ITDUC
      COMMON /ITRIND/ ITA01,ITA02,ITA03,ITA04,ITA05
      COMMON /ITRIND/ ITA06,ITA07,ITA08,ITA09,ITA10
      COMMON /ITRIND/ ITA11,ITA12,ITA13,ITA14,ITA15
      COMMON /ITRIND/ ITA16,ITA17,ITA18,ITA19,ITA20
      COMMON /ITRIND/ ITA21,ITA22,ITA23,ITA24,ITA25
      COMMON /ITRIND/ ITA26,ITA27,ITA28,ITA29,ITA30
      COMMON /ITRIND/ ITA31,ITA32,ITA33,ITA34,ITA35
      COMMON /ITRIND/ ITA36,ITA37,ITA38,ITA39,ITA40
      COMMON /ITRIND/ ITA41,ITA42,ITA43,ITA44,ITA45
      COMMON /ITRIND/ ITA46,ITA47,ITA48,ITA49,ITA50
      COMMON /ITRIND/ ITGS6,ITGSP
#else
      COMMON /ITRIND/ ITLWC,ITIWC,ITDMS,ITSO2,ITSO4,ITHPO 
      COMMON /ITRIND/ ITBCO,ITBCY,ITOCO,ITOCY,ITSSA,ITSSC,ITDUA,ITDUC
      COMMON /ITRIND/ ITH2O
#endif
      COMMON /ITRIND/ ITCO2
C
C     * Namelist information for tiling.
C
C     * First, namelist for tile numbers.
C     * Then,  namelist for tile names.
C     * Finally, common block to hold indices of tiles.
C 
      namelist /iltlist/
     1           ILTGEN
      namelist /namltilist/ LTILNAM
      COMMON /ILTILIND/ ILTGEN
C
      namelist /iktlist/
     1           IKTULK,IKTRWT,IKTRIC
      namelist /namktilist/ KTILNAM
      COMMON /IKTILIND/ IKTULK,IKTRWT,IKTRIC
C
      namelist /iwtlist/
     1           IWTWAT,IWTSIC
      namelist /namwtilist/WTILNAM
      COMMON /IWTILIND/ IWTWAT,IWTSIC
      COMMON /ISTIND  / IULAK, IRLWT, IRLIC, IOWAT, IOSIC
C
C     * TILE ARRAYS IN NAMELISTS PASSED IN/OUT THROUGH COMMON BLOCKS.
C
      INTEGER ILTNAM,IKTNAM,IWTNAM
      COMMON /ILTILE/ ILTNAM (NTLD)
      COMMON /IKTILE/ IKTNAM (NTLKD)
      COMMON /IWTILE/ IWTNAM (NTWT)
C
C     * COMMON BLOCK TO HOLD WORD SIZE.
C
      COMMON /MACHTYP/ MACHINE,INTSIZE
c--------------------------------------------------------------------
c     * check to make sure chmnam is large enough.
c
      if(ntrac.gt.1000) then
         write(6,*) "chmnam dimension smaller than ntrac"
         CALL XIT('TRINFO',-1)
      endif
C
C     * SET DEFAULTS.
C
      ilaun=0
      itdum=0

      do i=1,ntrac
        CHMNAM(i)="    "
        ITRPHS(i)=1
        ITRADV(i)=0
        ITRSRF(i)=0
        CONTRAC(i)=0.
        ITRLVS(I)=1
        ITRLVF(I)=ILEV
        XREF(I)=0.
        XPOW(I)=1.0
        ITRWET(i)=0
        ITRCCH(i)=0
        ITRDRY(I)=0
        ITRIC(I)=0
        MW(I)=0
      enddo
c
c     * read namelists.
c
      read(5,ingen,end=1001)
      read(5,nlinfolist,end=1002)
      read(5,indlist,end=1003)
      read(5,namlist,end=1004)
      read(5,advlist,end=1005)
      read(5,phslist,end=1006)
      read(5,srflist,end=1007)
      read(5,conlist,end=1008)
      read(5,reflist,end=1009)
      read(5,powlist,end=1010)
      read(5,wetlist,end=1011)
      read(5,cchlist,end=1012)
      read(5,drylist,end=1013)
      read(5,iclist,end=1014)
      read(5,mwlist,end=1015)
C
C     * SET QMIN VALUE.
C
      QMIN=SREF*(10.**(-16./REAL(MACHINE)))
C
C     * CHECK TO MAKE SURE ITRSRF IS SET CONSIST WITH CONTRAC
C     * AND SET ITS VALUE IF CONTRAC IS NONZERO
C
      DO N=1,NTRAC
         IF(CONTRAC(N).GT.0..AND.ITRSRF(N).GE.1) CALL XIT('TRINFO',-2)
      ENDDO

c     * write namelists out to log file (master node only)

      if(mynode.eq.0)                           then
      write(6,ingen)
      write(6,nlinfolist)
      write(6,indlist)
      write(6,namlist)
      write(6,advlist)
      write(6,phslist)
      write(6,srflist)
      write(6,conlist)
      write(6,reflist)
      write(6,powlist)
      write(6,wetlist)
      write(6,cchlist)
      write(6,drylist)
      write(6,iclist)
      write(6,mwlist)
      endif
C
C     * ENSURE THAT ALL TRACER NAMES HAVE BEEN SET IN NAMELIST
C     * AND ASSIGN THESE TO THE INTEGER ARRAY ITRNAM.
C
      DO I=1,NTRAC
         if(CHMNAM(I).EQ."    ") CALL XIT('TRINFO',-9)
         ITRNAM(i)=NC4TO8(CHMNAM(I))
      ENDDO
C
C     * REDEFINE XREF,XPOW IN CASE SPECIES NOT ADVECTED OR IS PURE S/L,
C     * SO THAT HYBRID VARIABLE WILL ALWAYS BE MIXING RATIO. 
C     * CALCULATE LOWER BOUND FOR HOLE-FILLING FOR HYBRID TRACER
C     * VARIABLE.
C
      DO N=1,NTRAC                                          
        IF(ITRADV(N).EQ.0 .OR. 
     1   (ITRVAR.NE.NC4TO8("QHYB") .AND. ITRVAR.NE.NC4TO8("SLQB"))) THEN
          XREF(N)=0.
          XPOW(N)=1.
        ENDIF
C
        TMIN (N) = XREF(N)*(10.**(-16./REAL(MACHINE)))
C
        IF(XREF(N).EQ.0.) THEN
          XMIN(N) = TMIN(N)
        ELSE
          PINV=1./XPOW(N)
          XMIN(N) = XREF(N)/((1.+XPOW(N)*LOG(XREF(N)/TMIN(N)))**PINV) 
        ENDIF
      ENDDO
C
C     * SUM UP TO DETERMINE WHETHER THERE ARE ANY TRACERS WITH
C     * PRESCRIBED SURFACE FLUXES OR INITIAL CONDITIONS. IF NOT,
C     * CERTAIN PARTS OF THE CODE CAN BE SKIPPED TO IMPROVE PERFORMANCE.
C
      ITRINIT=0
      ITRAFLX=0
      DO N=1,NTRAC                                          
        IF(ITRIC (N).NE.0) ITRINIT=ITRINIT+1
        IF(ITRSRF(N).EQ.1) ITRAFLX=ITRAFLX+1
      ENDDO
#if (defined(pla) && defined(pam))
C
C     * EXTRACT AEROSOL TRACERS FOR PLA CALCULATIONS. ALL TRACERS
C     * WITH NAMES THAT START WITH AN "A" AND ARE FOLLOWED BY A NUMBER
C     * ARE CONSIDERED AEROSOL TRACERS (I.E. FORMAT ' A##').
C
      IAINDT=-1
      NTRACP=0
      DO I=1,NTRAC
         CHTMP=CHMNAM(I)
         IF ( CHTMP(2:2) == 'A'
     1       .AND. (CHTMP(3:3) == '0' .OR. CHTMP(3:3) == '1' 
     2         .OR. CHTMP(3:3) == '2' .OR. CHTMP(3:3) == '3'
     3         .OR. CHTMP(3:3) == '4' .OR. CHTMP(3:3) == '5'
     4         .OR. CHTMP(3:3) == '6' .OR. CHTMP(3:3) == '7'
     5         .OR. CHTMP(3:3) == '8' .OR. CHTMP(3:3) == '9')
     6       .AND. (CHTMP(4:4) == '0' .OR. CHTMP(4:4) == '1' 
     7         .OR. CHTMP(4:4) == '2' .OR. CHTMP(4:4) == '3'
     8         .OR. CHTMP(4:4) == '4' .OR. CHTMP(4:4) == '5'
     9         .OR. CHTMP(4:4) == '6' .OR. CHTMP(4:4) == '7'
     1         .OR. CHTMP(4:4) == '8' .OR. CHTMP(4:4) == '9')
     2      ) THEN
           NTRACP=NTRACP+1
           IAINDT(NTRACP)=I
         ENDIF
      ENDDO
#endif
C
C     * TILES.
C
C     * FIRST, SET DEFAULTS.
C
      DO I=1,NTLD
        LTILNAM(I)="    "
      ENDDO

      IF(NTLK.GT.0)          THEN
        DO I=1,NTLK
          KTILNAM(I)="    "
        ENDDO
      ENDIF

      DO I=1,NTWT
        WTILNAM(I)="    "
      ENDDO
C
C     * Read namelists.
C
      read(5,iltlist,end=1016)
      read(5,namltilist,end=1017)
      read(5,iktlist,end=1018)
      if(ntlk.gt.0)  read(5,namktilist,end=1019)
      read(5,iwtlist,end=1020)
      read(5,namwtilist,end=1021)
C
C     * Write namelists out to log file (master node only).
C
      if(mynode.eq.0)                           then
      write(6,iltlist)
      write(6,namltilist)   
      write(6,iktlist)
      if(ntlk.gt.0)  write(6,namktilist)   
      write(6,iwtlist)
      write(6,namwtilist)   
      endif
C
C     * ENSURE THAT ALL TRACER NAMES HAVE BEEN SET IN NAMELIST
C     * AND ASSIGN THESE TO THE INTEGER ARRAYS {ILTNAM,IKTNAM,ITWNAM}.
C
      DO I=1,NTLD
         IF(LTILNAM(I).EQ."    ") CALL XIT('TRINFO',-10)
         ILTNAM(I)=NC4TO8(LTILNAM(I))
      ENDDO
      IF(NTLK.GT.0)                                        THEN
      DO I=1,NTLK
         IF(KTILNAM(I).EQ."    ") CALL XIT('TRINFO',-11)
         IKTNAM(I)=NC4TO8(KTILNAM(I))
      ENDDO
      ENDIF
      DO I=1,NTWT
         IF(WTILNAM(I).EQ."    ") CALL XIT('TRINFO',-12)
         IWTNAM(I)=NC4TO8(WTILNAM(I))
      ENDDO
C
C     * CALCULATE TILE INDEX LOCATIONS FOR BOTH UNRESOLVED AND
C     * RESOLVED LAKE WATER/ICE TILES REFERENCED BY THE PAT/ROT
C     * ARRAYS LIKE FAREPAT/FAREROT.
C
C     * THESE ARE STORED IN THE "ISTIND" COMMON BLOCK
C     * WHICH IS USED IN THE PHYSICS.
C
      IF(IKTULK.GT.0) THEN
        IULAK=NTLD+IKTULK
      ELSE
        IULAK=0
      ENDIF
C
      IF(IKTRWT.GT.0) THEN
        IRLWT=NTLD+IKTRWT
      ELSE
        IRLWT=0
      ENDIF
C
      IF(IKTRIC.GT.0) THEN
        IRLIC=NTLD+IKTRIC
      ELSE
        IRLIC=0
      ENDIF
C
C     * CALCULATE TILE INDEX LOCATIONS FOR OCEAN
C     * WATER/ICE TILES REFERENCED BY THE PAT/ROT
C     * ARRAYS LIKE FAREPAT/FAREROT.
C
C     * THESE ARE STORED IN THE "ISTIND" COMMON BLOCK
C     * WHICH IS USED IN THE PHYSICS.
C
      IOWAT=NTLD+NTLK+IWTWAT
      IOSIC=NTLD+NTLK+IWTSIC
C
      RETURN
C=======================================================================
 1001 CALL XIT('TRINFO INGEN',-1)
 1002 CALL XIT('TRINFO NLINFOLIST',-2)
 1003 CALL XIT('TRINFO INDLIST',-3)
 1004 CALL XIT('TRINFO NAMLIST',-4)
 1005 CALL XIT('TRINFO ADVLIST',-5)
 1006 CALL XIT('TRINFO PHSLIST',-6)
 1007 CALL XIT('TRINFO SRFLIST',-7)
 1008 CALL XIT('TRINFO CONLIST',-8)
 1009 CALL XIT('TRINFO REFLIST',-9)
 1010 CALL XIT('TRINFO POWLIST',-10)
 1011 CALL XIT('TRINFO WETLIST',-11)
 1012 CALL XIT('TRINFO CCHLIST',-12)
 1013 CALL XIT('TRINFO DRYLIST',-13)
 1014 CALL XIT('TRINFO ICLIST',-14)
 1015 CALL XIT('TRINFO MWLIST',-15)
 1016 CALL XIT('TRINFO ILTILIST',-16)
 1017 CALL XIT('TRINFO NAMLTILIST',-17)
 1018 CALL XIT('TRINFO IKTILIST',-18)
 1019 CALL XIT('TRINFO NAMKTILIST',-19)
 1020 CALL XIT('TRINFO IWTILIST',-20)
 1021 CALL XIT('TRINFO NAMWTILIST',-21)
C
      END
