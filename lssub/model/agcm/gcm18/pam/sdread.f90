      SUBROUTINE SDREAD
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     INPUT AND SAVING OF AEROSOL TYPE INFORMATION.
!
!     HISTORY:
!     --------
!     * MAR 29/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPIO
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, DIMENSION(KMAX) :: KIOEXT,KIOINT
      NAMELIST /SDBPAR/ AEXT,AINT
!
!-----------------------------------------------------------------------
!     * INITIALIZE WORK AND IO ARRAYS.
!
      KIOEXT=INA
      KIOINT=INA
      DO KX=1,KMAX
        AEXT%TP(KX)%ISEC=INA
        AEXT%TP(KX)%DPST=YNA
        AEXT%TP(KX)%RADB=YNA
        AEXT%TP(KX)%DENS=YNA
        AEXT%TP(KX)%MOLW=YNA
        AEXT%TP(KX)%NUIO=YNA
        AEXT%TP(KX)%KAPPA=YNA
        AEXT%TP(KX)%STICK=YNA
        AEXT%TP(KX)%PSIM(:)=YNA
        AEXT%TP(KX)%NAME(1:1)=CNA
      ENDDO
      AINT%ISEC=INA
      AINT%DPST=YNA
      AINT%RADB=YNA
      AINT%STICK=YNA
      AINT%PSIM(:)=YNA
      DO KX=1,KMAX
        AINT%TP(KX)%ISCB=1
        AINT%TP(KX)%ISCE=INA
        AINT%TP(KX)%DENS=YNA
        AINT%TP(KX)%MOLW=YNA
        AINT%TP(KX)%NUIO=YNA
        AINT%TP(KX)%KAPPA=YNA
        AINT%TP(KX)%NAME(1:1)=CNA
      ENDDO
!
!-----------------------------------------------------------------------
!     * READ NAMELISTS.
!
      READ(5,NML=SDBPAR,END=1001)
!
!-----------------------------------------------------------------------
!     * CHECK INPUT FOR EXTERNALLY MIXED AEROSOL.
!
      DO KX=1,KMAX
        ICNTE=0
        ICNTP=0
        IF ( AEXT%TP(KX)%ISEC /= INA )              ICNTE=ICNTE+1
        IF ( ABS(AEXT%TP(KX)%RADB-YNA) > YTINY )    ICNTE=ICNTE+1
        IF ( ABS(AEXT%TP(KX)%PSIM(1)-YNA) > YTINY ) ICNTE=ICNTE+1
        IF ( AEXT%TP(KX)%NAME(1:1) /= CNA )         ICNTE=ICNTE+1
        IF ( ABS(AEXT%TP(KX)%DENS-YNA) > YTINY )    ICNTE=ICNTE+1
        IF ( ABS(AEXT%TP(KX)%STICK-YNA) > YTINY )   ICNTE=ICNTE+1
        IF ( ABS(AEXT%TP(KX)%MOLW-YNA) > YTINY ) THEN
          ICNTP=ICNTP+1
        ENDIF
        IF ( ABS(AEXT%TP(KX)%NUIO-YNA) > YTINY ) THEN
          ICNTP=ICNTP+1
        ENDIF
        IF ( ABS(AEXT%TP(KX)%KAPPA-YNA) <= YTINY ) THEN
          IF ( .NOT.((ICNTP == 2 .AND. ICNTE == 6) .OR. ICNTP == 0) &
                                                                 ) THEN
            CALL XIT('SDREAD',-2)
          ENDIF
        ELSE
          IF ( .NOT.(ICNTE == 6)                                 ) THEN
            CALL XIT('SDREAD',-2)
          ENDIF
        ENDIF
      ENDDO
      ICNT=0
      DO KX=1,KMAX
      DO KY=KX+1,KMAX
        IF ( AEXT%TP(KX)%NAME == AEXT%TP(KY)%NAME &
                            .AND. AEXT%TP(KX)%NAME(1:1) /= CNA ) ICNT=1
      ENDDO
      ENDDO
      IF ( ICNT /= 0 ) THEN
        CALL XIT('SDREAD',-3)
      ENDIF
!
!     * CHECK INPUT FOR INTERNALLY MIXED AEROSOL.
!
      ICNTE=0
      IF ( AINT%ISEC /= INA )              ICNTE=ICNTE+1
      IF ( ABS(AINT%RADB-YNA) > YTINY )    ICNTE=ICNTE+1
      IF ( ABS(AINT%STICK-YNA) > YTINY )   ICNTE=ICNTE+1
      IF ( ABS(AINT%PSIM(1)-YNA) > YTINY ) ICNTE=ICNTE+1
      DO KX=1,KMAX
        ICNTP=0
        ICNTK=0
        IF ( AINT%TP(KX)%NAME(1:1) /= CNA ) THEN
          ICNTP=ICNTP+1
          ICNTK=ICNTK+1
        ENDIF
        IF ( ABS(AINT%TP(KX)%DENS-YNA) > YTINY ) THEN
          ICNTP=ICNTP+1
          ICNTK=ICNTK+1
        ENDIF
        IF ( ABS(AINT%TP(KX)%MOLW-YNA) > YTINY ) THEN
          ICNTP=ICNTP+1
        ENDIF
        IF ( ABS(AINT%TP(KX)%NUIO-YNA) > YTINY ) THEN
          ICNTP=ICNTP+1
        ENDIF
        IF ( ABS(AINT%TP(KX)%KAPPA-YNA) <= YTINY ) THEN
          IF ( .NOT.((ICNTP == 4 .AND. ICNTE == 4) .OR. ICNTP == 0) &
                                                                 ) THEN
            CALL XIT('SDREAD',-4)
          ENDIF
        ELSE
          IF ( .NOT.((ICNTK == 2 .AND. ICNTE == 4) .OR. ICNTK == 0) &
                                                                 ) THEN
            CALL XIT('SDREAD',-4)
          ENDIF
        ENDIF
      ENDDO
      ICNT=0
      DO KX=1,KMAX
      DO KY=KX+1,KMAX
        IF ( AINT%TP(KX)%NAME == AINT%TP(KY)%NAME &
                            .AND. AINT%TP(KX)%NAME(1:1) /= CNA ) ICNT=1
      ENDDO
      ENDDO
      IF ( ICNT /= 0 ) THEN
        CALL XIT('SDREAD',-5)
      ENDIF
!
!-----------------------------------------------------------------------
!     * ASSUME SIZE DISTRIBUTION COVERS THE ENTIRE RANGE FOR
!     * INTERNALLY MIXED AEROSOL SPECIES IF NOT OTHERWISE SPECIFIED.
!
      DO KX=1,KMAX
        IF ( AINT%TP(KX)%ISCE == INA .AND. AINT%ISEC /= INA ) THEN
          AINT%TP(KX)%ISCE=AINT%ISEC
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!     * DETERMINE LENGTHS OF INDIVIDUAL INPUT ARRAYS FOR EXTERNALLY
!     * MIXED AEROSOL.
!
      DO KX=1,KMAX
        ISECT=INA
        DO IS=1,ISMAX
          IF ( ABS(AEXT%TP(KX)%PSIM(IS)-YNA) > YTINY ) THEN
            ISECT=IS
          ENDIF
        ENDDO
        IF ( ISECT /= AEXT%TP(KX)%ISEC ) THEN
          CALL XIT('SDREAD',-6)
        ENDIF
      ENDDO
!
!     * DETERMINE LENGTHS OF INDIVIDUAL INPUT ARRAYS FOR INTERNALLY
!     * MIXED AEROSOL.
!
      ISECT=INA
      DO IS=1,ISMAX
        IF ( ABS(AINT%PSIM(IS)-YNA) > YTINY ) THEN
          ISECT=IS
        ENDIF
      ENDDO
      IF ( ISECT /= AINT%ISEC ) THEN
        CALL XIT('SDREAD',-7)
      ENDIF
!
!-----------------------------------------------------------------------
!     * DETERMINE TOTAL NUMBER OF EXTERNALLY MIXED AEROSOL TYPES.
!
      KEXT=0
      DO KX=1,KMAX
        IF ( AEXT%TP(KX)%NAME(1:1) /= CNA ) THEN
          KEXT=KEXT+1
          KIOEXT(KEXT)=KX
        ENDIF
      ENDDO
!
!     * DETERMINE TOTAL NUMBER OF INTERNALLY MIXED AEROSOL TYPES.
!
      KINT=0
      DO KX=1,KMAX
        IF ( AINT%TP(KX)%NAME(1:1) /= CNA ) THEN
          KINT=KINT+1
          KIOINT(KINT)=KX
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!     * CHECK IF THERE IS AT LEAST ONE AEROSOL TYPE IN EACH SECTION
!     * OF THE INTERNALLY MIXED AEROSOL.
!
      IF ( KINT > 0 ) THEN
        IST=AINT%ISEC
        DO IS=1,IST
          IOK=0
          DO KX=1,KINT
            ISCBT=AINT%TP(KIOINT(KX))%ISCB
            ISCET=AINT%TP(KIOINT(KX))%ISCE
            IF ( IS >= ISCBT .AND. IS <= ISCET ) THEN
              IOK=IOK+1
            ENDIF
          ENDDO
          IF ( IOK == 0 ) THEN
            CALL XIT('SDREAD',-8)
          ENDIF
        ENDDO
      ENDIF
!
!     * CHECK IF DETAILED CHEMICAL INFORMATION IS USED OR
!     * KAPPA APPROACH FOR EXTERNALLY MIXED AEROSOL.
!
      IF ( KEXT > 0 ) THEN
        ICNT=0
        DO KX=1,KEXT
          IF ( ABS(AEXT%TP(KX)%KAPPA-YNA) > YTINY )   ICNT=ICNT+1
        ENDDO
        IF ( ICNT==KEXT ) IEXKAP=1
        ICNT=0
        DO KX=1,KEXT
          IF ( ABS(AEXT%TP(KX)%NUIO-YNA) > YTINY )    ICNT=ICNT+1
        ENDDO
        IF ( (ICNT==KEXT .AND. IEXKAP==1) &
                           .OR. (ICNT /= KEXT .AND. IEXKAP /= 1) ) THEN
          CALL XIT('SDREAD',-9)
        ENDIF
      ENDIF
!
!     * CHECK IF DETAILED CHEMICAL INFORMATION IS USED OR
!     * KAPPA APPROACH FOR INTERNALLY MIXED AEROSOL.
!
      IF ( KINT > 0 ) THEN
        ICNT=0
        DO KX=1,KINT
          IF ( ABS(AINT%TP(KX)%KAPPA-YNA) > YTINY )   ICNT=ICNT+1
        ENDDO
        IF ( ICNT==KINT ) IINKAP=1
        ICNT=0
        DO KX=1,KINT
          IF ( ABS(AINT%TP(KX)%NUIO-YNA) > YTINY )    ICNT=ICNT+1
        ENDDO
        IF ( (ICNT==KINT .AND. IINKAP==1) &
                           .OR. (ICNT /= KINT .AND. IINKAP /= 1) ) THEN
          CALL XIT('SDREAD',-9)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * ALLOCATE INTERNALLY USED PARAMETER ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        ALLOCATE(AEXTF%TP(KEXT))
        DO KX=1,KEXT
          ALLOCATE(AEXTF%TP(KX)%PSIM(AEXT%TP(KIOEXT(KX))%ISEC))
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        ALLOCATE(AINTF%TP(KINT))
        ALLOCATE(AINTF%PSIM(AINT%ISEC))
      ENDIF
!
!-----------------------------------------------------------------------
!     * COPY RESULTS FOR EXTERNALLY MIXED AEROSOLS INTO CORRESPONDING
!     * INTENRALLY USED ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        DO KX=1,KEXT
          AEXTF%TP(KX)%NAME=AEXT%TP(KIOEXT(KX))%NAME
          IST=AEXT%TP(KIOEXT(KX))%ISEC
          AEXTF%TP(KX)%ISEC=IST
          DO IS=1,IST
            AEXTF%TP(KX)%PSIM(IS)=AEXT%TP(KIOEXT(KX))%PSIM(IS)
          ENDDO
          AEXTF%TP(KX)%DPST =AEXT%TP(KIOEXT(KX))%DPST
          AEXTF%TP(KX)%RADB =AEXT%TP(KIOEXT(KX))%RADB
          AEXTF%TP(KX)%DENS =AEXT%TP(KIOEXT(KX))%DENS
          AEXTF%TP(KX)%MOLW =AEXT%TP(KIOEXT(KX))%MOLW
          AEXTF%TP(KX)%NUIO =AEXT%TP(KIOEXT(KX))%NUIO
          AEXTF%TP(KX)%KAPPA=AEXT%TP(KIOEXT(KX))%KAPPA
          AEXTF%TP(KX)%STICK=AEXT%TP(KIOEXT(KX))%STICK
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * COPY RESULTS FOR INTERNALLY MIXED AEROSOLS INTO CORRESPONDING
!     * INTENRALLY USED ARRAYS.
!
      IF ( KINT > 0 ) THEN
        IST=AINT%ISEC
        AINTF%ISEC=IST
        DO IS=1,IST
          AINTF%PSIM(IS)=AINT%PSIM(IS)
        ENDDO
        AINTF%DPST=AINT%DPST
        AINTF%RADB=AINT%RADB
        AINTF%STICK=AINT%STICK
        DO KX=1,KINT
          AINTF%TP(KX)%NAME=AINT%TP(KIOINT(KX))%NAME
          ISCBT=AINT%TP(KIOINT(KX))%ISCB
          ISCET=AINT%TP(KIOINT(KX))%ISCE
          IF ( ISCBT >= 1 .AND. ISCET <= AINT%ISEC ) THEN
            AINTF%TP(KX)%ISCB=ISCBT
            AINTF%TP(KX)%ISCE=ISCET
          ELSE
            CALL XIT('SDREAD',-8)
          ENDIF
          AINTF%TP(KX)%DENS =AINT%TP(KIOINT(KX))%DENS
          AINTF%TP(KX)%MOLW =AINT%TP(KIOINT(KX))%MOLW
          AINTF%TP(KX)%NUIO =AINT%TP(KIOINT(KX))%NUIO
          AINTF%TP(KX)%KAPPA=AINT%TP(KIOINT(KX))%KAPPA
        ENDDO
      ENDIF
!
      RETURN
 1001 CALL XIT('SDREAD',-1)
!
      END SUBROUTINE SDREAD
