      SUBROUTINE DPLAOUT (DEN0,DEPHI0,DEPSI,DEDDN, &
                          ISDIAG,ILGA,DESIZE,DEFLUX,DEFNUM)
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL(R8), PARAMETER :: Y2CM3=1.E-06_R8
      REAL(R8), PARAMETER :: Y2UG=1.E+15_R8
      REAL, INTENT(IN), DIMENSION(ILGA,1,ISEXTMD) :: DEN0, &
                                        DEPHI0,DEPSI,DEDDN
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDIAG) :: DESIZE, &
                                             DEFLUX,DEFNUM
!
      REAL, DIMENSION(ISDIAG) :: ANSD,AMSD,APHIP
      REAL(R8) :: ATMP
!
      ISEC=ISEXTMD
      RADB=AEXTF%TP(KEXTMD)%RADB
      RADE=AEXTF%TP(KEXTMD)%RADE
      PH0=LOG(RADB/R0)
      PH1=LOG(RADE/R0)
      DPH=PH1-PH0
      DO IL=1,ILGA
      DO IS=1,ISEXTMD
        DO IP=1,ISDIAG
          PH=PH0+REAL(IP-1)*DPH/REAL(ISDIAG-1)
          RAD=EXP(PH)
          APHIP(IP)=PH
          ISP=INA
          IF ( APHIP(IP) <= AEXTF%TP(KEXTMD)%PHI(1)%VL ) THEN
            ISP=1
          ELSE IF ( APHIP(IP) > AEXTF%TP(KEXTMD)%PHI(ISEC)%VR ) THEN
            ISP=ISEC
          ELSE
            DO ISI=1,ISEC
              IF (      APHIP(IP) >  AEXTF%TP(KEXTMD)%PHI(ISI)%VL &
                  .AND. APHIP(IP) <= AEXTF%TP(KEXTMD)%PHI(ISI)%VR) THEN
                ISP=ISI
              END IF
            ENDDO
          ENDIF
          IF ( ISP /= INA ) THEN
            IF ( DEN0(IL,1,ISP) > YTINY &
                          .AND. ABS(DEPSI(IL,1,ISP)-YNA) > YTINY ) THEN
              ATMP=-DEPSI(IL,1,ISP)*(APHIP(IP)-DEPHI0(IL,1,ISP))**2
              ATMP=                                                    &!PROC-DEPEND
                   EXP(ATMP)
              ANSD(IP)=DEN0(IL,1,ISP)*Y2CM3*ATMP
              AMSD(IP)=Y2UG*YCNST*((R0*RAD)**3)*DEDDN(IL,1,ISP)*ANSD(IP)
            ELSE
              ANSD(IP)=0.
              AMSD(IP)=0.
            ENDIF
          ELSE
            ANSD(IP)=0.
            AMSD(IP)=0.
          ENDIF
        ENDDO
!
        DO IP=1,ISDIAG
!        radius [um]
          DESIZE(IL,IP)=EXP(APHIP(IP))
!        dM/dlogR [ug/m3]
          DEFLUX(IL,IP)=AMSD(IP)
!        dN/dlogR [1/cm3]
          DEFNUM(IL,IP)=ANSD(IP)
        ENDDO
!
      ENDDO
      ENDDO
!
      END SUBROUTINE DPLAOUT
