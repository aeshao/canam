      SUBROUTINE SDMAP(FIL1,FIL2,FIDDN,FIFRC,FIVOLW,FIVOLA,FIPHIC,VOLGF, &
                       PIDDN,PIFRC,PIWETRC,PIMAS,PIPSI,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     MAPPING OF PLA SIZE DISTRIBUTION ONTO PICEWISE LINEAR SIZE
!     DISTRIBUTION WITH DOUBLING OF VOLUME BETWEEN SECTION BOUNDARIES
!     FOR COAGULATION CALCULATIONS.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIDDN,PIWETRC,PIMAS, &
                                                   PIPSI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: &
                                                   PIFRC
      REAL(R8), INTENT(INOUT), DIMENSION(ILGA,LEVA,ISFINT) :: FIL1,FIL2
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) ::   VOLGF
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFINT) :: &
                                                   FIDDN,FIPHIC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFINTB) :: &
                                                   FIVOLW
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFINT,KINT) :: &
                                                   FIFRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFTRI) :: &
                                                   FIVOLA
      REAL, DIMENSION(ILGA,LEVA,ISFTRI) ::         TERM,FIPHIA0
      REAL, DIMENSION(ILGA,LEVA) :: TOTM,TOTX,GRWTHF,DPHIT
      LOGICAL, DIMENSION(ILGA,LEVA) :: KOK
!
!-----------------------------------------------------------------------
!     * MASS-WEIGHTED PARTICLE SIZE GROWTH FACTOR. IN THE FOLLOWING,
!     * IT IS APPLIED TO ALL SIZE SECTIONS SO THAT THERE ARE NO
!     * OVERLAPPING SIZE SECTIONS FOR THE WET SIZE DISTRIBUTION. NOTE
!     * THAT THE CODE CURRENTLY DOES NOT PERMIT DIFFERENT GROWTH
!     * FACTORS FOR INDIVIDUAL SECTIONS.
!
      TOTM=0.
      TOTX=0.
      KOK=.TRUE.
      DO IS=1,ISAINT
        WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
          TOTX(:,:)=TOTX(:,:)+MAX(PIWETRC(:,:,IS)/PIDRYRC(IS),1.) &
                                 *PIMAS(:,:,IS)
          TOTM(:,:)=TOTM(:,:)+PIMAS(:,:,IS)
        ELSEWHERE
          KOK(:,:)=.FALSE.
        ENDWHERE
      ENDDO
      GRWTHF=1.
      WHERE ( TOTM > MAX(TOTX/YLARGE,YTINY) .AND. TOTX > YTINY &
                                                            .AND. KOK )
        GRWTHF=MAX(TOTX/TOTM,1.)
      ENDWHERE
!
      DPHIT=LOG(GRWTHF)
      VOLGF=GRWTHF**3.
!
!     * PARTICLE SIZE AT LOWER BOUNDARY OF EACH SECTION AND SECTION
!     * WIDTH.
!
      DO IS=1,ISFINTB
        FIVOLW(:,:,IS)=FIDRYVB(IS)*VOLGF(:,:)
      ENDDO
      DO IS=1,ISFINT
        FIL1  (:,:,IS)=FIL1(:,:,IS)/VOLGF(:,:)
        FIL2  (:,:,IS)=FIL2(:,:,IS)/VOLGF(:,:)**2
        FIPHIC(:,:,IS)=FIPHI(IS)%VL+DPHIT(:,:)+.5*COAGP%DPSTAR
      ENDDO
!
!     * COPY PLA PARAMETERS INTO CORRESPONDING ARRAYS FOR COAGULATION
!     * CALCULATIONS.
!
      ISC=0
      DO IS=1,ISAINT
      DO IST=1,COAGP%ISEC
        ISC=ISC+1
        FIDDN (:,:,ISC)  =PIDDN (:,:,IS)
        FIFRC (:,:,ISC,:)=PIFRC (:,:,IS,:)
      ENDDO
      ENDDO
      ITRI=0
      DO IS=1,ISFINT
      DO IK=1,IS
        ITRI=ITRI+1
        FIPHIA0(:,:,ITRI)=FIPHIB0(IS,IK)+DPHIT(:,:)
      ENDDO
      ENDDO
      TERM=3.*FIPHIA0
      FIVOLA=EXP(TERM)
      R03=R0**3
      FIVOLA=R03*FIVOLA
!
      END SUBROUTINE SDMAP
