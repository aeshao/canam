      SUBROUTINE AMDIAG(ACAS,ACOA,ACBC,ACSS,ACMD,PEMAS,PIMAS,PIFRC, &
                        RHOA,ILGA,LEVA)
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: ACAS,ACOA,ACBC,ACSS, &
                                                 ACMD
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: RHOA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: PEMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: PIMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
!
!     * AMMONIUM SULPHATE.
!
      ACAS=0.
      IF ( KEXTSO4 > 0 ) THEN
        DO IS=1,ISEXTSO4
          ACAS=ACAS+PEMAS(:,:,IEXSO4(IS))
        ENDDO
      ENDIF
      IF ( KINTSO4 > 0 ) THEN
        DO IS=1,ISINTSO4
          ACAS=ACAS+PIMAS(:,:,IINSO4(IS))*PIFRC(:,:,IINSO4(IS),KINTSO4)
        ENDDO
      ENDIF
      ACAS=ACAS*RHOA
!
!     * ORGANIC AEROSOL.
!
      ACOA=0.
      IF ( KEXTOC > 0 ) THEN
        DO IS=1,ISEXTOC
          ACOA=ACOA+PEMAS(:,:,IEXOC(IS))
        ENDDO
      ENDIF
      IF ( KINTOC > 0 ) THEN
        DO IS=1,ISINTOC
          ACOA=ACOA+PIMAS(:,:,IINOC(IS))*PIFRC(:,:,IINOC(IS),KINTOC)
        ENDDO
      ENDIF
      ACOA=ACOA*RHOA
!
!     * BLACK CARBON.
!
      ACBC=0.
      IF ( KEXTBC > 0 ) THEN
        DO IS=1,ISEXTBC
          ACBC=ACBC+PEMAS(:,:,IEXBC(IS))
        ENDDO
      ENDIF
      IF ( KINTBC > 0 ) THEN
        DO IS=1,ISINTBC
          ACBC=ACBC+PIMAS(:,:,IINBC(IS))*PIFRC(:,:,IINBC(IS),KINTBC)
        ENDDO
      ENDIF
      ACBC=ACBC*RHOA
!
!     * SEA SALT.
!
      ACSS=0.
      IF ( KEXTSS > 0 ) THEN
        DO IS=1,ISEXTSS
          ACSS=ACSS+PEMAS(:,:,IEXSS(IS))
        ENDDO
      ENDIF
      IF ( KINTSS > 0 ) THEN
        DO IS=1,ISINTSS
          ACSS=ACSS+PIMAS(:,:,IINSS(IS))*PIFRC(:,:,IINSS(IS),KINTSS)
        ENDDO
      ENDIF
      ACSS=ACSS*RHOA
!
!     * MINERAL DUST.
!
      ACMD=0.
      IF ( KEXTMD > 0 ) THEN
        DO IS=1,ISEXTMD
          ACMD=ACMD+PEMAS(:,:,IEXMD(IS))
        ENDDO
      ENDIF
      IF ( KINTMD > 0 ) THEN
        DO IS=1,ISINTMD
          ACMD=ACMD+PIMAS(:,:,IINMD(IS))*PIFRC(:,:,IINMD(IS),KINTMD)
        ENDDO
      ENDIF
      ACMD=ACMD*RHOA
!
      END SUBROUTINE AMDIAG
