      SUBROUTINE WRN(NAME,N)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PRINTS OUT WARNING MESSAGE.
!
!     HISTORY:
!     --------
!     * APR 10/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      CHARACTER*(*) NAME
      CHARACTER*4   DASH, STAR
      CHARACTER*8   NAME8
!
      DATA DASH /'----'/, STAR /'****'/
!---------------------------------------------------------------------
!
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,6010) (DASH,I=1,2),NAME8,(DASH,I=1,17),N
!
      IF(N.LT.0) WRITE(6,6010) (STAR,I=1,2),NAME8,(STAR,I=1,17),N
!
!---------------------------------------------------------------------
 6010 FORMAT('0',2A4,'  WARNING  ',A8,17A4,I8)
!
      END SUBROUTINE WRN
