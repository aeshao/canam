      SUBROUTINE SCLDSP(PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT,DSIVDT, &
                        DHPDT,DERSN,DERSM,DIRSMN,DIRSN,DIRSMS,DSICP, &
                        DWR1N,DWR2N,DWR3N,DWR1M,DWR2M,DWR3M,DWS1N, &
                        DWS2N,DWS3N,DWS1M,DWS2M,DWS3M,PEHENR,PIHENR, &
                        PEMAS,PENUM,PEPHIS0,PEDPHI0,PEPSI,PEDDN,PENFRC, &
                        PEMFRC,PEWETRC,CERCI,PIMAS,PINUM,PIFRC,PIPHIS0, &
                        PIDPHI0,PIPSI,PINFRC,PIMFRC,PIWETRC,O3FRC, &
                        H2O2FRC,CIRCI,XSIVFRC,HPFRC,XBC,TA,RHOA,DPA, &
                        PMREP,ZMLWC,PFSNOW,PFRAIN,ZCLF,CLRFR,CLRFS, &
                        PFEVAP,PFSUBL,XSIV,XHP,XO3,XNA,XAM,CO2_PPM,DT, &
                        ZCTHR,ILGA,LEVA,IERR)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CLOUD PROCESSES: WET REMOVAL AND SULPHATE IN-CLOUD PRODUCTION.
!
!     HISTORY:
!     --------
!     * JUN 10/2014 - K.VONSALZEN   SUBGRID TRACER PERTURBATION CONCENTRATION
!     * FEB 27/2012 - K.VONSALZEN   BUG FIX CLEAR/CLOUDY AND DIAGNOSTICS
!     * MAY 26/2010 - K.VONSALZEN   CHANGE CALL TO NM2PAR
!     * JAN 12/2009 - K.VONSALZEN   ACCOUNT FOR CONCENTRATION RATIOS
!     *                             CLOUDY-SKY/CLEAR-SKY
!     * JUN 12/2008 - K.VONSALZEN   INCLUDE CRITICAL PARTICLE SIZE
!     * OCT  2/2007 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, PARAMETER :: ASRPHOB = 0.
      REAL, PARAMETER :: ASRPHIL = 1.
      REAL, PARAMETER :: DRCRIT0= 0.05E-06
      REAL, PARAMETER :: CLDIMX = 0.9  ! limit non-homogeneity in concentration
                                       ! (1, max. inhom., 0, no inhom.)
      INTEGER, PARAMETER :: ISX=1
      INTEGER, PARAMETER :: NEQP=13
!
      LOGICAL, PARAMETER :: KSCCN=.FALSE.
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: XBC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: DSIVDT,DHPDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: DERSN,DERSM
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: DIRSMN,DIRSN,DIRSMS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: DSICP,DWR1N,DWR2N, &
                                                 DWR3N,DWR1M,DWR2M, &
                                                 DWR3M,DWS1N,DWS2N, &
                                                 DWS3N,DWS1M,DWS2M, &
                                                 DWS3M
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: PIHENR,PEHENR,O3FRC, &
                                                 H2O2FRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAEXT) :: PEDNDT,PEDMDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT) :: PIDNDT,PIDMDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIDFDT
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                           PENUM,PEMAS,PEDDN,PEPHIS0,PEDPHI0,PEPSI, &
                           PEWETRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                           PINUM,PIMAS,PIPHIS0,PIDPHI0,PIPSI, &
                           PIWETRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: TA,RHOA,DPA,PMREP, &
                           ZMLWC,PFSNOW,PFRAIN,ZCLF,CLRFR,CLRFS, &
                           PFEVAP,PFSUBL,XSIV,XHP,XO3,XNA,XAM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,KEXT) :: CERCI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: CIRCI
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: XSIVFRC,HPFRC
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAEXT) :: PENFRC,PEMFRC
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT) :: PINFRC
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIMFRC
      REAL, DIMENSION(ILGA,LEVA) :: PCESMAS,PCISMAS,ATERM,ATERMS,SDSM, &
                            DXSIV,DXSVI,DXHP,ZHENRY,XSVI,XSS,XCSIV, &
                            XASIV,XCHP,XAHP,TMPSCL,TEXSVI,TEXSVIP, &
                            TEXSVIA,TIXSVI,TIMASS,TIDEN,TIMASSP, &
                            TIMASSA
      REAL, DIMENSION(ILGA,LEVA,KEXT) :: PCERCI,PCEPCI
      REAL, DIMENSION(ILGA,LEVA) :: PCIRCI,PCIPCI
      REAL, DIMENSION(ILGA,LEVA,ISX) :: PXDMDT,DAXMAS,DCXMAS,DXWR1M, &
                            DXWR2M,DXWR3M,DXWS1M,DXWS2M,DXWS3M,PAXMAS, &
                            PCXMAS,PXHENM,PXMCEF
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PCEDNDT,PCEDMDT
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PCIDNDT,PCIDMDT
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PCIDFDT,PIMSCL
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                            PCENUM,PCEMAS,PCEN0,PCEPHI0,PCEPSI,PCEDDN, &
                            PAENUM,PAEMAS,PAEN0,PAEPHI0,PAEPSI,PAEDDN, &
                            PEHENN,PEHENM,DAENUM,DCENUM,DAEMAS,DCEMAS, &
                            PENCEF,PEMCEF,PENSCL,PEMSCL
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                            PCINUM,PCIMAS,PCIN0,PCIPHI0,PCIPSI,PCIDDN, &
                            PAINUM,PAIMAS,PAIN0,PAIPHI0,PAIPSI,PAIDDN, &
                            PIHENN,PIHENM,DAINUM,DCINUM,DAIMAS,DCIMAS, &
                            PINCEF,PIMCEF,PINSCL
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                            PSIMAS,PSHENM,PASMAS,PCSMAS,PSDMDT,DASMAS, &
                            DCSMAS,PSMCEF
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PCIFRC,PAIFRC,PSIFRC, &
                                               PAITMP,PCITMP,PITMPC, &
                                               PIMASI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIMAST
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TEDPHIS,TEPHISS,TIDPHIS, &
                                             TIPHISS,TETRUM
      REAL, ALLOCATABLE, DIMENSION(:,:) :: TITRUM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                            DEWR1N,DEWR2N,DEWR3N,DEWR1M,DEWR2M,DEWR3M, &
                            DEWS1N,DEWS2N,DEWS3N,DEWS1M,DEWS2M,DEWS3M
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                            DIWR1N,DIWR2N,DIWR3N,DIWR1M,DIWR2M,DIWR3M, &
                            DIWS1N,DIWS2N,DIWS3N,DIWS1M,DIWS2M,DIWS3M
      INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: PEICRIT
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: PIICRIT

!-----------------------------------------------------------------------
!     * ALLOCATION.
!
      IF ( ISAEXT > 0 ) THEN
        ALLOCATE(PCENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DCENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DCEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEN0   (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEPHI0 (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEPSI  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEDDN  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEDNDT (ILGA,LEVA,ISAEXT))
        ALLOCATE(PCEDMDT (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DAENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DAEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAEN0   (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAEPHI0 (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAEPSI  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PAEDDN  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEHENN  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEHENM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PENCEF  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEMCEF  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PENSCL  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEMSCL  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEICRIT (ILGA,LEVA,KEXT))
        ALLOCATE(DEWR1N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWR2N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWR3N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS1N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS2N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS3N  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWR1M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWR2M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWR3M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS1M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS2M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(DEWS3M  (ILGA,LEVA,ISAEXT))
        ALLOCATE(TEDPHIS (ILGA,LEVA,ISAEXT))
        ALLOCATE(TEPHISS (ILGA,LEVA,ISAEXT))
        ALLOCATE(TETRUM  (ILGA,LEVA,KEXT))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(PCINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(DCINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(DCIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIN0   (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIDDN  (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIDNDT (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIDMDT (ILGA,LEVA,ISAINT))
        ALLOCATE(PCIFRC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PCIDFDT (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PAITMP  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PCITMP  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PITMPC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIMASI  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PAINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(DAINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(DAIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIN0   (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIDDN  (ILGA,LEVA,ISAINT))
        ALLOCATE(PAIFRC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIMAST  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIHENN  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIHENM  (ILGA,LEVA,ISAINT))
        ALLOCATE(PSIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(PSHENM  (ILGA,LEVA,ISX))
        ALLOCATE(PASMAS  (ILGA,LEVA,ISX))
        ALLOCATE(PCSMAS  (ILGA,LEVA,ISX))
        ALLOCATE(PSDMDT  (ILGA,LEVA,ISX))
        ALLOCATE(DASMAS  (ILGA,LEVA,ISX))
        ALLOCATE(DCSMAS  (ILGA,LEVA,ISX))
        ALLOCATE(PSMCEF  (ILGA,LEVA,ISX))
        ALLOCATE(PSIFRC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PINCEF  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIMCEF  (ILGA,LEVA,ISAINT))
        ALLOCATE(PINSCL  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIMSCL  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIICRIT (ILGA,LEVA))
        ALLOCATE(DIWR1N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWR2N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWR3N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWS1N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWS2N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWS3N  (ILGA,LEVA,ISAINT))
        ALLOCATE(DIWR1M  (ILGA,LEVA,ISX))
        ALLOCATE(DIWR2M  (ILGA,LEVA,ISX))
        ALLOCATE(DIWR3M  (ILGA,LEVA,ISX))
        ALLOCATE(DIWS1M  (ILGA,LEVA,ISX))
        ALLOCATE(DIWS2M  (ILGA,LEVA,ISX))
        ALLOCATE(DIWS3M  (ILGA,LEVA,ISX))
        ALLOCATE(TIDPHIS (ILGA,LEVA,ISAINT))
        ALLOCATE(TIPHISS (ILGA,LEVA,ISAINT))
        ALLOCATE(TITRUM  (ILGA,LEVA))
      ENDIF
!
!     INITIALIZATIONS.
!
      XBC=0.
      DERSN =0.
      DERSM =0.
      DIRSMN=0.
      DIRSN =0.
      DIRSMS=0.
      DWR1N =0.
      DWR2N =0.
      DWR3N =0.
      DWR1M =0.
      DWR2M =0.
      DWR3M =0.
      DWS1N =0.
      DWS2N =0.
      DWS3N =0.
      DWS1M =0.
      DWS2M =0.
      DWS3M =0.
!
!-----------------------------------------------------------------------
!     * CRITICAL PARTICLE RADIUS (DRY) FOR CLOUD PROCESSING.
!
      IF ( KSCCN ) THEN
!
!       * USE SPECIFIED CRITICAL RADIUS.
!
        IF ( ISAEXT > 0 ) THEN
          PCERCI=DRCRIT0
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PCIRCI=DRCRIT0
        ENDIF
      ELSE
        IF ( ISAEXT > 0 ) THEN
          PCERCI=CERCI
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PCIRCI=CIRCI
        ENDIF
      ENDIF
!
!     * RESTRICT CRITICAL SIZES TO VALUES WITHIN THE SIMULATED 
!     * PARTICLE SIZE RANGE.
!
      IF ( ISAEXT > 0 ) THEN
        DO KX=1,KEXT
          PCERCI(:,:,KX)=MAX(PCERCI(:,:,KX), &
                         AEXTF%TP(KX)%DRYR(1)%VL &
                +0.01*(AEXTF%TP(KX)%DRYR(1)%VR-AEXTF%TP(KX)%DRYR(1)%VL))
          PCERCI(:,:,KX)=MIN(PCERCI(:,:,KX), &
                         AEXTF%TP(KX)%DRYR(AEXTF%TP(KX)%ISEC)%VR &
                -0.01*(AEXTF%TP(KX)%DRYR(AEXTF%TP(KX)%ISEC)%VR &
                -AEXTF%TP(KX)%DRYR(AEXTF%TP(KX)%ISEC)%VL))
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DO KX=1,KINT
          PCIRCI(:,:)=MAX(PCIRCI(:,:),AINTF%DRYR(1)%VL &
                    +0.01*(AINTF%DRYR(1)%VR-AINTF%DRYR(1)%VL))
          PCIRCI(:,:)=MIN(PCIRCI(:,:),AINTF%DRYR(AINTF%ISEC)%VR &
                    -0.01*(AINTF%DRYR(AINTF%ISEC)%VR & 
                    -AINTF%DRYR(AINTF%ISEC)%VL))
        ENDDO
      ENDIF
!
!     * CORRESPONDING PLA SIZE PARAMETER.
!
      IF ( ISAEXT > 0 ) THEN
        DO KX=1,KEXT
          ATERM=PCERCI(:,:,KX)/R0
          ATERMS=LOG(ATERM)
          PCEPCI(:,:,KX)=ATERMS
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ATERM=PCIRCI/R0
        PCIPCI=LOG(ATERM)
      ENDIF
!
!     * COPY GRID-CELL MEAN RESULTS INTO CLOUD (PC..) AND CLEAR-SKY (PA..)
!     * ARRAYS. CONCENTRATIONS DIFFERENCES BETWEEN CLOUDY AND CLEAR-SKY
!     * ARE GIVEN BY PENFRC AND PEMFRC FOR NUMBER RESP. MASS. FOR
!     * EXTERNALLY MIXED AEROSOL AND PINFRC AND PIMFRC FOR NUMBER RESP.
!     * MASS. FOR INTERNALLY MIXED AEROSOL (FROM PREVIOUS TIME STEP).
!     * THESE DIFFERENCES ARE ADDED TO THE GRID-CELL MEAN VALUE TO OBTAIN
!     * VALUES FOR CLEAR- AND CLOUDY-SKY.
!
      IF ( ISAEXT > 0 ) THEN
!
!       * FIRST, LIMIT CLEAR/CLOUD CONCENTRATION DIFFERENCES TO ENSURE
!       * POSITIVE CONCENTRATIONS IN THE ENTIRE GRID CELL.
!
        PENSCL=1.
        PEMSCL=1.
        DO IS=1,ISAEXT
          WHERE ( ABS(PEPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              WHERE ( PENFRC(:,:,IS) < -YSMALL )
                PENSCL(:,:,IS)=-PENUM(:,:,IS)*(1.-ZCLF(:,:)) &
                                                 /PENFRC(:,:,IS)*CLDIMX
              ELSEWHERE ( PENFRC(:,:,IS) > YSMALL )
                PENSCL(:,:,IS)=PENUM(:,:,IS)*ZCLF(:,:)/PENFRC(:,:,IS) &
                                                                *CLDIMX
              ELSEWHERE
                PENSCL(:,:,IS)=0.
              ENDWHERE
              WHERE ( PEMFRC(:,:,IS) < -YSMALL )
                PEMSCL(:,:,IS)=-PEMAS(:,:,IS)*(1.-ZCLF(:,:)) &
                                                 /PEMFRC(:,:,IS)*CLDIMX
              ELSEWHERE ( PEMFRC(:,:,IS) > YSMALL )
                PEMSCL(:,:,IS)=PEMAS(:,:,IS)*ZCLF(:,:)/PEMFRC(:,:,IS) &
                                                                *CLDIMX
              ELSEWHERE
                PEMSCL(:,:,IS)=0.
              ENDWHERE
            ENDWHERE
          ENDWHERE
        ENDDO
        PENSCL=MAX(MIN(PENSCL,1.),0.)
        PEMSCL=MAX(MIN(PEMSCL,1.),0.)
        PENSCL=MIN(PENSCL,PEMSCL)
        PEMSCL=PENSCL
        PENFRC=PENSCL*PENFRC
        PEMFRC=PEMSCL*PEMFRC
!
!       * NUMBER AND MASS FOR EXTERNALLY MIXED AEROSOL.
!
        PAENUM=0.
        PAEMAS=0.
        PCENUM=0.
        PCEMAS=0.
        DO IS=1,ISAEXT
          WHERE ( ABS(PEPSI(:,:,IS)-YNA) > YTINY )
            PAENUM(:,:,IS) = PENUM(:,:,IS)
            PAEMAS(:,:,IS) = PEMAS(:,:,IS)
            PCENUM(:,:,IS) = PENUM(:,:,IS)
            PCEMAS(:,:,IS) = PEMAS(:,:,IS)
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PAENUM(:,:,IS) = PAENUM(:,:,IS) &
                                         +PENFRC(:,:,IS)/(1.-ZCLF(:,:))
              PAEMAS(:,:,IS) = PAEMAS(:,:,IS) &
                                         +PEMFRC(:,:,IS)/(1.-ZCLF(:,:))
              PCENUM(:,:,IS) = PCENUM(:,:,IS) &
                                         -PENFRC(:,:,IS)/ZCLF(:,:)
              PCEMAS(:,:,IS) = PCEMAS(:,:,IS) &
                                         -PEMFRC(:,:,IS)/ZCLF(:,:)
            ENDWHERE
          ENDWHERE
        ENDDO
!
!       * DENSITY.
!
        PAEDDN=PEDDN
        PCEDDN=PEDDN
!
!       * TENDENCIES AND OTHER FIELDS.
!
        PEDNDT =0.
        PEDMDT =0.
        PCEDMDT=0.
        PCEDNDT=0.
        DAENUM =0.
        DCENUM =0.
        DAEMAS =0.
        DCEMAS =0.
      ENDIF
      IF ( ISAINT > 0 ) THEN
!
!       * FIRST, LIMIT CLEAR/CLOUD CONCENTRATION DIFFERENCES TO ENSURE
!       * POSITIVE CONCENTRATIONS IN THE ENTIRE GRID CELL.
!
        PINSCL=1.
        PIMSCL=1.
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              WHERE ( PINFRC(:,:,IS) < -YSMALL )
                PINSCL(:,:,IS)=-PINUM(:,:,IS)*(1.-ZCLF(:,:)) &
                                                 /PINFRC(:,:,IS)*CLDIMX
              ELSEWHERE ( PINFRC(:,:,IS) > YSMALL )
                PINSCL(:,:,IS)=PINUM(:,:,IS)*ZCLF(:,:)/PINFRC(:,:,IS) &
                                                                *CLDIMX
              ELSEWHERE
                PINSCL(:,:,IS)=0.
              ENDWHERE
            ENDWHERE
          ENDWHERE
        ENDDO
        DO KX=1,KINT
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              WHERE ( PIMFRC(:,:,IS,KX) < -YSMALL )
                PIMSCL(:,:,IS,KX)=-PIFRC(:,:,IS,KX)*PIMAS(:,:,IS) &
                               *(1.-ZCLF(:,:))/PIMFRC(:,:,IS,KX)*CLDIMX
              ELSEWHERE ( PIMFRC(:,:,IS,KX) > YSMALL )
                PIMSCL(:,:,IS,KX)=PIFRC(:,:,IS,KX)*PIMAS(:,:,IS) &
                                           *ZCLF(:,:)/PIMFRC(:,:,IS,KX) &
                                                                *CLDIMX
              ELSEWHERE
                PIMSCL(:,:,IS,KX)=0.
              ENDWHERE
            ENDWHERE
          ENDWHERE
        ENDDO
        ENDDO
        PINSCL=MAX(MIN(PINSCL,1.),0.)
        PIMSCL=MAX(MIN(PIMSCL,1.),0.)
        DO KX=1,KINT
          PINSCL=MIN(PINSCL,PIMSCL(:,:,:,KX))
          PIMSCL(:,:,:,KX)=PINSCL(:,:,:)
        ENDDO
        PINFRC=PINSCL*PINFRC
        PIMFRC=PIMSCL*PIMFRC
!
!       * NUMBER, MASS, AND MASS FRACTIONS FOR INTERNALLY MIXED AEROSOL
!       * FOR CLEAR-SKY.
!
        PAINUM=0.
        PAIMAS=0.
        PITMPC=0.
        PIMASI=0.
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            PAINUM(:,:,IS) = PINUM(:,:,IS)
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PAINUM(:,:,IS) = PAINUM(:,:,IS) &
                                         +PINFRC(:,:,IS)/(1.-ZCLF(:,:))
            ENDWHERE
          ENDWHERE
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
              PITMPC(:,:,IS,KX)=PIFRC(:,:,IS,KX)*PIMAS(:,:,IS)
              PIMASI(:,:,IS,KX)=PITMPC(:,:,IS,KX)
              WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
                PITMPC(:,:,IS,KX) = PITMPC(:,:,IS,KX) &
                                       +PIMFRC(:,:,IS,KX)/(1.-ZCLF(:,:))
              ENDWHERE
              PAIMAS(:,:,IS)=PAIMAS(:,:,IS)+PITMPC(:,:,IS,KX)
            ENDWHERE
          ENDDO
        ENDDO
        DO IT=1,KINT
          WHERE ( PAIMAS > MAX(PITMPC(:,:,:,IT)/YLARGE,YTINY) )
            PAIFRC(:,:,:,IT)=MAX(MIN(PITMPC(:,:,:,IT)/PAIMAS,1.),0.)
          ELSEWHERE
            PAIFRC(:,:,:,IT)=1./REAL(KINT)
          ENDWHERE
          PAITMP(:,:,:,IT)=PITMPC(:,:,:,IT)
        ENDDO
!
!       * NUMBER, MASS, AND MASS FRACTIONS FOR INTERNALLY MIXED AEROSOL
!       * FOR CLOUDY-SKY.
!
        PCINUM=0.
        PCIMAS=0.
        PITMPC=0.
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            PCINUM(:,:,IS) = PINUM(:,:,IS)
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PCINUM(:,:,IS) = PCINUM(:,:,IS)-PINFRC(:,:,IS)/ZCLF(:,:)
            ENDWHERE
          ENDWHERE
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
              PITMPC(:,:,IS,KX)=PIFRC(:,:,IS,KX)*PIMAS(:,:,IS)
              WHERE ( ZCLF(:,:) > YSMALL &
                                        .AND. (1.-ZCLF(:,:)) > YSMALL )
                PITMPC(:,:,IS,KX) = PITMPC(:,:,IS,KX) &
                                           -PIMFRC(:,:,IS,KX)/ZCLF(:,:)
              ENDWHERE
              PCIMAS(:,:,IS)=PCIMAS(:,:,IS)+PITMPC(:,:,IS,KX)
            ENDWHERE
          ENDDO
        ENDDO
        DO IT=1,KINT
          WHERE ( PCIMAS > MAX(PITMPC(:,:,:,IT)/YLARGE,YTINY) )
            PCIFRC(:,:,:,IT)=MAX(MIN(PITMPC(:,:,:,IT)/PCIMAS,1.),0.)
          ELSEWHERE
            PCIFRC(:,:,:,IT)=1./REAL(KINT)
          ENDWHERE
          PCITMP(:,:,:,IT)=PITMPC(:,:,:,IT)
        ENDDO
!
!       * TENDENCIES AND OTHER FIELDS.
!
        PIDNDT =0.
        PIDMDT =0.
        PIDFDT =0.
        PCIDNDT=0.
        PCIDMDT=0.
        PCIDFDT=0.
        DAINUM =0.
        DCINUM =0.
        DAIMAS =0.
        DCIMAS =0.
      ENDIF
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
        PXDMDT=0.
        DAXMAS=0.
        DCXMAS=0.
      ENDIF
!
!     * CLEAR-SKY AND CLOUDY-SKY CONCENTRATIONS FOR SULPHUR DIOXIDE.
!
      TMPSCL=1.
      WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
        WHERE ( XSIVFRC < -YSMALL )
          TMPSCL=-XSIV*(1.-ZCLF)/XSIVFRC
        ELSEWHERE ( XSIVFRC > YSMALL )
          TMPSCL=XSIV*ZCLF/XSIVFRC
        ELSEWHERE
          TMPSCL=0.
        ENDWHERE
      ENDWHERE
      TMPSCL=MAX(MIN(TMPSCL,1.),0.)
      XSIVFRC=TMPSCL*XSIVFRC
      XASIV=XSIV
      XCSIV=XSIV
      WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
        XASIV=XASIV+XSIVFRC/(1.-ZCLF)
        XCSIV=XCSIV-XSIVFRC/ZCLF
      ENDWHERE
!
!     * CLEAR-SKY AND CLOUDY-SKY CONCENTRATIONS FOR HYDROGEN PEROXIDE.
!
      TMPSCL=1.
      WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
        WHERE ( HPFRC < -YSMALL )
          TMPSCL=-XHP*(1.-ZCLF)/HPFRC
        ELSEWHERE ( HPFRC > YSMALL )
          TMPSCL=XHP*ZCLF/HPFRC
        ELSEWHERE
          TMPSCL=0.
        ENDWHERE
      ENDWHERE
      TMPSCL=MAX(MIN(TMPSCL,1.),0.)
      HPFRC=TMPSCL*HPFRC
      XAHP=XHP
      XCHP=XHP
      WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
        XAHP=XAHP+HPFRC/(1.-ZCLF)
        XCHP=XCHP-HPFRC/ZCLF
      ENDWHERE
!
!-----------------------------------------------------------------------
!     * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS FOR
!     * CLEAR-SKY AND CLOUDY-SKY.
!
      CALL NM2PAR (PAEN0,PAEPHI0,PAEPSI,PEPHIS0,PEDPHI0,PAIN0,PAIPHI0, &
                   PAIPSI,PIPHIS0,PIDPHI0,PAIDDN,PAENUM,PAEMAS,PAEDDN, &
                   PAINUM,PAIMAS,PAIFRC,ILGA,LEVA)
      CALL NM2PAR (PCEN0,PCEPHI0,PCEPSI,PEPHIS0,PEDPHI0,PCIN0,PCIPHI0, &
                   PCIPSI,PIPHIS0,PIDPHI0,PCIDDN,PCENUM,PCEMAS,PCEDDN, &
                   PCINUM,PCIMAS,PCIFRC,ILGA,LEVA)
      IF ( ISAINT > 0 ) THEN
        PSIMAS=PCIMAS
        PSIFRC=PCIFRC
      ENDIF
!
!-----------------------------------------------------------------------
!     * COLLECTION EFFICIENCY FOR BELOW-CLOUD SCAVENGING.
!
      CALL COLLEFF (PENCEF,PEMCEF,PAENUM,PAEMAS,PAEN0,PAEPHI0, &
                    PAEPSI,PEPHIS0,PEDPHI0,PEWETRC,PEDRYRC,PAEDDN, &
                    ILGA,LEVA,ISAEXT)
      CALL COLLEFF (PINCEF,PIMCEF,PAINUM,PAIMAS,PAIN0,PAIPHI0, &
                    PAIPSI,PIPHIS0,PIDPHI0,PIWETRC,PIDRYRC,PAIDDN, &
                    ILGA,LEVA,ISAINT)
!
!-----------------------------------------------------------------------
!     * TEMPORARY AEROSOL FIELDS FOR SULPHATE AEROSOL FOR DRY
!     * AND WET PARTICLE SIZES FOR EXTERNALLY MIXED AEROSOL TYPE.
!
      IF ( KEXT > 0 ) THEN
        DO IS=1,ISAEXT
          TEDPHIS(:,:,IS)=PEDPHIS(IS)
          TEPHISS(:,:,IS)=PEPHISS(IS)
        ENDDO
      ENDIF
!
!     * TEMPORARY AEROSOL FIELDS FOR SULPHATE AEROSOL FOR DRY
!     * AND WET PARTICLE SIZES FOR INTERNALLY MIXED AEROSOL TYPE.
!
      IF ( KINTSO4 > 0 ) THEN
        DO IS=1,ISAINT
          TIDPHIS(:,:,IS)=PIDPHIS(IS)
          TIPHISS(:,:,IS)=PIPHISS(IS)
        ENDDO
      ENDIF
!
!     * OVERWRITE SECTION BOUNDARIES TO ACCOUNT FOR CRITICAL
!     * PARTICLE RADIUS IN THE CLOUD. IN-CLOUD PROCESSING IS
!     * ONLY CONSIDERED FOR PARTICLE RADII THAT ARE EQUAL OR GREATER
!     * THAN THE CRITICAL RADIUS. THE NUMERICAL APPROACH IS TO FIRST FIND
!     * THE SECTION THAT INCLUDES THE CRITICAL RADIUS. THE LOWER SECTION
!     * BOUNDARY OF THAT SECTION IS THEN REDIFINED SO THAT IT MATCHES
!     * THE CRITICAL SIZE. SUBSEQUENT CALCULATIONS OF IN-CLOUD PROCESSES
!     * ARE ONLY APPLIED TO THAT AND FOLLOWING SECTIONS.
!
      IF ( KEXT > 0 ) THEN
        PEICRIT=INA
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          DO L=1,LEVA
          DO IL=1,ILGA
            IF (       PCEPCI(IL,L,KX) >= TEPHISS(IL,L,IS) &
                 .AND. PCEPCI(IL,L,KX) <  TEPHISS(IL,L,IS) &
                                          +TEDPHIS(IL,L,IS)    ) THEN
              TEDPHIS(IL,L,IS)=MAX(TEPHISS(IL,L,IS)+TEDPHIS(IL,L,IS) &
                                  -PCEPCI(IL,L,KX),0.)
              TEPHISS(IL,L,IS)=PCEPCI(IL,L,KX)
              PEICRIT(IL,L,KX)=IS
            ENDIF
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        PIICRIT=INA
        DO IS=1,ISAINT
        DO L=1,LEVA
        DO IL=1,ILGA
          IF (       PCIPCI(IL,L) >= TIPHISS(IL,L,IS) &
               .AND. PCIPCI(IL,L) <  TIPHISS(IL,L,IS) &
                                 +TIDPHIS(IL,L,IS)             ) THEN
            TIDPHIS(IL,L,IS)=MAX(TIPHISS(IL,L,IS)+TIDPHIS(IL,L,IS) &
                                -PCIPCI(IL,L),0.)
            TIPHISS(IL,L,IS)=PCIPCI(IL,L)
            PIICRIT(IL,L)=IS
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!     * CALCULATE NON-ACTIVATED AEROSOL CONCENTRATION FOR THE
!     * TRUNCATED SECTION THAT CONTAINS THE CRITICAL PARTICLE SIZE.
!
      RMOM=3.
      IF ( KEXT > 0 ) THEN
        TETRUM=0.
        DO KX=1,KEXT
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=PEICRIT(IL,L,KX)
          IF ( IS /= INA ) THEN
            IF ( ABS(PCEPSI(IL,L,IS)-YNA) > YTINY ) THEN
              ATMP2=PCEDDN(IL,L,IS)*YCNST*PCEN0(IL,L,IS) &
                        *SDINTB(PCEPHI0(IL,L,IS),PCEPSI(IL,L,IS),RMOM, &
                                TEPHISS(IL,L,IS),TEDPHIS(IL,L,IS))
              TETRUM(IL,L,KX)=MAX(PCEMAS(IL,L,IS)-ATMP2,0.)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        TITRUM=0.
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=PIICRIT(IL,L)
          IF ( IS /= INA ) THEN
            IF ( ABS(PCIPSI(IL,L,IS)-YNA) > YTINY ) THEN
              ATMP2=PCIDDN(IL,L,IS)*YCNST*PCIN0(IL,L,IS) &
                      *SDINTB(PCIPHI0(IL,L,IS),PCIPSI(IL,L,IS),RMOM, &
                              TIPHISS(IL,L,IS),TIDPHIS(IL,L,IS))
              TITRUM(IL,L)=MAX(PCIMAS(IL,L,IS)-ATMP2,0.)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
      ENDIF
!
!     * INITIALIZE FIELDS.
!
      DSIVDT=0.
      DHPDT=0.
      O3FRC=0.
      H2O2FRC=0.
      TEXSVI=0.
      TEXSVIP=0.
      TEXSVIA=0.
      TIXSVI=0.
      XSS=0.
      XBC=0.
      TIMASS=0.
      TIMASSP=0.
      TIMASSA=0.
      TIDEN=0.
!
!     * CLOUDY-SKY SULPHATE AEROSOL MASS CONCENTRATION
!     * FOR ACTIVATED PARTICLES.
!
      IF ( KEXTSO4 > 0 ) THEN
        KX=KEXTSO4
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PEICRIT(IL,L,KX)
          ISH=AEXTF%TP(KX)%ISO(AEXTF%TP(KX)%ISEC)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISH
              TEXSVI(IL,L)=TEXSVI(IL,L)+PCEMAS(IL,L,IS)
            ENDDO
            TEXSVI(IL,L)=MAX(TEXSVI(IL,L)-TETRUM(IL,L,KX),0.)
            TEXSVIA(IL,L)=PCEMAS(IL,L,ISL)
            TEXSVIP(IL,L)=MAX(TEXSVIA(IL,L)-TETRUM(IL,L,KX),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      IF ( KINTSO4 > 0 ) THEN
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PIICRIT(IL,L)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISAINT
              TIXSVI(IL,L)=TIXSVI(IL,L) &
                        +PCIMAS(IL,L,IS)*PCIFRC(IL,L,IS,KINTSO4)
            ENDDO
            TIXSVI(IL,L)=MAX(TIXSVI(IL,L)-TITRUM(IL,L) &
                                   *PCIFRC(IL,L,ISL,KINTSO4),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      XSVI=(TEXSVI+TIXSVI)*WS/WAMSUL
!
!     * CLOUDY-SKY SEA SALT AEROSOL MASS CONCENTRATION
!     * FOR ACTIVATED PARTICLES.
!
      IF ( KEXTSS > 0 ) THEN
        KX=KEXTSS
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PEICRIT(IL,L,KX)
          ISH=AEXTF%TP(KX)%ISO(AEXTF%TP(KX)%ISEC)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISH
              XSS(IL,L)=XSS(IL,L)+PCEMAS(IL,L,IS)
            ENDDO
            XSS(IL,L)=MAX(XSS(IL,L)-TETRUM(IL,L,KX),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      IF ( KINTSS > 0 ) THEN
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PIICRIT(IL,L)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISAINT
              XSS(IL,L)=XSS(IL,L) &
                        +PCIMAS(IL,L,IS)*PCIFRC(IL,L,IS,KINTSS)
            ENDDO
            XSS(IL,L)=MAX(XSS(IL,L)-TITRUM(IL,L) &
                                   *PCIFRC(IL,L,ISL,KINTSS),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
!
!     * CLOUDY-SKY BLACK CARBON AEROSOL MASS CONCENTRATION
!     * FOR ACTIVATED PARTICLES.
!
      IF ( KEXTBC > 0 ) THEN
        KX=KEXTBC
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PEICRIT(IL,L,KX)
          ISH=AEXTF%TP(KX)%ISO(AEXTF%TP(KX)%ISEC)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISH
              XBC(IL,L)=XBC(IL,L)+PCEMAS(IL,L,IS)
            ENDDO
            XBC(IL,L)=MAX(XBC(IL,L)-TETRUM(IL,L,KX),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      IF ( KINTBC > 0 ) THEN
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PIICRIT(IL,L)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISAINT
              XBC(IL,L)=XBC(IL,L) &
                        +PCIMAS(IL,L,IS)*PCIFRC(IL,L,IS,KINTBC)
            ENDDO
            XBC(IL,L)=MAX(XBC(IL,L)-TITRUM(IL,L) &
                                   *PCIFRC(IL,L,ISL,KINTBC),0.)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
!
!     * CLOUDY-SKY TOTAL AEROSOL MASS CONCENTRATION
!     * FOR ACTIVATED PARTICLES.
!
      IF ( KINT > 0 ) THEN
        DO L=1,LEVA
        DO IL=1,ILGA
          ISL=PIICRIT(IL,L)
          IF ( ISL /= INA ) THEN
            DO IS=ISL,ISAINT
              TIMASS(IL,L)=TIMASS(IL,L)+PCIMAS(IL,L,IS)
              TIDEN(IL,L)=PCIMAS(IL,L,IS)/PCIDDN(IL,L,IS)
            ENDDO
            TIMASS(IL,L)=MAX(TIMASS(IL,L)-TITRUM(IL,L),0.)
            TIMASSA(IL,L)=PCIMAS(IL,L,ISL)
            TIMASSP(IL,L)=MAX(TIMASSA(IL,L)-TITRUM(IL,L),0.)
            TIDEN(IL,L)=MAX(TIDEN(IL,L)-TITRUM(IL,L) &
                                       /PCIDDN(IL,L,ISL),0.)
          ENDIF
        ENDDO
        ENDDO
        WHERE ( TIDEN > MAX(TIMASS/YLARGE,YTINY) )
          TIDEN=TIMASS/TIDEN
        ELSEWHERE
          TIDEN=1000.
        ENDWHERE
      ENDIF
      IF ( KEXTSO4 > 0 .OR. KINTSO4 > 0 ) THEN
!
!       * CHANGES IN SO2, SO4, AND H2O2 CONCENTRATIONS FROM IN-CLOUD
!       * OXIDATION.
!
        CALL SULOXI (DXSIV,DXSVI,DXHP,ZHENRY,O3FRC,H2O2FRC,XSIV, &
                     XSVI,XO3,XCHP,XNA,XAM,XSS,ZCLF,ZMLWC,TA,RHOA, &
                     CO2_PPM,DT,ILGA,LEVA,NEQP)
        SDSM=DXSVI*WAMSUL/WS
        O3FRC=-O3FRC*WAMSUL/WS
        H2O2FRC=-H2O2FRC*WAMSUL/WS
!
!       * CHANGE IN TOTAL IN-CLOUD AEROSOL MASS AFTER OXIDATION.
!
        PCESMAS=0.
        PCISMAS=0.
        IF ( KEXT > 0 ) THEN
          PCESMAS=SDSM
        ENDIF
        IF ( KINT > 0 ) THEN
          PCISMAS=SDSM
        ENDIF
!
!       * ACCOUNT FOR CHANGES IN SIZE DISTRIBUTIONS FROM IN-CLOUD
!       * PRODUCTION OF SULPHATE. THE ADDITIONAL MASS IS DISTRIBUTED
!       * ACCORDING TO THE ASSUMPTION OF VOLUME-CONTROLLED GROWTH
!       * OF DROPLETS FROM IN-CLOUD OXIDATION.
!
        CALL SICPRD (PCEDNDT,PCEDMDT,PCIDNDT,PCIDMDT,PCIDFDT, &
                     DERSN,DERSM,DIRSMN,DIRSN,DIRSMS, &
                     TEXSVI,TEXSVIP,TEXSVIA,TIMASS,TIMASSP, &
                     TIMASSA,TIDEN,PEICRIT,PIICRIT,PCESMAS, &
                     PCEMAS,PCENUM,PCEN0,PCEPHI0,PCEPSI,PCEDDN, &
                     PCISMAS,PCIMAS,PCINUM,PCIN0,PCIPHI0,PCIPSI, &
                     PCIDDN,PCIFRC,DT,ILGA,LEVA,IERR)
!
!       * UPDATE RESIDUAL TERMS FOR DIAGNOSTIC PURPOSES.
!
        DERSN =ZCLF*DERSN
        DERSM =ZCLF*DERSM
        DIRSMN=ZCLF*DIRSMN
        DIRSN =ZCLF*DIRSN
        DIRSMS=ZCLF*DIRSMS
!
!       * UPDATE AEROSOL NUMBER AND MASS FOR IN-CLOUD SPECIES TO
!       * ACCOUNT FOR IN-CLOUD CHEMICAL PRODUCTION.
!
        IF ( ISAEXT > 0 ) THEN
          PCENUM=MAX(PCENUM+DT*PCEDNDT,0.)
          PCEMAS=MAX(PCEMAS+DT*PCEDMDT,0.)
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PCINUM=MAX(PCINUM+DT*PCIDNDT,0.)
          PCIMAS=MAX(PCIMAS+DT*PCIDMDT,0.)
          PCIFRC=MAX(MIN(PCIFRC+DT*PCIDFDT,1.),0.)
          DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            PIMASI(:,:,IS,KX)=PIMASI(:,:,IS,KX) &
                          +ZCLF(:,:)*(PCIFRC(:,:,IS,KX)*PCIMAS(:,:,IS) &
                                     -PSIFRC(:,:,IS,KX)*PSIMAS(:,:,IS))
            PCITMP(:,:,IS,KX)=PCIFRC(:,:,IS,KX)*PCIMAS(:,:,IS)
          ENDDO
          ENDDO
        ENDIF
        XCSIV=XCSIV+DXSIV
        XCHP=XCHP+DXHP
!
!-----------------------------------------------------------------------
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS FOR
!       * CLOUDY-SKY.
!
        CALL NM2PAR (PCEN0,PCEPHI0,PCEPSI,PEPHIS0,PEDPHI0,PCIN0,PCIPHI0, &
                     PCIPSI,PIPHIS0,PIDPHI0,PCIDDN,PCENUM,PCEMAS,PCEDDN, &
                     PCINUM,PCIMAS,PCIFRC,ILGA,LEVA)
      ENDIF
!
!     * DIAGNOSE IN-CLOUD PRODUCTION RATE FOR SULPHATE.
!
      DSICP=0.
      IF ( KEXTSO4 > 0 ) THEN
        DO IS=1,ISEXTSO4
          DSICP(:,:)=DSICP(:,:)+PCEDMDT(:,:,IEXSO4(IS))
        ENDDO
      ENDIF
      IF ( KINTSO4 > 0 ) THEN
        DO IS=1,ISINTSO4
          DSICP(:,:)=DSICP(:,:)+ZCLF(:,:) &
            *(PCIFRC(:,:,IINSO4(IS),KINTSO4)*PCIMAS(:,:,IINSO4(IS)) &
             -PSIFRC(:,:,IINSO4(IS),KINTSO4)*PSIMAS(:,:,IINSO4(IS)))/DT
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * WET REMOVAL.
!
      IF ( ISAEXT > 0 ) THEN
!
!       * SCAVENGING EFFICIENCY. COMPLETELY EFFICIENT IN-CLOUD REMOVAL
!       * IS ASSUMED FOR PARICLES CONTAINING SOLUBLE MATERIAL WITH
!       * RADII THAT EXCEED THE CRITICAL PARTICLE SIZE FOR IN-CLOUD
!       * PROCESSING. NO IN-CLOUD REMOVAL FOR COMPLETELY INSOLUBLE
!       * AEROSOL.
!
        DO KX=1,KEXT
        DO L=1,LEVA
        DO IL=1,ILGA
          ISI=AEXTF%TP(KX)%ISO(1)
          ISL=PEICRIT(IL,L,KX)
          ISH=AEXTF%TP(KX)%ISO(AEXTF%TP(KX)%ISEC)
          ISLP=MIN(ISL+1,ISH)
          IF ( ISL /= INA ) THEN
            PEHENN(IL,L,ISI:ISL)=ASRPHOB
            PEHENM(IL,L,ISI:ISL)=ASRPHOB
            PEHENN(IL,L,ISLP:ISH)=ASRPHIL
            PEHENM(IL,L,ISLP:ISH)=ASRPHIL
          ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!       * OVERWRITE RESULTS FOR SECTION CONTAINING CRITICAL PARICLE
!       * SIZE TO ACCOUNT FOR PARTIAL REMOVAL IN THAT SECTION. THE
!       * REMOVAL EFFICIENCIES ARE DETERMINED AS THE FRACTION OF
!       * THE AEROSOL WHOSE PARTICLE SIZES ARE GREATER THAN THE
!       * CRITICAL SIZE RELATIVE TO THE TOTAL AEROSOL IN THE SECTION.
!
        RMOM=3.
        DO KX=1,KEXT
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=PEICRIT(IL,L,KX)
          IF ( IS /= INA ) THEN
            IF ( ABS(PCEPSI(IL,L,IS)-YNA) > YTINY ) THEN
              AEXTSN=PCEN0(IL,L,IS) &
                    *SDINTB0(PCEPHI0(IL,L,IS),PCEPSI(IL,L,IS), &
                           TEPHISS(IL,L,IS),TEDPHIS(IL,L,IS))
              AEXTSM=PCEDDN(IL,L,IS)*YCNST*PCEN0(IL,L,IS) &
                    *SDINTB(PCEPHI0(IL,L,IS),PCEPSI(IL,L,IS),RMOM, &
                          TEPHISS(IL,L,IS),TEDPHIS(IL,L,IS))
              IF (        PCENUM(IL,L,IS) > YSMALL &
                    .AND. PCEMAS(IL,L,IS) > YSMALL &
                    .AND. ABS(PCEPSI(IL,L,IS)-YNA) > YTINY ) THEN
                WGTN=MIN(MAX(1.-AEXTSN/PCENUM(IL,L,IS),0.),1.)
                PEHENN(IL,L,IS)=ASRPHIL*(1.-WGTN)+WGTN*ASRPHOB
                WGTM=MIN(MAX(1.-AEXTSM/PCEMAS(IL,L,IS),0.),1.)
                PEHENM(IL,L,IS)=ASRPHIL*(1.-WGTM)+WGTM*ASRPHOB
              ENDIF
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!       * MEAN REMOVAL EFFICIENCY FOR SULPHATE FOR DIAGNOSTIC
!       * PURPOSES.
!
        IF ( KEXTSO4 > 0 ) THEN
          ATERM=0.
          ATERMS=0.
          DO IS=1,ISEXTSO4
            ATERM (:,:)=ATERM (:,:)+PCEMAS(:,:,IEXSO4(IS))
            ATERMS(:,:)=ATERMS(:,:)+PCEMAS(:,:,IEXSO4(IS)) &
                                   *PEHENM(:,:,IEXSO4(IS))
          ENDDO
          WHERE ( ATERM > MAX(ATERMS/YLARGE,YTINY) .AND. ZCLF > YTINY )
            PEHENR=ATERMS/ATERM
          ELSEWHERE
            PEHENR=0.
          ENDWHERE
        ENDIF
!
!       * CALCULATE TENDENCIES FOR WET REMOVAL FOR NUMBER.
!
        CALL SWETDEP(PEDNDT,DAENUM,DCENUM,DEWR1N,DEWR2N,DEWR3N, &
                     DEWS1N,DEWS2N,DEWS3N,PAENUM,PCENUM,PEHENN, &
                     PENCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                     ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                     ILGA,LEVA,ISAEXT)
!
!       * DIAGNOSE WET REMOVAL TENDENCIES FOR SULPHATE.
!
        IF ( KEXTSO4 > 0 ) THEN
          DO IS=1,ISEXTSO4
            DWR1N(:,:)=DWR1N(:,:)+DEWR1N(:,:,IEXSO4(IS))
            DWR2N(:,:)=DWR2N(:,:)+DEWR2N(:,:,IEXSO4(IS))
            DWR3N(:,:)=DWR3N(:,:)+DEWR3N(:,:,IEXSO4(IS))
            DWS1N(:,:)=DWS1N(:,:)+DEWS1N(:,:,IEXSO4(IS))
            DWS2N(:,:)=DWS2N(:,:)+DEWS2N(:,:,IEXSO4(IS))
            DWS3N(:,:)=DWS3N(:,:)+DEWS3N(:,:,IEXSO4(IS))
          ENDDO
        ENDIF
!
!       * CALCULATE TENDENCIES FOR WET REMOVAL FOR MASS.
!
        CALL SWETDEP(PEDMDT,DAEMAS,DCEMAS,DEWR1M,DEWR2M,DEWR3M, &
                     DEWS1M,DEWS2M,DEWS3M,PAEMAS,PCEMAS,PEHENM, &
                     PEMCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                     ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                     ILGA,LEVA,ISAEXT)
!
!       * DIAGNOSE WET REMOVAL TENDENCIES FOR SULPHATE.
!
        IF ( KEXTSO4 > 0 ) THEN
          DO IS=1,ISEXTSO4
            DWR1M(:,:)=DWR1M(:,:)+DEWR1M(:,:,IEXSO4(IS))
            DWR2M(:,:)=DWR2M(:,:)+DEWR2M(:,:,IEXSO4(IS))
            DWR3M(:,:)=DWR3M(:,:)+DEWR3M(:,:,IEXSO4(IS))
            DWS1M(:,:)=DWS1M(:,:)+DEWS1M(:,:,IEXSO4(IS))
            DWS2M(:,:)=DWS2M(:,:)+DEWS2M(:,:,IEXSO4(IS))
            DWS3M(:,:)=DWS3M(:,:)+DEWS3M(:,:,IEXSO4(IS))
          ENDDO
        ENDIF
      ENDIF
      IF ( ISAINT > 0 ) THEN
!
!       * SCAVENGING EFFICIENCY.
!
        DO L=1,LEVA
        DO IL=1,ILGA
          ISI=1
          ISL=PIICRIT(IL,L)
          ISH=ISAINT
          ISLP=MIN(ISL+1,ISH)
          IF ( ISL /= INA ) THEN
            PIHENN(IL,L,ISI:ISL)=ASRPHOB
            PIHENM(IL,L,ISI:ISL)=ASRPHOB
            PIHENN(IL,L,ISLP:ISH)=ASRPHIL
            PIHENM(IL,L,ISLP:ISH)=ASRPHIL
          ENDIF
        ENDDO
        ENDDO
!
!       * OVERWRITE RESULTS FOR SECTION CONTAINING CRITICAL PARICLE
!       * SIZE TO ACCOUNT FOR PARTIAL REMOVAL IN THAT SECTION.
!
        RMOM=3.
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=PIICRIT(IL,L)
          IF ( IS /= INA ) THEN
            IF ( ABS(PCIPSI(IL,L,IS)-YNA) > YTINY ) THEN
              AINTSN=PCIN0(IL,L,IS) &
                  *SDINTB0(PCIPHI0(IL,L,IS),PCIPSI(IL,L,IS), &
                           TIPHISS(IL,L,IS),TIDPHIS(IL,L,IS))
              AINTSM=PCIDDN(IL,L,IS)*YCNST*PCIN0(IL,L,IS) &
                  *SDINTB(PCIPHI0(IL,L,IS),PCIPSI(IL,L,IS),RMOM, &
                          TIPHISS(IL,L,IS),TIDPHIS(IL,L,IS))
              IF (        PCINUM(IL,L,IS) > YSMALL &
                    .AND. PCIMAS(IL,L,IS) > YSMALL               ) THEN
                WGTN=MIN(MAX(1.-AINTSN/PCINUM(IL,L,IS),0.),1.)
                PIHENN(IL,L,IS)=ASRPHIL*(1.-WGTN)+WGTN*ASRPHOB
                WGTM=MIN(MAX(1.-AINTSM/PCIMAS(IL,L,IS),0.),1.)
                PIHENM(IL,L,IS)=ASRPHIL*(1.-WGTM)+WGTM*ASRPHOB
              ENDIF
            ENDIF
          ENDIF
        ENDDO
        ENDDO
!
!       * MEAN REMOVAL EFFICIENCY FOR DIAGNOSTIC PURPOSES.
!
        ATERM=SUM(PCIMAS,DIM=3)
        ATERMS=SUM(PCIMAS*PIHENM,DIM=3)
        WHERE ( ATERM > MAX(ATERMS/YLARGE,YTINY) .AND. ZCLF > YTINY )
          PIHENR=ATERMS/ATERM
        ELSEWHERE
          PIHENR=0.
        ENDWHERE
!
!       * CALCULATE TENDENCIES FOR WET REMOVAL FOR NUMBER.
!
        CALL SWETDEP(PIDNDT,DAINUM,DCINUM,DIWR1N,DIWR2N,DIWR3N, &
                     DIWS1N,DIWS2N,DIWS3N,PAINUM,PCINUM,PIHENN, &
                     PINCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                     ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                     ILGA,LEVA,ISAINT)
!
!       * DIAGNOSE WET REMOVAL TENDENCIES FOR SULPHATE.
!
        IF ( KINTSO4 > 0 ) THEN
          DO IS=1,ISINTSO4
            DWR1N(:,:)=DWR1N(:,:)+DIWR1N(:,:,IINSO4(IS))
            DWR2N(:,:)=DWR2N(:,:)+DIWR2N(:,:,IINSO4(IS))
            DWR3N(:,:)=DWR3N(:,:)+DIWR3N(:,:,IINSO4(IS))
            DWS1N(:,:)=DWS1N(:,:)+DIWS1N(:,:,IINSO4(IS))
            DWS2N(:,:)=DWS2N(:,:)+DIWS2N(:,:,IINSO4(IS))
            DWS3N(:,:)=DWS3N(:,:)+DIWS3N(:,:,IINSO4(IS))
          ENDDO
        ENDIF
        DO IS=1,ISAINT
        DO IST=1,SINTF%ISAER(IS)%ITYPT
          KX=SINTF%ISAER(IS)%ITYP(IST)
!
!         * SAVE RESULTS FOR EACH INDIVIDUAL CHEMICAL SPECIES AND
!         * EACH SECTION.
!
          PSHENM(:,:,ISX)=PIHENM(:,:,IS)
          PASMAS(:,:,ISX)=PAIMAS(:,:,IS)*PAIFRC(:,:,IS,KX)
          PCSMAS(:,:,ISX)=PCIMAS(:,:,IS)*PCIFRC(:,:,IS,KX)
          PSMCEF(:,:,ISX)=PIMCEF(:,:,IS)
!
!         * CALCULATE TENDENCIES FOR WET REMOVAL FOR MASS.
!
          CALL SWETDEP(PSDMDT,DASMAS,DCSMAS,DIWR1M,DIWR2M,DIWR3M, &
                       DIWS1M,DIWS2M,DIWS3M,PASMAS,PCSMAS,PSHENM, &
                       PSMCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                       ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                       ILGA,LEVA,ISX)
!
!         * CALCULATE TOTAL TENDENCIES FOR ALL SPECIES IN EACH SECTION.
!
          PIDMDT(:,:,IS)=PIDMDT(:,:,IS)+PSDMDT(:,:,ISX)
          DCIMAS(:,:,IS)=DCIMAS(:,:,IS)+DCSMAS(:,:,ISX)
          DAIMAS(:,:,IS)=DAIMAS(:,:,IS)+DASMAS(:,:,ISX)
!
!         * CALCULATE NEW MASSES FOR EACH SPECIES AFTER IN-CLOUD PRODUCTION
!         * AND WET REMOVAL. FOR ALL-SKY, CLEAR-SKY, AND CLOUDY SKY.
!
          PIMASI(:,:,IS,KX)=PIMASI(:,:,IS,KX)+DT*PSDMDT(:,:,ISX)
          PAITMP(:,:,IS,KX)=PASMAS(:,:,ISX)-DASMAS(:,:,ISX)
          PCITMP(:,:,IS,KX)=PCSMAS(:,:,ISX)-DCSMAS(:,:,ISX)
!
!         * DIAGNOSE WET REMOVAL TENDENCIES FOR SULPHATE.
!
          IF ( KX == KINTSO4 ) THEN
            DWR1M(:,:)=DWR1M(:,:)+DIWR1M(:,:,ISX)
            DWR2M(:,:)=DWR2M(:,:)+DIWR2M(:,:,ISX)
            DWR3M(:,:)=DWR3M(:,:)+DIWR3M(:,:,ISX)
            DWS1M(:,:)=DWS1M(:,:)+DIWS1M(:,:,ISX)
            DWS2M(:,:)=DWS2M(:,:)+DIWS2M(:,:,ISX)
            DWS3M(:,:)=DWS3M(:,:)+DIWS3M(:,:,ISX)
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
!
!       * WET REMOVAL FOR SULPHUR DIOXIDE. NO BELOW-CLOUD SCAVENGING.
!
        PAXMAS(:,:,ISX)=XASIV
        PCXMAS(:,:,ISX)=XCSIV
        PXHENM(:,:,ISX)=ZHENRY
        PXMCEF(:,:,ISX)=0.
        CALL SWETDEP(PXDMDT,DAXMAS,DCXMAS,DXWR1M,DXWR2M,DXWR3M, &
                     DXWS1M,DXWS2M,DXWS3M,PAXMAS,PCXMAS,PXHENM, &
                     PXMCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                     ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                     ILGA,LEVA,ISX)
      ENDIF
!
!-----------------------------------------------------------------------
!     * UPDATE AEROSOL NUMBER AND MASS FOR CLEAR-SKY AND IN-CLOUD
!     * AEROSOL SPECIES TO ACCOUNT FOR EFFECTS OF WET REMOVAL. FOR
!     * INTERNALLY MIXED SPECIES, THE MASS FRACTIONS MAY CHANGE
!     * DUE TO WET REMOVAL SO THAT THE CORRESPONDING RESULTS FOR
!     * MASS WILL BE CALCULATED LATER.
!
      IF ( ISAEXT > 0 ) THEN
        PCENUM=MAX(PCENUM-DCENUM,0.)
        PCEMAS=MAX(PCEMAS-DCEMAS,0.)
        PAENUM=MAX(PAENUM-DAENUM,0.)
        PAEMAS=MAX(PAEMAS-DAEMAS,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PCINUM=MAX(PCINUM-DCINUM,0.)
        PAINUM=MAX(PAINUM-DAINUM,0.)
      ENDIF
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
        XCSIV=MAX(XCSIV-DCXMAS(:,:,1),0.)
        XASIV=MAX(XASIV-DAXMAS(:,:,1),0.)
      ENDIF
!
!     * UPDATE NUMBER AND MASS TENDENCIES FOR EXTERNALLY MIXED AEROSOL
!     * FOR EACH EXTERNALLY MIXED SPECIES AFTER IN-CLOUD PRODUCTION
!     * AND WET REMOVAL. FOR GRID-CELL MEAN RESULTS.
!
      IF ( ISAEXT > 0 ) THEN
        DO IS=1,ISAEXT
          PEDNDT(:,:,IS)=PEDNDT(:,:,IS)+ZCLF(:,:)*PCEDNDT(:,:,IS)
          PEDMDT(:,:,IS)=PEDMDT(:,:,IS)+ZCLF(:,:)*PCEDMDT(:,:,IS)
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
!
!       * TOTAL MASS FOR ALL SPECIES.
!
        PIMAST=0.
        DO IS=1,ISAINT
        DO IST=1,SINTF%ISAER(IS)%ITYPT
          KX=SINTF%ISAER(IS)%ITYP(IST)
          PIMAST(:,:,IS)=PIMAST(:,:,IS)+PIMASI(:,:,IS,KX)
        ENDDO
        ENDDO
!
!       * CHANGE IN MASS FRACTION FOR EACH SPECIES.
!
        DO IS=1,ISAINT
        DO IST=1,SINTF%ISAER(IS)%ITYPT
          KX=SINTF%ISAER(IS)%ITYP(IST)
          DO L=1,LEVA
          DO IL=1,ILGA
            IF ( PIMAST(IL,L,IS) > MAX(PIMASI(IL,L,IS,KX)/YLARGE,YTINY) &
                           .AND. ABS(PIPSI(IL,L,IS)-YNA) > YTINY ) THEN
              FRCN=MAX(MIN(PIMASI(IL,L,IS,KX)/PIMAST(IL,L,IS),1.),0.)
              PIDFDT(IL,L,IS,KX)=(FRCN-PIFRC(IL,L,IS,KX))/DT
            ELSE
              PIDFDT(IL,L,IS,KX)=0.
            ENDIF
          ENDDO
          ENDDO
        ENDDO
        ENDDO
!
!       * UPDATE NUMBER AND MASS TENDENCIES FOR INTERNALLY MIXED AEROSOL.
!
        DO IS=1,ISAINT
          PIDNDT(:,:,IS)=PIDNDT(:,:,IS)+ZCLF(:,:)*PCIDNDT(:,:,IS)
          PIDMDT(:,:,IS)=PIDMDT(:,:,IS)+ZCLF(:,:)*PCIDMDT(:,:,IS)
        ENDDO
      ENDIF
!
!       * UPDATE TENDENCIES FOR GASES.
!
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
        DSIVDT=PXDMDT(:,:,1)+ZCLF*DXSIV/DT
        DHPDT=ZCLF*DXHP/DT
      ENDIF
!
!-----------------------------------------------------------------------
!     * UPDATE CLEAR/CLOUD CONCENTRATION DIFFERENCES.
!
      IF ( ISAEXT > 0 ) THEN
        PENFRC=0.
        PEMFRC=0.
        DO IS=1,ISAEXT
          WHERE ( ABS(PEPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PENFRC(:,:,IS)=ZCLF(:,:)*(1.-ZCLF(:,:)) &
                                       *(PAENUM(:,:,IS)-PCENUM(:,:,IS))
              PEMFRC(:,:,IS)=ZCLF(:,:)*(1.-ZCLF(:,:)) &
                                       *(PAEMAS(:,:,IS)-PCEMAS(:,:,IS))
            ENDWHERE
          ENDWHERE
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINFRC=0.
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PINFRC(:,:,IS)=ZCLF(:,:)*(1.-ZCLF(:,:)) &
                                       *(PAINUM(:,:,IS)-PCINUM(:,:,IS))
            ENDWHERE
          ENDWHERE
        ENDDO
        PIMFRC=0.
        DO IT=1,KINT
        DO IS=1,ISAINT
          WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
            WHERE ( ZCLF(:,:) < ZCTHR .AND. (1.-ZCLF(:,:)) < ZCTHR )
              PIMFRC(:,:,IS,IT)=ZCLF(:,:)*(1.-ZCLF(:,:)) &
                                 *(PAITMP(:,:,IS,IT)-PCITMP(:,:,IS,IT))
            ENDWHERE
          ENDWHERE
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
        XSIVFRC=0.
        WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
          XSIVFRC=ZCLF*(1.-ZCLF)*(XASIV-XCSIV)
        ENDWHERE
        HPFRC=0.
        WHERE ( ZCLF < ZCTHR .AND. (1.-ZCLF) < ZCTHR )
          HPFRC=ZCLF*(1.-ZCLF)*(XAHP-XCHP)
        ENDWHERE
      ENDIF
!
!     * RESET IF NECESSARY.
!
      IF ( ISAEXT > 0 ) THEN
        WHERE ( ABS(PEPSI-YNA) <= YTINY )
          PEDNDT=0.
          PEDMDT=0.
        ENDWHERE
      ENDIF
      IF ( ISAINT > 0 ) THEN
        WHERE ( ABS(PIPSI-YNA) <= YTINY )
          PIDNDT=0.
          PIDMDT=0.
        ENDWHERE
      ENDIF
!
!-----------------------------------------------------------------------
!     * DEALLOCATION.
!
      IF ( ISAEXT > 0 ) THEN
        DEALLOCATE(PCENUM  )
        DEALLOCATE(PCEMAS  )
        DEALLOCATE(DCENUM  )
        DEALLOCATE(DCEMAS  )
        DEALLOCATE(PCEN0   )
        DEALLOCATE(PCEPHI0 )
        DEALLOCATE(PCEPSI  )
        DEALLOCATE(PCEDDN  )
        DEALLOCATE(PCEDNDT )
        DEALLOCATE(PCEDMDT )
        DEALLOCATE(PAENUM  )
        DEALLOCATE(PAEMAS  )
        DEALLOCATE(DAENUM  )
        DEALLOCATE(DAEMAS  )
        DEALLOCATE(PAEN0   )
        DEALLOCATE(PAEPHI0 )
        DEALLOCATE(PAEPSI  )
        DEALLOCATE(PAEDDN  )
        DEALLOCATE(PEHENN  )
        DEALLOCATE(PEHENM  )
        DEALLOCATE(PENCEF  )
        DEALLOCATE(PEMCEF  )
        DEALLOCATE(PENSCL  )
        DEALLOCATE(PEMSCL  )
        DEALLOCATE(PEICRIT )
        DEALLOCATE(DEWR1N  )
        DEALLOCATE(DEWR2N  )
        DEALLOCATE(DEWR3N  )
        DEALLOCATE(DEWS1N  )
        DEALLOCATE(DEWS2N  )
        DEALLOCATE(DEWS3N  )
        DEALLOCATE(DEWR1M  )
        DEALLOCATE(DEWR2M  )
        DEALLOCATE(DEWR3M  )
        DEALLOCATE(DEWS1M  )
        DEALLOCATE(DEWS2M  )
        DEALLOCATE(DEWS3M  )
        DEALLOCATE(TEDPHIS )
        DEALLOCATE(TEPHISS )
        DEALLOCATE(TETRUM  )
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(PCINUM  )
        DEALLOCATE(PCIMAS  )
        DEALLOCATE(DCINUM  )
        DEALLOCATE(DCIMAS  )
        DEALLOCATE(PCIN0   )
        DEALLOCATE(PCIPHI0 )
        DEALLOCATE(PCIPSI  )
        DEALLOCATE(PCIDDN  )
        DEALLOCATE(PCIDNDT )
        DEALLOCATE(PCIDMDT )
        DEALLOCATE(PCIFRC  )
        DEALLOCATE(PCIDFDT )
        DEALLOCATE(PAITMP  )
        DEALLOCATE(PCITMP  )
        DEALLOCATE(PITMPC  )
        DEALLOCATE(PIMASI  )
        DEALLOCATE(PAINUM  )
        DEALLOCATE(PAIMAS  )
        DEALLOCATE(DAINUM  )
        DEALLOCATE(DAIMAS  )
        DEALLOCATE(PAIN0   )
        DEALLOCATE(PAIPHI0 )
        DEALLOCATE(PAIPSI  )
        DEALLOCATE(PAIDDN  )
        DEALLOCATE(PAIFRC  )
        DEALLOCATE(PIMAST  )
        DEALLOCATE(PIHENN  )
        DEALLOCATE(PIHENM  )
        DEALLOCATE(PSIMAS  )
        DEALLOCATE(PSHENM  )
        DEALLOCATE(PASMAS  )
        DEALLOCATE(PCSMAS  )
        DEALLOCATE(PSDMDT  )
        DEALLOCATE(DASMAS  )
        DEALLOCATE(DCSMAS  )
        DEALLOCATE(PSMCEF  )
        DEALLOCATE(PSIFRC  )
        DEALLOCATE(PINCEF  )
        DEALLOCATE(PIMCEF  )
        DEALLOCATE(PINSCL  )
        DEALLOCATE(PIMSCL  )
        DEALLOCATE(PIICRIT )
        DEALLOCATE(DIWR1N  )
        DEALLOCATE(DIWR2N  )
        DEALLOCATE(DIWR3N  )
        DEALLOCATE(DIWS1N  )
        DEALLOCATE(DIWS2N  )
        DEALLOCATE(DIWS3N  )
        DEALLOCATE(DIWR1M  )
        DEALLOCATE(DIWR2M  )
        DEALLOCATE(DIWR3M  )
        DEALLOCATE(DIWS1M  )
        DEALLOCATE(DIWS2M  )
        DEALLOCATE(DIWS3M  )
        DEALLOCATE(TIDPHIS )
        DEALLOCATE(TIPHISS )
        DEALLOCATE(TITRUM  )
      ENDIF
!
      END SUBROUTINE SCLDSP
