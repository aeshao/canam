      SUBROUTINE GRWTRM(CNDGP,CGR1,CGR2,CT1,CT2,PN0,ANUM,WETRC, &
                        DRYRC,DRYR,DDN,PHI0,PSI,PHISS,DPHIS, &
                        FRP,DFSO4,WRAT,RHOA,LEVA,ILGA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     RATE COEFFICIENT FOR CONDENSATION OF H2SO4 ONTO PRE-EXISTING
!     AEROSOL (CNDGP, IN 1/S).
!
!     HISTORY:
!     --------
!     * APR 17/2015 - K.VONSALZEN   CHANGES TO SUPPORT SINGLE PRECISION
!     * NOV 17/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL(R8), INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: &
                                                CNDGP,CT1,CT2
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: &
                                                CGR1,CGR2
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                                ANUM,WETRC,DDN,PHI0, &
                                                PSI,PHISS,DPHIS,PN0
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA) :: DFSO4
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: FRP,RHOA
      REAL, INTENT(IN), DIMENSION(ISEC) :: DRYRC
      REAL, INTENT(IN), DIMENSION(ISEC+1) :: DRYR
      REAL, INTENT(IN) :: WRAT
      REAL(R8), DIMENSION(ILGA,LEVA,ISEC) :: DBL
      REAL(R8) :: FGR,WETRL,WETRR,AKNL,AKNR,FNL,FNR,ANL,ANR,DR,DAFDR, &
                  CTERM
!
!     * INTRINSIC FUNCTIONS.
!
      REAL(R8) :: ZFN,ZAN,AG1,AG2
      ZFN(AG1)=(1.+AG1)/(1.+1.71*AG1+1.33*AG1*AG1)
      ZAN(AG1,AG2)=1./(1.+1.33*AG1*AG2*(1./ACE-1.))
!
!-----------------------------------------------------------------------
!
      CGR1=0.
      CGR2=0.
      CT1=0.
      CT2=0.
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ANUM(IL,L,IS) > YTINY ) THEN
          FGR=WETRC(IL,L,IS)/DRYRC(IS)
!
!         * TERM A*F IN GROWTH EQUATION AT LOWER AND UPPER BOUNDARIES
!         * OF THE SECTION FROM LINEARIZATION.
!
          WETRL=FGR*DRYR(IS)
          AKNL=FRP(IL,L)/WETRL
          FNL=ZFN(AKNL)
          ANL=ZAN(AKNL,FNL)
          WETRR=FGR*DRYR(IS+1)
          AKNR=FRP(IL,L)/WETRR
          FNR=ZFN(AKNR)
          ANR=ZAN(AKNR,FNR)
          AFL=FNL*ANL
          DR=DRYR(IS+1)-DRYR(IS)
          DAFDR=(FNR*ANR-AFL)/DR
!
!         * TERMS FOR CALCULATION OF SECTION-MEAN CONDENSATION RATE.
!
          CTERM=4.*YPI*DFSO4(IL,L)*FGR*RHOA(IL,L)
          CT1(IL,L,IS)=CTERM*(AFL-DAFDR*DRYR(IS))
          CT2(IL,L,IS)=CTERM*DAFDR
!
!         * TERMS FOR CALCULATION OF GROWTH FACTOR (CG, M5/KG/SEC).
!
          CTERM=DFSO4(IL,L)*FGR*WRAT/DDN(IL,L,IS)
          CGR1(IL,L,IS)=CTERM*(AFL-DAFDR*DRYR(IS))
          CGR2(IL,L,IS)=CTERM*DAFDR
        ENDIF
      ENDDO
      ENDDO
      ENDDO
      RMOM1=1.
      RMOM2=2.
      DBL=PN0
      CNDGP=DBL*(CT1*SDINT(PHI0,PSI,RMOM1,PHISS,DPHIS,ILGA,LEVA,ISEC) &
                +CT2*SDINT(PHI0,PSI,RMOM2,PHISS,DPHIS,ILGA,LEVA,ISEC))
      CNDGP=MAX(0._R8,CNDGP)
!
      END SUBROUTINE GRWTRM
