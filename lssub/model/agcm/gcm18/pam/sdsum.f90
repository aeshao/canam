      SUBROUTINE SDSUM
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     WRITES OUT SUMMARY OF RESULTS FROM PLA INITIALIZATION.
!
!     HISTORY:
!     --------
!     * MAR 31/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      CHARACTER(LEN=17) :: CTMP17
      CHARACTER(LEN=3) :: CTMP2
!
!-----------------------------------------------------------------------
!
      WRITE(6,'(1X)')
      WRITE(6,'(A22)')    '*** Aerosol Properties'
      WRITE(6,'(1X)')
      WRITE(6,'(A36,I3)') &
            'Total number of types of aerosols : ',KEXT+KINT
      WRITE(6,'(A36,I3)') &
            '                 Externally mixed : ',KEXT
      WRITE(6,'(A36,I3)') &
            '                 Internally mixed : ',KINT
      IF ( KEXT > 0 ) THEN
        WRITE(6,'(1X)')
        WRITE(6,'(A34)') &
            'Externally mixed types of aerosols'
        DO KX=1,KEXT
          WRITE(6,'(1X)')
          WRITE(6,'(2X,A70)') AEXTF%TP(KX)%NAME
          WRITE(6,'(2X,A28,I3)') &
              'Number of sections        : ',AEXTF%TP(KX)%ISEC
          WRITE(6,'(2X,A28,F6.3,A3,F6.3)') &
              'Radius range (micron)     : ',AEXTF%TP(KX)%RADB/R0,' - ' &
                                            ,AEXTF%TP(KX)%RADE/R0
          WRITE(6,'(2X,A28,F10.8,A4,F10.8)') &
              'Width of sections         : ',AEXTF%TP(KX)%DPST,' -> ', &
                                             AEXTF%TP(KX)%DPSTAR
          WRITE(6,'(4X,A43)') &
              'Section    PLA distribution width parameter'
          DO IS=1,AEXTF%TP(KX)%ISEC
            VMIN=YLARGE
            VMAX=-YLARGE
            DO IR=1,AEXTF%TP(KX)%IDRB
              VAL=AEXTF%TP(KX)%PSIB(IS,IR)%VL
              IF ( AEXTF%TP(KX)%FLG(IS,IR) ) THEN
                VMIN=MIN(VMIN,VAL)
                VMAX=MAX(VMAX,VAL)
              ENDIF
            ENDDO
            WRITE(6,'(4X,I3,7X,F5.2,A4,F6.2,A3,F6.2)') &
              IS,AEXTF%TP(KX)%PSIM(IS),' -> ',VMIN,' - ',VMAX
          ENDDO
          WRITE(6,'(2X,A28,F7.2)') &
              'Density (kg/m3)           : ',AEXTF%TP(KX)%DENS
          WRITE(6,'(2X,A28,F7.2)') &
              'Molecular weight (g/mol)  : ',AEXTF%TP(KX)%MOLW*1.E+03
          WRITE(6,'(2X,A28,F7.2)') &
              'Number of ions            : ',AEXTF%TP(KX)%NUIO
          WRITE(6,'(2X,A28,F7.2)') &
              'Kappa                     : ',AEXTF%TP(KX)%KAPPA
          WRITE(6,'(2X,A28,F7.2)') &
              'Sticking probability      : ',AEXTF%TP(KX)%STICK
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        WRITE(6,'(1X)')
        WRITE(6,'(A34)') &
            'Internally mixed types of aerosols'
        WRITE(6,'(1X)')
        WRITE(6,'(2X,A28,I3)') &
              'Number of sections        : ',AINTF%ISEC
        WRITE(6,'(2X,A28,F6.3,A3,F6.3)') &
              'Radius range (micron)     : ',AINTF%RADB/R0,' - ' &
                                            ,AINTF%RADE/R0
        WRITE(6,'(2X,A28,F10.8,A4,F10.8)') &
              'Width of sections         : ',AINTF%DPST,' -> ', &
                                             AINTF%DPSTAR
        WRITE(6,'(2X,A28,F7.2)') &
              'Sticking probability      : ',AINTF%STICK
        WRITE(6,'(4X,A43)') &
              'Section    PLA distribution width parameter'
        DO IS=1,AINTF%ISEC
          VMIN=YLARGE
          VMAX=-YLARGE
          DO IR=1,AINTF%IDRB
            VAL=AINTF%PSIB(IS,IR)%VL
            IF ( AINTF%FLG(IS,IR) ) THEN
              VMIN=MIN(VMIN,VAL)
              VMAX=MAX(VMAX,VAL)
            ENDIF
          ENDDO
          WRITE(6,'(4X,I3,7X,F5.2,A4,F6.2,A3,F6.2)') &
              IS,AINTF%PSIM(IS),' -> ',VMIN,' - ',VMAX
        ENDDO
        DO KX=1,KINT
          WRITE(6,'(1X)')
          WRITE(6,'(2X,A70)') AINTF%TP(KX)%NAME
          ISECT=AINTF%TP(KX)%ISCE-AINTF%TP(KX)%ISCB+1
          IF ( ISECT /= AINTF%ISEC ) THEN
            WRITE(6,'(2X,A28,I2)') &
              'Number of sections        : ',ISECT
            WRITE(6,'(2X,A28,F6.3,A3,F6.3)') &
              'Radius range (micron)     : ', &
                              AINTF%DRYR(AINTF%TP(KX)%ISCB)%VL/R0,' - ' &
                             ,AINTF%DRYR(AINTF%TP(KX)%ISCE)%VR/R0
          ENDIF
          WRITE(6,'(2X,A28,F7.2)') &
              'Density (kg/m3)           : ',AINTF%TP(KX)%DENS
          WRITE(6,'(2X,A28,F7.2)') &
              'Molecular weight (g/mol)  : ',AINTF%TP(KX)%MOLW*1.E+03
          WRITE(6,'(2X,A28,F7.2)') &
              'Number of ions            : ',AINTF%TP(KX)%NUIO
          WRITE(6,'(2X,A28,F7.2)') &
              'Kappa                     : ',AINTF%TP(KX)%KAPPA
        ENDDO
      ENDIF
      WRITE(6,'(1X)')
      WRITE(6,'(A19)')    '*** Aerosol Tracers'
      WRITE(6,'(1X)')
      WRITE(6,'(A34,I3)') &
            'Total number of aerosol tracers : ',NTRACS
      WRITE(6,'(1X)')
      WRITE(6,'(A26)') &
            'Aerosol mass concentration'
      IF ( KEXT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Externally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISAEXT
          CTMP17=AEXTF%TP(SEXTF%ISAER(IS)%ITYP)%NAME
          IND=SEXTF%ISAER(IS)%ISM
          IF ( IND < 10 ) THEN
            WRITE(CTMP2,'(A2,I1)') 'A0',IND
          ELSE
            WRITE(CTMP2,'(A1,I2)') 'A',IND
          ENDIF
          WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SEXTF%ISAER(IS)%ISI
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Internally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            CTMP17=AINTF%TP(SINTF%ISAER(IS)%ITYP(IST))%NAME
            IND=SINTF%ISAER(IS)%ISM(IST)
            IF ( IND < 10 ) THEN
              WRITE(CTMP2,'(A2,I1)') 'A0',IND
            ELSE
              WRITE(CTMP2,'(A1,I2)') 'A',IND
            ENDIF
            WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SINTF%ISAER(IS)%ISI
          ENDDO
        ENDDO
      ENDIF
      WRITE(6,'(A28)') &
            'Aerosol number concentration'
      IF ( KEXT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Externally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISAEXT
          CTMP17=AEXTF%TP(SEXTF%ISAER(IS)%ITYP)%NAME
          IND=SEXTF%ISAER(IS)%ISN
          IF ( IND < 10 ) THEN
            WRITE(CTMP2,'(A2,I1)') 'A0',IND
          ELSE
            WRITE(CTMP2,'(A1,I2)') 'A',IND
          ENDIF
          WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SEXTF%ISAER(IS)%ISI
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Internally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            CTMP17=AINTF%TP(SINTF%ISAER(IS)%ITYP(IST))%NAME
            IND=SINTF%ISAER(IS)%ISN
            IF ( IND < 10 ) THEN
              WRITE(CTMP2,'(A2,I1)') 'A0',IND
            ELSE
              WRITE(CTMP2,'(A1,I2)') 'A',IND
            ENDIF
            WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SINTF%ISAER(IS)%ISI
          ENDDO
        ENDDO
      ENDIF
      WRITE(6,'(1X)')
      WRITE(6,'(A36,I3)') &
            'Number of soluble aerosol tracers : ',ISWETT
      WRITE(6,'(1X)')
      WRITE(6,'(A26)') &
            'Aerosol mass concentration'
      IF ( KEXT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Externally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISWEXT
          CTMP17=AEXTF%TP(SEXTF%ISWET(IS)%ITYP)%NAME
          IND=SEXTF%ISWET(IS)%ISM
          IF ( IND < 10 ) THEN
            WRITE(CTMP2,'(A2,I1)') 'A0',IND
          ELSE
            WRITE(CTMP2,'(A1,I2)') 'A',IND
          ENDIF
          WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SEXTF%ISWET(IS)%ISI
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Internally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISWINT
          DO IST=1,SINTF%ISWET(IS)%ITYPT
            CTMP17=AINTF%TP(SINTF%ISWET(IS)%ITYP(IST))%NAME
            IND=SINTF%ISWET(IS)%ISM(IST)
            IF ( IND < 10 ) THEN
              WRITE(CTMP2,'(A2,I1)') 'A0',IND
            ELSE
              WRITE(CTMP2,'(A1,I2)') 'A',IND
            ENDIF
            WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SINTF%ISWET(IS)%ISI
          ENDDO
        ENDDO
      ENDIF
      WRITE(6,'(A28)') &
            'Aerosol number concentration'
      IF ( KEXT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Externally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISWEXT
          CTMP17=AEXTF%TP(SEXTF%ISWET(IS)%ITYP)%NAME
          IND=SEXTF%ISWET(IS)%ISN
          IF ( IND < 10 ) THEN
            WRITE(CTMP2,'(A2,I1)') 'A0',IND
          ELSE
            WRITE(CTMP2,'(A1,I2)') 'A',IND
          ENDIF
          WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SEXTF%ISWET(IS)%ISI
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        WRITE(6,'(A36)') &
            '  Internally mixed types of aerosols'
        WRITE(6,'(2X,A50)')                                             &
            'Aerosol Index      Aerosol Type      Section Index'
        DO IS=1,ISWINT
          DO IST=1,SINTF%ISWET(IS)%ITYPT
            CTMP17=AINTF%TP(SINTF%ISWET(IS)%ITYP(IST))%NAME
            IND=SINTF%ISWET(IS)%ISN
            IF ( IND < 10 ) THEN
              WRITE(CTMP2,'(A2,I1)') 'A0',IND
            ELSE
              WRITE(CTMP2,'(A1,I2)') 'A',IND
            ENDIF
            WRITE(6,'(4X,A3,14X,A17,3X,I3)') &
                                       CTMP2,CTMP17,SINTF%ISWET(IS)%ISI
          ENDDO
        ENDDO
      ENDIF
      WRITE(6,'(1X)')
!
      END SUBROUTINE SDSUM
