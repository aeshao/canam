      SUBROUTINE CORNME (RESM,RESN,CORM,CORN,BNUM,BMASS,DRYDN, &
                         ISMIN,ISMAX,PHISS,DPHIS,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CHECK AND CORRECT NUMBER AND MASS IN EACH SECTION OF THE SIZE
!     DISRIBUTIONS IN ORDER TO AVOID INVALID INPUTS TO MICROPHYSICS
!     AND/OR CHEMISTRY CALCULATIONS. THIS SUBROUTINE SHOULD BE USED
!     FOR EXTERNALLY MIXED AEROSOL SPECIES.
!
!     HISTORY:
!     --------
!     * MAR 08/2007 - K.VONSALZEN   SPLIT CODE FOR EXT. AND INT.
!     *                             MIXTURES (BASED ON CORNM)
!     * APR 07/2006 - K.VONSALZEN   COMPLETELY REVISED FOR USE WITH
!     *                             INTERNALLY AND EXTERNALLY MIXED
!     *                             TYPES OF AEROSOLS.
!     * DEC 11/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: RESM,RESN,CORM,CORN
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISEC) :: BNUM,BMASS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: DRYDN
      REAL, INTENT(IN), DIMENSION(ISEC) :: PHISS,DPHIS
      INTEGER, INTENT(IN), DIMENSION(ISEC) :: ISMIN,ISMAX
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: ANUM,AMASS,PHIHAT,ARG,TERM
      REAL, PARAMETER :: YSCAL=1.E+09
!
!-----------------------------------------------------------------------
!     * ALLOCATE WORK ARRAYS.
!
      ALLOCATE(ANUM  (ILGA,LEVA,ISEC))
      ALLOCATE(AMASS (ILGA,LEVA,ISEC))
      ALLOCATE(PHIHAT(ILGA,LEVA,ISEC))
      ALLOCATE(ARG   (ILGA,LEVA,ISEC))
      ALLOCATE(TERM  (ILGA,LEVA,ISEC))
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS.
!
      ONETHIRD=1./3.
      R0P=R0**3
      ACNST=YCNST*R0P*YSCAL
      RESN=0.
      RESM=0.
      CORN=0.
      CORM=0.
!
!-----------------------------------------------------------------------
!     * MAKE SURE THERE ARE ONLY POSITIVE VALUES FOR NUMBER AND MASS
!     * IN EACH SECTION. ALSO MAKE SURE THAT NUMBER AND MASS FALL
!     * INTO THE CORRECT SECTION BY CHECKING WHETHER THE RATIO
!     * MASS/NUMBER IS IN THE CORRECT RANGE, I.E. THE MAXIMUM AND
!     * MINIMUM POSSIBLE PARTICLE SIZES FOR DELTA-FUNCTION SIZE
!     * DISTRIBUTIONS ARE NOT SMALLER OR BIGGER THAN THE PARTICLE SIZE
!     * RANGE DEFINED BY THE BOUNDARIES OF THE SECTIONS. MOVE MASS AND
!     * NUMBER TO THE CORRECT SECTION IN CASE THERE IS NO PHYSICAL
!     * SOLUTION.
!
      ARG=YNA
      TERM=ACNST*DRYDN*BNUM
      AMASS=BMASS*YSCAL
      WHERE ( BNUM > YTINY .AND. BMASS > YTINY &
                                 .AND. TERM > MAX(AMASS/YLARGE,YTINY) )
        ARG=AMASS/TERM
      ENDWHERE
      ANUM=0.
      AMASS=0.
      WHERE ( ABS(ARG-YNA) <= YTINY )
        ARG=1.
        ANUM=BNUM
        AMASS=BMASS
        BNUM =0.
        BMASS=0.
      ENDWHERE
      DO IS=1,ISEC
        RESN(:,:)=RESN(:,:)+ANUM (:,:,IS)
        RESM(:,:)=RESM(:,:)+AMASS(:,:,IS)
      ENDDO
      CORN=CORN+RESN
      CORM=CORM+RESM
      PHIHAT=ONETHIRD*LOG(ARG)
!
!-----------------------------------------------------------------------
!     * RESIDUALS.
!
      ANUM =0.
      AMASS=0.
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( BNUM(IL,L,IS) > YTINY .AND. BMASS(IL,L,IS) > YTINY ) THEN
          RAT0=(PHIHAT(IL,L,IS)-PHISS(IS))/DPHIS(IS)
          IBOFF=INT(RAT0)
          IF ( RAT0 < 0. ) IBOFF=IBOFF-1
          IF ( IBOFF /= 0 ) THEN
            ISO=IS+IBOFF
            IF ( ISO < ISMIN(IS) .OR. ISO > ISMAX(IS) ) THEN
              RESN(IL,L)=RESN(IL,L)+BNUM (IL,L,IS)
              RESM(IL,L)=RESM(IL,L)+BMASS(IL,L,IS)
            ELSE
              ANUM (IL,L,ISO)=ANUM (IL,L,ISO)+BNUM (IL,L,IS)
              AMASS(IL,L,ISO)=AMASS(IL,L,ISO)+BMASS(IL,L,IS)
            ENDIF
            CORN(IL,L)=CORN(IL,L)+BNUM (IL,L,IS)
            CORM(IL,L)=CORM(IL,L)+BMASS(IL,L,IS)
            BNUM (IL,L,IS)=0.
            BMASS(IL,L,IS)=0.
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      ENDDO
      BNUM =BNUM +ANUM
      BMASS=BMASS+AMASS
!
!-----------------------------------------------------------------------
!     * DEALLOCATE WORK ARRAYS.
!
      DEALLOCATE(ANUM)
      DEALLOCATE(AMASS)
      DEALLOCATE(PHIHAT)
      DEALLOCATE(ARG)
      DEALLOCATE(TERM)
!
      END SUBROUTINE CORNME
