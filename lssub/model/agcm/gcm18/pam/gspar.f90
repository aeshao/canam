      SUBROUTINE GSPAR(GSVDN,GSVDM,RHOA,VCFL,AMU,AMFP,ANUM,AMAS,PN0, &
                       PHI0,PSI,PHIS0,DPHI0,DRYDN,WETRC, &
                       DRYRC,GRAV,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     GRAVITATIONAL SETTLING VELOCITIES FOR AEROSOL NUMBER AND MASS
!     CONCENTRATIONS.
!
!     HISTORY:
!     --------
!     * DEC 26/2007 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: GSVDN,GSVDM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                         ANUM,AMAS,PN0,PHI0,PSI,WETRC, &
                                         DRYDN,PHIS0,DPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: RHOA,VCFL,AMU,AMFP
      REAL, INTENT(IN), DIMENSION(ISEC) :: DRYRC
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, DIMENSION(ILGA,LEVA,ISEC) :: AM0,AM1,AM2,AM3,AM4,AM5,PHI0W, &
                                         PHIS0W,ASCM,ASCN,ASC,WETDN
!
!-----------------------------------------------------------------------
!     * SIZE-RELATED PARAMETERS.
!
      PHI0W =PHI0
      PHIS0W=PHIS0
      WETDN=DRYDN
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ANUM(IL,L,IS) > YTINY .AND. AMAS(IL,L,IS) > YTINY &
                             .AND. ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
          FGR=WETRC(IL,L,IS)/DRYRC(IS)
          ALFGR=LOG(FGR)
          PHI0W (IL,L,IS)=PHI0W (IL,L,IS)+ALFGR
          PHIS0W(IL,L,IS)=PHIS0W(IL,L,IS)+ALFGR
          FGR3I=1./FGR**3
          WETDN(IL,L,IS)=DNH2O*(1.-FGR3I)+DRYDN(IL,L,IS)*FGR3I
        ENDIF
      ENDDO
      ENDDO
      ENDDO
      AM0=SDINT0(PHI0W,PSI,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=1.
      AM1=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=2.
      AM2=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=3.
      AM3=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=4.
      AM4=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=5.
      AM5=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
!
!-----------------------------------------------------------------------
!     * SETTLING VELOCITY.
!
      GSVDM=0.
      GSVDN=0.
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( AM0(IL,L,IS) > YTINY .AND. AM3(IL,L,IS) > YTINY ) THEN
          APA=2.*(WETDN(IL,L,IS)-RHOA(IL,L))*GRAV/(9.*AMU(IL,L))
          APB=APA*AMFP(IL,L) &
             *(1.257+0.4*EXP(-1.1*(2.*WETRC(IL,L,IS))/(2.*AMFP(IL,L))))
          GSVDM(IL,L,IS)= &
                       (APA*AM5(IL,L,IS)+APB*AM4(IL,L,IS))/AM3(IL,L,IS)
          GSVDN(IL,L,IS)= &
                       (APA*AM2(IL,L,IS)+APB*AM1(IL,L,IS))/AM0(IL,L,IS)
        ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * ENSURE CFL CRITERION.
!
      ASCM=1.
      ASCN=1.
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( GSVDM(IL,L,IS) > MAX(VCFL(IL,L)/YLARGE,YTINY) ) THEN
          ASCM(IL,L,IS)=VCFL(IL,L)/GSVDM(IL,L,IS)
        END IF
        IF ( GSVDN(IL,L,IS) > MAX(VCFL(IL,L)/YLARGE,YTINY) ) THEN
          ASCN(IL,L,IS)=VCFL(IL,L)/GSVDN(IL,L,IS)
        END IF
      ENDDO
      ENDDO
      ENDDO
      ASC=MIN(1.,ASCM,ASCN)
      GSVDM=GSVDM*ASC
      GSVDN=GSVDN*ASC
!
      END SUBROUTINE GSPAR
