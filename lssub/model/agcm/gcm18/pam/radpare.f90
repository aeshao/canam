      SUBROUTINE RADPARE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                         PEWETRC,PEDDN,PERE,PEVE,PELOAD, &
                         ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     RADIATION PARAMETERS (LOAD, EFFECTIVE RADIUS, EFFECTIVE VARIANCE).
!
!     HISTORY:
!     --------
!     * NOV 21/2012 - K. VON SALZEN    REMOVE INTERNALLY MIXED AEROSOL
!     * JAN 17/2012 - K. VON SALZEN    BASED ON SSRAD
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      REAL, INTENT(IN),  DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                      PENUM,PEMAS,PEWETRC,PEDDN, &
                                      PEN0,PEPSI,PEPHI0,PEPHIS0,PEDPHI0
      REAL, INTENT(OUT),  DIMENSION(ILGA,LEVA,KEXT) :: PERE,PEVE,PELOAD
      REAL,  DIMENSION(ILGA,LEVA,ISAEXT) :: PEWETRT
      REAL,  DIMENSION(ILGA,LEVA) :: TRE,TVE,TLOAD
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS.
!
      IF ( KEXT > 0 ) THEN
        PELOAD=0.
        PERE=0.
        PEVE=0.
      ENDIF
!
!     * EXTERNALLY MIXED SEA SALT AEROSOL.
!
      IF ( KEXTSS > 0 ) THEN
        CALL EFFRVE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                    PEWETRC,PEDDN,TRE,TVE,TLOAD,ILGA,LEVA,IEXSS, &
                    KEXTSS,ISEXTSS)
        PELOAD(:,:,KEXTSS)=TLOAD
        PERE(:,:,KEXTSS)=TRE
        PEVE(:,:,KEXTSS)=TVE
      ENDIF
!
!     * EXTERNALLY MIXED MINERAL DUST AEROSOL. ONLY CONSIDER
!     * DRY SIZE DISTRIBUTION (FOR CONSISTENCY WITH RADIATION).
!
      IF ( KEXTMD > 0 ) THEN
        DO IS=1,ISEXTMD
          ISX=IEXMD(IS)
          PEWETRT(:,:,ISX)=PEDRYRC(ISX)
        ENDDO
        CALL EFFRVE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                    PEWETRT,PEDDN,TRE,TVE,TLOAD,ILGA,LEVA,IEXMD, &
                    KEXTMD,ISEXTMD)
        PELOAD(:,:,KEXTMD)=TLOAD
        PERE(:,:,KEXTMD)=TRE
        PEVE(:,:,KEXTMD)=TVE
      ENDIF
!
!     * EXTERNALLY MIXED BLACK CARBON AEROSOL.
!
      IF ( KEXTBC > 0 ) THEN
        DO IS=1,ISEXTBC
          ISX=IEXBC(IS)
          PEWETRT(:,:,ISX)=PEDRYRC(ISX)
        ENDDO
        CALL EFFRVE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                    PEWETRT,PEDDN,TRE,TVE,TLOAD,ILGA,LEVA,IEXBC, &
                    KEXTBC,ISEXTBC)
        PELOAD(:,:,KEXTBC)=TLOAD
        PERE(:,:,KEXTBC)=TRE
        PEVE(:,:,KEXTBC)=TVE
      ENDIF
!
!     * EXTERNALLY MIXED ORGANIC CARBON AEROSOL.
!
      IF ( KEXTOC > 0 ) THEN
        DO IS=1,ISEXTOC
          ISX=IEXOC(IS)
          PEWETRT(:,:,ISX)=PEDRYRC(ISX)
        ENDDO
        CALL EFFRVE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                    PEWETRT,PEDDN,TRE,TVE,TLOAD,ILGA,LEVA,IEXOC, &
                    KEXTOC,ISEXTOC)
        PELOAD(:,:,KEXTOC)=TLOAD
        PERE(:,:,KEXTOC)=TRE
        PEVE(:,:,KEXTOC)=TVE
      ENDIF
!
      END SUBROUTINE RADPARE
