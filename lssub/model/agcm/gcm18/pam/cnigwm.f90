      SUBROUTINE CNIGWM(FRES,SVN,SV,QRC,QRVN,RSN,TEMPN,CERC,CESC, &
                        CEWMASS,CEWETRN,CEWETRB,CIWMASS,CIWETRN, &
                        CIWETRB,CIRC,CISC,CEBK,CETSCL,CERAT,CEIRD, &
                        CEN0,CEPSI,CEMOM2,CEMOM3,CEMOM4,CEMOM5,CEMOM6, &
                        CEMOM7,CEMOM8,CIBK,CITSCL,CIRAT,CIIRD,CIN0, &
                        CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                        CIMOM7,CIMOM8,HLST,DHDT,QR,DRDT,ZH,PRES,AK, &
                        SVI,DT,ILGA,LEVA,CEMOD1,CEMOD2,CEMOD3,CIMOD1, &
                        CIMOD2,CIMOD3)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     RESIDUAL FOR CALCULATION OF DROPLET GROWTH FROM CHANGES
!     IN ENVIRONMENTAL CONDITIONS AND AEROSOL CONCENTRATIONS. RESULTS
!     OF THIS SUBROUTINE ARE USED TO DETERMINE THE SUPERSATURATION.
!     FOR USE WITH SCALED VERSION OF THE GROWTH EQUATION.
!
!     HISTORY:
!     --------
!     * FEB 04/2015 - K.VONSALZEN   COSMETIC CHANGE FOR GCM18:
!     *                             {AK,CEBK,CIBK} DECLARED
!     *                             WITH INTENT(INOUT) INSTEAD OF INTENT(IN).
!     * JUN 20/2008 - K.VONSALZEN   ACCOUNT FOR NUMERICAL TRUNCATION
!                                   IN DROPLET GROWTH, ADD KCOR.
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SCPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILGA,LEVA
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISEXTB) :: CEBK
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISINTB) :: CIBK
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISEXTB) :: CEWETRB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISINTB) :: CIWETRB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: AK
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: DRDT,DHDT,SV,QR, &
                                                HLST,ZH,PRES,DT,SVI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXT) :: CEN0,CEPSI
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEXT) :: &
                                          CEMOM2,CEMOM3,CEMOM4,CEMOM5, &
                                          CEMOM6,CEMOM7,CEMOM8
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINT) :: CIN0,CIPSI
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISINT) :: &
                                          CIMOM2,CIMOM3,CIMOM4,CIMOM5, &
                                          CIMOM6,CIMOM7,CIMOM8
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                          CETSCL,CERAT
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISINTB) :: CEIRD
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                          CITSCL,CIRAT
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXTB) :: CIIRD
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                          CEMOD1,CEMOD2,CEMOD3
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                          CIMOD1,CIMOD2,CIMOD3
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                          CERC,CESC,CEWETRN
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                          CIRC,CISC,CIWETRN
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXT) :: CEWMASS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINT) :: CIWMASS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: QRC,FRES,RSN,TEMPN, &
                                                 QRVN,SVN
      REAL, PARAMETER :: YSSEC=0.999
      REAL, PARAMETER :: YGRWL=1000.
      LOGICAL, PARAMETER :: KCOR=.TRUE.
!      LOGICAL, PARAMETER :: KCOR=.FALSE.
!
!-----------------------------------------------------------------------
!     * INITIALIZATION OF DIAGNOSTIC RESULTS.
!
      IF ( ISEXTB > 0 ) THEN
        CEMOD1=0
        CEMOD2=0
        CEMOD3=1
      ENDIF
      IF ( ISINTB > 0 ) THEN
        CIMOD1=0
        CIMOD2=0
        CIMOD3=1
      ENDIF
!
!     * INTEGRATE GROWTH EQUATION.
!
      IF ( ISEXTB > 0 ) THEN
        CALL CNIGGE(CEWETRN,CERC,CESC,CEWETRB,CEBK,CETSCL,SV,AK, &
                    SVI,CERAT,CEIRD,CEDRYRB,DT,ILGA,LEVA,ISEXTB, &
                    CEMOD1,CEMOD2,CEMOD3)
        CEWETRB=CEWETRN
      ENDIF
      IF ( ISINTB > 0 ) THEN
        CALL CNIGGE(CIWETRN,CIRC,CISC,CIWETRB,CIBK,CITSCL,SV,AK, &
                    SVI,CIRAT,CIIRD,CIDRYRB,DT,ILGA,LEVA,ISINTB, &
                    CIMOD1,CIMOD2,CIMOD3)
        CIWETRB=CIWETRN
      ENDIF
!
      IF ( KCOR ) THEN
!
!       * LIMIT DROPLET GROWTH TO PHYSICALLY REASONABLE RANGE.
!
        DO IS=1,ISEXTB
          CEWETRB(:,:,IS)=MIN(CEWETRB(:,:,IS),YGRWL*CEDRYRB(IS))
        ENDDO
        DO IS=1,ISINTB
          CIWETRB(:,:,IS)=MIN(CIWETRB(:,:,IS),YGRWL*CIDRYRB(IS))
        ENDDO
!
!       * DO NOT ALLOW SMALLER PARTICLES TO OVERTAKE LARGER PARTICLES.
!       * PREVENT THIS BY LIMITING THE GROWTH FOR THE SMALLER PARTICLES,
!       * IF NECESSARY. THIS MAY OCCUR OWING TO NUMERICAL TRUNCATION
!       * ERRORS FOR THE DROPLET GROWTH CALCULATIONS.
!
        IF ( ISEXTB > 0 ) THEN
          ISC=ISEXTB+1
          DO IS=ISWEXT,2,-1
            KX=SEXTF%ISWET(IS)%ITYP
            KXM=SEXTF%ISWET(IS-1)%ITYP
            DO IST=1,ISESC
              ISC=ISC-1
              WHERE ( CEWETRB(:,:,ISC-1) >= CEWETRB(:,:,ISC) )
                CEWETRB(:,:,ISC-1)=YSSEC*CEWETRB(:,:,ISC)
              ENDWHERE
            ENDDO
            IF ( KXM /= KX ) THEN
              ISC=ISC-1
            ENDIF
          ENDDO
          DO IST=1,ISESC
            ISC=ISC-1
            WHERE ( CEWETRB(:,:,ISC-1) >= CEWETRB(:,:,ISC) )
              CEWETRB(:,:,ISC-1)=YSSEC*CEWETRB(:,:,ISC)
            ENDWHERE
          ENDDO
        ENDIF
        IF ( ISINTB > 0 ) THEN
          DO IS=ISINTB,2,-1
            WHERE ( CIWETRB(:,:,IS-1) >= CIWETRB(:,:,IS) )
              CIWETRB(:,:,IS-1)=YSSEC*CIWETRB(:,:,IS)
            ENDWHERE
          ENDDO
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * CALCULATE AMOUNT OF CONDENSED WATER.
!
      CALL SCWATM(QRC,CEWMASS,CIWMASS,CEN0,CEPSI,CEWETRB,CEMOM2, &
                  CEMOM3,CEMOM4,CEMOM5,CEMOM6,CEMOM7,CEMOM8, &
                  CIN0,CIPSI,CIWETRB,CIMOM2,CIMOM3,CIMOM4,CIMOM5, &
                  CIMOM6,CIMOM7,CIMOM8,ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * IMPLIED SUPERSATURATION FOR GIVEN CLOUD WATER CONTENT.
!
      CALL CNSBND(SVN,QRVN,RSN,TEMPN,QRC,HLST,DHDT,QR, &
                  DRDT,ZH,PRES,DT,ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * RESIDUAL SUPERSATURATION.
!
      FRES=SVN-SV
!
      END SUBROUTINE CNIGWM
