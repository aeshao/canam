      SUBROUTINE EQUIP (ILG,ILEV,NEQP,ILEVI,IL1,IL2,ACHPA,ATEMP)
!-------------------------------------------------------------------------
!     * MAY 26/2010 - K. VONSALZEN     NEW SUBROUTINE CREATED FROM OEQUIP2
!
!     * CHEMICAL PARAMETERS REQUIRED FOR IN-CLOUD OXIDATION OF S(IV).
!     * THIS SUBROUTINE PROVIDES HENRY'S LAW CONSTANT AND REACTION RATES.
!-----------------------------------------------------------------------
      USE FPDEF
!
      IMPLICIT NONE
!
      REAL, DIMENSION(ILG,ILEV) :: ATEMP
      REAL(R8), DIMENSION(ILG,ILEV,NEQP) :: ACHPA(ILG,ILEV,NEQP)
!
      REAL(R8) :: TEMP
      INTEGER :: ILG,ILEV,NEQP,ILEVI,IL1,IL2,L,IL
!-----------------------------------------------------------------------
!
      DO L=ILEVI,ILEV
      DO IL=IL1,IL2
        TEMP=ATEMP(IL,L)
!
        ACHPA(IL,L,1) = TEMP*1.0093E-01_R8*                            &!PROC-DEPEND
                                EXP(3120._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,2) = TEMP*1.7158E-03_R8*                            &!PROC-DEPEND
                                EXP(5210._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,3) = TEMP*1.0295E-10_R8*                            &!PROC-DEPEND
                                EXP(6330._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,4) = TEMP*2.5520E-03_R8*                            &!PROC-DEPEND
                                EXP(2423._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,5) = TEMP*1.0973E-09_R8*                            &!PROC-DEPEND
                                EXP(1510._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,6) = TEMP*7.9595E+03_R8*                            &!PROC-DEPEND
                                EXP(6600._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,7) = TEMP*9.4366E-04_R8*                            &!PROC-DEPEND
                                EXP(2560._R8*(1._R8/TEMP-1._R8/298._R8))
        ACHPA(IL,L,8) = TEMP*206.032E+03_R8*                           &!PROC-DEPEND
                                    EXP( 29.1657_R8*(298._R8/TEMP-1._R8) &
                                       + 16.8322_R8*(1._R8+            &!PROC-DEPEND
                                        LOG(298._R8/TEMP)-298._R8/TEMP))
        ACHPA(IL,L,9) = TEMP*8.44851E+09_R8*                           &!PROC-DEPEND
                                     EXP(34.8536_R8*(298._R8/TEMP-1._R8) &
                                         -5.3930_R8*(1._R8+            &!PROC-DEPEND
                                        LOG(298._R8/TEMP)-298._R8/TEMP))
        ACHPA(IL,L,10) = TEMP*161.675E+03_R8*                          &!PROC-DEPEND
                                     EXP(30.2355_R8*(298._R8/TEMP-1._R8) &
                                       + 19.9083_R8*(1._R8+            &!PROC-DEPEND
                                        LOG(298._R8/TEMP)-298._R8/TEMP))
        ACHPA(IL,L,11) = 4.4E+11_R8*                                   &!PROC-DEPEND
                                    EXP(-4131._R8/TEMP)
        ACHPA(IL,L,12) = 2.6E+03_R8*                                   &!PROC-DEPEND
                                    EXP(-966._R8 /TEMP)
        ACHPA(IL,L,13) = 8.0E+04_R8*                                   &!PROC-DEPEND
                               EXP(-3650._R8*(1._R8/TEMP-1._R8/298._R8))
      ENDDO
      ENDDO
!
      END SUBROUTINE EQUIP
