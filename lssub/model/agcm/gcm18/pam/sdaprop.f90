      SUBROUTINE SDAPROP (PEWETRC,PIWETRC,PEN0,PEPHI0,PEPSI,PEDDN, &
                          PIN0,PIPHI0,PIPSI,PIFRC,PIDDN,TA,RHA, &
                          ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CALCULATION OF WET PARTICLE RADIUS AND HYGROSCOPIC AEROSOL
!     PROPERTIES FOR HUMIDITY GROWTH.
!
!     MAKE SURE THAT DRY AEROSOL DENSITY IS UP-TO-DATE BEFORE
!     THE CALL OF THIS SUBROUTINE.
!
!     HISTORY:
!     --------
!     * APR 17/2015 - K.VONSALZEN  CHANGES TO SUPPORT SINGLE PRECISION
!     * OCT 28/2011 - K.VONSALZEN  NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAEXT) :: PEWETRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT) :: PIWETRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: RHA,TA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEN0,PEPHI0,PEPSI, &
                                                   PEDDN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIDDN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      INTEGER, INTENT(IN) :: ILGA,LEVA
!
      REAL, PARAMETER :: YOTRD = 1./3.
      INTEGER, PARAMETER :: NPNT=20   ! NUMBER OF REFERENCE POINTS
      INTEGER, PARAMETER :: NTPT=5    ! NUMBER OF AEROSOL SPECIES
      INTEGER, PARAMETER :: ITMAX=3   ! NUMBER OF ITERATIONS
      REAL, DIMENSION(NPNT) :: RH
      DATA RH / 0.100, 0.382, 0.541, 0.643, 0.714 &
              , 0.766, 0.807, 0.838, 0.864, 0.885 &
              , 0.903, 0.919, 0.932, 0.943, 0.953 &
              , 0.962, 0.970, 0.978, 0.984, 0.990 /
      REAL, DIMENSION(NPNT,NTPT) :: PKATPI
      DATA PKATPI / 1.4974408, 0.9838770, 0.8290761, 0.7245096     &! (NH4)2SO4
                  , 0.6532416, 0.6018254, 0.5618502, 0.5322418     &! (NH4)2SO4
                  , 0.5083230, 0.4901409, 0.4760467, 0.4655360     &! (NH4)2SO4
                  , 0.4593138, 0.4566030, 0.4571863, 0.4613250     &! (NH4)2SO4
                  , 0.4691201, 0.4825648, 0.4982601, 0.5223324     &! (NH4)2SO4
                  , 11.0825653,  3.9426887,  2.8485217,  2.3564157 &! SEA SALT
                  ,  2.0665963,  1.8737581,  1.7310804,  1.6282451 &! SEA SALT
                  ,  1.5455265,  1.4813753,  1.4286581,  1.3839465 &! SEA SALT
                  ,  1.3494499,  1.3218253,  1.2982354,  1.2785497 &! SEA SALT
                  ,  1.2626650,  1.2490141,  1.2412405,  1.2378957 &! SEA SALT
                  , 1.3266588, 0.5646486, 0.5823150, 0.5709559     &! NH4NO3
                  , 0.5603971, 0.5560658, 0.5577084, 0.5635365     &! NH4NO3
                  , 0.5726734, 0.5830175, 0.5939313, 0.6050492     &! NH4NO3
                  , 0.6149372, 0.6238601, 0.6324733, 0.6408188     &! NH4NO3
                  , 0.6490519, 0.6587952, 0.6681154, 0.6812770     &! NH4NO3
                  , 0.       , 0.       , 0.       , 0.            &! NH4CL
                  , 0.       , 0.       , 1.1404024, 1.0998064     &! NH4CL
                  , 1.0654703, 1.0378653, 1.0145106, 0.9942313     &! NH4CL
                  , 0.9783312, 0.9655310, 0.9546949, 0.9459390     &! NH4CL
                  , 0.9393936, 0.9347875, 0.9335700, 0.9362975     &! NH4CL
                  ,  6.2151999, 2.4391816, 1.7912617, 1.4808251    &! H2SO4
                  , 1.2884889, 1.1546655, 1.0514783, 0.9740624     &! H2SO4
                  , 0.9092117, 0.8567778, 0.8117985, 0.7718493     &! H2SO4
                  , 0.7395788, 0.7125072, 0.6882302, 0.6668478     &! H2SO4
                  , 0.6484994, 0.6313590, 0.6202098, 0.6129278 /    ! H2SO4
!
      INTEGER, DIMENSION(ILGA,LEVA) :: ITO
      REAL, DIMENSION(ILGA,LEVA) :: RHAT,SFCTW,AK,WGT,PKAAS,PKAAN,PKAAC, &
                                    PKASS,PKASA,FGRWTH,VNOK,TERM,TERM1, &
                                    TERM2,AKELVL,AKELVH,AKELV,VFRC,VTOT, &
                                    DTOT,AVGVT,AVGGF,VOLSD,VOLSDT
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIKAP,PEKAP
      REAL, ALLOCATABLE, DIMENSION(:) :: PIKMAX,PEKMAX
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PIVFRC
      REAL(R8), DIMENSION(ILGA,LEVA) :: DBL1,DBL2,DBL3
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY.
!
      IF ( KEXT > 0 ) THEN
        ALLOCATE (PEKAP (ILGA,LEVA,ISAEXT))
        ALLOCATE (PEKMAX(ISAEXT))
      ENDIF
      IF ( KINT > 0 ) THEN
        ALLOCATE (PIKAP (ILGA,LEVA,ISAINT))
        ALLOCATE (PIKMAX(ISAINT))
        ALLOCATE (PIVFRC(ILGA,LEVA,ISAINT,KINT))
      ENDIF
!
!-----------------------------------------------------------------------
!     * RESTRICT RELATIVE HUMIDITY TO VALID RANGE.
!
      RHAT=MAX(0.1,MIN(0.99,RHA))
!
!     * SURFACE TENSION OF WATER.
!
      SFCTW=0.0761-1.55E-04*(TA-273.)
!
!     * A-TERM IN KOEHLER EQUATION (FOR CURVATURE EFFECT).
!
      AK=4.*WH2O*SFCTW/(RGASM*TA*RHOH2O)
      AK=AK*(YPI/6.)**YOTRD
!
!-----------------------------------------------------------------------
!     * HYGROSCOPICITY PARAMETERS FOR INDIVIDUAL CHEMICAL CONSTITUENTS.
!     * THE DEFAULT APPROACH IS TO USE THE KAPPA PARAMETERS FROM
!     * CCN CALCULATIONS. OTHERWISE, MORE DETAILED INFORMATION IS USED
!     * (CURRENTLY: AMMONIUM SULPHATE, SODIUM CHLORIDE, AMMONIUM NITRATE).
!
!     * PARAMETERS FOR HUMIDITY INTERPOLATION.
!
      ITO=2
      DO IT=1,NPNT-1
        DO L=1,LEVA
        DO IL=1,ILGA
          IF ( RHAT(IL,L) .GT. RH(IT) ) ITO(IL,L)=IT+1
        ENDDO
        ENDDO
      ENDDO
      DO L=1,LEVA
      DO IL=1,ILGA
        ITOX=ITO(IL,L)
        WGT(IL,L)=(RH(ITOX)-RHAT(IL,L))/(RH(ITOX)-RH(ITOX-1))
      ENDDO
      ENDDO
!
!     * INTERPOLATION OF TABULATED HYGROSCOPICITIES.
!
      IF ( KEXTSO4 > 0 .OR. KINTSO4 > 0 ) THEN
        NTX=1
        DO L=1,LEVA
        DO IL=1,ILGA
          ITOX=ITO(IL,L)
          WGTX=WGT(IL,L)
          PKAAS(IL,L)=WGTX*PKATPI(ITOX-1,NTX) &
                                        +(1.-WGTX)*PKATPI(ITOX,NTX)
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTSS > 0 .OR. KINTSS > 0 ) THEN
        NTX=2
        DO L=1,LEVA
        DO IL=1,ILGA
          ITOX=ITO(IL,L)
          WGTX=WGT(IL,L)
          PKASS(IL,L)=WGTX*PKATPI(ITOX-1,NTX) &
                                        +(1.-WGTX)*PKATPI(ITOX,NTX)
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTNO3 > 0 .OR. KINTNO3 > 0 ) THEN
        NTX=3
        DO L=1,LEVA
        DO IL=1,ILGA
          ITOX=ITO(IL,L)
          WGTX=WGT(IL,L)
          PKAAN(IL,L)=WGTX*PKATPI(ITOX-1,NTX) &
                                        +(1.-WGTX)*PKATPI(ITOX,NTX)
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTCL > 0 .OR. KINTCL > 0 ) THEN
        NTX=4
        DO L=1,LEVA
        DO IL=1,ILGA
          ITOX=ITO(IL,L)
          WGTX=WGT(IL,L)
          PKAAC(IL,L)=WGTX*PKATPI(ITOX-1,NTX) &
                                        +(1.-WGTX)*PKATPI(ITOX,NTX)
        ENDDO
        ENDDO
      ENDIF
      IF ( KEXTSAC > 0 .OR. KINTSAC > 0 ) THEN
        NTX=5
        DO L=1,LEVA
        DO IL=1,ILGA
          ITOX=ITO(IL,L)
          WGTX=WGT(IL,L)
          PKASA(IL,L)=WGTX*PKATPI(ITOX-1,NTX) &
                                        +(1.-WGTX)*PKATPI(ITOX,NTX)
        ENDDO
        ENDDO
      ENDIF
!
!     * HYGROSCOPICITY OF THE AEROSOL.
!
      IF ( KEXT > 0 ) THEN
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTOC ) THEN
            PEKAP(:,:,IS)=AEXTF%TP(KEXTOC)%KAPPA
          ENDIF
          IF ( KX == KEXTBC ) THEN
            PEKAP(:,:,IS)=AEXTF%TP(KEXTBC)%KAPPA
          ENDIF
          IF ( KX == KEXTMD ) THEN
            PEKAP(:,:,IS)=AEXTF%TP(KEXTMD)%KAPPA
          ENDIF
          IF ( KX == KEXTSO4 ) THEN
            PEKAP(:,:,IS)=PKAAS(:,:)
          ENDIF
          IF ( KX == KEXTSS ) THEN
            PEKAP(:,:,IS)=PKASS(:,:)
          ENDIF
          IF ( KX == KEXTNO3 ) THEN
            PEKAP(:,:,IS)=PKAAN(:,:)
          ENDIF
          IF ( KX == KEXTCL ) THEN
            PEKAP(:,:,IS)=PKAAC(:,:)
          ENDIF
          IF ( KX == KEXTSAC ) THEN
            PEKAP(:,:,IS)=PKASA(:,:)
          ENDIF
          PEKMAX(IS)=MAXVAL(PEKAP(:,:,IS))
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
!
!       * VOLUME FRACTIONS DERIVED FROM MASS FRACTIONS.
!
        DO IS=1,ISAINT
          DO KXT=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(KXT)
            ISI=SINTF%ISAER(IS)%ISI
            PIVFRC(:,:,IS,KX)= &
                     PIFRC(:,:,ISI,KX)*PIDDN(:,:,ISI)/AINTF%TP(KX)%DENS
          ENDDO
        ENDDO
!
!       * APPLY ZSR-RULE TO OBTAIN VOLUME-WEIGHTED MEAN HYGROSCOPICITY.
!
        PIKAP=0.
        IF ( KINTOC > 0 ) THEN
          PKAOC=AINTF%TP(KINTOC)%KAPPA
          PIKAP=PIKAP+PIVFRC(:,:,:,KINTOC)*PKAOC
        ENDIF
        IF ( KINTBC > 0 ) THEN
          PKABC=AINTF%TP(KINTBC)%KAPPA
          PIKAP=PIKAP+PIVFRC(:,:,:,KINTBC)*PKABC
        ENDIF
        IF ( KINTMD > 0 ) THEN
          PKAMD=AINTF%TP(KINTMD)%KAPPA
          PIKAP=PIKAP+PIVFRC(:,:,:,KINTMD)*PKAMD
        ENDIF
        IF ( KINTSO4 > 0 ) THEN
          DO IS=1,ISAINT
            PIKAP(:,:,IS)=PIKAP(:,:,IS) &
                                  +PIVFRC(:,:,IS,KINTSO4)*PKAAS(:,:)
          ENDDO
        ENDIF
        IF ( KINTSS > 0 ) THEN
          DO IS=1,ISAINT
            PIKAP(:,:,IS)=PIKAP(:,:,IS) &
                                  +PIVFRC(:,:,IS,KINTSS)*PKASS(:,:)
          ENDDO
        ENDIF
        IF ( KINTNO3 > 0 ) THEN
          DO IS=1,ISAINT
            PIKAP(:,:,IS)=PIKAP(:,:,IS) &
                                  +PIVFRC(:,:,IS,KINTNO3)*PKAAN(:,:)
          ENDDO
        ENDIF
        IF ( KINTCL > 0 ) THEN
          DO IS=1,ISAINT
            PIKAP(:,:,IS)=PIKAP(:,:,IS) &
                                  +PIVFRC(:,:,IS,KINTCL)*PKAAC(:,:)
          ENDDO
        ENDIF
        IF ( KINTSAC > 0 ) THEN
          DO IS=1,ISAINT
            PIKAP(:,:,IS)=PIKAP(:,:,IS) &
                                  +PIVFRC(:,:,IS,KINTSAC)*PKASA(:,:)
          ENDDO
        ENDIF
        DO IS=1,ISAINT
          PIKMAX(IS)=MAXVAL(PIKAP(:,:,IS))
        ENDDO
      ENDIF
!
!     * WET PARTICLE SIZE FOR ENSEMBLE OF TEST PARTICLES.
!
      IF ( KEXT > 0 ) THEN
        DO IS=1,ISAEXT
          IF ( PEKMAX(IS) > YTINY ) THEN
            VOLSDT=0.
            AVGVT=0.
            DO IT=1,AEXTFG%SEC(IS)%NTSTP
              DDRY=2.*AEXTFG%SEC(IS)%RADT(IT)
!
!             * VOLUME OF DRY TEST PARTICLE.
!
              VDRY=(YPI/6.)*DDRY**3
!
!             * VOLUME OF PARTICLE IN ABSENCE OF KELVIN EFFECT.
!
              FGRWTH=1.+PEKAP(:,:,IS)*RHAT/(1.-RHAT)
              VNOK=VDRY*FGRWTH
!
!             * BRACKET SOLUTION FOR KELVIN TERM.
!
              TERM=AK*VDRY**(-YOTRD)
              TERM1=EXP(TERM)
              AKELVH=TERM1
              TERM2=-YOTRD
              TERM=VNOK
              TERM1=TERM**TERM2
              TERM=AK*TERM1
              TERM1=EXP(TERM)
              AKELVL=TERM1
!
!             * INITIAL GUESS FOR KELVIN TERM AND VOLUME FRACTION
!             * OF WATER (RELATIVE TO DRY PARTICLE).
!
              AKELV=.5*(AKELVH+AKELVL)
              VFRC=PEKAP(:,:,IS)*(RHAT/AKELV)/(1.-RHAT/AKELV)
!
!             * ITERATIVE CALCULATION OF VOLUME FRACTION OF WATER
!             * AND TOTAL PARTICLE VOLUME FOR EACH TEST PARTICLE.
!
              DO ITX=1,ITMAX
                FGRWTH=1.+VFRC
                VTOT=VDRY*FGRWTH
                TERM2=-YOTRD
                TERM=VTOT
                TERM1=TERM**TERM2
                TERM=AK*TERM1
                TERM1=EXP(TERM)
                AKELV=TERM1
                VFRC=PEKAP(:,:,IS)*(RHAT/AKELV)/(1.-RHAT/AKELV)
              ENDDO
              FGRWTH=1.+VFRC
              VTOT=VDRY*FGRWTH
!
!             * VOLUME SIZE DISTRIBUTION AND TOTAL VOLUME
!             * FOR TEST PARTICLES.
!
              APHI=LOG(.5*DDRY/R0)
              TERM=0.
              WHERE ( ABS(PEPSI(:,:,IS)-YNA) > YTINY )
                TERM=-PEPSI(:,:,IS)*(APHI-PEPHI0(:,:,IS))**2
              ENDWHERE
              TERM1=EXP(TERM)
              WHERE ( ABS(PEPSI(:,:,IS)-YNA) > YTINY )
                VOLSD=VDRY*PEN0(:,:,IS)*TERM1
                VOLSDT=VOLSDT+VOLSD
                AVGVT=AVGVT+VFRC*VOLSD
              ENDWHERE
            ENDDO
!
!           * VOLUME-WEIGHTED VOLUME FRACTION OF WATER FOR EACH SECTION.
!
            WHERE ( VOLSDT > MAX(AVGVT/YLARGE,YTINY) )
              AVGVT=AVGVT/VOLSDT
            ENDWHERE
!
!           * AVERAGE AEROSOL RADIUS GROWTH FACTOR.
!
            FGRWTH=1.+AVGVT
            TERM2=YOTRD
            TERM=FGRWTH
            DBL1=TERM2
            DBL2=TERM
            DBL3=DBL2**DBL1
            AVGGF=DBL3
            PEWETRC(:,:,IS)=PEDRYRC(IS)*AVGGF
          ELSE
            PEWETRC(:,:,IS)=PEDRYRC(IS)
          ENDIF
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        DO IS=1,ISAINT
          IF ( PIKMAX(IS) > YTINY ) THEN
            VOLSDT=0.
            AVGVT=0.
            DO IT=1,AINTFG%SEC(IS)%NTSTP
              DDRY=2.*AINTFG%SEC(IS)%RADT(IT)
!
!             * VOLUME OF DRY TEST PARTICLE.
!
              VDRY=(YPI/6.)*DDRY**3
!
!             * VOLUME OF PARTICLE IN ABSENCE OF KELVIN EFFECT.
!
              FGRWTH=1.+PIKAP(:,:,IS)*RHAT/(1.-RHAT)
              VNOK=VDRY*FGRWTH
!
!             * BRACKET SOLUTION FOR KELVIN TERM.
!
              TERM=AK*VDRY**(-YOTRD)
              TERM1=EXP(TERM)
              AKELVH=TERM1
              TERM2=-YOTRD
              TERM=VNOK
              TERM1=TERM**TERM2
              TERM=AK*TERM1
              TERM1=EXP(TERM)
              AKELVL=TERM1
!
!             * INITIAL GUESS FOR KELVIN TERM AND VOLUME FRACTION
!             * OF WATER (RELATIVE TO DRY PARTICLE).
!
              AKELV=.5*(AKELVH+AKELVL)
              VFRC=PIKAP(:,:,IS)*(RHAT/AKELV)/(1.-RHAT/AKELV)
!
!             * ITERATIVE CALCULATION OF VOLUME FRACTION OF WATER
!             * AND TOTAL PARTICLE VOLUME FOR EACH TEST PARTICLE.
!
              DO ITX=1,ITMAX
                FGRWTH=1.+VFRC
                VTOT=VDRY*FGRWTH
                TERM2=-YOTRD
                TERM=VTOT
                TERM1=TERM**TERM2
                TERM=AK*TERM1
                TERM1=EXP(TERM)
                AKELV=TERM1
                VFRC=PIKAP(:,:,IS)*(RHAT/AKELV)/(1.-RHAT/AKELV)
              ENDDO
              FGRWTH=1.+VFRC
              VTOT=VDRY*FGRWTH
!
!             * VOLUME SIZE DISTRIBUTION AND TOTAL VOLUME
!             * FOR TEST PARTICLES.
!
              APHI=LOG(.5*DDRY/R0)
              TERM=0.
              WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY &
                                        .AND. PIPSI(:,:,IS) < YLARGES )
                TERM=-PIPSI(:,:,IS)*(APHI-PIPHI0(:,:,IS))**2
              ENDWHERE
              TERM1=EXP(TERM)
              WHERE ( ABS(PIPSI(:,:,IS)-YNA) > YTINY )
                VOLSD=VDRY*PIN0(:,:,IS)*TERM1
                VOLSDT=VOLSDT+VOLSD
                AVGVT=AVGVT+VFRC*VOLSD
              ENDWHERE
            ENDDO
!
!           * VOLUME-WEIGHTED VOLUME FRACTION OF WATER FOR EACH SECTION.
!
            WHERE ( VOLSDT > MAX(AVGVT/YLARGE,YTINY) )
              AVGVT=AVGVT/VOLSDT
            ENDWHERE
!
!           * AVERAGE AEROSOL RADIUS GROWTH FACTOR.
!
            FGRWTH=1.+AVGVT
            TERM2=YOTRD
            TERM=FGRWTH
            DBL1=TERM2
            DBL2=TERM
            DBL3=DBL2**DBL1
            AVGGF=DBL3
            PIWETRC(:,:,IS)=PIDRYRC(IS)*AVGGF
          ELSE
            PIWETRC(:,:,IS)=PIDRYRC(IS)
          ENDIF
        ENDDO
      ENDIF
!
!     * DEALLOCATE MEMORY.
!
      IF ( KEXT > 0 ) THEN
        DEALLOCATE (PEKAP )
        DEALLOCATE (PEKMAX)
      ENDIF
      IF ( KINT > 0 ) THEN
        DEALLOCATE (PIKAP )
        DEALLOCATE (PIKMAX)
        DEALLOCATE (PIVFRC)
      ENDIF
!
      END SUBROUTINE SDAPROP
