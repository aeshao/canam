      SUBROUTINE SWETDEP(DPXDT,PDCLR,PDCLD,DWR1,DWR2,DWR3, &
                         DWS1,DWS2,DWS3,PXCLR,PXCLD,PHENRY, &
                         PCEF,RHOA,DPA,PMREP,ZMLWC,PFSNOW,PFRAIN, &
                         ZCLF,CLRFR,CLRFS,PFEVAP,PFSUBL,ZCTHR,DT, &
                         ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     WET DEPOSITION OF TRACE GASES OR AEROSOLS
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN  NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPHYS
!
      IMPLICIT REAL   (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILGA,LEVA
      REAL, INTENT(IN) :: DT
      REAL, INTENT(IN), DIMENSION (ILGA,LEVA,ISEC) :: &
                                     PXCLR,PXCLD,PHENRY,PCEF
      REAL, INTENT(IN), DIMENSION (ILGA,LEVA) :: &
                                     ZMLWC,RHOA,DPA,PMREP,PFSNOW, &
                                     PFEVAP,ZCLF,CLRFR,PFRAIN,CLRFS, &
                                     PFSUBL
      REAL, INTENT(OUT), DIMENSION (ILGA,LEVA,ISEC) :: &
                                     DPXDT,PDCLR,PDCLD
      REAL, INTENT(OUT), DIMENSION (ILGA,LEVA,ISEC) :: &
                                     DWR1,DWR2,DWR3,DWS1,DWS2,DWS3
      REAL, DIMENSION(ILGA,ISEC) :: ZDEPR,ZDEPS
      REAL, DIMENSION(ILGA,LEVA,ISEC) :: PDEPR,PDEPS
!
      REAL, PARAMETER :: ZERO=0.
      REAL, PARAMETER :: ONE =1.
      REAL, PARAMETER :: ZMIN=1.E-20
      REAL, PARAMETER :: ZCLRFM=0.05
      REAL, PARAMETER :: ZEP=1.
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      PDEPS=0.
      PDEPR=0.
      ZDEPR=0.
      ZDEPS=0.
      DPXDT=0.
      PDCLD=0.
      PDCLR=0.
      DWR1=0.
      DWR2=0.
      DWR3=0.
      DWS1=0.
      DWS2=0.
      DWS3=0.
!
!-----------------------------------------------------------------------
!
      DO IS=1,ISEC
      DO L=1,LEVA
!
!     * IN-CLOUD SCAVENGING FOR LIQUID WATER CLOUDS.
!
        DO IL=1,ILGA
          IF ( PMREP(IL,L) > ZMIN ) THEN
            IF (ZMLWC(IL,L) > ZMIN) THEN
              ZICSCAV=PMREP(IL,L)/ZMLWC(IL,L)
            ELSE
              ZICSCAV=ZERO
            END IF
            ZICSCAV=MAX(ZERO,MIN(ONE,ZICSCAV))
            ZEPS=ZEP*PHENRY(IL,L,IS)
            ZICSCAV=ZEPS*ZICSCAV
            DWR1(IL,L,IS)=PXCLD(IL,L,IS)*ZICSCAV*ZCLF(IL,L)
            PDEPR(IL,L,IS)=PDEPR(IL,L,IS)+DWR1(IL,L,IS)
            PDCLD(IL,L,IS)=PDCLD(IL,L,IS)+PXCLD(IL,L,IS)*ZICSCAV
          END IF
        ENDDO
!
!       * BELOW CLOUD SCAVENGING FOR ICE AND WATER CLOUDS (SCOTT, 1978).
!
        DO IL=1,ILGA
          IF ( PFSNOW(IL,L) > ZMIN .AND. CLRFS(IL,L) >= ZCLRFM ) THEN
            ZFTOM=DT*GRAV/DPA(IL,L)
            ZBCSCAV=3.7*PCEF(IL,L,IS)*(PFSNOW(IL,L)/CLRFS(IL,L))*ZFTOM &
                   *RHOA(IL,L)
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            DWS2(IL,L,IS)=ZBCSCAV*PXCLR(IL,L,IS)*CLRFS(IL,L)
            PDEPS(IL,L,IS)=PDEPS(IL,L,IS)+DWS2(IL,L,IS)
            IF ( ZCLF(IL,L) < ZCTHR ) THEN
              PDCLR(IL,L,IS)=PDCLR(IL,L,IS)+ZBCSCAV*PXCLR(IL,L,IS) &
                            *CLRFS(IL,L)/(1.-ZCLF(IL,L))
            ENDIF
          END IF
        ENDDO
        DO IL=1,ILGA
          IF ( PFRAIN(IL,L) > ZMIN .AND. CLRFR(IL,L) >= ZCLRFM ) THEN
            ZFTOM=DT*GRAV/DPA(IL,L)
            ZBCSCAV=5.2*PCEF(IL,L,IS)*(PFRAIN(IL,L)/CLRFR(IL,L))*ZFTOM &
                   *RHOA(IL,L)
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            DWR2(IL,L,IS)=ZBCSCAV*PXCLR(IL,L,IS)*CLRFR(IL,L)
            PDEPR(IL,L,IS)=PDEPR(IL,L,IS)+DWR2(IL,L,IS)
            IF ( ZCLF(IL,L) < ZCTHR ) THEN
              PDCLR(IL,L,IS)=PDCLR(IL,L,IS)+ZBCSCAV*PXCLR(IL,L,IS) &
                            *CLRFR(IL,L)/(1.-ZCLF(IL,L))
            ENDIF
          END IF
        ENDDO
        DO IL=1,ILGA
          ZMTOF=DPA(IL,L)/(DT*GRAV)
          ZDEPR(IL,IS)=ZDEPR(IL,IS)+PDEPR(IL,L,IS)*ZMTOF
          ZDEPS(IL,IS)=ZDEPS(IL,IS)+PDEPS(IL,L,IS)*ZMTOF
        ENDDO
!
!       * REEVAPORATION. ONLY COMPLETE EVAPORATION OF RRECIP IN THE
!       * CLEAR-SKY PORTION OF THE GRID CELL THAT IS AFFECTED BY RAIN)
!       * WILL CAUSE TRACERS TO BE RELEASED INTO THE ENVIRONMENT.
!
        DO IL=1,ILGA
          ZFTOM=DT*GRAV/DPA(IL,L)
          DWS3(IL,L,IS)=-ZDEPS(IL,IS)*ZFTOM*PFSUBL(IL,L)
          PDEPS(IL,L,IS)=PDEPS(IL,L,IS)+DWS3(IL,L,IS)
          ZDEPS(IL,IS)=ZDEPS(IL,IS)*(1.-PFSUBL(IL,L))
          IF ( ZCLF(IL,L) < ZCTHR ) THEN
            PDCLR(IL,L,IS)=PDCLR(IL,L,IS) &
                       -ZDEPS(IL,IS)*ZFTOM*PFSUBL(IL,L)/(1.-ZCLF(IL,L))
          ENDIF
        ENDDO
        DO IL=1,ILGA
          ZFTOM=DT*GRAV/DPA(IL,L)
          DWR3(IL,L,IS)=-ZDEPR(IL,IS)*ZFTOM*PFEVAP(IL,L)
          PDEPR(IL,L,IS)=PDEPR(IL,L,IS)+DWR3(IL,L,IS)
          ZDEPR(IL,IS)=ZDEPR(IL,IS)*(1.-PFEVAP(IL,L))
          IF ( ZCLF(IL,L) < ZCTHR ) THEN
            PDCLR(IL,L,IS)=PDCLR(IL,L,IS) &
                       -ZDEPR(IL,IS)*ZFTOM*PFEVAP(IL,L)/(1.-ZCLF(IL,L))
          ENDIF
        ENDDO
!
!       * DIAGNOSE TENDENCIES AND CONCENTRATIONS.
!
        DO IL=1,ILGA
          DPXDT(IL,L,IS)=-(PDEPR(IL,L,IS)+PDEPS(IL,L,IS))/DT
          DWR1(IL,L,IS)=-DWR1(IL,L,IS)/DT
          DWR2(IL,L,IS)=-DWR2(IL,L,IS)/DT
          DWR3(IL,L,IS)=-DWR3(IL,L,IS)/DT
          DWS1(IL,L,IS)=-DWS1(IL,L,IS)/DT
          DWS2(IL,L,IS)=-DWS2(IL,L,IS)/DT
          DWS3(IL,L,IS)=-DWS3(IL,L,IS)/DT
!
          PDCLR(IL,L,IS)=MIN(PDCLR(IL,L,IS),PXCLR(IL,L,IS))
          PDCLD(IL,L,IS)=MIN(PDCLD(IL,L,IS),PXCLD(IL,L,IS))
        ENDDO
      ENDDO
      ENDDO
!
      END SUBROUTINE SWETDEP
