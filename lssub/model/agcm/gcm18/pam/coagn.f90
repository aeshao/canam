      SUBROUTINE COAGN(DNDTS,KERN,FMOM0,FMOM3,FMOM0I,FMOM0D,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     VARIOUS TERMS IN COAGULATION EQUATION FOR AEROSOL NUMBER.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFINT,ISAINT) :: DNDTS
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISFINT) :: FMOM0,FMOM3
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISFTRIM) :: &
                                                        FMOM0I,FMOM0D
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISFTRI) ::  KERN
      REAL(R8), DIMENSION(ILGA,LEVA) ::                 DNDTT
!
!-----------------------------------------------------------------------
!
      DNDTS=0.
!
!-----------------------------------------------------------------------
!     * FIRST, THIRD AND FIFTH TERM IN NUMBER TENDENCY EQUATION.
!
      DO IS=2,ISFINT
        IR1=IS-1
        IR2=IR1
        ITS=ITR(IR1,IR2)
        DNDTT=.5*KERN(:,:,ITS)*FMOM0(:,:,IR1)**2
        IRX=IRO(IR1,IR2)
        DNDTS(:,:,IS-1,IRX)=DNDTS(:,:,IS-1,IRX)-2.*DNDTT
        DNDTS(:,:,IS  ,IRX)=DNDTS(:,:,IS  ,IRX)+DNDTT
      ENDDO
      IS=ISFINT+1
      IR1=IS-1
      IR2=IR1
      ITS=ITR(IR1,IR2)
      DNDTT=.5*KERN(:,:,ITS)*FMOM0(:,:,IR1)**2
      IRX=IRO(IR1,IR2)
      DNDTS(:,:,IS-1,IRX)=DNDTS(:,:,IS-1,IRX)-2.*DNDTT
!
!     * SECOND AND FOURTH TERM IN NUMBER TENDENCY EQUATION.
!
      DO IS=3,ISFINT
        IR1=IS-1
        DO IK=1,IS-2
          IR2=IK
          ITS=ITR(IR1,IR2)
          ITSM=ITRM(IR1,IR2)
          DNDTT=FMOM0(:,:,IR2)*KERN(:,:,ITS)*FMOM0I(:,:,ITSM) &
               +FMOM3(:,:,IR2)*KERN(:,:,ITS)*FMOM0D(:,:,ITSM)
          IRX=IRO(IR1,IR2)
          DNDTS(:,:,IS-1,IRX)=DNDTS(:,:,IS-1,IRX)-DNDTT
          DNDTS(:,:,IS  ,IRX)=DNDTS(:,:,IS  ,IRX)+DNDTT
        ENDDO
      ENDDO
      IS=ISFINT+1
      IR1=IS-1
      DO IK=1,IS-2
        IR2=IK
        ITS=ITR(IR1,IR2)
        ITSM=ITRM(IR1,IR2)
        DNDTT=FMOM0(:,:,IR2)*KERN(:,:,ITS)*FMOM0I(:,:,ITSM) &
             +FMOM3(:,:,IR2)*KERN(:,:,ITS)*FMOM0D(:,:,ITSM)
        IRX=IRO(IR1,IR2)
        DNDTS(:,:,IS-1,IRX)=DNDTS(:,:,IS-1,IRX)-DNDTT
      ENDDO
!
!     * SIXTH TERM IN NUMBER TENDENCY EQUATION.
!
      DO IS=1,ISFINT
        IR1=IS
        DO IK=1,IS-1
          IR2=IK
          ITS=ITR(IR1,IR2)
          DNDTT=-KERN(:,:,ITS)*FMOM0(:,:,IR1)*FMOM0(:,:,IR2)
          IRX=IRO(IR1,IR2)
          DNDTS(:,:,IK,IRX)=DNDTS(:,:,IK,IRX)+DNDTT
        ENDDO
      ENDDO
!
      END SUBROUTINE COAGN
