      SUBROUTINE truncfilt(G,WRK,LM,LA,LATOTAL,LMTOTAL,
     1                     LSRTOTAL,ntrunc)
C
C     sunrountine returns G filter
c     1 < ntrunc
c     0 > ntrunc
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C     * LEVEL 2,G
      COMPLEX G(LA)
      COMPLEX WRK(LATOTAL)
      INTEGER LSRTOTAL(2,LMTOTAL+1)
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE
C
C     * ICOM IS A SHARED I/O AREA. IT MUST BE LARGE ENOUGH
C     * FOR AN 8 WORD LABEL FOLLOWED BY A PACKED GAUSSIAN GRID.
C
      COMMON /ICOM/ IBUF(8),IDAT(1)
      COMMON /MACHTYP/ MACHINE, INTSIZE

C-----------------------------------------------------------------------
      DO 100 M=1,LMTOTAL
      KL=LSRTOTAL(1,M)
      KR=LSRTOTAL(1,M+1)-1
      DO 100 K=KL,KR
      NS=(M-1)+(K-KL)
C
C     * EH=11.   (T35=06h -> T47=24h  -> T63=162days)
C     * EH=15.96 (T21=24h -> T35=2.4d -> T47=17d -> T63=1000d)
C
      EH=15.96
      IF(NS.gt.ntrunc) then
C use this line for smooth reduction of filter strength
C         GR=EXP(-(FLOAT(NS-ntrunc))**2/(EH**2))
C use this line for step-wise filter strength
         GR=0.
      ELSE
         GR=1.0
      ENDIF
cccc      GR=FLOAT(NS*(NS+1))
      GI=0.E0
cccc      write(6,*) "ns, GR ",ns, GR
  100 WRK(K)=CMPLX(GR,GI)


      LMH=LMTOTAL/2
      MC=0
      LAC=0
C
C     * MPI HOOK.
C
      MSTART=MYNODE*LM+1
      MEND  =MYNODE*LM+LM
C
C     * CONSTRUCT "RECTANGLE" FROM PAIRS OF LOW/HIGH ZONAL WAVENUMBERS.
C
      DO M=1,LMH
C
        MVAL=M
        MC=MC+1

        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          NL=LSRTOTAL(1,MVAL)
          NR=LSRTOTAL(1,MVAL+1)-1
          DO N=NL,NR
            LAC=LAC+1
            G(LAC)=WRK(N)
          ENDDO
        ENDIF
C
        MVAL=LMTOTAL-M+1
        MC=MC+1

        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          NL=LSRTOTAL(1,MVAL)
          NR=LSRTOTAL(1,MVAL+1)-1
          DO N=NL,NR
            LAC=LAC+1
            G(LAC)=WRK(N)
          ENDDO
        ENDIF
C
      ENDDO


      RETURN
      END
