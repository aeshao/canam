      subroutine tstamp(kount,delt,iyr,imn,idy,ihr,iymdh)

      integer iymdh,kount,iyearst
      integer iyr,imn,idy,ihr

      integer kyears,kmonths,kdays,khours
      integer month(365),mthbnd(12)

      data month/31*1,28*2,31*3,30*4,31*5,30*6,31*7,31*8,30*9,
     1     31*10,30*11,31*12/
      data mthbnd/0,31,59,90,120,151,181,212,243,273,304,334/

      integer daysec,yearsec,idelt
      data daysec/86400/

      idelt=nint(delt)
      itspy=365*daysec/idelt
      itspd=daysec/idelt
      itsph=3600/idelt

      kyears=kount/itspy
      kdays = (kount-kyears*itspy)/itspd
      khours= (kount-kyears*itspy-kdays*itspd)/itsph

      iyr=kyears+1
      imn=month(kdays+1)
      idy=kdays-mthbnd(imn)+1
      ihr=khours

c     * in 32-bit this works only until year 2147
      iymdh=iyr*1000000+imn*10000+idy*100+ihr


      return
      end
