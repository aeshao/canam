      SUBROUTINE CLASSA(FC,     FG,     FCS,    FGS,    ALVSCN, ALIRCN,
     1                  ALVSG,  ALIRG,  ALVSCS, ALIRCS, ALVSSN, ALIRSN,           
     2                  ALVSGC, ALIRGC, ALVSSC, ALIRSC, TRVSCN, TRIRCN, 
     3                  TRVSCS, TRIRCS, FSVF,   FSVFS,  
     4                  RAICAN, RAICNS, SNOCAN, SNOCNS, FRAINC, FSNOWC, 
     5                  FRAICS, FSNOCS, DISP,   DISPS,  ZOMLNC, ZOMLCS, 
     6                  ZOELNC, ZOELCS, ZOMLNG, ZOMLNS, ZOELNG, ZOELNS, 
     7                  CHCAP,  CHCAPS, CMASSC, CMASCS, CWLCAP, CWFCAP,
     8                  CWLCPS, CWFCPS, RC,     RCS,    RBCOEF, FROOT,
     9                  FROOTS, ZPLIMC, ZPLIMG, ZPLMCS, ZPLMGS, ZSNOW,
     A                  WSNOW,  ALVS,   ALIR,   HTCC,   HTCS,   HTC,    
     B                  ALTG,   ALSNO,  TRSNOWC,TRSNOWG,                  
     C                  WTRC,   WTRS,   WTRG,   CMAI,   FSNOW,
     D                  FCANMX, ZOLN,   ALVSC,  ALIRC,  PAIMAX, PAIMIN, 
     E                  CWGTMX, ZRTMAX, RSMIN,  QA50,   VPDA,   VPDB,
     F                  PSIGA,  PSIGB,  PAIDAT, HGTDAT, ACVDAT, ACIDAT, 
     G                  ASVDAT, ASIDAT, AGVDAT, AGIDAT,
     H                  ALGWV,  ALGWN,  ALGDV,  ALGDN,
     I                  THLIQ,  THICE,  TBAR,   RCAN,   SNCAN,  TCAN,   
     J                  GROWTH, SNO,    TSNOW,  RHOSNO, ALBSNO, ZBLEND,
     K                  Z0ORO,  SNOLIM, ZPLMG0, ZPLMS0, 
     L                  FCLOUD, TA,     VPD,    RHOAIR, COSZS,  
     M                  FSDB, FSFB, REFSNO, BCSNO,                        
     N                  QSWINV, RADJ,   DLON,   RHOSNI, DELZ,   DELZW,  
     O                  ZBOTW,  THPOR,  THLMIN, PSISAT, BI,     PSIWLT, 
     P                  HCPS,   ISAND,  
     Q                  FCANCMX,ICTEM,  ICTEMMOD, RMATC, 
     R                  AILC,   PAIC,   L2MAX,  NOL2PFTS, SLAIC,
     S                  AILCG,  AILCGS, FCANC,  FCANCS,
     T                  IDAY,   ILG,    IL1,    IL2, NBS,                 
     U                  JL,N,   IC,     ICP1,   IG,     IDISP,  IZREF,
     V                  IWF,    IPAI,   IHGT,   IALC,   IALS,   IALG,
     W                  ISNOALB)                                          
C
C     * FEB 09/15 - D.VERSEGHY. New version for gcm18 and class 3.6:
C     *                         - {ALGWV,ALGWN,ALGDV,ALGDN} are passed
C     *                           in (originating in CLASSB) and then
C     *                           passed into GRALB, instead of 
C     *                           {ALGWET,ALGDRY}.
C     * NOV 16/13 - J.COLE.     FINAL VERSION FOR GCM17:
C     *                         - PASS "RHOSNO"IN TO SNOALBA TO
C     *                           CALCULATE THE PROPER BC MIXING RATIO
C     *                           IN SNOW.
C     * NOV 14/11 - M.LAZARE.   IMPLEMENT CTEM SUPPORT, PRIMARILY
C     *                         INVOLVING ADDITIONAL FIELDS TO PASS
C     *                         IN/OUT OF NEW APREP ROUTINE. THIS 
C     *                         INCLUDES NEW INPUT ARRAY "PAIC".
C     * NOV 30/06 - D.VERSEGHY. CONVERT RADJ TO REGULAR PRECISION.
C     * APR 13/06 - D.VERSEGHY. SEPARATE GROUND AND SNOW ALBEDOS FOR 
C     *                         OPEN AND CANOPY-COVERED AREAS; KEEP
C     *                         FSNOW AS OUTPUT ARRAY.
C     * APR 06/06 - D.VERSEGHY. INTRODUCE MODELLING OF WSNOW.
C     * MAR 14/05 - D.VERSEGHY. RENAME SCAN TO SNCAN (RESERVED NAME
C     *                         IN F90); CHANGE SNOLIM FROM CONSTANT
C     *                         TO VARIABLE.
C     * NOV 03/04 - D.VERSEGHY. ADD "IMPLICIT NONE" COMMAND.
C     * DEC 05/02 - D.VERSEGHY. NEW PARAMETERS FOR APREP.
C     * JUL 31/02 - D.VERSEGHY. MODIFICATIONS ASSOCIATED WITH NEW
C     *                         CALCULATION OF STOMATAL RESISTANCE.
C     *                         SHORTENED CLASS3 COMMON BLOCK.
C     * JUL 23/02 - D.VERSEGHY. MODIFICATIONS TO MOVE ADDITION OF AIR
C     *                         TO CANOPY MASS INTO APREP; SHORTENED
C     *                         CLASS4 COMMON BLOCK.
C     * MAR 18/02 - D.VERSEGHY. NEW CALLS TO ALL SUBROUTINES TO ENABLE
C     *                         ASSIGNMENT OF USER-SPECIFIED VALUES TO
C     *                         ALBEDOS AND VEGETATION PROPERTIES; NEW
C     *                         "CLASS8" COMMON BLOCK; MOVE CALCULATION 
C     *                         OF "FCLOUD" INTO CLASS DRIVER.
C     * SEP 19/00 - D.VERSEGHY. PASS ADDITIONAL ARRAYS TO APREP IN COMMON 
C     *                         BLOCK CLASS7, FOR CALCULATION OF NEW 
C     *                         STOMATAL RESISTANCE COEFFICIENTS USED
C     *                         IN TPREP.
C     * APR 12/00 - D.VERSEGHY. RCMIN NOW VARIES WITH VEGETATION TYPE:
C     *                         PASS IN BACKGROUND ARRAY "RCMINX".
C     * DEC 16/99 - D.VERSEGHY. ADD "XLEAF" ARRAY TO CLASS7 COMMON BLOCK
C     *                         AND CALCULATION OF LEAF DIMENSION PARAMETER
C     *                         "DLEAF" IN APREP.
C     * NOV 16/98 - M.LAZARE.   "DLON" NOW PASSED IN AND USED DIRECTLY
C     *                         (INSTEAD OF INFERRING FROM "LONSL" AND 
C     *                         "ILSL" WHICH USED TO BE PASSED) TO PASS
C     *                         TO APREP TO CALCULATE GROWTH INDEX. THIS
C     *                         IS DONE TO MAKE THE PHYSICS PLUG COMPATIBLE
C     *                         FOR USE WITH THE RCM WHICH DOES NOT HAVE
C     *                         EQUALLY-SPACED LONGITUDES.
C     * JUN 20/97 - D.VERSEGHY. CLASS - VERSION 2.7.
C     *                         MODIFICATIONS TO ALLOW FOR VARIABLE
C     *                         SOIL PERMEABLE DEPTH.
C     * SEP 27/96 - D.VERSEGHY. CLASS - VERSION 2.6.
C     *                         FIX BUG TO CALCULATE GROUND ALBEDO
C     *                         UNDER CANOPIES AS WELL AS OVER BARE
C     *                         SOIL.
C     * JAN 02/96 - D.VERSEGHY. CLASS - VERSION 2.5.
C     *                         COMPLETION OF ENERGY BALANCE
C     *                         DIAGNOSTICS.
C     *                         ALSO, PASS IDISP TO SUBROUTINE APREP.
C     * AUG 30/95 - D.VERSEGHY. CLASS - VERSION 2.4.
C     *                         VARIABLE SURFACE DETENTION CAPACITY
C     *                         IMPLEMENTED.
C     * AUG 16/95 - D.VERSEGHY. THREE NEW ARRAYS TO COMPLETE WATER
C     *                         BALANCE DIAGNOSTICS.
C     * OCT 14/94 - D.VERSEGHY. CLASS - VERSION 2.3.
C     *                         REVISE CALCULATION OF FCLOUD TO
C     *                         HANDLE CASES WHERE INCOMING SOLAR
C     *                         RADIATION IS ZERO AT LOW SUN ANGLES.
C     * NOV 24/92 - M.LAZARE.   CLASS - VERSION 2.1.
C     *                         MODIFIED FOR MULTIPLE LATITUDES.
C     * OCT 13/92 - D.VERSEGHY/M.LAZARE. REVISED AND VECTORIZED CODE
C     *                                  FOR MODEL VERSION GCM7.
C     * AUG 12/91 - D.VERSEGHY. CODE FOR MODEL VERSION GCM7U -
C     *                         CLASS VERSION 2.0 (WITH CANOPY).
C     * APR 11/89 - D.VERSEGHY. VISIBLE AND NEAR-IR ALBEDOS AND 
C     *                         TRANSMISSIVITIES FOR COMPONENTS OF
C     *                         LAND SURFACE.
C
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER IDAY,ILG,IL1,IL2,JL,IC,ICP1,IG,IDISP,IZREF,IWF,
     1        IPAI,IHGT,IALC,IALS,IALG,I,J,N, NBS, ISNOALB
C
C     * OUTPUT ARRAYS.
C
      REAL FC    (ILG),   FG    (ILG),   FCS   (ILG),   FGS   (ILG),    
     1     ALVSCN(ILG),   ALIRCN(ILG),   ALVSG (ILG),   ALIRG (ILG),     
     2     ALVSCS(ILG),   ALIRCS(ILG),   ALVSSN(ILG),   ALIRSN(ILG),   
     3     ALVSGC(ILG),   ALIRGC(ILG),   ALVSSC(ILG),   ALIRSC(ILG),
     3     TRVSCN(ILG),   TRIRCN(ILG),   TRVSCS(ILG),   TRIRCS(ILG),  
     4     FSVF  (ILG),   FSVFS (ILG),   
     5     RAICAN(ILG),   RAICNS(ILG),   SNOCAN(ILG),   SNOCNS(ILG),   
     6     FRAINC(ILG),   FSNOWC(ILG),   FRAICS(ILG),   FSNOCS(ILG),
     7     DISP  (ILG),   DISPS (ILG),   ZOMLNC(ILG),   ZOMLCS(ILG),   
     8     ZOELNC(ILG),   ZOELCS(ILG),   ZOMLNG(ILG),   ZOMLNS(ILG),   
     9     ZOELNG(ILG),   ZOELNS(ILG),   CHCAP (ILG),   CHCAPS(ILG),   
     A     CMASSC(ILG),   CMASCS(ILG),   CWLCAP(ILG),   CWFCAP(ILG),   
     B     CWLCPS(ILG),   CWFCPS(ILG),   RC    (ILG),   RCS   (ILG),   
     C     ZPLIMC(ILG),   ZPLIMG(ILG),   ZPLMCS(ILG),   ZPLMGS(ILG),   
     D     RBCOEF(ILG),   TRSNOWC(ILG),  ZSNOW (ILG),   WSNOW (ILG),
     E     ALVS  (ILG),   ALIR  (ILG),   HTCC  (ILG),   HTCS  (ILG),   
     F     WTRC  (ILG),   WTRS  (ILG),   WTRG  (ILG),   CMAI  (ILG),
     G     FSNOW (ILG)
C
      REAL TRSNOWG(ILG,NBS), ALTG(ILG,NBS), ALSNO(ILG,NBS)
C
      REAL FROOT (ILG,IG),  FROOTS(ILG,IG),  HTC   (ILG,IG)
C
C     * INPUT ARRAYS DEPENDENT ON LONGITUDE.
C  
      REAL FCANMX(ILG,ICP1),   ZOLN  (ILG,ICP1),
     1     ALVSC (ILG,ICP1),   ALIRC (ILG,ICP1),
     2     PAIMAX(ILG,IC),     PAIMIN(ILG,IC),     CWGTMX(ILG,IC),                     
     3     ZRTMAX(ILG,IC),     RSMIN (ILG,IC),     QA50  (ILG,IC),
     4     VPDA  (ILG,IC),     VPDB  (ILG,IC),     PSIGA (ILG,IC),
     5     PSIGB (ILG,IC),     PAIDAT(ILG,IC),     HGTDAT(ILG,IC),     
     4     ACVDAT(ILG,IC),     ACIDAT(ILG,IC),
     5     THLIQ (ILG,IG),     THICE (ILG,IG),     TBAR  (ILG,IG)  
C
      REAL ASVDAT(ILG),   ASIDAT(ILG),   AGVDAT(ILG),   AGIDAT(ILG),
     1     ALGWV (ILG),   ALGWN (ILG),   ALGDV (ILG),   ALGDN (ILG),
     +     RHOSNI(ILG),   Z0ORO (ILG),
     2     RCAN  (ILG),   SNCAN (ILG),   TCAN  (ILG),   GROWTH(ILG),      
     3     SNO   (ILG),   TSNOW (ILG),   RHOSNO(ILG),   ALBSNO(ILG),
     4     FCLOUD(ILG),   TA    (ILG),   VPD   (ILG),   RHOAIR(ILG),
     5     COSZS (ILG),   QSWINV(ILG),   DLON  (ILG),   ZBLEND(ILG),
     6     SNOLIM(ILG),   ZPLMG0(ILG),   ZPLMS0(ILG),   RADJ  (ILG),
     7     REFSNO(ILG),   BCSNO(ILG)
C
      REAL, DIMENSION(ILG,NBS) ::
     + FSDB, FSFB
C
C    * SOIL PROPERTY ARRAYS.
C
      REAL DELZW (ILG,IG),  ZBOTW (ILG,IG),  THPOR (ILG,IG),  
     1     THLMIN(ILG,IG),  PSISAT(ILG,IG),  BI    (ILG,IG),
     2     PSIWLT(ILG,IG),  HCPS  (ILG,IG)
C
      INTEGER   ISAND (ILG,IG)
C
C     * OTHER DATA ARRAYS WITH NON-VARYING VALUES.
C
      REAL GROWYR(18,4,2),  DELZ  (IG),      ZORAT (4),
     1     CANEXT(4),       XLEAF (4)
C
C     * CTEM-RELATED FIELDS.

C     * AILCG  - GREEN LAI FOR USE IN PHOTOSYNTHESIS
C     * AILCGS - GREEN LAI FOR CANOPY OVER SNOW SUB-AREA
C     * AILCMIN- MIN. LAI FOR CTEM PFTs
C     * AILCMAX- MAX. LAI FOR CTEM PFTs
C     * L2MAX  - MAXIMUM NUMBER OF LEVEL 2 CTEM PFTs
C     * NOL2PFTS - NUMBER OF LEVEL 2 CTEM PFTs
C     * FCANC  - FRACTION OF CANOPY OVER GROUND FOR CTEM's 9 PFTs
C     * FCANCS - FRACTION OF CANOPY OVER SNOW FOR CTEM's 9 PFTs
C     * SEE BIO2STR SUBROUTINE FOR DEFINITION OF OTHER CTEM VARIABLES
C
      REAL FCANCMX(ILG,ICTEM),   RMATC(ILG,IC,IG),
     1      AILC  (ILG,IC),      PAIC (ILG,IC),
     2      AILCG (ILG,ICTEM),   AILCGS(ILG,ICTEM),
     3      FCANC(ILG,ICTEM),    FCANCS(ILG,ICTEM),
     4      SLAIC(ILG,IC)

      INTEGER ICTEM, ICTEMMOD, L2MAX, NOL2PFTS(IC)
C                                                                                 
C     * INTERNAL WORK ARRAYS FOR THIS AND ASSOCIATED SUBROUTINES.
C
      REAL RMAT (ILG,IC,IG),H     (ILG,IC),  HS    (ILG,IC),
     1     PAI   (ILG,IC),  PAIS  (ILG,IC),  FCAN  (ILG,IC),  
     2     FCANS (ILG,IC),  CXTEFF(ILG,IC),  AIL   (ILG,IC),
     3     RCACC (ILG,IC),  RCG   (ILG,IC),  RCV   (ILG,IC)
C
      REAL PSIGND(ILG),     CWCPAV(ILG),     
     1     GROWA (ILG),     GROWN (ILG),     GROWB (ILG),     
     2     RRESID(ILG),     SRESID(ILG),  
     +     FRTOT (ILG),     FRTOTS(ILG),
     3     TRVS  (ILG),     TRIR  (ILG),     RCT   (ILG),     
     4     GC    (ILG),     PAICAN(ILG),     PAICNS(ILG) 
C
C     * COMMON BLOCK PARAMETERS.
C
      REAL DELT,TFREZ,RGAS,RGASV,GRAV,SBC,VKC,CT,VMIN,TCW,TCICE,
     1     TCSAND,TCCLAY,TCOM,TCDRYS,RHOSOL,RHOOM,HCPW,HCPICE,
     2     HCPSOL,HCPOM,HCPSND,HCPCLY,SPHW,SPHICE,SPHVEG,SPHAIR,
     3     RHOW,RHOICE,TCGLAC,CLHMLT,CLHVAP,PI,ZOLNG,ZOLNS,ZOLNI,
     4     ZORATG,ALVSI,ALIRI,ALVSO,ALIRO,ALBRCK
C
      COMMON /CLASS1/ DELT,TFREZ                                               
      COMMON /CLASS2/ RGAS,RGASV,GRAV,SBC,VKC,CT,VMIN
      COMMON /CLASS3/ TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,
     1                RHOSOL,RHOOM
      COMMON /CLASS4/ HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     1                SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,
     2                TCGLAC,CLHMLT,CLHVAP
      COMMON /CLASS6/ PI,GROWYR,ZOLNG,ZOLNS,ZOLNI,ZORAT,ZORATG                   
      COMMON /CLASS7/ CANEXT,XLEAF
      COMMON /CLASS8/ ALVSI,ALIRI,ALVSO,ALIRO,ALBRCK
                                                                                  
C------------------------------------------------------------------
C     * CALCULATION OF SNOW DEPTH ZSNOW AND FRACTIONAL SNOW COVER
C     * FSNOW; INITIALIZATION OF COMPUTATIONAL ARRAYS. 
C                                                                                  
      DO 100 I=IL1,IL2                                                            
          IF(SNO(I).GT.0.0) THEN                                              
              ZSNOW(I)=SNO(I)/RHOSNO(I)                                       
              IF(ZSNOW(I).GE.(SNOLIM(I)-0.00001)) THEN                                     
                  FSNOW(I)=1.0                                                   
              ELSE                                                            
                  FSNOW(I)=ZSNOW(I)/SNOLIM(I)
                  ZSNOW(I)=SNOLIM(I)
                  WSNOW(I)=WSNOW(I)/FSNOW(I)
              ENDIF                                                           
          ELSE                                                                
              ZSNOW(I)=0.0                                                    
              FSNOW(I)=0.0                                                       
          ENDIF
C
          ALVSCN(I)=0.0                                                   
          ALIRCN(I)=0.0                                                   
          ALVSCS(I)=0.0  
          ALIRCS(I)=0.0    
          TRVSCN(I)=0.0                                                   
          TRIRCN(I)=0.0                                                   
          TRVSCS(I)=0.0                                                   
          TRIRCS(I)=0.0
          ALVSSN(I)=0.0                                                   
          ALIRSN(I)=0.0                                                   
          ALVSG (I)=0.0
          ALIRG (I)=0.0
          ALVSGC(I)=0.0
          ALIRGC(I)=0.0
          ALVSSC(I)=0.0
          ALIRSC(I)=0.0
          TRSNOWC(I)=0.0                                                       

          TRSNOWG(I,1:NBS) = 0.0
          ALTG(I,1:NBS)    = 0.0                                                     
          ALSNO(I,1:NBS)   = 0.0

  100 CONTINUE
C
C     * PREPARATION.
C
      CALL APREP (FC,FG,FCS,FGS,PAICAN,PAICNS,FSVF,FSVFS, 
     1            FRAINC,FSNOWC,FRAICS,FSNOCS,RAICAN,RAICNS,SNOCAN,
     2            SNOCNS,DISP,DISPS,ZOMLNC,ZOMLCS,ZOELNC,ZOELCS,
     3            ZOMLNG,ZOMLNS, ZOELNG,ZOELNS,CHCAP,CHCAPS,CMASSC,
     4            CMASCS,CWLCAP,CWFCAP,CWLCPS,CWFCPS,RBCOEF,
     5            ZPLIMC,ZPLIMG,ZPLMCS,ZPLMGS,HTCC,HTCS,HTC,
     +            FROOT,FROOTS,
     6            WTRC,WTRS,WTRG,CMAI,PAI,PAIS,AIL,FCAN,FCANS,PSIGND,
     7            FCANMX,ZOLN,PAIMAX,PAIMIN,CWGTMX,ZRTMAX,
     8            PAIDAT,HGTDAT,THLIQ,THICE,TBAR,RCAN,SNCAN,
     9            TCAN,GROWTH,ZSNOW,TSNOW,FSNOW,RHOSNO,SNO,Z0ORO,
     A            ZBLEND,ZPLMG0,ZPLMS0,
     B            TA,RHOAIR,RADJ,DLON,RHOSNI,DELZ,DELZW,ZBOTW,
     C            THPOR,THLMIN,PSISAT,BI,PSIWLT,HCPS,ISAND,
     D            ILG,IL1,IL2,JL,IC,ICP1,IG,IDAY,IDISP,IZREF,IWF,
     E            IPAI,IHGT,RMAT,H,HS,CWCPAV,GROWA,GROWN,GROWB,
     F            RRESID,SRESID,FRTOT,FRTOTS,
     G            FCANCMX,ICTEM,ICTEMMOD,RMATC,
     H            AILC,PAIC,AILCG,L2MAX,NOL2PFTS,
     I            AILCGS,FCANCS,FCANC,SLAIC)

C
C     * BARE SOIL ALBEDOS.
C
      CALL GRALB(ALVSG,ALIRG,ALVSGC,ALIRGC,
     1            ALGWV,ALGWN,ALGDV,ALGDN,
     2            THLIQ,FSNOW,ALVSC(1,5),ALIRC(1,5),
     3            FCANMX(1,5),AGVDAT,AGIDAT,FG,ISAND,
     4            ILG,IG,IL1,IL2,JL,IALG) 


C     * SNOW ALBEDOS AND TRANSMISSIVITY.
C 
      CALL SNOALBA(ALVSSN,ALIRSN,ALVSSC,ALIRSC,ALBSNO,
     1             TRSNOWC, ALSNO, TRSNOWG, FSDB, FSFB, RHOSNO,
     2             REFSNO, BCSNO,SNO,COSZS,ZSNOW,FSNOW,ASVDAT,ASIDAT,
     3             ALVSG, ALIRG,
     4             ILG,IG,IL1,IL2,JL,IALS,NBS,ISNOALB)

C     * CANOPY ALBEDOS AND TRANSMISSIVITIES, AND VEGETATION
C     * STOMATAL RESISTANCE.
C
      CALL CANALB(ALVSCN,ALIRCN,ALVSCS,ALIRCS,TRVSCN,TRIRCN,
     1            TRVSCS,TRIRCS,RC,RCS,
     2            ALVSC,ALIRC,RSMIN,QA50,VPDA,VPDB,PSIGA,PSIGB,
     3            FC,FCS,FSNOW,FSNOWC,FSNOCS,FCAN,FCANS,PAI,PAIS,
     4            AIL,PSIGND,FCLOUD,COSZS,QSWINV,VPD,TA,
     5            ACVDAT,ACIDAT,ALVSGC,ALIRGC,ALVSSC,ALIRSC,
     6            ILG,IL1,IL2,JL,IC,ICP1,IG,IALC,
     7            CXTEFF,TRVS,TRIR,RCACC,RCG,RCV,RCT,GC) 
C
C     * EFFECTIVE WHOLE-SURFACE VISIBLE AND NEAR-IR ALBEDOS.
C
      DO 500 I=IL1,IL2
          ALVS(I)=FC(I)*ALVSCN(I)+FG(I)*ALVSG(I)+FCS(I)*ALVSCS(I)+
     1            FGS(I)*ALVSSN(I)                                                
          ALIR(I)=FC(I)*ALIRCN(I)+FG(I)*ALIRG(I)+FCS(I)*ALIRCS(I)+            
     1            FGS(I)*ALIRSN(I)                                                
  500 CONTINUE
C
      IF (ISNOALB .EQ. 0) THEN
         DO I = IL1, IL2
            ALTG(I,1) = ALVS(I)
            ALTG(I,2) = ALIR(I)
            ALTG(I,3) = ALIR(I)
            ALTG(I,4) = ALIR(I)
         END DO ! I
      ELSEIF (ISNOALB .EQ. 1) THEN
         DO I = IL1, IL2
            ALTG(I,1) = FC(I)*ALVSCN(I)+FG(I)*ALVSG(I)+FCS(I)*ALVSCS(I)+
     1                  FGS(I)*ALSNO(I,1)
            ALTG(I,2) = FC(I)*ALIRCN(I)+FG(I)*ALIRG(I)+FCS(I)*ALIRCS(I)+
     1                  FGS(I)*ALSNO(I,2)
            ALTG(I,3) = FC(I)*ALIRCN(I)+FG(I)*ALIRG(I)+FCS(I)*ALIRCS(I)+
     1                  FGS(I)*ALSNO(I,3)
            ALTG(I,4) = FC(I)*ALIRCN(I)+FG(I)*ALIRG(I)+FCS(I)*ALIRCS(I)+
     1                  FGS(I)*ALSNO(I,4)
         END DO ! I
      END IF ! ISNOALB

      RETURN                                                                      
      END
