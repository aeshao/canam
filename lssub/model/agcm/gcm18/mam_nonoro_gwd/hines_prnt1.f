      SUBROUTINE HINES_PRNT1 (FLUX_U,FLUX_V,DRAG_U,DRAG_V,VEL_U,VEL_V,
     1                        ALT,SIGMA_T,SIGMA_ALPHA,M_ALPHA,
     2                        IU_PRINT,IV_PRINT,NMESSG,
     3                        ILPRT1,ILPRT2,LEVPRT1,LEVPRT2,
     4                        NAZ,NLONS,NLEVS,NAZMTH)
C
C  Print out altitude profiles of various quantities from
C  Hines Doppler spread gravity wave parameterization scheme.
C  (NOTE: only for NAZ = 4, 8 or 12). 
C
C  Aug. 8/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Feb. 2/96 - C. McLandress (12 and 16 azimuths)
C

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Input arguements:
C  -----------------
C
C     * IU_PRINT = 1 to print out values in east-west direction.
C     * IV_PRINT = 1 to print out values in north-south direction.
C     * NMESSG   = unit number for printed output.
C     * ILPRT1   = first longitudinal index to print.
C     * ILPRT2   = last longitudinal index to print.
C     * LEVPRT1  = first altitude level to print.
C     * LEVPRT2  = last altitude level to print.
C
      INTEGER  NAZ, ILPRT1, ILPRT2, LEVPRT1, LEVPRT2
      INTEGER  NLONS, NLEVS, NAZMTH
      INTEGER  IU_PRINT, IV_PRINT, NMESSG
      REAL    FLUX_U(NLONS,NLEVS), FLUX_V(NLONS,NLEVS)
      REAL    DRAG_U(NLONS,NLEVS), DRAG_V(NLONS,NLEVS)
      REAL    VEL_U(NLONS,NLEVS), VEL_V(NLONS,NLEVS)
      REAL    ALT(NLONS,NLEVS), SIGMA_T(NLONS,NLEVS)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH)
C
C  Internal variables.
C  -------------------
C
      INTEGER  N_EAST, N_WEST, N_NORTH, N_SOUTH
      INTEGER  I, L
C-----------------------------------------------------------------------
C
C  Azimuthal indices of cardinal directions.
C
      N_EAST = 1
      IF (NAZ.EQ.4)  THEN
        N_NORTH = 2
        N_WEST  = 3       
        N_SOUTH = 4       
      ELSE IF (NAZ.EQ.8)  THEN
        N_NORTH = 3
        N_WEST  = 5       
        N_SOUTH = 7       
      ELSE IF (NAZ.EQ.12)  THEN
        N_NORTH = 4
        N_WEST  = 7       
        N_SOUTH = 10       
      ELSE IF (NAZ.EQ.16)  THEN
        N_NORTH = 5
        N_WEST  = 9       
        N_SOUTH = 13       
      END IF
C
C  Print out values for range of longitudes.
C
      DO 100 I = ILPRT1,ILPRT2
C
C  Print east-west wind, sigmas, cutoff wavenumbers, flux and drag.
C
        IF (IU_PRINT.EQ.1)  THEN
          WRITE (NMESSG,*) 
          WRITE (NMESSG,6001) I
          WRITE (NMESSG,6005) 
 6001     FORMAT ( 'Hines GW (east-west) at longitude I =',I3)
 6005     FORMAT (15x,' U ',2x,'sig_E',2x,'sig_T',3x,'m_E',
     &            4x,'m_W',4x,'fluxU',5x,'gwdU')
          DO 10 L = LEVPRT1,LEVPRT2
            WRITE (NMESSG,6701) ALT(I,L)/1.E3, VEL_U(I,L),
     &                          SIGMA_ALPHA(I,L,N_EAST), SIGMA_T(I,L),
     &                          M_ALPHA(I,L,N_EAST)*1.E3, 
     &                          M_ALPHA(I,L,N_WEST)*1.E3,
     &                          FLUX_U(I,L)*1.E5, DRAG_U(I,L)*24.*3600.
  10      CONTINUE
 6701     FORMAT (' z=',f7.2,1x,3f7.1,2f7.3,f9.4,f9.3)
        END IF
C
C  Print north-south winds, sigmas, cutoff wavenumbers, flux and drag.
C
        IF (IV_PRINT.EQ.1)  THEN
          WRITE(NMESSG,*) 
          WRITE(NMESSG,6002) 
 6002     FORMAT ( 'Hines GW (north-south) at longitude I =',I3)
          WRITE(NMESSG,6006) 
 6006     FORMAT (15x,' V ',2x,'sig_N',2x,'sig_T',3x,'m_N',
     &            4x,'m_S',4x,'fluxV',5x,'gwdV')
          DO 20 L = LEVPRT1,LEVPRT2
            WRITE (NMESSG,6701) ALT(I,L)/1.E3, VEL_V(I,L),
     &                          SIGMA_ALPHA(I,L,N_NORTH), SIGMA_T(I,L),
     &                          M_ALPHA(I,L,N_NORTH)*1.E3, 
     &                          M_ALPHA(I,L,N_SOUTH)*1.E3,
     &                          FLUX_V(I,L)*1.E5, DRAG_V(I,L)*24.*3600.
 20       CONTINUE
        END IF
C
 100  CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
