      SUBROUTINE HINES_HEAT2 (HEAT,DIFFCO,ALT,M_ALPHA,SPECFAC,
     1                       DRAGIL,BVFREQ,DENSITY,DENSB,
     2                       SIGMA_T,SIGMA_ALPHA,VISC_MOL,
     3                       KSTAR,SLOPE,F1,F2,F3,F5,F6,NAZ,
     4                       IL1,IL2,LEVBOT,LEVTOP,NLONS,NLEVS,NAZMTH)
C
C  This routine calculates the gravity wave induced heating and eddy 
C  diffusion coefficient on a longitude by altitude grid for the Hines 
C  Doppler spread parameterization scheme. The output is placed on the 
C  intermediate levels such that the highest (and lowest) levels for 
C  diffusion and heating are 1/2 grid step above (and below) the highest 
C  (and lowest) momentum levels. This routine can be used for a nonzero 
C  (constant) minimum cutoff wavenumber since its vertical derivative is zero.
C
C  Aug. 6/95 - C. McLandress
C
C  Modifications:
C  --------------
C
C  Jun 16/06 - M. Lazare     (replace constants by variables in 
c                             intrinsic calls such as "max" to
c                             enable compiling in 32-bits with real*8.
C                             This change is cosmetic)
C  Aug.25/99 - C. McLandress (incorrect heating formulation given in Hines
C                             JASTP,1997 corrected)
C  Feb. 2/96 - C. McLandress (diffusion and heating calculated at half levels;
C                             logical flags added; vertical derivative of
C                             cutoff wavenumber calculated in this routine;
C                             molecular viscosity not used in calculation
C                             of eddy diffusion coefficient) 
C  Jul. 1/96 - C. McLandress (R1**R2 for R1<0 avoided in diffusion calc.)
C
C  Output arguements:
C  ------------------
C     * HEAT   = gravity wave heating (K/sec) (must be initialized to zero).
C     * DIFFCO = diffusion coefficient (m^2/sec).
C
C  Input arguements:
C  -----------------
C     * ALT         = altitude (m) of levels.
C     * M_ALPHA     = cutoff vertical wavenumber (1/m) specified on
C     *               the momentum levels.
C     * SPECFAC     = AK_ALPHA * K_ALPHA.
C     * DRAGIL      = longitudes and levels where drag to be calculated.
C     * BVFREQ      = background Brunt Vassala frequency (rad/sec).
C     * DENSITY     = background density (kg/m^3).
C     * DENSB       = background density at bottom level (kg/m^3).
C     * SIGMA_T     = total rms horizontal wind (m/s).
C     * SIGMA_ALPHA = horizontal wind variances in each azimuth (m/s).
C     * VISC_MOL    = molecular viscosity (m^2/s).
C     * KSTAR       = typical gravity wave horizontal wavenumber (1/m).
C     * SLOPE       = slope of incident vertical wavenumber spectrum.
C     * F1-F6       = Hines's fudge factors.
C     * NAZ         = number of horizontal azimuths used.
C     * IL1         = first longitudinal index to use (IL1 >= 1).
C     * IL2         = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEVBOT      = index of bottom drag level.
C     * LEVTOP      = index of top drag level.
C     * NLONS       = number of longitudes.
C     * NLEVS       = number of vertical levels.
C     * NAZMTH      = azimuthal array dimension (NAZMTH >= NAZ).
C
      IMPLICIT NONE
      INTEGER  NAZ, IL1, IL2, LEVBOT, LEVTOP, NLONS, NLEVS, NAZMTH
cccc      LOGICAL  DRAGIL(NLONS,NLEVS)
      INTEGER  DRAGIL(NLONS,NLEVS)
      REAL    KSTAR, SLOPE, F1, F2, F3, F5, F6, ZERO
      REAL    HEAT(NLONS,NLEVS), DIFFCO(NLONS,NLEVS), ALT(NLONS,NLEVS)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH), SPECFAC(NLONS,NAZMTH)
      REAL    BVFREQ(NLONS,NLEVS), DENSITY(NLONS,NLEVS),  DENSB(NLONS) 
      REAL    SIGMA_T(NLONS,NLEVS), VISC_MOL(NLONS,NLEVS)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
C
C Internal variables.
C -------------------
C
      INTEGER  I, L, N, L1, LH, LP1
      INTEGER  LEV1, LEV2, LEV1P, LEV2M
      REAL    M_SUB_M_TURB, M_SUB_M_MOL, M_SUB_M, HEATNG
      REAL    VISC, VISC_MIN, CPGAS, SM1, DMDZ, MALP
      REAL    SIG, SIGA, BVF, DENS
C
      DATA  CPGAS / 1004. /         ! specific heat at constant pressure
      DATA  VISC_MIN / 1.E-10 /     ! minimum permissible viscosity
      DATA ZERO /0./
C-----------------------------------------------------------------------     
C
C  For MAM level indices which increase from top down then levels of
C  diffusion coefficient and heating defined so that half level L=2 is 
C  half way between fulle levels L=1 (the model top)  and L=2, etc. 
C  For other models in which the indices increase from bottom up then 
C  first intermediate half level designated L=1.
C
      L1 = 0
      LEV1 = LEVBOT
      LEV2 = LEVTOP - 1
      IF (LEVBOT.GT.LEVTOP)  THEN
        L1 = 1
        LEV1 = LEVTOP
        LEV2 = LEVBOT - 1
      END IF
C
      SM1 = SLOPE - 1.
C
C  Term proportional to energy deposition rate (store in HEAT array)
C 
      DO 30 N = 1,NAZ
      DO 30 L = LEV1,LEV2
        LH  = L + L1
        LP1 = L + 1           
        DO 25 I = IL1,IL2
          IF (DRAGIL(I,L).eq.1)  THEN
            BVF  = ( BVFREQ(I,L)  + BVFREQ(I,LP1)  ) / 2.
            SIG  = ( SIGMA_T(I,L) + SIGMA_T(I,LP1) ) / 2.
            SIGA = ( SIGMA_ALPHA(I,L,N) + SIGMA_ALPHA(I,LP1,N) ) / 2.
            VISC = ( VISC_MOL(I,L) + VISC_MOL(I,LP1) ) / 2.
            VISC = MAX ( VISC, VISC_MIN )
            MALP = ( M_ALPHA(I,LP1,N) + M_ALPHA(I,L,N) ) / 2. 
            DMDZ = ( M_ALPHA(I,LP1,N) - M_ALPHA(I,L,N) ) 
     ^             / ( ALT(I,LP1) - ALT(I,L) )
            M_SUB_M_TURB = BVF / ( F2 * SIG )
            M_SUB_M_MOL  = ( BVF * KSTAR / VISC )**0.33333333 / F3
            M_SUB_M    = MIN ( M_SUB_M_TURB, M_SUB_M_MOL )
            HEAT(I,LH) = HEAT(I,LH) + SPECFAC(I,N) * MALP**SM1 * DMDZ
     ^                   * ( F1 * SIGA + BVF / M_SUB_M )           
          END IF
 25     CONTINUE
 30   CONTINUE
C
C  Heating rate and turbulent diffusion coefficient.
C
      DO 40 L = LEV1,LEV2
        LH  = L + L1
        LP1 = L + 1           
        DO 35 I = IL1,IL2
          IF (DRAGIL(I,L).eq.1)  THEN
            DENS = ( DENSITY(I,L) + DENSITY(I,LP1) ) / 2.
            BVF  = ( BVFREQ(I,L)  + BVFREQ(I,LP1)  ) / 2.
            SIG  = ( SIGMA_T(I,L) + SIGMA_T(I,LP1) ) / 2.
            VISC = ( VISC_MOL(I,L) + VISC_MOL(I,LP1) ) / 2.
            VISC = MAX ( VISC, VISC_MIN )
            M_SUB_M_TURB = BVF / ( F2 * SIG )
            M_SUB_M_MOL  = ( BVF * KSTAR / VISC )**0.33333333 / F3
            M_SUB_M = MIN ( M_SUB_M_TURB, M_SUB_M_MOL )
            HEATNG  = ABS ( MIN ( HEAT(I,LH), ZERO ) ) 
     ^                * F5 * DENSB(I) / DENS
            HEAT(I,LH) = HEATNG / CPGAS
            DIFFCO(I,LH) = F6 * HEATNG**0.33333333 
     ^                     / M_SUB_M_TURB**1.33333333
          END IF
 35     CONTINUE
 40   CONTINUE
C
C  Calculate diffusion and heating at top level for the case
C  where indices of vertical levels increase from top down.
C
      IF (LEVBOT.GT.LEVTOP)  THEN
        LEV1P = LEVTOP + 1
        DO 45 I = IL1,IL2
          IF (DRAGIL(I,LEV1P).eq.1)  THEN
            DIFFCO(I,LEVTOP) = DIFFCO(I,LEV1P) 
            HEAT(I,LEVTOP)   = HEAT(I,LEV1P) 
          END IF
 45     CONTINUE
        RETURN
      END IF
C
C  Calculate diffusion and heating at top level for the case
C  where indices of vertical levels increase from bottom up.
C
      IF (LEVBOT.LT.LEVTOP)  THEN
        LEV2M = LEVTOP - 1
        DO 50 I = IL1,IL2
          IF (DRAGIL(I,LEV2M).eq.1)  THEN
            DIFFCO(I,LEVTOP) = DIFFCO(I,LEV2M) 
            HEAT(I,LEVTOP)   = HEAT(I,LEV2M) 
          END IF
 50     CONTINUE
      END IF
C
      RETURN
C-----------------------------------------------------------------------
      END
