      SUBROUTINE HINES_EXP (DATA,DATA_ZMAX,ALT,ALT_EXP,IORDER,
     1                      IL1,IL2,LEV1,LEV2,NLONS,NLEVS)
C
C  This routine exponentially damps a longitude by altitude array 
C  of data above a specified altitude.
C
C  Aug. 13/95 - C. McLandress
C
C  Output arguement:
C  -----------------
C
C     * DATA = modified data array.
C
C  Input arguements:
C  -----------------
C
C     * DATA    = original data array.
C     * ALT     = altitudes.
C     * ALT_EXP = altitude above which exponential decay applied.
C     * IORDER  = 1 means vertical levels are indexed from top down 
C     *           (i.e., highest level indexed 1 and lowest level NLEVS);
C     *           .NE. 1 highest level is index NLEVS.
C     * IL1     = first longitudinal index to use (IL1 >= 1).
C     * IL2     = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEV1    = first altitude level to use (LEV1 >=1). 
C     * LEV2    = last altitude level to use (LEV1 < LEV2 <= NLEVS).
C     * NLONS   = number of longitudes.
C     * NLEVS   = number of vertical
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Input work arrays:
C  ------------------
C
C     * DATA_ZMAX = data values just above altitude ALT_EXP.
C
      INTEGER  IORDER, IL1, IL2, LEV1, LEV2, NLONS, NLEVS
      REAL    ALT_EXP
      REAL    DATA(NLONS,NLEVS), DATA_ZMAX(NLONS), ALT(NLONS,NLEVS)
C
C  Internal variables.
C  -------------------
C
      INTEGER  LEVBOT, LEVTOP, LINCR, I, L
      REAL    HSCALE
      DATA  HSCALE / 5.E3 /
C-----------------------------------------------------------------------     
C
C  Index of lowest altitude level (bottom of drag calculation).
C
      LEVBOT = LEV2
      LEVTOP = LEV1
      LINCR  = 1
      IF (IORDER.NE.1)  THEN
        LEVBOT = LEV1
        LEVTOP = LEV2
        LINCR  = -1
      END IF
C
C  Data values at first level above ALT_EXP.
C
      DO 20 I = IL1,IL2
        DO 10 L = LEVTOP,LEVBOT,LINCR
          IF (ALT(I,L) .GE. ALT_EXP)  THEN
            DATA_ZMAX(I) = DATA(I,L) 
          END IF   
 10     CONTINUE
 20   CONTINUE
C
C  Exponentially damp field above ALT_EXP to model top at L=1.
C
      DO 40 L = 1,LEV2 
        DO 30 I = IL1,IL2
          IF (ALT(I,L) .GE. ALT_EXP)  THEN
            DATA(I,L) = DATA_ZMAX(I) * EXP( (ALT_EXP-ALT(I,L))/HSCALE )
          END IF
 30     CONTINUE
 40   CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
