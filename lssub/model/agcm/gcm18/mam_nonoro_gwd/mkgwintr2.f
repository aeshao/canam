      SUBROUTINE MKGWINTR2(UG,VG,TFROW,THROW,
     1           SHXKJ,SHJ,TSG,RGOCP,
     2           SGJ,SGBJ,UTENDGW,VTENDGW,URMS,
     3           PRESSG,RGAS,CPRES,ILEV,ILG,IL1,IL2,nhar)
C
C    * JUN 27/2006 - M.LAZARE.   NEW VERSION FOR GCM15F BASED ON MKGWINTR:
C    *                           - WORK ARRAYS ARE NOW LOCAL TO THE
C    *                             ROUTINE, IE NOT PASSED IN WITH
C    *                             "WRK" POINTERS.
C    * OCT 27/1998 - A.MEDVEDEV. ORIGINAL VERSION MKGWINTR.
C Isotropic case: waves in 4 directions (NWSE)
C Exact copy of MAM4 version
C
C PURPOSE: 
C    INTERFACE FOR GRAVITY WAVE DRAG ROUTINE (MK95, JGR95, D12)
C
C AUTHOR:   A. MEDVEDEV  (1996)
C           A. MEDVEDEV  (1998):  VECTORIZED 
C
C---------------- INPUT PARAMETERS: ----------------------------------   
C    UG(ILG,ILEV)- ROW OF *PF* LEVEL U-WIND
C    VG(ILG,ILEV)- ROW OF *PF* LEVEL V-WIND  
C TFROW(ILG,ILEV) - ROW OF FULL LEVEL TEMPERATURES (*PF*)
C THROW(ILG,ILEV) - ROW OF HALF LEVEL TEMPERATURES (*PH*) 
C   SGJ(ILG,ILEV)- VERT COORD SIGMA (CORRESPOND TO "FULL" IN GWD.f)
C  SGBJ(ILG,ILEV)- "BOTTOM" INTERFACE FOR SGJ (CORRESPOND TO "HALF" IN GWD.f)
C    PRESSG(ILG) - LATITUDE ROW OF SURFACE PRESSURE (PA)
C
C---------------- OUTPUT PARAMETERS: --------------------------------
C UTENDGW(ILG,ILEV) - GWD IN U-DIRECTION
C VTENDGW(ILG,ILEV) - GWD IN V-DIRECTION
C URMS   (ILG,ILEV) - RMS HORIZONTAL WIND

C---------------- IGW SPECTRUM PARAMETERS: --------------------------
C Modified DeSaubies spectrum

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C      PARAMETER(NHAR=15)
      PARAMETER(XKZ1=19.E3, XKZ2=900.)
      PARAMETER(ILEVGWS=16)
      PARAMETER(AMPL=200., XM=0.006, WAVELEN=3.0E5)
      PARAMETER(TNDMAX=500./86400.) !  Maximum allowed tendency (in m/s/

      REAL        UG(ILG,ILEV),       VG(ILG,ILEV),
     1         TFROW(ILG,ILEV),    THROW(ILG,ILEV),
     1       UTENDGW(ILG,ILEV),  VTENDGW(ILG,ILEV),
     2          SGBJ(ILG,ILEV),      SGJ(ILG,ILEV),
     3         SHXKJ(ILG,ILEV),      SHJ(ILG,ILEV),
     4           TSG(ILG,ILEV),   
     5          URMS(ILG,ILEV)
      REAL   PRESSG(ILG)
C      
C     * INTERNAL WORK ARRAYS:
C
      REAL    UU(ILG,ilev),  VV(ILG,ilev),  PF(ILG,ilev)  
      REAL    PH(ILG,ilev)
      REAL   DPF(ILG,ilev), DPH(ILG,ilev), RMS(ILG,ilev)

      REAL   USQ(ILG,NHAR,ilev), ZKZR(ILG,NHAR,ilev), 
     1      OMEG(ILG,NHAR,ilev), BETA(ILG,NHAR,ilev)

      REAL   SIGMA(ILG,NHAR), ALPHA(ILG,NHAR), OM(ILG,NHAR),
     1         ZKZ(NHAR), DDKZ(NHAR), USQL(NHAR)      

      REAL   AX(ILG,ilev),AXX(ILG,ilev)
      REAL   UBM(ILG,ilev), XV(ILG), YV(ILG)
      real BVFREQ(ILG,ILEV)

      REAL   V2(ilg,NHAR), DRAG(ilg,NHAR), GWKHU(ilg), VV2(ilg)
      REAL   XX(ilg), DZ(ilg), BVKH(ilg), VV3(ilg)                
 
      grav=9.80616
      PI=3.1415926
      RG=RGAS/GRAV 
      GCPRES=GRAV/CPRES

      LUPP=ILEV

C WAVE SOURCE AT 165 mb (xx km)

      LEVGWS=ILEVGWS
      GWKH=2.*PI/WAVELEN
      X1=2.*PI/XKZ1
      X2=2.*PI/XKZ2

      DXKZ=(LOG(X2)-LOG(X1))/REAL(NHAR-1) 
      DXKZ=EXP(DXKZ)                ! Set up the condensed m-grid
      ZKZ(1)=X2
      DO L=1,NHAR-1
         ZKZ(L+1)=ZKZ(L)/DXKZ
         DDKZ(L) =ZKZ(L)-ZKZ(L+1)
      END DO
      DDKZ(NHAR)=ZKZ(NHAR)*(DXKZ-1.)/DXKZ

      DO L=1,NHAR
         PSD=AMPL*ZKZ(L)/XM/(1.+(ZKZ(L)/XM)**4)
         USQL(L)=PSD*DDKZ(L)
      END DO
c
c---------------------------------------------------------------------------
c Determine the source layer wind and unit vectors, then project winds.
c---------------------------------------------------------------------------
c
c Just use the source level midpoint values for the source
c wind speed and direction (unit vector).
c
c     k=ilev+1-levgws
c     do i = IL1,IL2
c        ubm(i,k) = sqrt (UG(i,k)**2 + VG(i,k)**2)
c        xv(i) = UG(i,k) / ubm(i,k)
c        yv(i) = VG(i,k) / ubm(i,k)
c     end do
c
c Project the local wind at midpoints onto the source wind.
c
c     do k = 1, ilev+1-levgws
c        do i = IL1,IL2
c           ubm(i,k) = UG(i,k) * xv(i) + VG(i,k) * yv(i)
c        end do
c     end do
c
c     * Calculate  B-V frequency at all longitudes 
c 
      DO L=2,ILEV
         DO I=IL1,IL2
           DTTDSF=(THROW(I,L)/SHXKJ(I,L)-THROW(I,L-1)/
     1            SHXKJ(I,L-1))/(SHJ(I,L)-SHJ(I,L-1))
           DTTDSF=MIN(DTTDSF, -5./SGJ(I,L))
           BVFREQ(I,L)=SQRT(-DTTDSF*SGJ(I,L)*(SGJ(I,L)**RGOCP)/RGAS)
     1                   *GRAV/TSG(I,L)
         END DO
      END DO
c
c     * Define BVFREQ at top, smooth BVFREQ and set lower limit
c
      DO L=1,ILEV
         DO I=IL1,IL2
           IF(L.EQ.1)                        THEN
             BVFREQ(I,L) = BVFREQ(I,L+1)
           ENDIF
           IF(L.GT.1)                        THEN
             RATIO=5.*LOG(SGJ(I,L)/SGJ(I,L-1))
             BVFREQ(I,L) = (BVFREQ(I,L-1) + RATIO*BVFREQ(I,L))
     1                      /(1.+RATIO)
           ENDIF
           BVFREQ(I,L) = MAX(BVFREQ(I,L), 0.001)
         END DO
      END DO

C :-------------------------------------------------------------------
C
      DO L=1,ILEV                    ! Prepare 1D arrays for MKGWD
         LL=ILEV+1-L
         DO I=IL1,IL2
            PF(I,L)= SGJ(I,LL)*PRESSG(I)
            PH(I,L)=SGBJ(I,LL)*PRESSG(I)
            UU(I,L)=UG(I,LL)
            VV(I,L)=VG(I,LL) 
C           UTENDGW(I,LL)=0.0        ! Initialize gravity wave
C           VTENDGW(I,LL)=0.0        ! drag tendencies to zero
            RMS(I,L) =0.0
         END DO
      END DO

      DO L=1,ILEV-1
         DO I=IL1,IL2
            DPF(I,L)=PF(I,L+1)-PF(I,L)
         END DO 
      END DO

      DO L=1,NHAR                    ! Initialize squared amplitudes of
         DO I=IL1,IL2
           USQ(I,L,LEVGWS)=USQL(L) 
         END DO                      ! the spectrum at the bottom, levgw
      END DO                         ! (where wave source is assumed)

C EXTRAWAVES  (c > 0) ......................X-DIRECTION...............

      DO L=1,NHAR
         DO I=IL1,IL2
            OM(I,L)=BVFREQ(I,ILEV+1-LEVGWS)*GWKH/ZKZ(L)
            OMEG(I,L,LEVGWS)=OM(I,L) - GWKH * UU(I,LEVGWS)
         END DO
      END DO

         CALL MKGWD(UU,PF,PH,THROW,TFROW,BVFREQ,DPF,GWKH,RG,
     1           AX,AXX,USQ,SIGMA,ZKZR,OMEG,ALPHA,BETA,
     2           RMS,ILEV,NHAR,LEVGWS,LUPP,ILG,IL1,IL2,
     3           v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

      DO L=1,ILEV                    !   accumulate GW tendency
         DO I=IL1,IL2
            UTENDGW(I,L)=UTENDGW(I,L)+AX(I,ILEV+1-L)
            URMS(I,L)   =RMS(I,ILEV+1-L)
         END DO
      END DO

C INTRAWAVES  (c < 0) .................................................

      DO L=1,NHAR
         DO I=IL1,IL2
            OMEG(I,L,LEVGWS)=-OM(I,L) - GWKH * UU(I,LEVGWS)
         END DO
      END DO

         CALL MKGWD(UU,PF,PH,THROW,TFROW,BVFREQ,DPF,GWKH,RG,
     1           AX,AXX,USQ,SIGMA,ZKZR,OMEG,ALPHA,BETA,
     2           RMS,ILEV,NHAR,LEVGWS,LUPP,ILG,IL1,IL2,
     3           v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

      DO L=1,ILEV                    !   accumulate GW tendency
         DO I=IL1,IL2
            UTENDGW(I,L)=UTENDGW(I,L)+AX(I,ILEV+1-L)
            URMS(I,L)   =URMS(I,L)+RMS(I,ILEV+1-L)
            IF( ABS(UTENDGW(I,L)) .GT. TNDMAX ) THEN
               UTENDGW(I,L)=SIGN(TNDMAX,UTENDGW(I,L))
            ENDIF
         END DO
      END DO  

C EXTRAWAVES  (c > 0) .......................Y-DIRECTION...............

      DO L=1,NHAR
         DO I=IL1,IL2
            OMEG(I,L,LEVGWS)=OM(I,L) - GWKH * VV(I,LEVGWS)
         END DO
      END DO

         CALL MKGWD(VV,PF,PH,THROW,TFROW,BVFREQ,DPF,GWKH,RG,
     1           AX,AXX,USQ,SIGMA,ZKZR,OMEG,ALPHA,BETA,
     2           RMS,ILEV,NHAR,LEVGWS,LUPP,ILG,IL1,IL2,
     3           v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

      DO L=1,ILEV
         DO I=IL1,IL2
            VTENDGW(I,L)=VTENDGW(I,L)+AX(I,ILEV+1-L)
            URMS(I,L)   =URMS(I,L)+RMS(I,ILEV+1-L)
         END DO
      END DO

C INTRAWAVES  (c < 0) .................................................

      DO L=1,NHAR
         DO I=IL1,IL2
            OMEG(I,L,LEVGWS)=-OM(I,L) -  GWKH * VV(I,LEVGWS)
         END DO
      END DO

         CALL MKGWD(VV,PF,PH,THROW,TFROW,BVFREQ,DPF,GWKH,RG,
     1           AX,AXX,USQ,SIGMA,ZKZR,OMEG,ALPHA,BETA,
     2           RMS,ILEV,NHAR,LEVGWS,LUPP,ILG,IL1,IL2,
     3           v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

      DO L=1,ILEV
         DO I=IL1,IL2 
            VTENDGW(I,L)=VTENDGW(I,L)+AX(I,ILEV+1-L)
            URMS(I,L)   =URMS(I,L)+RMS(I,ILEV+1-L)
            IF( ABS(VTENDGW(I,L)) .GT. TNDMAX)  THEN
               VTENDGW(I,L)=SIGN(TNDMAX,VTENDGW(I,L))
            ENDIF
         END DO
      END DO
C
      RETURN
      END
