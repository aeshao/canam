      SUBROUTINE HINES_INTGRL2 (I_ALPHA,V_ALPHA,M_ALPHA,BVFB,
     1                         DRAG,DO_ALPHA,M_MIN,SLOPE,NAZ,LEV,
     2                         IL1,IL2,NLONS,NLEVS,NAZMTH)
C
C  This routine calculates the vertical wavenumber integral
C  for a single vertical level at each azimuth on a longitude grid
C  for the Hines' Doppler spread GWD parameterization scheme.
C  NOTE: the integral is written in terms of the product QM
C        which by definition is always less than 1. Series
C        solutions are used for small |QM| and analytical solutions
C        for remaining values.
C
C  Aug. 8/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Aug. 8/99 - C. McLandress (nonzero M_MIN for all other values of SLOPE;
C                             code for SLOPE=3 added)
C  Feb. 2/96 - C. McLandress (added minimum cutoff wavenumber M_MIN and
C                             logical flags; I_ALPHA < 0 disallowed)
C
C  Output arguements:
C  ------------------
C
C     * I_ALPHA  = Hines' integral.
C     * DRAG     = logical flag indicating longitudes where calculations
C     *            to be performed.
C     * DO_ALPHA = logical flag indicating azimuths and longitudes
C     *            where calculations to be performed (for SLOPE=1).
C
C  Input arguements:
C  -----------------
C
C     * V_ALPHA  = azimuthal wind component (m/s) at this level.
C     * M_ALPHA  = azimuthal cutoff vertical wavenumber (1/m).
C     * BVFB     = background Brunt Vassala frequency at model bottom.
C     * DO_ALPHA = logical flag indicating azimuths and longitudes
C     *            where calculations to be performed.
C     * M_MIN    = minimum allowable cutoff vertical wavenumber.
C     * SLOPE    = slope of initial vertical wavenumber spectrum. 
C     * NAZ      = number of horizontal azimuths used.
C     * LEV      = altitude level to process.
C     * IL1      = first longitudinal index to use (IL1 >= 1).
C     * IL2      = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * NLONS    = number of longitudes.
C     * NLEVS    = number of vertical levels.
C     * NAZMTH   = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Constants in DATA statements:
C  ----------------------------
C
C     * QMIN = minimum value of Q_ALPHA (avoids indeterminant form of integral)
C     * QM_MIN = minimum value of Q_ALPHA * M_ALPHA (used to avoid numerical
C     *          problems).
C
      IMPLICIT NONE
      INTEGER  LEV, NAZ, IL1, IL2, NLONS, NLEVS, NAZMTH
cccc      LOGICAL  DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH)
      INTEGER  DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH)
      REAL    I_ALPHA(NLONS,NAZMTH), V_ALPHA(NLONS,NAZMTH)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    BVFB(NLONS), M_MIN, SLOPE
C
C  Internal variables.
C  -------------------
C
      INTEGER  I, N
      REAL    Q_ALPHA, QM, QMM, SQRTQM, Q_MIN, QM_MIN, IVAL, ZERO
      REAL    SQRTQMM
C
      DATA  Q_MIN / 1.0 /, QM_MIN / 0.01 /, ZERO / 0. /
C-----------------------------------------------------------------------     
C
C  Initialize DRAG array.
C
      DO 5 I = IL1,IL2
        DRAG(I) = 0
 5    CONTINUE
C
C  For integer value SLOPE = 1.
C
      IF (SLOPE .EQ. 1.)  THEN
C
        DO 10 N = 1,NAZ
        DO 10 I = IL1,IL2
C
C  Calculate integral only in regions where cutoff wavenumber
C  is greater than minimum allowable value, otherwise set integral
C  to zero and turn off flag to calculate this azimuth.
C
          IF (M_ALPHA(I,LEV,N).GT.M_MIN)  THEN
C
            DRAG(I) = 1
            Q_ALPHA = V_ALPHA(I,N) / BVFB(I)
            QM      = Q_ALPHA * M_ALPHA(I,LEV,N)
            QMM     = Q_ALPHA * M_MIN 
C
C  If |QM| is small then use first 4 terms in the Taylor series
C  expansion of integral in order to avoid indeterminate form of integral,
C  otherwise use analytical form of integral.
C
            IF (ABS(Q_ALPHA).LT.Q_MIN .OR. ABS(QM).LT.QM_MIN)  THEN  
              IF (Q_ALPHA .EQ. ZERO)  THEN
                IVAL = ( M_ALPHA(I,LEV,N)**2 - M_MIN**2 ) / 2.
              ELSE
                IVAL = ( QM**2/2.  + QM**3/3.  + QM**4/4.  + QM**5/5. 
     ^                 - QMM**2/2. - QMM**3/3. - QMM**4/4. - QMM**5/5. )
     ^                 / Q_ALPHA**2
              END IF
            ELSE
              IVAL = - ( LOG((1.-QM)/(1.-QMM)) + QM - QMM ) 
     ^                 / Q_ALPHA**2
            END IF
C
C  If I_ALPHA negative (due to round off error) then set it to zero.
C
            I_ALPHA(I,N) = MAX ( IVAL, ZERO )
C
          ELSE
C
            I_ALPHA(I,N)  = ZERO
            DO_ALPHA(I,N) = 0
C
          END IF
C
 10     CONTINUE
C
      END IF
C
C  For integer value SLOPE = 2.
C
      IF (SLOPE .EQ. 2.)  THEN
C
        DO 20 N = 1,NAZ
        DO 20 I = IL1,IL2
C
          IF (M_ALPHA(I,LEV,N).GT.M_MIN)  THEN
C
            DRAG(I) = 1
            Q_ALPHA = V_ALPHA(I,N) / BVFB(I)
            QM      = Q_ALPHA * M_ALPHA(I,LEV,N)
            QMM     = Q_ALPHA * M_MIN 
C
C  If |QM| is small then use first 4 terms in the Taylor series
C  expansion of integral in order to avoid indeterminate form of integral,
C  otherwise use analytical form of integral.
C
            IF (ABS(Q_ALPHA).LT.Q_MIN .OR. ABS(QM).LT.QM_MIN)  THEN  
              IF (Q_ALPHA .EQ. ZERO)  THEN
                IVAL = ( M_ALPHA(I,LEV,N)**3 - M_MIN**3 ) / 3.
              ELSE
                IVAL = ( QM**3/3.  + QM**4/4.  + QM**5/5.  + QM**6/6.  
     ^                 - QMM**3/3. - QMM**4/4. - QMM**5/5. - QMM**6/6. )
     ^                 / Q_ALPHA**3
              END IF
            ELSE
              IVAL = - ( LOG((1.-QM)/(1.-QMM)) + QM - QMM
     ^                   + QM**2/2. - QMM**2/2. ) / Q_ALPHA**3
            END IF
C
C  If I_ALPHA negative (due to round off error) then set it to zero.
C
            I_ALPHA(I,N) = MAX ( IVAL, ZERO )
C
          ELSE
C
            I_ALPHA(I,N) = ZERO
            DO_ALPHA(I,N) = 0
C
          END IF
C
 20     CONTINUE
C
      END IF
C
C  For integer value SLOPE = 3.
C
      IF (SLOPE .EQ. 3.)  THEN
C
        DO 25 N = 1,NAZ
        DO 25 I = IL1,IL2
C
          IF (M_ALPHA(I,LEV,N).GT.M_MIN)  THEN
C
            DRAG(I) = 1
            Q_ALPHA = V_ALPHA(I,N) / BVFB(I)
            QM      = Q_ALPHA * M_ALPHA(I,LEV,N)
            QMM     = Q_ALPHA * M_MIN 
C
C  If |QM| is small then use first 4 terms in the Taylor series
C  expansion of integral in order to avoid indeterminate form of integral,
C  otherwise use analytical form of integral.
C
            IF (ABS(Q_ALPHA).LT.Q_MIN .OR. ABS(QM).LT.QM_MIN)  THEN  
              IF (Q_ALPHA .EQ. ZERO)  THEN
                IVAL = ( M_ALPHA(I,LEV,N)**4 - M_MIN**4 ) / 4.
              ELSE
                IVAL = ( QM**4/4.  + QM**5/5.  + QM**6/6.  + QM**7/7.  
     ^                 - QMM**4/4. - QMM**5/5. - QMM**6/6. - QMM**7/7. )
     ^                 / Q_ALPHA**4
              END IF
            ELSE
              IVAL = - ( LOG((1.-QM)/(1.-QMM)) + QM - QMM 
     ^               + QM**2/2. - QMM**2/2. + QM**3/3. - QMM**3/3. ) 
     ^                 / Q_ALPHA**4
            END IF
C
C  If I_ALPHA negative (due to round off error) then set it to zero.
C
            I_ALPHA(I,N) = MAX ( IVAL, ZERO )
C
          ELSE
C
            I_ALPHA(I,N) = ZERO
            DO_ALPHA(I,N) = 0
C
          END IF
C
 25     CONTINUE
C
      END IF
C
C  For real value SLOPE = 1.5.
C
      IF (SLOPE .EQ. 1.5)  THEN
C
        DO 30 N = 1,NAZ
        DO 30 I = IL1,IL2
C
          IF (M_ALPHA(I,LEV,N).GT.M_MIN)  THEN
C
            DRAG(I) = 1
            Q_ALPHA = V_ALPHA(I,N) / BVFB(I)
            QM      = Q_ALPHA * M_ALPHA(I,LEV,N)       
            QMM     = Q_ALPHA * M_MIN 
C
C  If |QM| is small then use first 4 terms in the Taylor series
C  expansion of integral in order to avoid indeterminate form of integral,
C  otherwise use analytical form of integral.
C
            IF (ABS(Q_ALPHA).LT.Q_MIN .OR. ABS(QM).LT.QM_MIN)  THEN  
              IF (Q_ALPHA .EQ. ZERO)  THEN
                IVAL = ( M_ALPHA(I,LEV,N)**2.5 - M_MIN**2.5 ) / 2.5
              ELSE
                IVAL = ( ( QM/2.5  + QM**2/3.5 + QM**3/4.5 + QM**4/5.5 )
     ^             * M_ALPHA(I,LEV,N)**1.5
     ^             - ( QMM/2.5 + QMM**2/3.5 + QMM**3/4.5 + QMM**4/5.5 ) 
     ^             * M_MIN**1.5 ) / Q_ALPHA
              END IF
            ELSE
              QM      = ABS(QM)
              QMM     = ABS(QMM)
              SQRTQM  = SQRT(QM)
              SQRTQMM = SQRT(QMM)
              IF (Q_ALPHA .GE. ZERO)  THEN
                IVAL = ( LOG( (1.+SQRTQM)/(1.-SQRTQM) )
     ^                 - LOG( (1.+SQRTQMM)/(1.-SQRTQMM) )
     ^                 - 2.*SQRTQM*(1.+QM/3.)
     ^                 + 2.*SQRTQMM*(1.+QMM/3.) ) / Q_ALPHA**2.5
              ELSE
                IVAL = 2. * ( ATAN(SQRTQM) - ATAN(SQRTQMM)
     ^                 + SQRTQM*(QM/3.-1.) - SQRTQMM*(QMM/3.-1.) )
     ^                 / ABS(Q_ALPHA)**2.5
              END IF
            END IF
C
C  If I_ALPHA negative (due to round off error) then set it to zero.
C
            I_ALPHA(I,N) = MAX ( IVAL, ZERO )
C
          ELSE
C
            I_ALPHA(I,N) = ZERO
            DO_ALPHA(I,N) = 0
C
          END IF
C
 30     CONTINUE
C
      END IF
C
      RETURN
C-----------------------------------------------------------------------
      END
