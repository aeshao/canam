      SUBROUTINE HINES_SIGMA (SIGMA_T,SIGMA_ALPHA,SIGSQH_ALPHA,DRAG,
     1                        NAZ,LEV,IL1,IL2,NLONS,NLEVS,NAZMTH)
C
C  This routine calculates the total rms and azimuthal rms horizontal 
C  velocities at a given level on a longitude by altitude grid for 
C  the Hines' Doppler spread GWD parameterization scheme.
C  NOTE: only 4, 8, 12 or 16 azimuths can be used.
C
C  Aug. 7/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Feb. 2/96 - C. McLandress (added: 12 and 16 azimuths; logical flags) 
C

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Output arguements:
C  ------------------
C
C     * SIGMA_T     = total rms horizontal wind (m/s).
C     * SIGMA_ALPHA = total rms wind in each azimuth (m/s).
C
C  Input arguements:
C  -----------------
C
C     * SIGSQH_ALPHA = portion of wind variance from waves having wave
C     *                normals in the alpha azimuth (m/s).
C     * DRAG      = logical flag indicating longitudes where calculations
C     *             to be performed.
C     * NAZ       = number of horizontal azimuths used (4, 8, 12 or 16).
C     * LEV       = altitude level to process.
C     * IL1       = first longitudinal index to use (IL1 >= 1).
C     * IL2       = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * NLONS     = number of longitudes.
C     * NLEVS     = number of vertical levels.
C     * NAZMTH    = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Subroutine arguements.
C  ---------------------
C
      INTEGER  LEV, NAZ, IL1, IL2
      INTEGER  NLONS, NLEVS, NAZMTH
cccc      LOGICAL  DRAG(NLONS)
      INTEGER  DRAG(NLONS)
      REAL    SIGMA_T(NLONS,NLEVS)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGSQH_ALPHA(NLONS,NLEVS,NAZMTH)
C
C  Internal variables.
C  -------------------
C
      INTEGER  I, N
      REAL    SUM_2468,   SUM_1357 
      REAL    SUM_26812,  SUM_35911,  SUM_1379
      REAL    SUM_461012, SUM_24810,  SUM_15711  
      REAL    SUM_281016, SUM_371115, SUM_461214, SUM_13911  
      REAL    SUM_481216, SUM_571315, SUM_241012, SUM_15913
      REAL    SUM_681416, SUM_351113, SUM_261014, SUM_17915
      REAL    C22SQ, C67SQ
      DATA  C22SQ / 0.8535534 /, C67SQ / 0.1464466 /  
C-----------------------------------------------------------------------     
C
C  Calculate azimuthal rms velocity for the 4 azimuth case.
C
      IF (NAZ.EQ.4)  THEN
        DO 10 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            SIGMA_ALPHA(I,LEV,1) = SQRT ( SIGSQH_ALPHA(I,LEV,1)
     ^                                  + SIGSQH_ALPHA(I,LEV,3) )
            SIGMA_ALPHA(I,LEV,2) = SQRT ( SIGSQH_ALPHA(I,LEV,2)
     ^                                  + SIGSQH_ALPHA(I,LEV,4) )
            SIGMA_ALPHA(I,LEV,3) = SIGMA_ALPHA(I,LEV,1)
            SIGMA_ALPHA(I,LEV,4) = SIGMA_ALPHA(I,LEV,2)
          END IF
 10     CONTINUE
      END IF
C
C  Calculate azimuthal rms velocity for the 8 azimuth case.
C
      IF (NAZ.EQ.8)  THEN
        DO 20 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            SUM_1357 = SIGSQH_ALPHA(I,LEV,1) + SIGSQH_ALPHA(I,LEV,3) 
     ^               + SIGSQH_ALPHA(I,LEV,5) + SIGSQH_ALPHA(I,LEV,7) 
            SUM_2468 = SIGSQH_ALPHA(I,LEV,2) + SIGSQH_ALPHA(I,LEV,4)
     ^               + SIGSQH_ALPHA(I,LEV,6) + SIGSQH_ALPHA(I,LEV,8)
            SIGMA_ALPHA(I,LEV,1) = SQRT ( SIGSQH_ALPHA(I,LEV,1) 
     ^                           + SIGSQH_ALPHA(I,LEV,5) + SUM_2468/2. )
            SIGMA_ALPHA(I,LEV,2) = SQRT ( SIGSQH_ALPHA(I,LEV,2) 
     ^                           + SIGSQH_ALPHA(I,LEV,6) + SUM_1357/2. )
            SIGMA_ALPHA(I,LEV,3) = SQRT ( SIGSQH_ALPHA(I,LEV,3) 
     ^                           + SIGSQH_ALPHA(I,LEV,7) + SUM_2468/2. )
            SIGMA_ALPHA(I,LEV,4) = SQRT ( SIGSQH_ALPHA(I,LEV,4) 
     ^                           + SIGSQH_ALPHA(I,LEV,8) + SUM_1357/2. )
            SIGMA_ALPHA(I,LEV,5) = SIGMA_ALPHA(I,LEV,1)
            SIGMA_ALPHA(I,LEV,6) = SIGMA_ALPHA(I,LEV,2)
            SIGMA_ALPHA(I,LEV,7) = SIGMA_ALPHA(I,LEV,3)
            SIGMA_ALPHA(I,LEV,8) = SIGMA_ALPHA(I,LEV,4)
          END IF
 20     CONTINUE
      END IF
C
C  Calculate azimuthal rms velocity for the 12 azimuth case.
C
      IF (NAZ.EQ.12)  THEN
        DO 30 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            SUM_26812  = SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV, 6)
     ^                 + SIGSQH_ALPHA(I,LEV, 8) + SIGSQH_ALPHA(I,LEV,12)
            SUM_35911  = SIGSQH_ALPHA(I,LEV, 3) + SIGSQH_ALPHA(I,LEV, 5)
     ^                 + SIGSQH_ALPHA(I,LEV, 9) + SIGSQH_ALPHA(I,LEV,11)
            SUM_1379   = SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 3)
     ^                 + SIGSQH_ALPHA(I,LEV, 7) + SIGSQH_ALPHA(I,LEV, 9)
            SUM_461012 = SIGSQH_ALPHA(I,LEV, 4) + SIGSQH_ALPHA(I,LEV, 6)
     ^                 + SIGSQH_ALPHA(I,LEV,10) + SIGSQH_ALPHA(I,LEV,12)
            SUM_24810  = SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV, 4)
     ^                 + SIGSQH_ALPHA(I,LEV, 8) + SIGSQH_ALPHA(I,LEV,10)
            SUM_15711  = SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 5)
     ^                 + SIGSQH_ALPHA(I,LEV, 7) + SIGSQH_ALPHA(I,LEV,11)
            SIGMA_ALPHA(I,LEV,1)  = SQRT ( SIGSQH_ALPHA(I,LEV,1) 
     ^                            + SIGSQH_ALPHA(I,LEV,7) 
     ^                            + 0.75*SUM_26812 + 0.25*SUM_35911 )
            SIGMA_ALPHA(I,LEV,2)  = SQRT ( SIGSQH_ALPHA(I,LEV,2) 
     ^                            + SIGSQH_ALPHA(I,LEV,8) 
     ^                            + 0.75*SUM_1379 + 0.25*SUM_461012 )
            SIGMA_ALPHA(I,LEV,3)  = SQRT ( SIGSQH_ALPHA(I,LEV,3) 
     ^                            + SIGSQH_ALPHA(I,LEV,9) 
     ^                            + 0.75*SUM_24810 + 0.25*SUM_15711 )
            SIGMA_ALPHA(I,LEV,4)  = SQRT ( SIGSQH_ALPHA(I,LEV,4) 
     ^                            + SIGSQH_ALPHA(I,LEV,10) 
     ^                            + 0.75*SUM_35911 + 0.25*SUM_26812 )
            SIGMA_ALPHA(I,LEV,5)  = SQRT ( SIGSQH_ALPHA(I,LEV,5) 
     ^                            + SIGSQH_ALPHA(I,LEV,11) 
     ^                            + 0.75*SUM_461012 + 0.25*SUM_1379 )
            SIGMA_ALPHA(I,LEV,6)  = SQRT ( SIGSQH_ALPHA(I,LEV,6) 
     ^                            + SIGSQH_ALPHA(I,LEV,12) 
     ^                            + 0.75*SUM_15711 + 0.25*SUM_24810 )
            SIGMA_ALPHA(I,LEV,7)  = SIGMA_ALPHA(I,LEV,1)
            SIGMA_ALPHA(I,LEV,8)  = SIGMA_ALPHA(I,LEV,2)
            SIGMA_ALPHA(I,LEV,9)  = SIGMA_ALPHA(I,LEV,3)
            SIGMA_ALPHA(I,LEV,10) = SIGMA_ALPHA(I,LEV,4)
            SIGMA_ALPHA(I,LEV,11) = SIGMA_ALPHA(I,LEV,5)
            SIGMA_ALPHA(I,LEV,12) = SIGMA_ALPHA(I,LEV,6)
          END IF
 30    CONTINUE
      END IF
C
C  Calculate azimuthal rms velocity for the 16 azimuth case.
C
      IF (NAZ.EQ.16)  THEN
        DO 40 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            SUM_281016 = SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV, 8)
     ^                 + SIGSQH_ALPHA(I,LEV,10) + SIGSQH_ALPHA(I,LEV,16)
            SUM_371115 = SIGSQH_ALPHA(I,LEV, 3) + SIGSQH_ALPHA(I,LEV, 7)
     ^                 + SIGSQH_ALPHA(I,LEV,11) + SIGSQH_ALPHA(I,LEV,15)
            SUM_461214 = SIGSQH_ALPHA(I,LEV, 4) + SIGSQH_ALPHA(I,LEV, 6)
     ^                 + SIGSQH_ALPHA(I,LEV,12) + SIGSQH_ALPHA(I,LEV,14)
            SUM_13911  = SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 3)
     ^                 + SIGSQH_ALPHA(I,LEV, 9) + SIGSQH_ALPHA(I,LEV,11)
            SUM_481216 = SIGSQH_ALPHA(I,LEV, 4) + SIGSQH_ALPHA(I,LEV, 8)
     ^                 + SIGSQH_ALPHA(I,LEV,12) + SIGSQH_ALPHA(I,LEV,16)
            SUM_571315 = SIGSQH_ALPHA(I,LEV, 5) + SIGSQH_ALPHA(I,LEV, 7)
     ^                 + SIGSQH_ALPHA(I,LEV,13) + SIGSQH_ALPHA(I,LEV,15)
            SUM_241012 = SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV, 4)
     ^                 + SIGSQH_ALPHA(I,LEV,10) + SIGSQH_ALPHA(I,LEV,12)
            SUM_15913  = SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 5)
     ^                 + SIGSQH_ALPHA(I,LEV, 9) + SIGSQH_ALPHA(I,LEV,13)
            SUM_681416 = SIGSQH_ALPHA(I,LEV, 6) + SIGSQH_ALPHA(I,LEV, 8)
     ^                 + SIGSQH_ALPHA(I,LEV,14) + SIGSQH_ALPHA(I,LEV,16)
            SUM_351113 = SIGSQH_ALPHA(I,LEV, 3) + SIGSQH_ALPHA(I,LEV, 5)
     ^                 + SIGSQH_ALPHA(I,LEV,11) + SIGSQH_ALPHA(I,LEV,13)
            SUM_261014 = SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV, 6)
     ^                 + SIGSQH_ALPHA(I,LEV,10) + SIGSQH_ALPHA(I,LEV,14)
            SUM_17915  = SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 7)
     ^                 + SIGSQH_ALPHA(I,LEV, 9) + SIGSQH_ALPHA(I,LEV,15)
            SIGMA_ALPHA(I,LEV,1)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 1) + SIGSQH_ALPHA(I,LEV, 9)
     ^                 + C22SQ * SUM_281016     + 0.5 * SUM_371115 
     ^                 + C67SQ * SUM_461214 ) 
            SIGMA_ALPHA(I,LEV,2)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 2) + SIGSQH_ALPHA(I,LEV,10)
     ^                 + C22SQ * SUM_13911      + 0.5 * SUM_481216 
     ^                 + C67SQ * SUM_571315 ) 
            SIGMA_ALPHA(I,LEV,3)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 3) + SIGSQH_ALPHA(I,LEV,11)
     ^                 + C22SQ * SUM_241012     + 0.5 * SUM_15913
     ^                 + C67SQ * SUM_681416 ) 
            SIGMA_ALPHA(I,LEV,4)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 4) + SIGSQH_ALPHA(I,LEV,12)
     ^                 + C22SQ * SUM_351113     + 0.5 * SUM_261014
     ^                 + C67SQ * SUM_17915 ) 
            SIGMA_ALPHA(I,LEV,5)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 5) + SIGSQH_ALPHA(I,LEV,13)
     ^                 + C22SQ * SUM_461214     + 0.5 * SUM_371115
     ^                 + C67SQ * SUM_281016 ) 
            SIGMA_ALPHA(I,LEV,6)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 6) + SIGSQH_ALPHA(I,LEV,14)
     ^                 + C22SQ * SUM_571315     + 0.5 * SUM_481216
     ^                 + C67SQ * SUM_13911 ) 
            SIGMA_ALPHA(I,LEV,7)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 7) + SIGSQH_ALPHA(I,LEV,15)
     ^                 + C22SQ * SUM_681416     + 0.5 * SUM_15913
     ^                 + C67SQ * SUM_241012 ) 
            SIGMA_ALPHA(I,LEV,8)  = SQRT (  
     ^                   SIGSQH_ALPHA(I,LEV, 8) + SIGSQH_ALPHA(I,LEV,16)
     ^                 + C22SQ * SUM_17915      + 0.5 * SUM_261014
     ^                 + C67SQ * SUM_351113 ) 
            SIGMA_ALPHA(I,LEV,9)  = SIGMA_ALPHA(I,LEV,1)
            SIGMA_ALPHA(I,LEV,10) = SIGMA_ALPHA(I,LEV,2)
            SIGMA_ALPHA(I,LEV,11) = SIGMA_ALPHA(I,LEV,3)
            SIGMA_ALPHA(I,LEV,12) = SIGMA_ALPHA(I,LEV,4)
            SIGMA_ALPHA(I,LEV,13) = SIGMA_ALPHA(I,LEV,5)
            SIGMA_ALPHA(I,LEV,14) = SIGMA_ALPHA(I,LEV,6)
            SIGMA_ALPHA(I,LEV,15) = SIGMA_ALPHA(I,LEV,7)
            SIGMA_ALPHA(I,LEV,16) = SIGMA_ALPHA(I,LEV,8)
          END IF
 40    CONTINUE
      END IF
C
C  Initialize rms wind.
C
      DO 50 I = IL1,IL2
        SIGMA_T(I,LEV) = 0.
 50   CONTINUE
C
C  Calculate total rms wind.
C
      DO 60 N = 1,NAZ
      DO 60 I = IL1,IL2
        IF (DRAG(I).eq.1)  THEN
          SIGMA_T(I,LEV) = SIGMA_T(I,LEV) + SIGSQH_ALPHA(I,LEV,N)
        END IF   
 60   CONTINUE
      DO 70 I = IL1,IL2
        IF (DRAG(I).eq.1)  THEN
          SIGMA_T(I,LEV) = SQRT ( SIGMA_T(I,LEV) )
        END IF   
 70   CONTINUE
C
      RETURN
C-----------------------------------------------------------------------     
      END
