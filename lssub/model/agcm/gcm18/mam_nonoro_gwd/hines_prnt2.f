      SUBROUTINE HINES_PRNT2 (M_ALPHA,SIGMA_ALPHA,VEL_U,VEL_V,
     1                        UBOT,VBOT,ALT,DRAGIL,V_ALPHA,
     2                        NMESSG,ILPRT1,ILPRT2,LEVPRT1,LEVPRT2,
     3                        NAZ,NLONS,NLEVS,NAZMTH,work)
C
C  Print out altitude profiles of cutoff wavenumbers, rms winds and
C  background winds at each horizontal azimuth for the Hines Doppler spread 
C  gravity wave parameterization scheme.
C
C  Feb. 2/96 - C. McLandress
C

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Input arguements:
C  -----------------
C
C     * M_ALPHA      = cutoff wavenumber at each azimuth (1/m).
C     * SIGMA_ALPHA  = total rms wind in each azimuth (m/s).
C     * VEL_U    = background zonal wind component (m/s).
C     * VEL_V    = background meridional wind component (m/s).
C     * UBOT     = background zonal wind component at bottom level.
C     * VBOT     = background meridional wind component at bottom level.
C     * ALT      = altitude (m).
C     * DRAGIL   = logical flag indicating longitudes and levels where 
C     *            calculations to be performed.
C     * NMESSG   = unit number for printed output.
C     * ILPRT1   = first longitudinal index to print.
C     * ILPRT2   = last longitudinal index to print.
C     * LEVPRT1  = first altitude level to print.
C     * LEVPRT2  = last altitude level to print.
C     * NAZ      = actual number of horizontal azimuths used.
C     * NLONS    = number of longitudes.
C     * NLEVS    = number of vertical levels.
C     * NAZMTH   = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Input work arrays:
C  ------------------
C
C     * V_ALPHA  = wind component at each azimuth (m/s). 
C
      INTEGER  NAZ, ILPRT1, ILPRT2, LEVPRT1, LEVPRT2, NMESSG
      INTEGER  NLONS, NLEVS, NAZMTH
cccc      LOGICAL DRAGIL(NLONS,NLEVS)
      INTEGER DRAGIL(NLONS,NLEVS)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    VEL_U(NLONS,NLEVS),    VEL_V(NLONS,NLEVS)
      REAL    ALT(NLONS,NLEVS), UBOT(NLONS),  VBOT(NLONS)
      REAL    V_ALPHA(NLONS,NAZMTH)
C
C Internal variables.
C -------------------
C
      INTEGER  IBIG, I, L, N, NAZ1
cccc      PARAMETER  ( IBIG = 50 )
cccc      REAL  ZKM, WORK(IBIG)
      real work(nazmth)
C-----------------------------------------------------------------------  

      NAZ1 = NAZ
      IF (NAZ.GT.12)  NAZ1 = 12
C
C  Print out values for range of longitudes.
C
      DO 100 I = ILPRT1,ILPRT2
C
C Print cutoff wavenumber at all azimuths.
C
        WRITE (NMESSG,*) 
        WRITE (NMESSG,6001) I
        WRITE (NMESSG,*) 
 6001   FORMAT ('Cutoff wavenumber (X 1.E3) at longitude I =',I3)
        DO 10 L = LEVPRT1,LEVPRT2
          ZKM = ALT(I,L)/1.E3
          DO 5 N = 1,NAZ1
            WORK(N) = M_ALPHA(I,L,N) * 1.E3
  5       CONTINUE
          WRITE (NMESSG,6100) ZKM, (WORK(N),N=1,NAZ1)
 10     CONTINUE
        IF (NAZ.GT.12)  THEN
          DO 11 L = LEVPRT1,LEVPRT2
            ZKM = ALT(I,L)/1.E3
            DO 6 N = 13,NAZ
              WORK(N) = M_ALPHA(I,L,N) * 1.E3
  6         CONTINUE
            WRITE (NMESSG,6100) ZKM, (WORK(N),N=13,NAZ)
 11       CONTINUE
        END IF
        WRITE (NMESSG,*) 
 6100   FORMAT (F5.1,'km',12F6.2)
C
C Print rms wind at all azimuths.
C
        WRITE (NMESSG,*) 
        WRITE (NMESSG,6002) I
        WRITE (NMESSG,*) 
 6002   FORMAT ('RMS wind (m/s) at longitude I =',I3)
        DO 20 L = LEVPRT1,LEVPRT2
          ZKM = ALT(I,L)/1.E3
          WRITE (NMESSG,6110) ZKM, (SIGMA_ALPHA(I,L,N),N=1,NAZ1)
 20     CONTINUE
        IF (NAZ.GT.12)  THEN
          DO 21 L = LEVPRT1,LEVPRT2
            ZKM = ALT(I,L)/1.E3
            WRITE (NMESSG,6110) ZKM, (SIGMA_ALPHA(I,L,N),N=13,NAZ)
 21       CONTINUE
        END IF
        WRITE (NMESSG,*) 
 6110   FORMAT (F5.1,'km',12F6.1)
C
C Print background wind at all azimuths.
C
        WRITE (NMESSG,*) 
        WRITE (NMESSG,6003) I
        WRITE (NMESSG,*) 
 6003   FORMAT ('Background wind (m/s) at longitude I =',I3)
        DO 30 L = LEVPRT1,LEVPRT2
          ZKM = ALT(I,L)/1.E3
          CALL HINES_WIND ( V_ALPHA, 
     ^                      VEL_U(1,L), VEL_V(1,L), UBOT, VBOT, 
     ^                      DRAGIL(1,L), NAZ, I, I, NLONS, NAZMTH )
          WRITE (NMESSG,6110) ZKM, (V_ALPHA(I,N),N=1,NAZ1)
 30     CONTINUE
        IF (NAZ.GT.12)  THEN
          DO 31 L = LEVPRT1,LEVPRT2
            ZKM = ALT(I,L)/1.E3
            CALL HINES_WIND ( V_ALPHA, 
     ^                        VEL_U(1,L), VEL_V(1,L), UBOT, VBOT, 
     ^                        DRAGIL(1,L), NAZ, I, I, NLONS, NAZMTH )
            WRITE (NMESSG,6110) ZKM, (V_ALPHA(I,N),N=13,NAZ)
 31       CONTINUE
        END IF
C
 100  CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
