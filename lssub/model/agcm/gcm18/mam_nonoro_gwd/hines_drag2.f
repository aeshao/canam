      SUBROUTINE HINES_DRAG2 (FLUX_U,FLUX_V,DRAG_U,DRAG_V,ALT,DENSITY,
     1                       DENSB,M_ALPHA,SPECFAC,DRAGIL,M_MIN,SLOPE,
     2                       NAZ,IL1,IL2,LEV1,LEV2,NLONS,NLEVS,NAZMTH)
C
C  Calculate zonal and meridional components of the vertical flux 
C  of horizontal momentum and corresponding wave drag (force per unit mass)
C  on a longitude by altitude grid needed for the Hines Doppler spread 
C  gravity wave parameterization scheme.
C
C  Aug. 6/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Aug. 8/99 - C. McLandress (nonzero M_MIN for all other values of SLOPE)
C  Feb. 2/96 - C. McLandress (added: minimum cutoff wavenumber M_MIN;
C                             12 and 16 azimuths; logical flags)
C
C  Output arguements:
C  ------------------
C
C     * FLUX_U  = zonal component of vertical momentum flux (Pascals)
C     * FLUX_V  = meridional component of vertical momentum flux (Pascals)
C     * DRAG_U  = zonal component of drag (m/s^2).
C     * DRAG_V  = meridional component of drag (m/s^2).
C
C  Input arguements:
C  -----------------
C
C     * ALT     = altitudes (m).
C     * DENSITY = background density (kg/m^3).
C     * DENSB   = background density at bottom level (kg/m^3).
C     * M_ALPHA = cutoff vertical wavenumber (1/m).
C     * SPECFAC = AK_ALPHA * K_ALPHA.
C     * DRAGIL  = longitudes and levels where drag to be calculated.
C     * DRAGIL  = logical flag indicating longitudes and levels where 
C     *           calculations to be performed.
C     * M_MIN   = minimum allowable cutoff vertical wavenumber.
C     * SLOPE   = slope of incident vertical wavenumber spectrum.
C     * NAZ     = number of horizontal azimuths used (4, 8, 12 or 16).
C     * IL1     = first longitudinal index to use (IL1 >= 1).
C     * IL2     = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEV1    = first altitude level to use (LEV1 >=1). 
C     * LEV2    = last altitude level to use (LEV1 < LEV2 <= NLEVS).
C     * NLONS   = number of longitudes.
C     * NLEVS   = number of vertical levels.
C     * NAZMTH  = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Constants in DATA statement.
C  ----------------------------
C
C     * COS45 = cosine of 45 degrees. 
C     * COS30 = cosine of 30 degrees. 
C     * SIN30 = sine of 30 degrees.
C     * COS22 = cosine of 22.5 degrees. 
C     * SIN22 = sine of 22.5 degrees. 
C
C  Subroutine arguements.
C  ----------------------
C
      IMPLICIT NONE
      INTEGER  NAZ, IL1, IL2, LEV1, LEV2
      INTEGER  NLONS, NLEVS, NAZMTH
cccc      LOGICAL  DRAGIL(NLONS,NLEVS)
      INTEGER  DRAGIL(NLONS,NLEVS)
      REAL    M_MIN, SLOPE
      REAL    FLUX_U(NLONS,NLEVS), FLUX_V(NLONS,NLEVS)
      REAL    DRAG_U(NLONS,NLEVS), DRAG_V(NLONS,NLEVS)
      REAL    ALT(NLONS,NLEVS),    DENSITY(NLONS,NLEVS), DENSB(NLONS)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SPECFAC(NLONS,NAZMTH)
C
C  Internal variables.
C  -------------------
C
      INTEGER  I, L, LEV1P, LEV2M
      REAL    COS45, COS30, SIN30, COS22, SIN22, DDZ, DDZ2, ZERO, MMS
      REAL    FLUX2,  FLUX3,  FLUX4,  FLUX5,  FLUX6,  FLUX7,  FLUX8
      REAL    FLUX9,  FLUX10, FLUX11, FLUX12, FLUX14, FLUX15, FLUX16
      DATA  COS45 / 0.7071068 /
      DATA  COS30 / 0.8660254 /, SIN30 / 0.5       /
      DATA  COS22 / 0.9238795 /, SIN22 / 0.3826834 /
      DATA  ZERO / 0. /   
C-----------------------------------------------------------------------
C
      LEV1P = LEV1 + 1
      LEV2M = LEV2 - 1
C
C  Sum over azimuths for case where SLOPE = 1 (NOTE: this is the only
C  value of the slope for which a nonzero M_MIN is used).
C
      IF (SLOPE.EQ.1.)  THEN
C
C  Case with 4 azimuths.
C
        IF (NAZ.EQ.4)  THEN
          DO 10 L = LEV1,LEV2
          DO 10 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX_U(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,1) * ( M_ALPHA(I,L,1) - M_MIN )
     ^                    - SPECFAC(I,3) * ( M_ALPHA(I,L,3) - M_MIN ) )
              FLUX_V(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,2) * ( M_ALPHA(I,L,2) - M_MIN )
     ^                    - SPECFAC(I,4) * ( M_ALPHA(I,L,4) - M_MIN ) )
            END IF
 10       CONTINUE
        END IF
C
C  Case with 8 azimuths.
C
        IF (NAZ.EQ.8)  THEN
          DO 20 L = LEV1,LEV2
          DO 20 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2 = SPECFAC(I,2) * ( M_ALPHA(I,L,2) - M_MIN )
              FLUX4 = SPECFAC(I,4) * ( M_ALPHA(I,L,4) - M_MIN )
              FLUX6 = SPECFAC(I,6) * ( M_ALPHA(I,L,6) - M_MIN )
              FLUX8 = SPECFAC(I,8) * ( M_ALPHA(I,L,8) - M_MIN )
              FLUX_U(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,1) * ( M_ALPHA(I,L,1) - M_MIN )
     ^                    - SPECFAC(I,5) * ( M_ALPHA(I,L,5) - M_MIN )
     ^                    + COS45 * ( FLUX2 - FLUX4 - FLUX6 + FLUX8 ) )
              FLUX_V(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,3) * ( M_ALPHA(I,L,3) - M_MIN )
     ^                    - SPECFAC(I,7) * ( M_ALPHA(I,L,7) - M_MIN )
     ^                    + COS45 * ( FLUX2 + FLUX4 - FLUX6 - FLUX8 ) )
            END IF
 20       CONTINUE
        END IF
C
C  Case with 12 azimuths.
C
        IF (NAZ.EQ.12)  THEN
          DO 30 L = LEV1,LEV2
          DO 30 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2  = SPECFAC(I,2)  * ( M_ALPHA(I,L,2)  - M_MIN )
              FLUX3  = SPECFAC(I,3)  * ( M_ALPHA(I,L,3)  - M_MIN )
              FLUX5  = SPECFAC(I,5)  * ( M_ALPHA(I,L,5)  - M_MIN )
              FLUX6  = SPECFAC(I,6)  * ( M_ALPHA(I,L,6)  - M_MIN )
              FLUX8  = SPECFAC(I,8)  * ( M_ALPHA(I,L,8)  - M_MIN )
              FLUX9  = SPECFAC(I,9)  * ( M_ALPHA(I,L,9)  - M_MIN )
              FLUX11 = SPECFAC(I,11) * ( M_ALPHA(I,L,11) - M_MIN )
              FLUX12 = SPECFAC(I,12) * ( M_ALPHA(I,L,12) - M_MIN )
              FLUX_U(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,1) * ( M_ALPHA(I,L,1) - M_MIN )
     ^                    - SPECFAC(I,7) * ( M_ALPHA(I,L,7) - M_MIN )
     ^                    + COS30 * ( FLUX2 - FLUX6 - FLUX8 + FLUX12 ) 
     ^                    + SIN30 * ( FLUX3 - FLUX5 - FLUX9 + FLUX11 ) )
              FLUX_V(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,4)  * ( M_ALPHA(I,L,4)  - M_MIN )
     ^                    - SPECFAC(I,10) * ( M_ALPHA(I,L,10) - M_MIN )
     ^                    + COS30 * ( FLUX3 + FLUX5 - FLUX9 - FLUX11 ) 
     ^                    + SIN30 * ( FLUX2 + FLUX6 - FLUX8 - FLUX12 ) )
            END IF
 30       CONTINUE
        END IF
C
C  Case with 16 azimuths.
C
        IF (NAZ.EQ.16)  THEN
          DO 40 L = LEV1,LEV2
          DO 40 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2  = SPECFAC(I,2)  * ( M_ALPHA(I,L,2)  - M_MIN )
              FLUX3  = SPECFAC(I,3)  * ( M_ALPHA(I,L,3)  - M_MIN )
              FLUX4  = SPECFAC(I,4)  * ( M_ALPHA(I,L,4)  - M_MIN )
              FLUX6  = SPECFAC(I,6)  * ( M_ALPHA(I,L,6)  - M_MIN )
              FLUX7  = SPECFAC(I,7)  * ( M_ALPHA(I,L,7)  - M_MIN )
              FLUX8  = SPECFAC(I,8)  * ( M_ALPHA(I,L,8)  - M_MIN )
              FLUX10 = SPECFAC(I,10) * ( M_ALPHA(I,L,10) - M_MIN )
              FLUX11 = SPECFAC(I,11) * ( M_ALPHA(I,L,11) - M_MIN )
              FLUX12 = SPECFAC(I,12) * ( M_ALPHA(I,L,12) - M_MIN )
              FLUX14 = SPECFAC(I,14) * ( M_ALPHA(I,L,14) - M_MIN )
              FLUX15 = SPECFAC(I,15) * ( M_ALPHA(I,L,15) - M_MIN )
              FLUX16 = SPECFAC(I,16) * ( M_ALPHA(I,L,16) - M_MIN )
              FLUX_U(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,1) * ( M_ALPHA(I,L,1) - M_MIN )
     ^                    - SPECFAC(I,9) * ( M_ALPHA(I,L,9) - M_MIN )
     ^                   + COS22 * ( FLUX2 - FLUX8 - FLUX10 + FLUX16 ) 
     ^                   + COS45 * ( FLUX3 - FLUX7 - FLUX11 + FLUX15 ) 
     ^                   + SIN22 * ( FLUX4 - FLUX6 - FLUX12 + FLUX14 ) )
              FLUX_V(I,L) = DENSB(I) * (
     ^                      SPECFAC(I,5)  * ( M_ALPHA(I,L,5)  - M_MIN )
     ^                    - SPECFAC(I,13) * ( M_ALPHA(I,L,13) - M_MIN )
     ^                   + COS22 * ( FLUX4 + FLUX6 - FLUX12 - FLUX14 ) 
     ^                   + COS45 * ( FLUX3 + FLUX7 - FLUX11 - FLUX15 ) 
     ^                   + SIN22 * ( FLUX2 + FLUX8 - FLUX10 - FLUX16 ) )
            END IF
 40       CONTINUE
        END IF
C
      END IF
C
C  Sum over azimuths for case where SLOPE not equal to 1.
C
      IF (SLOPE.NE.1.)  THEN

        MMS = M_MIN**SLOPE
C
C  Case with 4 azimuths.
C
        IF (NAZ.EQ.4)  THEN
          DO 50 L = LEV1,LEV2
          DO 50 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX_U(I,L) = DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,1) * ( M_ALPHA(I,L,1)**SLOPE - MMS )
     ^                - SPECFAC(I,3) * ( M_ALPHA(I,L,3)**SLOPE - MMS ) )
              FLUX_V(I,L) = DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,2) * ( M_ALPHA(I,L,2)**SLOPE - MMS )
     ^                - SPECFAC(I,4) * ( M_ALPHA(I,L,4)**SLOPE - MMS ) )
            END IF
 50       CONTINUE
        END IF
C
C  Case with 8 azimuths.
C
        IF (NAZ.EQ.8)  THEN
          DO 60 L = LEV1,LEV2
          DO 60 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2 = SPECFAC(I,2) * ( M_ALPHA(I,L,2)**SLOPE - MMS )
              FLUX4 = SPECFAC(I,4) * ( M_ALPHA(I,L,4)**SLOPE - MMS )
              FLUX6 = SPECFAC(I,6) * ( M_ALPHA(I,L,6)**SLOPE - MMS )
              FLUX8 = SPECFAC(I,8) * ( M_ALPHA(I,L,8)**SLOPE - MMS )
              FLUX_U(I,L) = DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,1) * ( M_ALPHA(I,L,1)**SLOPE - MMS )
     ^                - SPECFAC(I,5) * ( M_ALPHA(I,L,5)**SLOPE - MMS )
     ^                   + COS45 * ( FLUX2 - FLUX4 - FLUX6 + FLUX8 ) )
              FLUX_V(I,L) =  DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,3) * ( M_ALPHA(I,L,3)**SLOPE - MMS )
     ^                - SPECFAC(I,7) * ( M_ALPHA(I,L,7)**SLOPE - MMS )
     ^                   + COS45 * ( FLUX2 + FLUX4 - FLUX6 - FLUX8 ) )
            END IF
 60       CONTINUE
        END IF
C
C  Case with 12 azimuths.
C
        IF (NAZ.EQ.12)  THEN
          DO 70 L = LEV1,LEV2
          DO 70 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2  = SPECFAC(I,2)  * ( M_ALPHA(I,L,2)**SLOPE - MMS )
              FLUX3  = SPECFAC(I,3)  * ( M_ALPHA(I,L,3)**SLOPE - MMS )
              FLUX5  = SPECFAC(I,5)  * ( M_ALPHA(I,L,5)**SLOPE - MMS )
              FLUX6  = SPECFAC(I,6)  * ( M_ALPHA(I,L,6)**SLOPE - MMS )
              FLUX8  = SPECFAC(I,8)  * ( M_ALPHA(I,L,8)**SLOPE - MMS )
              FLUX9  = SPECFAC(I,9)  * ( M_ALPHA(I,L,9)**SLOPE - MMS )
              FLUX11 = SPECFAC(I,11) * ( M_ALPHA(I,L,11)**SLOPE - MMS )
              FLUX12 = SPECFAC(I,12) * ( M_ALPHA(I,L,12)**SLOPE - MMS )
              FLUX_U(I,L) = DENSB(I) / SLOPE * (
     ^                   SPECFAC(I,1) * ( M_ALPHA(I,L,1)**SLOPE - MMS )
     ^                 - SPECFAC(I,7) * ( M_ALPHA(I,L,7)**SLOPE - MMS )
     ^                    + COS30 * ( FLUX2 - FLUX6 - FLUX8 + FLUX12 ) 
     ^                    + SIN30 * ( FLUX3 - FLUX5 - FLUX9 + FLUX11 ) )
              FLUX_V(I,L) =  DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,4)  * ( M_ALPHA(I,L,4)**SLOPE - MMS )
     ^                - SPECFAC(I,10) * ( M_ALPHA(I,L,10)**SLOPE - MMS )
     ^                    + COS30 * ( FLUX3 + FLUX5 - FLUX9 - FLUX11 ) 
     ^                    + SIN30 * ( FLUX2 + FLUX6 - FLUX8 - FLUX12 ) )
            END IF
 70       CONTINUE
        END IF
C
C  Case with 16 azimuths.
C
        IF (NAZ.EQ.16)  THEN
          DO 80 L = LEV1,LEV2
          DO 80 I = IL1,IL2
            IF (DRAGIL(I,L).eq.1)  THEN
              FLUX2  = SPECFAC(I,2)  * ( M_ALPHA(I,L,2)**SLOPE - MMS )
              FLUX3  = SPECFAC(I,3)  * ( M_ALPHA(I,L,3)**SLOPE - MMS )
              FLUX4  = SPECFAC(I,4)  * ( M_ALPHA(I,L,4)**SLOPE - MMS )
              FLUX6  = SPECFAC(I,6)  * ( M_ALPHA(I,L,6)**SLOPE - MMS )
              FLUX7  = SPECFAC(I,7)  * ( M_ALPHA(I,L,7)**SLOPE - MMS )
              FLUX8  = SPECFAC(I,8)  * ( M_ALPHA(I,L,8)**SLOPE - MMS )
              FLUX10 = SPECFAC(I,10) * ( M_ALPHA(I,L,10)**SLOPE - MMS )
              FLUX11 = SPECFAC(I,11) * ( M_ALPHA(I,L,11)**SLOPE - MMS )
              FLUX12 = SPECFAC(I,12) * ( M_ALPHA(I,L,12)**SLOPE - MMS )
              FLUX14 = SPECFAC(I,14) * ( M_ALPHA(I,L,14)**SLOPE - MMS )
              FLUX15 = SPECFAC(I,15) * ( M_ALPHA(I,L,15)**SLOPE - MMS )
              FLUX16 = SPECFAC(I,16) * ( M_ALPHA(I,L,16)**SLOPE - MMS )
              FLUX_U(I,L) = DENSB(I) / SLOPE * (
     ^                   SPECFAC(I,1) * ( M_ALPHA(I,L,1)**SLOPE - MMS )
     ^                 - SPECFAC(I,9) * ( M_ALPHA(I,L,9)**SLOPE - MMS )
     ^                   + COS22 * ( FLUX2 - FLUX8 - FLUX10 + FLUX16 ) 
     ^                   + COS45 * ( FLUX3 - FLUX7 - FLUX11 + FLUX15 ) 
     ^                   + SIN22 * ( FLUX4 - FLUX6 - FLUX12 + FLUX14 ) )
              FLUX_V(I,L) =  DENSB(I) / SLOPE * (
     ^                  SPECFAC(I,5)  * ( M_ALPHA(I,L,5)**SLOPE - MMS )
     ^                - SPECFAC(I,13) * ( M_ALPHA(I,L,13)**SLOPE - MMS )
     ^                   + COS22 * ( FLUX4 + FLUX6 - FLUX12 - FLUX14 ) 
     ^                   + COS45 * ( FLUX3 + FLUX7 - FLUX11 - FLUX15 ) 
     ^                   + SIN22 * ( FLUX2 + FLUX8 - FLUX10 - FLUX16 ) )
            END IF
 80       CONTINUE
        END IF
C
      END IF
C
C  Calculate drag at intermediate levels using centered differences.
C      
      DO 90 L = LEV1P,LEV2M
      DO 90 I = IL1,IL2
        IF (DRAGIL(I,L).eq.1)  THEN
          DDZ2 = DENSITY(I,L) * ( ALT(I,L+1) - ALT(I,L-1) )
          DRAG_U(I,L) = - ( FLUX_U(I,L+1) - FLUX_U(I,L-1) ) / DDZ2
          DRAG_V(I,L) = - ( FLUX_V(I,L+1) - FLUX_V(I,L-1) ) / DDZ2
        END IF
 90   CONTINUE
C
C  Drag at first and last levels using one-side differences.
C 
      DO 100 I = IL1,IL2
        IF (DRAGIL(I,LEV1).eq.1)  THEN
          DDZ = DENSITY(I,LEV1) * ( ALT(I,LEV1P) - ALT(I,LEV1) ) 
          DRAG_U(I,LEV1) = - ( FLUX_U(I,LEV1P) - FLUX_U(I,LEV1) ) / DDZ
          DRAG_V(I,LEV1) = - ( FLUX_V(I,LEV1P) - FLUX_V(I,LEV1) ) / DDZ
        END IF
 100  CONTINUE
      DO 110 I = IL1,IL2
        IF (DRAGIL(I,LEV2).eq.1)  THEN
          DDZ = DENSITY(I,LEV2) * ( ALT(I,LEV2) - ALT(I,LEV2M) )
          DRAG_U(I,LEV2) = - ( FLUX_U(I,LEV2) - FLUX_U(I,LEV2M) ) / DDZ
          DRAG_V(I,LEV2) = - ( FLUX_V(I,LEV2) - FLUX_V(I,LEV2M) ) / DDZ
        END IF
 110  CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
