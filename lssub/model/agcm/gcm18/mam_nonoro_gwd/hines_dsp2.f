      SUBROUTINE HINES_DSP2 (DRAG_U,DRAG_V,HEAT,DIFFCO,FLUX_U,FLUX_V,
     1                       VEL_U,VEL_V,BVFREQ,DENSITY,VISC_MOL,ALT,
     2                       RMS_WIND,K_ALPHA,V_ALPHA,M_ALPHA,
     3                       SIGMA_ALPHA,SIGSQH_ALPHA,AK_ALPHA,
     &                       SPECFAC,DO_ALPHA,DRAG,DRAGIL,  
     4                       MMIN_ALPHA,I_ALPHA,SIGMA_T,DENSB,BVFB,
     5                       UBOT,VBOT,IHEATCAL,ICUTOFF,IPRNT1,IPRNT2,
     6                       NSMAX,SMCO,ALT_CUTOFF,KSTAR,M_MIN,SLOPE,
     7                       F1,F2,F3,F5,F6,NAZ,IL1,IL2,
     8                       LEVBOT,LEVTOP,NLONS,NLEVS,NAZMTH,wrk1,wrk2,
     9                       wrk3)
C
C  Main routine for Hines Doppler spread gravity wave parameterization
C  scheme which calculates zonal and meridional components of gravity 
C  wave drag, heating rates and diffusion coefficient on a longitude 
C  by altitude grid.
C
C  Aug. 13/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Aug.25/99 - C. McLandress (heating formulation corrected)
C  Aug. 8/99 - C. McLandress (nonzero M_MIN for all other values of SLOPE)
C  Feb. 2/96 - C. McLandress (added: minimum cutoff wavenumber M_MIN;
C                             12 and 16 azimuths; inclusion of introwaves
C                             by UBOT and VBOT; diffusion and heating
C                             calculated at half level; logical flags;
C                             V_ALPHA array only at single level;
C                             new print routine) 
C
C  Output arguements:
C  ------------------
C
C     * DRAG_U = zonal component of gravity wave drag (m/s^2).
C     * DRAG_V = meridional component of gravity wave drag (m/s^2).
C     * HEAT   = gravity wave heating (K/sec).
C     * DIFFCO = diffusion coefficient (m^2/sec)
C     * FLUX_U = zonal component of vertical momentum flux (Pascals)
C     * FLUX_V = meridional component of vertical momentum flux (Pascals)
C
C  Input arguements:
C  -----------------
C
C     * VEL_U      = background zonal wind component (m/s) (positive is
C     *              eastward).
C     * VEL_V      = background meridional wind component (m/s) (positive
C     *              northward).
C     * BVFREQ     = background Brunt Vassala frequency (radians/sec).
C     * DENSITY    = background density (kg/m^3) 
C     * VISC_MOL   = molecular viscosity (m^2/s)
C     * ALT        = altitude of momentum, density, buoyancy levels (m)
C     *              (levels must be ordered so ALT(I,LEVTOP) > ALT(I,LEVBOT))
C     * RMS_WIND   = root mean square gravity wave wind at bottom (reference)
C     *              level (m/s).
C     * K_ALPHA    = horizontal wavenumber of each azimuth (1/m).
C     * IHEATCAL   = 1 to calculate heating rates and diffusion coefficient.
C     * IPRNT1     = 1 to print out flux, drag arrays at specified longitudes.
C     * IPRNT2     = 1 to print out azimuthal arrays at specified longitudes.
C     * ICUTOFF    = 1 to exponentially damp drag, heating and diffusion 
C     *              arrays above the altitude ALT_CUTOFF.
C     * ALT_CUTOFF = altitude in meters above which exponential decay applied.
C     * SMCO       = smoothing factor used to smooth cutoff vertical 
C     *              wavenumbers and total rms winds in vertical direction
C     *              before calculating drag or heating
C     *              (SMCO >= 1 ==>  a 1:SMCO:1 three-point stencil is used).
C     * NSMAX      = number of times smoother applied ( >= 1),
C     *            = 0 means no smoothing performed.
C     * KSTAR      = typical gravity wave horizontal wavenumber (1/m).
C     * M_MIN      = minimum allowable cutoff wavenumber, e.g., 1/(3km). 
C     * SLOPE      = slope of incident vertical wavenumber spectrum
C     *              (SLOPE must equal 1., 1.5, 2. or 3.).
C     * F1 to F6   = Hines's fudge factors (F4 not needed since used for
C     *              vertical flux of vertical momentum).
C     * NAZ        = number of horizontal azimuths used (4, 8, 12 or 16).
C     * IL1        = first longitudinal index to use (IL1 >= 1).
C     * IL2        = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEVBOT     = index of bottom (reference) drag level.
C     * LEVTOP     = index of top drag level (if LEVBOT > LEVTOP then the
C     *              vertical indexes increase from top down, otherwise
C     *              indexes increase from bottom up).
C     * NLONS      = number of longitudes.
C     * NLEVS      = number of vertical levels.
C     * NAZMTH     = azimuthal array dimension (NAZMTH >= NAZ).
C 
C  Input work arrays:
C  ------------------
C
C     * M_ALPHA      = cutoff vertical wavenumber (1/m).
C     * V_ALPHA      = wind component at each azimuth (m/s).
C     * SIGMA_ALPHA  = total rms wind in each azimuth (m/s).
C     * SIGSQH_ALPHA = portion of wind variance from waves having wave
C     *                normals in the alpha azimuth (m/s).
C     * SIGMA_T      = total rms horizontal wind (m/s).
C     * AK_ALPHA     = spectral amplitude factor at each azimuth 
C     *                (i.e.,{AjKj}) in m^4/s^2.
C     * SPECFAC      = AK_ALPHA * K_ALPHA.
C     * DO_ALPHA     = logical flag indicating azimuths and longitudes
C     *                where calculations to be performed.
C     * DRAG         = logical flag indicating longitudes where calculations
C     *                to be performed.
C     * DRAGIL       = logical flag indicating longitudes and levels where 
C     *                calculations to be performed.
C     * I_ALPHA      = Hines' integral.
C     * MMIN_ALPHA   = minimum value of cutoff wavenumber.
C     * DENSB        = background density at reference level.
C     * BVFB         = buoyancy frequency at bottom reference and
C     *                work array for ICUTOFF = 1.
C     * UBOT         = background zonal wind component at reference level.
C     * VBOT         = background meridional wind component at reference level.
C
      IMPLICIT NONE
      INTEGER  NAZ, NLONS, NLEVS, NAZMTH, IL1, IL2, LEVBOT, LEVTOP
      INTEGER  ICUTOFF, NSMAX, IHEATCAL, IPRNT1, IPRNT2
cccc      LOGICAL  DRAGIL(NLONS,NLEVS), DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH)
      INTEGER  DRAGIL(NLONS,NLEVS), DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH)
      REAL    KSTAR, M_MIN, F1, F2, F3, F5, F6, SLOPE, ALT_CUTOFF, SMCO
      REAL    DRAG_U(NLONS,NLEVS),   DRAG_V(NLONS,NLEVS) 
      REAL    HEAT(NLONS,NLEVS),     DIFFCO(NLONS,NLEVS)
      REAL    FLUX_U(NLONS,NLEVS),   FLUX_V(NLONS,NLEVS)
      REAL    VEL_U(NLONS,NLEVS),    VEL_V(NLONS,NLEVS)
      REAL    BVFREQ(NLONS,NLEVS),   DENSITY(NLONS,NLEVS)
      REAL    VISC_MOL(NLONS,NLEVS), ALT(NLONS,NLEVS)
      REAL    RMS_WIND(NLONS),       BVFB(NLONS),   DENSB(NLONS)
      REAL    UBOT(NLONS),           VBOT(NLONS)
      REAL    SIGMA_T(NLONS,NLEVS)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGSQH_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH), V_ALPHA(NLONS,NAZMTH)
      REAL    AK_ALPHA(NLONS,NAZMTH),      K_ALPHA(NLONS,NAZMTH)
      REAL    MMIN_ALPHA(NLONS,NAZMTH) ,   I_ALPHA(NLONS,NAZMTH)
      REAL    SPECFAC(NLONS,NAZMTH)

      real wrk1(nlons),wrk2(nlons),wrk3(nazmth)
C
C  Internal variables.
C  -------------------
C
      INTEGER  IERROR, I, N, L, LEV1P, LEV2M, NUNIT
      INTEGER  ILPRT1, ILPRT2, LEV1, LEV2, IORDER, ICOUNT
      REAL    ZERO, RMS_MIN
      DATA  ZERO / 0. /, RMS_MIN / 0.001 / 
C----------------------------------------------------------------------- 
C
C  Check that things set up correctly, abort if not.
C 
      IERROR = 0
      IF (NAZ .GT. NAZMTH)                                  IERROR = 10
      IF (NAZ.NE.4 .AND. NAZ.NE.8 
     & .AND. NAZ.NE.12 .AND. NAZ.NE.16 )                    IERROR = 20
      IF (SLOPE.NE.1. .AND. SLOPE.NE.1.5 
     & .AND. SLOPE.NE.2. .AND. SLOPE.NE.3.)                 IERROR = 30
      IF (IERROR.NE.0)  THEN
        WRITE (6,*) 'aborting in HINES_DSP2: IERROR=',IERROR
        STOP
      END IF 
C
C  Ordering of levels.
C
      IF (LEVBOT.LT.LEVTOP)  THEN
        LEV1 = LEVBOT
        LEV2 = LEVTOP
        IORDER = -1
      ELSE
        LEV1 = LEVTOP
        LEV2 = LEVBOT
        IORDER = 1
      END IF
C
      LEV1P = LEV1 + 1
      LEV2M = LEV2 - 1
C
C  Initialize output and some work arrays.
C
      DO 5 L = 1,NLEVS
      DO 5 I = IL1,IL2
        DRAG_U(I,L) = ZERO
        DRAG_V(I,L) = ZERO
        HEAT(I,L)   = ZERO
        DIFFCO(I,L) = ZERO
        FLUX_U(I,L) = ZERO
        FLUX_V(I,L) = ZERO
        SIGMA_T(I,L) = ZERO
 5    CONTINUE
C
C  Initialize cutoff wavenumber array to minimum value. 
C
      DO 10 N = 1,NAZ
      DO 10 L = 1,NLEVS
      DO 10 I = IL1,IL2
        M_ALPHA(I,L,N) = M_MIN
 10   CONTINUE
C
C  Longitudes where drag to be calculated.
C
      ICOUNT = 0
      DO 15 I = IL1,IL2
        DRAG(I) = 0
        IF (RMS_WIND(I).GE.RMS_MIN)  THEN
          DRAG(I) = 1
          ICOUNT = ICOUNT + 1
        END IF
 15   CONTINUE
C
C  Return to calling program if no drag.
C
      IF (ICOUNT.EQ.0)  RETURN
C
C  Buoyancy, density and winds at bottom level.
C
      DO 20 I = IL1,IL2
        IF (DRAG(I).eq.1)  THEN
          BVFB(I)  = BVFREQ(I,LEVBOT)
          DENSB(I) = DENSITY(I,LEVBOT)
          UBOT(I)  = VEL_U(I,LEVBOT)
          VBOT(I)  = VEL_V(I,LEVBOT)
        END IF
 20   CONTINUE
C
C  Calculate cutoff vertical wavenumber and velocity variances.
C
      CALL HINES_WAVNUM2 (M_ALPHA, SIGMA_ALPHA, SIGSQH_ALPHA, SIGMA_T, 
     ^                    AK_ALPHA, VEL_U, VEL_V, UBOT, VBOT, 
     ^                    VISC_MOL, DENSITY, DENSB, BVFREQ, BVFB, 
     ^                    RMS_WIND, V_ALPHA, I_ALPHA, MMIN_ALPHA,
     ^                    DO_ALPHA, DRAGIL, DRAG, M_MIN, KSTAR, SLOPE, 
     ^                    F1, F2, F3, NAZ, LEVBOT, LEVTOP, 
     ^                    IL1, IL2,  NLONS, NLEVS, NAZMTH,wrk1,wrk2)
C
C  Multiplicative spectral factor at each azimuth.
C
      DO 50 N = 1,NAZ
      DO 50 I = IL1,IL2
        IF (DRAG(I).eq.1)  THEN
          SPECFAC(I,N) = AK_ALPHA(I,N) * K_ALPHA(I,N)
        END IF
 50   CONTINUE
C
C  Smooth cutoff wavenumbers and total rms velocity in the vertical 
C  direction NSMAX times, using FLUX_U as temporary work array.
C   
      IF (NSMAX.GT.0)  THEN
        DO 80 N = 1,NAZ
          CALL HINES_SMOOTH ( M_ALPHA(1,1,N), 
     ^                        FLUX_U, DRAGIL, SMCO, NSMAX,
     ^                        IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
 80     CONTINUE
        CALL HINES_SMOOTH ( SIGMA_T, 
     ^                      FLUX_U, DRAGIL, SMCO, NSMAX,
     ^                      IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
      END IF
C
C  Calculate zonal and meridional components of the
C  momentum flux and drag.
C
      CALL HINES_DRAG2 (FLUX_U, FLUX_V, DRAG_U, DRAG_V, 
     ^                  ALT, DENSITY, DENSB, M_ALPHA, 
     ^                  SPECFAC, DRAGIL, M_MIN, SLOPE, NAZ,
     ^                  IL1, IL2, LEV1, LEV2, NLONS, NLEVS, NAZMTH )
C
C  Heating rate and diffusion coefficient at midpoint between momentum levels.
C
      IF (IHEATCAL.EQ.1)  THEN 
        CALL HINES_HEAT2 (HEAT, DIFFCO, 
     ^                    ALT, M_ALPHA, SPECFAC, DRAGIL, 
     ^                    BVFREQ, DENSITY, DENSB, 
     ^                    SIGMA_T, SIGMA_ALPHA, VISC_MOL, 
     ^                    KSTAR, SLOPE, F1, F2, F3, F5, F6, 
     ^                    NAZ, IL1, IL2,
     ^                    LEVBOT, LEVTOP, NLONS, NLEVS, NAZMTH)
      END IF
C
C  Apply exponential decay to drag, heating and diffusion above 
C  ALT_CUTOFF (use BVFB as temporary work array).
C
      IF (ICUTOFF.EQ.1)  THEN
        CALL HINES_EXP ( DRAG_U, 
     ^                   BVFB, ALT, ALT_CUTOFF, IORDER,
     ^                   IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
        CALL HINES_EXP ( DRAG_V, 
     ^                   BVFB, ALT, ALT_CUTOFF, IORDER,
     ^                   IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
        IF (IHEATCAL.EQ.1)  THEN
          CALL HINES_EXP ( HEAT,
     ^                     BVFB, ALT, ALT_CUTOFF, IORDER,
     ^                     IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
          CALL HINES_EXP ( DIFFCO, 
     ^                     BVFB, ALT, ALT_CUTOFF, IORDER,
     ^                     IL1, IL2, LEV1, LEV2, NLONS, NLEVS )
        END IF   
      END IF   
C
C  Print out flux, drag, etc arrays for diagnostic purposes.
C
      IF (IPRNT1.EQ.1)  THEN
        ILPRT1 = 1
        ILPRT2 = 1
        NUNIT  = 11
        CALL HINES_PRNT1 ( FLUX_U, FLUX_V, DRAG_U, DRAG_V, VEL_U, VEL_V,
     ^                     ALT, SIGMA_T, SIGMA_ALPHA, M_ALPHA,
     ^                     1, 1, NUNIT, ILPRT1, ILPRT2, LEV1, LEV2,
     ^                     NAZ, NLONS, NLEVS, NAZMTH)
      END IF
C
C  Print out azimuthal arrays for diagnostic purposes.
C
      IF (IPRNT2.EQ.1)  THEN
        ILPRT1 = 1
        ILPRT2 = 1
        NUNIT  = 11
        CALL HINES_PRNT2 (M_ALPHA, SIGMA_ALPHA, VEL_U, VEL_V,
     ^                    UBOT, VBOT, ALT, DRAGIL, V_ALPHA,
     ^                    NUNIT, ILPRT1, ILPRT2, LEV1, LEV2,
     ^                    NAZ, NLONS, NLEVS, NAZMTH,wrk3)
      END IF
C
C  Finished.
C
      RETURN
C-----------------------------------------------------------------------
      END
