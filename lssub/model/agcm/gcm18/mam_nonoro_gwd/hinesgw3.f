      SUBROUTINE HINESGW3 (UG,VG,TH,TSG,PRESSG,SGJ,SHJ,SHXKJ,DSGJ,
     1                     RADJ,UTENDGW,VTENDGW,HEAT,DIFFCO,URMS,
     2                     RGAS,RGOCP,ILEV,IL1,IL2,ILG,nazmth)
C
C     ********************************************************************
C     * THIS ROUTINE CALCULATES THE HORIZONTAL WIND TENDENCIES,
C     * DIFFUSION COEFFICIENT AND HEATING RATE USING THE DOPPLER SPREAD 
C     * GWD PARAMETERIZATION OF HINES (JASTP,97).
C     ********************************************************************

C     * JUN 27, 2006 - M.LAZARE.   NEW VERSION FOR GCM15F BASED ON HINESGW2:
C     *                           - REPLACE CONSTANTS BY VARIABLES IN 
C     *                             INTRINSIC CALLS SUCH AS "MAX" TO
C     *                             ENABLE COMPILING IN 32-BITS WITH REAL*8.
C     *                           - WORK ARRAYS ARE NOW LOCAL TO THE
C     *                             ROUTINE, IE NOT PASSED IN WITH
C     *                             "WRK" POINTERS.
C     * AUTHOR - C.MCLANDRESS NOV. 29/99 (BASED ON GWDORDS2; ALL HINES INPUT  
C     *                                   PARAMETERS ARE SPECIFIED HERE)
C     *                                  THIS IS THE PREVIOUS VERSION HINEWGW2. 
C     * MODIFICATIONS: 
C     *   C.MCL (AUG. 25/99) - OPTIONS TO USE LATITUDINALLY-DEPENDENT 
C     *                        HORIZONTAL WAVENUMBER USED BY MANZINI & 
C     *                        MCFARLANE (JGR,98); GAUSSIAN-SHAPED
C     *                        RMS WIND WITH MAX AT EQUATOR;
C     *                        NEW ROUTINES WITH CORRECT HEATING AS WELL AS
C     *                        NONZERO M_MIN FOR DIFFERENT SLOPES ARE USED)

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C     * INPUT VARIABLES AND ARRAYS:

      INTEGER  ILEV,IL1,IL2,ILG
      REAL       RGAS, RGOCP, GRAV
      REAL   UG(ILG,ILEV),        VG(ILG,ILEV)
      REAL   TH(ILG,ILEV),       TSG(ILG,ILEV),    SGJ(ILG,ILEV),
     1      SHJ(ILG,ILEV),     SHXKJ(ILG,ILEV),   DSGJ(ILG,ILEV)
      REAL   PRESSG(ILG)
C
C     * ARRAYS PASSED THROUGH CALL TO PHYSICS ARE NATIVE REAL.
C
      REAL RADJ(ILG)

C     * INPUT/OUTPUT ARRAYS:

      REAL   UTENDGW(ILG,ILEV), VTENDGW(ILG,ILEV)

C     * OUTPUT ARRAYS:
C     * HEAT   = GW HEATING RATE (DEGK/S)
C     * DIFFCO = GW DIFFUSION COEFFICIENT (M^2/S)
C     * URMS   = TOTAL RMS GW HORIZONTAL WIND (M/S)

      REAL   HEAT(ILG,ILEV),   DIFFCO(ILG,ILEV), URMS(ILG,ILEV)

C     * WORK ARRAYS (DEFINED HERE USING DIMENSIONS SPECIFIED IN 
C     * THE PARAMETER STATEMENTS: MAKE SURE THAT JILG AND JILEV 
C     * ARE SAME AS ILG AND ILEV)

c      INTEGER  JILG, JILEV, NAZMTH
c      PARAMETER  ( JILG = $I$, JILEV = $L$, NAZMTH = 8 )
c      parameter(NAZMTH = 8 )
cccc      LOGICAL LODRAG(ILG),    
cccc     1        LODRAGIL(ILG,ILEV), LODO_ALPHA(ILG,NAZMTH)
      INTEGER LODRAG(ILG),    
     1        LODRAGIL(ILG,ILEV), LODO_ALPHA(ILG,NAZMTH)
      REAL   M_ALPHA(ILG,ILEV,NAZMTH),     
     1       SIGMA_ALPHA(ILG,ILEV,NAZMTH), 
     2       SIGSQH_ALPHA(ILG,ILEV,NAZMTH),
     3       BVFREQ(ILG,ILEV),      DENSITY(ILG,ILEV),
     4       UTDSP(ILG,ILEV),       VTDSP(ILG,ILEV),
     5       FLUX_U(ILG,ILEV),      FLUX_V(ILG,ILEV),
     6       VISC_MOL(ILG,ILEV),    ALT(ILG,ILEV),
     7       AK_ALPHA(ILG,NAZMTH),   K_ALPHA(ILG,NAZMTH),
     8       MMIN_ALPHA(ILG,NAZMTH), I_ALPHA(ILG,NAZMTH),
     9       V_ALPHA(ILG,NAZMTH),    SPECFAC(ILG,NAZMTH),
     A       RMSWIND(ILG),  UB(ILG), VB(ILG), DENSB(ILG), BVFB(ILG)

      real wrk1(ilg),wrk2(ilg),wrk3(nazmth)

C     * FLAGS TO CONTROL PRINTOUT:
C     * IFL     = flag indicating first call to this routine ("SAVE" it)
C     * NMESSG  = unit number for printed messages.
C     * IPRNT0  = 1 to print out basic parameters (SLOPE, F1, etc)
C     * IPRNT1  = 1 to print out Hines arrays (drag, flux, ...)
C     * IPRNT2  = 1 to print out Hines arrays (M_ALPHA, V_ALPHA, ...)

      INTEGER  IFL, NMESSG, IPRNT0, IPRNT1, IPRNT2
      DATA  IFL / 0 /, NMESSG / 6 /
      DATA  IPRNT0 / 1 /, IPRNT1 / 0 /, IPRNT2 / 0 /
      SAVE  IFL

C     * INPUT CONSTANTS FOR HINES SCHEME:
C     * LEVBOT     = index of launch level (e.g. LEVBOT=ILEV).
C     * LEVTOP     = index of top level of gwd calculation (e.g. LEVTOP=1)
C     * RMSCON     = root mean square gravity wave wind (m/s) at launch height.
C     * NAZ        = number of horizontal azimuths used (4, 8, 12 or 16);
C     *              (NAZ <= NAZMTH)
C     * SLOPE      = slope of incident vertical wavenumber spectrum
C     *              (code set up presently for SLOPE 1., 1.5 or 2.).
C     * F1         = "fudge factor" used in calculation of trial value of
C     *              azimuthal cutoff wavenumber M_ALPHA (1.2 <= F1 <= 1.9).
C     * F2         = "fudge factor" used in calculation of maximum
C     *              permissible instabiliy-induced cutoff wavenumber 
C     *              M_SUB_M_TURB (0.1 <= F2 <= 1.4).
C     * F3         = "fudge factor" used in calculation of maximum 
C     *              permissible molecular viscosity-induced cutoff wavenumber 
C     *              M_SUB_M_MOL (0.1 <= F2 <= 1.4).
C     * F5         = "fudge factor" used in calculation of heating rate
C     *              (1 <= F5 <= 3).
C     * F6         = "fudge factor" used in calculation of turbulent 
C     *              diffusivity coefficient.
C     * KSTAR      = typical gravity wave horizontal wavenumber (1/m)
C     *              used in calculation of M_SUB_M_TURB (real #). 
C     * M_MIN      = minimum allowable cutoff vertical wavenumber, eg 1/(3km); 
C     *              M_MIN can only be nonzero for SLOPE=1 (real #).
C     * ICUTOFF    = 1 to exponentially damp off GWD, heating and diffusion 
C     *              arrays above ALT_CUTOFF; otherwise arrays not modified.
C     * ALT_CUTOFF = altitude in meters above which exponential decay applied.
C     * SMCO       = smoother used to smooth cutoff vertical wavenumbers
C     *              and total rms winds before calculating drag or heating.
C     *              (==> a 1:SMCO:1 stencil used; SMCO >= 1.).
C     * NSMAX      = number of times smoother applied ( >= 1),
C     *            = 0 means no smoothing performed.
C     * IHEATCAL   = 1 to calculate heating rates and diffusion coefficient.
C     *            = 0 means only drag and flux calculated.

      INTEGER  LEVBOT, LEVTOP, NAZ, IHEATCAL, ICUTOFF, NSMAX
      REAL     RMSCON, KSTAR, M_MIN, SLOPE, K_MIN, K_MAX
      REAL     F1, F2, F3, F5, F6, ALT_CUTOFF, SMCO
      REAL     RMS_EQUATOR, RMS_WIDTH
      LOGICAL  VARIABLE_K, VARIABLE_RMS
C
      DATA BVFREQ_MIN /0.001/
C-----------------------------------------------------------------------
C
C     * SPECIFY CONSTANTS NEEDED FOR HINES GWD CALCULATIONS.
C
      grav=9.80616
      jilg=ilg
      jilev=ilev
      LEVBOT = ILEV
      LEVTOP = 1
      RMSCON = 1.5
      RMS_EQUATOR = 1.5
      RMS_WIDTH = 15. * (3.141593/180.) 
      NAZ    = 8
      SLOPE  = 1.
      F1     = 1.5 
      F2     = 0.3 
      F3     = 1.0 
      F5     = 1.0  
      F6     = 0.25 
      M_MIN  = 1./3.E3
      K_MIN  = 1.E-5
      K_MAX  = 1.E-4
      KSTAR  = 5.E-6
      ICUTOFF    = 0   
      ALT_CUTOFF = 105.E3
      SMCO       = 2.0 
      NSMAX      = 2
      IHEATCAL   = 0
C ---------- uncomment following line for diffusion coeff calculation
c      IHEATCAL   = 1 
C -----------------------------------------------
      VARIABLE_RMS = .FALSE.
      VARIABLE_K   = .FALSE.

C     * PRINT OUT HINES PARAMETERS AT FIRST CALL IF REQUESTED

      IF (IPRNT0.EQ.1 .AND. IFL.EQ.0)  THEN
        WRITE (NMESSG,6000)
 6000   FORMAT (/' Input parameters for subroutine HINESGW2:')
        WRITE (NMESSG,*)  '  RMSCON = ', RMSCON
        IF (VARIABLE_RMS) THEN
          WRITE (NMESSG,*)  '  RMS_EQUATOR = ', RMS_EQUATOR
          WRITE (NMESSG,*)  '  RMS_WIDTH   = ', RMS_WIDTH
        END IF
        WRITE (NMESSG,*)  '  SLOPE = ', SLOPE
        WRITE (NMESSG,*)  '  NAZ = ', NAZ
        WRITE (NMESSG,*)  '  F1,F2,F3  = ', F1, F2, F3
        WRITE (NMESSG,*)  '  F5,F6     = ', F5, F6
        IF (VARIABLE_K) THEN 
          WRITE (NMESSG,*)  '  K_MIN     = ', K_MIN
          WRITE (NMESSG,*)  '  K_MAX     = ', K_MAX
        END IF
        WRITE (NMESSG,*)  '  KSTAR     = ', KSTAR
        WRITE (NMESSG,*)  '  M_MIN     = ', M_MIN
        IF (ICUTOFF .EQ. 1)  THEN
          WRITE (NMESSG,*) '  Drag exponentially damped above ',
     &                         ALT_CUTOFF/1.E3
        END IF
        IF (IHEATCAL .EQ. 1)  THEN
          WRITE (NMESSG,*) '  Hines diffusion coefficient used'
        END IF
        IF (NSMAX.LT.1 )  THEN
          WRITE (NMESSG,*) '  No smoothing of cutoff wavenumbers, etc'
        ELSE
          WRITE (NMESSG,*) '  Cutoff wavenumbers and sig_t smoothed:'
          WRITE (NMESSG,*) '    SMCO  =', SMCO
          WRITE (NMESSG,*) '    NSMAX =', NSMAX
        END IF
        IFL = 1
      END IF
C
C     * CHECK THAT THINGS ARE SETUP CORRECTLY
C
      IERROR = 0
      IF (NAZ .GT. NAZMTH)                                  IERROR = 10
      IF (SMCO .LT. 1.)                                     IERROR = 40
      IF (JILG.NE.ILG)                                      IERROR = 50
      IF (JILEV.NE.ILEV)                                    IERROR = 60
      IF (IERROR.NE.0)    GO TO 999

C     * USE LATITUDINALLY-DEPENDENT OR CONSTANT RMS WIND AT LAUNCH LEVEL.

      IF (VARIABLE_RMS) THEN 
        DO 100 I=IL1,IL2
          RMSWIND(I) = RMSCON + 
     ^         (RMS_EQUATOR-RMSCON)*EXP(-(RADJ(I)/RMS_WIDTH)**2)
 100    CONTINUE
      ELSE
        DO 105 I=IL1,IL2
          RMSWIND(I) = RMSCON
 105    CONTINUE
      ENDIF

C     * USE LATITUDINALLY-DEPENDENT OR CONSTANT HORIZONTAL WAVENUMBER.
C     * (RADJ IS LATITUDE IN RADIANS)

      IF (VARIABLE_K) THEN 
        DO 120 N=1,NAZ
        DO 120 I=IL1,IL2
          K_ALPHA(I,N) = K_MIN / ( COS(RADJ(I)) + K_MIN/K_MAX )
 120    CONTINUE
      ELSE
        DO 125 N=1,NAZ
        DO 125 I=IL1,IL2
          K_ALPHA(I,N) = KSTAR
 125    CONTINUE
      ENDIF

c      write(6,*) 
c      write(6,*) 'RADJ' 
c      write(6,*) (RADJ(i),i=il1,il2)
c      write(6,*) 'RMS' 
c      write(6,*) (RMSWIND(i),i=il1,il2)
c      write(6,*) 'K_ALPHA'
c      write(6,*) (k_alpha(i,1),i=il1,il2)
c      write(6,*) 

C     * SET MOLECULAR VISCOSITY TO A NEGLIGIBLE VALUE.
C     * (IF THE MODEL TOP IS GREATER THAN 100 KM THEN THE ACTUAL
C     * VISCOSITY COEFFICIENT COULD BE SPECIFIED HERE.)

      DO 140 L=1,ILEV
      DO 140 I=IL1,IL2
        VISC_MOL(I,L) = 1.5E-5
  140 CONTINUE

C     * CALCULATE BV FREQUENCY AT ALL POINTS, SMOOTH, AND SET LOWER LIMIT.

      DO 160 L=2,ILEV
      DO 160 I=IL1,IL2
        DTTDSF=(TH(I,L)/SHXKJ(I,L)-TH(I,L-1)/
     1          SHXKJ(I,L-1))/(SHJ(I,L)-SHJ(I,L-1))
        DTTDSF=MIN(DTTDSF, -5./SGJ(I,L))
        BVFREQ(I,L)=SQRT(-DTTDSF*SGJ(I,L)*(SGJ(I,L)**RGOCP)/RGAS)
     1                   *GRAV/TSG(I,L)
  160 CONTINUE
      DO 170 L=1,ILEV
      DO 170 I=IL1,IL2
        IF(L.EQ.1)                        THEN
          BVFREQ(I,L) = BVFREQ(I,L+1)
        ENDIF
        IF(L.GT.1)                        THEN
          RATIO=5.*LOG(SGJ(I,L)/SGJ(I,L-1))
          BVFREQ(I,L) = (BVFREQ(I,L-1) + RATIO*BVFREQ(I,L))
     1                   /(1.+RATIO)
        ENDIF
        BVFREQ(I,L) = MAX(BVFREQ(I,L), BVFREQ_MIN)
  170 CONTINUE

C     * CALCULATE GEOMETRIC ALTITUDE AND DENSITY.

      DO 180 I=IL1,IL2
        HSCAL = RGAS * TSG(I,ILEV) / GRAV
        DENSITY(I,ILEV) = SGJ(I,ILEV) * PRESSG(I) / (GRAV*HSCAL)
        ALT(I,ILEV) = 0.
  180 CONTINUE
      DO 190 L=ILEV-1,1,-1
      DO 190 I=IL1,IL2
        HSCAL = RGAS * TSG(I,L) / GRAV
        ALT(I,L) = ALT(I,L+1) + HSCAL * DSGJ(I,L) / SGJ(I,L)
        DENSITY(I,L) = SGJ(I,L) * PRESSG(I) / (GRAV*HSCAL)
cccc        if(DENSITY(I,L).lt.0.) then
cccc           write(6,*) "#### DENSITY(I,L) is lt 0."
cccc           write(6,*) "#### i,l,DENSITY(I,L)",i,l,DENSITY(I,L)
cccc           write(6,*) "####","SGJ(I,L),PRESSG(I),HSCAL"
cccc           write(6,*) "####",SGJ(I,L),PRESSG(I),HSCAL
cccc           write(6,*) "####","TSG(I,L),RGAS,grav"
cccc           write(6,*) "####",TSG(I,L),RGAS,grav
cccc        endif


  190 CONTINUE

C     * CALCULATE GWD (NOTE THAT DIFFUSION COEFFICIENT AND
C     * HEATING RATE ONLY CALCULATED IF IHEATCAL = 1).

C      WRITE(6,*)'HINESGW2',IL1,IL2,ILEV,ILG,NAZMTH,NAZ

      CALL HINES_DSP2 (UTDSP,VTDSP,HEAT,DIFFCO,FLUX_U,FLUX_V,
     1                 UG,VG,BVFREQ,DENSITY,VISC_MOL,ALT,
     2                 RMSWIND,K_ALPHA,V_ALPHA,M_ALPHA,
     3                 SIGMA_ALPHA,SIGSQH_ALPHA,AK_ALPHA,
     4                 SPECFAC,LODO_ALPHA,LODRAG,LODRAGIL,
     5                 MMIN_ALPHA,I_ALPHA,URMS,DENSB,BVFB,
     6                 UB,VB,IHEATCAL,ICUTOFF,IPRNT1,IPRNT2,
     7                 NSMAX,SMCO,ALT_CUTOFF,KSTAR,M_MIN,SLOPE,
     8                 F1,F2,F3,F5,F6,NAZ,IL1,IL2,LEVBOT,LEVTOP,
     9                 ILG,ILEV,NAZMTH,wrk1,wrk2,wrk3)

C     * ADD GWD TO PREVIOUS WIND TENDENCIES.

      DO 200 L=1,ILEV
      DO 200 I=IL1,IL2
        UTENDGW(I,L) = UTENDGW(I,L) + UTDSP(I,L)
        VTENDGW(I,L) = VTENDGW(I,L) + VTDSP(I,L)
  200 CONTINUE

C ---------------------------------------------------------------------
C     * FINISHED.

      RETURN

 999  CONTINUE

C     * IF ERROR DETECTED THEN ABORT.

      WRITE (NMESSG,6010)
      WRITE (NMESSG,6020) IERROR
 6010 FORMAT (/' EXECUTION ABORTED IN HINESGW2')
 6020 FORMAT ('     ERROR FLAG =',I4)
      STOP
C-----------------------------------------------------------------------
      END
