      SUBROUTINE GWDSAT3(UM,VM,TSG,TSGB,SGJ,SGBJ,PRESSG,RADJ,
     1                   RGAS,RGOCP,ILEV,IL1,IL2,ILG,LAUN_Z,EPLAUNCH,
     2                   NADIM,NCDIM,UTEND,VTEND,TWODTI)
C
C     * APR 29, 2012 - J.SCINOCCA. NEW VERSION FOR GCM16:
C     *                            - DEPOSIT ALL OUTGOING GWD IN TOP
C     *                              LAYER.
C     * JUN 27, 2006 - M.LAZARE.   PREVIOUS VERSION GWDSAT FOR GCM15F/G/H/I:
C     *                            - RADJ PASSED IN AND USED AS SIN(RADJ)
C     *                              INSTEAD OF PASSING IN SINJ (SINCE
C     *                              SINJ NO LONGER IN PHYSICS). NOTE
C     *                              THAT IT IS REAL AND NOT REAL*8.
C     *                            - WORK ARRAYS ARE NOW LOCAL TO THE
C     *                              ROUTINE, IE NOT PASSED IN WITH
C     *                              "WRK" POINTERS.
C     *                            - REPLACE CONSTANTS BY VARIABLES IN 
C     *                              INTRINSIC CALLS SUCH AS "MAX" TO
C     *                              ENABLE COMPILING IN 32-BITS WITH REAL*8.
C
C     NON-OROGRAPHIC GRAVITY-WAVE DRAG PARAMETERIZATION SCINOCCA (JAS 2003).
C     EXACT HYDROSTATIC, NON-ROTATING, VERSION OF WARNER AND MCINTYRE (1996) 
C     THIS VERSION INCLUDES A CRITICAL-LEVEL CORRECTION THAT PREVENTS THE MOMEMTUM
C     DEPOSITION IN EACH AZIMUTH FROM DRIVING THE FLOW TO SPEEDS FASTER THAN
C     THE PHASE SPEED OF THE WAVES THAT MEET THAIR CRITICAL LEVELS.
C     
C     PTWO - 2*P, WHERE -P IS THE EXPONENT OF THE INTRINSIC FREQUENCY IN THE
C            EXPRESSION FOR LAUNCH ENERGY DENSITY
C     IPREAL - FLAG TO INDICATE IF P IS INTEGER OR REAL -> USED FOR OPTIMIZATION

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER(PTWO=3.E0,IPREAL=1)
C
C     I/O ARRAYS.
C
      REAL    UM(ILG,ILEV),VM(ILG,ILEV)
      REAL   TSG(ILG,ILEV),TSGB(ILG,ILEV)
      REAL   SGJ(ILG,ILEV),SGBJ(ILG,ILEV),PRESSG(ILG)
      REAL   UTEND(ILG,ILEV),VTEND(ILG,ILEV)
C
C     * ARRAYS PASSED THROUGH CALL TO PHYSICS ARE NATIVE REAL.
C
      REAL   RADJ(ILG)
C
C     * LOCAL INTERNAL WORK ARRAYS.
C
      REAL   UB(ILG,ILEV),VB(ILG,ILEV),BVM(ILG,ILEV),BVB(ILG,ILEV)
      REAL   PM(ILG,ILEV),PB(ILG,ILEV),RHOM(ILG,ILEV),RHOB(ILG,ILEV)

      REAL   X(NCDIM),CI(NCDIM),DCI(NCDIM)
      REAL   UI(ILG,NADIM,ILEV)
      REAL   UL(ILG,NADIM),NL(ILG),UNIT(2,NADIM)
      REAL   FCT(ILG,ILEV),FNORM(ILG)
      REAL   CI_MIN(ILG,NADIM)

      REAL   EP(ILG,NCDIM,NADIM),ACT(ILG,NCDIM,NADIM)
      REAL   PU(ILG,ILEV,NADIM)
      REAL   EPF(ILG,ILEV,2)
      REAL   CRT(ILG,ILEV,NADIM),DFL(ILG,ILEV,NADIM)
      REAL   ACC(ILG,NCDIM,NADIM)

      REAL   ZM(ILG,ILEV),ZB(ILG,ILEV)

      REAL   MS_L,MS
      DATA GRAV /9.80616/
      DATA ZERO /0./
      DATA BVMMIN /1.E-20/
      DATA P5 /0.5/
C=====================================================================
      ILEVM=ILEV-1
      GRSQ=GRAV**2
      PI=2*ASIN(1.E0)
      ROT=7.292E-5
      TWODTINV=TWODTI
C     INPUT SPECTRA INFO
C     MS_L,MS - SPECIFICATION OF M-STAR IN LAUNCH SPECTRA
      MS_L=1000.E0
      MS=2*PI/MS_L
      PEXP=PTWO/2.E0


C     COORDINATE TRANSFORM
C     CIMIN,CIMAX - MIN,MAX INTRINSIC LAUNCH-LEVEL PHASE SPEED (C-U_O) (M/S)
      CIMIN=0.25
      CIMAX=100.
C     X=1/CI - TRANSFORMATION VARIABLE
      XMAX=1./CIMIN
      XMIN=1./CIMAX
C     PARAMETERS FOR COORDINATE TRANSFORM
      GAM=0.25
      XRNG=XMAX-XMIN
      DTX=XRNG/FLOAT(NCDIM-1)
      A=XRNG/(EXP(XRNG/GAM)-1.)
      B=XMIN-A

C     SET INITIAL MIN CI IN EACH COLUMN AND AZIMUTH (USED FOR CRITICAL LEVELS)
      DO IA=1,NADIM
         DO IL=IL1,IL2
            CI_MIN(IL,IA)=CIMIN
         ENDDO
      ENDDO


C     *     MOMENTUM         
C     * MID LAYER  INTERFACE 
C     *                      
C     *//////////////////////
C     *                      
C     *                      
C     *......SGJ(1).......... 
C     *                      
C     *-------------- SGBJ(1) 
C     *                      
C     *......SGJ(2).......... 
C     *                      
C     *-------------- SGBJ(2) 
C     *                      
C     *......SGJ(3).......... 
C     *                      
C     *-------------- SGBJ(3) 
C     *                      
C     *......SGJ(4).......... 
C     *                      
C     *-------------- SGBJ(4) 
C     *                      
C     *......SGJ(5).......... 
C     *                      
C     *                      
C     *////////////// SGBJ(5)=1.


C     CALCULATE PRESSUE ON MID AND BASE LEVELS
      DO IL=IL1,IL2
        PB(IL,1) = SGBJ(IL,1) * PRESSG(IL)    
        PM(IL,1)  = SGJ(IL,1) * PRESSG(IL)    
      ENDDO

      DO L=2,ILEV
         DO IL=IL1,IL2
            PB(IL,L)   = SGBJ(IL,L)   * PRESSG(IL)
            PM(IL,L)    = SGJ(IL,L)   * PRESSG(IL)
         ENDDO
      ENDDO
 
C     CALCULATE HEIGHT ON MID AND BASE LEVELS
      ROG=RGAS/GRAV                                                     
      DO IL=IL1,IL2                                                 
        ZB(IL,ILEV) = 0      
      ENDDO                                                       
                                                                        
      DO  L=ILEVM,1,-1                                               
         DO IL=IL1,IL2                                                 
            ZB(IL,L) = ZB(IL,L+1) + 
     1                 ROG*TSG(IL,L+1)*LOG(PB(IL,L+1)/PB(IL,L))  
         ENDDO
      ENDDO
                                                          
      DO IL=IL1,IL2                                                 
         ZM(IL,ILEV) = ROG*TSGB(IL,ILEV)*LOG(PRESSG(IL)/PM(IL,ILEV)) 
      ENDDO                                                       
                                                                        
      DO  L=ILEVM,1,-1                                               
         DO IL=IL1,IL2                                                 
            ZM(IL,L) = ZM(IL,L+1) + 
     1                 ROG*TSGB(IL,L)*LOG(PM(IL,L+1)/PM(IL,L))
         ENDDO
      ENDDO
                                                          
C     CALCULATE BV FREQUENCY ON MID LEVEL
      DO L=2,ILEV                                                 
         DO I=IL1,IL2                                                
          DTDS=-SGJ(I,L)/TSG(I,L)
     1        *(TSGB(I,L)-TSGB(I,L-1))/(SGBJ(I,L)-SGBJ(I,L-1))   
     2          +RGOCP                                                
          BVM(I,L)=MAX(GRSQ*(DTDS/(RGAS*TSG(I,L))),BVMMIN)         
          BVM(I,L)=SQRT(BVM(I,L))                               
       ENDDO
      ENDDO

      DO I=IL1,IL2                                                
         BVM(I,1)=BVM(I,2)                                     
      ENDDO

C     GET BASE-LEVEL WINDS AND BVF

      DO L=1,ILEV-1  
        DO I=IL1,IL2                                        
           FCT1=LOG(SGJ (I,L)/SGBJ(I,L))
           FCT2=LOG(SGBJ(I,L)/SGJ(I,L+1))
           FCT3=1./LOG(SGJ (I,L)/SGJ(I,L+1))
           UB(I,L) =    (UM(I,L+1)*FCT1+ UM(I,L)*FCT2)*FCT3
           VB(I,L) =    (VM(I,L+1)*FCT1+ VM(I,L)*FCT2)*FCT3
           BVB(I,L) = (BVM(I,L+1)*FCT1+BVM(I,L)*FCT2)*FCT3
        ENDDO
      ENDDO

      DO I=IL1,IL2
         UB(I,ILEV)=UB(I,ILEV-1)
         VB(I,ILEV)=VB(I,ILEV-1)
         BVB(I,ILEV)=BVB(I,ILEV-1)
      ENDDO

C     REMOVE DIURNAL CYCLE IN BVF TO BE CONSISTENT WITH ORO SCHEME
      DO L=2,ILEV                                                 
         DO I=IL1,IL2                                                
            RATIO=5.*LOG(SGJ(I,L)/SGJ(I,L-1))                           
            BVM(I,L) = (BVM(I,L-1) + RATIO*BVM(I,L))           
     1           /(1.+RATIO)                                  
         ENDDO
      ENDDO

      DO L=2,ILEV                                                 
         DO I=IL1,IL2                                                
            RATIO=5.*LOG(SGBJ(I,L)/SGBJ(I,L-1))                           
            BVB(I,L) = (BVB(I,L-1) + RATIO*BVB(I,L))           
     1           /(1.+RATIO)                                  
         ENDDO
      ENDDO

C     MID AND BASE LEVEL DENSITY
      DO L=1,ILEV
         DO IL=IL1,IL2
            RHOM(IL,L)=PM(IL,L)/(RGAS*TSG(IL,L))
            RHOB(IL,L)=PB(IL,L)/(RGAS*TSGB(IL,L))
         ENDDO
      ENDDO
      

C     SET UP AZIMUTH DIRECTIONS AND SOME TRIG FACTORS
      DA=2*PI/NADIM
      AZ_FCT=1.
C     GET NORMALIZATION FACTOR TO ENSURE THAT THE SAME AMOUNT OF E-P
C     FLUX IS DIRECTED (N,S,E,W) NO MATER HOW MANY AZIMUTHS ARE SELECTED.
C     NOTE, HOWEVER, THE CODE BELOW ASSUMES A SYMMETRIC DISTRIBUTION OF
C     OF AZIMUTHAL DIRECTIONS (IE 4,8,16,32,...)
      ANORM=0.
      DO IA=1,NADIM
        AZ=(IA-1)*DA
        UNIT(1,IA)=COS(AZ)
        UNIT(2,IA)=SIN(AZ)
        ANORM=ANORM+ABS(COS(AZ))
      ENDDO
      AZ_FCT=2.*AZ_FCT/ANORM


C     DEFINE COORDINATE TRANSFORMATION
C     NOTE THAT THIS IS EXPRESED IN TERMS OF THE INTRINSIC PHASE SPEED
C     AT LAUNCH CI=C-U_O SO THAT THE TRANSFORMATION IS IDENTICAL AT EVERY
C     LAUNCH SITE.
      DO I=1,NCDIM
         TX=FLOAT(I-1)*DTX+XMIN
         X(I)=A*EXP((TX-XMIN)/GAM)+B
         CI(I)=1./X(I)
         DCI(I)=CI(I)**2*(A/GAM)*EXP((TX-XMIN)/GAM)*DTX
      ENDDO

C     IF CONSTANT RESOLUTION DESIRED JUST UNCOMMENT THE LOOP BELOW
CCCC      DCIINC=(CIMAX-CIMIN)/(NCDIM-1)
CCCC      DO I=1,NCDIM
CCCC         CI(I)=FLOAT(NCDIM-I)*DCIINC+CIMIN
CCCC         X(I)=1./CI(I)
CCCC         DCI(I)=DCIINC
CCCC      ENDDO

C     DEFINE INTRINSIC VELOCITY U(Z)-U(Z_LAUNCH), AND COEFFICIENTS
      DO IA=1,NADIM
         DO IL=IL1,IL2
            UL(IL,IA)=UNIT(1,IA)*UB(IL,LAUN_Z)+UNIT(2,IA)*VB(IL,LAUN_Z)
         ENDDO
      ENDDO
      DO IL=IL1,IL2
         NL(IL)=BVB(IL,LAUN_Z)
      ENDDO

            
      DO L=1,LAUN_Z
         DO IA=1,NADIM
            DO IL=IL1,IL2
               U=UNIT(1,IA)*UB(IL,L)+UNIT(2,IA)*VB(IL,L)
               UI(IL,IA,L)=U-UL(IL,IA)
            ENDDO
         ENDDO
      ENDDO

C     SET PROPORTIONALITY CONSTANTS
      DO L=1,LAUN_Z
         DO IL=IL1,IL2
            F=2.E0*ROT*ABS(SIN(RADJ(IL)))
            IF(PEXP.NE.1.) THEN
               RD=1.-PEXP
               BS=RD/(BVB(IL,L)**RD-F**RD)
            ELSE
               BS=1./LOG(BVB(IL,L)/F)
            ENDIF
            FCT(IL,L)=BS*RHOB(IL,L)/BVB(IL,L)
         ENDDO
      ENDDO

C     INITIALIZE INTEGRATED E-P FLUX TO ZERO
      DO IA=1,NADIM
         DO L=1,ILEV
            DO IL=IL1,IL2
               PU(IL,L,IA)=0.E0
               CRT(IL,L,IA)=0.E0
               DFL(IL,L,IA)=0.E0
            ENDDO
         ENDDO      
      ENDDO         

C     INITIALIZE TENDENCIES TO ZERO
      DO L=1,ILEV
         DO IL=IL1,IL2
            UTEND(IL,L)=0.E0
            VTEND(IL,L)=0.E0
         ENDDO
      ENDDO

C     ZERO FINAL EP FLUX VECTOR AND FORCING VECTOR
      DO L=1,ILEV
         DO IL=IL1,IL2
          EPF(IL,L,1)=0.D0
          EPF(IL,L,2)=0.D0
          ENDDO
      ENDDO



C     SET LAUNCH EP FLUX DENSITY
C     DO THIS FOR ONLY ONE AZIMUTH SINCE IT IS IDENTICAL TO ALL AZIMUTHS
C     AND IT WILL BE RENORMALIZED
C     S=1 CASE
CCCC      DO IC=1,NCDIM
CCCC         CIN=CI(IC)
CCCC         CIN4=(MS*CIN)**4
CCCC         DO IL=IL1,IL2
CCCC            BV4=NL(IL)**4
CCCC            DENOM=(BV4+CIN4)
CCCC            EP(IL,IC,1)=FCT(IL,LAUN_Z)*BV4*CIN/(BV4+CIN4)
CCCC            ACT(IL,IC,1)=1.E0
CCCC         ENDDO
CCCC      ENDDO
C     S=-1 CASE
CCCC      DO IC=1,NCDIM
CCCC         CIN=CI(IC)
CCCC         CIN2=(MS*CIN)**2
CCCC         DO IL=IL1,IL2
CCCC            BV2=NL(IL)**2
CCCC            EP(IL,IC,1)=FCT(IL,LAUN_Z)*BV2*CIN/(BV2+CIN2)
CCCC            ACT(IL,IC,1)=1.E0
CCCC         ENDDO
CCCC      ENDDO
C     S=0 CASE
      DO IC=1,NCDIM
         CIN=CI(IC)
         CIN3=(MS*CIN)**3
         DO IL=IL1,IL2
            BV3=NL(IL)**3
            EP(IL,IC,1)=FCT(IL,LAUN_Z)*BV3*CIN/(BV3+CIN3)
            ACT(IL,IC,1)=1.E0
            ACC(IL,IC,1)=1.E0
         ENDDO
      ENDDO


C     NORMALIZE LAUNCH EP FLUX
C     FIRST INTEGRATE
      DO IC=1,NCDIM
         CINC=DCI(IC)
         DO IL=IL1,IL2
            PU(IL,LAUN_Z,1)=PU(IL,LAUN_Z,1)+EP(IL,IC,1)*CINC
         ENDDO
      ENDDO
      DO IL=IL1,IL2
         FNORM(IL)=EPLAUNCH/PU(IL,LAUN_Z,1)
      ENDDO
      DO IA=1,NADIM
         DO IL=IL1,IL2
            PU(IL,LAUN_Z,IA)=EPLAUNCH
         ENDDO
      ENDDO

C     ADJUST PROPORTIONALITY CONSTANT FCT USED TO NORMALIZE SATURATION
C     BOUND
      DO L=1,LAUN_Z
         DO IL=IL1,IL2
            FCT(IL,L)=FNORM(IL)*FCT(IL,L)
         ENDDO
      ENDDO

C     RENORMALIZE EACH SPECTRAL ELEMENT IN ONE AZIMUTH
      DO IC=1,NCDIM
         DO IL=IL1,IL2
            EP(IL,IC,1)=FNORM(IL)*EP(IL,IC,1)
         ENDDO
      ENDDO

C     COPY RESULTS INTO ALL OTHER AZIMUTHS
C     ACT - HAS A VALUE OF 1 OR 0 USED TO IENTIFY ACTIVE SPECTRAL
C           ELEMENTS (IE THOSE THAT HAVE NOT BEEN REMOVED BY CRITICAL 
C           LEVELS
      DO IA=2,NADIM
         DO IC=1,NCDIM
            DO IL=IL1,IL2
               EP(IL,IC,IA)=EP(IL,IC,1)
               ACT(IL,IC,IA)=1.E0
               ACC(IL,IC,IA)=1.E0
            ENDDO
         ENDDO
      ENDDO

C     BEGIN MAIN LOOP OVER LEVELS

      DO L=LAUN_Z-1,1,-1

         DO IA=1,NADIM

C     FIRST DO CRITICAL LEVELS
            DO IL=IL1,IL2
               CI_MIN(IL,IA)=MAX(CI_MIN(IL,IA),UI(IL,IA,L))               
            ENDDO


C     SET ACT TO ZERO IF CRITICAL LEVEL ENCOUNTERED
            DO IC=1,NCDIM
               CIN=CI(IC)
               DO IL=IL1,IL2
                  ATMP=0.5E0+SIGN(P5,CIN-CI_MIN(IL,IA))
                  ACC(IL,IC,IA)=ACT(IL,IC,IA)-ATMP
                  ACT(IL,IC,IA)=ATMP
               ENDDO
            ENDDO

C     INTEGRATE TO GET CRITICAL-LEVEL CONTRIBUTION TO MOM DEPOSITION ON THIS LEVEL
            DO IC=1,NCDIM
               CINC=DCI(IC)
               DO IL=IL1,IL2
                  DFL(IL,L,IA)=DFL(IL,L,IA)+
     1                        ACC(IL,IC,IA)*EP(IL,IC,IA)*CINC
               ENDDO
            ENDDO

C     GET WEIGHTED AVERAGE OF PHASE SPEED IN LAYER
            DO IL=IL1,IL2
               IF(DFL(IL,L,IA).GT.0.) THEN
                  DO IC=1,NCDIM
                     CRT(IL,L,IA)=CRT(IL,L,IA)+CI(IC)*
     1                    ACC(IL,IC,IA)*EP(IL,IC,IA)*DCI(IC)
                  ENDDO
                  CRT(IL,L,IA)=CRT(IL,L,IA)/DFL(IL,L,IA)
               ELSE
                  CRT(IL,L,IA)=CRT(IL,L+1,IA)
               ENDIF
            ENDDO


C     DO SATURATION
C     SLOW FOR IPREAL
            IF(IPREAL.NE.1) THEN
               RD=2.-PEXP
               DO IC=1,NCDIM
                  CIN=CI(IC)
                  DO IL=IL1,IL2
                     F0=ABS(CIN-UI(IL,IA,L))
                     EPS=FCT(IL,L)*F0*(F0/CIN)**RD
                     DEP=ACT(IL,IC,IA)*(EP(IL,IC,IA)-EPS)
                     IF(DEP.GT.0.E0) EP(IL,IC,IA)=EPS
                  ENDDO
               ENDDO
            ELSEIF(PTWO.EQ.3.E0) THEN
              DO IC=1,NCDIM
                  CIN=CI(IC)
                  DO IL=IL1,IL2
                     F0=CIN-UI(IL,IA,L)
                     F1=FCT(IL,L)*F0
                     EPSQ=F1*F1*F0/CIN
                     DEP=ACT(IL,IC,IA)*(EP(IL,IC,IA)**2-EPSQ)
                     IF(DEP.GT.0.E0) EP(IL,IC,IA)=SQRT(EPSQ)
                  ENDDO
               ENDDO
            ELSEIF(PTWO.EQ.2.E0) THEN
               DO IC=1,NCDIM
                  CIN=CI(IC)
                  DO IL=IL1,IL2
                     EPS=FCT(IL,L)*(CIN-UI(IL,IA,L))**2/CIN
                     DEP=ACT(IL,IC,IA)*(EP(IL,IC,IA)-EPS)
                     IF(DEP.GT.0.E0) EP(IL,IC,IA)=EPS
                  ENDDO
               ENDDO
            ENDIF



C     INTEGRATE SPECTRUM
            DO IC=1,NCDIM
               CINC=DCI(IC)
               DO IL=IL1,IL2
                  PU(IL,L,IA)=PU(IL,L,IA)+
     1                        ACT(IL,IC,IA)*EP(IL,IC,IA)*CINC
               ENDDO
            ENDDO

         ENDDO
      ENDDO

C     MAKE CORRECTION FOR CRITICAL-LEVEL MOMENTUM DEPOSITION
      DO IA=1,NADIM
         DO IL=IL1,IL2
            CNG=0.E0
            ULON=UL(IL,IA)
            DO L=2,LAUN_Z
               ULM=UNIT(1,IA)*UM(IL,L)+UNIT(2,IA)*VM(IL,L)-ULON
               DFL(IL,L-1,IA)=DFL(IL,L-1,IA)+CNG
               DFT=MIN(DFL(IL,L-1,IA),(ZB(IL,L-1)-ZB(IL,L))*RHOM(IL,L)*
     1                 (CRT(IL,L-1,IA)-ULM)*TWODTINV)
               DFT=MAX(DFT,ZERO)
               CNG=(DFL(IL,L-1,IA)-DFT)
           ENDDO
         ENDDO
      ENDDO

C     SUM CONTRIBUTION FOR TOTAL ZONAL AND MERIDIONAL FLUX
      DO IA=1,NADIM
         DO L=LAUN_Z,1,-1
            DO IL=IL1,IL2
               EPF(IL,L,1)=EPF(IL,L,1)+(PU(IL,L,IA))
     1              *AZ_FCT*UNIT(1,IA)
               EPF(IL,L,2)=EPF(IL,L,2)+(PU(IL,L,IA))
     1              *AZ_FCT*UNIT(2,IA)
            ENDDO
         ENDDO
      ENDDO

C     PERFORM VERTICAL DIFFERENCE OF FLUX TO GET FORCING TENDENCY
      DO L=2,LAUN_Z
         DO IL=IL1,IL2
            S1=ZB(IL,L-1)-ZB(IL,L)
            AFCT=-1.E0/(RHOM(IL,L)*S1)
            UTEND(IL,L)=AFCT*(EPF(IL,L-1,1)-EPF(IL,L,1)) 
            VTEND(IL,L)=AFCT*(EPF(IL,L-1,2)-EPF(IL,L,2))  
         ENDDO
      ENDDO 

      IMCONS=1
      IF(IMCONS.EQ.1) THEN
C     DEPOSIT ALL MOMENTUM IN TOP LAYER
         L=1
         DO IL=IL1,IL2
            S1=ZB(IL,1)-ZB(IL,2)
            AFCT=-1.E0/(RHOM(IL,L)*S1)
            AFDP=RHOM(IL,L)
            UTEND(IL,L)=-AFCT*(EPF(IL,L,1))
            VTEND(IL,L)=-AFCT*(EPF(IL,L,2))
         ENDDO
      ELSE
c     LET MOM ESCAPE OUT THE TOP OF MODEL
         DO IL=IL1,IL2
            UTEND(IL,1)=0.E0
            VTEND(IL,1)=0.E0
         ENDDO
      ENDIF

      RETURN
      END  
