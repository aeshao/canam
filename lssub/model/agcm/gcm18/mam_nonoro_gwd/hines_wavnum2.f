      SUBROUTINE HINES_WAVNUM2(M_ALPHA,SIGMA_ALPHA,SIGSQH_ALPHA,SIGMA_T,
     1                         AK_ALPHA,VEL_U,VEL_V,UBOT,VBOT,VISC_MOL,
     2                         DENSITY,DENSB,BVFREQ,BVFB,
     3                         RMS_WIND,V_ALPHA,I_ALPHA,MMIN_ALPHA,
     4                         DO_ALPHA,DRAGIL,DRAG,M_MIN,KSTAR,SLOPE,
     5                         F1,F2,F3,NAZ,LEVBOT,LEVTOP,IL1,IL2,
     6                         NLONS,NLEVS,NAZMTH,N_OVER_M,SIGFAC)
C
C  This routine calculates the cutoff vertical wavenumber and velocity
C  variances on a longitude by altitude grid needed for the Hines' Doppler 
C  spread gravity wave drag parameterization scheme.
C
C  Aug. 10/95 - C. McLandress
C
C  Modifications 
C  -------------
C  Aug. 8/99 - C. McLandress (nonzero M_MIN for all other values of SLOPE)
C  Feb. 2/96 - C. McLandress (added: minimum cutoff wavenumber M_MIN;
C                             12 and 16 azimuths; inclusion of introwaves
C                             by UBOT and VBOT; diffusion and heating
C                             calculated at half level; logical flags;
C                             new print routine) 
C
C  Output arguements:
C  ------------------
C
C     * M_ALPHA      = cutoff wavenumber at each azimuth (1/m).
C     * SIGMA_ALPHA  = total rms wind in each azimuth (m/s).
C     * SIGSQH_ALPHA = portion of wind variance from waves having wave
C     *                normals in the alpha azimuth (m/s).
C     * SIGMA_T      = total rms horizontal wind (m/s).
C     * AK_ALPHA     = spectral amplitude factor at each azimuth 
C     *                (i.e.,{AjKj}) in m^4/s^2.
C     * DRAGIL       = logical flag indicating longitudes and levels where 
C     *                calculations to be performed.
C
C  Input arguements:
C  -----------------
C
C     * VEL_U    = background zonal wind component (m/s).
C     * VEL_V    = background meridional wind component (m/s).
C     * UBOT     = background zonal wind component at bottom level.
C     * VBOT     = background meridional wind component at bottom level.
C     * VISC_MOL = molecular viscosity (m^2/s)
C     * DENSITY  = background density (kg/m^3).
C     * DENSB    = background density at model bottom (kg/m^3).
C     * BVFREQ   = background Brunt Vassala frequency (radians/sec).
C     * BVFB     = background Brunt Vassala frequency at model bottom.
C     * RMS_WIND = root mean square gravity wave wind at lowest level (m/s).
C     * DRAG     = logical flag indicating longitudes where calculations
C     *            to be performed.
C     * KSTAR    = typical gravity wave horizontal wavenumber (1/m).
C     * M_MIN    = minimum allowable cutoff vertical wavenumber. 
C     * SLOPE    = slope of incident vertical wavenumber spectrum.
C     * F1,F2,F3 = Hines's fudge factors.
C     * NAZ      = number of horizontal azimuths used (4, 8, 12 or 16).
C     * LEVBOT   = index of lowest vertical level.
C     * LEVTOP   = index of highest vertical level. 
C     * IL1      = first longitudinal index to use (IL1 >= 1).
C     * IL2      = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * NLONS    = number of longitudes.
C     * NLEVS    = number of vertical levels.
C     * NAZMTH   = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Input work arrays:
C  -----------------
C
C     * V_ALPHA    = wind component at each azimuth (m/s). 
C     * I_ALPHA    = Hines' integral at a single level.
C     * MMIN_ALPHA = minimum value of cutoff wavenumber.
C     * DO_ALPHA   = logical flag indicating azimuths and longitudes
C     *              where calculations to be performed.
C
      IMPLICIT NONE
      INTEGER  NAZ, LEVBOT, LEVTOP, IL1, IL2, NLONS, NLEVS, NAZMTH
cccc      LOGICAL DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH), DRAGIL(NLONS,NLEVS)
      INTEGER DRAG(NLONS), DO_ALPHA(NLONS,NAZMTH), DRAGIL(NLONS,NLEVS)
      REAL    SLOPE, M_MIN, KSTAR, F1, F2, F3
      REAL    M_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGMA_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGSQH_ALPHA(NLONS,NLEVS,NAZMTH)
      REAL    SIGMA_T(NLONS,NLEVS)
      REAL    AK_ALPHA(NLONS,NAZMTH)
      REAL    VISC_MOL(NLONS,NLEVS)
      REAL    VEL_U(NLONS,NLEVS),    VEL_V(NLONS,NLEVS)
      REAL    UBOT(NLONS),           VBOT(NLONS)
      REAL    DENSITY(NLONS,NLEVS),  DENSB(NLONS)
      REAL    BVFREQ(NLONS,NLEVS),   BVFB(NLONS),  RMS_WIND(NLONS)
      REAL    I_ALPHA(NLONS,NAZMTH), MMIN_ALPHA(NLONS,NAZMTH)
      REAL    V_ALPHA(NLONS,NAZMTH)
C
C Internal variables.
C -------------------
C
cccc      INTEGER  IBIG
cccc      PARAMETER  ( IBIG = 1000 )
cccc      REAL  N_OVER_M(IBIG), SIGFAC(IBIG)
      real N_OVER_M(nlons),SIGFAC(nlons)
      INTEGER  I, L, N, LSTART, LINCR, LBELOW, ICOUNT
      REAL    M_SUB_M_TURB, M_SUB_M_MOL, M_TRIAL, MMSQ, MMSP1
      REAL    VISC, VISC_MIN, AZFAC, SP1
      DATA  VISC_MIN / 1.E-10 /
C-----------------------------------------------------------------------     
C
C  Quit if work arrays not big enough.
C
cccc      IF (IBIG.LT.NLONS)  THEN
cccc        WRITE (6,*)
cccc        WRITE (6,*) ' HINES_WAVNUM2: increase IBIG'
cccc        STOP
cccc      END IF
C
      SP1 = SLOPE + 1.
      MMSQ = M_MIN**2
      MMSP1 = M_MIN**SP1
C
C  Indices of levels to process.
C
      IF (LEVBOT.GT.LEVTOP)  THEN
        LSTART = LEVBOT - 1     
        LINCR  = -1
      ELSE
        LSTART = LEVBOT + 1
        LINCR  = 1
      END IF
C
C  Initialize logical flags and determine number of longitudes
C  at bottom where calculations to be done.
C
      DO 5 L = 1,NLEVS
      DO 5 I = IL1,IL2
cccc        DRAGIL(I,L) = .FALSE.
        DRAGIL(I,L) = 0
 5    CONTINUE
      ICOUNT = 0
      DO 10 I = IL1,IL2
        IF (DRAG(I).eq.1)  ICOUNT = ICOUNT + 1
        DRAGIL(I,LEVBOT) = DRAG(I)
 10   CONTINUE
      DO 15  N = 1,NAZMTH
      DO 15  I = IL1,IL2
        DO_ALPHA(I,N) = DRAG(I)
 15   CONTINUE
C
C  Use horizontal isotropy to calculate azimuthal variances at bottom level.
C
      AZFAC = 1. / FLOAT(NAZ)
      DO 20 N = 1,NAZ
      DO 20 I = IL1,IL2
        IF (DRAG(I).eq.1)  THEN
          SIGSQH_ALPHA(I,LEVBOT,N) = AZFAC * RMS_WIND(I)**2
        END IF
 20   CONTINUE
C
C  Velocity variances at bottom level.
C
      CALL HINES_SIGMA ( SIGMA_T, SIGMA_ALPHA, 
     ^                   SIGSQH_ALPHA, DRAG, NAZ, LEVBOT, 
     ^                   IL1, IL2, NLONS, NLEVS, NAZMTH)
C
C  Calculate cutoff wavenumber and spectral amplitude factor 
C  at bottom level where it is assumed that background winds vanish
C  and also initialize minimum value of cutoff wavnumber.
C
        IF (SLOPE.EQ.1.)  THEN
          DO 30 N = 1,NAZ
          DO 30 I = IL1,IL2
            IF (DRAG(I).eq.1)  THEN
              M_ALPHA(I,LEVBOT,N) =  BVFB(I) / 
     ^                           ( F1 * SIGMA_ALPHA(I,LEVBOT,N) 
     ^                           + F2 * SIGMA_T(I,LEVBOT) )
              AK_ALPHA(I,N) = 2. * SIGSQH_ALPHA(I,LEVBOT,N) /
     ^                        ( M_ALPHA(I,LEVBOT,N)**2 - MMSQ ) 
              MMIN_ALPHA(I,N) = M_ALPHA(I,LEVBOT,N)
cccc              if(AK_ALPHA(I,N).lt.0.) then
cccc                 write(6,*) "####### AK_ALPHA(i,n) lesss thn zero "
cccc             write(6,*) "####","i,n,levbot,f1,f2"
cccc             write(6,*) "####",i,n,levbot,f1,f2
cccc             write(6,*) "####","SIGSQH_ALPHA(I,LEVBOT,N),bvfb(i)"
cccc             write(6,*) "####",SIGSQH_ALPHA(I,LEVBOT,N),bvfb(i)
cccc           write(6,*) "####","SIGMA_ALPHA(I,LEVBOT,N),SIGMA_T(I,LEVBOT)"
cccc             write(6,*) "####",SIGMA_ALPHA(I,LEVBOT,N),SIGMA_T(I,LEVBOT)
cccc             write(6,*) "####","M_ALPHA(I,LEVBOT,N)**2 - MMSQ"
cccc             write(6,*) "####",M_ALPHA(I,LEVBOT,N)**2 - MMSQ
cccc             write(6,*) "####","M_ALPHA(I,LEVBOT,N)**2,MMSQ"
cccc             write(6,*) "####",M_ALPHA(I,LEVBOT,N)**2,MMSQ
cccc              endif
            END IF
 30       CONTINUE
        ELSE
          DO 40 N = 1,NAZ
          DO 40 I = IL1,IL2
            IF (DRAG(I).eq.1)  THEN
              M_ALPHA(I,LEVBOT,N) =  BVFB(I) / 
     ^                           ( F1 * SIGMA_ALPHA(I,LEVBOT,N) 
     ^                           + F2 * SIGMA_T(I,LEVBOT) )
              AK_ALPHA(I,N) = SP1 * SIGSQH_ALPHA(I,LEVBOT,N) / 
     ^                        ( M_ALPHA(I,LEVBOT,N)**SP1 - MMSP1 )
              MMIN_ALPHA(I,N) = M_ALPHA(I,LEVBOT,N)
            END IF
 40       CONTINUE
        END IF
C
C  Calculate quantities from the bottom upwards, 
C  starting one level above bottom.
C
      DO 150 L = LSTART,LEVTOP,LINCR
C
C  Return to calling program if no more longitudes left to do.
C
        IF (ICOUNT.EQ.0)  GO TO 160
C
C  Level beneath present level.
C
        LBELOW = L - LINCR 
C
C  Compute azimuthal wind components from zonal and meridional winds.
C
        CALL HINES_WIND ( V_ALPHA, 
     ^                    VEL_U(1,L), VEL_V(1,L), UBOT, VBOT, 
     ^                    DRAGIL(1,LBELOW), NAZ, IL1, IL2, 
     ^                    NLONS, NAZMTH )
C
C  Calculate N/m_M where m_M is maximum permissible value of the vertical
C  wavenumber (i.e., m > m_M are obliterated) and N is buoyancy frequency.
C  m_M is taken as the smaller of the instability-induced 
C  wavenumber (M_SUB_M_TURB) and that imposed by molecular viscosity
C  (M_SUB_M_MOL). Since variance at this level is not yet known
C  use value at level below.
C
        DO 50 I = IL1,IL2
          IF (DRAGIL(I,LBELOW).eq.1)  THEN
            VISC = MAX ( VISC_MOL(I,L), VISC_MIN )
            M_SUB_M_TURB = BVFREQ(I,L) / ( F2 * SIGMA_T(I,LBELOW) )
            M_SUB_M_MOL  = (BVFREQ(I,L)*KSTAR/VISC)**0.33333333/F3
            IF (M_SUB_M_TURB .LT. M_SUB_M_MOL)  THEN
              N_OVER_M(I) = F2 * SIGMA_T(I,LBELOW)
            ELSE
              N_OVER_M(I) = BVFREQ(I,L) / M_SUB_M_MOL 
            END IF
          END IF
  50    CONTINUE
C
C  Calculate cutoff wavenumber at this level.
C
        DO 60 N = 1,NAZ
        DO 60 I = IL1,IL2
C
          IF (DO_ALPHA(I,N).eq.1)  THEN 
C
C  Calculate trial value (since variance at this level is not yet known
C  use value at level below). If trial value is negative or if it exceeds 
C  minimum value found at lower levels (not permitted) then set it 
C  to this minimum value. 
C
            M_TRIAL = BVFB(I) / ( F1 * SIGMA_ALPHA(I,LBELOW,N)  
     ^                + N_OVER_M(I) + V_ALPHA(I,N) )
            IF (M_TRIAL.LE.0. .OR. M_TRIAL.GT.MMIN_ALPHA(I,N))  THEN
              M_TRIAL = MMIN_ALPHA(I,N)
            END IF
            M_ALPHA(I,L,N) = M_TRIAL
C
C  Do not permit cutoff wavenumber to be less than minimum allowable value.
C
            IF (M_ALPHA(I,L,N) .LT. M_MIN)  M_ALPHA(I,L,N) = M_MIN
C
C  Reset minimum value of cutoff wavenumber if necessary.
C
            IF (M_ALPHA(I,L,N) .LT. MMIN_ALPHA(I,N))  THEN
              MMIN_ALPHA(I,N) = M_ALPHA(I,L,N)
            END IF
C
          ELSE
C
            M_ALPHA(I,L,N) = M_MIN
C
          END IF          
C
 60     CONTINUE
C
C  Calculate the Hines integral at this level.
C
        CALL HINES_INTGRL2 (I_ALPHA, 
     ^                      V_ALPHA, M_ALPHA, BVFB, 
     ^                      DRAGIL(1,L), DO_ALPHA, M_MIN, SLOPE, NAZ, 
     ^                      L, IL1, IL2, NLONS, NLEVS, NAZMTH )
C
C  Calculate the velocity variances at this level.
C
        DO 80 I = IL1,IL2
          IF (DRAGIL(I,L).eq.1)  THEN
            SIGFAC(I) = DENSB(I) / DENSITY(I,L) 
     ^                  * BVFREQ(I,L) / BVFB(I) 
cccc            if(SIGFAC(I).lt.0.) then
cccc               write(6,*) "##### SIGFAC(I).lt.0."
cccc               write(6,*) "####","i,l,SIGFAC(I) ",i,l,SIGFAC(I)
cccc               write(6,*) "####","DENSB(I),DENSITY(I,L)"
cccc               write(6,*) "####",DENSB(I),DENSITY(I,L)
cccc               write(6,*) "####","BVFREQ(I,L),BVFB(I)"
cccc               write(6,*) "####",BVFREQ(I,L),BVFB(I)
cccc            endif
          END IF
 80     CONTINUE
C
C  Calculate the velocity variances at this level.
C
        DO 90 N = 1,NAZ
        DO 90 I = IL1,IL2
          IF (DRAGIL(I,L).eq.1)  THEN
            SIGSQH_ALPHA(I,L,N) = SIGFAC(I) * AK_ALPHA(I,N) 
     ^                            * I_ALPHA(I,N)
cccc            if(SIGSQH_ALPHA(I,L,N).lt.0.) then
cccc               write(6,*) "###### SIGSQH_ALPHA(I,L,N) is lt 0."
cccc               write(6,*) "####","i,l,n ",i,l,n
cccc               write(6,*) "####","SIGFAC(I),AK_ALPHA(I,N),I_ALPHA(I,N)"
cccc               write(6,*) "####",SIGFAC(I),AK_ALPHA(I,N),I_ALPHA(I,N)
cccc            endif
          END IF
  90    CONTINUE
        CALL HINES_SIGMA ( SIGMA_T, SIGMA_ALPHA, 
     ^                     SIGSQH_ALPHA, DRAGIL(1,L), NAZ, L, 
     ^                     IL1, IL2, NLONS, NLEVS, NAZMTH )
C
C  If total rms wind zero (no more drag) then set DRAG to false 
C  and update longitude counter.
C
        DO 110 I = IL1,IL2 
          IF (SIGMA_T(I,L) .EQ. 0.)  THEN
            DRAGIL(I,L) = 0
            ICOUNT = ICOUNT - 1   
          END IF
 110    CONTINUE
C
C  End of level loop.
C
 150  CONTINUE
C
 160  CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
