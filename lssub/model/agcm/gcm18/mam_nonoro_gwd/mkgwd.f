      SUBROUTINE MKGWD(U,PF,PH,TF,TH,BVFR,DP,GWKH,RG,
     1           AX,AXX,USQ,SIGMA,ZKZR,OMEG,ALPHA,BETA,
     2           RMS,ILEV,NHAR,LEVGWS,LUPP,ILG,IL1,IL2,
     3           v2,drag,gwkhu,vv2,xx,dz,bvkh,vv) 

C Vectorized version:  A. MEDVEDEV (1998)   OCT 27
C
C INPUT PARAMETERS:
C     U(ILG,ILEV)   - ARRAY OF BACKGROUND WIND
C    PF(ILG,ILEV)   - STANDARD PRESSURE LEVELS IN "FULL" POINTS
C    PH(ILG,ILEV)   - STANDARD PRESSURE LEVELS IN "HALF" POINTS
C    TF(ILG,ILEV)   - TEMPERATURE IN "FULL" VERTICAL POINTS(THROW)
C    TH(ILG,ILEV)   - TEMPERATURE IN "HALF" VERTICAL POINTS(TFROW)
C    BVFR(ILG,ILEV) - BRUNT-VAISALA FREQUENCY IN "FULL" POINTS
C    GWKH       - HORIZONTAL WAVENUMBER
C    RG         - RGAS/GRAV
C    NHAR       - NUMBER OF HARMONICS IN THE SPECTRUM
C    LEVGWS     - HEIGHT LEVEL NUMBER WHERE GW SOURCE IS SPECIFIED
C    LUPP       - UPPER LEVEL FOR CALCULATION OF GWD (=ILEV usually)

C OUTPUT DATA:
C     USQ(ILG,NHAR,ILEV)- HORIZONTAL VELOCITY SQUARED
C     AXILG,(ILEV)      - VERTICAL PROFILE OF GWD 
C     RMS(ILG,ILEV)     - RMS HORIZONTAL WIND

C WORKING ARRAYS:
C     SIGMA(ILG,NHAR)  - INTRINSIC FREQUENCIES
C     ZKZR(ILG,NHAR,ILEV)-VWN 
C     OMEG(ILG,NHAR,ILEV)-FREQUENCIES
C     ALPHA(ILG,NHAR)
C     BETA(NHAR,ILEV)-DAMPING RATES
C//////////////////////////////////////////////////////////////////////

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C      PARAMETER(NHAR=15, VPZERO=0.0)
      PARAMETER(VPZERO=0.0)
      REAL     U(ILG,ILEV), PF(ILG,ILEV),   PH(ILG,ILEV)
      REAL    TF(ILG,ILEV), TH(ILG,ILEV), BVFR(ILG,ILEV)
      REAL    AX(ILG,ILEV),AXX(ILG,ILEV),   DP(ILG,ILEV)
      REAL   RMS(ILG,ILEV)

      REAL    USQ(ILG,NHAR,ILEV), ZKZR(ILG,NHAR,ILEV) 
      REAL   OMEG(ILG,NHAR,ILEV), BETA(ILG,NHAR,ILEV)
      REAL   SIGMA(ILG,NHAR), ALPHA(ILG,NHAR)

      REAL   V2(ilg,NHAR), DRAG(ilg,NHAR), GWKHU(ilg), VV2(ilg)
      REAL   XX(ilg), DZ(ilg), BVKH(ilg), VV(ilg)

      SQRT2 =1.4142136
      SQRTP =1.7724539
      SQRT2P=2.5066283
      SQRTKH=SQRT2*GWKH 
      CRITAMPL=1.0E-30*0.6E-7/REAL(NHAR)
c
c Initialize gravity wave drag tendency to zero
c
      L1P=LEVGWS+1
      DO L=1,LUPP
         DO I=IL1,IL2
             AX(I,L)=VPZERO
            AXX(I,L)=VPZERO
         END DO
      END DO
c
c Compute parameters at the lower boundary
c
      LL=ILEV+1-LEVGWS
      DO I=IL1,IL2
         GWKHU(I)=GWKH*U(I,LEVGWS)
         VV2(I)=VPZERO
      END DO

      DO K=1,NHAR
         DO I=IL1,IL2
            SIGMA(I,K)=OMEG(I,K,LEVGWS)+GWKHU(I)
            ZKZR(I,K,LEVGWS)=GWKH*BVFR(I,LL)/OMEG(I,K,LEVGWS)
            VV2(I)=VV2(I)+USQ(I,K,LEVGWS)
            V2(I,K)=VV2(I)
            VV(I)=SQRT(VV2(I))
            ALPHA(I,K)=OMEG(I,K,LEVGWS)/SQRT2/(GWKH*VV(I))
c     * add this since it appears that beta(i,k,LEVGWS) has not been
c     * assigned
            beta(i,k,LEVGWS)=0.
         END DO
      END DO
c
c Step-by-step integration upward
c
      DO L=L1P,LUPP
         LL=ILEV+1-L
         LM=L-1
         LLM=LL+1
         DO I=IL1,IL2
            GWKHU(I)=GWKH*U(I,L)
            XX(I)=TF(I,LL)*PF(I,LM)/TF(I,LLM)/PF(I,L)
            DZ(I)=-RG*DP(I,LM)*TH(I,LL)/PH(I,L)
            BVKH(I)=BVFR(I,LL)*GWKH
         END DO
c
         DO NH=1,NHAR
            DO I=IL1,IL2       !.......... Longitude loop begins........
c
c Exclude harmonics which have been filtered out by critical levels
c
            IF(USQ(I,NH,LM) .LE. CRITAMPL) THEN
               USQ(I,NH,L)=VPZERO
               DRAG(I,NH) =VPZERO
            ELSE
c
c Calculate only for harmonics with nonzero amplitude, i.e. which haven't
c met critical levels yet
c
               VV(I)=SQRT(V2(I,NH)) 
               OMEG(I,NH,L)=SIGMA(I,NH)-GWKHU(I)
               ALPHA1=OMEG(I,NH,L)/(SQRTKH*VV(I))
               A1 =ALPHA1*ALPHA(I,NH)
               A11=ALPHA1*ALPHA1   
               IF((A1.LE.VPZERO).OR.(A11.LE.0.5)) THEN
                  USQ(I,NH,L)=VPZERO
                  DRAG(I,NH) =VPZERO
               ELSE 
                  ALPHA(I,NH)=ALPHA1
                  BETA(I,NH,L)=SQRT2P*EXP(-A11)*BVFR(I,LL)/VV(I)
                  ZKZR(I,NH,L)=BVKH(I)/OMEG(I,NH,L)
                  XXX=ZKZR(I,NH,L)/ZKZR(I,NH,LM)
                  X=XX(I)*XXX
                  BET=0.5*(BETA(I,NH,L)+BETA(I,NH,LM))
                  USQ(I,NH,L)=X*EXP(-BET*DZ(I))*USQ(I,NH,LM)
                  DRAG(I,NH)=BET*USQ(I,NH,L)*GWKH/ZKZR(I,NH,L)
               ENDIF
            ENDIF
            END DO             !.......... End of longitute loop........
         END DO

         DO I=IL1,IL2 
            VV2(I)=VPZERO
         END DO

         DO NH=1,NHAR
            DO I=IL1,IL2
               AXX(I,L)=AXX(I,L)+DRAG(I,NH)
               VV2(I)=VV2(I)+USQ(I,NH,L)
               V2(I,NH)=VV2(I)
            END DO
         END DO
 
         DO I=IL1,IL2
            RMS(I,L)=SQRT(V2(I,NHAR))
         END DO

      END DO                   !..........End of altitude loop.........

      DO L=2,LUPP-1
         DO I=IL1,IL2
            AX(I,L)=(AXX(I,L-1)+2.*AXX(I,L)+AXX(I,L+1))*0.25
         END DO
      END DO

      DO I=IL1,IL2
         AX(I,1)=AXX(I,1)
         AX(I,LUPP)=AXX(I,LUPP)
      END DO

      RETURN
      END
