      SUBROUTINE GHG_SCENARIO3(RF_SCENARIO,NRF,CURR_GHG,NGHG)
C
C     * June  24, 2013 - K.Vonsalzen. New version for gcm17:
C     *                               - Add option for PAM
C     *                                 aerosol module.
C     * Oct   02, 2012 - J.Cole.    Modified (upwardly
C     *                             compatible):
C     *                             - Add "TEST" scenario.
C     *                             - Initialize output aerosol
C     *                               values so always exist
C     *                               regardless of scenario 
C     *                               chosen.
C     * April 29, 2012 - Y.Peng.    Previous version ghg_scenario2
C     *                             for gcm16:
C     *                             - Add "AERORF" aerosol
C     *                               forcing definitions.
C     * March 03, 2008 - L.Solheim/ Previous version
C     *                  M.Lazare.  ghg_scenario for gcm15h/i.       
C
C     * set ghg concentrations etc, depending on RF_SCENARIO
C     * and the value of the loop counter NRF.
C
      IMPLICIT NONE
C
      !--- Current values of time dependent GHG constituents
      REAL :: CURR_GHG(NGHG)
C
      CHARACTER(LEN=10) :: RF_SCENARIO
      INTEGER NRF,NGHG
C
C     * INPUT PPM VALUES FROM RADCONS SUBROUTINE:
C
      REAL :: SOLAR_C, CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM,
     1        F12_PPM, F113_PPM, F114_PPM
      COMMON /RADCON/ SOLAR_C, CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM,
     1                F12_PPM, F113_PPM, F114_PPM
C
C     * OUTPUT PPM VALUES BASED ON SCENARIO REQUESTED:
C
      REAL :: SOLAR_C_T, CO2_PPM_T, CH4_PPM_T, N2O_PPM_T,
     1        F11_PPM_T, F12_PPM_T, F113_PPM_T, F114_PPM_T
      COMMON /RADCON_T/ SOLAR_C_T, CO2_PPM_T, CH4_PPM_T, N2O_PPM_T,
     1                  F11_PPM_T, F12_PPM_T, F113_PPM_T, F114_PPM_T
!$OMP THREADPRIVATE (/RADCON_T/)
C
C     * OUTPUT AEROSOL VALUES BASED ON SCENARIO REQUESTED:
C
      REAL :: RMSO4,RMSSA,RMSSC,RMDUA,RMDUC,RMBCO,RMBCY,RMOCO,RMOCY
      COMMON /TRACEA / RMSO4,RMSSA,RMSSC,RMDUA,RMDUC,
     1                 RMBCO,RMBCY,RMOCO,RMOCY
!$OMP THREADPRIVATE (/TRACEA/)
C
C     * OUTPUT BASED ON PAM AEROSOL MODEL.
C
      REAL :: RMAI, RMSO4I, RMOCI, RMBCI, RMOC, RMBC, RMDU, RMSS
      COMMON /PAMSCL/ RMAI, RMSO4I, RMOCI, RMBCI, RMOC, RMBC, RMDU, RMSS
!$OMP THREADPRIVATE (/PAMSCL/)
C=================================================================
C     * INITIALIZE OUTPUT AEROSOL VALUES SO ALWAYS EXIST REGARDLESS
C     * OF CHOSEN SCENARIO.
C
      RMSO4      = 1.0
      RMBCO      = 1.0
      RMBCY      = 1.0
      RMOCO      = 1.0
      RMOCY      = 1.0
      RMSSA      = 1.0
      RMSSC      = 1.0
      RMDUA      = 1.0
      RMDUC      = 1.0
C
C     * DEFAULT PARAMETERS FOR PAM AEROSOL MODEL.
C
      RMAI       = 1.0     ! INTERNALLY MIXED AEROSOL (ACCUMULATION MODE)
      RMSO4I     = 0.0     ! INT. MIXED, WITHOUT SULPHATE
      RMOCI      = 0.0     ! INT. MIXED, WITHOUT ORGANIC CARBON
      RMBCI      = 0.0     ! INT. MIXED, WITHOUT BLACK CARBON
      RMOC       = 1.0     ! EXTERNALLY MIXED ORGANIC CARBON
      RMBC       = 1.0     ! EXTERNALLY MIXED BLACK CARBON
      RMDU       = 1.0     ! EXTERNALLY MIXED MINERAL DUST
      RMSS       = 1.0     ! EXTERNALLY MIXED SEA SALT
C
      IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'2XCO2') THEN
        if (NRF.eq.1) then
          !--- 2x CO2
          CO2_PPM_T = 2.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-1)
        endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'4XCO2') THEN
        if (NRF.eq.1) then
          !--- 4x CO2
          CO2_PPM_T = 4.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-2)
        endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'2X4XCO2') THEN
        if (NRF.eq.1) then
          !--- 2x CO2
          CO2_PPM_T = 2.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else if (NRF.eq.2) then
          !--- 4x CO2
          CO2_PPM_T = 4.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-2)
        endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'CFMIPA') THEN
        if (NRF.eq.1) then
          !--- 2x CO2
          SOLAR_C_T = SOLAR_C
          CO2_PPM_T = 2.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else if (NRF.eq.2) then
          !--- increased solar constant
          SOLAR_C_T = 1.025*SOLAR_C
          CO2_PPM_T = CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else if (NRF.eq.3) then
          !--- no cfcs
          SOLAR_C_T = SOLAR_C
          CO2_PPM_T = CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = 0.0
          F12_PPM_T = 0.0
          F113_PPM_T= 0.0
          F114_PPM_T= 0.0
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-3)
        endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'SRESA1B' .OR.
     &         TRIM(ADJUSTL(RF_SCENARIO)).EQ.'SRESA2'  .OR.
     &         TRIM(ADJUSTL(RF_SCENARIO)).EQ.'SRESB1')     THEN
        !--- These are scenarios used in the IPCC AR4 report.
        !--- NOTE: When these scenario runs were done using GCM13
        !--- aerosol forcing was included via a kludge that used a
        !--- time dependent aerosol loading pattern to modify surface
        !--- reflectance in the short wave. The code to implement
        !--- this kludge was not ported to gcm15.
        if (NRF.eq.1) then
          !--- set ghg concentrations to scenario values
          CO2_PPM_T = CURR_GHG(1)
          CH4_PPM_T = CURR_GHG(2)
          N2O_PPM_T = CURR_GHG(3)
          F11_PPM_T = CURR_GHG(4)
          F12_PPM_T = CURR_GHG(5)
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-4)
        endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'AERORF') THEN
       if  (NRF.eq.1) then
         !--- zero out  SO4 sulfate
         RMSO4  = 0.0
         RMBCO  = 1.0
         RMBCY  = 1.0
         RMOCO  = 1.0 
         RMOCY  = 1.0
         RMSSA  = 1.0
         RMSSC  = 1.0
         RMDUA  = 1.0
         RMDUC  = 1.0
       else if (NRF.eq.2) then
         !--- zero out  SS seasalt
         RMSSA  = 0.0
         RMSSC  = 0.0
         RMSO4  = 1.0
         RMBCO  = 1.0
         RMBCY  = 1.0
         RMOCO  = 1.0 
         RMOCY  = 1.0
         RMDUA  = 1.0
         RMDUC  = 1.0
       else if (NRF.eq.3) then
         !--- zero out  DU dust
         RMDUA  = 0.0
         RMDUC  = 0.0
         RMSO4  = 1.0
         RMBCO  = 1.0
         RMBCY  = 1.0
         RMOCO  = 1.0 
         RMOCY  = 1.0
         RMSSA  = 1.0
         RMSSC  = 1.0
       else if (NRF.eq.4) then
         !--- zero out  BC black carbon
         RMBCO  = 0.0
         RMBCY  = 0.0
         RMSO4  = 1.0
         RMOCO  = 1.0 
         RMOCY  = 1.0
         RMSSA  = 1.0
         RMSSC  = 1.0
         RMDUA  = 1.0
         RMDUC  = 1.0
       else if (NRF.eq.5) then
         !--- zero out  OC organic carbon
         RMOCO  = 0.0
         RMOCY  = 0.0
         RMSO4  = 1.0
         RMBCO  = 1.0 
         RMBCY  = 1.0
         RMSSA  = 1.0
         RMSSC  = 1.0
         RMDUA  = 1.0
         RMDUC  = 1.0
       else if (NRF.eq.6) then
         !--- zero out  all aerosols
         RMSO4  = 0.0
         RMBCO  = 0.0
         RMBCY  = 0.0
         RMOCO  = 0.0 
         RMOCY  = 0.0
         RMSSA  = 0.0
         RMSSC  = 0.0
         RMDUA  = 0.0
         RMDUC  = 0.0
       else
         WRITE(6,6010) NRF
         CALL XIT('GHG_SCENARIO3',-5)
       endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'PAMRF') THEN
       if (NRF.eq.1) then
         !--- zero out all aerosols
         RMSO4I     = 0.0
         RMOCI      = 0.0
         RMBCI      = 0.0
         RMOC       = 0.0
         RMBC       = 0.0
         RMDU       = 0.0
         RMSS       = 0.0
       else if (NRF.eq.2) then
         !--- include all aerosols
         RMSO4I     = 1.0
         RMOCI      = 1.0
         RMBCI      = 1.0
         RMOC       = 1.0
         RMBC       = 1.0
         RMDU       = 1.0
         RMSS       = 1.0
       else if (NRF.eq.3) then
         !--- zero out SS seasalt
         RMSO4I     = 1.0
         RMOCI      = 1.0
         RMBCI      = 1.0
         RMOC       = 1.0
         RMBC       = 1.0
         RMDU       = 1.0
         RMSS       = 0.0
       else if (NRF.eq.4) then
         !--- zero out DU dust
         RMSO4I     = 1.0
         RMOCI      = 1.0
         RMBCI      = 1.0
         RMOC       = 1.0
         RMBC       = 1.0
         RMDU       = 0.0
         RMSS       = 1.0
       else if (NRF.eq.5) then
         !--- zero out fossil and biofuel aerosols
         RMSO4I     = 0.0
         RMOCI      = 0.0
         RMBCI      = 0.0
         RMOC       = 0.0
         RMBC       = 0.0
         RMDU       = 1.0
         RMSS       = 1.0
       else if (NRF.eq.6) then
         !--- zero out internally mixed mode
         RMSO4I     = 0.0
         RMOCI      = 0.0
         RMBCI      = 0.0
         RMOC       = 1.0
         RMBC       = 1.0
         RMDU       = 1.0
         RMSS       = 1.0
       else if (NRF.eq.7) then
         !--- zero out SO4 sulfate
         RMSO4I     = 0.0
         RMOCI      = 1.0
         RMBCI      = 1.0
         RMOC       = 1.0
         RMBC       = 1.0
         RMDU       = 1.0
         RMSS       = 1.0
       else if (NRF.eq.8) then
         !--- zero out BC black carbon
         RMSO4I     = 1.0
         RMOCI      = 1.0
         RMBCI      = 0.0
         RMOC       = 1.0
         RMBC       = 0.0
         RMDU       = 1.0
         RMSS       = 1.0
       else if (NRF.eq.9) then
         !--- zero out OC organic carbon
         RMSO4I     = 1.0
         RMOCI      = 0.0
         RMBCI      = 1.0
         RMOC       = 0.0
         RMBC       = 1.0
         RMDU       = 1.0
         RMSS       = 1.0
       else
         WRITE(6,6010) NRF
         CALL XIT('GHG_SCENARIO',-6)
       endif
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'TEST' .OR.
     1         TRIM(ADJUSTL(RF_SCENARIO)).EQ.'VOL'.OR.
     2         TRIM(ADJUSTL(RF_SCENARIO)).EQ.'GHAN') THEN
         if (NRF.eq.1) then
          CO2_PPM_T  = CO2_PPM
          CH4_PPM_T  = CH4_PPM
          N2O_PPM_T  = N2O_PPM
          F11_PPM_T  = F11_PPM
          F12_PPM_T  = F12_PPM
          F113_PPM_T = F113_PPM
          F114_PPM_T = F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-8)
        endif         
      ELSE IF (TRIM(ADJUSTL(RF_SCENARIO)).EQ.'4XCO2_GHAN') THEN
        if (NRF.eq.1) then
          !--- 4xCO2
          CO2_PPM_T = 4.0*CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else if (NRF.eq.2) then
          !--- GHAN
          CO2_PPM_T = CO2_PPM
          CH4_PPM_T = CH4_PPM
          N2O_PPM_T = N2O_PPM
          F11_PPM_T = F11_PPM
          F12_PPM_T = F12_PPM
          F113_PPM_T= F113_PPM
          F114_PPM_T= F114_PPM
        else
          WRITE(6,6010) NRF
          CALL XIT('GHG_SCENARIO3',-9)
        endif
      ELSE
        WRITE(6,6020) TRIM(RF_SCENARIO)
        CALL XIT('GHG_SCENARIO3',-10)
      ENDIF
C-----------------------------------------------------------------
 6010 FORMAT('GHG_SCENARIO2: NRF is out of range. NRF = ',I5)
 6020 FORMAT('GHG_SCENARIO2: UNKNOWN RF SCENARIO :: ',A10)
      RETURN
      END
