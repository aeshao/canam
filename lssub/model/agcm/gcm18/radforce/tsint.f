      SUBROUTINE TSINT(RVAL,CURR_TIME,TIMES,TS,LD,NIN,NT)
c
c=======================================================================
c     * Mar 24,2004 - L. Solheim - Interpolate an array from a time
c                                  series of arrays.
c=======================================================================
c
      implicit none
c
      real RVAL(NIN)     ! space for return values
      real*8 CURR_TIME   ! current time
      real*8 TIMES(NT)   ! times corresponding to 2nd dim of TS
      real TS(LD,NT)     ! nodal values
      integer LD         ! leading dim of TS
      integer NIN        ! number of values to interpolate
      integer NT         ! number of temporal nodes

      !--- local
      real*8 den
      real w1,w2
      integer i,n1,n2,nn
      integer, parameter :: verbose=0
C=====================================================================
      !--- do some checks
      if (nin.le.0) then
        write(6,*)'TSINT: ***WARNING*** No interpolation done for',
     &            ' NIN=',NIN
        return
      endif
      if (ld.lt.nin) call xit("TSINT",-1)

      !--- Find indicies in times array that bound the current time
      n1=0
      n2=0
      do i=1,nt-1
        if (curr_time.ge.times(i) .and. curr_time.le.times(i+1)) then
          n1=i
          n2=i+1
          exit
        endif
      enddo
      if (n1.eq.0 .or. n2.eq.0) then
        write(6,*)'TSINT: curr_time is out of range. '
        write(6,*)'        curr_time = ',curr_time
        write(6,*)'        times = ',times
        call xit("TSINT",-2)
      endif

      !--- linearly interpolate nin elements to the current time
      den=times(n2)-times(n1)
      if (den.eq.0.0) then
        write(6,*)'TSINT: adjacent nodal values in the TIMES array ',
     &            'are identical at nodes ',n1,' and ',n2
        write(6,*)'       TIMES(',n1,')=',times(n1),
     &            '  TIMES(',n2,')=',times(n2)
        call xit("TSINT",-3)
      endif
      w2=(curr_time-times(n1))/den
      w1=1.0-w2
      do i=1,nin
        RVAL(i)=w1*TS(i,n1)+w2*TS(i,n2)
      enddo
      if (verbose.gt.0) then
        nn=min(5,nin)
        write(6,'(a,i6,a,1pe16.8,a,$)')'node ',n1,':: time ',
     &                                 TIMES(n1),' :: '
        write(6,'(1p6e16.8)')TS(1:nn,n1)
        write(6,'(a,1pe16.8,a,$)')'   int val :: time ',curr_time,' :: '
        write(6,'(1p6e16.8)')RVAL(1:nn)
        write(6,'(a,i6,a,1pe16.8,a,$)')'node ',n2,':: time ',
     &                                 TIMES(n2),' :: '
        write(6,'(1p6e16.8)')TS(1:nn,n2)
        write(6,*)'===================================================='
      endif

      RETURN
c-----------------------------------------------------------------------
      END
