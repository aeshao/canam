      SUBROUTINE RADFLAB(LR,LEVRF,PTOIT,LH,ILEV)

C     * MAR 3/2008 - L.SOLHEIM,M.LAZARE.
C
C     * DEFINES LEVEL INDEX VALUES FOR RADIATIVE FORCING DIAGNOSTIC FIELDS.
C
      IMPLICIT NONE
C
      INTEGER LR(LEVRF), LH(ILEV)
C
      REAL PTOIT
      INTEGER LEVRF,ILEV,LHTOP,L
C-----------------------------------------------------------------------
      CALL LVCODE(LHTOP,1e-5*PTOIT,1)
      DO L=1,LEVRF
        if (L.eq.1) then
          !--- TOA values are stored in first level.
          LR(L)=LHTOP
        else if (L.eq.LEVRF) then
          !--- Surface values are stored in last level. 
          LR(L)=1111
        else
          !--- model levels are stored in between.
          LR(L)=LH(L-1)
        endif
      ENDDO
C
      RETURN
      END
