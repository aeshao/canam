      SUBROUTINE GETDUE(NF,SPOTPAK,ST01PAK,ST02PAK,
     1                  ST03PAK,ST04PAK,ST06PAK,ST13PAK,
     2                  ST14PAK,ST15PAK,ST16PAK,ST17PAK,
     3                  IJPAK,NLON,NLAT,GG)
C
C     * MAY/08 - Y.PENG. NEW ROUTINE FOR GCM16 TO GET
C     *                  FIELDS REQUIRED FOR NEW DUST
C     *                  EMISSION SCHEME.   
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * INVARIANT FIELDS:
C
      REAL SPOTPAK(IJPAK),ST01PAL(IJPAK)
      REAL ST02PAK(IJPAK),ST03PAK(IJPAK),ST04PAK(IJPAK)
      REAL ST06PAK(IJPAK),ST13PAK(IJPAK),ST14PAK(IJPAK)
      REAL ST15PAK(IJPAK),ST16PAK(IJPAK),ST17PAK(IJPAK)

C
C     * WORK FIELD:
C
      REAL GG(1)
C-----------------------------------------------------------------------
C     * GRIDS READ FROM FILE NF.

      REWIND NF
C
      CALL GETGGBX(SPOTPAK,NC4TO8("SPOT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST01PAK,NC4TO8("ST01"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST02PAK,NC4TO8("ST02"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST03PAK,NC4TO8("ST03"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST04PAK,NC4TO8("ST04"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST06PAK,NC4TO8("ST06"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST13PAK,NC4TO8("ST13"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST14PAK,NC4TO8("ST14"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST15PAK,NC4TO8("ST15"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST16PAK,NC4TO8("ST16"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST17PAK,NC4TO8("ST17"),NF,NLON,NLAT,0,1,GG)
C
      RETURN
      END
