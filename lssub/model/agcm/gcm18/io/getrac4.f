      SUBROUTINE GETRAC4(NF,TRAC,LA,LM,ILEV,LH,ITRAC,NTRAC,NTRACA,
     1                   INDXA,ITRNAM,ITRIC,
     2                   LATOTAL,LMTOTAL,LSRTOTAL,GLL)
C
C     * NOV 03/03 - M.LAZARE.   NEW VERSION BASED ON GETRAC3, WHICH
C     *                         CALLS NEW ROUTINE "GETSPEN" USING
C     *                         WORK ARRAY "GLL", TO GET LOAD-BALANCED
C     *                         SPECTRAL DATA APPLICABLE TO EACH NODE.
C     * OCT 31/02 - J.SCINOCCA. PREVIOUS VERSION GETRAC3.
C  
C     * SELECTIVELY GETS THE INPUT SPECTRAL TRACER FIELDS, UNDER CONTROL
C     * OF "ITRIC".
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TRAC(LA,ILEV,NTRACA) 
      COMPLEX GLL(LATOTAL)
C
      INTEGER INDXA(NTRAC),ITRNAM(NTRAC),ITRIC(NTRAC)
      INTEGER LH(ILEV)
      INTEGER LSRTOTAL(2,LMTOTAL+1)
C 
      LOGICAL OK
C-------------------------------------------------------------------------- 
      IF(ITRAC.EQ.0)RETURN
C 
      DO 300 NA=1,NTRACA
       N=INDXA(NA)
       IF(ITRIC(N).EQ.1) THEN
        NAM=ITRNAM(N)
        DO 200 L=1,ILEV
         CALL GETSPN(NF,TRAC(1,L,NA),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1               NC4TO8("SPEC"),0,NAM,LH(L),OK)
  200   CONTINUE
       ENDIF
  300 CONTINUE
      RETURN
C-------------------------------------------------------------------------- 
      END 
