      SUBROUTINE RGTOCG2(PAKO,PAKI,LON,NLAT,ILAT)

C     * JUNE 30, 2003 - M.LAZARE.
C
C     * THIS ROUTINE TAKES A NORMAL S-N GRID FIELD, "PAKI" 
C     * AND CONVERTS IT INTO ORDERED S-N PAIRS, "PAKO",
C     * WHICH ARE USED WITHIN THE MODEL ** ON EACH NODE **.

C     * ** NOTE ** THE CYCLIC LONGITUDE IS STRIPPED OFF!
C     * ** NOTE ** THE VARIABLE "MYNODE" PASSED THROUGH THE
C     *            COMMON DECK "MPINFO" IS USED TO SELECT THE
C     *            CHUNK FOR A GIVEN NODE. 

C     * THIS ROUTINE IS TYPICALLY CALLED WHEN READING-IN EXTERNAL
C     * FIELDS FROM THE "AN" FILE.
C     * "RGTOCG" SHOULD ** NOT ** BE USED.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
      REAL PAKO(LON*ILAT), PAKI(LON+1,NLAT)
      INTEGER*4 MYNODE
      LOGICAL LRCM
C
      COMMON /MPINFO/ MYNODE
      COMMON /MODEL/  LRCM
C==================================================================
      IF(.NOT.LRCM)                                  THEN
C
C**************************************************************************
C     * GCM WITH REORGANIZED LATITUDES AND POSSIBLE MPI.
C**************************************************************************
C
C      * "NI" REPRESENTS THE COUNTER FOR EACH GRID POINT WRITTEN INTO "PAKO".
C      * "NL" REPRESENTS THE COUNTER FOR LATITUDE IN THE MODEL STRUCTURE
C      * (IE S-N QUARTETS STARTING AT POLES,EQUATOR).
C 
       NI=0
       NL=0
       IF(MOD(NLAT,4).NE.0)       CALL XIT('RGTOCG2',-1)
       NLATH=NLAT/2 
       NLATQ=NLAT/4
C
C      * MPI HOOK.
C
       JSTART=MYNODE*ILAT+1
       JEND  =MYNODE*ILAT+ILAT
C
       DO J=1,NLATQ
         JSP=J
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JSP)
            ENDDO
         ENDIF
C
         JNP=NLAT-J+1 
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JNP)
            ENDDO
         ENDIF
C
         JSE=NLATH-J+1
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JSE)
            ENDDO
         ENDIF 
C
         JNE=NLATH+J
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JNE)
            ENDDO
         ENDIF
       ENDDO 
C
      ELSE
C
C**************************************************************************
C      * RCM WITH USUAL LATITUDES AND NO MPI.
C**************************************************************************
C
       NI=0
       DO J=1,NLAT
         DO I=1,LON
           NI=NI+1
           PAKO(NI)=PAKI(I,J)
         ENDDO
       ENDDO
      ENDIF
C
      RETURN
      END
