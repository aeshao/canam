      SUBROUTINE GETCHEM4(DMSOPAK,DMSOPAL,EDMSPAK,EDMSPAL,
     1                    SUZ0PAK,SUZ0PAL,PDSFPAK,PDSFPAL,  
     2                      OHPAK,  OHPAL,
     3                    H2O2PAK,H2O2PAL,  O3PAK,  O3PAL,
     4                     NO3PAK, NO3PAL,HNO3PAK,HNO3PAL,
     5                     NH3PAK, NH3PAL, NH4PAK, NH4PAL,
     6                    SO4CPAK,SO4CPAL,
     7                    NLON,NLAT,ILEV,INCD,IDAY,MDAY,MDAY1,
     8                    KOUNT,IJPAK,NUCH,NUOX,NUXSFX,ISULF,LH,GG)
C
C     * JUN 11, 2018 - M.LAZARE. Read from unit number NUCH ("llchem" file)
C     *                          instead of NUGG for non-transient chemistry
C     *                          forcing (several calls). Also read from unit
C     *                          number NUOX ("oxi" file) for non-transient
C     *                          oxidants in GETCHEM4 (subroutine also changed).
C     * MAY 10/2012 - Y.PENG/   NEW VERSION FOR GCM16:
C     *               M.LAZARE. - ADD SUZ0,PDSF INPUT FIELDS FOR NEW
C     *                           DUST SCHEME.
C     *                         - MDAY AND MDAY1 PASSED IN (CALCULATED
C     *                           IN "GETMDAY") RATHER THAN BEING
C     *                           DETERMINED HERE.
C     * KNUT VON SALZEN - FEB 07,2009. PREVIOUS VERSION GETCHEM3 FOR 
C     *                                GCM15H/I:
C     *                                REMOVE EOST (NOW DONE IN GETCAC).
C     * JAN 17/08 - X.MA/     PREVIOUS VERSION GETCHEM2 FOR GCM15G:
C     *             M.LAZARE. ADD EOST AND REMOVE EBC,EOC,EOCF.
C     * DEC 15/03 - M.LAZARE. PREVIOUS VERSION GETCHEMX UP TO GCM15F:
C     *                       - ROW FIELDS REMOVED.
C     *                       - USES MPI-READY I/O INTERFACE ROUTINES. 
C     *                       - ONLY PRINTS OUT ON NODE ZERO.
C     *                       - "IFIZ" REMOVED.
C     * JUL 16/01 - M.LAZARE. PREVIOUS VERSION GETCHEM FOR GCM15.
C
C     * READ IN CHEMISTRY FIELDS (SURFACE EMISSIONS AND ATMOSPHERIC
C     * SPECIES CONCENTRATIONS) AND INTERPOLATE TO CURRENT IDAY.
C
C     * TIME LABEL IS THE JULIAN DATE OF THE FIRST OF THE MONTH 
C     * (NFDM) FOR WHICH THE SST ARE THE MONTHLY AVERAGE. 
C
C     * WHEN THE MODEL IS MOVING THROUGH THE YEAR (INCD.NE.0),
C     * THEN NEW FIELDS ARE READ ON EVERY MID MONTH DAY (MMD), 
C     * AND INTERPOLATION WILL BE DONE BETWEEN THE TWO ADJACENT 
C     * MID MONTH DAYS. "PAK" IS THE PRECEDENT, "PAL" IS THE TARGET.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL DMSOPAK(IJPAK), DMSOPAL(IJPAK)
      REAL EDMSPAK(IJPAK), EDMSPAL(IJPAK)
      REAL SUZ0PAK(IJPAK), SUZ0PAL(IJPAK)  
      REAL PDSFPAK(IJPAK), PDSFPAL(IJPAK) 
C
C     * MULTI-LEVEL SPECIES.
C
      REAL   OHPAK(IJPAK,ILEV),   OHPAL(IJPAK,ILEV)
      REAL H2O2PAK(IJPAK,ILEV), H2O2PAL(IJPAK,ILEV)
      REAL   O3PAK(IJPAK,ILEV),   O3PAL(IJPAK,ILEV)
      REAL  NO3PAK(IJPAK,ILEV),  NO3PAL(IJPAK,ILEV)
      REAL HNO3PAK(IJPAK,ILEV), HNO3PAL(IJPAK,ILEV)
      REAL  NH3PAK(IJPAK,ILEV),  NH3PAL(IJPAK,ILEV)
      REAL  NH4PAK(IJPAK,ILEV),  NH4PAL(IJPAK,ILEV)
C
C     * CLIMATOLOGICAL SULFATE CONCENTRATIONS FROM FILE NUXSFX (IFF ISULF=0).
C
      REAL SO4CPAK(IJPAK,ILEV), SO4CPAL(IJPAK,ILEV)
C
      REAL GG(*)
C
      INTEGER LH(ILEV)
      INTEGER*4 MYNODE
C
      COMMON /MPINFO/ MYNODE 
C----------------------------------------------------------------------
      JOUR =MDAY
      JOUR1=MDAY1
C
      IF(INCD.EQ.0) THEN
C 
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH. 
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          REWIND NUCH
C
          CALL GETAGBX(DMSOPAK,NC4TO8("DMSO"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(EDMSPAK,NC4TO8("EDMS"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(SUZ0PAK,NC4TO8("SUZ0"),NUCH,NLON,NLAT,IDAY,GG)
          CALL GETAGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(PDSFPAK,NC4TO8("SLAI"),NUCH,NLON,NLAT,IDAY,GG)
          CALL GETAGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,IDAY,GG)
C
          REWIND NUOX
C
          DO  8  L=1,ILEV
           CALL GETGGBX(  OHPAK(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
    8     CONTINUE     
C
          DO  9  L=1,ILEV
           CALL GETGGBX(H2O2PAK(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
    9     CONTINUE     
C
          DO 10  L=1,ILEV
           CALL GETGGBX(  O3PAK(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
   10     CONTINUE     
C
          DO 11  L=1,ILEV
           CALL GETGGBX( NO3PAK(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
   11     CONTINUE     
C
          DO 12  L=1,ILEV
           CALL GETGGBX(HNO3PAK(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
   12     CONTINUE    
C
          DO 13  L=1,ILEV
           CALL GETGGBX( NH3PAK(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
   13     CONTINUE     
C
          DO 14  L=1,ILEV
           CALL GETGGBX( NH4PAK(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,IDAY,
     1                  LH(L),GG)
   14     CONTINUE     
C
          IF(ISULF.EQ.0) THEN
            REWIND NUXSFX
            DO 15  L=1,ILEV
              CALL GETGGBX(SO4CPAK(1,L),NC4TO8("SO4C"),NUXSFX,NLON,NLAT,
     1                     IDAY,LH(L),GG)
   15       CONTINUE     
          ENDIF  
C
          DO 16 L=1,ILEV
          DO 16 I=1,IJPAK
             OHPAL  (I,L) = OHPAK  (I,L)
             H2O2PAL(I,L) = H2O2PAK(I,L)
             O3PAL  (I,L) = O3PAK  (I,L)
             NO3PAL (I,L) = NO3PAK (I,L)
             HNO3PAL(I,L) = HNO3PAK(I,L)
             NH3PAL (I,L) = NH3PAK (I,L)
             NH4PAL (I,L) = NH4PAK (I,L)
             IF(ISULF.EQ.0) THEN
             SO4CPAL(I,L) = SO4CPAK(I,L)
             ENDIF  
   16     CONTINUE     

        ELSE
C         
C         * NOT A NEW INTEGRATION. NO NEW FIELDS REQUIRED. 
C         
        ENDIF 

      ELSE
C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C        * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
         REWIND NUCH         
C
         CALL GETGGBX(DMSOPAK,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR1,1,GG)
         CALL GETGGBX(EDMSPAK,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR1,1,GG)
         CALL GETGGBX(SUZ0PAK,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR1,1,GG)   
         CALL GETGGBX(PDSFPAK,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR1,1,GG)
C
         REWIND NUOX
C
         DO 20  L=1,ILEV
          CALL GETGGBX(  OHPAK(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   20    CONTINUE     
C
         DO 21  L=1,ILEV
          CALL GETGGBX(H2O2PAK(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   21    CONTINUE     
C
         DO 22  L=1,ILEV
          CALL GETGGBX(  O3PAK(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   22    CONTINUE     
C
         DO 23  L=1,ILEV
          CALL GETGGBX( NO3PAK(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   23    CONTINUE     
C
         DO 24  L=1,ILEV
          CALL GETGGBX(HNO3PAK(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   24    CONTINUE     
C
         DO 25  L=1,ILEV
          CALL GETGGBX( NH3PAK(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   25    CONTINUE     
C
         DO 26  L=1,ILEV
          CALL GETGGBX( NH4PAK(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR1,
     1                 LH(L),GG)
   26    CONTINUE     
C
         IF(ISULF.EQ.0) THEN
           REWIND NUXSFX
           DO 27  L=1,ILEV
             CALL GETGGBX(SO4CPAK(1,L),NC4TO8("SO4C"),NUXSFX,NLON,NLAT,
     1                    JOUR1,LH(L),GG)
   27      CONTINUE     
         ENDIF
C
         REWIND NUCH
C
         CALL GETGGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR,1,GG)
         CALL GETGGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR,1,GG)
         CALL GETGGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR,1,GG)  
         CALL GETGGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR,1,GG)   
C
         REWIND NUOX
C
         DO 30  L=1,ILEV
          CALL GETGGBX(  OHPAL(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   30    CONTINUE     
C
         DO 31  L=1,ILEV
          CALL GETGGBX(H2O2PAL(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   31    CONTINUE     
C
         DO 32  L=1,ILEV
          CALL GETGGBX(  O3PAL(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   32    CONTINUE     
C
         DO 33  L=1,ILEV
          CALL GETGGBX( NO3PAL(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   33    CONTINUE     
C
         DO 34  L=1,ILEV
          CALL GETGGBX(HNO3PAL(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   34    CONTINUE     
C
         DO 35  L=1,ILEV
          CALL GETGGBX( NH3PAL(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   35    CONTINUE     
C
         DO 36  L=1,ILEV
          CALL GETGGBX( NH4PAL(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR,
     1                 LH(L),GG)
   36    CONTINUE     
C
         IF(ISULF.EQ.0) THEN
           REWIND NUXSFX
           DO 37  L=1,ILEV
             CALL GETGGBX(SO4CPAL(1,L),NC4TO8("SO4C"),NUXSFX,NLON,NLAT,
     1                    JOUR,LH(L),GG)
   37      CONTINUE     
         ENDIF
C
         LON=NLON-1
         DAY1=REAL(MDAY1) 
         DAY2=REAL(IDAY)
         DAY3=REAL(MDAY)
         IF(DAY2.LT.DAY1) DAY2=DAY2+365. 
         IF(DAY3.LT.DAY2) DAY3=DAY3+365. 
         W1=(DAY2-DAY1)/(DAY3-DAY1)
         W2=(DAY3-DAY2)/(DAY3-DAY1)
         IF(MYNODE.EQ.0) WRITE(6,6000) IDAY,MDAY1,MDAY,W1,W2 
C 
         DO 125 I=1,IJPAK
           DMSOPAK(I)=W1*DMSOPAL(I) + W2*DMSOPAK(I)
           EDMSPAK(I)=W1*EDMSPAL(I) + W2*EDMSPAK(I)
           SUZ0PAK(I)=W1*SUZ0PAL(I) + W2*SUZ0PAK(I)
           PDSFPAK(I)=W1*PDSFPAL(I) + W2*PDSFPAK(I)
  125    CONTINUE
C
         DO 150 L=1,ILEV
         DO 150 I=1,IJPAK
           OHPAK  (I,L)=W1*OHPAL  (I,L) + W2*OHPAK  (I,L)
           H2O2PAK(I,L)=W1*H2O2PAL(I,L) + W2*H2O2PAK(I,L)
           O3PAK  (I,L)=W1*O3PAL  (I,L) + W2*O3PAK  (I,L)
           NO3PAK (I,L)=W1*NO3PAL (I,L) + W2*NO3PAK (I,L)
           HNO3PAK(I,L)=W1*HNO3PAL(I,L) + W2*HNO3PAK(I,L)
           NH3PAK (I,L)=W1*NH3PAL (I,L) + W2*NH3PAK (I,L)
           NH4PAK (I,L)=W1*NH4PAL (I,L) + W2*NH4PAK (I,L)
           IF(ISULF.EQ.0) THEN
             SO4CPAK(I,L)=W1*SO4CPAL(I,L) + W2*SO4CPAK(I,L)
           ENDIF
  150    CONTINUE    

        ELSE
C 
C         * THIS IS IN THE MIDDLE OF A RUN. 
C
          REWIND NUCH
C
          CALL GETGGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR,1,GG)
          CALL GETGGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR,1,GG)
          CALL GETGGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR,1,GG) 
          CALL GETGGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR,1,GG)
C
          REWIND NUOX
C
          DO 230  L=1,ILEV
           CALL GETGGBX(  OHPAL(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  230     CONTINUE     
C
          DO 231  L=1,ILEV
           CALL GETGGBX(H2O2PAL(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  231     CONTINUE     
C
          DO 232  L=1,ILEV
           CALL GETGGBX(  O3PAL(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  232     CONTINUE     
C
          DO 233  L=1,ILEV
           CALL GETGGBX( NO3PAL(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  233     CONTINUE     
C
          DO 234  L=1,ILEV
           CALL GETGGBX(HNO3PAL(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  234     CONTINUE     
C
          DO 235  L=1,ILEV
           CALL GETGGBX( NH3PAL(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  235     CONTINUE     
C
          DO 236  L=1,ILEV
           CALL GETGGBX( NH4PAL(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR,
     1                  LH(L),GG)
  236     CONTINUE     
        ENDIF

      ENDIF 
      RETURN
C---------------------------------------------------------------------
 6000 FORMAT(' INTERPOLATING FOR', I5, ' BETWEEN', I5, ' AND',
     1       I5, ' WITH WEIGHTS=', 2F7.3)
      END
