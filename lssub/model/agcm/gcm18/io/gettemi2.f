      SUBROUTINE GETTEMI2 (SAIRPAK,SSFCPAK,SBIOPAK,SSHIPAK,
     1                     SSTKPAK,SFIRPAK,SAIRPAL,SSFCPAL,
     2                     SBIOPAL,SSHIPAL,SSTKPAL,SFIRPAL,
     3                     OAIRPAK,OSFCPAK,OBIOPAK,OSHIPAK,
     4                     OSTKPAK,OFIRPAK,OAIRPAL,OSFCPAL,
     5                     OBIOPAL,OSHIPAL,OSTKPAL,OFIRPAL,
     6                     BAIRPAK,BSFCPAK,BBIOPAK,BSHIPAK,
     7                     BSTKPAK,BFIRPAK,BAIRPAL,BSFCPAL,
     8                     BBIOPAL,BSHIPAL,BSTKPAL,BFIRPAL,
     9                     NLON,NLAT,INCD,IREFYR,IDAY,KOUNT,
     A                     IJPAK,NUAN,GG)
C
C     * April 17, 2015 - M.Lazare.     Cosmetic revision (same name kept)
C     *                                to have "keeptim" common block
C     *                                definition line in upper case,
C     *                                so that "myrssti" does not conflict
C     *                                with the CPP directive (compiler
C     *                                warnings generated).
C     * JUN 18/2013 - K.VONSALZEN. NEW VERSION FOR GCM17+:
C     *                            - ADD {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI}.  
C     * APR 20/2010 - K.VONSALZEN. PREVIOUS VERSION GETTEM FOR GCM15I+.
C
C     * READ-IN TRANSIENT EMISSION DATA.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
C     * 2D EMISSION FIELDS.
C
      REAL SAIRPAK(IJPAK), SAIRPAL(IJPAK)
      REAL SSFCPAK(IJPAK), SSFCPAL(IJPAK)
      REAL SBIOPAK(IJPAK), SBIOPAL(IJPAK)
      REAL SSHIPAK(IJPAK), SSHIPAL(IJPAK)
      REAL SSTKPAK(IJPAK), SSTKPAL(IJPAK)
      REAL SFIRPAK(IJPAK), SFIRPAL(IJPAK)
      REAL OAIRPAK(IJPAK), OAIRPAL(IJPAK)
      REAL OSFCPAK(IJPAK), OSFCPAL(IJPAK)
      REAL OBIOPAK(IJPAK), OBIOPAL(IJPAK)
      REAL OSHIPAK(IJPAK), OSHIPAL(IJPAK)
      REAL OSTKPAK(IJPAK), OSTKPAL(IJPAK)
      REAL OFIRPAK(IJPAK), OFIRPAL(IJPAK)
      REAL BAIRPAK(IJPAK), BAIRPAL(IJPAK)
      REAL BSFCPAK(IJPAK), BSFCPAL(IJPAK)
      REAL BBIOPAK(IJPAK), BBIOPAL(IJPAK)
      REAL BSHIPAK(IJPAK), BSHIPAL(IJPAK)
      REAL BSTKPAK(IJPAK), BSTKPAL(IJPAK)
      REAL BFIRPAK(IJPAK), BFIRPAL(IJPAK)
C
C     * Work space
C
      REAL GG(*)

      !--- mynode used to control i/o to stdout
      integer*4 mynode
      common /mpinfo/ mynode 

      !--- keeptime is required for iyear
      COMMON /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS

      !--- local
      integer :: year_prev, year_next, mon_prev, mon_next
      integer :: ib2p, ib2n, curr_year

      !--- A list containing mid month days of the year
      integer, parameter, dimension(12) ::
     &  mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

      !--- A list containing mid month days for each month
      integer, parameter, dimension(12) ::
     &  mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
C----------------------------------------------------------------------
C
      if (mynode.eq.0) then
        write(6,*)'    GETTEM: NEW CHEMISTRY FORCING REQUIRED'
      endif

c.....Determine the year for which data is required
      if (irefyr.gt.0) then
        !--- When irefyr > 0 read emissions from the data file for
        !--- a repeated annual cycle of year irefyr
        curr_year=irefyr
      else
        !--- Otherwise read time dependent emissions from the data
        !--- file using iyear to determine the current year
        curr_year=iyear
      endif

c.....check on iday
      if (iday.lt.1 .or. iday.gt.365) then
        if (mynode.eq.0) then
          write(6,*)'GETTEM: IDAY is out of range. IDAY=',iday
        endif
        call xit("GETTEM",-1)
      endif

c.....Determine previous and next year/month, relative to iday
      mon_prev=0
      if (iday.ge.1 .and. iday.lt.mm_doy(1)) then
        year_prev=curr_year-1
        year_next=curr_year
        mon_prev=12
        mon_next=1
      else if (iday.ge.mm_doy(12) .and. iday.le.365) then
        year_prev=curr_year
        year_next=curr_year+1
        mon_prev=12
        mon_next=1
      else
        do i=2,12
          if (iday.ge.mm_doy(i-1) .and. iday.lt.mm_doy(i)) then
            year_prev=curr_year
            year_next=curr_year
            mon_prev=i-1
            mon_next=i
            exit
          endif
        enddo
      endif

      if (mon_prev.eq.0) then
        if (mynode.eq.0) then
          write(6,*)'GETTEM: Problem setting year/month on IDAY=',iday
        endif
        call xit("GETTEM",-2)
      endif

cxxx
      if (year_prev.eq.1849.and.mon_prev.eq.12) then
        year_prev=1850
        mon_prev=12
      endif
      if (year_next.eq.2001.and.mon_next.eq.1) then
        year_next=2000
        mon_next=1
      endif
cxxx

      !--- determine time stamps (ibuf2 values) to be used to read
      !--- appropriate data from the file
      ! ib2p=1000000*year_prev+10000*mon_prev+100*mm_dom(mon_prev)
      ! ib2n=1000000*year_next+10000*mon_next+100*mm_dom(mon_next)
      ib2p=year_prev*100+mon_prev
      ib2n=year_next*100+mon_next

      if (mynode.eq.0) then
        write(6,*)'GETTEM: year_prev,mon_prev,year_next,mon_next: ',
     &                    year_prev,mon_prev,year_next,mon_next
        write(6,*)'GETTEM: ib2p,ib2n: ',ib2p,ib2n
      endif
C
C     * GET NEW BOUNDARY FIELDS FOR NEXT MID-MONTH DAY. 
C
      IF(INCD.EQ.0) THEN
C 
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH. 
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          CALL GETAGBX(SAIRPAK,NC4TO8("SAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSFCPAK,NC4TO8("SSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SBIOPAK,NC4TO8("SBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSHIPAK,NC4TO8("SSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSTKPAK,NC4TO8("SSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SFIRPAK,NC4TO8("SFIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BAIRPAK,NC4TO8("BAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSFCPAK,NC4TO8("BSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BBIOPAK,NC4TO8("BBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSHIPAK,NC4TO8("BSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSTKPAK,NC4TO8("BSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BFIRPAK,NC4TO8("BFIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OAIRPAK,NC4TO8("OAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSFCPAK,NC4TO8("OSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OBIOPAK,NC4TO8("OBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSHIPAK,NC4TO8("OSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSTKPAK,NC4TO8("OSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OFIRPAK,NC4TO8("OFIR"),NUAN,NLON,NLAT,IB2N,GG) 
C
          CALL GETAGBX(SAIRPAL,NC4TO8("SAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSFCPAL,NC4TO8("SSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SBIOPAL,NC4TO8("SBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSHIPAL,NC4TO8("SSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSTKPAL,NC4TO8("SSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SFIRPAL,NC4TO8("SFIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BAIRPAL,NC4TO8("BAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSFCPAL,NC4TO8("BSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BBIOPAL,NC4TO8("BBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSHIPAL,NC4TO8("BSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSTKPAL,NC4TO8("BSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BFIRPAL,NC4TO8("BFIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OAIRPAL,NC4TO8("OAIR"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSFCPAL,NC4TO8("OSFC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OBIOPAL,NC4TO8("OBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSHIPAL,NC4TO8("OSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSTKPAL,NC4TO8("OSTK"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OFIRPAL,NC4TO8("OFIR"),NUAN,NLON,NLAT,IB2N,GG) 
        ENDIF
C
      ELSE
C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
          REWIND NUAN         
          CALL GETAGBX(SAIRPAK,NC4TO8("SAIR"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(SSFCPAK,NC4TO8("SSFC"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(SBIOPAK,NC4TO8("SBIO"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(SSHIPAK,NC4TO8("SSHP"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(SSTKPAK,NC4TO8("SSTK"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(SFIRPAK,NC4TO8("SFIR"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(BAIRPAK,NC4TO8("BAIR"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(BSFCPAK,NC4TO8("BSFC"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(BBIOPAK,NC4TO8("BBIO"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(BSHIPAK,NC4TO8("BSHP"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(BSTKPAK,NC4TO8("BSTK"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(BFIRPAK,NC4TO8("BFIR"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(OAIRPAK,NC4TO8("OAIR"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(OSFCPAK,NC4TO8("OSFC"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(OBIOPAK,NC4TO8("OBIO"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(OSHIPAK,NC4TO8("OSHP"),NUAN,NLON,NLAT,IB2P,GG) 
          CALL GETAGBX(OSTKPAK,NC4TO8("OSTK"),NUAN,NLON,NLAT,IB2P,GG)
          CALL GETAGBX(OFIRPAK,NC4TO8("OFIR"),NUAN,NLON,NLAT,IB2P,GG)
C
          REWIND NUAN
          CALL GETAGBX(SAIRPAL,NC4TO8("SAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SSFCPAL,NC4TO8("SSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SBIOPAL,NC4TO8("SBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSHIPAL,NC4TO8("SSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSTKPAL,NC4TO8("SSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SFIRPAL,NC4TO8("SFIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BAIRPAL,NC4TO8("BAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BSFCPAL,NC4TO8("BSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BBIOPAL,NC4TO8("BBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSHIPAL,NC4TO8("BSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSTKPAL,NC4TO8("BSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BFIRPAL,NC4TO8("BFIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OAIRPAL,NC4TO8("OAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OSFCPAL,NC4TO8("OSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OBIOPAL,NC4TO8("OBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSHIPAL,NC4TO8("OSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSTKPAL,NC4TO8("OSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OFIRPAL,NC4TO8("OFIR"),NUAN,NLON,NLAT,IB2N,GG)
C
          !--- INTERPOLATE TO CURRENT DAY
          DAY1=REAL(MM_DOY(MON_PREV)) 
          DAY2=REAL(IDAY)
          DAY3=REAL(MM_DOY(MON_NEXT))
          IF(DAY2.LT.DAY1) DAY2=DAY2+365. 
          IF(DAY3.LT.DAY2) DAY3=DAY3+365. 
          W1=(DAY2-DAY1)/(DAY3-DAY1)
          W2=(DAY3-DAY2)/(DAY3-DAY1)
          IF (MYNODE.EQ.0) THEN
            WRITE(6,*)
     &        'GETTEM: Interpolating at ',curr_year,' day',iday,
     &        ' between ',year_prev,' day',mm_doy(mon_prev),
     &        ' and ',year_next,' day',mm_doy(mon_next),
     &        ' using weights ',w1,w2
          ENDIF
C 
          DO I=1,IJPAK
            SAIRPAK(I)=W1*SAIRPAL(I) + W2*SAIRPAK(I)
            SSFCPAK(I)=W1*SSFCPAL(I) + W2*SSFCPAK(I)
            SBIOPAK(I)=W1*SBIOPAL(I) + W2*SBIOPAK(I)
            SSHIPAK(I)=W1*SSHIPAL(I) + W2*SSHIPAK(I)
            SSTKPAK(I)=W1*SSTKPAL(I) + W2*SSTKPAK(I)
            SFIRPAK(I)=W1*SFIRPAL(I) + W2*SFIRPAK(I)
            OAIRPAK(I)=W1*OAIRPAL(I) + W2*OAIRPAK(I)
            OSFCPAK(I)=W1*OSFCPAL(I) + W2*OSFCPAK(I)
            OBIOPAK(I)=W1*OBIOPAL(I) + W2*OBIOPAK(I)
            OSHIPAK(I)=W1*OSHIPAL(I) + W2*OSHIPAK(I)
            OSTKPAK(I)=W1*OSTKPAL(I) + W2*OSTKPAK(I)
            OFIRPAK(I)=W1*OFIRPAL(I) + W2*OFIRPAK(I)
            BAIRPAK(I)=W1*BAIRPAL(I) + W2*BAIRPAK(I)
            BSFCPAK(I)=W1*BSFCPAL(I) + W2*BSFCPAK(I)
            BBIOPAK(I)=W1*BBIOPAL(I) + W2*BBIOPAK(I)
            BSHIPAK(I)=W1*BSHIPAL(I) + W2*BSHIPAK(I)
            BSTKPAK(I)=W1*BSTKPAL(I) + W2*BSTKPAK(I)
            BFIRPAK(I)=W1*BFIRPAL(I) + W2*BFIRPAK(I)
          ENDDO
        ELSE
C 
C         * THIS IS IN THE MIDDLE OF A RUN. 
C
          REWIND NUAN
C
          CALL GETAGBX(SAIRPAL,NC4TO8("SAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SSFCPAL,NC4TO8("SSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SBIOPAL,NC4TO8("SBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSHIPAL,NC4TO8("SSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(SSTKPAL,NC4TO8("SSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(SFIRPAL,NC4TO8("SFIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BAIRPAL,NC4TO8("BAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BSFCPAL,NC4TO8("BSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BBIOPAL,NC4TO8("BBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSHIPAL,NC4TO8("BSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(BSTKPAL,NC4TO8("BSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(BFIRPAL,NC4TO8("BFIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OAIRPAL,NC4TO8("OAIR"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OSFCPAL,NC4TO8("OSFC"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OBIOPAL,NC4TO8("OBIO"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSHIPAL,NC4TO8("OSHP"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(OSTKPAL,NC4TO8("OSTK"),NUAN,NLON,NLAT,IB2N,GG)
          CALL GETAGBX(OFIRPAL,NC4TO8("OFIR"),NUAN,NLON,NLAT,IB2N,GG)
        ENDIF
      ENDIF 
      RETURN
      END
