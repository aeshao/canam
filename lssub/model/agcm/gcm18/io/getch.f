      SUBROUTINE GETCH(EBOCPAK,EBOCPAL,EBBCPAK,EBBCPAL,
     1                 EAOCPAK,EAOCPAL,EABCPAK,EABCPAL,
     2                 EASLPAK,EASLPAL,EASHPAK,EASHPAL,
     3                 NLON,NLAT,INCD,IREFYR,IDAY,KOUNT,IJPAK,NUAN,GG)
C
C     * April 17, 2015 - M.Lazare.     Cosmetic revision (same name kept)
C     *                                to have "keeptim" common block
C     *                                definition line in upper case,
C     *                                so that "myrssti" does not conflict
C     *                                with the CPP directive (compiler
C     *                                warnings generated).
C     * KNUT VON SALZEN - FEB 07,2009. NEW ROUTINE FOR GCM15H TO READ IN
C     *                                EBBC,EOBC,EABC,EAOC,EASL,EASH.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL EBOCPAK(IJPAK), EBOCPAL(IJPAK)
      REAL EBBCPAK(IJPAK), EBBCPAL(IJPAK)
      REAL EAOCPAK(IJPAK), EAOCPAL(IJPAK)
      REAL EABCPAK(IJPAK), EABCPAL(IJPAK)
      REAL EASLPAK(IJPAK), EASLPAL(IJPAK)
      REAL EASHPAK(IJPAK), EASHPAL(IJPAK)
C
C     * Work space
C
      REAL GG(*)

      !--- mynode used to control i/o to stdout
      integer*4 mynode
      common /mpinfo/ mynode 

      !--- keeptime is required for iyear
      COMMON /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS

      !--- local
      integer :: year_prev, year_next, mon_prev, mon_next
      integer :: ib2p, ib2n, curr_year

      !--- A list containing mid month days of the year
      integer, parameter, dimension(12) ::
     &  mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

      !--- A list containing mid month days for each month
      integer, parameter, dimension(12) ::
     &  mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
C----------------------------------------------------------------------
C
      if (mynode.eq.0) then
        write(6,*)'    GETCH: NEW CHEMISTRY FORCING REQUIRED'
      endif

c.....Determine the year for which data is required
      if (irefyr.gt.0) then
        !--- When irefyr > 0 read emissions from the data file for
        !--- a repeated annual cycle of year irefyr
        curr_year=irefyr
      else
        !--- Otherwise read time dependent emissions from the data
        !--- file using iyear to determine the current year
        curr_year=iyear
      endif

c.....check on iday
      if (iday.lt.1 .or. iday.gt.365) then
        if (mynode.eq.0) then
          write(6,*)'GETCH: IDAY is out of range. IDAY=',iday
        endif
        call xit("GETCH",-1)
      endif

c.....Determine previous and next year/month, relative to iday
      mon_prev=0
      if (iday.ge.1 .and. iday.lt.mm_doy(1)) then
        year_prev=curr_year-1
        year_next=curr_year
        mon_prev=12
        mon_next=1
      else if (iday.ge.mm_doy(12) .and. iday.le.365) then
        year_prev=curr_year
        year_next=curr_year+1
        mon_prev=12
        mon_next=1
      else
        do i=2,12
          if (iday.ge.mm_doy(i-1) .and. iday.lt.mm_doy(i)) then
            year_prev=curr_year
            year_next=curr_year
            mon_prev=i-1
            mon_next=i
            exit
          endif
        enddo
      endif

      if (mon_prev.eq.0) then
        if (mynode.eq.0) then
          write(6,*)'GETCH: Problem setting year/month on IDAY=',iday
        endif
        call xit("GETCH",-2)
      endif

cxxx
      if (year_prev.eq.1849.and.mon_prev.eq.12) then
        year_prev=1850
        mon_prev=12
      endif
      if (year_next.eq.2001.and.mon_next.eq.1) then
        year_next=2000
        mon_next=1
      endif
cxxx

      !--- determine time stamps (ibuf2 values) to be used to read
      !--- appropriate data from the file
      ! ib2p=1000000*year_prev+10000*mon_prev+100*mm_dom(mon_prev)
      ! ib2n=1000000*year_next+10000*mon_next+100*mm_dom(mon_next)
      ib2p=year_prev*100+mon_prev
      ib2n=year_next*100+mon_next

      if (mynode.eq.0) then
        write(6,*)'GETCH: year_prev,mon_prev,year_next,mon_next: ',
     &                    year_prev,mon_prev,year_next,mon_next
        write(6,*)'GETCH: ib2p,ib2n: ',ib2p,ib2n
      endif
C
C     * GET NEW BOUNDARY FIELDS FOR NEXT MID-MONTH DAY. 
C
      IF(INCD.EQ.0) THEN
C 
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH. 
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          CALL GETAGBX(EBOCPAK,NC4TO8("EBOC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EBOCPAL,NC4TO8("EBOC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EBBCPAK,NC4TO8("EBBC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EBBCPAL,NC4TO8("EBBC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EAOCPAK,NC4TO8("EAOC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EAOCPAL,NC4TO8("EAOC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EABCPAK,NC4TO8("EABC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EABCPAL,NC4TO8("EABC"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EASLPAK,NC4TO8("EASL"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EASLPAL,NC4TO8("EASL"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EASHPAK,NC4TO8("EASH"),NUAN,NLON,NLAT,IB2N,GG) 
          CALL GETAGBX(EASHPAL,NC4TO8("EASH"),NUAN,NLON,NLAT,IB2N,GG) 
        ENDIF
C
      ELSE
C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
          REWIND NUAN         
          CALL GETGGBX(EBOCPAK,NC4TO8("EBOC"),NUAN,NLON,NLAT,IB2P,1,GG)
          CALL GETGGBX(EBBCPAK,NC4TO8("EBBC"),NUAN,NLON,NLAT,IB2P,1,GG)
          CALL GETGGBX(EAOCPAK,NC4TO8("EAOC"),NUAN,NLON,NLAT,IB2P,1,GG)
          CALL GETGGBX(EABCPAK,NC4TO8("EABC"),NUAN,NLON,NLAT,IB2P,1,GG)
          CALL GETGGBX(EASLPAK,NC4TO8("EASL"),NUAN,NLON,NLAT,IB2P,1,GG)
          CALL GETGGBX(EASHPAK,NC4TO8("EASH"),NUAN,NLON,NLAT,IB2P,1,GG)
C
          REWIND NUAN
          CALL GETGGBX(EBOCPAL,NC4TO8("EBOC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EBBCPAL,NC4TO8("EBBC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EAOCPAL,NC4TO8("EAOC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EABCPAL,NC4TO8("EABC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EASLPAL,NC4TO8("EASL"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EASHPAL,NC4TO8("EASH"),NUAN,NLON,NLAT,IB2N,1,GG)
C
          !--- INTERPOLATE TO CURRENT DAY
          DAY1=REAL(MM_DOY(MON_PREV)) 
          DAY2=REAL(IDAY)
          DAY3=REAL(MM_DOY(MON_NEXT))
          IF(DAY2.LT.DAY1) DAY2=DAY2+365. 
          IF(DAY3.LT.DAY2) DAY3=DAY3+365. 
          W1=(DAY2-DAY1)/(DAY3-DAY1)
          W2=(DAY3-DAY2)/(DAY3-DAY1)
          IF (MYNODE.EQ.0) THEN
            WRITE(6,*)
     &        'GETCH: Interpolating at ',curr_year,' day',iday,
     &        ' between ',year_prev,' day',mm_doy(mon_prev),
     &        ' and ',year_next,' day',mm_doy(mon_next),
     &        ' using weights ',w1,w2
          ENDIF
C 
          DO I=1,IJPAK
            EBOCPAK(I)=W1*EBOCPAL(I) + W2*EBOCPAK(I)
            EBBCPAK(I)=W1*EBBCPAL(I) + W2*EBBCPAK(I)
            EAOCPAK(I)=W1*EAOCPAL(I) + W2*EAOCPAK(I)
            EABCPAK(I)=W1*EABCPAL(I) + W2*EABCPAK(I)
            EASLPAK(I)=W1*EASLPAL(I) + W2*EASLPAK(I)
            EASHPAK(I)=W1*EASHPAL(I) + W2*EASHPAK(I)
          ENDDO
        ELSE
C 
C         * THIS IS IN THE MIDDLE OF A RUN. 
C
          REWIND NUAN
C
          CALL GETGGBX(EBOCPAL,NC4TO8("EBOC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EBBCPAL,NC4TO8("EBBC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EAOCPAL,NC4TO8("EAOC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EABCPAL,NC4TO8("EABC"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EASLPAL,NC4TO8("EASL"),NUAN,NLON,NLAT,IB2N,1,GG)
          CALL GETGGBX(EASHPAL,NC4TO8("EASH"),NUAN,NLON,NLAT,IB2N,1,GG)
        ENDIF
      ENDIF 
      RETURN
      END
