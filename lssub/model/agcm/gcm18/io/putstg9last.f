      SUBROUTINE PUTSTG9LAST(NF ,PS,P,C,T,ES,PHIS,
     1                       KOUNT,LA,LRLMT,ILEV,LEVS,LS,LH,
     2                       LSRTOTAL,LATOTAL,LMTOTAL,GLL)

C
C     * APR 24/10 - J. COLE   MODIFIED VERSION OF PUTSTG9 THAT SAVES
C     *                       THE FIELDS UNDER DIFFERENT NAMES
C     *                       USED TO COMPUTE THE FIELDS ON LAST TIMESTEP
C     *                       FOR TENDENCY COMPUTATIONS
C     * NOV 04/03 - M.LAZARE. NEW VERSION TO SUPPORT MPI, WHICH USES
C     *                       NEW ROUTINE "PUTSPN" AND WORK ARRAY "GLL".
C     * MAY 29/95 - M.LAZARE. PREVIOUS VERSION PUTSTG8.
C
C     * SAVES GLOBAL SPECTRAL FORECAST ON SEQUENTIAL FILE NF. 
C     * ALL FIELDS ARE WRITTEN UNPACKED.
C     * LS,LH = OUTPUT LABEL VALUES FOR FULL,HALF LEVELS. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX PS(LA),PHIS(LA) 
      COMPLEX T(LA,ILEV),P(LA,ILEV),C(LA,ILEV),ES(LA,LEVS)
      COMPLEX GLL(LATOTAL)

      INTEGER LSRTOTAL(2,LMTOTAL+1)
      INTEGER LS(ILEV),LH(ILEV) 
      INTEGER IBUF(8) 
  
      LOGICAL OK

      COMMON /IPARIO/  IPIO
      COMMON /KEEPTIM/ IYEAR,IYMDH,MYRSSTI,ISAVDTS
C-------------------------------------------------------------------- 
      MAX=2*LATOTAL
C
      IPHIS=NC4TO8("LPHI")
      ILNSP=NC4TO8("LLNS")
       ITMP=NC4TO8("LTMP")
      IVORT=NC4TO8("LVOR")
       IDIV=NC4TO8("LDIV")
        IES=NC4TO8(" LES")

C     * DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
C     * VALUE OF OPTION SWITCH "ISAVDTS".
C
      IF(ISAVDTS.NE.0)                   THEN
         IBUF2=IYMDH+1
      ELSE
         IBUF2=KOUNT+1
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("SPEC"),IBUF2,-1,1,-1,1,LRLMT,0)
C 
C     * SURFACE GEOPOTENTIAL (MOUNTAINS) ARE SAVED EVERY TIME.
C
      IBUF(3)=IPHIS
      CALL PUTSPN (NF,PHIS,LA,
     1             LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2             IBUF,MAX,IPIO,OK)
C
C     * SAVE LN(PS) IN PASCALS. 
C 
      IBUF(3)=ILNSP
      CALL PUTSPN (NF,PS,LA,
     1             LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2             IBUF,MAX,IPIO,OK)
C 
C     * SAVE TEMPERATURE FOR ILEV LEVELS. 
C
      IBUF(3)=ITMP 
      DO 310 L=1,ILEV 
         IBUF(4)=LH(L) 
         CALL PUTSPN (NF,T(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  310 CONTINUE
C 
C     * SAVE VORTICITY AND DIVERGENCE IN PAIRS FOR EACH LEVEL.
C 
      DO 410 L=1,ILEV 
         IBUF(4)=LS(L) 

         IBUF(3)=IVORT
         CALL PUTSPN (NF,P(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)

         IBUF(3)=IDIV
         CALL PUTSPN (NF,C(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  410 CONTINUE
C 
C     * MOISTURE VARIABLE SAVED FOR LEVS LEVELS.
C 
      IF(LEVS.EQ.0) RETURN
      IBUF(3)=IES
      DO 510 N=1,LEVS 
         L=(ILEV-LEVS)+N 
         IBUF(4)=LH(L) 
         CALL PUTSPN (NF,ES(1,N),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  510 CONTINUE
C 
      RETURN
C-----------------------------------------------------------------------
      END
