      SUBROUTINE RPKNAPHS(NF,G,NLEV,IBUF,LON,NLAT,ILAT,LEV,
     1                    ITRLVS,ITRLVF,LH,GG,OK) 

C     *                        NEW ROUTINE, BASED ON RPKPHS4, FOR NA TRACERS
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
  
      INTEGER IBUF(8), KBUF(8), LH(NLEV)
      REAL G ( LON*ILAT+1, NLEV)
      REAL GG( (LON+1)*NLAT)
      LOGICAL OK
C-----------------------------------------------------------------------
      OK = .FALSE. 
      LON1=LON+1
C
      DO 30 I=1,8
          KBUF(I)=IBUF(I)
   30 CONTINUE
C
      DO 100 L = ITRLVS,ITRLVF
         KBUF(4) = LH(L)
         CALL GETGGBX(G(1,L),KBUF(3),NF,LON1,NLAT,KBUF(2),KBUF(4),GG)
  100 CONTINUE
C
      DO 300 I=1,8
          IBUF(I)=KBUF(I)
  300 CONTINUE
C
      OK = .TRUE. 
      RETURN
C-------------------------------------------------------------------- 
      END
