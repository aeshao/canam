      SUBROUTINE GETRACNA3(NF,TRACNA,ILEV,LH,ITRAC,NTRAC,NTRACN,
     1                     INDXNA,ITRNAM,ITRIC,ITRLVS,ITRLVF,
     2                     IJPAK,NLON,NLAT,GG)
C
C     * FEB 20/2004 - J.SCINOCCA. NEW VERSION (GETRACNA3) FOR GCM13D
C     *                           ONWARD, PASSING IN AND USING ITRLVS
C     *                           AND ITRLVF AS SUBSET OF COMPLETE
C     *                           VERTICAL DOMAIN FOR NON-ADVECTED
C     *                           TRACERS. 
C     * DEC 10/2003 - M.LAZARE.   NEW VERSION (GETRACNA2) FOR GCM13C:
C     *                           MODIFIED FOR MPI.
C     * OCT 31/2002 - J.SCINOCCA. PREVIOUS VERSION GETRACNA.
C  
C     * SELECTIVELY GETS NON-ADVECTED TRACERS FOR SPECTRAL CASE.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL TRACNA(IJPAK,ILEV,NTRACN) 
      INTEGER ITRNAM(NTRAC),INDXNA(NTRAC),ITRIC(NTRAC)
      INTEGER ITRLVS(NTRAC),ITRLVF(NTRAC)
      INTEGER LH(ILEV)
C
C     * WORK FIELD:
C
      REAL GG(1)                              
C-------------------------------------------------------------------------- 
      DO 300 NN=1,NTRACN
        N=INDXNA(NN)
        IF(ITRIC(N).EQ.1)            THEN
          NAM=ITRNAM(N)
          DO 200 L=ITRLVS(N),ITRLVF(N)
            CALL GETGGBX(TRACNA(1,L,NN),NAM,NF,NLON,NLAT,0,LH(L),GG)
  200     CONTINUE
        ENDIF
  300 CONTINUE
      RETURN
C-------------------------------------------------------------------------- 
      END 
