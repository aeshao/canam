      SUBROUTINE PKZEROS2(PKFLD,IJPAK,ILEV) 
 
C     * MAY 27, 2003 - M. LAZARE. NEW VERSION WITH "ROW" FIELDS,
C     *                           INTERNAL PACKING AND "WRKS" REMOVED.
C     * NOV 13/87 - R. MENARD - PREVIOUS VERSION PKZEROS.
C
C     * INITIALIZE PKFLD FIELD TO ZERO IN THE GCM PACKING FORMAT. 
C  
      IMPLICIT NONE
      INTEGER IJPAK,ILEV,L,IJ
      REAL PKFLD(IJPAK,ILEV)
C-------------------------------------------------------------------
      DO 100 L = 1,ILEV
      DO 100 IJ = 1,IJPAK
          PKFLD(IJ,L)=0.
  100 CONTINUE     
C 
      RETURN
      END      
