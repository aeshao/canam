      SUBROUTINE GETSPN(NF,G,WRK,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1                  KIND,NSTEP,NAME,NLEVEL,OK)
C 
C     * NOV 03/03 - M.LAZARE. NEW ROUTINE TO READ SPECTRAL DATA INTO
C     *                       REORDERED FORM FOR LOADBALANCING, AND
C     *                       WHICH CONTAINS SUBSET OF DATA APPLICABLE
C     *                       FOR EACH NODE.
C 
      COMPLEX G(LA) 
      COMPLEX WRK(LATOTAL)
      INTEGER LSRTOTAL(2,LMTOTAL+1)
      INTEGER*4 MYNODE
C 
      LOGICAL OK
C
      COMMON /MPINFO/ MYNODE
C
C     * ICOM IS A SHARED I/O AREA. IT MUST BE LARGE ENOUGH
C     * FOR AN 8 WORD LABEL FOLLOWED BY A PACKED GAUSSIAN GRID. 
C 
      COMMON /ICOM/ IBUF(8),IDAT(1) 
      COMMON /MACHTYP/ MACHINE, INTSIZE 
C---------------------------------------------------------------------
      MAXX=( 2*LATOTAL + 8 )*MACHINE
C
C     * FIRST, READ THE COMPLETE INPUT RECORD INTO WORK SPACE "WRK".
C
      OK=.TRUE.
      CALL GETFLD2(NF,    WRK,KIND,NSTEP,NAME,NLEVEL,IBUF,MAXX,OK) 
      IF(.NOT.OK) RETURN
C     WRITE(6,6025) (IBUF(I),I=1,8)
C
      LMH=LMTOTAL/2       
      MC=0
      LAC=0
C
C     * MPI HOOK.
C
      MSTART=MYNODE*LM+1
      MEND  =MYNODE*LM+LM
C
C     * CONSTRUCT "RECTANGLE" FROM PAIRS OF LOW/HIGH ZONAL WAVENUMBERS.
C
      DO M=1,LMH
C
        MVAL=M
        MC=MC+1
        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          NL=LSRTOTAL(1,MVAL)
          NR=LSRTOTAL(1,MVAL+1)-1
          DO N=NL,NR
            LAC=LAC+1
            G(LAC)=WRK(N)
          ENDDO
        ENDIF
C
        MVAL=LMTOTAL-M+1
        MC=MC+1
        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          NL=LSRTOTAL(1,MVAL)
          NR=LSRTOTAL(1,MVAL+1)-1
          DO N=NL,NR
            LAC=LAC+1
            G(LAC)=WRK(N)
          ENDDO
        ENDIF
C
      ENDDO  
C
      RETURN
C------------------------------------------------------------------ 
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
      END 
