      SUBROUTINE RAYLEI (TAUR, IB, DP, IL1, IL2, ILG, LAY)
C
C     * APR 25,2003 - J.LI.
C----------------------------------------------------------------------C
C     RAYLEIGH SCATTERING FOR BANDS2-BANDS4, NEAR INFRARED REGION      C
C                                                                      C
C     TAUR: RAYLEIGH OPTICAL DEPTH                                     C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   RI(3), TAUR(ILG,LAY), DP(ILG,LAY)
      DATA RI / .16305E-04, .17997E-05, .13586E-06 /
C=======================================================================
      IBM1 = IB - 1
      DO 100 K = 1, LAY
      DO 100 I = IL1, IL2
        TAUR(I,K) =  RI(IBM1) * DP(I,K)
  100 CONTINUE
C
      RETURN
      END
