      SUBROUTINE CLDIFM5(CLDMIN, CLDMAX, CLD, A1, NCD, NCU, NBLK, NCT, 
     1                   NCUM, NCDM, LEVC, CUT, MAXC,
     2                   IL1, IL2, ILG, LAY, LEV)
C
C     * DEC 05/07 - K.VONSALZEN/ NEW VERSION FOR GCM15G:
C     *             M.LAZARE.    - CALCULATION OF "ANU" MOVED OUT TO
C     *                            PHYSICS DRIVER. 
C     * JAN 13/07 - K.VONSALZEN/ PREVIOUS VERSION CLDIFM4 FOR GCM15F. 
C     *             M.LAZARE. 
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   CLDMIN(ILG,LAY), CLDMAX(ILG,LAY), CLD(ILG,LAY), A1(ILG,10)
C
      INTEGER NCD(ILG,LAY), NCU(ILG,LAY), NBLK(ILG,LAY), NCT(ILG), 
     1        NCUM(LAY), NCDM(LAY), LEVC(ILG,LAY)
C
C     * INTERNAL WORK ARRASY.
C
      INTEGER INTG1(ILG), INTG2(ILG)
      PARAMETER ( ALF=-3.E-05 )
C
C----------------------------------------------------------------------C
C     THIS SUBROUTINE DETERMINES THE INFO FOR CLOUD AND LEVEL INFO     C
C     FOR GASEOUS CALCULATION                                          C
C----------------------------------------------------------------------C
C
      DO 10 I = IL1, IL2
        INTG1(I)                =  0
        INTG2(I)                =  0
        NCT(I)                  =  LEV
   10 CONTINUE
C
C----------------------------------------------------------------------C
C     DETERMINE THE HIGHEST CLOUD LOCATION. NCT IS THE UPPER LEVEL OF  C
C     THE HIGHEST CLOUD,                                               C
C----------------------------------------------------------------------C
C
      MAXC = LEV
C
      DO 25 K = 1, LAY
        KM1 = K - 1
        DO 20 I = IL1, IL2
C
C----------------------------------------------------------------------C
C     CLDMAX THE MAXIMUM CLOUD FRACTION FOR EACH CLOUD BLOCK.          C
C     CLDMIN THE MINIMUM CLOUD FRACTION FOR EACH CLOUD BLOCK.          C
C----------------------------------------------------------------------C
C
          IF (CLD(I,K) .LT. CUT)                                    THEN
            CLDMAX(I,K)         =  0.0
          ELSE
            IF (K .EQ. 1)                                           THEN
              CLDMAX(I,K)       =  CLD(I,K)
            ELSE
              CLDMAX(I,K)       =  MAX (CLDMAX(I,KM1), CLD(I,K))
            ENDIF
C
            INTG2(I)            =  INTG2(I) + 1
            IF (INTG2(I) .EQ. 1) NCT(I) = K
          ENDIF
C
   20   CONTINUE
   25 CONTINUE
C
      DO 40 I = IL1, IL2
        MAXC                    =  MIN (NCT(I), MAXC)
        LEVC(I,1)               =  0
        LEVC(I,2)               =  0
        A1(I,1)                 =  0.
        A1(I,2)                 =  0.
        A1(I,3)                 =  0.
   40 CONTINUE
C
C----------------------------------------------------------------------C
C     DETERMINE THE LAYER ORDER FOR EACH CLOUD BLOCK THROUGH DOWN AND  C
C     UP PATHS, NCD AND NCU.                                           C
C     DETERMINE THE TOTAL CLOUD FRACTIONS LOOKING FROM TOP AND SURFACE C
C     FOR ONE CLOUD BLOCK (A CLOUD OCCUPY SEVERAL LAYERS, CHOOSE THE   C
C     MINIMUM VALUE OF NU FOR THE BLOCK.                               C
C     NCT IS THE TOP LEVEL NUMBER FOR THE HIGHEST CLOUD                C
C----------------------------------------------------------------------C
C
      DO 65 K = 2, LEV
        KM1 = K - 1
        L = LEV - K + 1
        LP1 = L + 1
        DO 60 I = IL1, IL2
          IF (CLD(I,KM1) .LT. CUT)                                  THEN
            LEVC(I,1)           =  0
            NCD(I,KM1)          =  0
          ELSE
            LEVC(I,1)           =  LEVC(I,1) + 1
            NCD(I,KM1)          =  LEVC(I,1)
          ENDIF
C
          IF (CLD(I,L) .GE. CUT .AND. L .LT. LAY)                   THEN
            CLDMAX(I,L)         =  MAX (CLDMAX(I,LP1), CLDMAX(I,L))
          ENDIF
   60   CONTINUE
   65 CONTINUE
C
      DO 75 L = LAY, 1, -1
        LP1 = L + 1
        DO 70 I = IL1, IL2
          IF (CLD(I,L) .LT. CUT)                                    THEN
            LEVC(I,2)           =  0
            NCU(I,L)            =  0
            NBLK(I,L)           =  0
            CLDMIN(I,L)         =  1.
          ELSE
            LEVC(I,2)           =  LEVC(I,2) + 1
            NCU(I,L)            =  LEVC(I,2)
            IF (NCU(I,L) .EQ. 1)                                    THEN
              INTG1(I)          =  INTG1(I) + 1
              NBLK(I,L)         =  INTG1(I)
              IF (NBLK(I,L) .GT. 3)  NBLK(I,L) =  3
              IF (NBLK(I,L) .EQ. 1)  A1(I,1)   =  CLDMAX(I,L)
              IF (NBLK(I,L) .EQ. 2)  A1(I,2)   =  CLDMAX(I,L)
              IF (NBLK(I,L) .EQ. 3)  A1(I,3)   =
     1                               MAX (A1(I,3), CLDMAX(I,L))
            ELSE
              NBLK(I,L)         =  NBLK(I,LP1)
            ENDIF
C
            IF (NCU(I,L) .EQ. 1)                                    THEN
              CLDMIN(I,L)       =  CLD(I,L)
            ELSE
              CLDMIN(I,L)       =  MIN (CLDMIN(I,LP1), CLD(I,L))
            ENDIF
          ENDIF
   70   CONTINUE
   75 CONTINUE
C
      DO 80 I = IL1, IL2
        X                       =  A1(I,3) * (1.0 - A1(I,1)) *
     1                            (1.0 - A1(I,2))
        A1(I,4)                 =  A1(I,1) * A1(I,2)
        A1(I,1)                 =  A1(I,1) * (1.0 - A1(I,2))
        IF (A1(I,3) .GE. X + A1(I,2))                               THEN
          Y                     =  A1(I,2)
          Z                     =  A1(I,3) - X - A1(I,2)
        ELSE
          Y                     =  A1(I,3) - X
          Z                     =  0.
        ENDIF
C
        IF (A1(I,3) .GE. X + A1(I,1))                               THEN
          A1(I,6)               =  A1(I,1)
          A1(I,5)               =  A1(I,3) - X - A1(I,6)
        ELSE
          A1(I,6)               =  A1(I,3) - X
          A1(I,5)               =  0.
        ENDIF
        A1(I,3)                 =  X
        A1(I,5)                 =  0.5 * (A1(I,5) + Y)
        A1(I,6)                 =  0.5 * (A1(I,6) + Z)
   80 CONTINUE
C
C----------------------------------------------------------------------C
C     DETERMINE THE MAXIMUM PORTION IN A CLOUD BLOCK                   C
C     DETERMINE THE MAXIMUM NUMBER FOR NCD AND NCU, FOR ITERATION IN   C
C     LONGWAVE                                                         C
C----------------------------------------------------------------------C
C
      DO 105 K = 1, LAY
        KM1 = K - 1
        NCUM(K)                 =  0
        NCDM(K)                 =  0
        DO 100 I = IL1, IL2
          IF (NCD(I,K) .GT. 1)                                      THEN
            CLDMIN(I,K)         =  MIN (CLDMIN(I,KM1), CLDMIN(I,K))
          ENDIF
C
          NCUM(K)               =  MAX (NCU(I,K), NCUM(K))
          NCDM(K)               =  MAX (NCD(I,K), NCDM(K))
  100   CONTINUE
  105 CONTINUE
C
      RETURN
      END
