      SUBROUTINE TLINEHC4(TAUG, COEF1U, COEF1D, COEF2U, COEF2D, Q, CO2,
     1                    DP, DIP, DIR, DT, INPTR, INPT, LEV1,
     2                    IL1, IL2, ILG, LAY)
C
C     * FEB 09,2009 - J.LI.     NEW VERSION FOR GCM15H:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     * APR 18,2008 - M.LAZARE. PREVIOUS VERSION TLINEHC3 FOR GCM15G:
C     *                         - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                           FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                           OF "RADFORCE" MODEL OPTION. 
C     * MAY 05,2006 - M.LAZARE. PREVIOIUS VERSION TLINEHC2 FOR GCM15E:
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION TLINEHC FOR GCM15D.
C----------------------------------------------------------------------C
C     THIS SUBROUTINE DETERMINES THE OPTICAL DEPTH FOR H2O AND CO2 IN  C
C     THE REGION OF 540-800 CM-1                                      C
C                                                                      C
C     TAUG: GASEOUS OPTICAL DEPTH                                      C
C     S:    INPUT H2O MIXING RATIO FOR EACH LAYER                      C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C     DIP:  INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO NEIGHBORING C
C           STANDARD INPUT DATA PRESSURE LEVELS                        C
C     DIR:  INTERPRETATION FACTOR FOR MASS RATIO OF H2O / CO2 BETWEEN  C
C           TWO NEIGHBORING STANDARD INPUT RATIOS                      C
C     DT:   LAYER TEMPERATURE - 250 K                                  C
C     INPR: NUMBER OF THE RATIO LEVEL FOR THE STANDARD 5 RATIOS        C
C     INPT: NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES  C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY), COEF1U(5,11), COEF1D(5,5,7), 
     1                      COEF2U(5,11), COEF2D(5,5,7)
      REAL   Q(ILG,LAY), CO2(ILG,LAY), DP(ILG,LAY), DIP(ILG,LAY), 
     1       DIR(ILG,LAY), DT(ILG,LAY)
      INTEGER INPTR(ILG,LAY), INPT(ILG,LAY)
C
C=======================================================================
      DO 300 K = LEV1, LAY
       IF (INPT(1,K) .LT. 950)                                      THEN
        DO 100 I = IL1, IL2
         M =  INPT(I,K)
         IF (M .LE. 11)                                             THEN
          N     =  M + 1
          IF (M .GT. 0)                                             THEN
            X1  =  COEF1U(1,M) + DT(I,K) * (COEF1U(2,M) + DT(I,K) * 
     1            (COEF1U(3,M) + DT(I,K) * (COEF1U(4,M) +
     2             DT(I,K) * COEF1U(5,M))))
            Y1  =  COEF2U(1,M) + DT(I,K) * (COEF2U(2,M) + DT(I,K) * 
     1            (COEF2U(3,M) + DT(I,K) * (COEF2U(4,M) +
     2             DT(I,K) * COEF2U(5,M))))
          ELSE
            X1  =  0.0
            Y1  =  0.0
          ENDIF
          IF (M .LT. 11)                                            THEN
            X2  =  COEF1U(1,N) + DT(I,K) * (COEF1U(2,N) + DT(I,K) * 
     1            (COEF1U(3,N) + DT(I,K) * (COEF1U(4,N) +
     2             DT(I,K) * COEF1U(5,N))))
            Y2  =  COEF2U(1,N) + DT(I,K) * (COEF2U(2,N) + DT(I,K) * 
     1            (COEF2U(3,N) + DT(I,K) * (COEF2U(4,N) +
     2             DT(I,K) * COEF2U(5,N))))
          ELSE
            X2  =  COEF1D(1,1,1) + DT(I,K) * (COEF1D(2,1,1) + DT(I,K) *
     1            (COEF1D(3,1,1) + DT(I,K) * (COEF1D(4,1,1) +
     2             DT(I,K) * COEF1D(5,1,1))))
            Y2  =  COEF2D(1,1,1) + DT(I,K) * (COEF2D(2,1,1) + DT(I,K) *
     1            (COEF2D(3,1,1) + DT(I,K) * (COEF2D(4,1,1) +
     2             DT(I,K) * COEF2D(5,1,1))))
          ENDIF
         ELSE
          J     =  M - 11
          N     =  J + 1
          L     =  INPTR(I,K)
          IF (L .LT. 1)                                             THEN
            X1  =  COEF1D(1,1,J) + DT(I,K) * (COEF1D(2,1,J) + DT(I,K) * 
     1            (COEF1D(3,1,J) + DT(I,K) * (COEF1D(4,1,J) +
     2             DT(I,K) * COEF1D(5,1,J))))
            X2  =  COEF1D(1,1,N) + DT(I,K) * (COEF1D(2,1,N) + DT(I,K) * 
     1            (COEF1D(3,1,N) + DT(I,K) * (COEF1D(4,1,N) +
     2             DT(I,K) * COEF1D(5,1,N))))
C
            Y1  =  COEF2D(1,1,J) + DT(I,K) * (COEF2D(2,1,J) + DT(I,K) * 
     1            (COEF2D(3,1,J) + DT(I,K) * (COEF2D(4,1,J) +
     2             DT(I,K) * COEF2D(5,1,J))))
            Y2  =  COEF2D(1,1,N) + DT(I,K) * (COEF2D(2,1,N) + DT(I,K) * 
     1            (COEF2D(3,1,N) + DT(I,K) * (COEF2D(4,1,N) +
     2             DT(I,K) * COEF2D(5,1,N))))
C
          ELSE IF (L .LT. 5)                                        THEN
            LP1 =  L + 1
            X11 =  COEF1D(1,L,J) + DT(I,K) * (COEF1D(2,L,J) + DT(I,K) * 
     1            (COEF1D(3,L,J) + DT(I,K) * (COEF1D(4,L,J) +
     2             DT(I,K) * COEF1D(5,L,J))))
            X21 =  COEF1D(1,L,N) + DT(I,K) * (COEF1D(2,L,N) + DT(I,K) * 
     1            (COEF1D(3,L,N) + DT(I,K) * (COEF1D(4,L,N) +
     2             DT(I,K) * COEF1D(5,L,N))))
C
            Y11 =  COEF2D(1,L,J) + DT(I,K) * (COEF2D(2,L,J) + DT(I,K) * 
     1            (COEF2D(3,L,J) + DT(I,K) * (COEF2D(4,L,J) +
     2             DT(I,K) * COEF2D(5,L,J))))
            Y21 =  COEF2D(1,L,N) + DT(I,K) * (COEF2D(2,L,N) + DT(I,K) * 
     1            (COEF2D(3,L,N) + DT(I,K) * (COEF2D(4,L,N) +
     2             DT(I,K) * COEF2D(5,L,N))))
C
            X12 =  COEF1D(1,LP1,J) + DT(I,K) * (COEF1D(2,LP1,J) +
     1                               DT(I,K) * (COEF1D(3,LP1,J) + 
     2                               DT(I,K) * (COEF1D(4,LP1,J) + 
     3                               DT(I,K) * COEF1D(5,LP1,J))))
            X22 =  COEF1D(1,LP1,N) + DT(I,K) * (COEF1D(2,LP1,N) +
     1                               DT(I,K) * (COEF1D(3,LP1,N) + 
     2                               DT(I,K) * (COEF1D(4,LP1,N) + 
     3                               DT(I,K) * COEF1D(5,LP1,N))))
C
            Y12 =  COEF2D(1,LP1,J) + DT(I,K) * (COEF2D(2,LP1,J) +
     1                               DT(I,K) * (COEF2D(3,LP1,J) + 
     2                               DT(I,K) * (COEF2D(4,LP1,J) + 
     3                               DT(I,K) * COEF2D(5,LP1,J))))
            Y22 =  COEF2D(1,LP1,N) + DT(I,K) * (COEF2D(2,LP1,N) +
     1                               DT(I,K) * (COEF2D(3,LP1,N) + 
     2                               DT(I,K) * (COEF2D(4,LP1,N) + 
     3                               DT(I,K) * COEF2D(5,LP1,N))))
C
            X1  =  X11 + (X12 - X11) * DIR(I,K)
            X2  =  X21 + (X22 - X21) * DIR(I,K)
            Y1  =  Y11 + (Y12 - Y11) * DIR(I,K)
            Y2  =  Y21 + (Y22 - Y21) * DIR(I,K)
          ELSE
            X1  =  COEF1D(1,5,J) + DT(I,K) * (COEF1D(2,5,J) + DT(I,K) * 
     1            (COEF1D(3,5,J) + DT(I,K) * (COEF1D(4,5,J) +
     2             DT(I,K) * COEF1D(5,5,J))))
            X2  =  COEF1D(1,5,N) + DT(I,K) * (COEF1D(2,5,N) + DT(I,K) * 
     1            (COEF1D(3,5,N) + DT(I,K) * (COEF1D(4,5,N) +
     2             DT(I,K) * COEF1D(5,5,N))))
            Y1  =  COEF2D(1,5,J) + DT(I,K) * (COEF2D(2,5,J) + DT(I,K) * 
     1            (COEF2D(3,5,J) + DT(I,K) * (COEF2D(4,5,J) +
     2             DT(I,K) * COEF2D(5,5,J))))
            Y2  =  COEF2D(1,5,N) + DT(I,K) * (COEF2D(2,5,N) + DT(I,K) *
     1            (COEF2D(3,5,N) + DT(I,K) * (COEF2D(4,5,N) +
     2             DT(I,K) * COEF2D(5,5,N))))
          ENDIF
         ENDIF
C
         TAUG(I,K) = ( (X1 + (X2 - X1) * DIP(I,K)) * Q(I,K) +
     1                 (Y1 + (Y2 - Y1) * DIP(I,K)) * CO2(I,K) ) * 
     2                DP(I,K)
  100   CONTINUE

       ELSE
        M =  INPT(1,K) - 1000
        DO 200 I = IL1, IL2
         IF (M .LE. 11)                                             THEN
          N     =  M + 1
          IF (M .GT. 0)                                             THEN
            X1  =  COEF1U(1,M) + DT(I,K) * (COEF1U(2,M) + DT(I,K) * 
     1            (COEF1U(3,M) + DT(I,K) * (COEF1U(4,M) +
     2             DT(I,K) * COEF1U(5,M))))
            Y1  =  COEF2U(1,M) + DT(I,K) * (COEF2U(2,M) + DT(I,K) * 
     1            (COEF2U(3,M) + DT(I,K) * (COEF2U(4,M) +
     2             DT(I,K) * COEF2U(5,M))))
          ELSE
            X1  =  0.0
            Y1  =  0.0
          ENDIF
          IF (M .LT. 11)                                            THEN
            X2  =  COEF1U(1,N) + DT(I,K) * (COEF1U(2,N) + DT(I,K) * 
     1            (COEF1U(3,N) + DT(I,K) * (COEF1U(4,N) +
     2             DT(I,K) * COEF1U(5,N))))
            Y2  =  COEF2U(1,N) + DT(I,K) * (COEF2U(2,N) + DT(I,K) * 
     1            (COEF2U(3,N) + DT(I,K) * (COEF2U(4,N) +
     2             DT(I,K) * COEF2U(5,N))))
          ELSE
            X2  =  COEF1D(1,1,1) + DT(I,K) * (COEF1D(2,1,1) + DT(I,K) *
     1            (COEF1D(3,1,1) + DT(I,K) * (COEF1D(4,1,1) +
     2             DT(I,K) * COEF1D(5,1,1))))
            Y2  =  COEF2D(1,1,1) + DT(I,K) * (COEF2D(2,1,1) + DT(I,K) *
     1            (COEF2D(3,1,1) + DT(I,K) * (COEF2D(4,1,1) +
     2             DT(I,K) * COEF2D(5,1,1))))
          ENDIF
         ELSE
          J     =  M - 11
          N     =  J + 1
          L     =  INPTR(I,K)
          IF (L .LT. 1)                                             THEN
            X1  =  COEF1D(1,1,J) + DT(I,K) * (COEF1D(2,1,J) + DT(I,K) * 
     1            (COEF1D(3,1,J) + DT(I,K) * (COEF1D(4,1,J) +
     2             DT(I,K) * COEF1D(5,1,J))))
            X2  =  COEF1D(1,1,N) + DT(I,K) * (COEF1D(2,1,N) + DT(I,K) * 
     1            (COEF1D(3,1,N) + DT(I,K) * (COEF1D(4,1,N) +
     2             DT(I,K) * COEF1D(5,1,N))))
C
            Y1  =  COEF2D(1,1,J) + DT(I,K) * (COEF2D(2,1,J) + DT(I,K) * 
     1            (COEF2D(3,1,J) + DT(I,K) * (COEF2D(4,1,J) +
     2             DT(I,K) * COEF2D(5,1,J))))
            Y2  =  COEF2D(1,1,N) + DT(I,K) * (COEF2D(2,1,N) + DT(I,K) * 
     1            (COEF2D(3,1,N) + DT(I,K) * (COEF2D(4,1,N) +
     2             DT(I,K) * COEF2D(5,1,N))))
C
          ELSE IF (L .LT. 5)                                        THEN
            LP1 =  L + 1
            X11 =  COEF1D(1,L,J) + DT(I,K) * (COEF1D(2,L,J) + DT(I,K) * 
     1            (COEF1D(3,L,J) + DT(I,K) * (COEF1D(4,L,J) +
     2             DT(I,K) * COEF1D(5,L,J))))
            X21 =  COEF1D(1,L,N) + DT(I,K) * (COEF1D(2,L,N) + DT(I,K) * 
     1            (COEF1D(3,L,N) + DT(I,K) * (COEF1D(4,L,N) +
     2             DT(I,K) * COEF1D(5,L,N))))
C
            Y11 =  COEF2D(1,L,J) + DT(I,K) * (COEF2D(2,L,J) + DT(I,K) * 
     1            (COEF2D(3,L,J) + DT(I,K) * (COEF2D(4,L,J) +
     2             DT(I,K) * COEF2D(5,L,J))))
            Y21 =  COEF2D(1,L,N) + DT(I,K) * (COEF2D(2,L,N) + DT(I,K) * 
     1            (COEF2D(3,L,N) + DT(I,K) * (COEF2D(4,L,N) +
     2             DT(I,K) * COEF2D(5,L,N))))
C
            X12 =  COEF1D(1,LP1,J) + DT(I,K) * (COEF1D(2,LP1,J) +
     1                               DT(I,K) * (COEF1D(3,LP1,J) + 
     2                               DT(I,K) * (COEF1D(4,LP1,J) + 
     3                               DT(I,K) * COEF1D(5,LP1,J))))
            X22 =  COEF1D(1,LP1,N) + DT(I,K) * (COEF1D(2,LP1,N) +
     1                               DT(I,K) * (COEF1D(3,LP1,N) + 
     2                               DT(I,K) * (COEF1D(4,LP1,N) + 
     3                               DT(I,K) * COEF1D(5,LP1,N))))
C
            Y12 =  COEF2D(1,LP1,J) + DT(I,K) * (COEF2D(2,LP1,J) +
     1                               DT(I,K) * (COEF2D(3,LP1,J) + 
     2                               DT(I,K) * (COEF2D(4,LP1,J) + 
     3                               DT(I,K) * COEF2D(5,LP1,J))))
            Y22 =  COEF2D(1,LP1,N) + DT(I,K) * (COEF2D(2,LP1,N) +
     1                               DT(I,K) * (COEF2D(3,LP1,N) + 
     2                               DT(I,K) * (COEF2D(4,LP1,N) + 
     3                               DT(I,K) * COEF2D(5,LP1,N))))
C
            X1  =  X11 + (X12 - X11) * DIR(I,K)
            X2  =  X21 + (X22 - X21) * DIR(I,K)
            Y1  =  Y11 + (Y12 - Y11) * DIR(I,K)
            Y2  =  Y21 + (Y22 - Y21) * DIR(I,K)
          ELSE
            X1  =  COEF1D(1,5,J) + DT(I,K) * (COEF1D(2,5,J) + DT(I,K) * 
     1            (COEF1D(3,5,J) + DT(I,K) * (COEF1D(4,5,J) +
     2             DT(I,K) * COEF1D(5,5,J))))
            X2  =  COEF1D(1,5,N) + DT(I,K) * (COEF1D(2,5,N) + DT(I,K) * 
     1            (COEF1D(3,5,N) + DT(I,K) * (COEF1D(4,5,N) +
     2             DT(I,K) * COEF1D(5,5,N))))
            Y1  =  COEF2D(1,5,J) + DT(I,K) * (COEF2D(2,5,J) + DT(I,K) * 
     1            (COEF2D(3,5,J) + DT(I,K) * (COEF2D(4,5,J) +
     2             DT(I,K) * COEF2D(5,5,J))))
            Y2  =  COEF2D(1,5,N) + DT(I,K) * (COEF2D(2,5,N) + DT(I,K) *
     1            (COEF2D(3,5,N) + DT(I,K) * (COEF2D(4,5,N) +
     2             DT(I,K) * COEF2D(5,5,N))))
          ENDIF
         ENDIF
C
         TAUG(I,K) = ( (X1 + (X2 - X1) * DIP(I,K)) * Q(I,K) +
     1                 (Y1 + (Y2 - Y1) * DIP(I,K)) * CO2(I,K) ) * 
     2                DP(I,K)
  200   CONTINUE
       ENDIF
  300 CONTINUE

      RETURN
      END
