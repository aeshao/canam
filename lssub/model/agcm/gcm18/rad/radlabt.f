      SUBROUTINE RADLABT(LSW,LLW,LSWT,LLWT,NBS,NBL,IM)

C     * Jan 26/14 - M.Lazare. New routine for gcm18.
C 
C     * DEFINES LEVEL INDEX VALUES FOR SOLAR AND LONGWAVE RADIATION.
C 
      IMPLICIT NONE

      INTEGER NBS,NBL,IM,L,M,MSW,MLW

      INTEGER LSW(NBS),LLW(NBL)
      INTEGER LSWT(IM*NBS),LLWT(IM*NBL)
C-----------------------------------------------------------------------
C     * NON-TILED USUAL INPUT/OUTPUT.
C
      DO 10 L=1,NBS 
        LSW(L)=L 
  10  CONTINUE
  
      DO 20 L=1,NBL
        LLW(L)=L 
  20  CONTINUE
C
C     * TILED INPUT/OUTPUT.
C
      IF(IM.EQ.1)                                                THEN
        DO 100 L=1,NBS 
          LSWT(L)=L 
  100   CONTINUE
  
        DO 200 L=1,NBL
          LLWT(L)=L 
  200   CONTINUE

      ELSE

        MSW=0
        DO 300 L=1,NBS
        DO 300 M=1,IM
          MSW      = MSW+1
          LSWT(MSW) = 1000*L + M 
  300   CONTINUE
C
        MLW=0   
        DO 400 L=1,NBL
        DO 400 M=1,IM
          MLW      = MLW+1
          LLWT(MLW) = 1000*L + M 
  400   CONTINUE

      ENDIF
  
      RETURN
      END 
