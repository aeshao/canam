      SUBROUTINE ATTENUE5(ATTEN, COEF1, S1, DP, DIP, DT, DT0, RMU,
     1                    INPT, LC, ISL, IL1, IL2, ILG)
C
C     * MAY 01,2012 - M.LAZARE. NEW VERSION FOR GCM16:
C     *                         - CORRECTS BUG WHICH (IT TURNS OUT)
C     *                           DOESN'T CHANGE THE BIT-PATTERN
C     *                           BECAUSE OF PFULL(1)~0.5 BEING
C     *                           BETWEEN PRESSURE LEVELS IN
C     *                           STANDP OF ROUTINE PREINTP2, THIS
C     *                           PART OF THE ROUTINE IS NEVER
C     *                           ACTIVATED AT THE MOMENT. 
C     *                           OTHERWISE, THERE WOULD HAVE BEEN A
C     *                           MEMORY FAULT. THIS REFERS TO THE
C     *                           CALCULATION OF "N" AT THE START OF
C     *                           LOOP 2000.
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION ATTENUE4 FOR GCM15H/I:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     * APR 18,2008 - M.LAZARE. PREVIOUS VERSION ATTENUE3 FOR GCM15G:
C     *                         - COSMETIC CHANGE OF N->LC PASSED
C     *                           IN AND USED IN DIMENSION OF COEF
C     *                           ARRAY(S), SO THAT IT IS NOT REDEFINED
C     *                           AND PASSED BACK OUT TO CALLING
C     *                           ROUTINE, CHANGING VALUE IN DATA
C     *                           STATEMENT!
C     *                         - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                           FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                           OF "RADFORCE" MODEL OPTION. 
C     * DEC 05,2007 - M.LAZARE/ NEW VERSION FOR GCM15G:
C     *               J.LI.     - WORK ARRAY "S1" NOW LOCAL INSTEAD
C     *                           OF PASSED IN.
C     *                         - METHANE CONTRIBUTION ADDED THROUGH
C     *                           NG=4. 
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION ATTENUE2 FOR GCM15E/F:
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     *                         PREVIOUS VERSION ATTENUE BY JIANGNAN LI.
C----------------------------------------------------------------------C
C     THIS SUBROUTINE CALCULATES THE DOWNWARD FLUX ATTENUATION ABOVE   C
C     THE MODEL TOP LEVEL                                              C
C     ISL = 1 FOR SOLAR, ISL = 2 FOR INFRARED.                         C
C     NG = 1, H2O; NG = 2, O3; NG = 3, CO2; NG = 6, O2                 C
C     ASSUMING THE TEMPERATURE AT 0.0005 MB IS 210 K                   C
C                                                                      C
C     ATTEN: FOR SOLAR: THE ATTENUATION FACTOR FOR DOWNWARD FLUX FROM  C
C            TOA TO THE MODEL TOP LEVEL; FOR LONGWAVE: THE OPTICAL     C
C            / DIFFUSE FACTOR                                          C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     O3:    O3 MASS MIXING RATIO                                      C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     DT0:   TEMPERATURE IN MOON LAYER - 250 K                         C
C     RMU:   COS OF SOLAR ZENITH ANGLE                                 C
C----------------------------------------------------------------------C
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL  ATTEN(ILG), COEF1(5,LC), DP(ILG), S1(ILG), DIP(ILG),
     1      DT(ILG), DT0(ILG), RMU(ILG)
      INTEGER INPT(ILG)
C
      DATA RU / 1.6487213 /
C=======================================================================
C
      IF (ISL .EQ. 1)                                               THEN
       IF(INPT(1).LT.950)                                           THEN
        DO 1000 I = IL1, IL2
          N =  INPT(I)
          NM1 =  MAX (N - 1, 1)
          X1       =   COEF1(1,NM1) + DT0(I) * (COEF1(2,NM1) + DT0(I) *
     1                (COEF1(3,NM1) + DT0(I) * (COEF1(4,NM1) + DT0(I) *
     2                 COEF1(5,1))))
          X2       =   COEF1(1,N) + DT(I) * (COEF1(2,N) + DT(I) *
     1                (COEF1(3,N) + DT(I) * (COEF1(4,N) + DT(I) *
     2                 COEF1(5,N))))
C
          TAU      =  (X1 + (X2 - X1) * DIP(I)) * 1.02 * S1(I) * DP(I)
C
          ATTEN(I) =   EXP( - TAU / RMU(I))
 1000   CONTINUE
       ELSE
        N =  INPT(1) - 1000
        NM1 =  MAX (N - 1, 1)
        DO 1002 I = IL1, IL2
          X1       =   COEF1(1,NM1) + DT0(I) * (COEF1(2,NM1) + DT0(I) *
     1                (COEF1(3,NM1) + DT0(I) * (COEF1(4,NM1) + DT0(I) *
     2                 COEF1(5,1))))
          X2       =   COEF1(1,N) + DT(I) * (COEF1(2,N) + DT(I) *
     1                (COEF1(3,N) + DT(I) * (COEF1(4,N) + DT(I) *
     2                 COEF1(5,N))))
C
          TAU      =  (X1 + (X2 - X1) * DIP(I)) * 1.02 * S1(I) * DP(I)
C
          ATTEN(I) =   EXP( - TAU / RMU(I))
 1002   CONTINUE
       ENDIF
C
      ELSE
       IF(INPT(1).LT.950)                                          THEN
        DO 2000 I = IL1, IL2
          N =  INPT(I)
          NM1 =  MAX (N - 1, 1)
          X1       =   COEF1(1,NM1) + DT0(I) * (COEF1(2,NM1) + DT0(I) *
     1                (COEF1(3,NM1) + DT0(I) * (COEF1(4,NM1) + DT0(I) *
     2                 COEF1(5,1))))
          X2       =   COEF1(1,N) + DT(I) * (COEF1(2,N) + DT(I) *
     1                (COEF1(3,N) + DT(I) * (COEF1(4,N) + DT(I) *
     2                 COEF1(5,N))))
C
          TAU      =  (X1 + (X2 - X1) * DIP(I)) * 1.02 * S1(I) * DP(I)
C
          ATTEN(I) =   RU * TAU
 2000   CONTINUE
       ELSE
        N =  INPT(1) - 1000
        NM1 =  MAX (N - 1, 1)
        DO 2002 I = IL1, IL2
          X1       =   COEF1(1,NM1) + DT0(I) * (COEF1(2,NM1) + DT0(I) *
     1                (COEF1(3,NM1) + DT0(I) * (COEF1(4,NM1) + DT0(I) *
     2                 COEF1(5,1))))
          X2       =   COEF1(1,N) + DT(I) * (COEF1(2,N) + DT(I) *
     1                (COEF1(3,N) + DT(I) * (COEF1(4,N) + DT(I) *
     2                 COEF1(5,N))))
C
          TAU      =  (X1 + (X2 - X1) * DIP(I)) * 1.02 * S1(I) * DP(I)
C
          ATTEN(I) =   RU * TAU
 2002   CONTINUE
       ENDIF 
      ENDIF
C
      RETURN
      END
