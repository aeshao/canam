      SUBROUTINE GASOPTL7(TAUG, GW, DP, IB, IG, O3, Q, CO2, CH4, AN2O,
     1                    F11, F12, F113, F114, INPTR, INPT, MCONT, 
     2                    DIR, DIP, DT, LEV1, GH, IL1, IL2, ILG, LAY)
C
C     * FEB 02/2018 - J.LI. CORRECTS THE WATER VAPOUR CONTINUUM IN BAND 6
C     *                     WHICH WAS NOT ACCURATE FOR MUCH WARMER TEMPS.
C     * APR 21/2010 - J.LI.     NEW VERSION FOR GCM15I:
C     *                         - REVISION OF MANY BANDS FOR GREATER 
C     *                           ACCURACY.
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION GASOPTL6 FOR GCM15H:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS TLINE{1,2,3}Z INSTEAD OF 
C     *                           TLINE{1,2,3}Y. ALSO CALLS TLINEHC4,
C     *                           TCONTHL4 INSTEAD OF TLINEHC3,TCONTHL3.
C     * APR 18,2008 - M.LAZARE/ PREVIOUS VERSION GASOPTL5 FOR GCM15G:
C     *               L.SOLHEIM/- COSMETIC CHANGE TO ADD THREADPRIVATE
C     *               J.LI.       FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                         - CALLS TLINE{1,2,3}Y INSTEAD OF 
C     *                           TLINE{1,2,3}X. ALSO CALLS TLINEHC3,
C     *                           TCONTHL3 INSTEAD OF TLINEHC2,TCONTHL2.
C     * JUN 21,2006 - J.LI.     PREVIOUS VERSION GASOPTL4 FOR GCM15F:
C     *                         - UPDATE THE MINOR GAS ABSORPTION.
C     *                         - CALLS NEW TCONTHL2.  
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION GASOPTL3 FOR GCM15E:
C     *                         - PASS INTEGER VARIABLES "INIT" AND
C     *                           "MIT" INSTEAD OF ACTUAL INTEGER
C     *                           VALUES, TO "TLINE_" ROUTINES.
C     * DEC 07/2004 - J.LI.     PREVIOUS VERSION GASOPTL2 FOR GCM15D.
C----------------------------------------------------------------------C
C     CALCULATION OF THE OPTICAL DEPTHS DUE TO NONGRAY GASEOUS         C
C     ABSORPTION FOR THE INFRARED, IN EACH LAYER FOR A GIVEN BAND IB   C
C     AND CUMULATIVE PROBABILITY GW.                                   C
C     FROM BAND1 TO BAND4, THE SOLAR AND INFRARED INTERACTION IS       C
C     CONSIDERED. THE TOTAL SOLAR ENERGY CONSIDERED IN THE INFRARED    C
C     REGION IS 11.9006 W / M2                                        C
C     TLINE, ETC., DEAL WITH LINE ABSORPTION AND TCONTL AND TCONTHL    C
C     DEAL WITH WATER VAPOR CONTINUUM                                  C
C                                                                      C
C     TAUG:  GASEOUS OPTICAL DEPTH                                     C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     O3:    O3 MASS MIXING RATIO                                      C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     AN2O:  N2O, ALSO CO2, CH4, F11, F12, F113, F114 ARE MASS MIXING  C
C            RATIOS                                                    C
C     DIR:   INTERPRETATION FACTOR FOR MASS RATIO OF H2O / CO2         C
C            BETWEEN TWO NEIGHBORING STANDARD INPUT RATIOS             C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     INPR:  NUMBER OF THE RATIO LEVEL FOR THE STANDARD 5 RATIOS       C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C     MCONT: THE HIGHEST LEVEL FOR WATER VAPOR CONTINUUM CALCULATION   C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY)
C
      REAL   DP(ILG,LAY), O3(ILG,LAY), Q(ILG,LAY), CO2(ILG,LAY),
     1       CH4(ILG,LAY), AN2O(ILG,LAY), F11(ILG,LAY), F12(ILG,LAY),
     2       F113(ILG,LAY), F114(ILG,LAY), DIR(ILG,LAY), DIP(ILG,LAY), 
     3       DT(ILG,LAY)
      INTEGER INPTR(ILG,LAY), INPT(ILG,LAY)
      LOGICAL GH
C
      COMMON /BANDL1/  GW1(1),  CL1H2O(5,18), CL1CO2(5,18)
      COMMON /BANDL2/  GW2(1),  CL2H2O(5,18), 
     1                          CL2CS(5,3), CL2CF(5,3)
      COMMON /BANDL3/  GW3(2),  CL3H2O(5,18,2), CL3CH4(2),
     1                          CL3CS(5,4,2), CL3CF(5,4,2)
      COMMON /BANDL4/  GW4(5),  CL4H2O(5,18,5), CL4N2O(5,18,5),
     1                          CL4CH4(5,18,5),
     2                          CL4CS(5,4,5), CL4CF(5,4,5),
     3                          CL4F12(5)
      COMMON /BANDL5/  GW5(2),  CL5H2O(5,18,2), CL5O3(5,18,2),
     1                          CL5CS(5,4,2), CL5CF(5,4,2),
     2                          CL5F11(2), CL5F12(2)
      COMMON /BANDL6/  GW6(3),  CL6H2O(5,18,3), CL6F11(5,18,3),
     1                          CL6F12(5,18,3), CL6CS(5,4,3), 
     2                          CL6CF(5,4,3)
      COMMON /BANDL7/  GW7(3),  CL7H2OU(5,11,3), CL7H2OD(5,5,7,3),
     1                          CL7CO2U(5,11,3), CL7CO2D(5,5,7,3), 
     2                          CL7N2O(3), CL7O3(3),
     3                          CL7CS(5,5,4,3), CL7CF(5,5,4,3)
      COMMON /BANDL8/  GW8(6),  CL8H2O(5,18,6),
     1                          CL8CS(5,6,4), CL8CF(5,6,4)
      COMMON /BANDL9/  GW9(4),  CL9H2O(5,18,4),
     1                          CL9CS(5,6,4), CL9CF(5,6,4)
C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY ("M" REFERENCES NON-SATURATED BANDS ACTIVE BELOW 1 MB ONLY).
C
      DATA MTL /18/
C=======================================================================
      IF (IB .EQ. 1)                                                THEN
C
C----------------------------------------------------------------------C
C     BAND (2500 - 2200 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O AND  C
C     CO2.                                                             C
C----------------------------------------------------------------------C
C
      CALL TLINE2Z(TAUG, CL1H2O, CL1CO2, Q, CO2, DP, DIP, DT, INPT,
     1             LEV1, GH, MTL, IL1, IL2, ILG, LAY)
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE N2O EFFECT                                        C
C----------------------------------------------------------------------C
C
      DO 100 K = LEV1, LAY
      DO 100 I = IL1, IL2
        CAN2O     =  2.0437 * 1.E-07 * SQRT(AN2O(I,K) * 1.E+07)
        FACT      =  Q(I,K) / (Q(I,K) + 8.E+04 * CAN2O)
        TAUG(I,K) =  TAUG(I,K) + (754.9786 + 10141.5049 * FACT * FACT) *
     1               CAN2O * DP(I,K)
  100 CONTINUE
C
      GW =  GW1(IG)
C
      ELSE IF (IB .EQ. 2)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (2200 - 1900 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O + N2OC
C----------------------------------------------------------------------C
C
      INIT = 2
      CALL TLINE1Z(TAUG, CL2H2O, Q, DP, DIP, DT, INPT, LEV1, GH, 
     1             MTL, INIT, IL1, IL2, ILG, LAY)
C
      LC =  3
      CALL TCONTL1(TAUG, CL2CS, CL2CF, Q, DP, DIP, DT, LC, INPT, MCONT,
     1             GH, IL1, IL2, ILG, LAY)
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE N2O EFFECT                                        C
C----------------------------------------------------------------------C
C
      DO 200 K = LEV1, LAY
      DO 200 I = IL1, IL2
        CAN2O     =  2.0437 * 1.E-07 * SQRT(AN2O(I,K) * 1.E+07)
        FACT      =  Q(I,K) / (Q(I,K) + 72000. * CAN2O)
        TAUG(I,K) =  TAUG(I,K) + (93. + 3500. * FACT * FACT) *
     1               CAN2O * DP(I,K)
  200 CONTINUE
C
      GW =  GW2(IG)
C
      ELSE IF (IB .EQ. 3)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (1900 - 1400 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.     C
C----------------------------------------------------------------------C
C
      INIT = 2
      CALL TLINE1Z(TAUG, CL3H2O(1,1,IG), Q, DP, DIP, DT, INPT, LEV1,
     1             GH, MTL, INIT, IL1, IL2, ILG, LAY)
C
      LC =  4
      CALL TCONTL1(TAUG, CL3CS(1,1,IG), CL3CF(1,1,IG), Q, DP, DIP, DT, 
     1             LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
      DO 300 K = LEV1, LAY
      DO 300 I = IL1, IL2
        TAUG(I,K) =  TAUG(I,K) + CL3CH4(IG) * CH4(I,K) * DP(I,K)
  300 CONTINUE
C
      GW =  GW3(IG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND3 (1100 - 1400 CM-1), OVERLAPPING ABSORPTION OF H2O, N2O,   C
C     CH4 AND CFC12. DIRECT MAPPING METHOD FOR H2O AND CH4 AND N2O     C
C     CFC ARE CONSIDERED AS MINOR GASES                                C
C----------------------------------------------------------------------C
C
      CALL TLINE3Z(TAUG, CL4H2O(1,1,IG), CL4CH4(1,1,IG), CL4N2O(1,1,IG),
     1             Q, CH4, AN2O, DP, DIP, DT, INPT, LEV1, GH, MTL, 
     2             IL1, IL2, ILG, LAY)
C
      LC =  4
      CALL TCONTL1(TAUG, CL4CS(1,1,IG), CL4CF(1,1,IG), Q, DP, DIP, DT, 
     1             LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE CFC EFFECT                                        C
C----------------------------------------------------------------------C
C
      DO 400 K = LEV1, LAY
      DO 400 I = IL1, IL2
          TAUG(I,K) =  TAUG(I,K) + (CL4F12(IG) * F12(I,K) +
     1                 1037.3 * F113(I,K) + 1426.9 * F114(I,K)) *
     2                 DP(I,K)
  400 CONTINUE
C
      GW =  GW4(IG)
C
      ELSE IF (IB .EQ. 5)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND5 (980 - 1100 CM-1), OVERLAPPING ABSORPTION OF H2O AND O3   C
C     DIRECT MAPPING METHOD. CO2 AND CFC ARE SIMPLY ADDED              C
C----------------------------------------------------------------------C
C
      CALL TLINE2Z(TAUG, CL5H2O(1,1,IG), CL5O3(1,1,IG), Q, O3, DP,
     1             DIP, DT, INPT, LEV1, GH, MTL,
     2             IL1, IL2, ILG, LAY)
C
      LC =  4
      CALL TCONTL1(TAUG, CL5CS(1,1,IG), CL5CF(1,1,IG), Q, DP, DIP, DT, 
     1             LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE CO2 + CFC EFFECT                                  C
C     SINCE THE INTERACTION OF CO2 AND H2O, Q(I,K) APPEARS IN CO2      C
C     EFFECT                                                           C
C----------------------------------------------------------------------C
C
      DO 500 K = LEV1, LAY
      DO 500 I = IL1, IL2
        CRMCO2    =  2.3056E-04 * SQRT(CO2(I,K) * 1.E+04)
        TAUG(I,K) =  TAUG(I,K) + ( (0.009 +  0.093 * Q(I,K) / (Q(I,K) +
     1               2.1 * CRMCO2)) * CO2(I,K) + CL5F11(IG) * F11(I,K) +
     2               CL5F12(IG) * F12(I,K) + 1687.4 * F113(I,K) +
     3               2924.1 * F114(I,K)) * DP(I,K)
  500 CONTINUE
C
      GW =  GW5(IG)
C
      ELSE IF (IB .EQ. 6)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (800 - 980 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.       C
C     + CFC11 AND CFC12                                                C
C----------------------------------------------------------------------C
C
        CALL TLINE3Z(TAUG, CL6H2O(1,1,IG), CL6F11(1,1,IG), 
     1               CL6F12(1,1,IG), Q, F11, F12, DP, DIP, DT, INPT, 
     2               LEV1, GH, MTL, IL1, IL2, ILG, LAY) 
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE CO2 + CFC EFFECT                                  C
C----------------------------------------------------------------------C
C
        DO 600 K = LEV1, LAY
        DO 600 I = IL1, IL2
          TAUG(I,K) =  TAUG(I,K) + ( (0.0074 + 0.0396 * Q(I,K) /
     1                (Q(I,K) + 2.8 * CO2(I,K))) * CO2(I,K) +
     2                 1191. * F113(I,K) + 1098. * F114(I,K) ) * DP(I,K)
  600   CONTINUE 
C
        LC =  4
        CALL TCONTL1 (TAUG, CL6CS(1,1,IG), CL6CF(1,1,IG), Q, DP, DIP,
     1                DT, LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
      GW =  GW6(IG)
C
      ELSE IF (IB .EQ. 7)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND6 (540 - 800 CM-1), OVERLAPPING ABSORPTION OF H2O AND CO2   C
C     EXACT MAPPING METHOD FOR H2O AND CO2, DIRECT MAPPING FOR N2O     C
C     O3 EFFECT IS SIMPLY ADDED                                        C
C----------------------------------------------------------------------C
C
      CALL TLINEHC4(TAUG, CL7H2OU(1,1,IG), CL7H2OD(1,1,1,IG),
     1              CL7CO2U(1,1,IG), CL7CO2D(1,1,1,IG), Q, CO2, DP, DIP,
     2              DIR, DT, INPTR, INPT, LEV1, IL1, IL2, ILG, LAY)
C
      CALL TCONTHL4(TAUG, CL7CS(1,1,1,IG), CL7CF(1,1,1,IG), Q, DP, DIP,
     1              DIR, DT, INPTR, INPT, MCONT, IL1, IL2, ILG, LAY)
C
C----------------------------------------------------------------------C
C     SIMPLY ADD THE O3 AND N2O EFFECT                                 C
C----------------------------------------------------------------------C
C
      DO 700 K = LEV1, LAY
      DO 700 I = IL1, IL2
        CAN2O     =  2.0437 * 1.E-07 * SQRT(AN2O(I,K) * 1.E+07)
        TAUG(I,K) =  TAUG(I,K) + (CL7O3(IG) * O3(I,K) +
     1               CL7N2O(IG) * CAN2O) * DP(I,K)
  700 CONTINUE
C
      GW =  GW7(IG)
C
      ELSE IF (IB .EQ. 8)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (340 - 540 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.       C
C----------------------------------------------------------------------C
C
      INIT = 2
      CALL TLINE1Z(TAUG, CL8H2O(1,1,IG), Q, DP, DIP, DT, INPT, LEV1,
     1             GH, MTL, INIT, IL1, IL2, ILG, LAY)
C
      IF (IG .LE. 4)                                                THEN
        LC =  6
        CALL TCONTL1(TAUG, CL8CS(1,1,IG), CL8CF(1,1,IG), Q, DP, DIP, DT,
     1               LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
      ENDIF
C
      GW =  GW8(IG)
C
      ELSE IF (IB .EQ. 9)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (0 - 340 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.         C
C----------------------------------------------------------------------C
C
      INIT = 2
      CALL TLINE1Z(TAUG, CL9H2O(1,1,IG), Q, DP, DIP, DT, INPT, LEV1,
     1             GH, MTL, INIT, IL1, IL2, ILG, LAY)
C
      LC =  6
      CALL TCONTL1(TAUG, CL9CS(1,1,IG), CL9CF(1,1,IG), Q, DP, DIP, DT,
     1             LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
      GW =  GW9(IG)
C
      ENDIF
C
      RETURN
      END
