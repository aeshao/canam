      SUBROUTINE STRANDNGH4 (TRANT, ITILE,
     1                       GWGH, ATTEN, TAUA, TAUOMA, TAUCS,
     2                       TAUOMC, CLD, RMU, DP, O3, Q, CO2, CH4, O2,
     3                       IB, IG, INPT, DIP, DT, LEV1, GH, CUT,
     4                       IL1, IL2, ILG, LAY, LEV, NTILE)
C
C     * JUN 02,2015 - M.LAZARE/ NEW VERSION FOR GCM19:
C     *               J.COLE:   - ADD TILED RADIATION CALCULATIONS
C     *                           (IE "TRANT")
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION STRANDNGH4 FOR GCM15H
C     *                         THROUGH GCM18:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS TLINE{1,2}Z INSTEAD OF TLINE{1,2}Y.
C     * APR 18,2008 - M.LAZARE/ PREVIOUS VERSION STRANDNGH3 FOR GCM15G:
C     *               L.SOLHEIM/- COSMETIC CHANGE TO ADD THREADPRIVATE
C     *               J.LI.       FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                           OF "RADFORCE" MODEL OPTION. 
C     *                         - CALLS TLINE{1,2}Y INSTEAD OF TLINE{1,2}X.
C     *                         - UPDATING O3, ADDING CH4 AND USING 
C     *                           KURUCZ SOLAR FUNCTION.
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION STRANDNGH2 FOR GCM15E/F:
C     *                         - PASS INTEGER VARIABLES "INIT" AND
C     *                           "NIT" INSTEAD OF ACTUAL INTEGER
C     *                           VALUES, TO "TLINE_" ROUTINES.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION STRANDNGH FOR GCM15D.
C----------------------------------------------------------------------C
C     CALCULATION OF THE DOWNWARD SOLAR FLUX UNDER THE CONDITION THAT  C
C     THE EXTINCTION COEFFICIENT OF GAS IS VERY LARGE, THE SCATTERING  C
C     EFFECTS CAN BE NEGLECTED. THE CLOUD OPTICAL DEPTH IS MUCH SMALLERC
C     THAN THE GASEOUS OPTICAL DEPTH, THE CLOUD EFFECT IS VERY SMALL   C
C     AND BE TREATED SIMPLY                                            C
C                                                                      C
C     HRS:   SOLAR HEATING RATE (K / SEC)                              C
C     TRAN:  DOWNWARD FLUX                                             C
C    TRANT:  TILED DOWNWARD FLUX                                       C
C    ITILE:  FLAG INDICATING CALCULATION FOR PARTICULAR TILE.          C
C     ATTEN: ATTENUATION FACTOR FOR DOWNWARD FLUX FROM TOA TO THE      C
C            MODEL TOP LEVEL                                           C
C     TAUCS: CLOUD OPTICAL DEPTH                                       C
C     CLD:   CLOUD FRACTION                                            C
C     RMU:   COS OF SOLAR ZENITH ANGLE                                 C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     O3:    O3 MASS MIXING RATIO                                      C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     CO2:   CH4, O2 ARE MASS MIXING                                   C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C----------------------------------------------------------------------C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL ATTEN(ILG), TAUA(ILG,LAY), TAUOMA(ILG,LAY),
     1     TAUCS(ILG,LAY), TAUOMC(ILG,LAY), CLD(ILG,LAY), RMU(ILG), 
     2     DP(ILG,LAY), O3(ILG,LAY), Q(ILG,LAY), CO2(ILG,LAY), 
     3     CH4(ILG,LAY), O2(ILG,LAY), DIP(ILG,LAY), DT(ILG,LAY)
      REAL TRANT(ILG,NTILE,2,LEV)
      INTEGER ITILE(ILG,NTILE)
      INTEGER INPT(ILG,LAY)
C
C     * INTERNAL WORK ARRAY.
C
      REAL TAUG(ILG,LAY),TRAN(ILG,2,LEV)
      LOGICAL GH
C
      COMMON /BANDS1GH/ GWS1GH(3), CS1O3GH(3,3),     CS1O2GH3
      COMMON /BANDS2GH/ GWS2GH(4), CS2H2OGH(5,28),   CS2O2GH(5,28,3)
      COMMON /BANDS3GH/ GWS3GH(4), CS3H2OGH(5,28,2), CS3CO2GH(5,28,4)
      COMMON /BANDS4GH/ GWS4GH(9), CS4H2OGH(5,28,6), CS4CO2GH(5,28,6),
     1                             CS4CH4GH(5,28)
C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY.
C
      DATA NTL /28/
C=======================================================================
      DO 10 I = IL1, IL2
        TRAN(I,1,1)           =  ATTEN(I)
        TRAN(I,2,1)           =  ATTEN(I)
   10 CONTINUE
C
      IF (IB .EQ. 1)                                                THEN
C
C----------------------------------------------------------------------C
C     BAND1 FOR UVC (35700 - 50000 CM-1), NONGRAY GASEOUS ABSORPTION  C
C     OF O2  AND O3. SOLAR ENERGY 7.5917  W M-2                       C
C----------------------------------------------------------------------C
C
        IF (IG .EQ. 3)                                              THEN
          DO 105 K = 1, LAY
            KP1 = K + 1
            DO 100 I = IL1, IL2
              DTO3            =  DT(I,K) + 23.13
              TAU             = ((CS1O3GH(1,IG) +
     1                           DTO3 * (CS1O3GH(2,IG) +
     2                           DTO3 * CS1O3GH(3,IG))) * O3(I,K) +
     3                           CS1O2GH3 * O2(I,K)) * DP(I,K) + 
     4                           TAUA(I,K)
              DTR1            =  EXP( - (TAU - TAUOMA(I,K)) / RMU(I))
              TRAN(I,1,KP1)   =  TRAN(I,1,K) * DTR1
C
              IF (CLD(I,K) .LT. CUT)                                THEN
                TRAN(I,2,KP1) =  TRAN(I,2,K) * DTR1
              ELSE
                ABSC          = (1.0 - CLD(I,K)) * DTR1 + CLD(I,K) *
     1                           EXP( - (TAU + TAUCS(I,K) - TAUOMC(I,K))
     2                           / RMU(I))
                TRAN(I,2,KP1) =  TRAN(I,2,K) * ABSC
              ENDIF
  100       CONTINUE
  105     CONTINUE
        ELSE
          DO 115 K = 1, LAY
            KP1 = K + 1
            DO 110 I = IL1, IL2
              DTO3            =  DT(I,K) + 23.13
              TAU             = (CS1O3GH(1,IG) + DTO3 * (CS1O3GH(2,IG) +
     1                           DTO3 * CS1O3GH(3,IG))) * O3(I,K) *
     2                           DP(I,K) + TAUA(I,K)
              DTR1            =  EXP( - (TAU - TAUOMA(I,K)) / RMU(I))
              TRAN(I,1,KP1)   =  TRAN(I,1,K) * DTR1
C
              IF (CLD(I,K) .LT. CUT)                                THEN
                TRAN(I,2,KP1) =  TRAN(I,2,K) * DTR1
              ELSE
                ABSC          = (1.0 - CLD(I,K)) * DTR1 + CLD(I,K) *
     1                           EXP( - (TAU + TAUCS(I,K) - TAUOMC(I,K))
     2                           / RMU(I))
                TRAN(I,2,KP1) =  TRAN(I,2,K) * ABSC
              ENDIF
  110       CONTINUE
  115     CONTINUE
        ENDIF
      GWGH =  GWS1GH(IG)
C
      ELSE IF (IB .EQ. 2)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (8400 - 14500 CM-1), NONGRAY GASEOUS ABSORPTION OF O2      C
C     AND O3. SOLAR ENERGY 8.9036 W M-2                               C
C----------------------------------------------------------------------C
C
        IF (IG .EQ. 1)                                              THEN
          INIT = 2
          CALL TLINE1Z (TAUG, CS2H2OGH(1,1), Q, DP, DIP, DT, INPT,
     1                  LEV1, GH, NTL, INIT, IL1, IL2, ILG, LAY)
        ELSE
          IM =  IG - 1
          INIT = 2
          CALL TLINE1Z (TAUG, CS2O2GH(1,1,IM), O2, DP, DIP, DT, INPT,
     1                  LEV1, GH, NTL, INIT, IL1, IL2, ILG, LAY)
        ENDIF
C
        DO 205 K = 1, LAY
          KP1 = K + 1
          DO 200 I = IL1, IL2
            TAU               =  TAUG(I,K) + TAUA(I,K)
            DTR1              =  EXP( - (TAU - TAUOMA(I,K)) / RMU(I))
            TRAN(I,1,KP1)     =  TRAN(I,1,K) * DTR1
C
            IF (CLD(I,K) .LT. CUT)                                  THEN
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * DTR1
            ELSE
              ABSC            = (1.0 - CLD(I,K)) * DTR1 + CLD(I,K) *
     1                           EXP( - (TAU + TAUCS(I,K) - TAUOMC(I,K))
     2                           / RMU(I))
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * ABSC
            ENDIF
  200     CONTINUE
  205   CONTINUE
C
        GWGH =  GWS2GH(IG)
C
      ELSE IF (IB .EQ. 3)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (4200 - 8400 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O AND  C
C     CO2. SOLAR ENERGY 7.4453 W M-2                                  C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 2)                                              THEN
          CALL TLINE2Z (TAUG, CS3H2OGH(1,1,IG), CS3CO2GH(1,1,IG), Q, 
     1                  CO2, DP, DIP, DT, INPT, LEV1, GH, NTL, 
     2                  IL1, IL2, ILG, LAY)

        ELSE
          INIT = 2
          CALL TLINE1Z (TAUG, CS3CO2GH(1,1,IG), CO2, DP, DIP, DT, 
     1                  INPT, LEV1, GH, NTL, INIT, IL1, IL2, ILG, LAY)
        ENDIF
C
        DO 305 K = 1, LAY
          KP1 = K + 1
          DO 300 I = IL1, IL2
            TAU               =  TAUG(I,K) + TAUA(I,K)
            DTR1              =  EXP( - (TAU - TAUOMA(I,K)) / RMU(I))
            TRAN(I,1,KP1)     =  TRAN(I,1,K) * DTR1
C
            IF (CLD(I,K) .LT. CUT)                                  THEN
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * DTR1
            ELSE
              ABSC            = (1.0 - CLD(I,K)) * DTR1 + CLD(I,K) *
     1                           EXP( - (TAU + TAUCS(I,K) - TAUOMC(I,K))
     2                           / RMU(I))
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * ABSC
            ENDIF
  300     CONTINUE
  305   CONTINUE
C
        GWGH =  GWS3GH(IG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (2500 - 4200 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O      C
C     AND CO2. SOLAR ENERGY 7.0384 W M-2                              C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 3)                                              THEN
          CALL TLINE2Z (TAUG, CS4H2OGH(1,1,IG), CS4CO2GH(1,1,IG), Q, 
     1                  CO2, DP, DIP, DT, INPT, LEV1, GH, NTL,
     2                  IL1, IL2, ILG, LAY)
        ELSE IF (IG .EQ. 6 .OR. IG .EQ. 8)                          THEN
          IF (IG .EQ. 6)  IM = 5
          IF (IG .EQ. 8)  IM = 6
          INIT = 2
          CALL TLINE1Z (TAUG, CS4H2OGH(1,1,IM), Q, DP, DIP, DT, INPT, 
     1                  LEV1, GH, NTL, INIT, IL1, IL2, ILG, LAY)
C
        ELSE IF (IG .EQ. 5 .OR. IG .EQ. 7 .OR. IG .EQ. 9)           THEN
          IF (IG .EQ. 5)  IM = 4
          IF (IG .EQ. 7)  IM = 5
          IF (IG .EQ. 9)  IM = 6
          INIT = 2
          CALL TLINE1Z (TAUG, CS4CO2GH(1,1,IM), CO2, DP, DIP, DT, 
     1                  INPT, LEV1, GH, NTL, INIT, IL1, IL2, ILG, LAY)
        ELSE
          IM = 4
          CALL TLINE2Z (TAUG, CS4H2OGH(1,1,IM), CS4CH4GH, Q, CH4,
     1                  DP, DIP, DT, INPT, LEV1, GH, NTL,
     2                  IL1, IL2, ILG, LAY)
        ENDIF
C
        DO 405 K = 1, LAY
          KP1 = K + 1
          DO 400 I = IL1, IL2
            TAU               =  TAUG(I,K) + TAUA(I,K)
            DTR1              =  EXP( - (TAU - TAUOMA(I,K)) / RMU(I))
            TRAN(I,1,KP1)     =  TRAN(I,1,K) * DTR1
C
            IF (CLD(I,K) .LT. CUT)                                  THEN
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * DTR1
            ELSE
              ABSC            = (1.0 - CLD(I,K)) * DTR1 + CLD(I,K) *
     1                           EXP( - (TAU + TAUCS(I,K) - TAUOMC(I,K))
     2                           / RMU(I))
              TRAN(I,2,KP1)   =  TRAN(I,2,K) * ABSC
            ENDIF
  400     CONTINUE
  405   CONTINUE
C
        GWGH =  GWS4GH(IG)
C
      ENDIF
      DO K = 1, LEV
         DO M = 1, NTILE
            DO I = IL1, IL2
               IF (ITILE(I,M) .GT. 0) THEN
                  TRANT(I,M,1,K) = TRAN(I,1,K)
                  TRANT(I,M,2,K) = TRAN(I,2,K)
               END IF
            END DO ! I
         END DO ! M
      END DO ! K
C
      RETURN
      END
