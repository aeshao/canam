      SUBROUTINE TCONTL1(TAUG, COEF1, COEF2, S, DP, DIP, DT, LC, INPT,
     1                   MCONT, GH, IL1, IL2, ILG, LAY)
C
C     * MAY 05,2006 - M.LAZARE. NEW VERSION FOR GCM15E:
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION TCONTL FOR GCM15D.
C----------------------------------------------------------------------C
C     INFRARED WATER VAPOR CONTINUUM, COEF1 IS THE COEFFICIENT FOR     C
C     SELF, COEF2 IS THE COEFFICIENT FOR FOREIGN. THE CONTINUUM ONLY   C
C     APPLIES TO THE LAYERS BELOW 138.9440 MB OR EVEN LOWER REGION     C
C     DEPENDING ON EACH BAND. LC IS NUMBER OF LEVEL FOR STANDARD       C
C     PRESSURE CONSIDERED IN CALCULATING THE CONTINUUM.                C
C     1.608 = 28.97 / 18.016, A FCTR FOR WATER VAPOR PARTIAL PRESSURE  C
C                                                                      C
C     TAUG:  GASEOUS OPTICAL DEPTH                                     C
C     S:     INPUT H2O MIXING RATIO FOR EACH LAYER                     C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C     MCONT: THE HIGHEST LEVEL FOR WATER VAPOR CONTINUUM CALCULATION   C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY), COEF1(5,LC), COEF2(5,LC)
      REAL   S(ILG,LAY), DP(ILG,LAY), DIP(ILG,LAY), DT(ILG,LAY)
      INTEGER INPT(ILG,LAY)
      LOGICAL GH
C=======================================================================
      IF (GH)                                                       THEN
        NC =  29 - LC
      ELSE
        NC =  19 - LC
      ENDIF
C 
      DO 200 K = MCONT, LAY
        IF (INPT(1,K) .LT. 950)                                     THEN
          DO 100 I = IL1, IL2
            J =  INPT(I,K)
            IF (J .GE. NC)                                          THEN
              M  =  J - NC + 1
              N  =  M + 1
              X1        =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) +
     1                     DT(I,K) * (COEF1(3,M) + DT(I,K) *
     2                    (COEF1(4,M) + DT(I,K) * COEF1(5,M))))
C
              X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) +
     1                     DT(I,K) * (COEF1(3,N) + DT(I,K) *
     2                    (COEF1(4,N) + DT(I,K) * COEF1(5,N))))
C
              Y1        =  COEF2(1,M) + DT(I,K) * (COEF2(2,M) +
     1                     DT(I,K) * (COEF2(3,M) + DT(I,K) *
     2                    (COEF2(4,M) + DT(I,K) * COEF2(5,M))))
C
              Y2        =  COEF2(1,N) + DT(I,K) * (COEF2(2,N) +
     1                     DT(I,K) * (COEF2(3,N) + DT(I,K) *
     2                    (COEF2(4,N) + DT(I,K) * COEF2(5,N))))
C
              TAUG(I,K) =  TAUG(I,K) +
     1                    ( (X1 - Y1 + (X2 - X1 - Y2 + Y1) *
     2                     DIP(I,K)) * 1.608 * S(I,K) +
     3                       Y1 + (Y2 - Y1) * DIP(I,K) ) *
     4                      S(I,K) * DP(I,K)
            ENDIF
  100     CONTINUE
        ELSE
          J  =  INPT(1,K) - 1000
          M  =  J - NC + 1
          N  =  M + 1
          DO 150 I = IL1, IL2
            IF (J .GE. NC)                                          THEN
              X1        =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) +
     1                     DT(I,K) * (COEF1(3,M) + DT(I,K) *
     2                    (COEF1(4,M) + DT(I,K) * COEF1(5,M))))
C
              X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) +
     1                     DT(I,K) * (COEF1(3,N) + DT(I,K) *
     2                    (COEF1(4,N) + DT(I,K) * COEF1(5,N))))
C
              Y1        =  COEF2(1,M) + DT(I,K) * (COEF2(2,M) +
     1                     DT(I,K) * (COEF2(3,M) + DT(I,K) *
     2                    (COEF2(4,M) + DT(I,K) * COEF2(5,M))))
C
              Y2        =  COEF2(1,N) + DT(I,K) * (COEF2(2,N) +
     1                     DT(I,K) * (COEF2(3,N) + DT(I,K) *
     2                    (COEF2(4,N) + DT(I,K) * COEF2(5,N))))
C
              TAUG(I,K) =  TAUG(I,K) +
     1                    ( (X1 - Y1 + (X2 - X1 - Y2 + Y1) *
     2                     DIP(I,K)) * 1.608 * S(I,K) +
     3                       Y1 + (Y2 - Y1) * DIP(I,K) ) *
     4                      S(I,K) * DP(I,K)
            ENDIF
  150     CONTINUE
        ENDIF
  200 CONTINUE
C
      RETURN
      END
