      SUBROUTINE STRANUP3(REFL, ITILE, DP, DT, O3, O2, IB, IG, LEV1,
     1                    IL1, IL2, ILG, LAY, LEV, NTILE)
C
C     * JUN 02,2015 - M.LAZARE/ NEW VERSION FOR GCM19:
C     *               J.COLE:   - ADD TILED RADIATION CALCULATIONS
C     *                           (IE "REFLT"), UNDER CONTROL OF
C     *                           "ITILRAD".
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION STRANUP3 FOR GCM15H
C     *                         THROUGH GCM18:
C     *                         - O2 PASSED DIRECTLY, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK.
C     * APR 21,2008 - L.SOLHEIM/ PREVIOUS VERSION STRANUP2 FOR GCM15G:
C     *               J.LI.      - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                            FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                            OF "RADFORCE" MODEL OPTION. 
C     *                          - MORE ACCUATE TREATMENT OF O3 .
C     * APR 25,2003 - J.LI. PREVIOUS VERSION STRANUP FOR GCM15E/F.
C
C----------------------------------------------------------------------C
C     CALCULATION OF THE UPWARD SOLAR FLUX ABOVE 1 MB, NO SCATTERING   C
C     EFFECT IS CONSIDERED                                             C
C                                                                      C
C     REFL:  REFLECTIVITY                                              C
C     TAU:   OPTICAL DEPTH                                             C
C     DTR:   DIRECT TRANSMISSION FUNCTION                              C
C     1.6487213 DIFFUSIVITY FACTOR (LI, 2000 JAS P753)                 C
C----------------------------------------------------------------------C
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL REFL(ILG,NTILE,2,LEV), DP(ILG,LAY), DT(ILG,LAY), O3(ILG,LAY),
     1     O2(ILG,LAY)
      INTEGER ITILE(ILG,NTILE)
C
      COMMON /BANDS1/ GWS1(6), CS1O3(3,6), CS1O21
C=======================================================================
C
      LEV1M1 =  LEV1 - 1
C
      IF (IB .EQ. 1)                                                THEN
C
        IF (IG .EQ. 1)                                              THEN
          DO 105 K = LEV1M1, 1, - 1
            KP1 = K + 1
            DO 101 M = 1, NTILE
            DO 100 I = IL1, IL2
              IF (ITILE(I,M) .GT. 0) THEN
              DTO3        =  DT(I,K) + 23.13
              TAU         = ((CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                       DTO3 * CS1O3(3,IG))) * O3(I,K) + 
     2                       CS1O21 * O2(I,K)) * DP(I,K)
              DTR         =  EXP( - 1.6487213 * TAU)
              REFL(I,M,1,K) =  REFL(I,M,1,KP1) * DTR
              REFL(I,M,2,K) =  REFL(I,M,2,KP1) * DTR
           END IF
  100       CONTINUE
 101  CONTINUE
  105     CONTINUE
C
        ELSE
          DO 115 K = LEV1M1, 1, - 1
            KP1 = K + 1
            DO 111 M = 1, NTILE
            DO 110 I = IL1, IL2
              IF (ITILE(I,M) .GT. 0) THEN
              DTO3        =  DT(I,K) + 23.13
              TAU         = (CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                       DTO3 * CS1O3(3,IG))) * O3(I,K) * DP(I,K)
              DTR         =  EXP( - 1.6487213 * TAU)
              REFL(I,M,1,K) =  REFL(I,M,1,KP1) * DTR
              REFL(I,M,2,K) =  REFL(I,M,2,KP1) * DTR
           END IF
  110       CONTINUE
 111  CONTINUE
  115     CONTINUE
C
        ENDIF
C
      ELSE
C
        DO 250 K = LEV1M1, 1, - 1
          KP1 = K + 1
            DO 201 M = 1, NTILE
          DO 200 I = IL1, IL2
              IF (ITILE(I,M) .GT. 0) THEN
              REFL(I,M,1,K) =  REFL(I,M,1,KP1)
              REFL(I,M,2,K) =  REFL(I,M,2,KP1)
           END IF
  200     CONTINUE
 201  CONTINUE
  250   CONTINUE
C
      ENDIF
C
      RETURN
      END
