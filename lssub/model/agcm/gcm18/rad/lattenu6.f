      SUBROUTINE LATTENU6(ATTEN, IB, IG, O3, Q, CO2, DP, DIP, DT, DT0,
     1                    INPT, IL1, IL2, ILG)
C
C     * JUN 22,2013 - J.COLE.   SET THE RADIATIVE EFFECT OF THE MOON
C     *                         LAYER TO ZERO AT END (ATTEN=0.),
C     *                         SO THAT BALT=BALX.
C     * MAY 01,2012 - M.LAZARE. PREVIOUS VERSION LATTENU5 FOR GCM16:
C     *                         - CALLS ATTENUE5 INSTEAD OF ATTENUE4.
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION LATTENU4 FOR GCM15H/I:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS ATTENUE4 INSTEAD OF ATTENUE3.
C     * APR 18,2008 - M.LAZARE/ PREVIOUS VERSION LATTENU3 FOR GCM15G:
C     *               J.LI.     - CALLS ATTENUE3 INSTEAD OF ATTENUE2.
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION LATTENU2 FOR GCM15E/F:
C     *                         - PASS INTEGER VARIABLES "INIT" AND
C     *                           "NIT" INSTEAD OF ACTUAL INTEGER
C     *                           VALUES, TO "ATTENUE" ROUTINES.
C     * ORIGINAL VERSION LATTENU BY JIANGNAN LI.
C----------------------------------------------------------------------C
C     CALCULATION OF THE ATTENUATION FOR THE DOWNWARD FLUX ABOVE THE   C
C     MODEL TOP LEVEL. SINCE THE TEMPERATURE AT 0.005 MB IS UNKNOWN WE C
C     ASSUME IT IS THE SAME AS THAT OF MODEL TOP LEVEL                 C
C                                                                      C
C     ATTEN: FOR SOLAR: THE ATTENUATION FACTOR FOR DOWNWARD FLUX FROM  C
C            TOA TO THE MODEL TOP LEVEL; FOR LONGWAVE: THE OPTICAL     C
C            / DIFFUSE FACTOR                                          C
C     DP:    HERE DP IS ONLY THE PRESSURE DIFFERENCE, DIFFERENT FROM   C
C            THAT DEFINED IN RADDRIV. SO THERE IS A FACTOR 1.02        C
C     O3:    O3 MASS MIXING RATIO                                      C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     DT0:   TEMPERATURE IN MOON LAYER - 250 K                         C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   ATTEN(ILG), O3(ILG), Q(ILG), CO2(ILG), DP(ILG), DIP(ILG), 
     1       DT(ILG), DT0(ILG), RMU(ILG)
      INTEGER INPT(ILG)
C
      COMMON /BANDL1GH/  GW1GH(5),  CL1CO2GH(5,28,5)
      COMMON /BANDL3GH/  GW3GH(3),  CL3H2OGH(5,28,3), CL3CSGH(5,4), 
     1                              CL3CFGH(5,4)
      COMMON /BANDL5GH/  GW5GH(4),  CL5H2OGH(5,28,4), CL5O3GH(5,28,4),
     1                              CL5CSGH(5,4,2), CL5CFGH(5,4,2)
      COMMON /BANDL7GH/  GW7GH(7),  CL7H2OGH(5,28,4), CL7CO2GH(5,28,7),
     1                              CL7O3GH(2)
      COMMON /BANDL8GH/  GW8GH(3),  CL8H2OGH(5,28,3)
      COMMON /BANDL9GH/  GW9GH(6),  CL9H2OGH(5,28,6)
C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY.
C
      DATA NTL /28/
C=======================================================================
      IF (IB .EQ. 1)                                                THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL1CO2GH(1,1,IG), CO2, DP, DIP, DT, DT0, 
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 2)                                           THEN
        DO 200 I = IL1, IL2
          ATTEN(I) =  0.0  
  200   CONTINUE
C
      ELSE IF (IB .EQ. 3)                                           THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL3H2OGH(1,1,IG), Q, DP, DIP, DT, DT0, 
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
        DO 400 I = IL1, IL2
          ATTEN(I) =  0.0
  400   CONTINUE
C
      ELSE IF (IB .EQ. 5)                                           THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL5O3GH(1,1,IG), O3, DP, DIP, DT, DT0, 
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 7)                                           THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL7CO2GH(1,1,IG), CO2, DP, DIP, DT, DT0, 
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 8)                                           THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL8H2OGH(1,1,IG), Q, DP, DIP, DT, DT0, 
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 9)                                           THEN
        ISL = 2
        CALL ATTENUE5(ATTEN, CL9H2OGH(1,1,IG), Q, DP, DIP, DT, DT0,
     1                RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ENDIF
C
C     * SET THE RADIATIVE EFFECT OF THE MOON LAYER TO ZERO.
C
      DO I = IL1, IL2
        ATTEN(I) =  0.0
      END DO
C
      RETURN
      END
