      SUBROUTINE GASOPTS5 (TAUG, GW, DP, IB, IG, O3, Q, CO2, CH4, O2, 
     1                     INPT, MCONT, DIP, DT, RMU3, LEV1, GH,
     2                     IL1, IL2, ILG, LAY)
C
C     * MAY 01,2012 - J.LI.     NEW VERSION FOR GCM16:
C     *                         - INCLUDE WATER VAPOUR CONTINUUM 
C     *                           (BANDS 2-4).
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION GASOPTS4 FOR GCM15H/I:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS TLINE{1,2,3}Z INSTEAD OF 
C     *                           TLINE{1,2,3}Y.
C     * APR 18,2008 - M.LAZARE/ PREVIOUS VERSION GASOPTS3 FOR GCM15G:
C     *               L.SOLHEIM/- COSMETIC CHANGE TO ADD THREADPRIVATE
C     *               J.LI.       FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                         - CALLS TLINE{1,2,3}Y INSTEAD OF 
C     *                           TLINE{1,2,3}X.
C     *                         - USING TEMPERTURE DEPENDENT O3 ABSORPTION
C     *                           DATA, ADDING CH4 IN SOLAR RANGE, USING 
C     *                           KURUZ SOLAR FUNCTION.
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION GASOPTS2 FOR GCM15E/F:
C     *                         - PASS INTEGER VARIABLES "INIT" AND
C     *                           "MIT" INSTEAD OF ACTUAL INTEGER
C     *                           VALUES, TO "TLINE_" ROUTINES.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION GASOPTS FOR GCM15D.
C----------------------------------------------------------------------C
C     CALCULATION OF THE OPTICAL DEPTHS DUE TO NONGRAY GASEOUS         C
C     ABSORPTION FOR THE SOLAR, IN EACH LAYER FOR A GIVEN BAND IB AND  C
C     CUMULATIVE PROBABILITY GW.                                       C
C     RELATIVE SOLAR ENERGY IN EACH SOLAR BAND ARE                     C
C     BAND 1:   622.8483                                               C
C     BAND 1GH:   7.5917                                               C     
C     BAND 2:   430.0919                                               C
C     BAND 2GH:   8.9036                                               C
C     BAND 3:   238.6979                                               C
C     BAND 3GH:   7.4453                                               C
C     BAND 4:    33.4129                                               C
C     BAND 4GH:   7.0384                                               C
C                                                                      C
C     TOTAL RELATIVE SOLAR ENERGY IN FROM 0.2 - 4 UM IS                C
C     1356.0300 W / M2, PLUS 11.9096 W / M2 IN 4 - 10 UM.            C
C     TOTAL  1367.9396 W / M2                                         C 
C                                                                      C
C     THIS SUBROUTINE ONLY CALCULATES TAUG BELOW 1 MB                  C
C                                                                      C
C     TAUG:  GASEOUS OPTICAL DEPTH                                     C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     O3:    O3 MASS MIXING RATIO                                      C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     CO2,   CH4, O2 ALSO ARE MASS MIXING RATIOS                       C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C     RMU3:  A FACTOR OF SOLAR ZENITH ANGLE, GIVEN IN RADDRIV          C
C     MCONT: THE HIGHEST LEVEL FOR WATER VAPOR CONTINUUM CALCULATION   C
C----------------------------------------------------------------------C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, PARAMETER :: R_ZERO=0.0
C
      REAL TAUG(ILG,LAY)
C
      REAL DP(ILG,LAY), O3(ILG,LAY), Q(ILG,LAY), CO2(ILG,LAY), 
     1     CH4(ILG,LAY), O2(ILG,LAY), DIP(ILG,LAY), DT(ILG,LAY), 
     2     RMU3(ILG), S(ILG,LAY)
      INTEGER INPT(ILG,LAY)
      LOGICAL GH
C
      COMMON /BANDS1/ GW1(6), CS1O3(3,6), CS1O21
      COMMON /BANDS2/ GW2(4), CS2H2O(5,18,4), CS2O2(5,18,2), CS2O3(4),
     1                        CS2CS(5,5,4),   CS2CF(5,5,4)
      COMMON /BANDS3/ GW3(6), CS3H2O(5,18,6), CS3CO2(5,18,6), 
     1                        CS3CH4(5,18,2), CS3CS(5,5,6), CS3CF(5,5,6)
      COMMON /BANDS4/ GW4(4), CS4H2O(5,18,4), CS4CO2(5,18,4),
     1                        CS4CH4(5,18,2), CS4CS(5,5,4), CS4CF(5,5,4)

C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY ("M" REFERENCES NON-SATURATED BANDS ACTIVE BELOW 1 MB ONLY).
C
      DATA MTL /18/
C=======================================================================
      IF (IB .EQ. 1)                                                THEN
C
C----------------------------------------------------------------------C
C     BAND (14500 - 50000 CM-1), NONGRAY GASEOUS ABSORPTION OF O3,    C
C     H2O AND O2.                                                      C
C     RELATIVE SOLAR ENERGY 630.4401 WM-2.                            C
C     IG9 (50000-43000)  UVC                           1.21100 (W M-2)C
C     IG8 (43000-37500)  UVC                           3.17570         C
C     IG7 (37500-35700)  UVC                           3.20501         C
C     UVC ALL INCLUDED IN GH PART                                      C
C                                                                      C
C     IG6 (35700-34200)  UVB                           4.73084         C
C     IG5 (34200-32185)  UVB                          10.14919         C
C     IG4 (32185-31250)  UVB  J VALUE: 32185 CM-1     6.70594         C
C                                                                      C
C     IG3 (31250-25000)  UVA                          83.43346         C
C                                                                      C
C     IG2 (25000-19000)  PAR                         236.97212         C
C     IG1 (19000-14500)  PAR                         280.85678         C
C     PAR: PHOTOSYNTHETIC ACTIVE RADIATION                             C
C     NOTE THE SPECTRAL STRUCTURE IS SLIGHTLY DIFF FROM LI & BARKER    C
C     (2005 JAS)                                                       C
C                                                                      C
C     THE EFFECT OF H2O AND O2 IS ADDED WITH SIMPLE METHOD             C
C----------------------------------------------------------------------C
C
        IF(IG .EQ. 1)                                               THEN
          DO 110 K = LEV1, LAY
          DO 110 I = IL1, IL2
            IF (INPT(1,K) .LT. 950)                                 THEN
              M =  INPT(I,K)
            ELSE 
              M =  INPT(I,K) - 1000
            ENDIF
C
            IF (M .LT. 7)                                           THEN
              X       = (CS1O21 - 0.881E-05 * RMU3(I)) * O2(I,K)
            ELSE
              X       = (0.108E-04 - 0.881E-05 * RMU3(I)) * O2(I,K)
            ENDIF
C
            IF (M .LT. 15)                                          THEN
              X       =  X + (0.199E-02 - 0.952E-03 * RMU3(I)) * Q(I,K) 
            ELSE
              X       =  X + (0.208E-02 - 0.952E-03 * RMU3(I)) * Q(I,K)
            ENDIF
C
            DTO3      =  DT(I,K) + 23.13
            TAUG(I,K) = ((CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                   DTO3 * CS1O3(3,IG))) * O3(I,K) + X) * DP(I,K)
  110     CONTINUE
        ELSE
          DO 120 K = LEV1, LAY
          DO 120 I = IL1, IL2
            DTO3      =  DT(I,K) + 23.13
            TAUG(I,K) = (CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                   DTO3 * CS1O3(3,IG))) * O3(I,K) * DP(I,K)
  120     CONTINUE
        ENDIF
C
        GW =  GW1(IG)
C
      ELSE IF (IB .EQ. 2)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (8400 - 14500 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O,    C
C     O2 AND O3                                                        C
C     RELATIVE SOLAR ENERGY 430.0919 W M-2                            C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 2)                                              THEN
          CALL TLINE2Z (TAUG, CS2H2O(1,1,IG), CS2O2(1,1,IG), Q, O2, 
     1                  DP, DIP, DT, INPT, LEV1, GH, MTL, 
     2                  IL1, IL2, ILG, LAY)
        ELSE 
          INIT = 2
          CALL TLINE1Z (TAUG, CS2H2O(1,1,IG), Q, DP, DIP, DT, INPT,
     1                  LEV1, GH, MTL, INIT, IL1, IL2, ILG, LAY)
        ENDIF
C
C----------------------------------------------------------------------C
C     SIMPLY ADD O3 EFFECT                                             C
C----------------------------------------------------------------------C
C
        DO 200 K = LEV1, LAY
        DO 200 I = IL1, IL2
          TAUG(I,K)   =  TAUG(I,K) + CS2O3(IG) * O3(I,K) * DP(I,K)
  200   CONTINUE
C
C----------------------------------------------------------------------C
C     WATER VAPOUR CONTINUUM                                           C
C----------------------------------------------------------------------C
C
        LC =  5
        CALL TCONTL1 (TAUG, CS2CS(1,1,IG), CS2CF(1,1,IG), Q, DP, DIP, 
     1                DT, LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
        GW =  GW2(IG)
C
      ELSE IF (IB .EQ. 3)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (4200 - 8400 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O, CO2 C
C     AND CH4                                                          C
C     RELATIVE SOLAR ENERGY  238.6979 W M-2                           C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 2)                                              THEN
          CALL TLINE3Z (TAUG, CS3H2O(1,1,IG), CS3CO2(1,1,IG),
     1                  CS3CH4(1,1,IG), Q, CO2, CH4, DP, DIP, DT, INPT,
     2                  LEV1, GH, MTL, IL1, IL2, ILG, LAY)
        ELSE
          CALL TLINE2Z (TAUG, CS3H2O(1,1,IG), CS3CO2(1,1,IG), Q, CO2, 
     1                  DP, DIP, DT, INPT, LEV1, GH, MTL, 
     2                  IL1, IL2, ILG, LAY)
        ENDIF
C
C----------------------------------------------------------------------C
C     WATER VAPOUR CONTINUUM                                           C
C----------------------------------------------------------------------C
C
        LC =  5
        CALL TCONTL1 (TAUG, CS3CS(1,1,IG), CS3CF(1,1,IG), Q, DP, DIP,
     1                DT, LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
        GW =  GW3(IG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (2500 - 4200 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O      C
C     AND CO2                                                          C
C     RELATIVE SOLAR ENERGY 33.4129 W M-2                             C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 2)                                              THEN
          CALL TLINE3Z (TAUG, CS4H2O(1,1,IG), CS4CO2(1,1,IG),
     1                  CS4CH4(1,1,IG), Q, CO2, CH4, DP, DIP, DT, INPT,
     2                  LEV1, GH, MTL, IL1, IL2, ILG, LAY)
        ELSE
          CALL TLINE2Z (TAUG, CS4H2O(1,1,IG), CS4CO2(1,1,IG), Q, CO2, 
     1                  DP, DIP, DT, INPT, LEV1, GH, MTL, 
     2                  IL1, IL2, ILG, LAY)
        ENDIF
C
C----------------------------------------------------------------------C
C     WATER VAPOUR CONTINUUM                                           C
C----------------------------------------------------------------------C
C
        LC =  5
        CALL TCONTL1 (TAUG, CS4CS(1,1,IG), CS4CF(1,1,IG), Q, DP, DIP,
     1                DT, LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
C
        GW =  GW4(IG)
C
      ENDIF
C
C CHECK TO VERIFY THAT TAUG IS GREATER THAN 0.0
      
      DO K = LEV1, LAY
         DO I = IL1, IL2
            IF (TAUG(I,K) < R_ZERO) TAUG(I,K) = R_ZERO
         END DO ! I
      END DO ! I

      RETURN
      END
