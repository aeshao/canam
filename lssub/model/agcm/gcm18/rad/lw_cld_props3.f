      SUBROUTINE LW_CLD_PROPS3(CLDG, TAUCI, OMCI, GCI, F2,             ! OUTPUT
     1                         DZ, CLD, CLW, CIC, RADEQVW, RADEQVI,    ! INPUT
     2                         CUT, UU3, IL1, IL2, ILG, LAY, IB)       ! CONTROL
C
C     * FEB 10/2015 - J.LI.     New version for gcm18+:
C     *                         - Revised ice cloud optical properties
C     *                           from Yang et al (2012).
C     * FEB 10/2009 - M.LAZARE. PREVIOUS VERSION LW_CLD_PROPS2 FOR GCM15H:
C     *                         - CALCULATE OPTICAL PROPERTIES REGARDLESS
C     *                           OF CLOUD LAYER WATER CONTENT, IT NO
C     *                           CUTOFF TO ZERO BELOW CERTAIN THRESHOLD.
C     * OCT 22/2007 - J.COLE/   PREVIOUS VERSION LW_CLD_PROPS FOR GCM15G:
C     *               M.LAZARE. COMPUTE CLOUD L/W OPTICAL PROPERTIES
C     *                         REQUIRED FOR RADIATION. IT IS COMPOSED
C     *                         OF THREE PARTS:
C     *                          1. ASSIGNS "CLD" TO "CLDG". 
C     *                          2. CALCULATION OF {TAUCL,OMCL,GCL}
C     *                             MIGRATED FROM CLOUDS15 ROUTINES IN
C     *                             PHYSICS (SEPARATED INTO S/W AND L/W
C     *                             SEPARATE COMPONENTS).
C     *                          3. CALCULATION OF RESULTING OPTICAL
C     *                             PROPERTIES ARRAYS REQUIRED FOR
C     *                             RADIATION.
C     *                         NOTE THAT THIS MEANS {TAUCL,OMCL,GCL}
C     *                         MERELY BECOME SCALARS.
C
      IMPLICIT NONE
C
      INTEGER NBS, NBL
      PARAMETER (NBS = 4, NBL = 9)
C
C     * OUTPUT DATA.
C
      REAL, INTENT(OUT) ::
     1 CLDG (ILG,LAY),
     2 TAUCI(ILG,LAY), 
     3 OMCI (ILG,LAY), 
     4 GCI  (ILG,LAY),
     5 F2   (ILG,LAY)
C
C     * INPUT DATA.
C
      REAL, INTENT(IN) ::
     1 DZ     (ILG,LAY),
     2 CLD    (ILG,LAY),
     3 CLW    (ILG,LAY),  ! IN G/M^3
     4 CIC    (ILG,LAY),  ! IN G/M^3
     5 RADEQVW(ILG,LAY),
     6 RADEQVI(ILG,LAY)

      REAL, INTENT(IN) :: CUT, UU3

      INTEGER, INTENT(IN) :: IL1, IL2, ILG, LAY, IB
C
C     * LOCAL DATA (SCALAR) 
C
      REAL  AWS, BWS, CWS, AWL, BWL, CWL, AIS, BIS, CIS, AIL, BIL, CIL
      REAL  WCDW, WCDI, WCLW, WCLI, REW1, REW2, REW3, DG, DG2, DG3, 
     1      TAULW, OMLW, GLW, TAULI, OMLI, GLI, TAUCL, OMCL, GCL, Y1, Y2
      REAL ABSLI
      INTEGER I, K
C
      COMMON / WATOP / AWS(4,NBS), BWS(4,NBS), CWS(4,NBS),
     1                 AWL(5,NBL), BWL(4,NBL), CWL(4,NBL)
      COMMON / ICEOP / AIS(3,NBS), BIS(4,NBS), CIS(4,NBS),
     1                 AIL(3,NBL), BIL(4,NBL), CIL(4,NBL)
C----------------------------------------------------------------------C
C     CLOUD RADIATIVE PROPERTIES FOR RADIATION.                        C
C     TAUCS, OMCS, GCS (TAUCL, OMCL, GCL): OPTICAL DEPTH, SINGLE       C
C     SCATTERING ALBEDO, ASYMMETRY FACTOR FOR SOLAR (INFRARED).        C
C     RADEQVW: EFFECTIVE RADIUS(IN MICROMETER) FOR WATER CLOUD         C
C     RADEQVI: EFFECTIVE RADIUS(IN MICROMETER) FOR ICE CLOUD           C
C     DG: GEOMETRY LENGTH FOR ICE CLOUD                                C
C     WCDW (WCDI): LIQUID WATER (ICE) CONTENT (IN GRAM / M^3)          C
C     WCLW (WCLI): LIQUID WATER (ICE) PATH LENGTH (IN GRAM / M^2)      C
C     CCLD: CLOUD FRACTION                                             C
C     PARAMETERIZATION FOR WATER CLOUD:                                C
C     DOBBIE, ETC. 1999, JGR, 104, 2067-2079                           C
C     LINDNER, T. H. AND J. LI., 2000, J. CLIM., 13, 1797-1805.        C
C     PARAMETERIZATION FOR ICE CLOUD:                                  C
C     ICE CLOUD OPTICAL PROPERTY BASED ON Yang et al. (2012) JAS       C
C     AND A SERIRS PAPERS BY GROUP OF U A&M AND U WISCONSIN            C
C     DG IS THE EFFECTIVE DIAMETER                                     C
 
C----------------------------------------------------------------------C
C
      DO K = 1, LAY
      DO I = IL1, IL2
         CLDG(I,K) = CLD(I,K)
         IF (CLD(I,K) .LE. CUT)                                     THEN
            TAUCI(I,K)          =  0.0 
            OMCI(I,K)           =  0.0 
            GCI(I,K)            =  0.0 
            F2(I,K)             =  0.0 
         ELSE
            WCDW = CLW(I,K)
            WCDI = CIC(I,K)
            WCLW = WCDW*DZ(I,K)
            WCLI = WCDI*DZ(I,K)
            REW1 = RADEQVW(I,K)
            REW2 = REW1 * REW1
            REW3 = REW2 * REW1
            DG   = 2.0 * MIN (MAX (RADEQVI(I,K), 5.0), 60.0)
            DG2  = DG  * DG
            DG3  = DG2 * DG
C     
            TAULW =  WCLW * (AWL(1,IB) + AWL(2,IB) * REW1 + 
     1               AWL(3,IB) / REW1 + AWL(4,IB) / REW2  +
     2               AWL(5,IB) / REW3)
            OMLW  =  1.0 - (BWL(1,IB) + BWL(2,IB) / REW1 + 
     1               BWL(3,IB) * REW1 + BWL(4,IB) * REW2)
            GLW   =  CWL(1,IB) + CWL(2,IB) / REW1 + 
     1               CWL(3,IB) * REW1 + CWL(4,IB) * REW2
C
C----------------------------------------------------------------------C
C     SINCE IN YANG, IT IS A PARAM FOR ABSORPTANCE DEPTH
C----------------------------------------------------------------------C
C
            ABSLI =  AIL(1,IB) + AIL(2,IB) / DG + AIL(3,IB) / DG2 
            OMLI  =  1.0 - (BIL(1,IB) + BIL(2,IB) * DG + 
     1               BIL(3,IB) * DG2 + BIL(4,IB) * DG3) 
            TAULI =  WCLI * (ABSLI / (1.0 - OMLI))
            GLI   =  CIL(1,IB) + CIL(2,IB) * DG + CIL(3,IB) * DG2 + 
     1               CIL(4,IB) * DG3
C
            TAUCL    =  TAULW + TAULI
            IF (TAUCL .GT. 0.)                             THEN
               Y1    =  OMLW * TAULW 
               Y2    =  OMLI * TAULI
               OMCL  = (Y1 + Y2) / TAUCL
               GCL   = (GLW * Y1 + GLI * Y2) / (Y1 + Y2)
            ELSE
                OMCL =  0.
                GCL  =  0.
            ENDIF
C
            TAUCI(I,K) =  TAUCL
            OMCI(I,K)  =  OMCL * TAUCI(I,K)
            F2(I,K)    =  GCL * GCL
            GCI(I,K)   = (GCL - F2(I,K)) / 
     1                    (1.0 - F2(I,K))
            GCI(I,K)   =  - 0.5 * (1.0 - UU3 
     1                    * GCI(I,K))
        ENDIF
      ENDDO
      ENDDO

      RETURN
      END
