      SUBROUTINE AEROMX3OP (EXTA, EXOMA, EXOMGA, FA, ABSA, TAUA055, 
     1                      ABSA055, TAUA086, RHIN, SO4LOAD, BCOLOAD, 
     2                      BCYLOAD, OCOLOAD, OCYLOAD, REAM3, VEAM3, 
     3                      RESO4, VESO4, REBCY, VEBCY, REOCY, VEOCY, 
     4                      REBCO, VEBCO, REOCO, VEOCO, 
     5                      IL1, IL2, ILG, LAY) 
C
C     JULY. 2017, J. LI & K.VON SALZEN
C----------------------------------------------------------------------C
C     INTERNAL MIXING OF SULFATE (NH4)2SO4, BC AND OC AND ALSO CONSIDERC
C     PARIAL INTERNAL MIXING EFFECT, APPLY TO BULK AND PLA             C
C     NEW BC REFRACTIVE INDEX (FOLLOWING BOND) IS USED                 C
C                                                                      C
C     EXTA:    EXTINCTION COEFFICIENT                                  C
C     EXOMA:   EXTINCTION COEFFICIENT TIMES SINGLE SCATTERING ALBEDO   C
C     EXOMGA:  EXOMA TIMES ASYMMETRY FACTOR                            C
C     FA:      SQUARE OF ASYMMETRY FACTOR                              C
C     TAUA055: OPTICAL DEPTH AT 0.55/0.86 UM, 4: TOTAL OF THREE AS     C
C        /086  MIXING + HYDROPHOBIC, 1, 2, 3: PURE SULFATE, BC, OC     C
C     ABSA:    ABSORPTION COEFFICIENT                                  C
C              WITH DP FACTOR                                          C
C     RHIN:    INPUT RELATIVE HUMIDITY                                 C
C     SO4LOAD, BCOLOAD, BCYLOAD, OCOLOAD, OCYLOAD,                     C
C              Y/O: HYDROPHYLIC/HYDROPHOBIC                            C
C     RE:      EFFECTIVE RADIUS, LIKE RESO4 IS THE EFFECTIVE R FOR SO4 C
C     VE:      EFFECTIVE VARIANCE                                      C
C     REAM3/VEAM3:   EFFECTIVE RADIUS FOR THE MIXED THREE              C
C     RH:      RELATIVE HUMIDITY                                       C
C                                                                      C
C     SEXT:    AEROMX3S(1:17,IREC), 1 THE GROWTH FACTOR, 2-5 EXT FOR 4 C
C              SOLAR BANDS, 6-7 FOR 0.55 AND 0.86 UM, 8-11 SINGLE SCATTC
C              ALBEDO FOR 4 BANDS, 12 FOR 0.55 UM, 13-16 G FOR 4 BANDS C
C              17 G FOR 0.55 UM                                        C
C     LABS:    FIRST 9 ARE RESULTS FOR 9 LW BANDS                      C
C----------------------------------------------------------------------C
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (NBS = 4, NBL = 9, NH = 6, NR = 4, NV = 3, NF1 = 6,
     &           NF2 = 6)
      REAL, DIMENSION(ILG,LAY,NBS):: EXTA, EXOMA, EXOMGA, FA
      REAL, DIMENSION(ILG,LAY,NBL):: ABSA
      REAL, DIMENSION(NBS,6):: SEXTA, SOMGA, SGA
      REAL, DIMENSION(ILG,LAY):: RHIN, SO4LOAD, BCOLOAD, BCYLOAD,
     &                           OCOLOAD, OCYLOAD, REAM3, VEAM3, RESO4,
     &                           VESO4, REBCY, VEBCY, REOCY, VEOCY,
     &                           REBCO, VEBCO, REOCO, VEOCO
      REAL, DIMENSION(ILG,LAY,4):: TAUA055, ABSA055, TAUA086
      REAL  RHNODE(NH), RENODE(NR), VENODE(NV), FR1NODE(NF1),
     &      FR2NODE(NF2), SEXT(6), SOMG(5), SG(5), LABS(9), SEXTA055(6),
     &      SABSA055(6), SEXTA086(6), LABSA(NBL,6), SS(17)
      REAL, DIMENSION(2):: TH, TR, TV, TF1, TF2
      REAL :: REA1
C
      DATA RHNODE  /0.1, 0.6, 0.8, 0.9, 0.94, 0.98/
      DATA RENODE  /0.06, 0.14, 0.22, 0.3/
      DATA VENODE  /0.6, 1.5, 2.4/
      DATA FR1NODE /1.0, 0.8, 0.6, 0.4, 0.2, 0.0/
      DATA FR2NODE /0.0, 0.05, 0.1, 0.3, 0.7, 1.0/
C
      COMMON /AMIX3OPT/AEROMX3S(17,2592), AEROMX3L(9,2592)
C
C----------------------------------------------------------------------C
C     FACTOR 10000, BECAUSE THE UNIT OF SPECIFIC EXTINCTION FOR AEROSOLC
C     IS M^2/GRAM, WHILE FOR GAS IS CM^2/GRAM, IN RADDRIV THE SAME DP  C
C     (AIR DENSITY * LAYER THICKNESS) IS USED FOR BOTH GAS AND AEROSOL.C
C     ALOAD IS DRY LOADING IN UNIT G (AEROSOL) / G(AIR).               C
C----------------------------------------------------------------------C
C
      YTINY = 10. * TINY(REA1)
C
C     FRACSO4, FRACBCY, FRACOCY DETERMINE HOW MANY PERCENT (NH)4SO4, 
C     BCY, OCY JOIN THE MIXING, AS SO4M, BCYM, OCYM, AND HOW MANY ARE
C     INTERNAL MIXING, AS SO4N, BCYN, OCYN
C
      FRACSO4  = 1.
      FRACBCY  = 1.
      FRACOCY  = 1.
      DO 200 K = 1, LAY
      DO 200 I = IL1, IL2
        RH               =  MAX(MIN (RHIN(I,K), 0.98), 0.1)
        SO4M             =  10000. * SO4LOAD(I,K) * FRACSO4
        SO4N             =  10000. * SO4LOAD(I,K) * (1.0 - FRACSO4)
        BCO              =  10000. * BCOLOAD(I,K)
        BCYM             =  10000. * BCYLOAD(I,K) * FRACBCY
        BCYN             =  10000. * BCYLOAD(I,K) * (1.0 - FRACBCY)
        OCO              =  10000. * OCOLOAD(I,K)
        OCYM             =  10000. * OCYLOAD(I,K) * FRACOCY
        OCYN             =  10000. * OCYLOAD(I,K) * (1.0 - FRACOCY)
C
        SEXTA(:,:)       =  0.0
        SOMGA(:,:)       =  0.0
        SGA(:,:)         =  0.0
        SEXTA055(:)      =  0.0
        SABSA055(:)      =  0.0
        SEXTA086(:)      =  0.0
        LABSA(:,:)       =  0.0
C
        SBO              =  SO4M + BCYM + OCYM
        TOT              =  SBO + SO4N + BCYN  + OCYN + BCO  + OCO
        IF (TOT .GT. 5.E-09)                                        THEN
C 
        RE               =  MAX(MIN (REAM3(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VEAM3(I,K), 2.4), 0.6)
C
        IH               =  MVIDX(RHNODE, NH, RH)
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TH(2)            = (RH - RHNODE(IH)) /
     1                                       (RHNODE(IH+1) - RHNODE(IH))
        TH(1)            =  1.0 - TH(2)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
C
        IF (SBO .GT. YTINY)                                         THEN 
          FR1            =  MAX(MIN (SO4M / SBO, 1.0), 0.0)
          FR2            =  MAX(MIN (BCYM / SBO, 1.0), 0.0)
        ELSE
          FR1            =  0.0
          FR2            =  0.0
        ENDIF
        IF1              =  MVIDX(FR1NODE, NF1, FR1)
        IF2              =  MVIDX(FR2NODE, NF2, FR2)
C
        TF1(2)           = (FR1 - FR1NODE(IF1)) /
     1                                   (FR1NODE(IF1+1) - FR1NODE(IF1))
        TF1(1)           =  1.0 - TF1(2)
        TF2(2)           = (FR2 - FR2NODE(IF2)) /
     1                                   (FR2NODE(IF2+1) - FR2NODE(IF2))
        TF2(1)           =  1.0 - TF2(2)
C
        DO 101 IHH       =  IH, IH + 1
        DO 101 IRR       =  IR, IR + 1
        DO 101 IVV       =  IV, IV + 1
        DO 101 IFF1      =  IF1, IF1 + 1
        DO 101 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
          DO J  = 1, NBS
            SEXTA(J,1)   =  SEXTA(J,1) + SEXT(J) * SS(1) * WTT
            SOMGA(J,1)   =  SOMGA(J,1) + SOMG(J) * WTT
            SGA(J,1)     =  SGA(J,1) + SG(J) * WTT
          ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(1)  =  SEXTA055(1) + FACT
            SABSA055(1)  =  SABSA055(1) + FACT * (1.0 - SOMG(5))
            SEXTA086(1)  =  SEXTA086(1) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,1) =  LABSA(1:9,1) + LABS(1:9) * SS(1) * WTT
  101   CONTINUE
C
C PURE SOLFATE, THE EXTERNAL MIXING PART, HYDROSCOPIC
C
        IF (SO4M .GT. 1.E-10 .OR. SO4N .GT. 1.E-10)                 THEN
        RE               =  MAX(MIN (RESO4(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VESO4(I,K), 2.4), 0.6)
C
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
C
        TF1(1)           =  1.0
        TF1(2)           =  0.0
        TF2(1)           =  1.0
        TF2(2)           =  0.0
        IF1              =  1
        IF2              =  1
C
        DO 102 IHH       =  IH, IH + 1
        DO 102 IRR       =  IR, IR + 1
        DO 102 IVV       =  IV, IV + 1
        DO 102 IFF1      =  IF1, IF1 + 1
        DO 102 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
          DO J  = 1, NBS
            SEXTA(J,2)   =  SEXTA(J,2) + SEXT(J) * SS(1) * WTT
            SOMGA(J,2)   =  SOMGA(J,2) + SOMG(J) * WTT
            SGA(J,2)     =  SGA(J,2) + SG(J) * WTT
          ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(2)  =  SEXTA055(2) + FACT
            SABSA055(2)  =  SABSA055(2) + FACT * (1.0 - SOMG(5))
            SEXTA086(2)  =  SEXTA086(2) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,2) =  LABSA(1:9,2) + LABS(1:9) * SS(1) * WTT
  102   CONTINUE
        ENDIF
C
C PURE BCY EXTERNAL MIXING, HYDROSCPIC
C
        IF (BCYM .GT. 1.E-10 .OR. BCYN .GT. 1.E-10)                 THEN
        RE               =  MAX(MIN (REBCY(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VEBCY(I,K), 2.4), 0.6)
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
        TF1(1)           =  0.0
        TF1(2)           =  1.0
        TF2(1)           =  0.0
        TF2(2)           =  1.0
        IF1              =  5
        IF2              =  5
C
        DO 103 IHH       =  IH, IH + 1
        DO 103 IRR       =  IR, IR + 1
        DO 103 IVV       =  IV, IV + 1
        DO 103 IFF1      =  IF1, IF1 + 1
        DO 103 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
          DO J  = 1, NBS
            SEXTA(J,3)   =  SEXTA(J,3) + SEXT(J) * SS(1) * WTT
            SOMGA(J,3)   =  SOMGA(J,3) + SOMG(J) * WTT
            SGA(J,3)     =  SGA(J,3) + SG(J) * WTT
           ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(3)  =  SEXTA055(3) + FACT
            SABSA055(3)  =  SABSA055(3) + FACT * (1.0 - SOMG(5))
            SEXTA086(3)  =  SEXTA086(3) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,3) =  LABSA(1:9,3) + LABS(1:9) * SS(1) * WTT
  103   CONTINUE
        ENDIF
C
C PURE BCO EXTERNAL MIXING, NON-HYDROSCPIC
C
        IF (BCO .GT. 1.E-10)                                        THEN
        RE               =  MAX(MIN (REBCO(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VEBCO(I,K), 2.4), 0.6)
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
        TH(1)            =  1.0
        TH(2)            =  0.0
        IH               =  1
        TF1(1)           =  0.0
        TF1(2)           =  1.0
        TF2(1)           =  0.0
        TF2(2)           =  1.0
        IF1              =  5
        IF2              =  5
C
        DO 104 IHH       =  IH, IH + 1
        DO 104 IRR       =  IR, IR + 1
        DO 104 IVV       =  IV, IV + 1
        DO 104 IFF1      =  IF1, IF1 + 1
        DO 104 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
          DO J  = 1, NBS
            SEXTA(J,4)   =  SEXTA(J,4) + SEXT(J) * SS(1) * WTT
            SOMGA(J,4)   =  SOMGA(J,4) + SOMG(J) * WTT
            SGA(J,4)     =  SGA(J,4) + SG(J) * WTT
           ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(4)  =  SEXTA055(4) + FACT
            SABSA055(4)  =  SABSA055(4) + FACT * (1.0 - SOMG(5))
            SEXTA086(4)  =  SEXTA086(4) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,4) =  LABSA(1:9,4) + LABS(1:9) * SS(1) * WTT
  104   CONTINUE
        ENDIF
C
C PURE OCY EXTERNAL MIXING, HYDROSCPIC
C
        IF (OCYM .GT. 1.E-10 .OR. OCYN .GT. 1.E-10)                 THEN
        RE               =  MAX(MIN (REOCY(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VEOCY(I,K), 2.4), 0.6)
        IH               =  MVIDX(RHNODE, NH, RH)
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TH(2)            = (RH - RHNODE(IH)) /
     1                                       (RHNODE(IH+1) - RHNODE(IH))
        TH(1)            =  1.0 - TH(2)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
        TF1(1)           =  0.0
        TF1(2)           =  1.0
        TF2(1)           =  1.0
        TF2(2)           =  0.0
        IF1              =  5
        IF2              =  1
C
        DO 105 IHH       =  IH, IH + 1
        DO 105 IRR       =  IR, IR + 1
        DO 105 IVV       =  IV, IV + 1
        DO 105 IFF1      =  IF1, IF1 + 1
        DO 105 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
C
          DO J  = 1, NBS
            SEXTA(J,5)   =  SEXTA(J,5) + SEXT(J) * SS(1) * WTT
            SOMGA(J,5)   =  SOMGA(J,5) + SOMG(J) * WTT
            SGA(J,5)     =  SGA(J,5) + SG(J) * WTT
          ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(5)  =  SEXTA055(5) + FACT
            SABSA055(5)  =  SABSA055(5) + FACT * (1.0 - SOMG(5))
            SEXTA086(5)  =  SEXTA086(5) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,5) =  LABSA(1:9,5) + LABS(1:9) * SS(1) * WTT
  105   CONTINUE
        ENDIF
C
C PURE OCO EXTERNAL MIXING, NON-HYDROSCPIC
C
        IF (OCO .GT. 1.E-10)                                        THEN
        RE               =  MAX(MIN (REOCY(I,K), 0.3), 0.06)
        VE               =  MAX(MIN (VEOCY(I,K), 2.4), 0.6)
        IR               =  MVIDX(RENODE, NR, RE)
        IV               =  MVIDX(VENODE, NV, VE)
        TR(2)            = (RE - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)            =  1.0 - TR(2)
        TV(2)            = (VE - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)            =  1.0 - TV(2)
        TH(1)            =  1.0
        TH(2)            =  0.0
        IH               =  1
        TF1(1)           =  0.0
        TF1(2)           =  1.0
        TF2(1)           =  1.0
        TF2(2)           =  0.0
        IF1              =  5
        IF2              =  1
C
        DO 106 IHH       =  IH, IH + 1
        DO 106 IRR       =  IR, IR + 1
        DO 106 IVV       =  IV, IV + 1
        DO 106 IFF1      =  IF1, IF1 + 1
        DO 106 IFF2      =  IF2, IF2 + 1
C
          IREC           = (IHH - 1) * NR * NV * NF1 * NF2 +
     1                     (IRR - 1) * NV * NF1 * NF2 +
     2                     (IVV - 1) * NF1 * NF2 +
     3                     (IFF1 - 1) * NF2 + IFF2
C
          SS(1:17)       =  AEROMX3S(1:17,IREC)
          SEXT(1:6)      =  SS(2:7)
          SOMG(1:5)      =  SS(8:12)
          SG(1:5)        =  SS(13:17)
          LABS(1:9)      =  AEROMX3L(1:9,IREC)
C
          WTT            =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                      TV(IVV - IV + 1) * TF1(IFF1 - IF1 + 1) *
     2                      TF2(IFF2 - IF2 + 1)
C
          DO J  = 1, NBS
            SEXTA(J,6)   =  SEXTA(J,6) + SEXT(J) * SS(1) * WTT
            SOMGA(J,6)   =  SOMGA(J,6) + SOMG(J) * WTT
            SGA(J,6)     =  SGA(J,6) + SG(J) * WTT
          ENDDO
            FACT         =  SEXT(5) * SS(1) * WTT
            SEXTA055(6)  =  SEXTA055(6) + FACT
            SABSA055(6)  =  SABSA055(6) + FACT * (1.0 - SOMG(5))
            SEXTA086(6)  =  SEXTA086(6) + SEXT(6) * SS(1) * WTT
            LABSA(1:9,6) =  LABSA(1:9,6) + LABS(1:9) * SS(1) * WTT
  106   CONTINUE
        ENDIF
C
C----------------------------------------------------------------------C
C     THE RESULTS OF EXTA, EXOMA, EXOMGA, FA, ABSA ARE ACCUMULATED     C
C     WITH DUST & SSALT CALCULATED IN OTHER SUBROUTINES                C
C----------------------------------------------------------------------C
C
        DO J  = 1, NBS
          EXT1           =  SEXTA(J,1) * SBO
          EXT2           =  SEXTA(J,2) * SO4N
          EXT3           =  SEXTA(J,3) * BCYN
          EXT4           =  SEXTA(J,4) * BCO
          EXT5           =  SEXTA(J,5) * OCYN
          EXT6           =  SEXTA(J,6) * OCO
          EXTA(I,K,J)    =  EXTA(I,K,J) + EXT1 + EXT2 + EXT3 +
     1                                    EXT4 + EXT5 + EXT6
          EXTOM1         =  EXT1 * SOMGA(J,1)
          EXTOM2         =  EXT2 * SOMGA(J,2)
          EXTOM3         =  EXT3 * SOMGA(J,3)
          EXTOM4         =  EXT4 * SOMGA(J,4)
          EXTOM5         =  EXT5 * SOMGA(J,5)
          EXTOM6         =  EXT6 * SOMGA(J,6)
          EXOMA(I,K,J)   =  EXOMA(I,K,J) + EXTOM1 + EXTOM2 + EXTOM3 +
     1                                     EXTOM4 + EXTOM5 + EXTOM6
          EXTOMG1        =  EXTOM1 * SGA(J,1)
          EXTOMG2        =  EXTOM2 * SGA(J,2)
          EXTOMG3        =  EXTOM3 * SGA(J,3)
          EXTOMG4        =  EXTOM4 * SGA(J,4)
          EXTOMG5        =  EXTOM5 * SGA(J,5)
          EXTOMG6        =  EXTOM6 * SGA(J,6)
          EXOMGA(I,K,J)  =  EXOMGA(I,K,J) + EXTOMG1 + EXTOMG2 +
     1                            EXTOMG3 + EXTOMG4 + EXTOMG5 + EXTOMG6
C
          FA(I,K,J)      =  FA(I,K,J) + EXTOMG1 * SGA(J,1) +
     1                                  EXTOMG2 * SGA(J,2) +
     2                                  EXTOMG3 * SGA(J,3) +
     3                                  EXTOMG4 * SGA(J,4) +
     4                                  EXTOMG5 * SGA(J,5) +
     5                                  EXTOMG6 * SGA(J,6)
        ENDDO
C
C----------------------------------------------------------------------C
C     4: THE TOTAL MIXED THREE; 1: SULFATE; 2: BC; 3: OC               C
C----------------------------------------------------------------------C
C
          XM             =  SEXTA055(1) * SBO
          XS             =  SEXTA055(2) * SO4M
          XB             =  SEXTA055(3) * BCYM
          XO             =  SEXTA055(5) * OCYM
          TAUA055(I,K,1) =  SEXTA055(2) * SO4N + (XM - XB - XO)
          TAUA055(I,K,2) =  SEXTA055(3) * BCYN + SEXTA055(4) * BCO +
     1                     (XM - XS - XO)
          TAUA055(I,K,3) =  SEXTA055(5) * OCYN + SEXTA055(6) * OCO +
     1                     (XM - XS - XB)
          TAUA055(I,K,4) =  TAUA055(I,K,1) + TAUA055(I,K,2) + 
     1                      TAUA055(I,K,3)
C
          XM             =  SABSA055(1) * SBO
          XS             =  SABSA055(2) * SO4M
          XB             =  SABSA055(3) * BCYM
          XO             =  SABSA055(5) * OCYM
          ABSA055(I,K,1) =  SABSA055(2) * SO4N + (XM - XB - XO)
          ABSA055(I,K,2) =  SABSA055(3) * BCYN + SABSA055(4) * BCO +
     1                     (XM - XS - XO)
          ABSA055(I,K,3) =  SABSA055(5) * OCYN + SABSA055(6) * OCO +
     1                     (XM - XS - XB)
          ABSA055(I,K,4) =  ABSA055(I,K,1) + ABSA055(I,K,2) + 
     1                      ABSA055(I,K,3)
C
          XM             =  SEXTA086(1) * SBO
          XS             =  SEXTA086(2) * SO4M
          XB             =  SEXTA086(3) * BCYM
          XO             =  SEXTA086(5) * OCYM
          TAUA086(I,K,1) =  SEXTA086(2) * SO4N + (XM - XB - XO)
          TAUA086(I,K,2) =  SEXTA086(3) * BCYN + SEXTA086(4) * BCO +
     1                     (XM - XS - XO)
          TAUA086(I,K,3) =  SEXTA086(5) * OCYN + SEXTA086(6) * OCO +
     1                     (XM - XS - XB)
          TAUA086(I,K,4) =  TAUA086(I,K,1) + TAUA086(I,K,2) + 
     1                      TAUA086(I,K,3)
C
        DO J  = 1, NBL
          ABSA(I,K,J)    =  ABSA(I,K,J) + LABSA(J,1) * SBO +
     1                      LABSA(J,2) * SO4N + LABSA(J,3) * BCYN + 
     2                      LABSA(J,4) * BCO  + LABSA(J,5) * OCYN +                                    
     2                      LABSA(J,6) * OCO
        ENDDO
C
        ELSE
          TAUA055(I,K,:) =  0.0
          ABSA055(I,K,:) =  0.0
          TAUA086(I,K,:) =  0.0
        ENDIF
  200 CONTINUE
      RETURN
      END
