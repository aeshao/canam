      SUBROUTINE SW_CLD_PROPS4(CLDG, TAUCSG, TAUOMC, TAUOMGC, F2,   ! OUTPUT
     1                         WRKA, WRKB,                          ! OUTPUT 
     2                         DZ, CLD, CLW, CIC, RADEQVW, RADEQVI, ! INPUT
     3                         TAUA, TAUOMA, TAUOMGA, F1, ANU,      ! INPUT
     4                         CLDT, RMUG, ETA, ISUN,               ! INPUT
     5                         MCICA, CUT, LENGATH,                 ! CONTROL
     6                         IL1, IL2, ILG, LAY, IB)              ! CONTROL
C
C     * FEB 10/2015 - J.LI.     New version for gcm18+:
C     *                         - Revised ice cloud optical properties
C     *                           from Yang et al (2012).
C     * JUN 14/2013 - J. LI.    PREVIOUS VERSION SW_CLD_PROPS3 FOR GCM17+:
C                               - ADD SEMI-DIRECT EFFECT.
C     * FEB 10/2009 - M.LAZARE. PREVIOUS VERSION SW_CLD_PROPS2 FOR GCM15H
C     *                         THROUGH GCM16). 
C     *                         - CALCULATE OPTICAL PROPERTIES REGARDLESS
C     *                           OF CLOUD LAYER WATER CONTENT, IT NO
C     *                           CUTOFF TO ZERO BELOW CERTAIN THRESHOLD.
C     * OCT 22/2007 - J.COLE/   PREVIOUS VERSION SW_CLD_PROPS FOR GCM15G:
C     *               M.LAZARE. COMPUTE CLOUD S/W OPTICAL PROPERTIES
C     *                         REQUIRED FOR RADIATION. IT IS COMPOSED
C     *                         OF THREE PARTS:
C     *                          1. ASSIGNS "CLD" TO "CLDG". 
C     *                          2. CALCULATION OF {TAUCS,OMCS,GCS}
C     *                             MIGRATED FROM CLOUDS15 ROUTINES IN
C     *                             PHYSICS (SEPARATED INTO S/W AND L/W
C     *                             SEPARATE COMPONENTS).
C     *                          3. CALCULATION OF RESULTING OPTICAL
C     *                             PROPERTIES ARRAYS REQUIRED FOR
C     *                             RADIATION.
C     *                          4. CALCULATES WRKA AND WRKB AS INPUT
C     *                             TO ENSUING SURFACE ALBEDO
C     *                             CALCULATION. 
C     *                         NOTE THAT THIS MEANS {TAUCS,OMCS,GCS}
C     *                         MERELY BECOME INTERNAL WORK ARRAYS.         
C
      IMPLICIT NONE
C
      INTEGER NBS, NBL
      PARAMETER (NBS = 4, NBL = 9)
C
C     * OUTPUT DATA.
C
      REAL, INTENT(OUT) ::
     1 CLDG   (ILG,LAY),
     2 TAUCSG (ILG,LAY), 
     3 TAUOMC (ILG,LAY), 
     4 TAUOMGC(ILG,LAY),
     5 F2     (ILG,LAY)

      REAL, INTENT(OUT) ::
     1 WRKA   (ILG),
     2 WRKB   (ILG)  
C
C     * INPUT DATA.
C
      REAL, INTENT(IN) ::
     1 DZ     (ILG,LAY),
     2 CLD    (ILG,LAY),
     3 CLW    (ILG,LAY),  ! IN G/M^3
     4 CIC    (ILG,LAY),  ! IN G/M^3
     5 RADEQVW(ILG,LAY),
     6 RADEQVI(ILG,LAY),
     7 TAUA   (ILG,LAY),
     8 TAUOMA (ILG,LAY),
     9 TAUOMGA(ILG,LAY),
     A F1     (ILG,LAY),
     B ANU    (ILG,LAY),
     C ETA    (ILG,LAY)

      REAL, INTENT(IN) ::
     1 CLDT   (ILG),
     2 RMUG   (ILG)

      INTEGER, INTENT(IN) ::
     1 ISUN   (ILG)

      REAL, INTENT(IN) :: CUT

      INTEGER, INTENT(IN) :: MCICA, LENGATH, IL1, IL2, ILG, LAY, IB
C
C     * LOCAL DATA.
C
      REAL, DIMENSION(ILG,LAY) :: TAUCS, OMCS, GCS
      REAL, DIMENSION(ILG)     :: VI_TAUCS
C
      REAL  AWS, BWS, CWS, AWL, BWL, CWL, AIS, BIS, CIS, AIL, BIL, CIL
      REAL  WCDW, WCDI, WCLW, WCLI, REW1, REW2, REW3, DG, DG2, DG3, 
     1      TAUSW, OMSW, GSW, TAUSI, OMSI, GSI, Y1, Y2
      REAL  OMSM, GSM, SOM, SG, SETA, X1, X2, DLT, ETA_TAB(6)
      REAL  C20, C30, TAUCSI, X, X_CAS, VI_TAUCSM, RMUGFAC, ANUFAC
      INTEGER I, J, K, L, LP1
C
      COMMON / WATOP / AWS(4,NBS), BWS(4,NBS), CWS(4,NBS),
     1                 AWL(5,NBL), BWL(4,NBL), CWL(4,NBL)
      COMMON / ICEOP / AIS(3,NBS), BIS(4,NBS), CIS(4,NBS),
     1                 AIL(3,NBL), BIL(4,NBL), CIL(4,NBL)
      COMMON / CLDSM / SOM(3,NBS,6), SG(3,NBS,6)
C
      DATA ETA_TAB/1.E-08, 1.E-07, 1.E-06, 1.E-05, 1.E-04, 1.E-03/
C----------------------------------------------------------------------C
C     CLOUD RADIATIVE PROPERTIES FOR RADIATION.                        C
C     TAUCS, OMCS, GCS (TAUCL, OMCL, GCL): OPTICAL DEPTH, SINGLE       C
C     SCATTERING ALBEDO, ASYMMETRY FACTOR FOR SOLAR (INFRARED).        C
C     RADEQVW: EFFECTIVE RADIUS(IN MICROMETER) FOR WATER CLOUD         C
C     RADEQVI: EFFECTIVE RADIUS(IN MICROMETER) FOR ICE CLOUD           C
C     DG: GEOMETRY LENGTH FOR ICE CLOUD                                C
C     WCDW (WCDI): LIQUID WATER (ICE) CONTENT (IN GRAM / M^3)          C
C     WCLW (WCLI): LIQUID WATER (ICE) PATH LENGTH (IN GRAM / M^2)      C
C     CCLD: CLOUD FRACTION                                             C
C     PARAMETERIZATION FOR WATER CLOUD:                                C
C     DOBBIE, ETC. 1999, JGR, 104, 2067-2079                           C
C     LINDNER, T. H. AND J. LI., 2000, J. CLIM., 13, 1797-1805.        C
C     PARAMETERIZATION FOR ICE CLOUD:                                  C
C     ICE CLOUD OPTICAL PROPERTY BASED ON Yang et al. (2012) JAS       C
C     AND A SERIRS PAPERS BY GROUP OF U A&M AND U WISCONSIN            C
C     DG IS THE EFFECTIVE DIAMETER                                     C
C----------------------------------------------------------------------C
C
      DO K = 1, LAY
      DO I = IL1, IL2
         IF(CLD(I,K) .LE. CUT)                                      THEN
            TAUCS(I,K) =  0.0
            OMCS (I,K) =  0.0
            GCS  (I,K) =  0.0
         ELSE
            WCDW = CLW(I,K)
            WCDI = CIC(I,K)
            WCLW = WCDW*DZ(I,K)
            WCLI = WCDI*DZ(I,K)

            REW2 =  RADEQVW(I,K) * RADEQVW(I,K)
            REW3 =  REW2 * RADEQVW(I,K)
            DG   =  2.0 * MIN (MAX (RADEQVI(I,K), 5.0), 60.0)
            DG2  =  DG  * DG
            DG3  =  DG2 * DG
C
C----------------------------------------------------------------------C
C     ADD SEMI-DIRECT EFFECT INSIDE CLOUD                              C
C----------------------------------------------------------------------C
C
            IF (ETA(I,K) .GT. 1.E-03 .OR. ETA(I,K) .LT. 1.E-08)     THEN
              OMSM =  0.0
              GSM  =  0.0
            ELSE
              SETA =  LOG10(ETA(I,K)) + 9.
              L    =  INT(SETA)
              LP1  =  L + 1
              X1   =  SOM(1,IB,L) + SOM(2,IB,L) * RADEQVW(I,K) +
     1                SOM(3,IB,L) * REW2
              Y1   =  SG(1,IB,L) + SG(2,IB,L) * RADEQVW(I,K) +
     1                SG(3,IB,L) * REW2
              IF (LP1 .LE. 6)                                       THEN
                X2 =  SOM(1,IB,LP1) + SOM(2,IB,LP1) * RADEQVW(I,K) +
     1                SOM(3,IB,LP1) * REW2
                Y2 =  SG(1,IB,LP1) + SG(2,IB,LP1) * RADEQVW(I,K) +
     1                SG(3,IB,LP1) * REW2
              ELSE
                X2 =  0.
                Y2 =  0.
              ENDIF
              DLT  = (ETA(I,K) - ETA_TAB(L)) / 
     1               (ETA_TAB(LP1) - ETA_TAB(L))
              OMSM =  X1 + DLT * (X2 - X1)
              GSM  =  Y1 + DLT * (Y2 - Y1)
            ENDIF
C
            TAUSW = WCLW * 
     1             (AWS(1,IB) + AWS(2,IB) / RADEQVW(I,K) + 
     2              AWS(3,IB) / REW2 + AWS(4,IB) / REW3)
            OMSW  = 1.0 - (BWS(1,IB) + BWS(2,IB) * RADEQVW(I,K) +
     1              BWS(3,IB) * REW2 + BWS(4,IB) * REW3) + OMSM
            GSW   = CWS(1,IB) + CWS(2,IB) * RADEQVW(I,K) +
     1              CWS(3,IB) * REW2 + CWS(4,IB) * REW3 + GSM
C
            TAUSI = WCLI * ( AIS(1,IB) + AIS(2,IB) / DG +
     1              AIS(3,IB) / DG2)
            OMSI  = 1.0 - (BIS(1,IB) + BIS(2,IB) * DG +
     1              BIS(3,IB) * DG2 + BIS(4,IB) * DG3)
            GSI   = CIS(1,IB) + CIS(2,IB) * DG + CIS(3,IB) * DG2 + 
     1              CIS(4,IB) * DG3
C
            TAUCS(I,K)  =  TAUSW + TAUSI
            IF (TAUCS(I,K) .GT. 0.)                             THEN
               Y1          =  OMSW * TAUSW 
               Y2          =  OMSI * TAUSI 
               OMCS(I,K) = (Y1 + Y2) / TAUCS(I,K) 
               GCS (I,K) = (Y1 * GSW + Y2 * GSI) / (Y1 + Y2)
            ELSE
               OMCS(I,K) =  0.
               GCS (I,K) =  0.
            ENDIF
         ENDIF
      ENDDO
      ENDDO
C
      WRKA(1:LENGATH) = 0.
      WRKB(1:LENGATH) = 0.
C
      DO I = 1, LENGATH
        VI_TAUCS(I) = 0.
      ENDDO
C
      DO K = 1, LAY
      DO I = 1, LENGATH
         J = ISUN(I)
         CLDG(I,K) = CLD(J,K)
C
C----------------------------------------------------------------------C
C     SCALING THE CLOUD OPTICAL PROPERTIES                             C
C----------------------------------------------------------------------C
C
         TAUCSI                 =  TAUCS(J,K)
         IF (CLDG(I,K) .GE. CUT)                                    THEN
            VI_TAUCSM           =  VI_TAUCS(I)
            VI_TAUCS(I)         =  VI_TAUCS(I) + TAUCSI
            IF(MCICA.EQ.0)                                     THEN
              RMUGFAC           =  (2.0 - RMUG(I)) ** 0.40
              ANUFAC            =  1.0 / (1.0 + 5.68 * ANU(J,K) ** 1.4)
              X                 =  TAUCSI + 9.2 * SQRT(VI_TAUCSM) 
              TAUCSG(I,K)       =  TAUCSI / (1.0 + 0.185 *
     1                             X * RMUGFAC * ANUFAC)
            ELSE
              TAUCSG(I,K)       =  TAUCSI
            ENDIF
            IF (CLDT(J).GT.0.) THEN
              X_CAS = MAX(MIN(CLDG(I,K)/CLDT(J),1.),0.)
            ELSE
              X_CAS = 0.0
            ENDIF
            C20                 =  TAUCSG(I,K) * OMCS(J,K)
            TAUOMC(I,K)         =  TAUOMA(I,K) + C20
            C30                 =  C20 * GCS(J,K)
            TAUOMGC(I,K)        =  TAUOMGA(I,K) + C30
            F2(I,K)             =  F1(I,K) + C30 * GCS(J,K)
         ELSE
            X_CAS        = 0.
            VI_TAUCS(I)  = 0.   
            TAUCSG(I,K)  = 0.
            TAUOMC(I,K)  = 0.
            TAUOMGC(I,K) = 0.
            F2(I,K)      = 0.     
         ENDIF
C
         WRKA(I) = WRKA(I) + TAUA(I,K)         
         WRKB(I) = WRKB(I) + X_CAS*TAUCSI + TAUA(I,K)
      ENDDO
      ENDDO

      RETURN
      END
