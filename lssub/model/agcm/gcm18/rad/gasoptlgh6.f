      SUBROUTINE GASOPTLGH6(TAUG, GWGH, DP, IB, IG, O3, Q, CO2, CH4,
     1                      AN2O, INPT, MCONT, DIR, DIP, DT, LEV1, GH, 
     2                      IL1, IL2, ILG, LAY)
C
C     * APR 22/2010 - J.LI.     NEW VERSION FOR GCM15I:
C     *                         - ADD ONE EXTRA TERM TO BANDL4GH FOR
C     *                           GREATER ACCURACY.
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION GASOPTLGH5 FOR GCM15H:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS TLINE{1,2,3}Z INSTEAD OF 
C     *                           TLINE{1,2,3}Y.
C     * APR 18,2008 - M.LAZARE/ PREVIOUS VERSION GASOPTLGH4 FOR GCM15G:
C     *               J.LI.     - COSMETIC CHANGE TO USE SCALAR VARIABLE
C     *                           "INITAUG" (=2) IN CALLS TO TLINE1Y
C     *                           INSTEAD OF THE ACTUAL NUMBER ITSELF.
C     *                           SIMILAR COSMETIC CHANGE TO USE NTL(=28)
C     *                           SCALAR VARIABLE INSTEAD OF THE ACTUAL
C     *                           NUMBER ITSELF IN CALLS TO ALL "TLINE_"
C     *                           ROUTINES.
C     *                         - CALLS TLINE{1,2,3}Y INSTEAD OF 
C     *                           TLINE{1,2,3}X.
C     * MAY 05/2006 - M.LAZARE. PREVIOUS VERSION GASOPTLGH3 FOR GCM15E/F:
C     *                         - CALLS NEW VERSIONS OF:
C     *                           TLINE1X,TLINE2X,TLINE3Y,TCONTL1,
C     *                           TCONTHL1.
C     * DEC 07/2004 - J.LI. PREVIOUS VERSION GASOPTLGH2 FOR GCM15C/GCM15D:
C     *                     BUGFIX TO REVERSE CL4N2OGH AND CL4CH4GH
C     *                     IN BANDL4GH COMMON BLOCK TO BE CONSISTENT
C     *                     WITH DATA DEFINED IN CKDLW2.
C     * APR 25/2003 - J.LI. PREVIOUS VERSION GASOPTLGH FOR GCM15B.
C----------------------------------------------------------------------C
C     THE SAME AS GASOPTL BUT FOR INTERVALS CLOSE TO 1 IN THE          C
C     ACCUMULATED PROBABILITY SPACE                                    C
C     TLINE, ETC., DEAL WITH LINE ABSORPTION AND TCONTL AND TCONTHL    C
C     DEAL WITH WATER VAPOR CONTINUUM                                  C
C                                                                      C
C     TAUG: GASEOUS OPTICAL DEPTH                                      C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C     O3:   O3 MASS MIXING RATIO                                       C
C     Q:    WATER VAPOR MASS MIXING RATIO                              C
C     AN2O: N2O, ALSO CO2, CH4, F11, F12, F113, F114 ARE MASS MIXING   C
C           RATIOS                                                     C
C     DIP:  INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO             C
C           NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS            C
C     DT:   LAYER TEMPERATURE - 250 K                                  C
C     INPT: NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES  C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY)
C
      REAL   DP(ILG,LAY), Q(ILG,LAY), O3(ILG,LAY), CO2(ILG,LAY), 
     1       CH4(ILG,LAY), AN2O(ILG,LAY), DIR(ILG,LAY), DIP(ILG,LAY), 
     2       DT(ILG,LAY)
      INTEGER INPT(ILG,LAY)
      LOGICAL GH
C
      COMMON /BANDL1GH/  GW1GH(5),  CL1CO2GH(5,28,5)
      COMMON /BANDL2GH/  GW2GH(1),  CL2H2OGH(5,28), 
     1                              CL2CSGH(5,3), CL2CFGH(5,3)
      COMMON /BANDL3GH/  GW3GH(3),  CL3H2OGH(5,28,3),
     1                              CL3CSGH(5,4), CL3CFGH(5,4) 
      COMMON /BANDL4GH/  GW4GH(5),  CL4H2OGH(5,28,5), CL4N2OGH(5,28,5),
     1                              CL4CH4GH(5,28,5)
      COMMON /BANDL5GH/  GW5GH(4),  CL5H2OGH(5,28,4), CL5O3GH(5,28,4),
     1                              CL5CSGH(5,4,2), CL5CFGH(5,4,2)
C     (NO BAND 6 FOR GH)
      COMMON /BANDL7GH/  GW7GH(7),  CL7H2OGH(5,28,4), CL7CO2GH(5,28,7),
     1                              CL7O3GH(2)
      COMMON /BANDL8GH/  GW8GH(3),  CL8H2OGH(5,28,3)
      COMMON /BANDL9GH/  GW9GH(6),  CL9H2OGH(5,28,6)
C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY.
C
      DATA NTL /28/
C
C     * INITAUG IS A SWITCH USED IN TLINE1Y (AS "IPLUS") WHICH 
C     * INITIALIZES TAUG TO ZERO IF ITS VALUE IS TWO. THIS IS WHAT
C     * WE REQUIRE THROUGHOUT THIS ROUTINE.
C
      DATA INITAUG /2/
C=======================================================================
      IF (IB .EQ. 1)                                                THEN
C
C----------------------------------------------------------------------C
C     BAND (2500 - 2200 CM-1), NONGRAY GASEOUS ABSORPTION OF CO2.     C
C----------------------------------------------------------------------C
C
      CALL TLINE1Z(TAUG, CL1CO2GH(1,1,IG), CO2, DP, DIP, DT, INPT, 
     1             LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
C
      GWGH =  GW1GH(IG)
C
      ELSE IF (IB .EQ. 2)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (2200 - 1900 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O      C
C----------------------------------------------------------------------C
C
      CALL TLINE1Z(TAUG, CL2H2OGH, Q, DP, DIP, DT, INPT,
     1             LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
C
      LC =  3
      CALL TCONTL1(TAUG, CL2CSGH, CL2CFGH, Q, DP, DIP, DT, LC, INPT, 
     1             MCONT, GH, IL1, IL2, ILG, LAY)
C
      GWGH =  GW2GH(IG)
C
      ELSE IF (IB .EQ. 3)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (1900 - 1400 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.     C
C----------------------------------------------------------------------C
C
      CALL TLINE1Z(TAUG, CL3H2OGH(1,1,IG), Q, DP, DIP, DT, INPT,
     1             LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
C
      IF (IG .EQ. 1)                                                THEN
        LC =  4
        CALL TCONTL1(TAUG, CL3CSGH, CL3CFGH, Q, DP, DIP, DT, LC, INPT,
     1               MCONT, GH, IL1, IL2, ILG, LAY)
C
      ENDIF
C
      GWGH =  GW3GH(IG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND3 (1100 - 1400 CM-1), OVERLAPPING ABSORPTION OF H2O, N2O,   C
C     AND CH4. DIRECT MAPPING METHOD FOR H2O AND CH4 AND N2O           C
C----------------------------------------------------------------------C
C
      CALL TLINE3Z(TAUG, CL4H2OGH(1,1,IG), CL4CH4GH(1,1,IG), 
     1             CL4N2OGH(1,1,IG), Q, CH4, AN2O, DP, DIP, DT, INPT,
     2             LEV1, GH, NTL, IL1, IL2, ILG, LAY)
C
      GWGH =  GW4GH(IG)
C
      ELSE IF (IB .EQ. 5)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND5 (980 - 1100 CM-1), OVERLAPPING ABSORPTION OF H2O AND O3   C
C     DIRECT MAPPING METHOD                                            C
C----------------------------------------------------------------------C
C
      CALL TLINE2Z(TAUG, CL5H2OGH(1,1,IG), CL5O3GH(1,1,IG), Q, O3, 
     1             DP, DIP, DT, INPT, LEV1, GH, NTL, 
     2             IL1, IL2, ILG, LAY)
C
      IF (IG .LE. 2)                                                THEN
        LC =  4
        CALL TCONTL1(TAUG, CL5CSGH(1,1,IG), CL5CFGH(1,1,IG), Q, DP, DIP,
     1               DT, LC, INPT, MCONT, GH, IL1, IL2, ILG, LAY)
      ENDIF
C
      GWGH =  GW5GH(IG)
C
C----------------------------------------------------------------------C
C     BAND (800 - 980 CM-1), NO GH                                    C
C----------------------------------------------------------------------C
C
      ELSE IF (IB .EQ. 7)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND6 (540 - 800 CM-1), OVERLAPPING ABSORPTION OF H2O AND CO2   C
C     DIRECT MAPPING METHOD. FOR IG > 4, THE CONTRIBUTION BY H2O IS    C
C     VERY SMALL.                                                      C
C----------------------------------------------------------------------C
C
      IF (IG .LE. 4)                                                THEN
        CALL TLINE2Z(TAUG, CL7H2OGH(1,1,IG), CL7CO2GH(1,1,IG), Q, CO2, 
     1               DP, DIP, DT, INPT, LEV1, GH, NTL, 
     2               IL1, IL2, ILG, LAY)

C----------------------------------------------------------------------C
C     SIMPLY ADD THE O3 EFFECT                                         C
C----------------------------------------------------------------------C
C
        IF (IG .LE. 2)                                              THEN
          DO 700 K = 1, LAY
          DO 700 I = IL1, IL2
            TAUG(I,K) =  TAUG(I,K) + CL7O3GH(IG) * O3(I,K) * DP(I,K)
  700     CONTINUE
        ENDIF
      ELSE
C
        CALL TLINE1Z(TAUG, CL7CO2GH(1,1,IG), CO2, DP, DIP, DT, INPT,
     1               LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
      ENDIF
C
      GWGH =  GW7GH(IG)
C
      ELSE IF (IB .EQ. 8)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (340 - 540 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.       C
C----------------------------------------------------------------------C
C
      CALL TLINE1Z(TAUG, CL8H2OGH(1,1,IG), Q, DP, DIP, DT, INPT,
     1             LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
C
      GWGH =  GW8GH(IG)
C
      ELSE IF (IB .EQ. 9)                                           THEN
C
C----------------------------------------------------------------------C
C     BAND (0 - 340 CM-1), NONGRAY GASEOUS ABSORPTION OF H2O.         C
C----------------------------------------------------------------------C
C
      CALL TLINE1Z(TAUG, CL9H2OGH(1,1,IG), Q, DP, DIP, DT, INPT,
     1             LEV1, GH, NTL, INITAUG, IL1, IL2, ILG, LAY)
C
      GWGH =  GW9GH(IG)
C
      ENDIF
C
      RETURN
      END
