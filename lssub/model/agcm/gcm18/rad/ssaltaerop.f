      SUBROUTINE SSALTAEROP(EXTA, EXOMA, EXOMGA, FA, ABSA, EXTA055,
     1                      EXTA086, SSA055, RHIN, ALOAD, RE, VE,
     2                      IL1, IL2, ILG, LAY)
C
C     * FEB 07/2015 - K.VONSALZEN. Revised version for GCM18:
C     *                            - Expanded sizes for RHNODE,RENODE,
C     *                              VENODE,FR2NODE.
C     *                            - Changes to max/min bounds for
C     *                              VE and RH.
C     * 2012.4 - J.LI.   PLA VERSION BASED ON KNUT'S GROWTH METHOD,
C                        NO CRYSTALAZATION POINT.
C     * 2006.9 - LI & MA CONTINUOUS SIZE SCHEME
C----------------------------------------------------------------------C
C     CALCULATION OF OPTICAL PROPERTIES FOR SEA SALT AEROSOL           C
C     BASED ON WET SSALT CONCENTRATION, WITH LOG NORM DISTRIBUTION     C
C                                                                      C
C     EXTA:    EXTINCTION COEFFICIENT * ALOAD                          C
C     EXOMA:   EXTA TIMES SINGLE SCATTERING ALBEDO                     C
C     EXOMGA:  EXOMA TIMES ASYMMETRY FACTOR                            C
C     FA:      SQUARE OF ASYMMETRY FACTOR                              C
C     ABSA:    ABSORPTION COEFFICIENT * ALBEDO                         C
C                                                                      C
C     EXTA055: EXTINCTION COEFFICIENT AT 0.55 UM * ALOAD               C
C     EXTA086: EXTINCTION COEFFICIENT AT 0.865 UM * ALOAD              C
C     SSA055:  SINGLE SCATTERING ALBEDO AT 0.55 UM                     C
C     GA055:   ASYMMETRY FACTOR AT 0.55 UM                             C
C                                                                      C
C     RHIN:    INPUT RELATIVE HUMIDITY                                 C
C     ALOAD:   SEA SALT AEROSOL MASS RATIO FOR EACH LAYER (KG/KG)      C
C     RE:      EFFECTIVE RADIUS                                        C
C     RH:      RELATIVE HUMIDITY                                       C
C                                                                      C
C     SEXT:    FIRST 4 ARE RESULTS FOR 4 SOLAR BANDS, THE FIFTH IS     C
C              FOR 0.55 UM, THE LAST IS FOR 0.865 UM                   C
C     SOMG/SG: FOR 4 ARE FOR 4 SOLAR BANDS, THE LAST FOR 0.55 UM       C
C     SABS:    FIRST 9 ARE RESULTS FOR 9 LW BANDS                      C
C----------------------------------------------------------------------C
C
      USE RDMOD
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, DIMENSION(ILG,LAY,NBS):: EXTA, EXOMA, EXOMGA, FA
      REAL, DIMENSION(ILG,LAY,NBL):: ABSA
      REAL, DIMENSION(NBS):: SEXTA, SOMGA, SGA
      REAL                   SEXT(6), SOMG(5), SG(5), SABS(9)
      REAL, DIMENSION(NBL):: SABSA
      REAL, DIMENSION(ILG,LAY):: EXTA055, EXTA086, SSA055, GA055,
     &                           RH, RE, VE, FR1, FR2, ALOAD,
     &                           RHIN, SLOAD
      REAL  RHNODE(NHS), RENODE(NRS), VENODE(NVS)
      REAL, DIMENSION(2):: TH, TR, TV
C
      DATA RHNODE  /0.1, 0.5, 0.75, 0.85, 0.9, 0.93, 0.95, 0.96, 0.97,
     &              0.98, 0.99/
      DATA RENODE  /0.7, 1.7, 2.7, 3.7, 4.7, 5.7/
      DATA VENODE  /0.4, 0.6, 0.8/
C
C----------------------------------------------------------------------C
C     FACTOR 10000, BECAUSE THE UNIT OF SPECIFIC EXTINCTION FOR AEROSOLC
C     IS M^2/GRAM, WHILE FOR GAS IS CM^2/GRAM, IN RADDRIV THE SAME DP  C
C     (AIR DENSITY * LAYER THICKNESS) IS USED FOR BOTH GAS AND AEROSOL.C
C     ALOAD IS DRY LOADING IN UNIT G (AEROSOL) / G(AIR).               C
C----------------------------------------------------------------------C
C
      DO 200 K = 1, LAY
      DO 200 I = IL1, IL2
        IF (ALOAD(I,K) .GT. 1.E-12)                                 THEN
C
        SLOAD(I,K)      =  10000. * ALOAD(I,K)
        RH(I,K)         =  MAX(MIN (RHIN(I,K), 0.95), 0.1)
        RE(I,K)         =  MAX(MIN (RE(I,K), 5.7), 0.7)
        VE(I,K)         =  MAX(MIN (VE(I,K), 0.8), 0.4)
C
        IH              =  MVIDX(RHNODE, NHS, RH(I,K))
        IR              =  MVIDX(RENODE, NRS, RE(I,K))
        IV              =  MVIDX(VENODE, NVS, VE(I,K))
C
        TH(2)           = (RH(I,K) - RHNODE(IH)) /
     1                                       (RHNODE(IH+1) - RHNODE(IH))
        TH(1)           =  1.0 - TH(2)
        TR(2)           = (RE(I,K) - RENODE(IR)) /
     1                                       (RENODE(IR+1) - RENODE(IR))
        TR(1)           =  1.0 - TR(2)
        TV(2)           = (VE(I,K) - VENODE(IV)) /
     1                                       (VENODE(IV+1) - VENODE(IV))
        TV(1)           =  1.0 - TV(2)
C
        DO J = 1, NBS
          SEXTA(J)      =  0.0
          SOMGA(J)      =  0.0
          SGA(J)        =  0.0
        ENDDO
        SEXTA055        =  0.0
        SEXTA086        =  0.0
        SOMGA055        =  0.0
        SGA055          =  0.0
C
        DO J = 1, NBL
          SABSA(J)      =  0.0
        ENDDO
C
        DO 100 IHH      =  IH, IH + 1
        DO 100 IRR      =  IR, IR + 1
        DO 100 IVV      =  IV, IV + 1
C
          IREC          = (IHH - 1) * NRS * NVS +
     1                    (IRR - 1) * NVS + IV
C
          SEXT=SEXTT2(IREC,:)
          SOMG=SOMGT2(IREC,:)
          SG=SGT2(IREC,:)
          SABS=SABST2(IREC,:)
C
          WTT           =  TH(IHH - IH + 1) * TR(IRR - IR + 1) *
     1                     TV(IVV - IV + 1)
C
          DO J = 1, NBS
            SEXTA(J)    =  SEXTA(J) + SEXT(J) * WTT
            SOMGA(J)    =  SOMGA(J) + SOMG(J) * WTT
            SGA(J)      =  SGA(J) + SG(J) * WTT
          ENDDO
            SEXTA055    =  SEXTA055 + SEXT(5) * WTT
            SEXTA086    =  SEXTA086 + SEXT(6) * WTT
            SOMGA055    =  SOMGA055 + SOMG(5) * WTT
            SGA055      =  SGA055   + SG(5) * WTT
C
          DO J = 1, NBL
            SABSA(J)    =  SABSA(J) + SABS(J) * WTT
          ENDDO
C
  100   CONTINUE
C
C----------------------------------------------------------------------C
C     THE RESULTS OF EXTA, EXOMA, EXOMGA, FA, ABSA ARE ACCUMULATED     C
C     WITH DUST & SSALT CALCULATED IN OTHER SUBROUTINES                C
C----------------------------------------------------------------------C
C
        DO J = 1, NBS
          EXTLOAD       =  SEXTA(J) * SLOAD(I,K)
          EXTA(I,K,J)   =  EXTA(I,K,J) + EXTLOAD
          EXTOMLOAD     =  EXTLOAD * SOMGA(J)
          EXOMA(I,K,J)  =  EXOMA(I,K,J) + EXTOMLOAD
          EXTOMGLOAD    =  EXTOMLOAD * SGA(J)
          EXOMGA(I,K,J) =  EXOMGA(I,K,J) + EXTOMGLOAD
          FA(I,K,J)     =  FA(I,K,J) + EXTOMGLOAD * SGA(J)
        ENDDO
          EXTA055(I,K)  =  SEXTA055
          SSA055(I,K)   =  SOMGA055
          GA055(I,K)    =  SGA055
          EXTA086(I,K)  =  SEXTA086
C
        DO J = 1, NBL
          ABSA(I,K,J)   =  ABSA(I,K,J) + SABSA(J) * SLOAD(I,K)
        ENDDO
C
        ELSE
          EXTA055(I,K)  =  0.0
          SSA055(I,K)   =  0.0
          GA055(I,K)    =  0.0
          EXTA086(I,K)  =  0.0
        ENDIF
  200 CONTINUE
C
      RETURN
      END
