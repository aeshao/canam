      SUBROUTINE SET_MMR(CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM, F12_PPM,
     1                   F113_PPM, F114_PPM)
c
c=======================================================================
c Set mass mixing ratios in common TRACE from input concentrations.
c   L. Solheim ...Jan, 2008
c
c This has been extracted from subroutine RADCONS to provide a modular
c approach that will allow a consistent way to change these mixing
c ratios to thread specific values inside an openmp parallel region.
c=======================================================================
C
      IMPLICIT NONE
C
      REAL :: CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM, F12_PPM,
     1        F113_PPM, F114_PPM
C
C     * COMMON BLOCK FOR ABSORBER MIXING RATIOS.
C
      REAL :: RMCO2, RMCH4, RMN2O, RMO2, RMF11, RMF12, RMF113, RMF114
      COMMON /TRACE / RMCO2, RMCH4, RMN2O, RMO2, RMF11, RMF12, RMF113,
     1                RMF114
!$OMP THREADPRIVATE (/TRACE/)
C=======================================================================
C
C----------------------------------------------------------------------C
C     INPUT TRACE GAS CONCENTRATIONS IN UNIT PPMV,                     C
C     PARTS PER MILLION BY VOLUME, TRANSFORM TO MASS MIXING RATIO.     C
C     THE SAME AS WATER VAPOR AND OZONE.                               C
C     1.5188126 = 44.    / 28.97                                       C
C     0.5522955 = 16.    / 28.97                                       C
C     1.5188126 = 44.    / 28.97                                       C
C     O2 INPUT AS A CONSTANT, UNIT MIXING RATO BY MASS                 C
C     4.7418019 = 137.37 / 28.97                                       C
C     4.1736279 = 120.91 / 28.97                                       C
C     6.4704867 = 187.45 / 28.97                                       C
C     5.6920953 = 164.90 / 28.97                                       C
C     28.97 MOLECULAR WEIGHT OF AIR, E-06 PER MILLION                  C
C----------------------------------------------------------------------C
C
      RMCO2  =  CO2_PPM  * 1.5188126
      RMCH4  =  CH4_PPM  * 0.5522955
      RMN2O  =  N2O_PPM  * 1.5188126
      RMO2   =  0.2315
      RMF11  =  F11_PPM  * 4.7418019
      RMF12  =  F12_PPM  * 4.1736279
      RMF113 =  F113_PPM * 6.4704867
      RMF114 =  F114_PPM * 5.6920953

      RETURN
      END
