      SUBROUTINE TCONTHL4 (TAUG, COEF1, COEF2, S, DP, DIP, DIR, DT,
     1                     INPTR, INPT, MCONT, IL1, IL2, ILG, LAY)
C
C     * FEB 9,2009  - J.LI.      NEW VERSION FOR GCM15H:
C     *                          - REMOVE UNUSED TRACE COMMON BLOCK. 
C     * APR 21/2008 - L.SOLHEIM. PREVIOUS VERSION TCONTHL3 FOR GCM15G:
C     *                          - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                            FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                            OF "RADFORCE" MODEL OPTION. 
C     * JUN 21/2006 - J.LI.     PREVIOUS VERSION TCONTHL2 FOR GCM15F:
C     *                         - NEW SCHEME TO PROPERLY ACCOUNT FOR THE
C     *                           H2O/C)2 RATIO.
C     * JUN 19/2006 - M.LAZARE. PREVIOUS VERSION TCONTHL1 FOR GCM15E:
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     *                         - USE VARIABLE INSTEAD OF CONSTANT
C     *                           IN INTRINSICS SUCH AS "MAX",
C     *                           SO THAT CAN COMPILE IN 32-BIT MODE
C     *                           WITH REAL  .
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION TCONTHL FOR GCM15D.
C----------------------------------------------------------------------C
C     WATER VAPOR CONTINUUM FOR 540-800 CM-1. DIFFERENT FROM TCONTL,   C
C     VARIATION OF MASS MIXING RATIO FOR H2O AND CO2 IS CONSIDER.      C
C                                                                      C
C     TAUG:  GASEOUS OPTICAL DEPTH                                     C
C     S:     INPUT H2O MIXING RATIO FOR EACH LAYER                     C
C     DP:    AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).    C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DIR:  INTERPRETATION FACTOR FOR MASS RATIO OF H2O / CO2 BETWEEN  C
C           TWO NEIGHBORING STANDARD INPUT RATIOS                      C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     INPR: NUMBER OF THE RATIO LEVEL FOR THE STANDARD 5 RATIOS        C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C     MCONT: THE HIGHEST LEVEL FOR WATER VAPOR CONTINUUM CALCULATION   C
C----------------------------------------------------------------------C
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY), COEF1(5,5,4), COEF2(5,5,4)
C
      REAL   S(ILG,LAY), DP(ILG,LAY), DIP(ILG,LAY), DIR(ILG,LAY),
     1       DT(ILG,LAY)
      INTEGER INPTR(ILG,LAY), INPT(ILG,LAY)
C-----------------------------------------------------------------------
      DO 200 K = MCONT, LAY
       IF (INPT(1,K) .LT. 950)                                      THEN   
        DO 100 I = IL1, IL2
         M =  INPT(I,K) - 14
         N =  M + 1
         IF (M .GE. 1)                                              THEN
          L   =  INPTR(I,K)
          IF (L .LT. 1)                                             THEN
            X1  =  COEF1(1,1,M) + DT(I,K) * (COEF1(2,1,M) + DT(I,K) *
     1            (COEF1(3,1,M) + DT(I,K) * (COEF1(4,1,M) +
     2             DT(I,K) * COEF1(5,1,M))))
            X2  =  COEF1(1,1,N) + DT(I,K) * (COEF1(2,1,N) + DT(I,K) *
     1            (COEF1(3,1,N) + DT(I,K) * (COEF1(4,1,N) +
     2             DT(I,K) * COEF1(5,1,N))))
C
            Y1  =  COEF2(1,1,M) + DT(I,K) * (COEF2(2,1,M) + DT(I,K) *
     1            (COEF2(3,1,M) + DT(I,K) * (COEF2(4,1,M) +
     2             DT(I,K) * COEF2(5,1,M))))
            Y2  =  COEF2(1,1,N) + DT(I,K) * (COEF2(2,1,N) + DT(I,K) *
     1            (COEF2(3,1,N) + DT(I,K) * (COEF2(4,1,N) +
     2             DT(I,K) * COEF2(5,1,N))))
C
          ELSE IF (L .LT. 5)                                        THEN
            LP1 =  L + 1
            X11 =  COEF1(1,L,M) + DT(I,K) * (COEF1(2,L,M) + DT(I,K) *
     1            (COEF1(3,L,M) + DT(I,K) * (COEF1(4,L,M) +
     2             DT(I,K) * COEF1(5,L,M))))
            X21 =  COEF1(1,L,N) + DT(I,K) * (COEF1(2,L,N) + DT(I,K) *
     1            (COEF1(3,L,N) + DT(I,K) * (COEF1(4,L,N) +
     2             DT(I,K) * COEF1(5,L,N))))
C
            Y11 =  COEF2(1,L,M) + DT(I,K) * (COEF2(2,L,M) + DT(I,K) *
     1            (COEF2(3,L,M) + DT(I,K) * (COEF2(4,L,M) +
     2             DT(I,K) * COEF2(5,L,M))))
            Y21 =  COEF2(1,L,N) + DT(I,K) * (COEF2(2,L,N) + DT(I,K) *
     1            (COEF2(3,L,N) + DT(I,K) * (COEF2(4,L,N) +
     2             DT(I,K) * COEF2(5,L,N))))
C
            X12 =  COEF1(1,LP1,M) + DT(I,K) * (COEF1(2,LP1,M) +
     1                              DT(I,K) * (COEF1(3,LP1,M) +
     2                              DT(I,K) * (COEF1(4,LP1,M) +
     3                              DT(I,K) * COEF1(5,LP1,M))))
            X22 =  COEF1(1,LP1,N) + DT(I,K) * (COEF1(2,LP1,N) +
     1                              DT(I,K) * (COEF1(3,LP1,N) +
     2                              DT(I,K) * (COEF1(4,LP1,N) +
     3                              DT(I,K) * COEF1(5,LP1,N))))
C
            Y12 =  COEF2(1,LP1,M) + DT(I,K) * (COEF2(2,LP1,M) +
     1                              DT(I,K) * (COEF2(3,LP1,M) +
     2                              DT(I,K) * (COEF2(4,LP1,M) +
     3                              DT(I,K) * COEF2(5,LP1,M))))
            Y22 =  COEF2(1,LP1,N) + DT(I,K) * (COEF2(2,LP1,N) +
     1                              DT(I,K) * (COEF2(3,LP1,N) +
     2                              DT(I,K) * (COEF2(4,LP1,N) +
     3                              DT(I,K) * COEF2(5,LP1,N))))
C
            X1  =  X11 + (X12 - X11) * DIR(I,K)
            X2  =  X21 + (X22 - X21) * DIR(I,K)
            Y1  =  Y11 + (Y12 - Y11) * DIR(I,K)
            Y2  =  Y21 + (Y22 - Y21) * DIR(I,K)
          ELSE
            X1  =  COEF1(1,5,M) + DT(I,K) * (COEF1(2,5,M) + DT(I,K) *
     1            (COEF1(3,5,M) + DT(I,K) * (COEF1(4,5,M) +
     2             DT(I,K) * COEF1(5,5,M))))
            X2  =  COEF1(1,5,N) + DT(I,K) * (COEF1(2,5,N) + DT(I,K) *
     1            (COEF1(3,5,N) + DT(I,K) * (COEF1(4,5,N) +
     2             DT(I,K) * COEF1(5,5,N))))
            Y1  =  COEF2(1,5,M) + DT(I,K) * (COEF2(2,5,M) + DT(I,K) *
     1            (COEF2(3,5,M) + DT(I,K) * (COEF2(4,5,M) +
     2             DT(I,K) * COEF2(5,5,M))))
            Y2  =  COEF2(1,5,N) + DT(I,K) * (COEF2(2,5,N) + DT(I,K) *
     1            (COEF2(3,5,N) + DT(I,K) * (COEF2(4,5,N) +
     2             DT(I,K) * COEF2(5,5,N))))
          ENDIF
C
          TAUG(I,K) =  TAUG(I,K) + ((X1 - Y1 + (X2 - X1 - Y2 + Y1) *
     1                 DIP(I,K)) * 1.608 * S(I,K) + Y1 + (Y2 - Y1) *
     2                 DIP(I,K)) * S(I,K) * DP(I,K)
         ENDIF
  100   CONTINUE
       ELSE
        J =  INPT(1,K) - 1000
        M =  J - 14
        N =  M + 1
        DO 150 I = IL1, IL2
         IF (M .GE. 1)                                              THEN
          L   =  INPTR(I,K)
          IF (L .LT. 1)                                             THEN
            X1  =  COEF1(1,1,M) + DT(I,K) * (COEF1(2,1,M) + DT(I,K) *
     1            (COEF1(3,1,M) + DT(I,K) * (COEF1(4,1,M) +
     2             DT(I,K) * COEF1(5,1,M))))
            X2  =  COEF1(1,1,N) + DT(I,K) * (COEF1(2,1,N) + DT(I,K) *
     1            (COEF1(3,1,N) + DT(I,K) * (COEF1(4,1,N) +
     2             DT(I,K) * COEF1(5,1,N))))
C
            Y1  =  COEF2(1,1,M) + DT(I,K) * (COEF2(2,1,M) + DT(I,K) *
     1            (COEF2(3,1,M) + DT(I,K) * (COEF2(4,1,M) +
     2             DT(I,K) * COEF2(5,1,M))))
            Y2  =  COEF2(1,1,N) + DT(I,K) * (COEF2(2,1,N) + DT(I,K) *
     1            (COEF2(3,1,N) + DT(I,K) * (COEF2(4,1,N) +
     2             DT(I,K) * COEF2(5,1,N))))
C
          ELSE IF (L .LT. 5)                                        THEN
            LP1 =  L + 1
            X11 =  COEF1(1,L,M) + DT(I,K) * (COEF1(2,L,M) + DT(I,K) *
     1            (COEF1(3,L,M) + DT(I,K) * (COEF1(4,L,M) +
     2             DT(I,K) * COEF1(5,L,M))))
            X21 =  COEF1(1,L,N) + DT(I,K) * (COEF1(2,L,N) + DT(I,K) *
     1            (COEF1(3,L,N) + DT(I,K) * (COEF1(4,L,N) +
     2             DT(I,K) * COEF1(5,L,N))))
C
            Y11 =  COEF2(1,L,M) + DT(I,K) * (COEF2(2,L,M) + DT(I,K) *
     1            (COEF2(3,L,M) + DT(I,K) * (COEF2(4,L,M) +
     2             DT(I,K) * COEF2(5,L,M))))
            Y21 =  COEF2(1,L,N) + DT(I,K) * (COEF2(2,L,N) + DT(I,K) *
     1            (COEF2(3,L,N) + DT(I,K) * (COEF2(4,L,N) +
     2             DT(I,K) * COEF2(5,L,N))))
C
            X12 =  COEF1(1,LP1,M) + DT(I,K) * (COEF1(2,LP1,M) +
     1                              DT(I,K) * (COEF1(3,LP1,M) +
     2                              DT(I,K) * (COEF1(4,LP1,M) +
     3                              DT(I,K) * COEF1(5,LP1,M))))
            X22 =  COEF1(1,LP1,N) + DT(I,K) * (COEF1(2,LP1,N) +
     1                              DT(I,K) * (COEF1(3,LP1,N) +
     2                              DT(I,K) * (COEF1(4,LP1,N) +
     3                              DT(I,K) * COEF1(5,LP1,N))))
C
            Y12 =  COEF2(1,LP1,M) + DT(I,K) * (COEF2(2,LP1,M) +
     1                              DT(I,K) * (COEF2(3,LP1,M) +
     2                              DT(I,K) * (COEF2(4,LP1,M) +
     3                              DT(I,K) * COEF2(5,LP1,M))))
            Y22 =  COEF2(1,LP1,N) + DT(I,K) * (COEF2(2,LP1,N) +
     1                              DT(I,K) * (COEF2(3,LP1,N) +
     2                              DT(I,K) * (COEF2(4,LP1,N) +
     3                              DT(I,K) * COEF2(5,LP1,N))))
C
            X1  =  X11 + (X12 - X11) * DIR(I,K)
            X2  =  X21 + (X22 - X21) * DIR(I,K)
            Y1  =  Y11 + (Y12 - Y11) * DIR(I,K)
            Y2  =  Y21 + (Y22 - Y21) * DIR(I,K)
          ELSE
            X1  =  COEF1(1,5,M) + DT(I,K) * (COEF1(2,5,M) + DT(I,K) *
     1            (COEF1(3,5,M) + DT(I,K) * (COEF1(4,5,M) +
     2             DT(I,K) * COEF1(5,5,M))))
            X2  =  COEF1(1,5,N) + DT(I,K) * (COEF1(2,5,N) + DT(I,K) *
     1            (COEF1(3,5,N) + DT(I,K) * (COEF1(4,5,N) +
     2             DT(I,K) * COEF1(5,5,N))))
            Y1  =  COEF2(1,5,M) + DT(I,K) * (COEF2(2,5,M) + DT(I,K) *
     1            (COEF2(3,5,M) + DT(I,K) * (COEF2(4,5,M) +
     2             DT(I,K) * COEF2(5,5,M))))
            Y2  =  COEF2(1,5,N) + DT(I,K) * (COEF2(2,5,N) + DT(I,K) *
     1            (COEF2(3,5,N) + DT(I,K) * (COEF2(4,5,N) +
     2             DT(I,K) * COEF2(5,5,N))))
          ENDIF
C
          TAUG(I,K) =  TAUG(I,K) + ((X1 - Y1 + (X2 - X1 - Y2 + Y1) *
     1                 DIP(I,K)) * 1.608 * S(I,K) + Y1 + (Y2 - Y1) *
     2                 DIP(I,K)) * S(I,K) * DP(I,K)
         ENDIF
  150   CONTINUE
       ENDIF
  200 CONTINUE
C
      RETURN
      END
