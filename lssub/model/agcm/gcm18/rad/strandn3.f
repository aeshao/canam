      SUBROUTINE STRANDN3(TRANT, ATTN, ATTNTOP, RMU, DP, DT, O3, O2,
     1                    RMU3,ITILE,
     2                    IB, IG, LEV1, IL1, IL2, ILG, LAY, LEV, NTILE)
C
C     * FEB 09,2009 - J.LI.     NEW VERSION FOR GCM15H:
C     *                         - O2 PASSED DIRECTLY, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK.
C     * APR 21,2008 - L.SOLHEIM/ PREVIOUS VERSION STRANDN2 FOR GCM15G:
C     *               J.LI.      - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                            FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                            OF "RADFORCE" MODEL OPTION. 
C     *                          - MORE ACCUATE TREATMENT OF O3 
C     * APR 25,2003 - J.LI.  PREVIOUS VERSION STRANDN FOR GCM15E/F.
C----------------------------------------------------------------------C
C     CALCULATION OF THE DOWNWARD FLUX FROM TOP LEVEL TO 1 MB, NO      C
C     SCATTERING EFFECT IS CONSIDERED                                  C
C                                                                      C
C     TRAN:    TRANSMISIVITY                                           C
C     ATTN:    ATTENUATION FACTOR FOR REDUCING SOLAR FLUX FROM MODEL   C
C              TOP LEVEL TO 1 MB                                       C
C     ATTNTOP: ATTENUATION FACTOR FOR REDUCING SOLAR FLUX FROM TOA TO  C
C              MODEL TOP LEVEL                                         C
C     RMU:     COSINE OF SOLAR ZENITH ANGLE                            C
C     DP:      AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).  C
C     O3:      O3 MASS MIXING RATIO                                    C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TRANT(ILG,NTILE,2,LEV), TRAN(ILG,2,LEV), ATTN(ILG), 
     1       ATTNTOP(ILG), RMU(ILG), DP(ILG,LAY), DT(ILG,LAY), 
     2       O3(ILG,LAY), O2(ILG,LAY), RMU3(ILG)
      INTEGER ITILE(ILG,NTILE)
C
      COMMON /BANDS1/ GW1(6), CS1O3(3,6), CS1O21
C=======================================================================
      LEV1M1 =  LEV1 - 1
C
      IF (IB .EQ. 1)                                                THEN
C
C----------------------------------------------------------------------C
C     BAND (14500 - 50000 CM-1), NONGRAY GASEOUS ABSORPTION OF O3     C
C     ANS O2.                                                          C
C----------------------------------------------------------------------C
C
        DO 10 I = IL1, IL2
          TRAN(I,1,1)       =  ATTNTOP(I)
          TRAN(I,2,1)       =  ATTNTOP(I)
   10   CONTINUE
C
        IF (IG .EQ. 1)                                              THEN
          DO 150 K = 1, LEV1M1
            KP1 = K + 1
            DO 100 I = IL1, IL2
              DTO3          =  DT(I,K) + 23.13
              X             = (CS1O21 - 0.881E-05 * RMU3(I)) * O2(I,K)
              TAU           = ((CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                         DTO3 * CS1O3(3,IG))) * O3(I,K) + X) *
     2                         DP(I,K)
              TRAN(I,1,KP1) =  TRAN(I,1,K) * EXP( - TAU / RMU(I))
              TRAN(I,2,KP1) =  TRAN(I,1,KP1)
  100       CONTINUE
  150     CONTINUE
C
        ELSE 
          DO 250 K = 1, LEV1M1
            KP1 = K + 1
            DO 200 I = IL1, IL2
              DTO3          =  DT(I,K) + 23.13
              TAU           = (CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                         DTO3 * CS1O3(3,IG))) * O3(I,K) * DP(I,K)
              TRAN(I,1,KP1) =  TRAN(I,1,K) * EXP( - TAU / RMU(I))
              TRAN(I,2,KP1) =  TRAN(I,1,KP1)
  200       CONTINUE
  250     CONTINUE
        ENDIF
C
C----------------------------------------------------------------------C
C     FLUX ADJUSTMENT FOR REGION BELOW 1 MB                            C
C----------------------------------------------------------------------C
C
        DO 400 I = IL1, IL2
          ATTN(I)           =  TRAN(I,1,LEV1)
  400   CONTINUE
C
      ELSE
C
        DO 500 K = 1, LEV1
        DO 500 I = IL1, IL2
          TRAN(I,1,K)       =  1.0
          TRAN(I,2,K)       =  1.0
  500   CONTINUE
C
        DO 600 I = IL1, IL2
          ATTN(I)           =  1.0
  600   CONTINUE
      ENDIF
              DO K = 1, LEV1
                 DO IT = 1, NTILE
                    DO I = IL1, IL2
                       IF (ITILE(I,IT) .GT. 0) THEN
                          TRANT(I,IT,1,K) =  TRAN(I,1,K)
                          TRANT(I,IT,2,K) =  TRAN(I,2,K)
                       END IF
                    END DO !I
                 END DO ! IT
              END DO ! K
C
      RETURN
      END
