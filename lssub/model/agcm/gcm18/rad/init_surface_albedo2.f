      SUBROUTINE INIT_SURFACE_ALBEDO2(KSALB_IN)
C***********************************************************************
C     * APR 30/12 - L.SOLHEIM. NEW VERSION FOR GCM16:
C     *                        REMOVE CHECKSUM SINCE MORE THAN ONE CHOICE.
C     *                        ORIGINAL VERSION INIT_SURFACE_ALBEDO:
C     * FEB 05/07 - F.MAJAESS  ADJUSTED IN SUPPORT OF 32-BITS MODE.
C     * MAR 17/04 - L.SOLHEIM. MODIFIED DEC 08/04 TO REMOVE THREADPRIVATE.
C
C***********************************************************************
C     * READ A CCCMA FORMAT FILE CONTAINING A LOOKUP TABLE FOR SURFACE
C     * ALBEDO THAT HAS DEPENDENCE ON OPTICAL DEPTH, ZENITH ANGLE,
C     * WINDSPEED AND OCEAN CHLOROPHYL FOR EACH OF 4 SOLAR BANDS.
C
C     * INPUT:
C     *   KSALB - FLAGS THE TYPE OF SURFACE ALBEDO CALCULATION TO BE DONE
C     *           IF KSALB .NE. 1 THEN SIMPLY RETURN WITHOUT FILLING
C     *           THE TABLE IN SALBTAB COMMON
C***********************************************************************
        IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)

        INTEGER IU,KSALB_IN
        LOGICAL OK
        COMMON /ICOM/ IBUF(8), IDAT(1)
        integer machine,intsize
        INTEGER*4 MYNODE
        COMMON /MPINFO/ MYNODE
        common /machtyp/ machine,intsize
        !--- table dimensions
        ! nb ...number of bands
        !       4 currently 0.20-0.69, 0.69-1.19, 1.19-2.38, 2.38-4.00 um
        ! nt ...number of aerosol/cloud optical depths
        ! ns ...number of solar zenith angles
        ! nw ...number of wind speeds
        ! nc ...number of chlorophyll concentration values
        parameter (nb=4, nt=16, ns=15, nw=7, nc=5)

C       * SALBCOM communicates info about the type of surface albedo
C       * calculation to be used. Currently
C       * KSALB = 0 ...surface albedo is calculated in CLASS_
C       * KSALB = 1 ...surface albedo is calculated from a lookup table
C       * KSALB = 2 ...surface albedo is calculated from a formula
        COMMON /SALBCOM/ KSALB
C

C       * Surface albedo lookup table used when KSALB=1
        COMMON /SALBTAB/ ALBEDO_TABLE(nb,nc,nw,ns,nt), INIT_TABLE
        DATA INIT_TABLE /0/
C-----------------------------------------------------------------
c.......Assign this flag to common
        KSALB=KSALB_IN

        IF (KSALB.EQ.1) THEN
          IF (MYNODE.EQ.0) THEN
            WRITE(6,*)'INIT_SURFACE_ALBEDO2: Initializing albedo'
     1               ,' lookup table with data from a file.'
          ENDIF

c.........Get an available unit number
          iu=newunit(60)

c.........Open the file containing the albedo lookup table
          INQUIRE(FILE="ALBTABLE",EXIST=OK)
          IF (.NOT.OK) THEN
              WRITE(6,*)'INIT_SURFACE_ALBEDO2: ',
     1                  'Error accessing nasa_albedo_lookup_table2'
            CALL XIT('INIT_SURFACE_ALBEDO2',-1)
          ENDIF
          OPEN(IU,FILE='ALBTABLE',FORM='UNFORMATTED')

c.........Read the albedo lookup table
          LREC=nb*nc*nw*ns*machine
          DO I=1,nt
            CALL GETFLD2(IU,ALBEDO_TABLE(1,1,1,1,I),
     &           NC4TO8("GRID"),I,NC4TO8(" ALB"),1,IBUF,LREC,OK)
            IF (.NOT.OK) CALL XIT('INIT_SURFACE_ALBEDO2',-2)
          ENDDO
          CLOSE(IU)
          ICHKSUM=NINT(SUM(ALBEDO_TABLE))
c          IF (ICHKSUM.NE.4853) THEN ! checksum for old table
          INIT_TABLE=1
        ENDIF

        IF (KSALB.EQ.2) THEN
          IF (MYNODE.EQ.0) THEN
            WRITE(6,*)'INIT_SURFACE_ALBEDO2: Surface albedo'
     &               ,' calculation from formula'
          ENDIF
        ENDIF

        RETURN
      END
