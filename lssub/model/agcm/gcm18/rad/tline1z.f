      SUBROUTINE TLINE1Z(TAUG, COEF1, S, DP, DIP, DT, INPT, LEV1, GH,
     1                   LC, IPLUS, IL1, IL2, ILG, LAY)
C
C     * FEB 09,2009 - J.LI.     NEW VERSION FOR GCM15H:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     * APR 18,2008 - M.LAZARE. PREVIOUS VERSION TLINE1Y FOR GCM15G:
C     *                         - COSMETIC CHANGE OF N->LC PASSED
C     *                           IN AND USED IN DIMENSION OF COEF
C     *                           ARRAY(S), SO THAT IT IS NOT REDEFINED
C     *                           AND PASSED BACK OUT TO CALLING
C     *                           ROUTINE, CHANGING VALUE IN DATA
C     *                           STATEMENT!
C     *                         - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                           FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                           OF "RADFORCE" MODEL OPTION. 
C     *                         - WORK ARRAY "S1" NOW LOCAL INSTEAD OF
C     *                           PASSED-IN WORKSPACE. 
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION TLINE1X FOR GCM15E/F:
C     *                         - COSMETIC CLEANUP TO ELIMINATE DUPLICATE
C     *                           CODING BETWEEN IPLUS=1 AND IPLUS=2, BY
C     *                           INITIALIZING TAUG IF IPLUS=2 (THIS WAS
C     *                           THE ONLY DIFFERENCE BETWEEN THE TWO
C     *                           SECTIONS).
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION TLINE1 FOR GCM15D.
C----------------------------------------------------------------------C
C     CALCULATION OF OPTICAL DEPTH FOR ONE GAS (LINE CONTRIBUTION ONLY)C
C     THE GASEOUS ABSORPTION COEFFICIENTS IN UNITS OF CM2 / GRAM.     C
C     S IN MASS MIXING RATIO. ABSORPTION COEFFICIENT ARE CALCULATED    C
C     AT THE TEMPERATURE T FOR THE 18 OR 26 (28) PRESSURE LEVELS.      C
C     ISL = 1 FOR SOLAR, ISL = 2 FOR INFRARED.                         C
C                                                                      C
C     TAUG: GASEOUS OPTICAL DEPTH                                      C
C     S:    INPUT GAS MIXING RATIO FOR EACH LAYER                      C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C     DIP:  INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO NEIGHBORING C
C           STANDARD INPUT DATA PRESSURE LEVELS                        C
C     DT:   LAYER TEMPERATURE - 250 K                                  C
C     INPT: NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES  C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY), COEF1(5,LC), S(ILG,LAY), DP(ILG,LAY),
     1       DIP(ILG,LAY), DT(ILG,LAY)
      INTEGER INPT(ILG,LAY)
C
      LOGICAL GH
C
C=======================================================================
      IF (GH)                                                       THEN
        LAY1 =  1
      ELSE
        LAY1 =  LEV1
      ENDIF
      LAY2   =  LAY
C
C     * INITIALIZE TAUG IF IPLUS=2.
C
      IF (IPLUS .EQ. 2) THEN
        DO  50 K = LAY1, LAY2
        DO  50 I = IL1, IL2
          TAUG(I,K)     =  0.
   50   CONTINUE
      ENDIF
C
      DO 2000 K = LAY1, LAY2
        IF (INPT(1,K) .LT. 950)                                   THEN
          DO 1000 I = IL1, IL2  
            M  =  INPT(I,K)
            N  =  M + 1
            X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) + 
     1                   DT(I,K) * (COEF1(3,N) + DT(I,K) * 
     2                  (COEF1(4,N) + DT(I,K) * COEF1(5,N))))
            IF (M .GT. 0)                                         THEN
              X1      =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) + 
     1                   DT(I,K) * (COEF1(3,M) + DT(I,K) * 
     2                  (COEF1(4,M) + DT(I,K) * COEF1(5,M))))
            ELSE
              X1      =  0.0
            ENDIF
C
            TAUG(I,K) =  TAUG(I,K) + (X1 + (X2 - X1) * DIP(I,K)) * 
     1                   S(I,K) * DP(I,K)
 1000     CONTINUE        
        ELSE
          M  =  INPT(1,K) - 1000
          N  =  M + 1
          DO 1500 I = IL1, IL2
            X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) + 
     1                   DT(I,K) * (COEF1(3,N) + DT(I,K) * 
     2                  (COEF1(4,N) + DT(I,K) * COEF1(5,N))))
            IF (M .GT. 0)                                         THEN
              X1      =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) + 
     1                   DT(I,K) * (COEF1(3,M) + DT(I,K) * 
     2                  (COEF1(4,M) + DT(I,K) * COEF1(5,M))))
            ELSE
              X1      =  0.0
            ENDIF
C
            TAUG(I,K) =  TAUG(I,K) + (X1 + (X2 - X1) * DIP(I,K)) * 
     1                   S(I,K) * DP(I,K)
 1500     CONTINUE
        ENDIF 
 2000 CONTINUE
C
      RETURN
      END
