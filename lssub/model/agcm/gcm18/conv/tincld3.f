      SUBROUTINE TINCLD3(TC,ILG,ILEV,MSG,IL1,IL2,LAL,WRK,HMNC,QC,QSTC,
     1                   ZF,PF,RRL,GRAV,CPRES,EPS1,EPS2,A,B,ISKIP)
C-----------------------------------------------------------------------
C     * OCT 24/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - IMPLICIT NONE WITH NECESSARY 
C     *                              PROMOTION TO WORK IN 32-BIT.
C     * MAY 04/2006 - M.LAZARE/    PREVIOUS VERSION FOR GCM15E:
C     *               K.VONSALZEN. - CALCULATION OF SATURATION VAPOUR
C     *                              PRESSURE MODIFIED TO WORK OVER
C     *                              COMPLETE VERTICAL DOMAIN, BY
C     *                              SPECIFYING LIMIT AS ES->P.
C     *                              THIS REQUIRES PASSING IN EPS2.
C     * FEB 06/2004 - M.LAZARE/    PREVIOUS VERSION TINCLD FOR GCM15D:
C     *               K.VONSALZEN/
C     *               N.MCFARLANE. 
C                                                   
C     * CALCULATES IN-CLOUD TEMPERATURE AND SATURATION WATER VAPOUR 
C     * MIXING RATIO FROM MOIST STATIC ENERGY AND TOTAL WATER MIXING 
C     * RATIO. IT IS REQUIRED THAT THE WATER VAPOUR MIXING RATIO
C     * FROM THE INPUT IS LARGER THAN 0.
C-----------------------------------------------------------------------
      IMPLICIT NONE
C     
C     * MULTI-LEVEL WORK FIELDS. 
C
      REAL*8,  DIMENSION(ILG,ILEV)         :: TC,WRK,ZF,QSTC,PF,HMNC,QC
C     
C     * SINGLE-LEVEL WORK FIELDS. 
C      
      INTEGER, DIMENSION(ILG)              :: LAL,ISKIP
C
C     * SCALAR QUANTITIES PASSED IN.
C
      REAL    RRL,GRAV,CPRES,EPS1,EPS2,A,B
      INTEGER ILG,ILEV,MSG,IL1,IL2
C
C     * INTERNAL SCALAR QUANTITIES.
C
      REAL*8  AWRK,EST,ESTREF,QSTTMP,QTEMP,TTEMP,HMNPR,DHMNPR,ETMP
      REAL*8  TMIN,TMAX
      REAL    EPSLIM
      INTEGER L,IL
C
C     * LOCAL DATA CONSTANTS.
C
      DATA TMIN / 150. /
      DATA TMAX / 400. /
C-----------------------------------------------------------------------
C     * INITIALIZE CLOUD BASE VALUES BASED ON UNSATURATED 
C     * CONDITIONS.
C
      DO 60 L=MSG+2,ILEV 
      DO 60 IL=IL1,IL2 
         IF ( ISKIP(IL).EQ.0 ) THEN
            QSTC(IL,L)=QC(IL,L)
            WRK(IL,L)=LOG(QSTC(IL,L))
            TC(IL,L)=B/( A-LOG(PF(IL,L)/( EPS1/QSTC(IL,L)+1. )) )
         ENDIF
  60  CONTINUE
C
C     * CALCULATE TEMPERATURE AND SATURATION MIXING RATIO USING NEWTON-
C     * RAPHSON'S ITERATIVE METHOD UNDER SATURATED CONDITIONS. THE 
C     * ITERATIONS ARE PERFORMED FOR LOG(Q*) AS ITERATION VARIABLE IN 
C     * ORDER TO ENSURE POSITIVE DEFINITENESS AND CONVERGENCE OF THE 
C     * METHOD. 
C
      EPSLIM=0.001
      DO 80 L=ILEV,MSG+2,-1 
      DO 80 IL=IL1,IL2 
         IF ( L.LE.LAL(IL) .AND. ISKIP(IL).EQ.0 ) THEN
            AWRK=( HMNC(IL,L)-RRL*QC(IL,L)-GRAV*ZF(IL,L) )/CPRES
            AWRK=MIN(MAX(AWRK,TMIN),TMAX)
            EST=EXP(A-B/AWRK)
            ESTREF=PF(IL,L)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
            IF ( EST .GE. ESTREF ) THEN
C
C              *** NO SATURATION.
C
               TC(IL,L)=AWRK
               QSTC(IL,L)=1.-EPSLIM
            ELSE
               QSTTMP=EPS1*EST/(PF(IL,L)-EST)
               IF ( QSTTMP.GE.QC(IL,L) ) THEN
C
C                 *** NO SATURATION.
C
                  TC(IL,L)=AWRK
                  QSTC(IL,L)=QSTTMP
               ELSE
C
C                 *** FIRST ITERATION.
C
                  QTEMP=QSTC(IL,L)
                  TTEMP=TC(IL,L)
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** SECOND ITERATION.
C
                  QSTTMP=EPS1*EST/(PF(IL,L)-EST)
                  QTEMP=MAX(EXP(WRK(IL,L)),QSTTMP)
                  TTEMP=B/( A-LOG(PF(IL,L)/(EPS1/QTEMP+1.)) )
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** THIRD ITERATION.
C
                  QTEMP=MAX(EXP(WRK(IL,L)),QSTTMP)
                  TTEMP=B/( A-LOG(PF(IL,L)/(EPS1/QTEMP+1.)) )
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** RETRIEVE SATURATION WATER MIXING RATIO AND 
C                 *** TEMPERATURE.
C
                  QSTC(IL,L)=MAX(EXP(WRK(IL,L)),QSTTMP)
                  IF ( QSTC(IL,L).GT.0. ) THEN
                     TC(IL,L)=B/(A-LOG(PF(IL,L)/(EPS1/QSTC(IL,L)+1.)))     
                  ELSE
                     TC(IL,L)=0.
                  ENDIF
               ENDIF
            ENDIF
C
C           *** ALLOW ONLY TEMPERATURES BETWEEN TMIN AND TMAX.
C
            TC(IL,L)=MIN(MAX(TC(IL,L),TMIN),TMAX)
            ETMP=EXP(A-B/TC(IL,L))
            IF ( ETMP.LT.ESTREF ) THEN
              EST=ETMP
            ELSE
              EST=ESTREF
            ENDIF
            QSTC(IL,L)=EPS1*EST/(PF(IL,L)-EST)
         ENDIF
  80  CONTINUE
C
      RETURN
      END
