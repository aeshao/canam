      SUBROUTINE EVALSC3(T,Q,Z,P,DZ,MB,QFXROW,TFXROW,MAXI,LPBL,
     1                  ICLOS,ISHALL,ILG,ILGG,IL1,IL2,ILEV,MSG,GRAV,
     2                  EPS1,RGAS,RGASV,RGOCP,TVFA,WST)
C-------------------------------------------------------------------------
C     * MAY 27/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - NOW USES INTERNAL WORK ARRAYS
C     *                              INSTEAD OF PASSED WORKSPACE.    
C     *                            - "ZERO" DATA CONSTANT ADDED AND USED
C     *                              IN CALL TO INTRINSICS.
C     * DEC 12/05 - K.VONSALZEN.   PREVIOUS VERSION EVALSC2 FOR GCM15D/GCM15E:
C     *                            - WSTAR PASSED OUT TO PHYSICS (AS WST,
C     *                              BECOMING WSTAR IN PHYSICS, FOR USE IN
C     *                              GUSTINESS PARAMETERIZATION IN PHYSICS
C     *                              DRIVER.
C     * MAY 05/03 - K.VONSALZEN.   FINAL FROZEN VERSION EVALSC FOR GCM15B/C.
C     *                            CORRECT PROBLEMS WITH MOISTURE AND
C     *                            ENERGY CONSERVATION (I.E. AIR DENSITY).
C     * SEP 09/02 - K. VON SALZEN. CLOUD-BASE MASS FLUX USES 0.03
C     *                            INSTEAD OF 0.045. 
C     * JUN 13/01 - K. VON SALZEN. TUNED VERSION FOR GCM15.
C     * MAY 16/01 - K. VON SALZEN. EXPRESSION FOR MB NOW USES 0.045*...
C     *                            INSTEAD OF 0.03*...
C     * OCT  3/00 - K. VON SALZEN. REVISED AND ADAPTED FOR GCM.
C     * NOV 24/99 - K. VON SALZEN/ TEST VERSION FOR BOMEX.
C                   N. MCFARLANE.
C
C     * CALCULATES CLOUD-BASE MASS FLUX IN SHALLOW CUMULUS CLOUD
C     * ENSEMBLE AND OTHER PARAMETERS NEEDED IN SUBROUTINE TRANSC.
C     * PARAMETERIZATION OF CLOUD-BASE MASS FLUX ACCORDING TO
C     * GRANT (2001) FROM PBL INSTABILITY.
C-------------------------------------------------------------------------
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C      
C     * MULTI-LEVEL I/O FIELDS.
C     
      REAL  , DIMENSION(ILG,ILEV)  :: T,Z,Q,P,DZ
C
C     * SINGLE-LEVEL I/O FIELDS.
C
      REAL  , DIMENSION(ILG)  :: MB,QFXROW,TFXROW,WST
      INTEGER,DIMENSION(ILG)  :: MAXI,ICLOS,LPBL,ISHALL 
C
C     * INTERNAL WORKSPACE.
C
      REAL  , DIMENSION(ILG)  :: TVPPBL,DPBL
      INTEGER,DIMENSION(ILG)  :: LPTMP
C
      DATA ZERO   / 0.    /
C-------------------------------------------------------------------------
C
C     * INITIALIZATIONS.
C
      DO 20 IL=IL1,IL2                      
         MAXI(IL)  =ILEV
         LPTMP(IL) =ILEV
         MB(IL)    =0.
         TVPPBL(IL)=0.
         DPBL(IL)  =0.
         ICLOS(IL) =1
  20  CONTINUE
C
C     * PUT UPPER BOUND ON PBL HEIGHT.
C
      DO 70 L=ILEV,MSG+1,-1
      DO 70 IL=IL1,IL2
         IF ( Z(IL,L).LT.2000. ) THEN
            LPTMP(IL)=L
         END IF
  70  CONTINUE
      DO 80 IL=IL1,IL2
         LPTMP(IL)=MAX(LPBL(IL),LPTMP(IL))
  80  CONTINUE
C
C     * MEAN VIRTUAL POTENTIAL TEMPERATURE IN PBL.
C
      DO 140 L=ILEV,MSG+1,-1
      DO 140 IL=IL1,IL2
         IF ( L.GE.LPTMP(IL) ) THEN 
            TVP=T(IL,L)*(1.+TVFA*Q(IL,L))*(1000./P(IL,L))**RGOCP
            TVPPBL(IL)=TVPPBL(IL)+TVP*DZ(IL,L)
            DPBL(IL)=DPBL(IL)+DZ(IL,L)
         ENDIF
 140  CONTINUE
      DO 150 IL=IL1,IL2
         TVPPBL(IL)=TVPPBL(IL)/DPBL(IL)
 150  CONTINUE
C
C     * CALCULATE W* FROM BUOYANCY FLUX AT SURFACE AND
C     * USE THIS TO GET THE CLOUD BASE MASS FLUX MB.
C
      DO 160 IL=IL1,IL2
         L=LPTMP(IL)
         ZRHO=100.*P(IL,L)/(RGAS*T(IL,L))
         DRDQV=(1.+Q(IL,ILEV))**2
         RFX=QFXROW(IL)*DRDQV
         TPOT=T(IL,ILEV)*(1000./P(IL,ILEV))**RGOCP
         VPTFLX=-TFXROW(IL)*(1.+TVFA*Q(IL,ILEV))-TVFA*RFX*TPOT
         WSTAR=MAX(GRAV*VPTFLX*DPBL(IL)/TVPPBL(IL),ZERO)**(1./3.)
         WST(IL)=WSTAR
         MB(IL)=0.03*ZRHO*WSTAR
         IF ( MB(IL).EQ.0. ) ICLOS(IL)=0
 160  CONTINUE
C
C     * DETERMINE GRID POINTS AT WHICH THE SITUATION PERMITS 
C     * SHALLOW CONVECTION TO OCCUR.
C
      JYES=0
      JNO =IL2-IL1+2
      DO 200 IL=IL1,IL2
         IF ( ICLOS(IL).EQ.1 ) THEN
            JYES        =JYES+1
            ISHALL(JYES)=IL
         ELSE
            JNO         =JNO -1
            ISHALL( JNO)=IL
         ENDIF
  200 CONTINUE
      ILGG=JYES
C
      RETURN
      END
