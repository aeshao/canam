      SUBROUTINE VMRCO2(CO2VF,XVF,CCO2,KMX)

C***********************************************************************
C*                                                                     *
C*              SUBROUTINE vmrCO2                                      *
C*                                                                     *
C***********************************************************************

c to determine a vertical profile of the CO2 volume mixing ratio (vmr)
c by scaling the "standart" CO2 vmr profile using the ratio of the CO2
c vmr in the troposphere.
c                                             V. Fomichev, November, 1997
c C Called by MAMRAD
C Calls A18LIN

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

c INPUT: XVF - vertical array of scale heights
c        CCO2 - CO2 vmr in the troposphere

      real   xvf(kmx) 

c OUTPUT: CO2VF

      real   co2vf(kmx)

c the standard CO2 vmr profile
      real   xo(82), CO2o(82)

c internal array:
      real   aCO2o(82)

      data (xo(i), i=1,82)/
     & 0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75,
     & 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5, 4.75, 5.0, 5.25, 5.5, 5.75,
     & 6.0, 6.25, 6.5, 6.75, 7.0, 7.25, 7.5, 7.75, 8.0, 8.25, 8.5, 8.75,
     & 9.0, 9.25, 9.5, 9.75,10.0,10.25,10.5,10.75,11.0,11.25,11.5,11.75,
     &12.0,12.25,12.5,12.75,13.0,13.25,13.5,13.75,14.0,14.25,14.5,14.75,
     &15.0,15.25,15.5,15.75,16.0,16.25,16.5,16.75,17.0,17.25,17.5,17.75,
     &18.0,18.25,18.5,18.75,19.0,19.25,19.5,19.75,20.0,21.5/
      data (CO2o(i), i=1,82)   / 50*3.600E-04,
     & 3.58E-04, 3.54E-04, 3.50E-04, 3.41E-04, 3.28E-04, 3.11E-04,
     & 2.93E-04, 2.75E-04, 2.56E-04, 2.37E-04, 2.18E-04, 1.99E-04,
     & 1.80E-04, 1.61E-04, 1.42E-04, 1.24E-04, 1.06E-04, 9.00E-05,
     & 7.80e-05, 6.80e-05, 5.90e-05, 5.10e-05, 4.40e-05, 3.70e-05,
     & 3.00e-05, 2.40e-05, 1.90e-05, 1.40e-05, 1.00e-05, 7.00e-06,
     & 5.00e-06, 1.00e-10/

c to scale the CO2 vmr by CCO2 and take a logarithm

      do 1 k = 1, 82
      aCO2o(k) = log(CO2o(k)*CCO2/CO2o(1))
    1 continue

c to determine CO2 vmr at the model mid-layer levels

      do 2 k=1,KMX
        x = xvf(k)
        if(x.gt.21.5) then
          CO2vf(k) = exp(aCO2o(82))
        else
          CO2vf(k) = exp(a18lin(x,xo,aCO2o,1,82))
      end if
    2 continue

      return
      end
