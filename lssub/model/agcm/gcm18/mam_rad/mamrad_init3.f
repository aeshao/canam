      SUBROUTINE MAMRAD_INIT3(xvf, pvf, o2vf, cn2vf, ovf, amuvf, 
     1                        cpvf, gvf, co2vf,
     2                        effh, effsrc, to2, effo2nir,
     3                        x1chem, x2chem, dxchem, wchem, k1chem,
     4                        wwvs2, wwvs3, wco2s, wco2st,
     5                        wwvl3, wch4l, wo3l, k1co2s, k2co2s,
     6                        pdr, pcd, idr, icd,
     7                        sg,ptoit,co2_ppm,ilev)
C
C     * APR 30/2011 - M.LAZARE. NEW VERSION FOR GCM15J, BASED
C     *                         ON MAMRAD_INIT:
C     *                         - MODIFIED TO INTERFACE WITH
C     *                           CK SCHEME AND VICTOR'S MODIFIED
C     *                           MIDDLE ATMOSPHERE CODE.
C 
C     * THIS ROUTINE IS CALLED IN SECTION 0 OF THE MODEL AND 
C     * CALCULATES THE 1-D, INVARIANT DATA FOR RADIATIVE COOLING/
C     * HEATING IN THE MIDDLE ATMOSPHERE.
C
C     * INPUT DATA FOR MAMRAD2 (DETERMINED in THERMDAT2,VMRCO2 AND PRECL2).
C
      IMPLICIT NONE
C
      REAL PTOIT,CO2_PPM
      INTEGER ILEV,L
C
c     * output fields:
c
      real  , dimension(3,ilev):: pcd
      real  , dimension(3,67)  :: pdr
      real  , dimension(ilev)  :: xvf,pvf,o2vf,cn2vf,ovf,amuvf,
     1                            cpvf,gvf,co2vf,effh,effsrc,to2,
     2                            effo2nir
      real  , dimension(ilev)  :: wwvs2,wwvs3,wco2s,wco2st,wwvl3,
     1                            wch4l,wo3l,wchem
      integer,dimension(ilev)  :: icd
      integer,dimension(67)    :: idr
      real x1chem, x2chem, dxchem
      integer k1chem, k1co2s, k2co2s
C
c     * input fields.
c
      real sg(ilev)
C========================================================================
C     * DETERMINE ATMOSPHERIC COMPOSITION USED TO CALCULATE THE RADIATIVE
C     * HEATING/COOLING IN THE MIDDLE ATMOSPHERE. NOTE THAT 1-D FIELDS
C     * ARE USED BECAUSE AT HIGH ENOUGH ALTITUDES, MODEL SURFACES ARE
C     * ESSENTIALLY CONSTANT-PRESSURE INDEPENDANT OF LOCATION. 
C
      CALL THERMDAT2(xvf, pvf, O2vf, CN2vf, Ovf, AMuvf, 
     1               Cpvf, Gvf, 
     2               effH, effSRC, tO2, effO2nir,
     3               X1chem, X2chem, DXchem, WCHEM, K1CHEM,
     4               WWVS2, WWVS3, WCO2S, WCO2ST,
     5               WWVL3, WCH4L, WO3L, K1CO2S, K2CO2S,
     6               SG, ILEV)
      CALL DPMCO2
      CALL PCO2NIR
      CALL PRECL2(pdr, pcd, idr, icd, xvf, ilev)
      CALL VMRCO2(co2vf,xvf,co2_ppm,ilev)
      CALL CCO2GR(co2_ppm)
C
      RETURN
      END
