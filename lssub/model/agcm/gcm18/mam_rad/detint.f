      SUBROUTINE DETINT(X,XN,N,Z,Z1,Z2,K)
C***********************************************************************
C*                                                                     *
C*              SUBROUTINE DETINT                                      *
C*                                                                     *
C***********************************************************************
C
C to calculate the arrays of second order interpolation coefficients
C      from XN(N) grid to X level.  V. Fomichev, July 17, 1992.
C
C Called by PRECL
C Calls nothing

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C input:   X, XN(N)

      REAL   XN(N)

      K=1
      L1=N-1
    1 I=INT((K+L1)/2.)
      IF(I.EQ.K) GOTO 4
      IF(X-XN(I)) 2,3,3
    2 L1=I
      GOTO 1
    3 K=I
      GOTO 1
    4 CONTINUE

C output parameter  K   has been found:    XN(K) .LE. X .LT. XN(K+1)

      A=(X-XN(K+1))/(XN(K)-XN(K+2))
      A1=((X-XN(K))/(XN(K)-XN(K+1)))*(1.+A)
      A2=((X-XN(K))/(XN(K+1)-XN(K+2)))*A

C output: Z1, Z2, Z3 - interpolation coefficients (weight) for
C                     for XN(K),XN(K+1),XN(K+2) levels, correspondly.

      Z=1.+A1
      Z1=-A1-A2
      Z2=A2
      RETURN
      END
