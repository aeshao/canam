      SUBROUTINE O2STROB(HEAT,RMU0,PMB,TO2,EFFSRC,O2VF,AMUVF,CPVF,
     1                   KMX,KMXP, LMX,IL1,IL2)

C***********************************************************************
C*                                                                     *
C*              SUBROUTINE O2STROB                                     *
C*                                                                     *
C***********************************************************************
C
c to calculate the solar heating due to absorption in Schumann-Runge
c continuum (SRC) and Schumann-Runge bands by the method of Strobel (JRG,
c vol 83, p 6225, 1978) with taking into account the heating efficiency for
c SRC from Mlynczack&Solomon (JGR, vol 98, p 10517, 1993) (efficiency for SRB
c is unit).
c The SRC heating efficiency and O2 column amount is calculated in THERMDAT
c subroutine and passed through common block THERSH (arrays: effSRC and tO2)
c                                             V. Fomichev, November, 1997

C Called by MAMRAD
C Calls nothing

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   HEAT(LMX,KMX), PMB(LMX,KMXP), RMU0(LMX)
      REAL   TO2(KMX), EFFSRC(KMX),O2VF(KMX),AMUVF(KMX),CPVF(KMX)
C========================================================================
      do 20 i = 1, KMX
        do 10 IL = IL1, IL2
          heatO2 = 0.0
          if (RMU0(IL) .gt. 0.0) THEN
       
c           * O2 amount: downward looking paths

            g=tO2(i)*35./SQRT(1224.0*RMU0(IL)*RMU0(IL)+1.)

c           * SRC:

            SRC1=2.716E6*exp(-1.E-17*g)
            SRC2=(5.902E23*exp(-2.9E-19*g)-3.312E23*exp(-1.7E-18*g)
     1           -2.590E23*exp(-1.15E-17*g))/g
            SRC = (SRC1+SRC2)*effSRC(i)

c           * SRB:

            SRB=1.463E5
            if(g.gt.1.e18) SRB=1./(1.113E-24*g+5.712E-15*sqrt(g))

c           * O2 heating (K/s):

            heatO2=O2vf(i)*(SRC+SRB)/AMuvf(i)/Cpvf(i)
          endif

c         * total heating (O2 heating is added to the values from
c         * HEAT(KMX,LMX) array)

          HEAT(IL,I) = HEAT(IL,I) + heatO2

   10   continue
   20 continue

      return
      end
