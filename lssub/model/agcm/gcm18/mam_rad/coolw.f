      SUBROUTINE COOLW(CW,CmH2O,T,AMUVF,PVF,KMX,LMX,IL1,IL2)

C****************************************************************************
C*                                                                          *
C*                       SUBROUTINE  COOLW                                  *
C*                                                                          *
C****************************************************************************
C
C to calculate cooling rate due to rotational H2O band (cooling-to-space
C approximation, LTE).
C Method used: Gordiets & Markov (1982), desribed by Fomichev et al. (JATP,
C 1986, vol. 48, No 6, pp 529-544).
C                                          V Fomichev, October, 1999
C Called by MAMRAD
C Calls nothing

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C
C OUTPUT:
C CW - cooling rate in K/sec*(specific heat)

      real   CW(LMX,KMX)

C INPUT (all input data are specified for a level in question):
C CmH2O - H2O mass mixing ratio
C T - temperature (K)

      real   CmH2O(LMX,KMX), T(LMX,KMX)
      REAL   AMUVF(KMX),PVF(KMX)
C----------------------------------------------------------------------
C     * INITIALIZE.
C
      DO 5 K=1,KMX
      DO 5 IL=IL1,IL2
        CW(IL,K)=0.
    5 CONTINUE
C
      do 10 k=1,KMX
      do 20 il = IL1,IL2

      T2 = T(il,k)*T(il,k)

c to define the optical depth:
      ta = 1.096E12*AMuvf(k)*pvf(k)*CmH2O(il,k)/T2

c escape function:
      alta = log(ta)
      if (alta.le.1) then
        FL=1.           ! to avoid dividing by zero... L>1 in this case 
      else
        alta = log(ta*alta)
        f1 = 373.1*sqrt(pvf(k)/(ta*T(il,k)*sqrt(T(il,k))))
        f2 = 1.2/ta*(1.2+.5*alta+1.2/alta)
        FL = f1+f2
        if(FL.gt.1) FL = 1.
      end if

c cooling rate:
      CW(il,k) = 2007.*CmH2O(il,k)*T2*FL

   20 continue
   10 continue

      return
      end
