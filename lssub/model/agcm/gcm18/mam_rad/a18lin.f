      FUNCTION A18LIN(X,XN,YN,M,N)

C****************************************************************************
C*                                                                          *
C*                            FUNCTION A18LIN                               *
C*                                                                          *
C****************************************************************************
C
C linear interpolation
C
C called by CCO2GR
C calls nothing
C
C input:
C  X - argument for which a value of function should be found
C  XN(N),YN(N) - values of function YN(N) at XN(N) grid. X(N) should be
C                ordered so that X(I-1) < X(I).
C output:
C  A18LIN - value of function for X


      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      real   XN(N),YN(N)

      k=m-1
      do 1 i=m,n
      k=k+1
      if(x-xn(i)) 2,2,1
    1 continue
    2 if(k.eq.1) k=2

c k has been found so that xn(k).le.x.lt.xn(k+1)

      A18LIN=(yn(k)-yn(k-1))/(xn(k)-xn(k-1))*(x-xn(k))+yn(k)
      return
      end
