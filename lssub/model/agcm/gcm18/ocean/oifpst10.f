      SUBROUTINE OIFPST10(GT,SNO,SIC,ZSNOW,ALBS,
     1                    GC,GTA,HSEA,HSENS,HLAT,
     2                    FSG,FLG,SNOM,WTRG,WTRS,
     3                    TISL,DENS,FFLXT,SNOFAC,
     4                    DELT,ILG,IS,IF)
C.......................................................................
C     * AUG 17/18 - M.LAZARE.    SNOLIM lowered from effective 1200. 
C     *                          down to 160.
C     * AUG 12/18 - M.LAZARE.    CLEANUP: REMOVE UNUSED DELICE (PASSED IN)
C     *                          AND UNUSED SCALARS DELSIC AND DMELT.
C     * APR 12/16 - M.LAZARE.    NEW VERSION FOR GCM19+:
C     *                          - ELIMINATE "SICMIN" USE (BUG, LOWER
C     *                            BOUND SHOULD BE ZERO!).
C     *                          - RUNOFF REMOVED.
C     *                          - {BEGO,BWGO,FFLXB,CBGO} CALCULATIONS
C     *                            REMOVED AND ALSO IN CALL (OLD OCEAN). 
C     *                          - UNUSED {PRESS,TS,QS} REMOVED.
C     *                          - CALLED FOR AMIP AND NEMO/CICE/LIM2,
C     *                            ON GATHERED GRID, WHILE OLD OIFPST9
C     *                            STILL CALLED (FOR NOW) AS BEFORE.
C     *                          - SECTIONS WITH ICEMOD
C     *                            REMOVED, SINCE ONLY CALLED FOR 
C     *                            "ICEFAC"=0. "IADJ" ALSO THUS REMOVED.
C     *                          - CALCULATIONS DONE OVER ICE TIME, SO
C     *                            "HSEA" CALCULATION SIMPLIFIED TO
C     *                            PACK ICE ONLY.
C     *                          - "ZERO" AND "ONE" REPLACED BY 0 AND 1,
C     *                            RESPECTIVELY.
C     *                          - "RES" REMOVED SINCE WAS IN ICEMOD>0
C     *                            SECTION IN PREVIOUS VERSIONS.
C     *                          - ALSO "SICN","JL","ILSL" REMOVED SINCE
C     *                            NO LONGER RELEVANT.
C     * MAR 12/15 - G.FLATO.     PREVIOUS VERSION OIFPST9 FOR GCM18.
C     *                          - "HOPEN" MODIFIED TO PROPERLY INCLUDE
C     *                            RESIDUAL.
C     * NOV 15/13 - M.LAZARE/    PREVIOUS VERSION OIFPST8 FOR GCM17:
C     *             M.NAMAZI.    - ADD CALCULATION OF "TISL".
C     *                          - SNOFAC PROMOTED TO ARRAY AND PASSED OUT.
C     *                          - CORRECT SNOM CALCULATION.
C     * APR 30/12 - M.LAZARE.    PREVIOUS VERSION OIFPST7 FOR GCM16:
C     *                          - PASS IN AND USE FLGROL, RATHER THAN
C     *                            FDLROL AND USING SIGMA*T**4 (MORE
C     *                            ACCURATE).
C     *                          - HSEA PASSED OUT AND USED IN PHYSICS,
C     *                            RATHER THAN LEFT AS INTERNAL, IN
C     *                            CASE IT IS NEEDED LATER.
C     * JUN 15/06 - M.LAZARE.    PREVIOUS VERSION OIFPST6 FOR GCM15F/G/H/I:
C     *                          - "ZERO","ONE" DATA CONSTANTS ADDED AND
C     *                            USED IN CALLS TO INTRINSICS.
C     *                          - USES INTERNAL WORK ARRAYS INSTEAD
C     *                            OF PASSING IN "WRK" WORKSPACE FROM
C     *                            PHYSICS.  
C     * JAN 22/05 - D.ROBITAILLE.PREVIOUS VERSION OIFPST5 FOR GCM15C/D/E: 
C     *                          - DEFAULT ISICN=1 NOW AND CONSISTENCY
C     *                            CHECK ADDED AT END OF LOOP 500
C     *                            BETWEEN SIC AND SICN. 
C     * NOV 01/04 - G.FLATO      - PREVIOUS VERSION OIFPST4 FOR GCM15B.
C
C     * PURPOSE:
C     * OVER SEA-ICE, EVALUATE THE GROUND ENERGY BALANCE, TAKING INTO 
C     * ACCOUNT SOLAR FLUX, ATM. AND TERRESTRIAL FLUXES, SENSIBLE AND 
C     * EVAPORATION HEAT LOSS, HEAT LOST TO UNDERLYING SEA WATER.
C     * METHOD: THERMAL-INERTIA.
C     * SCHEME: FORWARD 1-DELT STEP.
C
C     * THIS IS FOLLOWED BY A THERMODYNAMIC SEA-ICE MODEL (AFTER
C     * SEMTNER, JPO MAY, 1976), WHICH DETERMINES CHANGES TO ICE AND
C     * SNOW AMOUNTS OVER PACK ICE.
C     
C     * FINALLY, CHANGE IN ICE CONCENTRATION IS COMPUTED (AFTER HIBLER,
C     * JPO, JULY, 1979) BASED ON GROWTH OR MELT.
C
C---------------------------------------------------------------------- 
C     * DICTIONARY OF VARIABLES
C
C  ASIC  - initial sea ice amount in (kg m-2)
C  ASNO  - initial snow amount (water equivalent) in (kg m-2)
C  AGT   - initial ground temperature in (K)
C  BGT   - snow (ground) temperature (K) after surface melt.
C  BSIC  - sea ice amount (kg m-2) after sublimation/deposition but
C          before surface melt
C  BSNO  - snow amount (kg m-2) after sublimation/deposition but
C          before surface melt
C  C     - SQRT(2) times the 'average' heat capacity of the upper snow/ice
C          surface layer (J m-2 K-1).
C  CICE  - SQRT(2) times CPACK (J m-2 K-1)
C  CONI  - thermal conductivity of ice (W m K-1)
C  CONS  - thermal conductivity of snow (W m K-1)
C  CPACK - heat capacity of 10cm 'upper layer' of pack ice (J m-2 K-1)
C  FLG   - net l/w flux at surface (W m-2)
C  FSG   - net solar flux absorbed at surface (W m-2)
C  DELE  - EVAPR * DELT - total evaporation over time step (kg m-2)
C  DELM  - equivalent heat available for surface melt (kg m-2)
C  DELT  - model time step (s)
C  DENI  - sea ice density (kg m-3)
C  DENS  - snow density (kg m-3)
C  DFSIC - thickness of 'thin' ice, specified in SPWCON8 (m)
C  EVAPR - ground evaporation rate (kg m-2 s-1)
C  FFLXT - fresh water flux into ocean due to surface melt (kg m-2)
C  FSG   - net absorbed solar flux at sfc (w m-2)
C  GC    - ground cover type (1.=pack ice, 0.=open water, -1.=land)
C  GT    - ground temperature (K)
C  GTA   - diurnal average ground temperature (K)
C  GTFSW - freezing point of sea water (K)
C  GTMSI - melting point of sea ice = TFREZ - 0.1 (K)
C  HF    - latent heat of fusion (J kg-1) of snow or sea ice = HS-HV
C  HLAT  - latent heat flux (W m-2)
C  HS    - latent heat of sublimation (J kg-1)
C  HV    - latent heat of vaporization (J kg-1)
C  HSEA  - conductive heat flux up through sea ice (W m-2)
C  HSENS - sensible heat flux (W m-2)
C  SIC   - sea ice amount (kg m-2)
C  SNO   - snow amount (kg m-2)
C  SNOFAC- fraction of snow on seaice
C  SNOM  - snow melting rate (water equivilent) (kg m-2 s-1)
C  TFREZ - freezing temperature of fresh water or melting point of snow (K)
C  WTRG  - rate of increase of mass of sea-ice due to compacting of snow
C          (kg m-2 s-1)
C  WTRS  - rate of decrease of mass of snow due to compacting into sea-ice
C          (kg m-2 s-1)
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * OUTPUT FIELDS:
C
      REAL  , DIMENSION(ILG)  :: GT,SNO,SIC,ZSNOW,ALBS,
     1                           SNOM,WTRG,WTRS,
     2                           HSEA,TISL,SNOFAC,DENS,FFLXT
C
C     * INPUT FIELDS:
C
      REAL  , DIMENSION(ILG)  :: GC,GTA,HSENS,HLAT,FSG,FLG
C
C     * INTERNAL WORK FIELDS.
C
      REAL  , DIMENSION(ILG)  :: C,EVAPR,DELE,DELM,ASIC,ASNO,AGT,
     1                           BSIC,BSNO,BGT,CSNOW,ZICE   
C
      COMMON /PARAMS/WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1               RGASV
      COMMON /PARAM1/PI,RVORD,TFREZ,HS,HV,DAYLNT
      COMMON /PARAM3/CSNO,CPACK,GTFSW,RKHI,SBC,SNOMAX 
      COMMON /PARAM5/CONI,DENI,XXX,DFSIC,CONF 
C-------------------------------------------------------------------
C
      SNOLIM=160.
      DO 100 I=IS,IF
        IF(GC(I).GT.0.5)                        THEN
C
C         * INITIALIZE VARIABLES.
C
          ASIC(I)=SIC(I)
          ASNO(I)=SNO(I)
          AGT (I)=GT (I)
C
C         * FRACTION OF SNOW COVER IN GRID SQUARE OVER SEA-ICE.
C
          SNOFAC(I)=MIN(1.,SQRT(SNO(I)/SNOMAX))
C 
C         * HEAT CAPACITY OF PACK ICE (NORMALIZED BY SQRT(2)).
C 
          C(I)=CPACK*SQRT(2.) 
C 
C         * HEAT FLUX THROUGH PACK ICE FROM UNDERLYING SEA WATER (HSEA). 
C         * SEE SUBROUTINE OIFPRP5 FOR SNOW DENSITY JUSTIFICATION COMMENTS. 
C         * CONVERT ANY SNOW MASS HAVING DENSITY GREATER THAN DENI TO 
C         * SEA-ICE (BASED ON USUAL EXPONENTIAL FORMULA WITH A=0.54 AND 
C         * SURFACE DENSITY OF 275 KG/M3).
C 
          IF(SNO(I).GT.SNOLIM) THEN 
            SIC(I)=SIC(I)+(SNO(I)-SNOLIM)
            SNO(I)=SNOLIM
            WTRG(I)=WTRG(I)+(SNO(I)-SNOLIM)/DELT
            WTRS(I)=WTRS(I)-(SNO(I)-SNOLIM)/DELT
          ENDIF
!          IF(SNO(I).GT.SNOMAX) THEN 
!            DENS(I)=0.54*SNO(I)/LOG(1.+0.54*SNO(I)/275.)
!          ELSE 
!            DENS(I)=275. 
!          ENDIF
! Set density of snow to 330 kg/m^3 to match that in LIM2...
          DENS(I) = 330.0
          CONS=2.805E-6*DENS(I)**2
          CSNOW(I)=CONS      
          RCON=CONI/CONS 
          DSNO=SNO(I)/DENS(I)
          ZSNOW(I)=DSNO 
          DSIC=SIC(I)/DENI
          DSIC=MAX(DSIC,DFSIC)
          ZICE(I)=DSIC       
          FACT=DSIC+RCON*DSNO
          HSEA(I)=CONI*(GTFSW-GTA(I))/FACT
          EVAPR(I)=HLAT(I)/HS
C 
C         * TIME STEP THE GROUND TEMPERATURE. 
C
          GT(I)=GT(I)+(FSG(I)+FLG(I)-HSENS(I)-HLAT(I)+HSEA(I)) * 
     1                DELT/C(I)
        ENDIF
  100 CONTINUE
C==================================================================
C     * THERMODYNAMIC ICE MODEL (AFTER SEMTNER).
C
      NADJ=0
      HF=HS-HV     
      CICE=CPACK*SQRT(2.) 
      GTMSI=TFREZ-0.1 

      DO 200 I=IS,IF
        IF(GC(I).GT.0.5)                                     THEN
C     
C         * INITIALIZE VARIABLES.
C
          FFLXT(I)=0.
          DELE(I)=0. 
          DELM(I)=0. 
C 
C         * SUBLIMATION/DEPOSITION. 
C 
          DELE(I)=EVAPR(I)*DELT 
          IF(SNO(I).GT.DELE(I))         THEN
            SNO(I)=SNO(I)-DELE(I) 
          ELSE
            SIC(I)=SIC(I)-(DELE(I)-SNO(I)) 
            SNO(I)=0. 
          ENDIF 
          SIC(I)=MAX(SIC(I),0.) 
          BSIC(I)=SIC(I)
          BSNO(I)=SNO(I)  
        ENDIF
  200 CONTINUE
C 
C     * CALCULATE MELTING IF GT IS GREATER THAN FREEZING 
C     * AND ADJUST GT FOR LATENT HEAT LOSS. 
C
      DO 300 I=IS,IF
        IF(GC(I).GT.0.5)                THEN 
          IF(GT(I).GT.TFREZ .AND. SNO(I).GT.0.0) THEN 
C 
C           * WHEN THERE IS SNOW THE SURFACE TEMPERATURE MUST BE ABOVE 
C           * THE MELTING POINT OF SNOW (TFREZ) FOR MELTING TO OCCUR.
C           * IF ALL SNOW IS MELTED, THE RESULTING GT IS CALCULATED. 
C 
            DELM(I)=(GT(I)-TFREZ)*C(I)/HF 
            IF(SNO(I).GE.DELM(I)) THEN 
              SNO(I)=SNO(I)-DELM(I)
              GT(I)=TFREZ
            ELSE 
              GT(I)=GT(I)-HF*SNO(I)/C(I)
              SNO(I)=0.0 
            ENDIF
          ENDIF
          SNOM(I)=(BSNO(I)-SNO(I))/DELT
          BGT(I)=GT(I)
C 
          IF(GT(I).GT.GTMSI .AND. SNO(I).EQ.0.0) THEN 
C 
C           * WHEN THERE ISN'T ANY SNOW THE SURFACE TEMPERATURE NEEDS ONLY 
C           * BE ABOVE THE MELTING POINT OF ICE (GTMSI) FOR MELTING
C           * TO OCCUR.
C 
            DELM(I)=(GT(I)-GTMSI)*CICE/HF
            IF(DELM(I).LT.SIC(I)) THEN 
              SIC(I)=SIC(I)-DELM(I)
              GT(I)=GTMSI
            ELSE 
              GT(I)=GT(I)-HF*SIC(I)/CICE 
              SIC(I)=0.
            ENDIF
          ENDIF 
        ENDIF
  300 CONTINUE
C
C     * CALCULATE FRESH WATER FLUX DUE TO SURFACE MELT.
C
      DO 350 I=IS,IF
        IF(GC(I).GT.0.5)                        THEN 
          FFLXT(I)=((BSIC(I)-SIC(I))+(BSNO(I)-SNO(I)))/DELT
        ENDIF
  350 CONTINUE
C
      HFODT=HF/DELT
      DO 400 I=IS,IF
        IF(GC(I).GT.0.5)                THEN 
C
C         * RESET SEA-ICE MASS TO INTERPOLATED VALUE COMING INTO
C         * THIS ROUTINE. 
C         * RESET FFLXT TO INCLUDE ONLY SNOWMELT FOR CORRECT DIAGNOSIS
C         * OF OBWG   --G.FLATO - 15/FEB/01
C
          FFLXT(I)=(BSNO(I)-SNO(I))/DELT
          SIC(I)=ASIC(I)
        ENDIF
  400 CONTINUE
C
C     * UPDATE SNOW ALBEDO.
C
      DO 600 I=IS,IF
        IF(GC(I).GT.0.5 .AND. SNO(I).GT.0.)                   THEN 
            IF(GT(I).GE.TFREZ .AND. ALBS(I).GT.0.50)      THEN                                           
              TIMFAC=EXP(LOG((ALBS(I)-0.50)/0.34)-                    
     1                 2.778E-6*DELT)                                      
              ALBS(I)=0.34*TIMFAC+0.50                                 
            ELSE IF(GT(I).LT.TFREZ .AND. ALBS(I).GT.0.70) THEN                                                            
              TIMFAC=EXP(LOG((ALBS(I)-0.70)/0.14)-                    
     1                 2.778E-6*DELT)                                      
              ALBS(I)=0.14*TIMFAC+0.70                                 
            ENDIF
        ENDIF 
  600 CONTINUE
C
      DO 800 I=IS,IF                   
        IF(GC(I).GT.0.5 .AND. SNO(I).GT.0.)   THEN                                         
          TISL(I)=(-CONI*ZSNOW(I)*GTFSW-CSNOW(I)*ZICE(I)
     1         *GT(I))/(-CONI*ZSNOW(I)-CSNOW(I)*ZICE(I))
        ENDIF 
  800 CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END
