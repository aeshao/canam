      SUBROUTINE OIFPRP10(SNO,EVAPF,QG,ALBS,                        !output
     1                    SNOW,GT,GC,PS,                            !input
     2                    DELT,ILG,IS,IF)
C
C     * May 06/14 - M.Lazare. New version for gcm18+:
C     *                       Based on oifprp9 but only calculations
C     *                       left are for EVAPF,QG, updating
C     *                       SNO with SNOW over seaice.
C     *                       The updating of RUNOFF with RAIN over
C     *                       seaice has been moved out to sfcproc2
C     *                       before the call to OIFPST9.
C     *                       All albedo calculations have
C     *                       been moved to SNOSICA for seaice and
C     *                       to OCNALB for open water.
C
C     * PERFORMS PRELIMINARY CALCULATIONS OVER OPEN WATER AND SEA-ICE.
C
      IMPLICIT NONE
C 
C     * OUTPUT OR I/O FIELDS:
C
      REAL   SNO    (ILG), EVAPF  (ILG), QG     (ILG), ALBS   (ILG)
C
C     * INPUT FIELDS:
C
      REAL   SNOW   (ILG), GT     (ILG), GC     (ILG), PS     (ILG)
C
      REAL DELT,FRACW
      INTEGER ILG,IS,IF,I

      REAL A,B,EPS1,EPS2,T1S,T2S,AI,BI,AW,BW,SLPE
      COMMON /EPS/ A,B,EPS1,EPS2               
      COMMON /HTCP  / T1S,T2S,AI,BI,AW,BW,SLPE               
C
C     * PARAMETERS USED IN NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      REAL RW1,RW2,RW3,RI1,RI2,RI3
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * IN-LINE FUNCTIONS:
C
C     * COMPUTES THE SATURATION VAPOUR PRESSURE OVER WATER OR ICE.             
C
      REAL TTT,UUU,ESW,ESI,ESTEFF,Q,EEE,P,EST
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
      ESTEFF(TTT,UUU) = UUU*ESW(TTT) + (1.-UUU)*ESI(TTT)
      Q(EEE,P,EPS1,EPS2)=EPS1*EEE/(P-EPS2*EEE)               
C-----------------------------------------------------------------------
      DO 100 I=IS,IF
C                                                                             
C       * GROUND EVAPORATION EFFICIENCY.                                       
C       * =1. OVER OPEN WATER AND PACK ICE, GC=0.,1.                          
C
        EVAPF(I)=1.                               
C
C       * SATURATION SPECIFIC HUMIDITY AT GROUND TEMPERATURE AND PRESSURE.
C       * COMPUTE THE FRACTIONAL PROBABILITY OF WATER PHASE EXISTING
C       * AS A FUNCTION OF TEMPERATURE (FROM ROCKEL,                  
C       * RASCHKE AND WEYRES, BEITR. PHYS. ATMOSPH., 1991.)                    
C
        FRACW = 1.
        IF(GT(I).LT.T1S)          THEN
          FRACW = 0.0059+0.9941*EXP(-0.003102*(T1S-GT(I))**2)
        ENDIF      
        EST=ESTEFF(GT(I),FRACW)
        QG(I)=Q(EST, .01*PS(I), EPS1, EPS2)                                
C
C       * UPDATE SNOW AMOUNT AND POSSIBLY SNOW ALBEDO.
C       * SNOW ACCUMULATES OVER PACK ICE.
C 
        IF(GC(I).EQ.1.)                                          THEN
          IF(SNOW(I).GT.0.)                                  THEN
            SNO(I)=SNO(I)+SNOW(I)*DELT
            ALBS(I)=0.84   
          ENDIF
        ENDIF
  100 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END 
