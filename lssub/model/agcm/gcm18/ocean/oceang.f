      SUBROUTINE OCEANG (SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     1                   REFGAT,  BCSNGAT, GCGAT,
     2                   SNOWGAT, TAGAT,
     3                   ULGAT,   VLGAT,   CSZGAT,  PRESGAT, 
     4                   FSGGAT,  FLGGAT,  QAGAT,   THLGAT,
     5                   VMODGAT, GUSTGAT,
     6                   ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT,
     +                   DEPBGAT,  SPCPGAT,  RHSIGAT,  PREGAT,
     7                   FSDGAT,  FSFGAT,  CSDGAT,  CSFGAT, 
     8                   WRKAGAT, WRKBGAT, CLDTGAT,
     9                   IWMOS,   JWMOS,
     A                   NMW,NL,NM,ILG,NBS,
     B                   SNOROT,  GTROT,   ANROT,   RHONROT, TNROT, 
     C                   REFROT,  BCSNROT, GCROT,
     D                   SNOWROW, TAROW,
     E                   ULROW,   VLROW,   CSZROW,  PRESROW, 
     F                   FSGROT,  FLGROT,  QAROW,   THLROW,
     G                   VMODL,   GUSTROL,
     H                   ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW,
     +                   DEPBROW,  SPCPROW,  RHSIROW,  PREROW,
     I                   FSDROT,  FSFROT,  CSDROT,  CSFROT,
     J                   WRKAROL, WRKBROL, CLDTROL)
C
C     * Aug 07, 2017 - M.Lazare. New Git version.
C     * Mar 03, 2015 - M.Lazare. New version called by sfcproc2
C     *                          in new model version gcm18. This
C     *                          is modelled on "classg" since
C     *                          ocean is now tiled as well.
C     *                          - ZRFM,ZRFH,ZDM,ZDH added.
C     *                          - SGL,SHL removed as no longer needed
C     *                            in OIFLUX11.
C     *                          - TISL,MASK removed and
C     *                            GT,GUST added.
C     *                          - WRKA,WRKB,CLDT added for ocean albedo 
C     *                            calculation.
C     *                          - DSIC,GTA,GTP,SIC,SICN removed.
C     *                          - RAIN,RES,ILSL,JL removed.
C     * Nov 15, 2013 - M.Lazare. Previous version "oceang" called by
C     *                          "sfcproc" in previous model version gcm17.
C     *                          This differs from the pre-Nov 15/2013
C     *                          version in that ROFN has been removed.
C     * NOTE: This contains the following changes compared to the
C     *       working temporary version used in conjunction with
C     *       updates to gcm16 (ie not official):
C     *         1) {TN,RHON,REF,BCSN,TISL} added for Maryam's
C     *            contribution to the new snow albedo formulation.
C     *         2) No more SNOROT,ANROT (SNOROW,ANROW still there)
C     *            since these are for land only.
C     *         3) {FSO,FSF,CSD,CSF| now 2-D arrays since defined
C     *            over 4 bands (hence NBS passed in).
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NMW,NL,NM,ILG,K,L,M,NBS,IOCEAN,IB
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM) ::
     1      SNOROT,  GTROT,   ANROT,   RHONROT, TNROT,   
     2      REFROT,  BCSNROT, GCROT, FSGROT, FLGROT

      REAL, DIMENSION(ILG)   ::
     1      SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     2      REFGAT,  BCSNGAT, GCGAT
C
C     * ATMOSPHERIC AND GRID-CONSTANT INPUT VARIABLES.
C
      REAL, DIMENSION(NL,NM,NBS) :: FSDROT, FSFROT, CSDROT, CSFROT
      REAL, DIMENSION(NL,NBS)    :: WRKAROL,WRKBROL

      REAL, DIMENSION(NL) ::
     1      SNOWROW, TAROW,   
     2      ULROW,   VLROW,   CSZROW,  CLDTROL,
     3      PRESROW, QAROW,
     4      THLROW,  VMODL,   GUSTROL, 
     5      ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW, DEPBROW,
     +      SPCPROW, RHSIROW, PREROW

      REAL, DIMENSION(ILG,NBS) :: FSDGAT, FSFGAT, CSDGAT, CSFGAT,
     1                            WRKAGAT,WRKBGAT

      REAL, DIMENSION(ILG) :: 
     1      SNOWGAT, TAGAT,   
     2      ULGAT,   VLGAT,   CSZGAT,  CLDTGAT,
     3      PRESGAT, FSGGAT,  FLGGAT,  QAGAT, 
     4      THLGAT,  VMODGAT, GUSTGAT, 
     5      ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT, DEPBGAT,
     +      SPCPGAT, RHSIGAT, PREGAT
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IWMOS (ILG),  JWMOS  (ILG)
C--------------------------------------------------------------------
      DO 100 K=1,NMW
          SNOGAT (K)=SNOROT (IWMOS(K),JWMOS(K))  
          GTGAT  (K)=GTROT  (IWMOS(K),JWMOS(K))  
          ANGAT  (K)=ANROT  (IWMOS(K),JWMOS(K))  
          RHONGAT(K)=RHONROT(IWMOS(K),JWMOS(K))  
          TNGAT  (K)=TNROT  (IWMOS(K),JWMOS(K))  
          REFGAT (K)=REFROT (IWMOS(K),JWMOS(K))  
          BCSNGAT(K)=BCSNROT(IWMOS(K),JWMOS(K))  
          GCGAT  (K)=GCROT  (IWMOS(K),JWMOS(K))
          FSGGAT (K)=FSGROT (IWMOS(K),JWMOS(K))
          FLGGAT (K)=FLGROT (IWMOS(K),JWMOS(K))
  100 CONTINUE
C
      DO 200 K=1,NMW
          SNOWGAT  (K)=SNOWROW  (IWMOS(K))
          TAGAT    (K)=TAROW    (IWMOS(K))
          ULGAT    (K)=ULROW    (IWMOS(K))
          VLGAT    (K)=VLROW    (IWMOS(K))
          CSZGAT   (K)=CSZROW   (IWMOS(K))
          CLDTGAT  (K)=CLDTROL  (IWMOS(K))
          PRESGAT  (K)=PRESROW  (IWMOS(K))
          QAGAT    (K)=QAROW    (IWMOS(K))
          THLGAT   (K)=THLROW   (IWMOS(K))
          VMODGAT  (K)=VMODL    (IWMOS(K))    
          GUSTGAT  (K)=GUSTROL  (IWMOS(K))
          ZRFMGAT  (K)=ZRFMROW  (IWMOS(K))
          ZRFHGAT  (K)=ZRFHROW  (IWMOS(K))
          ZDMGAT   (K)=ZDMROW   (IWMOS(K))
          ZDHGAT   (K)=ZDHROW   (IWMOS(K))
          DEPBGAT  (K)=DEPBROW  (IWMOS(K))
          SPCPGAT  (K)=SPCPROW  (IWMOS(K))
          RHSIGAT  (K)=RHSIROW  (IWMOS(K))
          PREGAT   (K)=PREROW   (IWMOS(K))
  200 CONTINUE
C
      DO 300 IB=1,NBS
      DO 300 K=1,NMW
            FSDGAT (K,IB) = FSDROT (IWMOS(K),JWMOS(K),IB)
            FSFGAT (K,IB) = FSFROT (IWMOS(K),JWMOS(K),IB)
            CSDGAT (K,IB) = CSDROT (IWMOS(K),JWMOS(K),IB)
            CSFGAT (K,IB) = CSFROT (IWMOS(K),JWMOS(K),IB)
            WRKAGAT(K,IB) = WRKAROL(IWMOS(K),IB)
            WRKBGAT(K,IB) = WRKBROL(IWMOS(K),IB)
  300 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
