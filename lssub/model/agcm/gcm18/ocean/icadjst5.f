      SUBROUTINE ICADJST5(SIC,SICN,SNO,OGT,GC,BWGO,FMI,ILSL,JL,
     1                    DELT,ILG,IL1,IL2)
C
C     * Jun 20/2013 - M.Lazare. New version for gcm17 (no change in answer):
C     *                         - Initialize FMI to zero.
C     *                         - Set SICN=0. if all snow/ice melted. 
C     * APR 23/2010 - M.LAZARE. Previous version icadjst4 for gcm15i,gcm16:
c     *                         - Mixed layer depth now 10m 
c     *                           instead of 50 m.
C     * MAY 29/2006 - M.LAZARE. Previous version icadjst3 for gcm15f/g/h
C     *                         which now has GCINIT as an internal work
C     *                         array.
C     * JAN 04/2005 - M.LAZARE/ Previous version ICADJST2:
C     *           D.ROBITAILLE. - Vectorized version which has a
C     *                           a 1-day timescale for icemelt/growth
C     *                           instead of 10 days.
C     * JUL 27/99 - S.TINIS.    Previous version ICADJST.
C     *                                        
C     * Subroutine to do initial ice adjustment in AGCM when coupled
C     * to MOM ocean model. If ocean target temperature is less than
C     * freezing, ice is grown and negative fresh water flux is
C     * calculated. If ocean target temperature is greater than
C     * freezing, ice is melted and positive fresh water flux is
C     * calculated
C
C********************************************************************
C
C     * Dictionary of Variables used in this Subroutine
C
C     *  1. Array elements:
C
C     BWGO  - Contribution to net freshwater flux into ocean upper 
C             due to initial ice growth or melt
C     FMI   - That portion of heat flux from ocean to ice which is used
C             to melt ice, calculated for diagnostic purposes (W m-2)
C     GC    - Ground cover type (1.=pack ice, 0.=open water, -1.=land)
C     GCINIT- Initial ground cover type before adjustment (internal)
C     ILSL  - Longitude index array for gathered points
C     JL    - Latitude index array for gathered points
C     OGT   - Ocean upper-layer temperature, initially passed from
C             ocean model and modified here (K)
C     SIC   - Sea ice amount (kg m-2)
C     SICN  - Sea ice concentration (0-1)
C     SNO   - Snow amount (kg m-2)
C
C     *  2. Parameters
C
C     CP    - Heat capacity of upper-layer sea water (J m-2 K-1)
C     CPW   - Heat capacity of sea water (J kg K-1)
C     DEP   - Depth of ocean upper-layer (m)
C     DNSW  - Density of sea water (kg m-3)
C     DELT  - Model time step (s) 
C     GTFSW - Freezing temperature of sea water (K)
C     HF    - Latent heat of fusion (=HS-HV) (J kg-1)
C     HS    - Latent heat of sublimation (J kg-1)
C     HV    - Latent heat of vaporization (J kg-1)
C     ILG   - Number of longitude points
C     IL1   - Starting latitude index
C     IL2   - Ending   latitude index
C     SICMIN- Minimum sea ice amount, below which grid cell is
C             considered open water (kg m-2)
C
C*******************************************************************
C
C     I/O ARRAYS:
C
      REAL,   DIMENSION(ILG) :: SIC,SICN,SNO,OGT,GC,BWGO,FMI       
      INTEGER,DIMENSION(ILG) :: ILSL,JL
C
C     * INTERNAL WORK ARRAY:
C
      REAL,   DIMENSION(ILG) :: GCINIT
C
      COMMON /PARAM1/ PI,RVORD,TFREZ,HS,HV,DAYLNT
      COMMON /PARAM3/ CSNO,CPACK,GTFSW,RKHI,SBC,SNOMAX
      COMMON /PARAM5/ CONI,DENI,SICMIN,DFSIC,CONF
C
C     * SPECIFY OCEAN PROPERTIES (CPW AND DNSW ARE VALUES AT FREEZING
C     * TEMP AND 35 PPT SALINITY)
C
      data     CPW,     DEP,      DNSW/
     1       3.990E+3,   10.,      1025./
C=======================================================================
C     * CALCULATE HEAT CAPACITY PER UNIT AREA OF UPPER-OCEAN LAYER
C
      CP=DNSW*CPW*DEP
      HF=HS-HV
C
C     * INITIALIZE FMI,BWGO TO ZERO
C     * NOTE INITIAL GROUND COVER TYPE
C
      DO IL=IL1,IL2
        FMI   (IL)=0.
        BWGO  (IL)=0.
        GCINIT(IL)=GC(IL)
      ENDDO
C
      NPTSCHG=0
      DO IL=IL1,IL2
       IF(GC(IL).GT.-0.5)                                         THEN
C
C       * TEST OCEAN UPPER-LAYER TEMPERATURE: IF LESS THAN FREEZING POINT,
C       * GROW MORE ICE
C  
        IF (OGT(IL).LT.GTFSW)                   THEN
          DSIC    =CP*(GTFSW-OGT(IL))/HF            ! calculate increase in ice mass
          FMI (IL)=-DSIC*HF/DELT                    ! calc. ocean->ice heat flux
          OGT (IL)=GTFSW                            ! reset ocean temp. to freezing
          SIC (IL)=SIC(IL)+DSIC                     ! increase ice mass
          BWGO(IL)=BWGO(IL)-DSIC/DELT               ! calc. implied fresh water flux
C
C         * RESET GROUND COVER TYPE IF IT HAS CHANGED; SET SICN=0.15 IF THIS IS
C         * NEW ICE FORMING.
C
          IF (SIC(IL).GE.SICMIN) THEN 
            GC(IL)=1.
            IF (GCINIT(IL) .LT. 0.5) SICN(IL)=0.15
          ENDIF
C
        ELSE
C
          DMASS=CP*(OGT(IL)-GTFSW)/HF               ! snow/ice mass that can be melted
C                                                   ! in a one-day time-scale
C
          IF (SNO(IL)+SIC(IL).LE.DMASS) THEN
C
C           * MORE HEAT IS AVAILABLE THAN SNOW AND ICE TOGETHER, SO MELT BOTH
C
            TMASS   =SNO(IL)+SIC(IL)                ! calc. total mass available
            OGT (IL)=OGT(IL)-TMASS*HF/CP            ! calc. new ocean temperature
            FMI (IL)=TMASS*HF/DELT                  ! calc. ocean->ice heat flux
            BWGO(IL)=BWGO(IL)+TMASS/DELT            ! calc. implied fresh water flux
            SIC (IL)=0.                             ! reset ice mass to zero
            SNO (IL)=0.                             ! reset snow mass to zero
            GC  (IL)=0.                             ! set ground cover type to ocean
            SICN(IL)=0.                             ! reset ice fraction to zero
C
          ELSEIF (SIC(IL).GE.DMASS) THEN
C
C           * THERE IS MORE ICE THAN AVAILABLE HEAT, SO ONLY MELT ICE
C
            OGT (IL)=OGT(IL)-DMASS*HF/CP            ! calc. new ocean temperature
            FMI (IL)=DMASS*HF/DELT                  ! calc. ocean -> ice heat flux
            BWGO(IL)=BWGO(IL)+DMASS/DELT            ! calc. implied fresh water flux
            SIC (IL)=SIC(IL)-DMASS                  ! reduce ice mass by melt amount
C
            IF (SIC(IL)+SNO(IL).LT.SICMIN) THEN
               SIC(IL)=SIC(IL)+SNO(IL)
               SNO(IL)=0.
            ELSE IF (SIC(IL).LT.SICMIN) THEN
               SNO(IL)=SNO(IL)-(SICMIN-SIC(IL))
               SIC(IL)=SICMIN
            ENDIF
C
C           * RESET GROUND COVER TYPE IF IT HAS CHANGED
C
            IF (SIC(IL).LT.SICMIN) THEN
               GC  (IL)=0.
               SICN(IL)=0.
            ENDIF
C
          ELSEIF (SIC(IL).LT.DMASS .AND. SNO(IL)+SIC(IL).GT.DMASS) THEN
C
C           * THERE IS MORE HEAT AVAILABLE THAN ICE, BUT LESS THAN ICE PLUS SNOW, 
C           * SO MELT ALL OF THE ICE PLUS SOME SNOW
C
            OGT (IL)=OGT(IL)-DMASS*HF/CP            ! calc. new ocean temperature
            FMI (IL)=DMASS*HF/DELT                  ! calc. ocean -> ice heat flux
            BWGO(IL)=BWGO(IL)+DMASS/DELT            ! calc. implied fresh water flux
            SNO (IL)=SNO(IL)+SIC(IL)-DMASS          ! calc. remaining snow mass
C
            IF (SNO(IL).GT.SICMIN) THEN
               SNO(IL)=SNO(IL)-SICMIN
               SIC(IL)=SICMIN
            ELSE
               SIC(IL)=SNO(IL)
               SNO(IL)=0.
            ENDIF
C
C           * RESET GROUND COVER TYPE IF IT HAS CHANGED
C
            IF (SIC(IL).LT.SICMIN) THEN
              GC  (IL)=0.
              SICN(IL)=0.
            ENDIF
          ENDIF
        ENDIF
C   
C       * KEEP TRACK OF NUMBER OF POINTS CHANGING GROUND COVER. THIS
C       * IS USED TO MINIMIZE ACTIVATION OF FOLLOWING LOOP.
C
        IF ( (GCINIT(IL).EQ.0. .AND. GC(IL).EQ.1.) .OR.
     1       (GCINIT(IL).EQ.1. .AND. GC(IL).EQ.0.) ) NPTSCHG=NPTSCHG+1

       ENDIF
      ENDDO
C
C     * WRITE OUT MESSAGE IF GROUND COVER HAS CHANGED FOR ANY POINT
C
      IF(NPTSCHG.NE.0) THEN
        DO IL=IL1,IL2
          IF (GCINIT(IL).EQ.0. .AND. GC(IL).EQ.1.) THEN
            I=ILSL(IL)
            J=JL  (IL)  
            WRITE(6,6000) I,J
          ENDIF
          IF (GCINIT(IL).EQ.1. .AND. GC(IL).EQ.0.) THEN
            I=ILSL(IL)
            J=JL  (IL)  
            WRITE(6,6001) I,J
          ENDIF
        ENDDO
      ENDIF
C
 6000 FORMAT('0 IN ICADJST, SEA ICE APPEARS AT I=',I5,', J=',I5)
 6001 FORMAT('0 IN ICADJST, SEA ICE DISAPPEARS AT I=',I5,', J=',I5)
C
      RETURN
      END
