      SUBROUTINE INTFLD(ROW,ROL,IL1,IL2,ILG,DELT,GMT,IDAY,MDAY)
C
C     * MAY 03/14 - M.LAZARE. NEW ROUTINE FOR GCM18+:
C     *                       - BASED ON INTGTIO2 BUT NO
C     *                         DEPENDANCE ON GCROW
C     *                         (HENCE "IMASK" REMOVED ALSO).
C     *                         MADE GENERAL FOR ANY FIELD BY
C     *                         REMOVING REFERENCES TO "GT".
C     * APR 29/03 - M.LAZARE. PREVIOUS VERSION INTGTIO2:
C     *                       1,LON -> IL1,IL2.
C     * OCT 16/97 - M.LAZARE. PREVIOUS VERSION INTGTIO.
C
C     * INPUT/OUTPUT:
C     * ROW  = CURRENT VALUE ARRAY.
C     * INPUT:
C     * ROL  = TARGET VALUE ARRAY (VALID AT MDAY).
C
C     * IL1    = START LONGITUDE INDEX.
C     * IL2    = END   LONGITUDE INDEX.
C     * ILG    = DIMENSION OF LONGITUDE ARRAYS.
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * MDAY   = DATE OF TARGET FIELD.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)                                         
 
      REAL ROW(ILG), ROL(ILG)
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      DAY=REAL(IDAY)+GMT/86400. 
      FMDAY=REAL(MDAY) 
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      DAYSM=FMDAY-DAY 
      STEPSM=DAYSM*86400./DELT
C 
C     * GENERAL INTERPLATION.
C 
      DO 210 I=IL1,IL2
        ROW  (I)  = ((STEPSM-1.) * ROW  (I) + ROL  (I)) / STEPSM      
  210 CONTINUE
C 
      RETURN
      END 
