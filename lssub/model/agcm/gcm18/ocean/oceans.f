      SUBROUTINE OCEANS (SNOROT,  GTROT,   ANROT,   TNROT,
     1                   RHONROT, BCSNROT, REFROT, EMISROT,
     2                   CDMROT,  QGROT,   HFSROT,  QFSROT, FNROT, 
     +                   SALBROT, CSALROT,
     3                   IWMOS,   JWMOS,
     4                   NMW,NL,NM,ILG,NBS,
     5                   SNOGAT,  GTGAT,   ANGAT,   TNGAT,
     6                   RHONGAT, BCSNGAT, REFGAT, EMISGAT,
     7                   CDMGAT,  QGGAT,   HFSGAT,  QFSGAT, FNGAT,
     +                   SALBGAT, CSALGAT)
C
C     * Jun 20, 2014 - M.Lazare. New version called by "sfcproc3"
C     *                          in new model version gcm18. This
C     *                          is modelled on "classs" since
C     *                          ocean is now tiled as well.
C     *                          - Adds SALBGAT/SALBROT, CSALGAT/CSALROT 
C     *                            and  EMISGAT/EMISROT.
C     *                          - {SNO,GT,AN,TN,RHON,BCSN,REF} now tiled.
C     *                          - Most output diagnostic fields removed
C     *                            since now accumulated over ocean tile(s)
C     *                            after call to this routine.
C     *                          - GC removed.
C     * Nov 15, 2013 - M.Lazare. Previous version "oceans" called by
C     *                          "sfcproc" in previous model version gcm17.
C     * Nov 15, 2013 - M.Lazare. ROFN now accumulated while
C                                scattering, not instantaneous.
C     * Jun 11, 2013 - M.Lazare. Ocean scatter routine called by 
C     *                          "sfcproc" in new version gcm17.
C     * NOTE: This contains the following changes compared to the
C     *       working temporary version used in conjunction with
C     *       updates to gcm16 (ie not official):
C     *         1) {TN,RHON,FN,TISL,ROFN} added for Maryam's
C     *            contribution to the new snow albedo formulation.
C     *         2) No more SNOROT,ANROT (SNOROW,ANROW still there)
C     *            since these are for land only.
C     *         3) Instantaneous fields ZNROL,SMLTROL scattered
C     *            instead of accumulated ZNROW,SMLTROW, since
C     *            these are now done in the physics driver. 
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NMW,NL,NM,ILG,K,L,M,NBS
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM,NBS) ::
     1      SALBROT, CSALROT

      REAL, DIMENSION(ILG,NBS) ::
     1      SALBGAT, CSALGAT

      REAL, DIMENSION(NL,NM) ::
     1      SNOROT,  GTROT,   ANROT,   RHONROT, TNROT,
     2      BCSNROT, REFROT,  EMISROT,
     3      CDMROT,  QGROT,   HFSROT,  QFSROT,  FNROT

      REAL, DIMENSION(ILG)   ::
     1      SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT,
     2      BCSNGAT, REFGAT,  EMISGAT,
     3      CDMGAT,  QGGAT,   HFSGAT,  QFSGAT,  FNGAT
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IWMOS  (ILG),  JWMOS (ILG)
C----------------------------------------------------------------------
      DO 100 K=1,NMW
          SNOROT (IWMOS(K),JWMOS(K))=SNOGAT (K)  
          GTROT  (IWMOS(K),JWMOS(K))=GTGAT  (K)  
          ANROT  (IWMOS(K),JWMOS(K))=ANGAT  (K)  
          TNROT  (IWMOS(K),JWMOS(K))=TNGAT  (K)  
          RHONROT(IWMOS(K),JWMOS(K))=RHONGAT(K)  
          BCSNROT(IWMOS(K),JWMOS(K))=BCSNGAT(K) 
          REFROT (IWMOS(K),JWMOS(K))=REFGAT (K) 
          EMISROT(IWMOS(K),JWMOS(K))=EMISGAT(K) 
          CDMROT (IWMOS(K),JWMOS(K))=CDMGAT (K)
          QGROT  (IWMOS(K),JWMOS(K))=QGGAT  (K)
          HFSROT (IWMOS(K),JWMOS(K))=HFSGAT (K)
          QFSROT (IWMOS(K),JWMOS(K))=QFSGAT (K)
          FNROT  (IWMOS(K),JWMOS(K))=FNGAT  (K)
  100 CONTINUE
C
      DO 200 L=1,NBS
      DO 200 K=1,NMW
          SALBROT(IWMOS(K),JWMOS(K),L)=SALBGAT(K,L)
          CSALROT(IWMOS(K),JWMOS(K),L)=CSALGAT(K,L)
  200 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
