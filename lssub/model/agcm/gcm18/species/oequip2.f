      SUBROUTINE OEQUIP2 (ILG,ILEV,NEQP,ILEVI,IL1,IL2,ACHPA,ATEMP)
C-------------------------------------------------------------------------
C     * OCT 24/2006 - M.LAZARE.        NEW VERSION FOR GCM15F:
C     *                                - IMPLICIT NONE WITH REAL*8
C     *                                  SCALAR FOR TEMPERATURE
C     *                                  TO WORK IN 32-BIT ENVIRONMENT.  
C     * JUN 19/01 - K. VON SALZEN      PREVIOUS VERSION OEQUIP UP TO GCM15E.
C     * FEB 2/98  - K. VON SALZEN      NEW SUBROUTINE.       
C
C     * CHEMICAL PARAMETERS REQUIRED FOR IN-CLOUD OXIDATION OF S(IV).
C     * THIS SUBROUTINE PROVIDES HENRY'S LAW CONSTANT AND REACTION RATES.
C-----------------------------------------------------------------------
      IMPLICIT NONE
C
      REAL ATEMP(ILG,ILEV)
      REAL ACHPA(ILG,ILEV,NEQP)
C
      REAL*8 TEMP
      INTEGER ILG,ILEV,NEQP,ILEVI,IL1,IL2,L,IL
C-----------------------------------------------------------------------
C
      DO 100 L=ILEVI,ILEV                                                  
      DO 100 IL=IL1,IL2                                 
         TEMP=ATEMP(IL,L)
         ACHPA(IL,L,1) = TEMP *
     1            1.0093E-01 * EXP (3120.*(1./TEMP - 1./298.))
         ACHPA(IL,L,2) = TEMP *
     1            1.7158E-03 * EXP (5210.*(1./TEMP - 1./298.))
         ACHPA(IL,L,3) = TEMP * 
     1            1.0295E-10 * EXP (6330.*(1./TEMP - 1./298.))
         ACHPA(IL,L,4) = TEMP *
     1            2.5520E-03 * EXP (2423.*(1./TEMP - 1./298.))
         ACHPA(IL,L,5) = TEMP *
     1            1.0973E-09 * EXP (1510.*(1./TEMP - 1./298.))
         ACHPA(IL,L,6) = TEMP *
     1            7.9595E+03 * EXP (6600.*(1./TEMP - 1./298.))
         ACHPA(IL,L,7) = TEMP *
     1            9.4366E-04 * EXP (2560.*(1./TEMP - 1./298.))
         ACHPA(IL,L,8) = TEMP *
     1           206.032E+03 * EXP ( 29.1657 * (298./TEMP - 1.) 
     2         + 16.8322 * (1. + LOG (298./TEMP) 
     3                               - 298./TEMP) )
         ACHPA(IL,L,9) = TEMP *
     1           8.44851E+09 * EXP ( 34.8536 * (298./TEMP - 1.) 
     2          - 5.3930 * (1. + LOG (298./TEMP) 
     3                               - 298./TEMP) )
         ACHPA(IL,L,10) = TEMP *
     1           161.675E+03 * EXP ( 30.2355 * (298./TEMP - 1.) 
     2         + 19.9083 * (1. + LOG (298./TEMP) 
     3                               - 298./TEMP) )
         ACHPA(IL,L,11) = 4.4E+11 * EXP ( -4131. / TEMP ) 
         ACHPA(IL,L,12) = 2.6E+03 * EXP ( -966.  / TEMP )
         ACHPA(IL,L,13)  = 8.0E+04 
     1               * EXP ( -3650. * (1. / TEMP - 1. / 298.) )
 100  CONTINUE
C
      RETURN
      END
