      SUBROUTINE XTEANT4(XEMIS,PRESSG,DSHJ,ZF,EAOCROW,EABCROW,EASLROW,
     1                   EASHROW,IOCO,IOCY,IBCO,IBCY,ISO2,ILG,IL1,IL2,
     2                   ILEV,LEV,NTRAC,ZTMST)
C
C     * JUN 17/2013 - K.VONSALZEN/ NEW VERSION FOR GCM17+:
C     *               M.LAZARE.    - ADD CONDITION TO SET WGT=1. FOR
C     *                              SPECIAL CASE WHERE HSO2=LSO2.
C     * APR 28/2012 - K.VONSALZEN/ PREVIOUS VERSION XTEANT3 FOR GCM16:
C     *               M.LAZARE.    - INITIALIZE LSO2 AND HSO2 TO
C     *                              ILEV INSTEAD OF ZERO, TO
C     *                              AVOID POSSIBLE INVALID 
C     *                              MEMORY REFERENCES.
C     *                            - BUGFIX FOR INCORRECT MEMORY
C     *                              REFERENCE: LSO2(L)->LSO2(IL).
C     * APR 26/2010 - K.VONSALZEN. PREVIOUS VERSION XTEANT2 FOR GCM15I:
C     *                            - ZF AND PF CALCULATED ONE-TIME AT
C     *                              BEGINNING OF PHYSICS AND ZF
C     *                              PASSED IN, WITH INTERNAL CALCULATION
C     *                              THUS REMOVED AND NO NEED TO PASS
C     *                              IN SHBJ,T.
C     *                            - EMISSIONS ACCUMULATED INTO GENERAL
C     *                              "XEMIS" ARRAY RATHER THAN UPDATING
C     *                              XROW (XROW UPDATED FROM XEMIS IN
C     *                              THE PHYSICS). 
C     *                            - REMOVAL OF DIAGNOSTIC FIELDS
C     *                              CALCULATION, SO NO NEED TO PASS
C     *                              IN "SAVERAD" OR "ISVCHEM".
C     *                            - WGT=0 (IE NO EMISSIONS IN LAYER)
C     *                              IF IT IS ABOVE TOP OR BELOW BASE
C     *                              OF EMISSIONS.
C     * KNUT VON SALZEN - FEB 07,2009. PREVIOUS ROUTINE XTEANT FOR 
C     *                                GCM15H TO APPLY EMISSIONS FOR:
C     *                                EASL,EASH,EAOC,EABC.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, DIMENSION(ILG) :: EAOCROW,EABCROW,EASLROW,EASHROW,
     1                        PRESSG
      REAL, DIMENSION(ILG,ILEV) :: DSHJ,ZF
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: XEMIS
C
      INTEGER, DIMENSION(ILG) ::  LSO2,HSO2 
C
      COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES, RGASV, CPRESV
C-------------------------------------------------------------------
C
      DO 30 IL=IL1,IL2
        FACT = ZTMST*GRAV/(DSHJ(IL,ILEV  )*PRESSG(IL))
C
C       * ANTHROPOGENIC ORGANIC CARBON.
C
        EOC=EAOCROW(IL)
        XEMIS(IL,ILEV,IOCO)=XEMIS(IL,ILEV,IOCO)+0.5*EOC*FACT
        XEMIS(IL,ILEV,IOCY)=XEMIS(IL,ILEV,IOCY)+0.5*EOC*FACT
C
C       * ANTHROPOGENIC BLACK CARBON.
C
        EBC=EABCROW(IL)
        XEMIS(IL,ILEV,IBCO)=XEMIS(IL,ILEV,IBCO)+0.8*EBC*FACT
        XEMIS(IL,ILEV,IBCY)=XEMIS(IL,ILEV,IBCY)+0.2*EBC*FACT
C
C       * ANTHROPOGENIC SULPHUR DIOXIDE FROM SURFACE SOURCES.
C
        XEMIS(IL,ILEV,ISO2)=XEMIS(IL,ILEV,ISO2)+EASLROW(IL)*FACT
   30 CONTINUE      
C
C     * DETERMINE LAYER CORRESPONDING TO POWER PLANT AND INDUSTRIAL
C     * SO2 EMISSIONS (100-300m).
C
      DO 100 IL = IL1, IL2
        LSO2(IL) = ILEV
        HSO2(IL) = ILEV
  100 CONTINUE
      DO 105 L = ILEV, 1, -1
      DO 105 IL = IL1, IL2
        IF(ZF(IL,L) .LE. 100.) LSO2(IL)=L
        IF(ZF(IL,L) .LE. 300.) HSO2(IL)=L
  105 CONTINUE
      JT=ISO2
      DO 110 L=1,ILEV
      DO 110 IL=IL1,IL2
        ESPI = EASHROW(IL)
        FACT = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
          WGT=1.
        ELSE IF(L.EQ.HSO2(IL)) THEN
          WGT=(300.-ZF(IL,L))/200.
        ELSE IF(L.GT.HSO2(IL).AND.L.LT.LSO2(IL)) THEN
          WGT=(ZF(IL,L-1)-ZF(IL,L))/200.
        ELSE IF(L.EQ.LSO2(IL)) THEN
          WGT=(ZF(IL,L-1)-100.)/200.
        ELSE
          WGT=0.
        ENDIF
        XEMIS(IL,L,JT)=XEMIS(IL,L,JT)+WGT*ESPI*FACT
  110 CONTINUE
C
      RETURN
      END 
