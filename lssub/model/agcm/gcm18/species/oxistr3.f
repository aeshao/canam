      SUBROUTINE OXISTR3(   IL1,     IL2,    ILEV,    ILG,   NTRAC,
     1                     NEQP,    ISO2,    ISO4,   IHPO,    ISSA,
     2                     ISSC,   ITRAC,
     2                   ROAROW,  NH3ROW,  NH4ROW, HNO3ROW,   ZZO3,
     3                   OGTSO2,  AGTSO4,   AGTO3, OGTCO2,  AGTHO2,
     4                  OGTHNO3,  OGTNH3,   ACHPA, ANTSO2,  ANTHO2,
     5                   ANTSO4,    XROW,  AOH2O2, ARESID,   ZMLWC,
     6                     ZCLF,   KCALC,      TH,     QV,     SHJ,  
     7                   PRESSG,     THG,    RGAS,   EPS1,   DQLDT,  
     8                    ZTMST,  ZXTP1C,    ZSO4,   AF1V,    AF2V,
     9                     AMHV,  ITRPHS,   ISULF,   ILWCP,  ICHEM,
     A                  SLO3ROW, SLHPROW,  PHLROW, SAVERAD,
     B                      LEV, ISVCHEM,    GRAV,    DSHJ,
     C                     WRK1,    WRK2,    WRK3, ZHENRY            )
C-------------------------------------------------------------------------
C     * APR 25/10 -  K. VONSALZEN/     NEW VERSION FOR GCM15I:
C     *              M. LAZARE.        - DIAGNOSTIC ARRAY CLPHROW REMOVED
C     *                                  AND CALCULATION OF DIAGNOSTIC
C     *                                  ARRAY PHLROW REVISED (USED TO
C     *                                  BE BASED ON CLPHROW), UNDER
C     *                                  CONTROL OF "ISVCHEM".
C     * NOV 23/06 -  M. LAZARE.        CODE REVISION (INCLUDING PROMOTED
C     *                                SCALARS) TO RUN ACCURATELY IN 32-BIT.
C     *                                THIS ALSO INCLUDES A REVISED CALL
C     *                                TO A NEW OEQUIP2. 
C     * JUN 20/06 -  M. LAZARE.        PREVIOUS VERSION OXISTR2 FOR GCM15F/G/H:
C     *                                - USE VARIABLE INSTEAD OF CONSTANT
C     *                                  IN INTRINSICS SUCH AS "MAX",
C     *                                  SO THAT CAN COMPILE IN 32-BIT MODE
C     *                                  WITH REAL*8.  
C     * DEC 20/02 -  K. VONSALZEN.     PREVIOUS FINAL VERSION OXISTR FOR
C     *                                GCM15B/C/D/E:
C     *                                - REMOVE "ITRIND" COMMON BLOCK
C     *                                  AND PASS INDICES INSTEAD. 
C     *                                - BUGFIX TO DIAGNOSTIC CALCULATIONS
C     *                                  INVOLVING WRK2.
C     *                                - INCLUDE SEA-SALT IN MOLARITY.
C     * JUN 18/02 -  K. VONSALZEN.     BUGFIXES TO DIAGNOSTIC CALCULATIONS
C     *                                OF SLO3ROW,SHHPROW INVOLVING 
C     *                                ADDED WORK ARRAYS: WRK1,WRK2,WRK3.
C     * MAR 20/02 -  M.LAZARE.         BUXFIXES TO: A. PASS LEV AND USE 
C     *                                PROPERLY XROW(L+1) INSTEAD OF XROW(L).
C     *                                B. SAVE AMH,AF1,AF2 
C     *                                INTO NEW WORK VECTORS TO BE
C     *                                PASSED TO LOOP 355 FOR DIAGNOSTIC
C     *                                CALCULATIONS. 
C     * OCT 17/01 -  K. VON SALZEN.    REVISED FOR GCM15
C     * APR 29/98 -  K. VON SALZEN.    NEW SUBROUTINE       
C
C     * CALCULATES IN-CLOUD CHEMICAL PROCESSES FOR SULPHUR SPECIES
C     * IN STRATIFORM CLOUDS (I.E. MIXING PROCESSES ARE NOT CONSIDERED).
C
C-----------------------------------------------------------------------
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      INTEGER YSUB
C
      PARAMETER ( YSUB   = 2 )
      PARAMETER ( YCOM3L = 1.E+03 )
      PARAMETER ( YMOLGA = 28.84 )
      PARAMETER ( YRHOW  = 1.E+03 )
      PARAMETER ( YMASS  = 32.06E-03 )
C
      REAL   ZCLF(ILG,ILEV),XROW(ILG,LEV,NTRAC),ZMLWC(ILG,ILEV),
     1       ARESID(ILG,ILEV),OGTSO2(ILG,ILEV),AGTSO4(ILG,ILEV),
     2       AGTO3(ILG,ILEV),OGTCO2(ILG,ILEV),ZZO3(ILG,ILEV),
     3       AGTHO2(ILG,ILEV),OGTHNO3(ILG,ILEV),OGTNH3(ILG,ILEV),
     4       ANTSO2(ILG,ILEV),ANTHO2(ILG,ILEV),ANTSO4(ILG,ILEV),
     5       HNO3ROW(ILG,ILEV),NH3ROW(ILG,ILEV),NH4ROW(ILG,ILEV)
      REAL   ROAROW(ILG,ILEV),AOH2O2(ILG,ILEV),QV(ILG,ILEV),
     1       SHJ(ILG,ILEV),PRESSG(ILG),TH(ILG,ILEV),DQLDT(ILG,ILEV),
     2       ZXTP1C(ILG,ILEV),ZSO4(ILG,ILEV),DSHJ(ILG,ILEV),
     3       THG(ILG,ILEV),WRK1(ILG,ILEV),WRK2(ILG,ILEV),WRK3(ILG,ILEV),
     4       ZHENRY(ILG,ILEV)
      REAL   AF1V(ILG),AF2V(ILG),AMHV(ILG)
      REAL   ACHPA(ILG,ILEV,NEQP)
      INTEGER ILWCP(ILG),ICHEM(ILG)
      INTEGER ITRPHS(NTRAC)
      LOGICAL KCALC(ILG,ILEV),KCHEM
C
C     * I/O FIELDS.
C
      REAL   SLO3ROW(ILG), SLHPROW(ILG)
      REAL    PHLROW(ILG,ILEV)
C
c
c     * scalar promotion.
c
      real*8 amso3,amnh4,amno3,amso4,assmr,aconvf,amssso4,amsscl,amssant
      real*8 amsscat,amcl,amhcl,agthcl,adelta,afoh,amh,atval
      real*8 afnh4,afno3,afcl,afhso,afco3,atvo3,af1,afrah,af2
      real*8 apara,aparb,aparc,apard,atvalx,amhmax,amhmin
C
      DATA ZERO,AMHMIN,AMHMAX /0., 1.E-10, 1.E-01/
C-----------------------------------------------------------------------
C
C     * INITIALIZATIONS.
C
      ATIMST = ZTMST/REAL(YSUB)
      DO 3 IL=IL1,IL2                                                  
         ILWCP(IL)=0
 3    CONTINUE
      DO 5 L=1,ILEV                                                  
      DO 5 IL=IL1,IL2                                                  
         KCALC(IL,L)  = .FALSE.
         ARESID(IL,L) = 0.
         WRK1(IL,L)   = 0.
         WRK2(IL,L)   = 0.
         WRK3(IL,L)   = 0.
 5    CONTINUE                                                          
      IF ( ITRAC.NE.0 ) THEN
         KCHEM=   ITRPHS(ISO2).GT.0 .AND. ITRPHS(ISO4).GT.0 
     1      .AND. ISULF.NE.0
      END IF
C
C     * TEST IF SUFFICIENT LIQUID WATER IS AVAILABLE.
C
      DO 10 L=1,ILEV                                                   
      DO 10 IL=IL1,IL2                                 
         IF (      ZMLWC(IL,L) .GT. 0.1E-06
     1       .AND. ZCLF(IL,L).GT.0.                  ) THEN
            KCALC(IL,L)=KCHEM
            ILWCP(IL)=1
            QMR=QV(IL,L)/(1.-QV(IL,L))
            RGASA=RGAS*(1.+QMR/EPS1)/(1.+QMR)
            ROAROW(IL,L)=SHJ(IL,L)*PRESSG(IL)/(RGASA*TH(IL,L))
         ENDIF
   10 CONTINUE
C
C     * DETERMINE GRID POINTS AT WHICH THE SITUATION PERMITS 
C     * CHEMICAL CALCULATIONS.
C
      JYES=0
      JNO =IL2-IL1+2
      DO 20 IL=IL1,IL2
         IF ( ILWCP(IL).EQ.1 ) THEN
            JYES        =JYES+1
            ICHEM(JYES)=IL
         ELSE
            JNO         =JNO -1
            ICHEM( JNO)=IL
         ENDIF
  20  CONTINUE
      ILGG=JYES
C
C     * HENRY'S LAW CONSTANTS.
C
      DO 30 L=1,ILEV
      DO 30 IL=1,ILGG
         THG(IL,L)=TH(ICHEM(IL),L)
 30   CONTINUE
C
      CALL OEQUIP2(ILG,ILEV,NEQP,1,1,ILGG,ACHPA,THG)
C
C     * UNIT CONVERSIONS.
C
      IF ( KCHEM ) THEN
         DO 100 L=1,ILEV                                                   
         DO 100 IL=1,ILGG                                 
            IF ( KCALC(ICHEM(IL),L) ) THEN
C
C              Sulphur dioxide. Conversion kg-S/kg -> mol/l.
               OGTSO2(IL,L)=ZXTP1C(ICHEM(IL),L)*ROAROW(ICHEM(IL),L)
     1                     /(YMASS*YCOM3L) 
C
C              Sulphate. Conversion kg-S/kg -> mol/l.
               AGTSO4(IL,L)=ZSO4(ICHEM(IL),L)*ROAROW(ICHEM(IL),L)
     1                     /(YMASS*YCOM3L)
C
C              Ozone in mol/l.
               AGTO3(IL,L)=ZZO3(ICHEM(IL),L)*1.E+06
     1                    /(6.022045E+23*YCOM3L)
C
C              Hydrogen peroxide. Conversion kg-S/kg -> mol/l.
               AGTHO2(IL,L)=XROW(ICHEM(IL),L+1,IHPO)*ROAROW(ICHEM(IL),L)
     1                     /(YMASS*YCOM3L)
               AOH2O2(IL,L)=AGTHO2(IL,L)
C
C              Nitric acid. Conversion VMR -> mol/l.
               OGTHNO3(IL,L)=HNO3ROW(ICHEM(IL),L)*ROAROW(ICHEM(IL),L)
     1                      /YMOLGA
C
C              Ammonia. Conversion VMR -> mol/l.
               OGTNH3(IL,L)=(NH3ROW(ICHEM(IL),L)+NH4ROW(ICHEM(IL),L))
     1                     *ROAROW(ICHEM(IL),L)/YMOLGA
C
C              Carbon dioxide. Conversion VMR -> mol/l.
               OGTCO2(IL,L)=370.E-06*ROAROW(ICHEM(IL),L)/YMOLGA
            END IF
 100     CONTINUE
      END IF
C
C     * S(IV) OXIDATION. CHEMICAL SOURCES/SINKS ARE CONSIDERED 
C     * FOR SULPHUR DIOXIDE, HYDROGEN PEROXIDE, OZONE. THEY
C     * ARE ONLY APPLIED IF ALL OF THESE THREE TRACERS PLUS
C     * SULPHATE ARE AVAILABLE. THE CORRESPONDING SULPHATE 
C     * PRODUCTION RATE IS GIVEN BY AF1*[O3]*[SO2]+AF2*[H2O2]*[SO2]
C     * (IN KG-SULPHUR/KG-AIR/SEC), WHERE THE CONCENTRATIONS
C     * REFER TO THE TOTAL (GAS PHASE + DISSOLVED) SPECIES (IN
C     * KG-SULPHUR/KG-AIR). THE INTERNAL TIME STEP IN THESE
C     * CALCULATIONS IS GIVEN BY: TIME_STEP(MODEL)/YSUB.
C
      DO 400 INDTI = 1, YSUB        
C
C      *** INITIALIZATION.
C
         DO 310 L=1,ILEV                   
         DO 310 IL=1,ILGG
            IF ( KCALC(ICHEM(IL),L) ) THEN
               ANTSO2(IL,L) = OGTSO2(IL,L)
               ANTHO2(IL,L) = AGTHO2(IL,L)
               ANTSO4(IL,L) = AGTSO4(IL,L)
            ENDIF
  310    CONTINUE
C
C      *** CHEMICAL FIELDS.
C
         DO 360 L=1,ILEV  
          DO 350 IL=1,ILGG
            IF ( KCALC(ICHEM(IL),L) ) THEN
C
C             Liquid water volume mixing ratio.
C
C             ATEMPO  = MAX(ZMLWC(IL,L)-DQLDT(IL,L),ZERO)
              ATEMPO  = MAX(ZMLWC(ICHEM(IL),L),ZERO)
              ALWCVMR = ATEMPO*ROAROW(ICHEM(IL),L)/YRHOW
C
C             Total (aq. + gas phase) concentrations.
C
              AGTSO2  = OGTSO2(IL,L)
              AGTCO2  = OGTCO2(IL,L)
              AGTHNO3 = OGTHNO3(IL,L)
              AGTNH3  = OGTNH3(IL,L)
C
C             Molarities.
C
              AMSO3 = 0.                         
              AMNH4 = AGTNH3 /ALWCVMR
              AMNO3 = AGTHNO3/ALWCVMR 
              AMSO4 = AGTSO4(IL,L)/ALWCVMR
C
C             SEA SALT SPECIES.
C
              ASSMR=XROW(ICHEM(IL),L+1,ISSA)+XROW(ICHEM(IL),L+1,ISSC)
              ACONVF=ROAROW(ICHEM(IL),L)/(ALWCVMR*YCOM3L)
C 
C             CALCULATE MOLARITIES OF SEA SALT SPECIES ASSUMING A TYPICAL
C             COMPOSITION OF SEA WATER. NO ANIONS OTHER THAN SULPHATE AND
C             CHLORIDE ARE CONSIDERED. ALL CATIONS, EXCEPT H+, ARE LUMPED 
C             TOGETHER. THE PH OF THE SEA WATER IS ASSUMED TO BE 8.2.
C             H+ IS ACCOUNTED FOR IN THE ION BALANCE AND IN THE CALCULATION
C             OF HYDROGEN CHLORINE IN THE GAS PHASE.
C
              AMSSSO4=0.077*ACONVF/96.058E-03*ASSMR
              AMSSCL =0.552*ACONVF/35.453E-03*ASSMR
              AMSSANI=0.
              AMSSCAT=(AMSSCL+2.*AMSSSO4+AMSSANI)*(1.-1.048E-08)
              AMSO4=AMSO4+AMSSSO4
              AMCL=AMSSCL
              AMHCL=AMSSCL*6.31E-09/ACHPA(IL,L,10)
              AGTHCL=(AMSSCL+AMHCL)*ALWCVMR
C              AGTHCL=AMSSCL*ALWCVMR
C
C             Initial guess H+ molarity from initial ion 
C             molarities.
C
              ADELTA=AMNO3+2.*(AMSO4+AMSO3)+AMCL+AMSSANI-AMNH4-AMSSCAT
              AFOH=1.E-14
              AMH=0.5*( ADELTA+SQRT(ADELTA**2+4.*(AFOH
     1           +ACHPA(IL,L,2)*AGTSO2+ACHPA(IL,L,5)*AGTCO2)) )
              AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)  
C
C             Equilibrium parameters for soluble species.
C
              ATVAL=ACHPA(IL,L,1)+ACHPA(IL,L,2)/AMH
     1             +ACHPA(IL,L,3)/AMH**2
              AFRAS=1./( 1.+ALWCVMR*ATVAL )
              AFNH4=1./( 1.+AGTNH3/(1./ACHPA(IL,L,9)
     1             +ALWCVMR*AMH) )
              AFNO3=AGTHNO3/( 1./ACHPA(IL,L,8)+ALWCVMR/AMH )
C
C             First iteration H+ molarity calculation.
C
              AFCL =AGTHCL/( 1./ACHPA(IL,L,10)+ALWCVMR/AMH )
              AFHSO=AGTSO2*AFRAS*ACHPA(IL,L,2)
              AFCO3=AGTCO2*ACHPA(IL,L,5) 
              AMSO3=( AGTSO2*AFRAS*ACHPA(IL,L,3) )/AMH**2
              ADELTA=AFNH4*( 2.*(AMSO4+AMSO3)+AMSSANI-AMSSCAT)
              AMH=0.5*( ADELTA+SQRT(ADELTA**2+4.*AFNH4*(AFOH
     1                              +AFCO3+AFHSO+AFNO3+AFCL)) )
              AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)
              ATVAL=ACHPA(IL,L,1)+ACHPA(IL,L,2)/AMH
     1             +ACHPA(IL,L,3)/AMH**2
              AFRAS=1./( 1.+ALWCVMR*ATVAL )
              AFNH4=1./( 1.+AGTNH3/(1./ACHPA(IL,L,9)
     1             +ALWCVMR*AMH) )
              AFNO3=AGTHNO3/( 1./ACHPA(IL,L,8)+ALWCVMR/AMH )
C
C             Second iteration H+ molarity calculation.
C
              AFCL =AGTHCL/( 1./ACHPA(IL,L,10)+ALWCVMR/AMH )
              AFHSO=AGTSO2*AFRAS*ACHPA(IL,L,2)
              AFCO3=AGTCO2*ACHPA(IL,L,5) 
              AMSO3=( AGTSO2*AFRAS*ACHPA(IL,L,3) )/AMH**2
              ADELTA=AFNH4*( 2.*(AMSO4+AMSO3)+AMSSANI-AMSSCAT)
              AMH=0.5*( ADELTA+SQRT(ADELTA**2+4.*AFNH4*(AFOH
     1                            +AFCO3+AFHSO+AFNO3+AFCL)) )
              AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)
              ATVAL=ACHPA(IL,L,1)+ACHPA(IL,L,2)/AMH
     1             +ACHPA(IL,L,3)/AMH**2
              AFRAS=1./( 1.+ALWCVMR*ATVAL )
C
C             Ozone oxidation parameter (in 1/sec).
C
              ATVO3 = 1./( 1.+ALWCVMR*ACHPA(IL,L,7) )
              AF1 = ( ACHPA(IL,L,11)+ACHPA(IL,L,12)/AMH )*AFRAS
     1              *ATVAL*ATVO3*ACHPA(IL,L,7)*ALWCVMR
              AF1=AF1*AGTO3(IL,L)
C
C             Hydrogen peroxide oxidation parameter 
C             (in litre-air/mol/sec).
C
              AFRAH = 1./( 1.+ALWCVMR*ACHPA(IL,L,6) )
              AF2 = ( ACHPA(IL,L,13)/(0.1+AMH) )*AFRAS
     1               *ACHPA(IL,L,1)*AFRAH*ACHPA(IL,L,6)*ALWCVMR 
C
C             Semi-implicit integration. 
              APARD = AGTHO2(IL,L)
              APARA = ( 1.+ATIMST*AF1 )*ATIMST*AF2
              APARC = -AGTSO2
              APARB = 1.+ATIMST*( AF1+AF2*(APARD+APARC) )
              ATVALX = -APARB/( 2.*APARA ) 
              ANTSO2(IL,L) = ATVALX+SQRT( ATVALX**2-APARC/APARA )
              ANTSO2(IL,L) = MAX(ANTSO2(IL,L),ZERO)
              ANTSO2(IL,L) = MIN(ANTSO2(IL,L),AGTSO2)
              ANTHO2(IL,L) = APARD/(1.+ATIMST*AF2*ANTSO2(IL,L))
              ANTHO2(IL,L) = MAX(ANTHO2(IL,L),ZERO)
              ANTHO2(IL,L) = MIN(ANTHO2(IL,L),AGTHO2(IL,L))
              ADELTA = AGTSO2-ANTSO2(IL,L)
              ANTSO4(IL,L) = AGTSO4(IL,L)+ADELTA
              ANTSO4(IL,L) = MAX(ANTSO4(IL,L),ZERO)
              WRK1(IL,L)   = WRK1(IL,L)+AF1/REAL(YSUB)
              WRK2(IL,L)   = WRK2(IL,L)+AF2*AGTHO2(IL,L)/REAL(YSUB)
              AMHV(IL)     = AMH
C
C           *** SCAVENGING RATIOS, I.E. FRACTION OF TRACER THAT
C           *** IS DISSOLVED. ONLY FOR SO2 SO FAR.
C
              ASRSO2=MAX( 1.-AFRAS, ZERO )
              ZHENRY(IL,L)=ZHENRY(IL,L)+ASRSO2/REAL(YSUB)
            ENDIF
  350     CONTINUE
C
C         * DIAGNOSTIC FIELDS, SAVED OPTIONALLY.
C
          IF(ISVCHEM.GT.0) THEN
          DO 355 IL=1,ILGG        
            IF ( KCALC(ICHEM(IL),L) ) THEN
              IF ( AMHV(IL) > 1.E-20 ) THEN
                PHLROW(ICHEM(IL),L)=-LOG10(AMHV(IL))
              ELSE
                PHLROW(ICHEM(IL),L)=0.
              ENDIF
            ENDIF
  355     CONTINUE
          ENDIF
  360    CONTINUE
C
C        * UPDATE FIELDS.
C
         DO 380 L=1,ILEV 
         DO 380 IL=1,ILGG 
            IF ( KCALC(ICHEM(IL),L) ) THEN
               ADELTA = OGTSO2(IL,L)-ANTSO2(IL,L)
               ARESID(ICHEM(IL),L) = ARESID(ICHEM(IL),L)+ADELTA 
C
C---           final concentrations after ATIMST
               OGTSO2(IL,L) = ANTSO2(IL,L)
               AGTHO2(IL,L) = ANTHO2(IL,L)
               AGTSO4(IL,L) = ANTSO4(IL,L)
            ENDIF
  380    CONTINUE
  400 CONTINUE
C
C     * FINAL RESULTS.
C
      DO 500 L=1,ILEV                                                  
      DO 500 IL=IL1,IL2                                 
         IF ( KCALC(IL,L) ) THEN
C
C          *** SULPHATE FRACTION PRODUCED BY H2O2.
C
            ADELTAX=ARESID(IL,L)*YCOM3L*YMASS/ROAROW(IL,L)
            ADELTAX=MAX(ZERO,ADELTAX)
            ADELTAX=MIN(ZXTP1C(IL,L),ADELTAX)
            WRK3(IL,L)=ADELTAX
C
C         *** SULPHUR DIOXIDE IN KG/KG.
C
            ZXTP1C(IL,L) = ZXTP1C(IL,L)-ADELTAX
C
C         *** SULPHATE IN KG/KG.
C 
            ZSO4(IL,L) = ZSO4(IL,L)+ADELTAX
         ENDIF
  500 CONTINUE     
C
C     * DIAGNOSTICS OF PRODUCTION RATES.
C
      IF ( ISVCHEM.GT.0 ) THEN
       DO 520 L=1,ILEV                                                  
       DO 520 IL=1,ILGG                                 
         IF ( KCALC(ICHEM(IL),L) ) THEN
            AFACT=WRK3(ICHEM(IL),L)*PRESSG(ICHEM(IL))*ZCLF(ICHEM(IL),L)
     1           *DSHJ(ICHEM(IL),L)/GRAV/ZTMST
            ASUM=WRK1(IL,L)+WRK2(IL,L)
            IF ( ASUM.NE.0. ) THEN
               SLO3ROW(ICHEM(IL))=SLO3ROW(ICHEM(IL))+WRK1(IL,L)*AFACT
     1                           *SAVERAD/ASUM
               SLHPROW(ICHEM(IL))=SLHPROW(ICHEM(IL))+WRK2(IL,L)*AFACT
     1                           *SAVERAD/ASUM
            END IF
         END IF
 520   CONTINUE
      END IF
C
      DO 550 L=1,ILEV                                                  
      DO 550 IL=1,ILGG                                 
         IF ( KCALC(ICHEM(IL),L) ) THEN
            ADELTX = ZCLF(ICHEM(IL),L)*(AGTHO2(IL,L)-AOH2O2(IL,L)) 
     1                  *YCOM3L*YMASS/ROAROW(ICHEM(IL),L)
C
C         *** HYDROGEN PEROXIDE.
C
            ADELTAX = ZCLF(ICHEM(IL),L)*(AGTHO2(IL,L)-AOH2O2(IL,L)) 
     1                  *YCOM3L*YMASS/ROAROW(ICHEM(IL),L)
            XROW(ICHEM(IL),L+1,IHPO) = XROW(ICHEM(IL),L+1,IHPO)+ADELTAX 
         ENDIF
 550  CONTINUE

C
      RETURN
      END
