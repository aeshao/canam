      SUBROUTINE XTEMISS10
     1   (XEMIS,XROW,ILG,IL1,IL2,ILEV,LEV,NTRAC,ZTMST,KOUNT,
     2   JL,LNZ0ROW,FCANROW,ICANP1,TH,
     3   GTROWBS,USTARBS,GTROWW,PRESSG,DSHJ,DMSOROW,EDMSROW,
     4   SHJ,SICNROW,EDSOROW,EDSLROW,ZSPDSO,ZSPDSBS,
     5   FLNDROW,FWATROW,FNROL,
     6   SPOTROW,ST01ROW,
     7   ST02ROW,ST03ROW,ST04ROW,ST06ROW,
     8   ST13ROW,ST14ROW,ST15ROW,ST16ROW,
     9   ST17ROW,SUZ0ROW,PDSFROW,ISVDUST,
     A   FALLROW,FA10ROW,FA2ROW,FA1ROW,
     B   DUWDROW,DUSTROW,DUTHROW,USMKROW,
     C   CLAYROW,SANDROW,ORGMROW,BSFRAC,SMFRAC,
     D   ESDROW,IGND,IPAM,SICN_CRT,
     E   ISSA,ISSC,IDUA,IDUC,IDMS,ISO2,SAVERAD,ISVCHEM)
C
C     * THIS ROUTINE CALCULATES THE OCEANIC SURFACE DMS EMISSION
C     * (FROM SPECIFIED CONCENTRATIONS), THE SURFACE DUST EMISSIONS
C     * (ACCUMULATION AND COARSE MODES) AND THE VOLCANIC DRY
C     * DRY DEPOSITION FLUX.
C
C     * JUN 11/2018 - K.VONSALZEN.  - INCREASE CUSCALE FROM 1.0 TO
C     *                               1.6 FOR CMIP6.  
C     * AUG 09/2017 - M.LAZARE.     - REMOVE UNUSED "MASKROW".
C     * AUG 07/2017 - M.LAZARE.     - NEW GIT VERSION.
C     * SEP 13/2014 - K.VONSALZEN/  NEW VERSION FOR GCM18+:
C     *               M.LAZARE.     - USTARBS PASSED IN AND USED TO
C     *                               DEFINE USTAR IN DUSTEMI3, INSTEAD
C     *                               OF PREVIOUS CALCULATION.
C     *                             - UNUSED SNOROW AND CDM REMOVED.
C     *                             - ZSPDSBS (SURFACE BARE SOIL AVERAGED
C     *                               WINDSPEED) IS PASSED IN AND USED
C     *                               (INCLUDING PASSING IN TO DUSTEMI3)
C     *                               RATHER THAN THE GRID-AVERAGE SURFACE
C     *                               WIND COMPONENTS AND GUSTINESS SEPARATELY.
C     *                             - ZSPDSO (SURFACE-LEVEL WIND INCLUDING
C     *                               GUSTINESS, OVER OCEANS) IS PASSED IN AND
C     *                               USED INSTEAD OF THE GRID-AVERAGE LOWEST-
C     *                               LEVEL WIND "ZSPD".
C     *                             - ICTEMMOD REMOVED SINCE NO LONGER
C     *                               NEEDED TO DISTINGUISH BETWEEN
C     *                               SURFACE FLUX FOR CLASS VS CTEM.
C     *                             - FLNDROW AND FWATROW ARE PASSED IN
C     *                               INSTEAD OF {GCROW} AND USED
C     *                               TO DETERMINE SEPARATE 
C     *                               EMISSIONS OVER LAND/NOLAND,
C     *                               AS FOLLOWS:
C     *                             - AFAC IS SCALED BY
C     *                               FWATROW(IL) IN LOOP 10 AND
C     *                               IF CONDITION ON GC,MASK MODIFIED TO
C     *                               USE FLNDROW INSTEAD.
C     *                             - ZDMSCON IS SCALED BY FWATROW(IL)
C     *                               IN LOOP 20 AND IF CONDITION ON GC,MASK
C     *                               MODIFIED TO USE FLNDROW INSTEAD.
C     *                             - EDMSROW IS SCALED BY FLNDROW(IL) IN
C     *                               LOOP 30 FOR DMS EMISSIONS FROM LAND
C     *                               SOURCES AND IF CONDITION MODIFIED.
C     *                             - SEPARATE GTROW FOR LAND AND WATER
C     *                               (GTROWBS AND GTROWW, RESPECTIVELY)
C     *                               ARE PASSED IN RATHER THAN ONE
C     *                               GTROW. GTROWBS IS PASSED ON TO
C     *                               THE NEW DUSTEMI3 WHILE GTROWW
C     *                               IS USED IN THE SEASALT EMISSIONS.
C     *                             - A NEW VERSION OF THE DUST
C     *                               EMISSIONS IS CALLED (DUSTEMI3)
C     *                               WITH {GCROW,MASKROW} REPLACED
C     *                               BY FLNDROW.
C     *                             - SICN_CRT IS USED TO DETERMINE
C     *                               OPEN WATER FOR SEASALT EMISSIONS.
C     * NOV 15/2013 - K.VONSALZEN/  CUSCALE VALUE NOW DEPENDANT ON
C     *               M.LAZARE.     CTEM SWITCH "ICTEMMOD" (HIGHER
C     *                             TUNED VALUE FOR CTEM).
C     * JUN 18/2013 - K.VONSALZEN/  PREVIOUS VERSION XTEMISS9 FOR GCM17+:
C     *               M.LAZARE.     - PASSES IN "IPAM" AND SKIPS AROUND
C     *                               SEASALT AND DUST EMISSIONS (**NOT**
C     *                               DMS EMISSIONS) FOR PLA.
C     *                             - USES "DUPARM1" WITH "CUSCALE"
C     *                               REMOVED (DEFINED IN THIS ROUTINE NOW).
C     *                             - CALLS NEW "DUINIT1", PASSIN IN
C     *                               "CUSCALE".
C     *                             - CALLS NEW "DUSTEMI2", PASSING IN
C     *                               "CUSCALE".
C     *                             - MORE ACCURATE SCHMIDT NUMBER CALCULATION.            
C     * APR 24/2012 - Y.PENG/       PREVIOUS VERSION XTEMISS8 FOR GCM16:
C     *               K.VONSALZEN.  - NEW DUST SCHEME.
C     *                               THIS INCLUDES NEW "DUPARM" MODULE
C     *                               AND NEW SUBROUTINES "DUINIT" AND
C     *                               DUSTEMI, WHICH ARE CALLED.
C     * APR 26/2010 - K.VONSALZEN/  PREVIOUS VERSION XTEMISS7 FOR GCM15I:
C     *               M.LAZARE.     - DAFX,DAFC REPLACED BY ESD (BOTH MODES).
C     *                             - BSFRAC USED FOR DUST PRODUCTION,
C     *                               INSTEAD OF EQUIVILENT (1.-VGFRAC).
C     *                             - EMISSIONS NOW STORED INTO GENERAL
C     *                               "EMIS" ARRAY, INSTEAD OF UPDATING
C     *                               XROW AT THIS POINT.
C     *                             - A DIFFERENT UPLIFTC IS USED FOR
C     *                               CTEM (BASED ON TUNING), WITH "ICTEMMOD"
C     *                               PASSED IN ACCORDINGLY.
C     *                             - VIRTUAL TEMPERATURE EFFECT IS ADDED
C     *                               TO CALCULATE MOIST AIR DENSITY,
C     *                               RATHER THAN JUST USING DRY AIR DENSITY
C     *                               FOR "ZRHO0".
C     * FEB 16/2008 - K.VONSALZEN.  PREVIOUS VERSION XTEMISS6 FOR GCM15H:
C     *                             - ADD DMS DUST SOURCE OVER LAND.
C     * JAN 17/2008 - K.VONSALZEN/  PREVIOUS VERSION XTEMISS5 FOR GCM15G:
C     *               M.LAZARE/     - ADD GUSTINESS EFFECT FOR ALL SPECIES
C     *               X.MA.           CONSISTENTLY THROUGH PASSING IN AND
C     *                               USING "ZSPD" WHICH HAS GUSTINESS
C     *                               INCORPORATED INTO IT IN PHYSICS DRIVER.
C     *                             - REMOVE CALCULATION OF REMOVED TSEM.
C     *                             - PASS IN "ADELT" FROM PHYSICS AS
C     *                               ZTMST AND USE DIRECTLY, RATHER THAN
C     *                               PASSING IN DELT AND FORMING
C     *                               ZTMST=2.*DELT INSIDE.
C     *                             - REMOVE UPDATING OF SO2 FROM OLD
C     *                               VOLCANOES FORMULATION AND REMOVE
C     *                               PASSING IN OF NOW REMOVED EVOL,HVOL.
C     *                             - REMOVE CALCULATION OF UNUSED "TSEM"
C     *                               AND TDEM->EDSO.
C     * JUN 19/2006 - M.LAZARE.     PREVIOUS VERSION XTEMISS4 FOR GCM15F:
C     *                             - USE VARIABLE INSTEAD OF CONSTANT
C     *                               IN INTRINSICS SUCH AS "MAX",
C     *                               SO THAT CAN COMPILE IN 32-BIT MODE
C     *                               WITH REAL*8.  
C     *                             - "KEEPTIM" COMMON BLOCK REMOVED (WASN'T
C     *                               USED).    
C     *                             - WORK ARRAYS NOW LOCAL.
C     * DEC 12/2005 - C.READER/     PREVIOUS VERSION XTEMISS3 FOR GCM15D/E:
C     *               M.LAZARE.     - DRAG PARTITION FORMULATION FOR SPEDT.
C     *                             - GUSTINESS PARAMETERIZATION ADDED FOR DUST.
C     *                             - SUPPRESSION OF DUST OVER FROZEN GROUND.
C     *                             - LIMIT ZSST TO MIN(ZSST,36.) SO THAT
C     *                               SCHMIDT NUMBER IS POSITIVE AND THUS
C     *                               DMS FLUX FORMULATION IS NOT ILL-BEHAVED.
C     * DEC 07/2004 - C.READER/     PREVIOUS VERSION XTEMISS2 FOR GCM15C:
C     *               M.LAZARE.     - VOLCANIC EMISSIONS NOWCONSISTENT
C     *                               WITH SPECIFIED "HVOL" FIELD BEING
C     *                               ABOVE GROUND AND NOT SEA-LEVEL
C     *                               (AND VOLCANIC ERUPTIONS PERMITTED
C     *                               IN LOWEST LAYER).
C     *                             - MANY CHANGES TO DUST EMISSIONS
C     *                               (NEW UPLIFTC, PASS IN CDM AND USE
C     *                               TO CALCULATE USTAR,WLT1 AND SPEDT
C     *                               NOW CALCULATED RATHER THAN 
C     *                               SPECIFIED DATA CONSTANTS,WLT2 AND
C     *                               SNOWT NO LONGER USED AND THUS
C     *                               REMOVED AS WELL, PLFX0=0. FOR
C     *                               USTAR.LE.SPEDT).
C     * OCT 24/2002 - K.VONSALZEN/- PREVIOUS VERSION XTEMISS FOR GCM15B
C     *               M.LAZARE/     (MODIFIED FROM LOHMAN ORIGINAL).
C     *               C.READER.
C     * MIKE LAZARE,ULRIKE LOHMAN  CCCMA,DALHOUSIE   08/2000         
C     * JOHANN FEICHTER          UNI/HAMBURG         08/91
C     * MODIFIED  U. SCHLESE    DKRZ-HAMBURG        JAN-95
C
      USE DUPARM1
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * I/O FIELDS.
C
      REAL   GTROWBS(ILG),USTARBS(ILG),GTROWW(ILG)
      REAL   DSHJ(ILG,ILEV),PRESSG(ILG)
      REAL   DMSOROW(ILG),EDMSROW(ILG)
      REAL   XEMIS(ILG,ILEV,NTRAC),XROW(ILG,LEV,NTRAC)
      REAL   LNZ0ROW(ILG,ICANP1),FCANROW(ILG,ICANP1)
      REAL   ZSPDSO (ILG),ZSPDSBS(ILG)
      REAL   FLNDROW(ILG),FWATROW(ILG)
      REAL   SICNROW(ILG),EDSOROW(ILG),EDSLROW(ILG)
      REAL   SHJ(ILG,ILEV),TH(ILG,ILEV)
      REAL   CLAYROW(ILG,IGND),SANDROW(ILG,IGND),ORGMROW(ILG,IGND)
      REAL   BSFRAC(ILG),SMFRAC(ILG)
      REAL   ESDROW(ILG)
      REAL   SSFRAC1,SSFRAC2,WLT1,PFLX1,PFLX2
      REAL   SPEDT,UPLIFTC
      REAL   FALLROW(ILG),FA10ROW(ILG),FA2ROW(ILG),FA1ROW(ILG)
      REAL   DUWDROW(ILG),DUSTROW(ILG),DUTHROW(ILG),USMKROW(ILG)
C
      REAL   SFCUROW(ILG),SFCVROW(ILG),GUSTROL(ILG),FNROL(ILG)
      REAL   SUZ0ROW(ILG),PDSFROW(ILG)
      REAL   SPOTROW(ILG),ST01ROW(ILG)
      REAL   ST02ROW(ILG),ST03ROW(ILG),ST04ROW(ILG),ST06ROW(ILG)
      REAL   ST13ROW(ILG),ST14ROW(ILG),ST15ROW(ILG),ST16ROW(ILG)
      REAL   ST17ROW(ILG)

      INTEGER JL(ILG)
C
C     * INTERNAL WORK FIELDS.
C
      REAL   UTH(NCLASS),SREL(NATS,NCLASS)
      REAL   SRELV(NATS,NCLASS),SU_SRELV(NATS,NCLASS)
      REAL  ,  DIMENSION(7)  :: APAA,APAC,APBA,APBC
C
      COMMON /HTCP/   T1S,T2S,AI,BI,AW,BW,SLP
      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES
      COMMON /PARAMS/ RGASV,CPRESV
C
      DATA APAA / 0.   , -5.49,  -30.18,   10.69,   17.55,   7.56,
     1            7.56 /
      DATA APAC / 0.   , -2.63, -104.15, -239.02, -139.53, -44.08,
     1           -44.08 /
      DATA APBA / .490, 1.405,  4.148,   .433,  -.095,   .571,   .571 /
      DATA APBC / .295,  .732, 12.012, 24.274, 16.621, 10.257, 10.257 /
C
C     * VARIABLE ARGUMENTS FOR MIN/MAX INTRINSICS.
C
      DATA ONE /1./
      DATA SSTMAX /36./
      DATA SSTMIN /-1.8/
      DATA OPEM4 /0.0001/
      DATA WLT1MIN,WLT1MAX /0.1, 0.15/
C --------------------------------------------------------------
C     * TUNING PARAMETER FOR BULK DUST
C     * (SCALE FACTOR FOR WIND STRESS THRESHOLD).
C
      CUSCALE=1.6
C
C     * 1.   SURFACE EMISSION.
C            ------- --------
C
C     * DRY SEA SALT CONCENTRATION IN FIRST MODEL LAYER (IN KG/KG).
C     * DUST SOURCE FLUX CALCULATION.
C
      ATUNE=1.3
      IF ( IPAM.EQ.0 ) THEN
       DO 10 IL=IL1,IL2
        IF(FWATROW(IL).GT.0. .AND. SICNROW(IL).LE.SICN_CRT) THEN
         ZRHO0=SHJ(IL,ILEV)*PRESSG(IL)/(RGAS*TH(IL,ILEV))
         DZTR=(DSHJ(IL,ILEV)*PRESSG(IL))/GRAV
         AFAC=FWATROW(IL)*(1.-SICNROW(IL))
         ZSPDGUST=ZSPDSO(IL)
         IF ( ZSPDGUST.LE.6. ) THEN
           ASSA=(APAA(1)+APBA(1)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(1)+APBC(1)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE IF ( ZSPDGUST.LE. 9. ) THEN
           ASSA=(APAA(2)+APBA(2)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(2)+APBC(2)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE IF ( ZSPDGUST.LE.11. ) THEN
           ASSA=(APAA(3)+APBA(3)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(3)+APBC(3)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE IF ( ZSPDGUST.LE.13. ) THEN
           ASSA=(APAA(4)+APBA(4)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(4)+APBC(4)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE IF ( ZSPDGUST.LE.15. ) THEN
           ASSA=(APAA(5)+APBA(5)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(5)+APBC(5)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE IF ( ZSPDGUST.LE.18. ) THEN
           ASSA=(APAA(6)+APBA(6)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(6)+APBC(6)*ZSPDGUST)/ZRHO0*1.E-09
         ELSE
           ASSA=(APAA(7)+APBA(7)*ZSPDGUST)/ZRHO0*1.E-09
           ASSC=(APAC(7)+APBC(7)*ZSPDGUST)/ZRHO0*1.E-09
         END IF
         XEMIS(IL,ILEV,ISSA)=XEMIS(IL,ILEV,ISSA) +
     1                             AFAC * (ATUNE*ASSA-XROW(IL,LEV,ISSA))
         XEMIS(IL,ILEV,ISSC)=XEMIS(IL,ILEV,ISSC) +
     1                             AFAC * (ATUNE*ASSC-XROW(IL,LEV,ISSC)) 
        ENDIF
   10  CONTINUE
C
       CALL DUINIT1(UTH,SREL,SRELV,SU_SRELV,CUSCALE)
C
       CALL DUSTEMI3
     1    (FLNDROW,GTROWBS,PRESSG,DSHJ,SMFRAC,FNROL,BSFRAC,
     2    ZSPDSBS,USTARBS,SUZ0ROW,PDSFROW, 
     3    SPOTROW,ST01ROW,ST02ROW,ST03ROW,ST04ROW,ST06ROW,
     4    ST13ROW,ST14ROW,ST15ROW,ST16ROW,ST17ROW,
     5    ESDROW,XEMIS,IDUA,IDUC,SAVERAD,ISVDUST,
     6    UTH,SREL,SRELV,SU_SRELV,FALLROW,FA10ROW,FA2ROW,FA1ROW,
     7    DUWDROW,DUSTROW,DUTHROW,USMKROW,
     8    ILG,IL1,IL2,ILEV,LEV,NTRAC,ZTMST,CUSCALE)
      ENDIF
C
C     * CALCULATE DMS EMISSIONS FOLLOWING LISS+MERLIVAT
C     * DMS SEAWATER CONC. FROM KETTLE ET AL.
C     * LIMIT ZSST SO FORMULATION FOR SCHMIDT NUMBER REMAINS
C     * POSITIVE AND REASONABLY SO.
C
      DO 20 IL=IL1,IL2
       IF(FWATROW(IL).GT.0. .AND. SICNROW(IL).LE.SICN_CRT) THEN
        ZDMSCON=DMSOROW(IL)*FWATROW(IL)*(1.-SICNROW(IL))
        ZSST=MAX( SSTMIN, MIN(GTROWW(IL)-273.15,SSTMAX) )
C
C       * SCHMIDT NUMBER ACCORDING TO SALTZMAN ET AL. (1993).
C
        ZSCHMIDT=2674.0-147.12*ZSST+3.726*ZSST**2-0.038*ZSST**3
C
C       * GAS TRANSFER VELOCITY AT SCHMIDT NUMBER OF 600 
C       * ACCORDING TO NIGTINGALE ET AL. (2000).
C
        ZKW=0.222*ZSPDSO(IL)**2+0.333*ZSPDSO(IL)
C
C       * GAS TRANSFER VELOCITY AT CURRENT SCHMIDT NUMBER, AIRSIDE
C       * TRANSFER VELOCITY, AND FINAL TRANSFER VELOCITY (MCGILLIS 
C       * ET AL., 2000).
C
        ZKW=ZKW*SQRT(600./ZSCHMIDT)
        AMWR=18.0153/62.13
        ZKA=659.*ZSPDSO(IL)*SQRT(AMWR)
        ALPHA=EXP(3525./(ZSST+273.15)-9.464) 
        GAMMA=1./(1.+ZKA/(ALPHA*ZKW))
        ZVDMS=ZKW*(1.-GAMMA)
C
C       * DMS FLUX IN KG/M2/SEC, CONVERTED FROM NANOMOL/LTR*CM/HOUR.
C
        ZDMSEMISS=ZDMSCON*ZVDMS*32.064E-11/3600.
C
C       * NANOMOL/LTR*CM/HOUR --> KG/M**2/SEC
C
        XEMIS(IL,ILEV,IDMS)=XEMIS(IL,ILEV,IDMS)+ZDMSEMISS*
     1                        ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))
        IF(ISVCHEM.NE.0)     THEN
          EDSOROW(IL)=EDSOROW(IL)+ZDMSEMISS*SAVERAD
        ENDIF
       ENDIF
   20 CONTINUE
C
C     * DMS EMISSIONS FROM LAND SOURCES.
C
      DO 30 IL=IL1,IL2
       IF(FLNDROW(IL).GT.0.) THEN
        FACT=ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))
        EDMSLND=EDMSROW(IL)*FLNDROW(IL)
        XEMIS(IL,ILEV,IDMS)=XEMIS(IL,ILEV,IDMS)+EDMSLND*FACT
        IF(ISVCHEM.NE.0)     THEN
          EDSLROW(IL)=EDSLROW(IL)+EDMSLND*SAVERAD
        ENDIF
       ENDIF
   30 CONTINUE
C
C     * END OF SURFACE EMISSIONS
C
      RETURN
      END
