      SUBROUTINE XTEWF5(XEMIS,EBWAROW,EOWAROW,ESWAROW,DSHJ,PRESSG,ZF, 
     1                  LEVWF, NTRAC, ZTMST, ILEV, LEV, IL1, IL2, ILG,
     2                  IOCO,IOCY,IBCO,IBCY,IDMS,ISO2)
C
C     * JUN 17/2013 - K.VONSALZEN/ NEW VERSION FOR GCM17+:
C     *               M.LAZARE.    - ADD CONDITION TO SET WGT=1. FOR
C     *                              SPECIAL CASE WHERE HSO2=LSO2.
C     * APR 28/2012 - K.VONSALZEN/ PREVIOUS VERSION XTEWF4 FOR GCM16:
C     *               M.LAZARE.    - INITIALIZE LSO2 AND HSO2 TO
C     *                              ILEV INSTEAD OF ZERO, TO
C     *                              AVOID POSSIBLE INVALID 
C     *                              MEMORY REFERENCES.
C     *                            - BUGFIX FOR INCORRECT MEMORY
C     *                              REFERENCE: LSO2(L)->LSO2(IL).
C     *                            - ELIMINATE RESETTING OF LSO2 TO
C     *                              ILEV IF RLH IS NOT POSITIVE.
C     * APR 26/2010 - K.VONSALZEN. PREVIOUS VERSION XTEWF3 FOR GCM15I:
C     *                            - ZF AND PF CALCULATED ONE-TIME AT
C     *                              BEGINNING OF PHYSICS AND ZF
C     *                              PASSED IN, WITH INTERNAL CALCULATION
C     *                              THUS REMOVED AND NO NEED TO PASS
C     *                              IN SHBJ,T.
C     *                            - EMISSIONS ACCUMULATED INTO GENERAL
C     *                              "XEMIS" ARRAY RATHER THAN UPDATING
C     *                              XROW (XROW UPDATED FROM XEMIS IN
C     *                              THE PHYSICS). 
C     *                            - REMOVAL OF DIAGNOSTIC FIELDS
C     *                              CALCULATION, SO NO NEED TO PASS
C     *                              IN "SAVERAD" OR "ISVCHEM".
C     *                            - WGT=0 (IE NO EMISSIONS IN LAYER)
C     *                              IF IT IS ABOVE TOP OR BELOW BASE
C     *                             OF EMISSIONS.
C     * MAR 30/2009 - K.VONSALZEN.  PREVIOUS VERSION XTEWF2 FOR GCM15H:
C     *                             - CORRECT UNITS FOR OC EMISSIONS
C     *                             - ADD DIAGNOSTIC FIELDS
C     * MAR 06/07 - X.MA.           PREVIOUS VERSION XTEWF FOR GCM15G:
C     *                             INTERPOLATE THE EMISSIONS TO THE
C     *                             MODEL LEVELS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C     
C     * INPUT FIELDS
C
      REAL EBWAROW(ILG,LEVWF),EOWAROW(ILG,LEVWF),ESWAROW(ILG,LEVWF) 
C
C     * I/O FIELDS.
C
      REAL DSHJ(ILG,ILEV), PRESSG(ILG), ZF(ILG,ILEV)
      REAL XEMIS(ILG,ILEV,NTRAC)
C
C     * INTERNAL WORK FIELDS.
C
      REAL  , DIMENSION(ILG,LEVWF)  :: EOWA
      REAL  ,  DIMENSION(ILG) ::  T_EBWA,T_BWA
      REAL  ,  DIMENSION(ILG) ::  T_EOWA,T_OWA
      REAL  ,  DIMENSION(ILG) ::  T_ESWA,T_SWA
      INTEGER, DIMENSION(ILG) ::  LSO2,HSO2
C
      COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES, RGASV, CPRESV
C
C     * LOCAL ARRAY...
C
      REAL   ZREF(6)
C
C     * HEIGHT LEVELS OF INPUT FIELD USED FOR INTERPOLATION.
C
      DATA ZREF /100., 500., 1000., 2000., 3000., 6000./
C----------------------------------------------------------------------
      POM2OC=1.4
      DO K=1,LEVWF
      DO IL=IL1,IL2
        EOWA(IL,K)=EOWAROW(IL,K)/POM2OC
      ENDDO
      ENDDO
C
C * FOR DIAGNOSTIC PURPOSE: TO MAKE SURE THE TOTAL EMISSIONS  
C        AFTER INTERPOLATION ARE SAME AS BEFORE.
C * T_E*WA: TOTAL FROM VALUES BEFORE INTERPOLATION;
C * T_*WA:  TOTAL FROM VALUES AFTER  INTERPOLATION;
C
      DO 90 IL = IL1, IL2
        T_EBWA(IL) = 0.
        T_EOWA(IL) = 0.
        T_ESWA(IL) = 0.
        T_BWA (IL) = 0.
        T_OWA (IL) = 0.
        T_SWA (IL) = 0.
   90 CONTINUE
C      
      DO 180 K = 1, LEVWF

       RUH = ZREF(K)
        IF(K.EQ.1) THEN
       RLH = 0.
        ELSE
       RLH = ZREF(K-1)
        ENDIF
       DUL = RUH-RLH
C
C     * DETERMINE LAYER CORRESPONDING TO SO2 EMISSIONS.
C
      DO 100 IL = IL1, IL2
        LSO2(IL) = ILEV
        HSO2(IL) = ILEV
  100 CONTINUE

      DO 105 L = ILEV, 1, -1
      DO 105 IL = IL1, IL2
          IF( ZF(IL,L) .LE. RLH)   LSO2(IL)=L
          IF( ZF(IL,L) .LE. RUH)   HSO2(IL)=L
  105 CONTINUE
C
C   * EBWA
C
      DO 108 IL = IL1, IL2
        T_EBWA(IL) = T_EBWA(IL) + EBWAROW(IL,K)
        T_EOWA(IL) = T_EOWA(IL) + EOWA(IL,K)
        T_ESWA(IL) = T_ESWA(IL) + ESWAROW(IL,K)
  108 CONTINUE
C
      DO 110 L=1, ILEV
      DO 110 IL = IL1, IL2
        FACT                   = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
          WGT=1.
        ELSE IF(L.EQ.HSO2(IL)) THEN
         WGT=( RUH-ZF(IL,L) )/DUL
        ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(IL) ) THEN
         WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
        ELSE IF( L.EQ.LSO2(IL) ) THEN
         WGT=( ZF(IL,L-1)-RLH )/DUL
        ELSE
         WGT=0.
        ENDIF
        T_BWA(IL) = T_BWA(IL) + WGT*EBWAROW(IL,K)
        XEMIS(IL,L,IBCO)=XEMIS(IL,L,IBCO)+0.8*WGT*EBWAROW(IL,K)*FACT
        XEMIS(IL,L,IBCY)=XEMIS(IL,L,IBCY)+0.2*WGT*EBWAROW(IL,K)*FACT
  110 CONTINUE
C
C   * EOWA
C
      DO 120 L=1, ILEV
      DO 120 IL = IL1, IL2
        FACT                 = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
          WGT=1.
        ELSE IF(L.EQ.HSO2(IL)) THEN
         WGT=( RUH-ZF(IL,L) )/DUL
        ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(IL) ) THEN
         WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
        ELSE IF( L.EQ.LSO2(IL) ) THEN
         WGT=( ZF(IL,L-1)-RLH )/DUL
        ELSE
         WGT=0.
        ENDIF
        T_OWA(IL) = T_OWA(IL) + WGT*EOWA(IL,K)
        XEMIS(IL,L,IOCO)=XEMIS(IL,L,IOCO)+0.5*WGT*EOWA(IL,K)*FACT
        XEMIS(IL,L,IOCY)=XEMIS(IL,L,IOCY)+0.5*WGT*EOWA(IL,K)*FACT
  120 CONTINUE
C
C   * ESWA
C
      JT=ISO2
C
      DO 130 L=1, ILEV
      DO 130 IL = IL1, IL2
         FACT                 = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF(L.EQ.HSO2(IL) .AND. L.EQ.LSO2(IL)) THEN
          WGT=1.
        ELSE IF(L.EQ.HSO2(IL)) THEN
          WGT=( RUH-ZF(IL,L) )/DUL
         ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(IL) ) THEN
          WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
         ELSE IF( L.EQ.LSO2(IL) ) THEN
          WGT=( ZF(IL,L-1)-RLH )/DUL
         ELSE
          WGT=0.
         ENDIF
         T_SWA(IL) = T_SWA(IL) + WGT*ESWAROW(IL,K)
         XEMIS(IL,L,JT)=XEMIS(IL,L,JT)+WGT*ESWAROW(IL,K)*FACT
     1                                                   *32.064/64.059
  130 CONTINUE
  180 CONTINUE
C
C   --------------------------------------------------------------
C
      RETURN
      END
