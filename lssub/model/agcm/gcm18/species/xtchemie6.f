      SUBROUTINE XTCHEMIE6(XROW,
     1                    TH,QV,DQLDT,DSHJ,SHJ,CSZROW,PRESSG,
     2                    SFRC,OHROW,H2O2ROW,O3ROW,NO3ROW,
     3                    HNO3ROW,NH3ROW,NH4ROW,WDLD,WDLB,WDLO,
     4                    WDLS,SLO3ROW,SLHPROW,PHLROW,ISVCHEM,
     5                    WDL4,WDL6,DEPB,DOX4ROW,DOXDROW,NOXDROW, 
     6                    ZCLF,ZMRATEP,ZFSNOW,ZFRAIN,ZMLWC,ZFEVAP,
     7                    CLRFR,CLRFS,ZFSUBL,ITRAC,ITRPHS,ITRWET,ISULF,
     8                    ISO2,IDMS,ISO4,IHPO,ISSA,ISSC,IDUA,IDUC,
     9                    IBCO,IBCY,IOCO,IOCY,KOUNT,ZTMST,NLATJ,
     A                    SAVERAD,NTRAC,ILG,IL1,IL2,ILEV,LEV)
C
C**** *XTCHEMIE5*  CALCULATES DRY AND WET CHEMISTRY
C
C     * JUN 20/2013 - M.NAMAZI.     NEW VERSION FOR GCM17:
C     *                             - CALCULATE "DEPB" AND PASS OUT
C     *                               TO PHYSICS.
C     * APR 25/2010 - M.LAZARE/     PREVIOUS VERSION XTCHEMIE5 FOR 
C     *               K.VONSALZEN.  GCM15I/GCM16:
C     *                             - CALLS NEW OXISTR3.
C     *                             - IF CONDITION FOR NIGHT-TIME
C     *                               CHEMISTRY NOW PROPERLY BASED
C     *                               ON "CSZROW(IL).LE.0." INSTEAD
C     *                               OF "CSZROW(IL).LT.0.".
C     *                             - UNDER CONTROL OF "ISVCHEM" FOR
C     *                               ADDITIONAL DIAGNOSTIC CHEMISTRY
C     *                               FIELDS, {WDLA,WDLC} REMOVED AND
C     *                               {WDLD,WDLB,WDLO,WDLS} (FOR DUST,
C     *                               BLACK CARBON, ORGANIC CARBON AND
C     *                               SEA-SALT, RESPECTIVELY) ADDED.
C     *                               ALSO, CLPHROW REMOVED AND 
C     *                               CALCULATION OF ALL DIAGNOSTIC
C     *                               FIELDS REVISED AND STREAMLINED.
C     *                             - ASRSO4 INCREASED FROM 0.75 TO 0.9.
C     * FEB 16/2009 - K.VONSALZEN.  PREVIOUS VERSION FOR GCM15H:   
C     *                             - INITALIZE IN-CLOUD AND CLEAR-SKY
C     *                               TRACER MIXING RATIOS USING THE 
C     *                               RATIO OF IN-CLOUD OVER CLEAR-SKY
C     *                               FROM THE PREVIOUS TIME STEP.
C     *                             - CALLS NEW WETDEP4. 
C     * DEC 18/2007 - K.VONSALZEN/  PREVIOUS VERSION XTCHEMIE3 FOR GCM15G:
C     *               M.LAZARE.     - REMOVE UNUSED KHANS=.TRUE. CODE.
C     *                             - PASS IN "ADELT" FROM PHYSICS AS
C     *                               ZTMST AND USE DIRECTLY, RATHER THAN
C     *                               PASSING IN DELT AND FORMING
C     *                               ZTMST=2.*DELT INSIDE.C
C     *                             - CALLS NEW WETDEP3.     
C     *                             - REMOVE SETTING OF ZCLF TO ZERO IN
C     *                               LOOP 210 IF ZCLF<1.E-4.
C     * NOV 23/2006 - M.LAZARE.     PREVIOUS VERSION XTCHEMIE2 FOR GCM15F:
C     *                             - OPTIMIZED TO AVOID UNNECESSARY 
C     *                               CALCULATIONS FOR KHANS=.TRUE.
C     *                             - ZDEP NOW AN INTERNAL WORK ARRAY
C     *                               AND NOT PASSED OUT.
C     *                             - CALLS REVISED WETDEP2 and OXISTR2.
C     *                             - USE VARIABLE INSTEAD OF CONSTANT
C     *                               IN INTRINSICS SUCH AS "MAX",
C     *                               SO THAT CAN COMPILE IN 32-BIT MODE
C     *                               WITH REAL*8.  
C     *                             - WORK ARRAYS NOW LOCAL.
C     *                             - ZZO3 NOW A WORK ARRAY AND NOT 
C     *                               PASSED (NEVER REALLY USED IN PHYSICS). 
C     * OCT 24/2002 - K.VONSALZEN.  PREVIOUS VERSION XTCHEMIE UP TO GCM15E.
C      M.LAZARE/K.VONSALZEN    CCCMA          30/08/2000.
C      J. FEICHTER             UNI HAMBURG    30/06/92
C
C      PURPOSE
C      ---------
C      THIS ROUTINE COMPUTES THE OXIDATION AND THE WET SCAVENGING
C      OF CHEMICAL SPECIES.
C
C**    INTERFACE
C      -----------
C      *XTCHEMIE5*   IS CALLED FROM   *PHYSICS*
C
C      EXTERNALS
C      ------------
C          *WETDEP4* CALCULATES THE WET DEPOSITION
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   XROW(ILG,LEV,NTRAC), SFRC(ILG,ILEV,NTRAC)
C
      REAL   TH  (ILG,ILEV),QV  (ILG,ILEV), DQLDT(ILG,ILEV),
     1       DSHJ(ILG,ILEV),SHJ (ILG,ILEV)
C
      REAL   CSZROW(ILG),PRESSG(ILG)
C
C     * SPECIES PASSED IN VMR (DIMENSIONLESS).
C
      REAL   OHROW  (ILG,ILEV), H2O2ROW(ILG,ILEV), O3ROW  (ILG,ILEV),
     1       NO3ROW (ILG,ILEV), HNO3ROW(ILG,ILEV), NH3ROW (ILG,ILEV),
     2       NH4ROW (ILG,ILEV) 
C
      REAL   WDL4(ILG),WDL6(ILG),DOX4ROW(ILG),DOXDROW(ILG),NOXDROW(ILG)
C
C     * ARRAYS SHARED WITH COND IN PHYSICS.
C
      REAL   WDLD(ILG), WDLB(ILG), WDLO(ILG), WDLS(ILG), DEPB(ILG)
      REAL   ZCLF(ILG,ILEV),ZMRATEP(ILG,ILEV),ZFRAIN(ILG,ILEV),
     1       CLRFR(ILG,ILEV),ZFSNOW(ILG,ILEV),ZMLWC(ILG,ILEV),
     2       ZFEVAP(ILG,ILEV),CLRFS(ILG,ILEV),ZFSUBL(ILG,ILEV)
C
C     * OPTIONAL DIAGNOSTIC ARRAYS UNDER CONTROL OF "ISVCHEM":
C
      REAL   PHLROW(ILG,ILEV)
      REAL   SLO3ROW(ILG),SLHPROW(ILG)
C
C     * CONTROL INDEX ARRAYS FOR TRACERS.
C
      INTEGER ITRPHS(NTRAC), ITRWET(NTRAC) 
C
C     * INTERNAL WORK ARRAYS:
C
C     * GENERAL WORK ARRAYS FOR XTCHEMIE5
C
      REAL  ,  DIMENSION(ILG,ILEV,NTRAC)  :: ZXTE, XCLD, XCLR
      REAL  ,  DIMENSION(ILG,ILEV)  :: ZXTP10,ZXTP1C,ZHENRY,ZSO4,
     1                                 ZRKH2O2,ZZOH,ZZH2O2,ZZO3,
     2                                 ZZNO3,AGTHPO,ZRHO0,ZRHCTOX,
     3                                 ZRHXTOC,ZDEP3D,PDCLR,PDCLD
      REAL  ,  DIMENSION(ILG)  :: ZA21,ZA22,ZDT,ZE3,ZFAC1,ZF_O3,
     1                            ZF_SO2,ZH2O2M,ZSO2M,ZSO4M,ZSUMH2O2,
     2                            ZSUMO3,ZXTP1,ZZA,ZDAYL
C
C     * WORK ARRAYS USED IN OXISTR3 ONLY.
C
      PARAMETER (NEQP=13)
      LOGICAL, DIMENSION(ILG,ILEV)  ::  KCALC
      REAL  ,  DIMENSION(ILG,ILEV,NEQP)  :: ACHPA
      REAL  ,  DIMENSION(ILG,ILEV)  :: ROAROW,THG,AGTSO2,AGTSO4,
     1                                 AGTO3,AGTCO2,AGTHO2,AGTHNO3,
     2                                 AGTNH3,ANTSO2,ANTHO2,ANTSO4,
     3                                 AOH2O2,ARESID,WRK1,WRK2,WRK3 
      INTEGER, DIMENSION(ILG)       :: ILWCP,ICHEM
C
      COMMON /EPS/    A,B,EPS1,EPS2
      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES
      COMMON /PARAMS/ RGASV,CPRESV
      COMMON /HTCP/   T1S,T2S,AI,BI,AW,BW,SLP
C
      DATA YTAU / 1800. /                                      
C
      DATA ZERO,ONE,PLARGE /0., 1., 1.E+02/
      DATA ZFARR1,ZFARR2,ZFARR3,ZFARR4 /8.E+04, -3650., 9.7E+04, 6600./
C=======================================================================
C
C     DEFINE FUNCTION FOR CHANGING THE UNITS
C     FROM MASS-MIXING RATIO TO MOLECULES PER CM**3 AND VICE VERSA
      XTOC(X,Y)=X*6.022E+20/Y
      CTOX(X,Y)=Y/(6.022E+20*X)
C   X = DENSITY OF AIR, Y = MOL WEIGHT IN GRAMM
C
      ZFARR(ZK,ZH,ZTPQ)=ZK*EXP(ZH*ZTPQ)
C-----------------------------------------------------------------------
C
C    CONSTANTS
C
      PCONS2=1./(ZTMST*GRAV)
      PQTMST=1./ZTMST
      ZMIN=1.E-20
      NITER=3
      ASRSO4 = 0.9
      ASRPHOB= 0.
C
C    REACTION RATE SO2-OH
      ZK2I=2.0E-12
      ZK2=4.0E-31
      ZK2F=0.45
C   REACTION RATE DMS-NO3
      ZK3=1.9E-13
C   MOLECULAR WEIGHTS IN G
      ZMOLGS=32.064
      ZMOLGH2O2=34.01474
      ZMOLGAIR=28.84
      ZMOLGW=18.015
C
      ZHPBASE=2.5E-06
      ZE1K=1.1E-02
      ZE1H=2300.
      ZE2K=1.23
      ZE2H=3020.
      ZE3K=1.2E-02
      ZE3H=2010.
      ZE4K=6.6E-08
      ZE4H=1510.
      ZQ298=1./298.
      ZRGAS=8.2E-02
C
      ZAVO=6.022E+23
      ZNAMAIR=1.E-03*ZAVO/ZMOLGAIR
C
      ZLWCMIN=1.E-07
C
C  DEFINE CONSTANT 2-D SLICES.
C
      DO 100 JK=1,ILEV
      DO 100 IL=IL1,IL2
        ZRHO0(IL,JK)=SHJ(IL,JK)*PRESSG(IL)/(RGAS*TH(IL,JK)
     1              *(1.+(RGASV/RGAS-1.)*QV(IL,JK)))
        ZRHXTOC(IL,JK)=XTOC(ZRHO0(IL,JK),ZMOLGS)
        ZRHCTOX(IL,JK)=CTOX(ZRHO0(IL,JK),ZMOLGS)
  100 CONTINUE 
C
C  OXIDANT CONCENTRATIONS in MOLECULE/CM**3
C
      DO 115 JK=1,ILEV
      DO 115 IL=IL1,IL2
        ZX=ZRHO0(IL,JK)*1.E-03
C
C---    convert to molecules/cm3 from VMR
C
        FACTCON = ZX*6.022045E23/ZMOLGAIR
        ZZOH  (IL,JK)=OHROW  (IL,JK)*FACTCON
        ZZH2O2(IL,JK)=H2O2ROW(IL,JK)*FACTCON
        ZZO3  (IL,JK)=O3ROW  (IL,JK)*FACTCON
        ZZNO3 (IL,JK)=NO3ROW (IL,JK)*FACTCON
  115 CONTINUE
      IF(KOUNT.LT.2) RETURN
C----------------------------------------------------------------- 
C  CALCULATE HYDROGEN PEROXIDE PRODUCTION AND INITIALIZE FIELDS.  
C
      DO 120 JK=1,ILEV
      DO 120 IL=IL1,IL2
C                                                                  
C---    convert molecules/cm**3 to kg-S/kg                      
C
        AGTHPO(IL,JK) = ZZH2O2(IL,JK) * 1.E+06 * 32.06E-03      
     1                / (6.022045E+23 * ZRHO0(IL,JK))                  
C                                                                  
        IF (KOUNT.EQ.2) XROW(IL,JK+1,IHPO)=AGTHPO(IL,JK)        
        XROW(IL,JK+1,IHPO) = XROW(IL,JK+1,IHPO)                   
     1                     - MIN ( ZTMST/YTAU, ONE )                 
     2                     * ( XROW(IL,JK+1,IHPO) - AGTHPO(IL,JK) )   
C
        ZDEP3D(IL,JK)=0.
  120 CONTINUE
C
      DO 200 JT=1,NTRAC
      DO 200 JK=1,ILEV
      DO 200 IL=IL1,IL2
        ZXTE(IL,JK,JT)=0.
  200 CONTINUE
C
      DO 212 JK=1,ILEV
        DO 210 IL=IL1,IL2
         ZHENRY(IL,JK)=0.
         IF(ZCLF(IL,JK).LT.1.E-04.OR.ZMLWC(IL,JK).LT.ZMIN) THEN                
           ZMRATEP(IL,JK)=0.            
           ZMLWC(IL,JK)=0.  
         ENDIF  
  210   CONTINUE
  212 CONTINUE
C
C   CALCULATES PROCESSES WHICH ARE DIFERENT INSIDE AND OUTSIDE OF CLOUDS
C
      JT=ISO4
      DO 304 JK=1,ILEV
        DO 302 IL=IL1,IL2
          XCLR(IL,JK,JT)=MAX(XROW(IL,JK+1,JT),ZERO)
     1           /(ZCLF(IL,JK)*(SFRC(IL,JK,JT)-1.)+1.)
          ZSO4(IL,JK)=SFRC(IL,JK,JT)*XCLR(IL,JK,JT)
          XCLD(IL,JK,JT)=ZSO4(IL,JK)
          ZSO4(IL,JK)=MAX(ZERO,ZSO4(IL,JK))
  302   CONTINUE
  304 CONTINUE
C
C
C   CALCULATE THE REACTION-RATES FOR SO2-H2O2
      DO 308 JK=1,ILEV
       DO 306 IL=IL1,IL2
        IF(ZMLWC(IL,JK).GT.ZMIN) THEN
         ZLWCL=ZMLWC(IL,JK)*ZRHO0(IL,JK)*1.E-06
         ZLWCV=ZMLWC(IL,JK)*ZRHO0(IL,JK)*1.E-03
         ZHP=ZHPBASE+ZSO4(IL,JK)*1000./(ZMLWC(IL,JK)*ZMOLGS)
         ZQTP1=1./TH(IL,JK)-ZQ298
         ZRK=ZFARR(ZFARR1,ZFARR2,ZQTP1)/(0.1+ZHP)
         ZRKE=ZRK/(ZLWCL*ZAVO)
C
         ZH_SO2=ZFARR(ZE2K,ZE2H,ZQTP1)
         ZPFAC=ZRGAS*ZLWCV*TH(IL,JK)
         ZP_SO2=ZH_SO2*ZPFAC
         ZF_SO2(IL)=ZP_SO2/(1.+ZP_SO2)
C
         ZH_H2O2=ZFARR(ZFARR3,ZFARR4,ZQTP1)
         ZP_H2O2=ZH_H2O2*ZPFAC
         ZF_H2O2=ZP_H2O2/(1.+ZP_H2O2)
C
         ZRKH2O2(IL,JK)=ZRKE*ZF_SO2(IL)*ZF_H2O2
        ELSE
         ZRKH2O2(IL,JK)=0.
        ENDIF
  306  CONTINUE
  308 CONTINUE
C
C     * HETEROGENOUS CHEMISTRY.
C
      JT=ISO2
C
C     * INITIALIZE.
C
      DO JK=1,ILEV
      DO IL=IL1,IL2
        ZXTP10(IL,JK)=XROW(IL,JK+1,JT)
     1               /(ZCLF(IL,JK)*(SFRC(IL,JK,JT)-1.)+1.)
        ZXTP1C(IL,JK)=SFRC(IL,JK,JT)*ZXTP10(IL,JK)
        XCLD(IL,JK,JT)=ZXTP1C(IL,JK)
        XCLR(IL,JK,JT)=ZXTP10(IL,JK)
      ENDDO
      ENDDO
C
         CALL OXISTR3(    IL1,     IL2,    ILEV,    ILG,   NTRAC,  
     1                   NEQP,    ISO2,    ISO4,   IHPO,    ISSA,
     2                   ISSC,   ITRAC,    
     2                 ROAROW,  NH3ROW,  NH4ROW, HNO3ROW,   ZZO3,  
     3                 AGTSO2,  AGTSO4,   AGTO3, AGTCO2,  AGTHO2,
     4                AGTHNO3,  AGTNH3,   ACHPA, ANTSO2,  ANTHO2,
     5                 ANTSO4,    XROW,  AOH2O2, ARESID,   ZMLWC,  
     6                   ZCLF,   KCALC,      TH,     QV,     SHJ,
     7                 PRESSG,     THG,    RGAS,   EPS1,   DQLDT,   
     8                  ZTMST,  ZXTP1C,  ZSO4, ZA21, ZA22, ZZA,
     9                  ITRPHS, ISULF, ILWCP,   ICHEM, 
     A                SLO3ROW, SLHPROW,  PHLROW, SAVERAD,
     B                    LEV, ISVCHEM,    GRAV,    DSHJ,
     C                   WRK1,    WRK2,    WRK3, ZHENRY          )
C
C    CALCULATE THE WET DEPOSITION
C
C     * SO2 WET DEPOSITION.
C
      JT=ISO2
      CALL WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, ZXTP10, 
     1             ZXTP1C,DSHJ,SHJ,PRESSG,TH,QV, ZMRATEP,ZMLWC, 
     2             ZFSNOW,ZFRAIN,ZDEP3D,ZCLF,CLRFR,ZHENRY,
     3             CLRFS,ZFEVAP,ZFSUBL,PDCLR,PDCLD,JT,ISO4)
C
C     * CALCULATE NEW TENDENCIES.
C
      DO 325 JK=1,ILEV
       DO 324 IL=IL1,IL2
          ZXTP1(IL)=(1.-ZCLF(IL,JK))*ZXTP10(IL,JK)+
     1           ZCLF(IL,JK)*ZXTP1C(IL,JK)
          ZXTP1(IL)=ZXTP1(IL)-ZDEP3D(IL,JK)
          ZXTE(IL,JK,JT)=(ZXTP1(IL)-XROW(IL,JK+1,JT))*PQTMST
          XCLD(IL,JK,JT)=ZXTP1C(IL,JK)-PDCLD(IL,JK)
          XCLR(IL,JK,JT)=ZXTP10(IL,JK)-PDCLR(IL,JK)
          IF (ISVCHEM.NE.0) THEN
            ATMP=MAX(ZDEP3D(IL,JK)*PCONS2*DSHJ(IL,JK)*PRESSG(IL)
     1               *SAVERAD,0.)
            WDL4(IL)=WDL4(IL)+ATMP
          ENDIF
  324  CONTINUE
  325 CONTINUE
C
      DO 354 JT=1,NTRAC
      IF(ITRWET(JT).NE.0 .AND. JT.NE.ISO2)                THEN

       IF(JT.EQ.ISO4) THEN
        DO 331 JK=1,ILEV
          DO 330 IL=IL1,IL2
            ZXTP1C(IL,JK)=ZSO4(IL,JK)
            ZXTP10(IL,JK)=XROW(IL,JK+1,JT)
     1               /(ZCLF(IL,JK)*(SFRC(IL,JK,JT)-1.)+1.)
            XCLD(IL,JK,JT)=ZSO4(IL,JK)
            XCLR(IL,JK,JT)=ZXTP10(IL,JK)
            ZDEP3D(IL,JK)=0.
  330     CONTINUE
  331   CONTINUE
       ELSE                               
        DO 333 JK=1,ILEV                                        
          DO 332 IL=IL1,IL2                                     
            ZXTP10(IL,JK)=XROW(IL,JK+1,JT)
     1                   /(ZCLF(IL,JK)*(SFRC(IL,JK,JT)-1.)+1.)
            ZXTP1C(IL,JK)=SFRC(IL,JK,JT)*ZXTP10(IL,JK)
            XCLD(IL,JK,JT)=ZXTP1C(IL,JK)
            XCLR(IL,JK,JT)=ZXTP10(IL,JK)
            ZDEP3D(IL,JK)=0.
  332     CONTINUE                                              
  333   CONTINUE                                                
       ENDIF   
C
       IF(ITRWET(JT).EQ.1) THEN
         DO 334 JK=1,ILEV                                        
         DO 334 IL=IL1,IL2                                     
           ZHENRY(IL,JK)=ASRSO4
  334    CONTINUE                                              
       ELSE
         DO 335 JK=1,ILEV                                        
         DO 335 IL=IL1,IL2                                     
           ZHENRY(IL,JK)=ASRPHOB
  335    CONTINUE                                              
       ENDIF
C
       CALL WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, ZXTP10, 
     1              ZXTP1C,DSHJ,SHJ,PRESSG,TH,QV, ZMRATEP,ZMLWC, 
     2              ZFSNOW,ZFRAIN,ZDEP3D,ZCLF,CLRFR,ZHENRY,
     3              CLRFS,ZFEVAP,ZFSUBL,PDCLR,PDCLD,JT,ISO4)
C
C   CALCULATE NEW TENDENCIES
      DO 336 JK=1,ILEV
      DO 336 IL=IL1,IL2
          ZXTP1(IL)=(1.-ZCLF(IL,JK))*ZXTP10(IL,JK)+
     1           ZCLF(IL,JK)*ZXTP1C(IL,JK)
          ZXTP1(IL)=ZXTP1(IL)-ZDEP3D(IL,JK)
          ZXTE(IL,JK,JT)=(ZXTP1(IL)-XROW(IL,JK+1,JT))*PQTMST
          XCLD(IL,JK,JT)=ZXTP1C(IL,JK)-PDCLD(IL,JK)
          XCLR(IL,JK,JT)=ZXTP10(IL,JK)-PDCLR(IL,JK)
          ATMP=MAX(ZDEP3D(IL,JK)*PCONS2*DSHJ(IL,JK)*PRESSG(IL),0.)
          IF (JT.EQ.IBCO .OR. JT.EQ.IBCY) DEPB(IL)=DEPB(IL)+ATMP
          ATMP=ATMP*SAVERAD
          IF ( ISVCHEM.NE.0 ) THEN 
            IF (JT.EQ.ISO4) 
     1        WDL6(IL)=WDL6(IL)+ATMP
            IF (JT.EQ.IDUA .OR. JT.EQ.IDUC) 
     1        WDLD(IL)=WDLD(IL)+ATMP
            IF (JT.EQ.IBCO .OR. JT.EQ.IBCY) 
     1        WDLB(IL)=WDLB(IL)+ATMP
            IF (JT.EQ.IOCO .OR. JT.EQ.IOCY) 
     1        WDLO(IL)=WDLO(IL)+ATMP
            IF (JT.EQ.ISSA .OR. JT.EQ.ISSC) 
     1        WDLS(IL)=WDLS(IL)+ATMP
          ENDIF
  336 CONTINUE
C
      ENDIF
  354 CONTINUE
C   END OF TRACER LOOP
C
C   OXIDATION WITH OH
C
C
C     * CALCULATE THE DAY-LENGTH.
C
      LONSL=IL2/NLATJ 
      DO 403 JJ=1,NLATJ
        ZVDAYL=0.
        DO 401 II=1,LONSL
          IL=II+(JJ-1)*LONSL
          IF(CSZROW(IL).GT.0.) THEN
            ZVDAYL=ZVDAYL+1.
          ENDIF
  401   CONTINUE
        IF(ZVDAYL.GT.0.) ZVDAYL=REAL(LONSL)/ZVDAYL
C
        DO 402 II=1,LONSL
          IL=II+(JJ-1)*LONSL
          ZDAYL(IL)=ZVDAYL
  402   CONTINUE     
  403 CONTINUE
C
      JT=ISO2
C   DAY-TIME CHEMISTRY
      DO 412 JK=1,ILEV
       DO 410 IL=IL1,IL2
        IF(CSZROW(IL).GT.0.) THEN
         ZXTP1SO2=XROW(IL,JK+1,JT)+ZXTE(IL,JK,JT)*ZTMST
         IF(ZXTP1SO2.LE.ZMIN) THEN
           ZSO2=0.
         ELSE
           ZTK2=ZK2*(TH(IL,JK)/300.)**(-3.3)
           ZM=ZRHO0(IL,JK)*ZNAMAIR
           ZHIL=ZTK2*ZM/ZK2I
           ZEXP=LOG10(ZHIL)
           ZEXP=1./(1.+ZEXP*ZEXP)
           ZTK23B=ZTK2*ZM/(1.+ZHIL)*ZK2F**ZEXP
           ZSO2=ZXTP1SO2*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*ZTK23B
     1         *ZDAYL(IL)
           ZSO2=ZSO2*ZRHCTOX(IL,JK)
           ZSO2=MIN(ZSO2,ZXTP1SO2*PQTMST)
           ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)-ZSO2
           ZXTE(IL,JK,ISO4)=ZXTE(IL,JK,ISO4)+ZSO2
         ENDIF
C
         ZXTP1DMS=XROW(IL,JK+1,IDMS)+ ZXTE(IL,JK,IDMS)*ZTMST
         IF(ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
          T=TH(IL,JK)
          ZTK1=(T*EXP(-234./T)+8.46E-10*EXP(7230./T)+
     1         2.68E-10*EXP(7810./T))/(1.04E+11*T+88.1*EXP(7460./T))
          ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*ZTK1
     1        *ZDAYL(IL)
          ZDMS=ZDMS*ZRHCTOX(IL,JK)
          ZDMS=MIN(ZDMS,ZXTP1DMS*PQTMST)
          ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
          ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)+ZDMS
         ENDIF
C
         IF(ISVCHEM.NE.0)                            THEN
          DOX4ROW(IL)=DOX4ROW(IL)+ZSO2*DSHJ(IL,JK)*PRESSG(IL)
     1               /GRAV*SAVERAD
          DOXDROW(IL)=DOXDROW(IL)+ZDMS*DSHJ(IL,JK)*PRESSG(IL)
     1               /GRAV*SAVERAD
         ENDIF
        ENDIF
  410  CONTINUE
  412 CONTINUE
C
C   NIGHT-TIME CHEMISTRY
      DO 416 JK=1,ILEV
       DO 414 IL=IL1,IL2
        IF(CSZROW(IL).LE.0.) THEN
         ZXTP1DMS=XROW(IL,JK+1,IDMS)+ ZXTE(IL,JK,IDMS)*ZTMST
         IF(ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
           ZTK3=ZK3*EXP(520./TH(IL,JK))
           ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZNO3(IL,JK)*ZTK3
           ZDMS=ZDMS*ZRHCTOX(IL,JK)
           ZDMS=MIN(ZDMS,ZXTP1DMS*PQTMST)
           ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
           ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)+ZDMS
           IF(ISVCHEM.NE.0)                            THEN
             NOXDROW(IL)=NOXDROW(IL)+ZDMS*DSHJ(IL,JK)*PRESSG(IL)
     1                  /GRAV*SAVERAD
           ENDIF
         ENDIF
        ENDIF
  414  CONTINUE
  416 CONTINUE
C
      DO 422 JT=1,NTRAC
       DO 421 JK=1,ILEV
        DO 420 IL=IL1,IL2
          XROW(IL,JK+1,JT)=XROW(IL,JK+1,JT)+ZXTE(IL,JK,JT)*ZTMST
          XROW(IL,JK+1,JT)=MAX(XROW(IL,JK+1,JT),ZERO)
          IF ( ITRWET(JT).NE.0 .AND. XROW(IL,JK+1,JT) > ZMIN 
     1         .AND. ZCLF(IL,JK) > 1.E-02 .AND. JT /= ISO2 .AND.
     2                                          JT /= IHPO ) THEN
            IF ( XCLR(IL,JK,JT) > ZMIN ) THEN
              SFRC(IL,JK,JT)=XCLD(IL,JK,JT)/XCLR(IL,JK,JT)
              SFRC(IL,JK,JT)=MIN(PLARGE,MAX(1./PLARGE,SFRC(IL,JK,JT)))
            ELSE
              SFRC(IL,JK,JT)=PLARGE
            ENDIF
          ELSE
            SFRC(IL,JK,JT)=1.
          ENDIF
 420    CONTINUE
 421   CONTINUE
 422  CONTINUE
C
      RETURN
      END
