      SUBROUTINE DRYDEP6(XROW,
     1                   FLNDROW,GTROWL,GTROWI,PRESSG,DSHJ,THROW,
     2                   SHJ,SICNROW,SNOROW,ZSOI,QROW,HMFNROW,
     3                   ZFOR, DEPB, DD4, DD6, DDD, DDB, DDO,DDS,
     4                   SAVERAD,ISVCHEM,
     5                   IBCO,IBCY,IOCO,IOCY,ISSA,ISSC,
     6                   IDUA,IDUC,ISO2,ISO4,IPAM,ZTMST,
     7                   ILG,IL1,IL2,ILEV,LEV,NTRAC)
C
C    THIS ROUTINE CALCULATES THE  DRY DEPOSITION FLUX.
C
C     * MAY 12/2014 - M.LAZARE.   NEW VERSION FOR GCM18+:
C     *                           - FRACTIONAL LAND (FLNDROW) REPLACES
C     *                             DISCRETE GROUND COVER (GCROW).
C     *                           - CHANGED CALL SEQUENCES.
C     *                           - GT over land (GTROWL) and seaice
C     *                             (GTROWI) are passed in, instead of
C     *                             general GTROW, and used in their
C     *                             respective sections.
C     * JUN 19/2013 - K.VONSALZEN/PREVIOUS VERSION DRYDEP5 FOR GCM17:
C     *               M.NAMAZI/   - PASS IN "IPAM" SWITCH TO NOT DO
C     *               M.LAZARE.     CALCULATIONS/DIAGNOSTICS ON MOST
C     *                             FIELDS (EXCEPT ISO2) IF USING PLA.
C     *                           - ALSO PASS IN AND CALCULATE BC DEPOSITION
C     *                             (DEPB) IF USING BULK.
C     * APR 25/2010 - M.LAZARE.   PREVIOUS VERSION DRYDEP4 FOR GCM15I,GCM16:
C     *                           - DDA,DDC REMOVED AND 
C     *                             DDD,DDB,DDO,DDS ADDED FOR
C     *                             DIAGNOSTIC FIELDS UNDER
C     *                             CONTROL OF "ISVCHEM" (DUST,
C     *                             BLACK CARBON, ORGANIC
C     *                             CARBON AND SEA-SALT, RESPECTIVELY).
C     * DEC 18/2007 - M.LAZARE.   PREVIOUS VERSION FOR GCM15G/H:
C     *                           PASS IN "ADELT" FROM PHYSICS AS
C     *                           ZTMST AND USE DIRECTLY, RATHER THAN
C     *                           PASSING IN DELT AND FORMING
C     *                           ZTMST=2.*DELT INSIDE.
C     * JUN 20/06 -  M. LAZARE.   PREVIOUS VERSION DRYDEP2 FOR GCM15F:
C     *                           - USE VARIABLE INSTEAD OF CONSTANT
C     *                             IN INTRINSICS SUCH AS "MAX",
C     *                             SO THAT CAN COMPILE IN 32-BIT MODE
C     *                             WITH REAL*8.  
C     * DEC 20/02 -  K. VONSALZEN.FINAL PREVIOUS VERSION DRYDEP FOR
C     *                           GCM15B/C/D/E:
C     *                           - REMOVE "ITRIND" COMMON BLOCK
C     *                             AND PASS INDICES INSTEAD. 
C     *                           - REMOVE XADDROW CALCULATIONS.
C     *                           - ADD DUST FIELDS.
C     *                           - OPTIMIZE LOOPS. 
C     * SEP 21/2001 K.VON SALZEN. CORRECT CALCULATION OF XADDROW.
C     *                           NOW OPTIONALLY SAVE  DD4, DD6.
C     * M.LAZARE - NOV 16/2000. PASS IN SICNROW INSTEAD OF SICROW
C     *                         TO BETTER DEFINE "ALFA", AND
C     *                         CORRECT "IF" CONDITION ON GCROW
C     *                         SO THAT FIRST BRANCH IS WATER/ICE
C     *                         INSTEAD OF JUST WATER.
C  
C    JOHANN FEICHTER          UNI/HAMBURG         08/91
C    MODIFIED  U. SCHLESE    DKRZ-HAMBURG        JAN-95
C
C    PURPOSE
C   ---------
C    THE LOWER BOUNDARY CONDITION FOR CALCULATING THE
C    TURBULENT EXCHANGE IN THE BOUNDARY LAYER IS
C    DETERMINED BY THE EMISSION AND THE DRY DEPOSITION FLUX.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   GTROWL(ILG),GTROWI(ILG)
      REAL   DSHJ(ILG,ILEV),PRESSG(ILG),SHJ(ILG,ILEV)
      REAL   THROW(ILG,ILEV),SICNROW(ILG),SNOROW(ILG),QROW(ILG,ILEV)
      REAL   XROW(ILG,LEV,NTRAC),FLNDROW(ILG),HMFNROW(ILG)
      REAL   ZVWC2,ZVW02,ZVWC4,ZVW04,ZSNCRI,ALFA
      REAL   ZSOI(ILG)
      REAL   ZFOR(ILG),DD4(ILG),DD6(ILG),DDD(ILG),DDB(ILG),DDO(ILG),
     1       DDS(ILG),DEPB(ILG)

      REAL   WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,RGASV,CPRESV

      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES
      COMMON /PARAMS/ RGASV,CPRESV
      COMMON /HTCP/   T1S,T2S,AI,BI,AW,BW,SLP
C=====================================================================
C     M WATER EQUIVALENT  CRITICAL SNOW HEIGHT (FROM *SURF*)
      ZSNCRI=0.025
C
C     COEFFICIENTS FOR ZVDRD = FUNCTION OF SOIL MOISTURE
C
      ZVWC2=(0.8E-2 - 0.2E-2)/(1. - 0.9)
      ZVW02=ZVWC2-0.8E-2
      ZVWC4=(0.2E-2 - 0.025E-2)/(1. - 0.9)
      ZVW04=ZVWC4-0.2E-2     
      ZVDPHOB=2.5E-4
      ZVDDUST=1.E-2
c
c
C*      2.    DRY DEPOSITION.
C             --- ----------
      DO 205 IL=IL1,IL2
       ZRHO0=SHJ(IL,ILEV)*PRESSG(IL)/(RGAS*THROW(IL,ILEV)
     1             *(1.+(RGASV/RGAS-1.)*QROW(IL,ILEV)))
C
       ZZDP=ZRHO0*ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))
 
       ZMAXVDRY=1./ZZDP
       ZVDRD1NL=0.
       ZVDRD2NL=0.
       ZVDRD1L =0.
       ZVDRD2L =0.
C
C      * DRY DEPOSITION OF SO2, SO4.
C
C      - SEA(WATER OR SEAICE) -
       IF(FLNDROW(IL).LT.1.) THEN
C        - MELTING/NOT MELTING SEAICE-
         IF(GTROWI(IL).GE.(T1S-0.1)) THEN
            ZVD2ICE=0.8E-2
            ZVD4ICE=0.2E-2
         ELSE
            ZVD2ICE=0.1E-2
            ZVD4ICE=0.025E-2
         ENDIF
         ALFA=SICNROW(IL)
         ZVDRD1NL=(1.-ALFA)*1.0E-2+ALFA*ZVD2ICE
         ZVDRD2NL=(1.-ALFA)*0.2E-2+ALFA*ZVD4ICE
         ZVDRD1NL=MIN(MAX(ZVDRD1NL,0.),ZMAXVDRY)
         ZVDRD2NL=MIN(MAX(ZVDRD2NL,0.),ZMAXVDRY)
       ENDIF
C
C      - LAND -
       IF(FLNDROW(IL).GT.0.) THEN
C        - NON-FOREST AREAS -
C        - SNOW/NO SNOW -
         IF(SNOROW(IL).GT.ZSNCRI) THEN
C          - MELTING/NOT MELTING SNOW -
           IF(HMFNROW(IL).GT.0.) THEN
             ZVD2NOF=0.8E-2
             ZVD4NOF=0.2E-2
           ELSE
             ZVD2NOF=0.1E-2
             ZVD4NOF=0.025E-2
           ENDIF
         ELSE
C          - FROZEN/NOT FROZEN SOIL -
           IF(GTROWL(IL).LE.T1S) THEN
             ZVD2NOF=0.2E-2
             ZVD4NOF=0.025E-2
           ELSE
C          - WET/DRY -
             IF(ZSOI(IL).GE.0.99)     THEN
               ZVD2NOF=0.8E-2
               ZVD4NOF=0.2E-2
             ELSE IF(ZSOI(IL).LT.0.9) THEN
               ZVD2NOF=0.2E-2
               ZVD4NOF=0.025E-2
             ELSE
               ZVD2NOF=ZVWC2*ZSOI(IL)-ZVW02
               ZVD4NOF=ZVWC4*ZSOI(IL)-ZVW04
             ENDIF
           ENDIF
         ENDIF
         ZZFCROW=MAX(ZFOR(IL),0.)
         ZVDRD1L=ZZFCROW*0.8E-2
     1                  +(1.-ZZFCROW)*ZVD2NOF
         ZVDRD2L=ZZFCROW*0.2E-2
     1                  +(1.-ZZFCROW)*ZVD4NOF
         ZVDRD1L=MIN(MAX(ZVDRD1L,0.),ZMAXVDRY)
         ZVDRD2L=MIN(MAX(ZVDRD2L,0.),ZMAXVDRY)
       ENDIF
       ZVDRD1 =FLNDROW(IL)*ZVDRD1L + (1.-FLNDROW(IL))*ZVDRD1NL
       ZVDRD2 =FLNDROW(IL)*ZVDRD2L + (1.-FLNDROW(IL))*ZVDRD2NL
C
       FAC    = ZZDP*SICNROW(IL) 
       FAC1NL = 1.-ZVDRD1NL*ZZDP
       FAC2NL = 1.-ZVDRD2NL*ZZDP
       FAC3NL = 1.-ZVDRD2NL*FAC
       FAC4NL = 1.-ZVDDUST*FAC
       FAC1L  = 1.-ZVDRD1L*ZZDP
       FAC2L  = 1.-ZVDRD2L*ZZDP
       FAC1   = FLNDROW(IL)*FAC1L + (1.-FLNDROW(IL))*FAC1NL
       FAC2   = FLNDROW(IL)*FAC2L + (1.-FLNDROW(IL))*FAC2NL
       FAC3   = 1.-ZVDPHOB*ZZDP
       FAC4   = 1.-ZVDDUST*ZZDP
       FAC5   = FLNDROW(IL)*FAC2L + (1.-FLNDROW(IL))*FAC3NL
       FAC6   = FLNDROW(IL)*FAC4  + (1.-FLNDROW(IL))*FAC4NL
C
C      * SULPHATE IS DONE REGARDLESS OF "IPAM".
C
       XROW(IL,LEV,ISO2)=XROW(IL,LEV,ISO2)*FAC1
       IF(ISVCHEM.NE.0)                          THEN
         DD4(IL)= DD4(IL)+XROW(IL,LEV,ISO2)*ZVDRD1*ZRHO0*SAVERAD
       ENDIF
C
       IF(IPAM.EQ.0) THEN
         DEPB(IL)=DEPB(IL)+(XROW(IL,LEV,IBCO)*ZVDPHOB
     1                    + XROW(IL,LEV,IBCY)*ZVDRD2)*ZRHO0 
         XROW(IL,LEV,ISO4)=XROW(IL,LEV,ISO4)*FAC2
         XROW(IL,LEV,IBCY)=XROW(IL,LEV,IBCY)*FAC2
         XROW(IL,LEV,IOCY)=XROW(IL,LEV,IOCY)*FAC2
         XROW(IL,LEV,IBCO)=XROW(IL,LEV,IBCO)*FAC3
         XROW(IL,LEV,IOCO)=XROW(IL,LEV,IOCO)*FAC3
         XROW(IL,LEV,IDUA)=XROW(IL,LEV,IDUA)*FAC2
         XROW(IL,LEV,IDUC)=XROW(IL,LEV,IDUC)*FAC4
         XROW(IL,LEV,ISSA)=XROW(IL,LEV,ISSA)*FAC5
         XROW(IL,LEV,ISSC)=XROW(IL,LEV,ISSC)*FAC6
         IF (ISVCHEM.NE.0 ) THEN
           DD6(IL)=DD6(IL)+XROW(IL,LEV,ISO4)*ZVDRD2 *ZRHO0 *SAVERAD
           DDD(IL)=DDD(IL)+XROW(IL,LEV,IDUA)*ZVDRD2 *ZRHO0 *SAVERAD
           DDD(IL)=DDD(IL)+XROW(IL,LEV,IDUC)*ZVDDUST*ZRHO0 *SAVERAD
           DDB(IL)=DDB(IL)+XROW(IL,LEV,IBCO)*ZVDPHOB*ZRHO0 *SAVERAD
           DDB(IL)=DDB(IL)+XROW(IL,LEV,IBCY)*ZVDRD2 *ZRHO0 *SAVERAD
           DDO(IL)=DDO(IL)+XROW(IL,LEV,IOCO)*ZVDPHOB*ZRHO0 *SAVERAD
           DDO(IL)=DDO(IL)+XROW(IL,LEV,IOCY)*ZVDRD2 *ZRHO0 *SAVERAD
           DDS(IL)=DDS(IL)+XROW(IL,LEV,ISSA)*ZVDRD2 *ZRHO0 *SAVERAD
           DDS(IL)=DDS(IL)+XROW(IL,LEV,ISSC)*ZVDDUST*ZRHO0 *SAVERAD
         ENDIF
       ENDIF
  205 CONTINUE
C
      RETURN 
      END
