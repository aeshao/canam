      SUBROUTINE INTCAC(EOSTROW,EOSTROL,
     1                  IL1,IL2,ILG,ILEV,DELT,GMT,IDAY,MDAY,ISULF)
C
C     * KNUT VON SALZEN - FEB 07,2009. NEW ROUTINE FOR GCM15H TO DO 
C     *                                INTERPOLATION OF EOST 
C     *                                (USED TO BE DONE BEFORE
C     *                                IN INTCHEM2).
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N) 
C 
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL   EOSTROW(ILG),EOSTROL(ILG)
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      DAY=REAL(IDAY)+GMT/86400. 
      FMDAY=REAL(MDAY) 
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      DAYSM=FMDAY-DAY 
      ISTEPSM=NINT(DAYSM*86400./DELT)
      STEPSM=REAL(ISTEPSM)
C 
C     * GENERAL INTERPOLATION.
C 
      DO 210 I=IL1,IL2
        EOSTROW(I)  = ((STEPSM-1.) * EOSTROW(I) + EOSTROL(I)) / STEPSM 
  210 CONTINUE
C 
      RETURN
      END 
