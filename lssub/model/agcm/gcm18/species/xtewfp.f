      SUBROUTINE XTEWFP(XEMIS,EBWAROW,EOWAROW,ESWAROW,DSHJ,PRESSG,ZF, 
     1                  DOC, DBC, DSU, PSUEF,
     1                  LEVWF, NTRAC, ZTMST, ILEV, IL1, IL2, ILG, ISO2)
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C     
C     * INPUT FIELDS
C
      REAL EBWAROW(ILG,LEVWF),EOWAROW(ILG,LEVWF),ESWAROW(ILG,LEVWF) 
C
C     * I/O FIELDS.
C
      REAL DSHJ(ILG,ILEV), PRESSG(ILG), ZF(ILG,ILEV)
      REAL XEMIS(ILG,ILEV,NTRAC)
      REAL, DIMENSION(ILG,ILEV) :: DOC, DBC, DSU
C
C     * INTERNAL WORK FIELDS.
C
      REAL  , DIMENSION(ILG,LEVWF)  :: EOWA
      REAL  ,  DIMENSION(ILG) ::  T_EBWA,T_BWA
      REAL  ,  DIMENSION(ILG) ::  T_EOWA,T_OWA
      REAL  ,  DIMENSION(ILG) ::  T_ESWA,T_SWA
      INTEGER, DIMENSION(ILG) ::  LSO2,HSO2
      REAL, PARAMETER :: WSO2    =64.059
      REAL, PARAMETER :: WH2SO4  =98.073
      REAL, PARAMETER :: WNH3    =17.030
      REAL, PARAMETER :: WAMSUL  =WH2SO4+2.*WNH3 
      REAL, PARAMETER :: WRAT    =WAMSUL/WSO2
C
      COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES, RGASV, CPRESV
C
C     * LOCAL ARRAY...
C
      REAL   ZREF(6)
C
C     * HEIGHT LEVELS OF INPUT FIELD USED FOR INTERPOLATION.
C
      DATA ZREF /100., 500., 1000., 2000., 3000., 6000./
C----------------------------------------------------------------------
      POM2OC=1.4
      DO K=1,LEVWF
      DO IL=IL1,IL2
        EOWA(IL,K)=EOWAROW(IL,K)/POM2OC
      ENDDO
      ENDDO
C
C * FOR DIAGNOSTIC PURPOSE: TO MAKE SURE THE TOTAL EMISSIONS  
C        AFTER INTERPOLATION ARE SAME AS BEFORE.
C * T_E*WA: TOTAL FROM VALUES BEFORE INTERPOLATION;
C * T_*WA:  TOTAL FROM VALUES AFTER  INTERPOLATION;
C
      DO 90 IL = IL1, IL2
        T_EBWA(IL) = 0.
        T_EOWA(IL) = 0.
        T_ESWA(IL) = 0.
        T_BWA (IL) = 0.
        T_OWA (IL) = 0.
        T_SWA (IL) = 0.
   90 CONTINUE
C      
      DO 180 K = 1, LEVWF

       RUH = ZREF(K)
        IF(K.EQ.1) THEN
       RLH = 0.
        ELSE
       RLH = ZREF(K-1)
        ENDIF
       DUL = RUH-RLH
C
C     * DETERMINE LAYER CORRESPONDING TO SO2 EMISSIONS.
C
      DO 100 IL = IL1, IL2
        LSO2(IL) = 0
        HSO2(IL) = 0
  100 CONTINUE

      DO 105 L = ILEV, 1, -1
      DO 105 IL = IL1, IL2
          IF( ZF(IL,L) .LE. RLH)   LSO2(IL)=L
          IF( ZF(IL,L) .LE. RUH)   HSO2(IL)=L
          IF( RLH.LE.0.)           LSO2(IL)=ILEV
  105 CONTINUE
C
C   * EBWA
C
      DO 108 IL = IL1, IL2
        T_EBWA(IL) = T_EBWA(IL) + EBWAROW(IL,K)
        T_EOWA(IL) = T_EOWA(IL) + EOWA(IL,K)
        T_ESWA(IL) = T_ESWA(IL) + ESWAROW(IL,K)
  108 CONTINUE
C
      DO 110 L=1, ILEV
      DO 110 IL = IL1, IL2
        FACT                   = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF ( L.EQ.LSO2(IL) .AND. L.EQ.HSO2(IL) ) THEN
         WGT=1.
        ELSE IF( L.EQ.HSO2(IL) ) THEN
         WGT=( RUH-ZF(IL,L) )/DUL
        ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(L) ) THEN
         WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
        ELSE IF( L.EQ.LSO2(IL) ) THEN
         WGT=( ZF(IL,L-1)-RLH )/DUL
        ELSE
         WGT=0.
        ENDIF
        T_BWA(IL) = T_BWA(IL) + WGT*EBWAROW(IL,K)
        DBC(IL,L)=DBC(IL,L)+WGT*EBWAROW(IL,K)*FACT/ZTMST
  110 CONTINUE
C
C   * EOWA
C
      DO 120 L=1, ILEV
      DO 120 IL = IL1, IL2
        FACT                 = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
        IF ( L.EQ.LSO2(IL) .AND. L.EQ.HSO2(IL) ) THEN
         WGT=1.
        ELSE IF( L.EQ.HSO2(IL) ) THEN
         WGT=( RUH-ZF(IL,L) )/DUL
        ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(L) ) THEN
         WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
        ELSE IF( L.EQ.LSO2(IL) ) THEN
         WGT=( ZF(IL,L-1)-RLH )/DUL
        ELSE
         WGT=0.
        ENDIF
        T_OWA(IL) = T_OWA(IL) + WGT*EOWA(IL,K)
        DOC(IL,L)=DOC(IL,L)+WGT*EOWA(IL,K)*FACT/ZTMST/POM2OC
  120 CONTINUE
C
C   * ESWA
C
      JT=ISO2
C
      DO 130 L=1, ILEV
      DO 130 IL = IL1, IL2
         FACT                 = ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
         IF ( L.EQ.LSO2(IL) .AND. L.EQ.HSO2(IL) ) THEN
          WGT=1.
         ELSE IF( L.EQ.HSO2(IL) ) THEN
          WGT=( RUH-ZF(IL,L) )/DUL
         ELSE IF( L.GT.HSO2(IL) .AND. L.LT.LSO2(L) ) THEN
          WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
         ELSE IF( L.EQ.LSO2(IL) ) THEN
          WGT=( ZF(IL,L-1)-RLH )/DUL
         ELSE
          WGT=0.
         ENDIF
         T_SWA(IL) = T_SWA(IL) + WGT*ESWAROW(IL,K)
         XEMIS(IL,L,JT)=XEMIS(IL,L,JT)+WGT*ESWAROW(IL,K)*FACT
     1                                   *32.064/64.059*(1.-PSUEF)
         DSU(IL,L)=DSU(IL,L)+WGT*ESWAROW(IL,K)*FACT*WRAT*PSUEF/ZTMST
  130 CONTINUE
  180 CONTINUE
C
C   --------------------------------------------------------------
C
      RETURN
      END
