      SUBROUTINE WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, PXTP10, 
     1                   PXTP1C,DSHJ,SHJ,PRESSG,TH,QV,PMRATEP,ZMLWC,
     2                   PFSNOW,PFRAIN,PDEP3D,ZCLF,CLRFR,PHENRY,
     3                   CLRFS,PFEVAP,PFSUBL,PDCLR,PDCLD,JT,ISO4)
C
C     *WETDEP4* CALCULATES THE WET DEPOSITION OF TRACE GASES OR AEROSOLS
C
C     * FEB 18/2009 - K.VONSALZEN. NEW VERSION FOR GCM15H:
C     *                            - ADD DIAGNOSTICS FOR IN-CLOUD AND 
C     *                              CLEAR-SKY SOURCES AND SINKS OF
C     *                              TRACERS.
C     * DEC 16/07 -  K. VONSALZEN. PREVIOUS VERSION WETDEP3 FOR GCM15G:
C     *                            - BUGFIXES FOR SCAVENGING.
C     * JUN 20/06 -  M. LAZARE.    PREVIOUS VERSION WETDEP2 FOR GCM15F:
C     *                            - USE VARIABLE INSTEAD OF CONSTANT
C     *                              IN INTRINSICS SUCH AS "MAX",
C     *                              SO THAT CAN COMPILE IN 32-BIT MODE
C     *                              WITH REAL*8.  
C
C   PURPOSE
C  ---------
C   TO CALCULATE THE WET SCAVENGING OF GASES OR AEROSOLS IN CLOUDS
C
C   INTERFACE
C  -------------
C   THIS ROUTINE IS CALLED FROM *XTCHEMIE*
C   C
C  METHOD
C  -------
C
C   NO EXTERNALS
C---------------
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   ZMLWC(ILG,ILEV),PXTP10(ILG,ILEV),  PXTP1C(ILG,ILEV),
     1       DSHJ(ILG,ILEV),SHJ(ILG,ILEV), PMRATEP(ILG,ILEV),
     2       PFSNOW(ILG,ILEV), TH(ILG,ILEV),PFEVAP(ILG,ILEV),
     3       ZCLF(ILG,ILEV),CLRFR(ILG,ILEV),PRESSG(ILG),QV(ILG,ILEV),
     4       PFRAIN(ILG,ILEV),CLRFS(ILG,ILEV),PFSUBL(ILG,ILEV)
      REAL   PDEP3D(ILG,ILEV),PHENRY(ILG,ILEV),ZDEPR(ILG),ZDEPS(ILG),
     1       PDEPR(ILG,ILEV),PDEPS(ILG,ILEV),PDCLR(ILG,ILEV),
     2       PDCLD(ILG,ILEV)
C
      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES
      COMMON /PARAMS/ RGASV,CPRESV
C
      DATA ZERO,ONE /0., 1./
C====================================================================
C     * PHYSICAL CONSTANTS
C
      PQTMST=1./ZTMST
      ZCONS1=5.2
      ZCOLLEFF=0.1
      ZMIN=1.E-20
      ZEP=1.
      VTMPC1=RGASV/RGAS-1.
C
      PDEPS=0.
      PDEPR=0.
      PDCLR=0.
      PDCLD=0.
      DO 100 IL=IL1,IL2
         ZDEPR(IL)=0.
         ZDEPS(IL)=0.
 100  CONTINUE
C
C     * MAJOR VERTICAL LOOP.
C
      DO 160 JK=1,ILEV
         DO 110 IL=IL1,IL2
          PXTP1C(IL,JK)=MAX(ZERO,PXTP1C(IL,JK))
          PXTP10(IL,JK)=MAX(ZERO,PXTP10(IL,JK))
  110    CONTINUE
C
C   2. IN-CLOUD SCAVENGING (GIORGI + CHAMEIDES)
C
         DO 120 IL=IL1,IL2
          IF(PMRATEP(IL,JK).GT.ZMIN) THEN
            ZICSCAV=PMRATEP(IL,JK)/ZMLWC(IL,JK)
            ZICSCAV=MAX(ZERO,MIN(ONE,ZICSCAV))
            ZEPS=ZEP*PHENRY(IL,JK)
            ZICSCAV=ZEPS*ZICSCAV
C
C           NO SCAVENGING IN ICE-CLOUDS
C
            PDEPR(IL,JK)=PDEPR(IL,JK)+PXTP1C(IL,JK)*ZICSCAV
     1             *ZCLF(IL,JK)
            PDCLD(IL,JK)=PDCLD(IL,JK)+PXTP1C(IL,JK)*ZICSCAV
C           PXTP1C(IL,JK)=PXTP1C(IL,JK)*(1.-ZICSCAV)
          END IF
  120    CONTINUE
C
C   3. BELOW CLOUD SCAVENGING FOR ICE AND WATER CLOUDS.
C
         DO 140 IL=IL1,IL2
          IF(PFSNOW(IL,JK).GT.ZMIN.AND.CLRFS(IL,JK).GE.0.05) THEN
            ZZEFF=ZCOLLEFF*3.
            ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
            ZRHO0=PRESSG(IL)*SHJ(IL,JK)/(RGAS*TH(IL,JK)
     1               *(1.+VTMPC1*QV(IL,JK)))
            ZBCSCAV=ZCONS1*ZZEFF*(PFSNOW(IL,JK)/CLRFS(IL,JK))
     1             *ZFTOM*ZRHO0
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            PDEPS(IL,JK)=PDEPS(IL,JK)+ZBCSCAV*PXTP10(IL,JK)*
     1                   CLRFS(IL,JK)
            IF ( ZCLF(IL,JK) < 0.99 ) THEN
              PDCLR(IL,JK)=PDCLR(IL,JK)+ZBCSCAV*PXTP10(IL,JK)
     1                    *CLRFS(IL,JK)/(1.-ZCLF(IL,JK))
            ENDIF
          END IF
  140    CONTINUE
         DO 145 IL=IL1,IL2
          IF(PFRAIN(IL,JK).GT.ZMIN.AND.CLRFR(IL,JK).GE.0.05) THEN
            ZZEFF=ZCOLLEFF*3.
            ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
            ZRHO0=PRESSG(IL)*SHJ(IL,JK)/(RGAS*TH(IL,JK)
     1               *(1.+VTMPC1*QV(IL,JK)))
            ZBCSCAV=ZCONS1*ZZEFF*(PFRAIN(IL,JK)/CLRFR(IL,JK))
     1             *ZFTOM*ZRHO0
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            PDEPR(IL,JK)=PDEPR(IL,JK)+ZBCSCAV*PXTP10(IL,JK)*
     1                   CLRFR(IL,JK)
            IF ( ZCLF(IL,JK) < 0.99 ) THEN
              PDCLR(IL,JK)=PDCLR(IL,JK)+ZBCSCAV*PXTP10(IL,JK)
     1                    *CLRFR(IL,JK)/(1.-ZCLF(IL,JK))
            ENDIF
          END IF
  145    CONTINUE
         DO IL=IL1,IL2
          ZMTOF=DSHJ(IL,JK)*PRESSG(IL)*PCONS2
          ZDEPR(IL)=ZDEPR(IL)+PDEPR(IL,JK)*ZMTOF
          ZDEPS(IL)=ZDEPS(IL)+PDEPS(IL,JK)*ZMTOF
         ENDDO
C
C   4. REEVAPORATION. ONLY COMPLETE EVAPORATION OF RRECIP IN THE 
C      CLEAR-SKY PORTION OF THE GRID CELL THAT IS AFFECTED BY RAIN) 
C      WILL CAUSE TRACERS TO BE RELEASED INTO THE ENVIRONMENT.
C
         DO IL=IL1,IL2
          ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
          PDEPS(IL,JK)=PDEPS(IL,JK)-ZDEPS(IL)*ZFTOM*PFSUBL(IL,JK)
          IF ( ZCLF(IL,JK) < 0.99 ) THEN
            PDCLR(IL,JK)=PDCLR(IL,JK)
     1                  -ZDEPS(IL)*ZFTOM*PFSUBL(IL,JK)/(1.-ZCLF(IL,JK))
          ENDIF
          ZDEPS(IL)=ZDEPS(IL)*(1.-PFSUBL(IL,JK))
         ENDDO
         DO IL=IL1,IL2
          ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
          PDEPR(IL,JK)=PDEPR(IL,JK)-ZDEPR(IL)*ZFTOM*PFEVAP(IL,JK)
          IF ( ZCLF(IL,JK) < 0.99 ) THEN
            PDCLR(IL,JK)=PDCLR(IL,JK)
     1                  -ZDEPR(IL)*ZFTOM*PFEVAP(IL,JK)/(1.-ZCLF(IL,JK))
          ENDIF
          ZDEPR(IL)=ZDEPR(IL)*(1.-PFEVAP(IL,JK))
         ENDDO
         DO IL=IL1,IL2
           PDEP3D(IL,JK)=PDEP3D(IL,JK)+PDEPR(IL,JK)+PDEPS(IL,JK)
           PDCLR (IL,JK)=MIN(PDCLR(IL,JK),PXTP10(IL,JK))
           PDCLD (IL,JK)=MIN(PDCLD(IL,JK),PXTP1C(IL,JK))
         ENDDO
  160 CONTINUE
C
      RETURN
      END
