      SUBROUTINE WILDFLAB(LF,LEVWF)

C     * MAR 6/2007 - X.MA.
C
C     * DEFINES LEVEL INDEX VALUES FOR INPUT EMISSIONS FROM WILD FIRE FIELDS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      INTEGER LF(LEVWF)
C-----------------------------------------------------------------------
      DO 100 L=1,LEVWF
        LF(L)=L
  100 CONTINUE

      RETURN
      END
