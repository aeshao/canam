      SUBROUTINE SWAPF4X(FOUT,FIN,ILEV,NLAT,ILAT,JOFF,ILH)

C     * AUG 3/2003 - M.LAZARE. LIKE SWAPF4 EXCEPT INPUT ARRAY
C     *                        IS FIN(2,ILH,NLAT,ILEV) INSTEAD OF
C     *                        F(ILEV,NLAT,2,ILH), AND OUTPUT ARRAY
C     *                        IS OVER ALL ILAT LATITUDES (NO
C     *                        WORKS WORK ARRAY).
C     * NOV 12/92. -  A.J.Stacey. PREVIOUS VERSION SWAPF4.
C
C     * Transpose FIN(2,ilh,nlat,ilev) to FOUT(2,ilev,ilat,ilh).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real fin (2,ilh,nlat,ilev)
      real fout(2,ilev,ilat,ilh)
C-----------------------------------------------------------------------
      do i=1,ilh
      do j=1,nlat
      do l=1,ilev
        fout(1,l,joff+j,i) = fin(1,i,j,l)
        fout(2,l,joff+j,i) = fin(2,i,j,l)
      enddo
      enddo
      enddo 
C
      return
      end
