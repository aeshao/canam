      SUBROUTINE FFWFG4 (FC,ILH, GD,ILG, IR,LON,WRK,ILEV,IFAX,TRIGS) 
C   
C     * AUG 01/03 - M.LAZARE. - GENERALIZED USING "MAXLOT" PASSED IN
C     *                         COMMON BLOCK, INSTEAD OF HARD-CODED
C     *                         VALUE OF 256 ONLY VALID FOR THE NEC.
C     *                         THIS IS CONSISTENT WITH WHAT IS DONE IN VFFT3.
C     *                       - CALLS NEW VFFT3 INSTEAD OF VFFT2.
C     * NOV 01/92 - J.STACEY. PREVIOUS VERSION FFWFG3.
C
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,   
C     * GRID TO COEFFICIENTS.      
C     * ====================      
C     * FC    = FOURIER COEFFICIENTS,     
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,    
C     * GD    = GRID DATA,      
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS  
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN, 
C     *         OBVIOUSLY THEN ILG MUST EQUAL 2*ILH.   
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE.  
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR),   
C     * LON   = NUMBER OF DISTINCT LONGITUDES,    
C     * WRK   = WORK SPACE, OF SIZE (LON+2)*LENGTH.   
C     * ILEV  = NUMBER OF LEVELS.     
C         
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL FC(ILG,ILEV),GD(ILG,ILEV),WRK(ILG*MAXLOT)   
      REAL TRIGS(LON)     
      INTEGER IFAX(*)

      COMMON /ITRANS/ MAXLOT
C----------------------------------------------------------------- 
      ISIGN=-1        
      INC  = 1        
      LON1 = LON+1       
      IF(ILG.LT.LON+2) CALL XIT('FFWFG4',-1)    
C  
C     * ABORT IF WORKSPACE SIZE OF "WRK" INSUFFICIENT.
C
      IF(MAXLOT.GT.ILEV) CALL XIT('FFWFG4',-2) 
C         
C     * DO AS MANY AS MAXLOT AT ONCE FOR VECTORIZATION.   
C         
      NTIMES=ILEV/MAXLOT       
      NREST =ILEV-NTIMES*MAXLOT      
      NSTART=1        
C         
      IF(NREST.NE.0) THEN      
         LENGTH=NREST       
         NTIMES=NTIMES+1      
      ELSE        
         LENGTH=MAXLOT       
      ENDIF        
C         
C     * DO THE FOURIER TRANSFORMS.     
C         
      DO 300 N=1,NTIMES       
       CALL VFFT3(GD(1,NSTART),WRK,TRIGS,IFAX,INC,ILG,LON,LENGTH,ISIGN)
       NSTART=NSTART+LENGTH      
       LENGTH=MAXLOT
  300 CONTINUE        
C         
      RETURN        
C-----------------------------------------------------------------------
      END        
