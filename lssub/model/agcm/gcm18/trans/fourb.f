      SUBROUTINE FOURB(FOURJ1,GRID,NLEV,
     1                 ILH,NLAT,ILAT,JOFF,LM,
     2                 LON,IFAX,TRIGS,
     3                 FOUR1,WRKS)
C
C     * NOV 16/03 - M. LAZARE.   REVISED TO SUPPORT REVISED SWAPF4X.
C     * OCT 29/03 - M. LAZARE.   NEW GENERIC ROUTINE BASED ON MHANLP_
C     *                          AND/OR MHANLQ_. WITH THE NUMBER OF
C     *                          TOTAL LEVELS ("NLEV") PASSED IN, THIS
C     *                          CAN NOW BE USED IN EITHER CASE.
C     *                          LEGENDRE MOVED TO NEW LEGBP ROUTINE.
C     * AUG 03/03 - M. LAZARE. - NOW USING COLUMN-WISE TRANSFORM, SO
C     *                          NEW CALLS TO SWAPF3X AND SWAPF4X.
C     *                        - OTHER UNNECESSARY INPUT ARRAYS REMOVED.
C     * AUG 01/03 - M. LAZZRE. - CALLS NEW FFWFG4.     
C     * JUN 25/03 - M. LAZARE. - FINAL OUTPUT ARRAY IS "SPEC", NOT
C     *                          ACTUAL SPECTRAL FIELDS, SINCE "SWAPS1"
C     *                          IS MOVED OUT OF PARALLEL REGION INTO
C     *                          MODEL DRIVER. THUS WRKT REMOVED.
C     *                        - DIMENSION->REAL.
C     * NOV 22/98 - M. LAZARE. PREVIOUS ROUTINE MHANLP3.
C     *  
C     * TRANSFORM GRID VALUES TO FOURIER COEFFICIENTS.
C     *
C     * INPUT FIELDS
C     * ------------
C     *
C     * AS IN FOURF, THIS ROUTINE DEPENDS ON DATA IN /GRD/ BEING ALIGNED
C     * APPROPRIATELY (TO INCREASE THE EFFECTIVE VECTOR LENGTH). THE
C     * TOTAL NUMBER OF LEVELS (FOR ALL VARIABLES) IS "NLEV". 

C     * THE DATA IN THE ABOVE GRID FIELDS IS NOT IN THE OPTIMAL ORGANIZATION
C     * FOR THE PRESENT ROUTINE.  ON ENTRY INTO THIS ROUTINE, THE DATA IN
C     * THE ABOVE COMMON BLOCK IS TRANSPOSED INTO WORKING ARRAY "FOUR1".
C     *
C
C     * OUTPUT FIELDS:
C     * -------------
C
C     * Fourier array:  The FOURJ1 array is big enough to contain all
C     * the data for the FFTs.
C
C     * WORK ARRAY:
C     * ----------
C
C     * THE ARRAY "WRKS" MUST BE DIMENSIONED AT LEAST AS LARGE AS THE SUM
C     * OF THE SIZES OF ALL THE INPUT GRID DATA.
C     *
C     * ADDITIONAL PARAMETERS
C     * ---------------------
C     *
C     * ILH  = FIRST DIMENSION OF COMPLEX ARRAYS.
C     * LON  = NUMBER OF DISTINCT LONGITUDES.
C     * NLAT = NUMBER OF LATITUDES PROCESSED IN SINGLE PASS. [AJS 29/SEP/92] 
C     * ILAT = NUMBER OF LATITUDES ON A SINGLE NODE.
C     * JOFF = OFFSET INTO TOTAL LATITUDE SET FOR GIVEN TASK.
C     * NLEV = NUMBER OF TOTAL VERTICAL LEVELS (ACROSS ALL VARIABLES)
C     *        BEING TRANSFORMED. 

C     *
C     * ROUTINES CALLED
C     * ---------------
C     *
C     * FFWFG4 - GRID TO FOURIER TRANSFORMS.
C     * SWAPF3X- INTERNAL DATA REORGANIZATION ROUTINES.
C     * SWAPF4X-
C     *
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * FOURIER OUTPUT ARRAYS.
C
      real fourj1(2,nlev,ilat,ilh)
C
C     * INPUT ARRAY.
C
      REAL GRID(*)
C
C     * WORK ARRAYS.
C
      real four1(2, ilh, nlat, nlev)
      real wrks (2, ilh, nlat, nlev)
C
C     * OTHER ARRAYS.
C
      REAL TRIGS(LON)
      INTEGER IFAX(*)
C-----------------------------------------------------------------------
      IR    = LM - 1 
      ILG   = 2*ILH 
C
C     * Transpose the grid input field into the respective
C     * "FOURx" working array.
C     * I.E.  GRID(2*ILH*NLAT+1,NLEV) to FOUR(2,ILH,NLAT,NLEV).
C     * This puts the grid data into column-wise format for the FFTs.
C
      call swapf3x(grid,ilh,nlat,nlev,lon,four1)
C
C     * ALL THE FFT ARE DONE AT ONCE. 
C 
      nlevn = nlev*nlat
      call ffwfg4(four1,ilh,four1,ilg,ir,lon,wrks,nlevn,ifax,trigs)
C
C     * After the FFT, the data must be again transformed into the
C     * format expected by the Legendre Transform routine.
C
      call swapf4x(fourj1,four1,nlev,nlat,ilat,joff,ilh)
C
      RETURN
      END
