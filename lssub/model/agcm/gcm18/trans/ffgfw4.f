      SUBROUTINE FFGFW4 (GD, ILG,FC,ILH,IR,LON,WRK,ILEV,IFAX,TRIGS) 
C   
C     * AUG 01/03 - M.LAZARE. - GENERALIZED USING "MAXLOT" PASSED IN
C     *                         COMMON BLOCK, INSTEAD OF HARD-CODED
C     *                         VALUE OF 256 ONLY VALID FOR THE NEC.
C     *                         THIS IS CONSISTENT WITH WHAT IS DONE IN VFFT3.
C     *                       - CALLS NEW VFFT3 INSTEAD OF VFFT2.
C     * NOV 01/92 - J.STACEY. PREVIOUS VERSION FFGFW3.
C
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,   
C     * COEFFICIENTS TO GRID.      
C     * ====================      
C     * FC    = FOURIER COEFFICIENTS,     
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,    
C     * GD    = GRID DATA,      
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS  
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN, 
C     *         SO OBVIOUSLY ILG MUST EQUAL 2*ILH,   
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE.  
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR),   
C     * LON   = NUMBER OF DISTINCT LONGITUDES,    
C     * WRK   = WORK SPACE,      
C     * ILEV  = NUMBER OF LEVELS.     
C         
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL FC(ILG,ILEV),GD(ILG,ILEV),WRK(ILG*MAXLOT)   
      REAL TRIGS(LON)     
      INTEGER IFAX(*)

      COMMON /ITRANS/ MAXLOT
C----------------------------------------------------------------- 
      ISIGN = +1       
      INC   =  1       
      IR121 =  (IR+1)*2 +1      
      IF(ILG.LT.LON+2) CALL XIT('FFGFW4',-1)    
C  
C     * ABORT IF WORKSPACE SIZE OF "WRK" INSUFFICIENT.
C
      IF(MAXLOT.GT.ILEV) CALL XIT('FFGFW4',-2) 
C         
C     * SET TO ZERO FOURIER COEFFICIENTS BEYOND TRUNCATION.  
C
      DO 100 L=1,ILEV                
      DO 100 I=IR121,LON+2      
  100 FC(I,L) =0.       
C         
C     * AS MANY AS MAXLOT ARE DONE AT ONCE FOR VECTORIZATION.  
C         
      NSTART=1        
      NTIMES=ILEV/MAXLOT       
      NREST =ILEV-NTIMES*MAXLOT      
C         
      IF(NREST.NE.0) THEN      
         LENGTH=NREST       
         NTIMES=NTIMES+1      
      ELSE        
         LENGTH=MAXLOT       
      ENDIF        
C         
C     * DO THE FOURIER TRANSFORMS.     
C         
      DO 300 N=1,NTIMES       
       CALL VFFT3(FC(1,NSTART),WRK,TRIGS,IFAX,INC,ILG,LON,LENGTH,ISIGN)
       NSTART=NSTART+LENGTH      
       LENGTH=MAXLOT       
  300 CONTINUE        
C         
      RETURN        
C-------------------------------------------------------------------- 
      END        
