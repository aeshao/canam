      SUBROUTINE SWAPF1X(FOUT,FIN,ILEV,NLAT,ILAT,JOFF,ILH)

C     * AUG 3/2003 - M.LAZARE. LIKE SWAPF1 EXCEPT OUTPUT ARRAY
C     *                        IS FOUT(2,ILH,NLAT,ILEV) INSTEAD OF
C     *                        F(ILEV,NLAT,2,ILH), AND INPUT ARRAY
C     *                        IS OVER ALL ILAT LATITUDES (NO
C     *                        WORKS WORK ARRAY).
C     * NOV 12/92. -  A.J.Stacey. PREVIOUS VERSION SWAPF1.
C
C     * Transpose FIN(2,ilev,ilat,ilh) to FOUT(2,ilh,nlat,ilev) for
C     * this task.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real fin (2,ilev,ilat,ilh)
      real fout(2,ilh,nlat,ilev)
C-----------------------------------------------------------------------
      do l=1,ilev
      do j=1,nlat
      do i=1,ilh
        fout(1,i,j,l) = fin(1,l,joff+j,i)
        fout(2,i,j,l) = fin(2,l,joff+j,i)
      enddo
      enddo
      enddo 
C
      return
      end
