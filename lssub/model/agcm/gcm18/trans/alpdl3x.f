      SUBROUTINE ALPDL3X(DELALP,ALP,LSR,LM,NLAT,IRAM)
C 
C     * OCT 29/03 - M. LAZARE. REVERSE ORDER OF POLYNOMIALS
C     *                        AND DO OVER ALL LATITUDES.
C     * OCT 15/92 - PREVIOUS VERSION ALPDL3.
C     * JUL 14/92 - PREVIOUS VERSION ALPDL2.
C 
C     * COMPUTES LAPLACIAN OF LEGENDRE POLYNOMIALS. 
C     * LSR CONTAINS ROW LENGTH INFO. 
C 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DELALP(NLAT,IRAM), ALP(NLAT,IRAM) 
      INTEGER LSR(2,LM+1)
C-------------------------------------------------------------------- 
      DO 300 M=1,LM 
        KL=LSR(2,M) 
        KR=LSR(2,M+1)-1 
        DO 220 K=KL,KR
          NS=(M-1)+(K-KL) 
          FNS=FLOAT(NS) 
          DO 210 NJ=1,NLAT
            DELALP(NJ,K)=-FNS*(FNS+1.)*ALP(NJ,K)
  210     CONTINUE
  220   CONTINUE   
  300 CONTINUE    
C 
      RETURN
      END 
