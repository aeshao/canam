      SUBROUTINE SWAPS1(S,WRKS,WRKS1,LA,ILEV)

C     * NOV 20/98 - M.LAZARE. LIKE ROUTINE SWAPS EXCEPT
C     *                       EXPLICITLY CODED TO GO FROM
C     *                       WRKS TO S. ALSO ADDED WORK
C     *                       ARRAY "WRKS1" WITH INDEX
C     *                       OF "LA+1" TO AVOID POTENTIAL
C     *                       BANK CONFLICTS.
C     * NOV 12/92. -  A.J.Stacey.
C
C     * Transpose S(2,LA,ILEV) from WRKS(2,ILEV,LA)
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real s(2,la,ilev),wrks(2,ilev,la),wrks1(2,ilev,la+1)
C-----------------------------------------------------------------------
C     * first determine if LA is an exact multiple of 32 to
C     * see if bank conflicts may be a problem.
C
      if(mod(la,32).ne.0) then
        do 10 j=1,ilev
        do 10 i=1,la
          s(1,i,j) = wrks(1,j,i)
          s(2,i,j) = wrks(2,j,i)
   10 continue
      else
        do 20 i=1,la
        do 20 j=1,ilev
          wrks1(1,j,i) = wrks(1,j,i)
          wrks1(2,j,i) = wrks(2,j,i)
   20   continue
C
        do 30 j=1,ilev
        do 30 i=1,la
          s(1,i,j) = wrks1(1,j,i)
          s(2,i,j) = wrks1(2,j,i)
   30   continue 
      endif
C
      return
      end
