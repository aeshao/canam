      SUBROUTINE LEGBS2(SPEC,FOUR1,ALP,
     1                  WL,WOCSL,LEVS,NLAT,
     2                  M,NTOT)
C
C     * SEP 11/06 - M. LAZARE. NEW VERSION FOR GCM15F:
C     *                        - CALLS NEW VFASTX2 TO SUPPORT 32-BIT DRIVER.
C     * OCT 29/03 - M. LAZARE. PREVIOUS VERSION LEGBS2 FROM IBM CONVERSION:
C     * OCT 29/03 - M. LAZARE. NEW ROUTINE TO DO EXCLUSIVELY THE BACKWARD
C     *                        LEGENDRE TRANSFORM FOR MOISTURE, BASED ON
C     *                        OLD CODE IN MHANLQ_ AND USING DGEMM.
C     *                        THIS IS USED ONLY IN SEMI-LAG INITIALIZTION
C     *                        LOOP.  
C     *  
C     * TRANSFORM FOURIER COEFFICIENTS FOR MOISTURE TO SPECTRAL TENDENCIES
C     * FOR THIS (POSSIBLY CHAINED) GAUSSIAN LATITUDE.
C
C     * THIS IS CALLED FOR A GIVEN ZONAL WAVENUMBER INDEX "M", IE INSIDE
C     * A LOOP OVER M.
C     *
C     *    ALP = LEGENDRE POLYNOMIALS *GAUSSIAN WEIGHT
C     *
C     * INPUT FIELDS
C     * ------------
C     *
C     * THIS ROUTINE IS CALLED WITHIN A LOOP OVER ZONAL INDEX "M" AND
C     * THUS ACCESSES THE INPUT FOR THAT PARTICULAR ZONAL INDEX FROM
C     * THE CALLING ARRAY.
C     *
C     *    ESG --> FOUR1(2,LEVS,NLAT)
C     *
C     * OUTPUT FIELDS
C     * -------------
C     *
C     * THE OUTPUT DATA FIELDS ARE THE SPECTRAL MOISTURE WORK FIELD,
C     * ACCUMULATED INTO THE FOLLOWING DATA STRUCTURE:
C     *
C     *           SPEC(2,LEVS,NTOT)
C     *
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * Input Fourier Coefficients.
C
      real four1(2,levs,nlat)
C
C     * Output spectral tendencies.
C
      real spec(2,levs,ntot)
C
C     * Other arrays.   
C
      REAL*8 ALP(nlat,ntot+1)
      REAL*8 WL(nlat),WOCSL(nlat)
C-----------------------------------------------------------------------
      beta=0.
      call vfastx2(four1,spec,alp,
     1             beta,levs,nlat,ntot)
C
      RETURN
      END
