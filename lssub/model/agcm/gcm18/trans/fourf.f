      SUBROUTINE FOURF(PRESSF,FOUR1J,NLEV,
     1                 ILH,LON,LM,IFAX,TRIGS,NLAT,ILAT,JOFF,
     2                 FOUR1,WRKS)
C
C     * NOV 16/03 - M. LAZARE.   REVISED TO SUPPORT REVISED SWAPF1X.
C     * OCT 29/03 - M. LAZARE.   NEW ROUTINE BASED ON MHEXP_.
C     *                          LEGENDRE MOVED TO NEW LEGF ROUTINE.
C     * AUG 01/03 - M. LAZARE. - CALLS NEW FFGWF4,SWAPF1X,SWAPF2X.     
C     * MAY 20/03 - M. LAZARE. - MOVE CALL TO SWAPS2 TO MAIN DRIVER OUTSIDE
C     *                          OF PARALLEL REGION (ONLY NEEDS TO BE
C     *                          CALLED ONCE SINCE OPERATING ON SPECTRAL 
C     *                          FIELDS. THUS "WRKT" REMOVED.
C     *                        - "P2" REMOVED.   
C     *                        - DIMENSION->REAL.
C     *                        - OLD COMMENTED-OUT CODE REMOVED.
C     * NOV 22/98 - M. LAZARE. PREVIOUS VERSION MHEXP7.
C     *  
C     * CONVERTS FOURIER ARRAYS OF STREAMFUNCTION (P) AND 
C     * VELOCITY POTENTIAL (C) TO GRID POINT SLICES OF VORTICITY (QF),
C     * DIVERGENCE (DF), AND WINDS (UF,VF). 
C     * ALSO CONVERTS LN(SURFACE PRESSURE) AND ITS DERIVATIVES. 
C     * ALSO CONVERTS TEMPERATURE (T) AND DEW POINT DEPRESSION (ES).
C     * TRACER VARIABLE TRANSFORMED ONLY IF ITRAC.NE.0. 
C     *
C     * INPUT FIELDS  
C     * ------------
C
C     * Fourier array:  The FOUR1J array is big enough to contain all
C     * the data for the FFTs.
C
C     * OUTPUT FIELDS
C     * -------------
C     *
C     * THE TRANSFORMED REAL GRID FIELDS ARE ALIGNED AS IN THE MAIN
C     * GCM DRIVER, REFERENCED AT THE STARTING ADDRESS OF "PRESSF". 
C     *
C     * WORK SPACE
C     * ----------
C     * 
C     * THE ARRAY "WRKS" MUST BE DIMENSIONED AT LEAST AS LARGE AS THE SUM
C     * OF THE SIZES OF ALL THE INPUT GRID DATA.
C     *
C     * ADDITIONAL PARAMETERS
C     * ---------------------
C     *
C     *  ILH  = FIRST DIMENSION OF COMPLEX FOURIER ARRAYS.
C     *  LON  = NUMBER OF DISTINCT LONGITUDES AROUND A LATITUDE CIRCLE. 
C     *  NLAT = NUMBER OF LATITUDES PROCESSED IN SINGLE PASS. [AJS 29/SEP/92] 
C     *  ILAT = NUMBER OF LATITUDES ON A SINGLE NODE.
C     *  JOFF = OFFSET INTO TOTAL LATITUDE SET FOR GIVEN TASK.
C     *  NLEV = NUMBER OF TOTAL VERTICAL LEVELS (ACROSS ALL VARIABLES)
C     *         BEING TRANSFORMED. 
C     *
C     * ROUTINES CALLED
C     * ---------------
C     *
C     * FFGFW4 - FOURIER TO GRID TRANSFORMS.
C     * SWAPF1X- INTERNAL DATA RE-ORDERING ROUTINES.
C     * SWAPF2X-
C     
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * OUTPUT ARRAY (CONTIGUOUS AT THIS STARTING ADDRESS).
C
      REAL PRESSF(*) 
C
C     * INPUT ARRAY.
C
      real four1j(2, nlev, ilat, ilh)
C
C     * WORK ARRAYS.
C
      real four1 (2,  ilh, nlat, nlev)
      real wrks  (2,  ilh, nlat, nlev)
C
C     * OTHER ARRAYS.
C
      REAL TRIGS(LON)
      INTEGER IFAX(*)
C-------------------------------------------------------------------- 
      IR       = LM - 1 
      ILG      = 2*ILH 
C
C     * First transpose the input data from
C     * four1j(2,nlev,ilat,ilh) --> four1(2,ilh,nlat,nlev)
C
      call swapf1x(four1,four1j,nlev,nlat,ilat,joff,ilh)
C
C     * Now call the FFT.
C
      nlevn=nlev*nlat
      call ffgfw4(four1,ilg,four1,ilh,ir,lon,wrks,nlevn,ifax,trigs)

C     * Finally, store the data into the format used by the physics routines.
C     * four1(2*ilh,nlat,nlev) --> g(2*ilh*nlat+1,nlev)
C     * where "g" points at the starting address of "pressg" in
C     * the "GR" common block (i.e. pressg-psdlg, inclusive).
C
      call swapf2x(four1,nlev,nlat,ilh,lon,pressf)
      RETURN
      END
