      subroutine vfastx2(f,s,wlp,
     1                   beta,nlev,nlat,ntot)

c     * sep 11/2006 - f.majaess/ New version for gcm15f:
c     *               m.lazare.  Revised to call "sgemm" for r4i4 mode. 
c     * oct 29/2003 - r.mclay/  Previous version vfastx from IBM conversion:
c     *               m.lazare. New Legendre transform, like vfast except
c     *                         uses vendor-supplied DGEMM.
c     *                         ** note ** : WLP order must be reversed
c     *                         to use this! 
c     * oct 30/92. -  a.j.stacey. previous version vfast.
c
c     * Note: The usual input to routines such as blas is 32-bit reals
c     *       and integers. Since, however, we are using "dgemm", the
c     *       input is 64-bit reals and 32-bit integers. Hence, the
c     *       required interface changes are to use 4-byte integers for
c     *       any integers passed to "dgemm".
c     *
c     * Also note that since "S" and "F" are complex in the calling
c     * routine and since the level dimension is the innermost, all
c     * sizes referring to level are passed as "2*...".
c
      implicit none
      real s(2*nlev,ntot),f(2*nlev,nlat)
      real*8  wlp(nlat,ntot+1)
      real alpha, beta
c
c     * internal work array for 32-bit case.
c     * this is required since sgemm requires 32-bit arrays and
c     * "wlp" is 64-bit from agcm driver!
c
      real*4  wlp4(nlat,ntot+1)
c
      integer*4 ntot4, nlat4, nlev4
      integer nlev, nlat, ntot
      integer machine,intsize
c
c     * common block to hold word size.
c
      common /machtyp/ machine,intsize
c-----------------------------------------------------------------------
      alpha  = 1.
      ntot4  = ntot
      nlat4  = nlat
      nlev4  = 2*nlev
c
      wlp4(1:nlat,1:ntot+1)=wlp(1:nlat,1:ntot+1) 
c
      if (machine.eq.2) then
       call sgemm('N', 'N', nlev4 , ntot4, nlat4, alpha, f,
     1           nlev4, wlp4, nlat4, beta, s, nlev4)
      else
       call dgemm('N', 'N', nlev4 , ntot4, nlat4, alpha, f,
     1           nlev4, wlp, nlat4, beta, s, nlev4)
      endif
c
      return
      end
