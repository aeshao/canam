      SUBROUTINE SWAPF3X(G,ILH,NLAT,ILEV,LON,F)
C
C     * AUG 26/2003 - M.LAZARE. New version for revised FFT's.
C     * NOV 12/92 A.J. STACEY.  Previous version SWAPF3.
C     *
C     * Store G(2*ILH*NLAT+1,ILEV) into F(2,ILH,NLAT,ILEV).
C     *
C     * NOTE: This means that the 2 extra memory locations for each 
C     * transform are unused by the physics, but are inserted here before
C     * the data is passed to the transforms (note that LON+2 = 2*ILH = ILG).
C     *
C     * Note that it is very important that the input array be dimensioned
C     * with the unused memory locations explicitly allocated, or else 
C     * the starting addresses according to level and variable will not be
C     * maintained. To avoid memory bank conflicts, the inner dimension of
C     * "g" is augmented by one over what is used.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      real f(2*ilh,nlat,ilev)
      real g(2*ilh*nlat+1,ilev)
C-----------------------------------------------------------------------
      do 200 lev=1,ilev
        k = 1
        do 100 lat=1,nlat
          do 100 j=1,lon
            f(j,lat,lev) = g(k,lev)
            k = k + 1
  100   continue
  200 continue
c
      return
      end
