      SUBROUTINE RZERO(A,N)

C     * NOV 12/92. -  A.J.Stacey.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * ZERO OUT ARRAY "A".

      real a(n)
C-----------------------------------------------------------------------
      do 10 i=1,n
   10 a(i) = 0.0
C
      return
      end
