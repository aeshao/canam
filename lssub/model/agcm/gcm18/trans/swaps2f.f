      SUBROUTINE SWAPS2F(S,WRKS,S1,LA,ILEV,HOSKF)

C     * DEC 10/2004 - J.SCINOCCA. ADD HOSKINS SPECTRAL FILTER OF
C     *                           INPUT FIELDS.
C     * NOV 20/98 - M.LAZARE. PREVIOUS VERSION SWAPS2. 
C     *                       LIKE ROUTINE SWAPS EXCEPT
C     *                       EXPLICITLY CODED TO GO FROM
C     *                       S TO WRKS. ALSO ADDED WORK
C     *                       ARRAY "WRKS1" WITH INDEX
C     *                       OF "LA+1" TO AVOID POTENTIAL
C     *                       BANK CONFLICTS.
C
C     * Transpose S(2,LA,ILEV) to WRKS(2,ILEV,LA)
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real s(2,la,ilev),wrks(2,ilev,la),s1(2,la+1,ilev)
      real hoskf(2,la)
C-----------------------------------------------------------------------
C     * first determine if LA is an exact multiple of 32 to
C     * see if bank conflicts may be a problem.
C
      if(mod(la,32).ne.0) then
        do 10 j=1,la
        flt=HOSKF(1,j)
        do 10 i=1,ilev
          wrks(1,i,j) = s(1,j,i)*flt
          wrks(2,i,j) = s(2,j,i)*flt
   10   continue
      else
        do 20 i=1,ilev
        do 20 j=1,la
          s1(1,j,i) = s(1,j,i)
          s1(2,j,i) = s(2,j,i)
   20   continue
C
        do 30 j=1,la
        flt=HOSKF(1,j)
        do 30 i=1,ilev
          wrks(1,i,j) = s1(1,j,i)*flt
          wrks(2,i,j) = s1(2,j,i)*flt
   30   continue 
      endif
C
      return
      end
