      SUBROUTINE SWAPF2X(F,ILEV,NLAT,ILH,LON,G)
C
C     * AUG 26/2003 - M.LAZARE. New version for revised FFT's.
C     * NOV 12/92 A.J. STACEY.  Previous version SWAPF2.
C     *
C     * Store F(2,ILH,NLAT,ILEV) into G(2*ILH*NLAT+1,ILEV).
C     *
C     * NOTE: This means that the 2 extra memory locations for each 
C     * transform, unused by the physics, are extracted here before the 
C     * data is passed to the physics (note that LON+2 = 2*ILH = ILG).
C     *
C     * Note that it is very important that the output array be dimensioned
C     * with the unused memory locations explicitly allocated, or else 
C     * the starting addresses according to level and variable will not be
C     * maintained. To avoid memory bank conflicts, the inner dimension of
C     * "g" is augmented by one over what is used.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      real f(2*ilh,nlat,ilev)
      real g(2*ilh*nlat+1,ilev)
C--------------------------------------------------------------------------
      do 200 lev=1,ilev
        k = 1
        do 100 lat=1,nlat
        do 100 j=1,lon
          g(k,lev) = f(j,lat,lev)
          k = k + 1
  100   continue
  200 continue
C
      return
      end
