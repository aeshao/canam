      SUBROUTINE QDTFPC3(Q,D,P,C,RNS2LA,LA,ILEV,KASE)
C
C     * NOV 12/03 - M.LAZARE. ACCOUNT FOR FACT THAT RNS2LA(1) MAY NOT
C     *                       BE ZERO IN A MULTI-NODE ENVIRONMENT, IE
C     *                       IT IS ONLY ZERO ON NODE 0.
C     * DEC 10/97 - B.DENIS.  PREVIOUS VERSION QDTFPC2.
C 
C     * APR 11/79 - J.D.HENDERSON. ORIGINAL VERSION QDTFPC.
C
C     * CONVERTS BETWEEN (P,C) AND (Q,D) BASED ON VALUE OF KASE.
C 
C     * Q(LA,ILEV) = SPECTRAL VORTICITY 
C     * D(LA,ILEV) = SPECTRAL DIVERGENCE
C     * P(LA,ILEV) = SPECTRAL STREAMFUNCTION
C     * C(LA,ILEV) = SPECTRAL VELOCITY POTENTIAL
C 
C     * KASE = +1 CONVERTS P,C TO Q,D.
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
C     * LEVEL 2,Q,D,P,C 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL RNS2LA(LA)
      COMPLEX Q(LA,ILEV),D(LA,ILEV),P(LA,ILEV),C(LA,ILEV) 
C-------------------------------------------------------------------
C     * KASE = +1 CONVERTS P,C TO Q,D.
C 
      IF(KASE.LT.0) GO TO 310 
      DO 290 L=1,ILEV 
      DO 290 K=1,LA
        Q(K,L)=-1.*RNS2LA(K)*P(K,L) 
        D(K,L)=-1.*RNS2LA(K)*C(K,L) 
  290 CONTINUE
      RETURN
C 
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
  310 CONTINUE
      DO 390 L=1,ILEV 
        IF(RNS2LA(1).EQ.0.) THEN
          P(1,L)=0.
          C(1,L)=0.
        ELSE
          RFNS1 =1./RNS2LA(1)
          P(1,L)=-RFNS1*Q(1,L)
          C(1,L)=-RFNS1*D(1,L)
        ENDIF
C 
        DO 380 K=2,LA
          RFNS1 =1./RNS2LA(K)
          P(K,L)=-RFNS1*Q(K,L)
          C(K,L)=-RFNS1*D(K,L)
  380   CONTINUE
  390 CONTINUE
C 
      RETURN
      END
