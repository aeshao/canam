      SUBROUTINE FILLNAB(TRACNA,FSLICE,WFS,FAC,IOFF,
     1                   NTRAC,NTRACN,NTRSPEC,ITRADV,
     2                   ILEV,IJPAK,ILG,IL1,IL2)

C     * DEC 10/03 - M.LAZARE.   CONSISTENT WITH NEW METHODOLOGY FOR
C     *                         IBM PORT.
C     * OCT 29/02 - J.SCINOCCA. PREVIOUS VERSION FILNAB.

C     * UPDATES TRACNA WITH PHYSICS TENDENCIES (FSLICE) FOR NON-ADVECTED
C     * TRACERS. OTHERWISE, TENDENCIES ARE STORED INTO "WFS".
C     * "IOFF" IS OFFSET INTO TRACNA FOR THE PARTICULAR TASK.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL TRACNA (IJPAK,ILEV,NTRACN)
      REAL FSLICE (ILG,ILEV,NTRAC),WFS(ILG,ILEV,NTRSPEC)
      INTEGER ITRADV(NTRAC)
C--------------------------------------------------------------------
      NA=0
      NNA=0
      DO 300 N=1,NTRAC
         IF(ITRADV(N).EQ.1) THEN
            NA=NA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  WFS(I,L,NA)=FSLICE(I,L,N)
               ENDDO
            ENDDO
         ELSE
            NNA=NNA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  TRACNAO=TRACNA(IOFF+I,L,NNA)
                  TRACNA(IOFF+I,L,NNA)=MAX(TRACNAO+FAC*FSLICE(I,L,N),0.)
               ENDDO
            ENDDO
         ENDIF
 300     CONTINUE

      RETURN
      END
