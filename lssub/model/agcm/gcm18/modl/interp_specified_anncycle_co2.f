      SUBROUTINE INTERP_SPECIFIED_ANNCYCLE_CO2(RESULT,      ! OUTPUT
     1           DELT,GMT,IDAY,MDAYT)                       ! INPUT  
C
C     * AUG 10/2013 - M.LAZARE.
C
C     * INTERPOLATES BETWEEN TWELVE MID-MONTH VALUES REPRESENTING
C     * DEPARTURE FROM ANNUAL MEAN OF CO2_PPM, TO BE ADDED
C     * TO CO2_PPM (CONSERVING ANNUAL MEAN) TO GET SPECIFIED
C     * CO2 FOR LOWEST LEVEL UNDER THAT CPP OPTION.
C
C     * RESULT = INTERPOLATED VALUE TO ADD TO ANNUAL MEAN.
C
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * MDAYT  = DATE OF NEXT MID-MONTH.
C
C     * INTERNAL:
C     * MDAYT1 = DATE OF PREVIOUS MID-MONTH.
C     * MON    = INTEGER NUMBER OF NEXT MID-MONTH (IE MAY=5).
C     * MON1   = INTEGER NUMBER OF PREVIOUS MID-MONTH.
C
      IMPLICIT NONE
 
      REAL    RESULT,DELT,GMT
      INTEGER IDAY,MDAYT
C
      REAL DAY,FMDAY,FLDAY,DAYSM,DAYSL,STEPSM,STEPSL,CO2M,CO2P
      INTEGER L,M,MM,MON,MON1,MDAYT1
C
C     * MID-MONTH SPECIFIED CO2 DEPARTURE FROM ANNUAL MEAN.
C
      REAL CO2T(12)
      DATA CO2T /0.725193, 1.12429,        1.22137,   0.950729,
     1           0.504833, 5.2302983E-04, -0.434364, -0.958304,   
     2          -1.55863, -1.24994,       -0.469586,  0.217938 /
C
C     * MID-MONTH JULIAN DAY VALUES AS PER USUAL.
C
      INTEGER MMD(12)
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAYT AND
C     * FROM MDAYT1 TO HERE. 
C
      DAY=REAL(IDAY)+GMT/86400. 
C
C     * GET PREVIOUS MID-MONTH TARGET FOR INITIALIZATION AT KOUNT=0.
C
      M=0
      DO MM=1,12
        IF(MMD(MM).EQ.MDAYT) M=MM
      ENDDO 
      MON=M
      L=M-1
      IF(L.EQ.0) L=12
      MDAYT1=MMD(L)
      MON1=L
C
      FMDAY=REAL(MDAYT) 
      FLDAY=REAL(MDAYT1)
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      IF(FLDAY.GT.DAY) FLDAY=FLDAY-365.
      DAYSM=FMDAY-DAY 
      DAYSL=DAY-FLDAY
      STEPSM=DAYSM*86400./DELT
      STEPSL=DAYSL*86400./DELT
      CO2P=CO2T(MON)
      CO2M=CO2T(MON1)
C 
C     * NOW INTERPOLATE.
C 
      RESULT = (STEPSM*CO2M + STEPSL*CO2P) / (STEPSL+STEPSM)

      RETURN
      END 
