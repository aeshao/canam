      SUBROUTINE CALCW (WJ, OMEGJ,
     1                  DJ, UJ, VJ, TJ, PSLMJ, PSTHJ, PSJ,
     2                  SGJ, DSGJ, DSHJ, DLNSGJ, 
     3                  A1SGJ, B1SGJ, A2SGJ, B2SGJ, DB,
     4                  ILEV, ILG, IL1, IL2)
C
C     * SEP 07/2009 - M.LAZARE. NEW ROUTINE FOR GCM15I.
C  
C     * CALCULATES VERTICAL VELOCITY ON PHYSICS GRID (M/SEC).
C     * CODE PARALLELS CALCULATION ON DYNAMICS GRID IN DYNCAL4D. 
C
      IMPLICIT NONE
C
C     * OUTPUT FIELDS:
C
      REAL WJ(ILG,ILEV), OMEGJ(ILG,ILEV)
C  
C     * PRIMARY INPUT FIELDS:
C  
      REAL  DJ (ILG,ILEV), TJ (ILG,ILEV)
      REAL  UJ (ILG,ILEV), VJ (ILG,ILEV)
      REAL  PSLMJ(ILG), PSTHJ(ILG), PSJ(ILG)
C  
C     * VERTICAL DISCRETIZATION INPUT FIELDS:
C  
      REAL  DSGJ  (ILG,ILEV), DSHJ  (ILG,ILEV)
      REAL  SGJ   (ILG,ILEV), DLNSGJ(ILG,ILEV)
      REAL  A1SGJ (ILG,ILEV), B1SGJ (ILG,ILEV)
      REAL  A2SGJ (ILG,ILEV), B2SGJ (ILG,ILEV)
      REAL  DB (ILEV)
C
C     * WORK FIELDS:
C  
      REAL VGRPSJ(ILG,ILEV)
      REAL SSDRJK(ILG)
C
C     * SCALARS:
C
      REAL OMSPJ,DRJKI,DRJKPI
      REAL WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,RGASV,CPRESV
      INTEGER K,I,ILEV,ILG,IL1,IL2
C
      COMMON /PARAMS/ WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1                RGASV,CPRESV
C-----------------------------------------------------------------------
      SSDRJK=0.
C
      DO 150 K = 1,ILEV
      DO 150 I = IL1,IL2
         VGRPSJ(I,K) =  UJ(I,K)*PSLMJ(I) + VJ(I,K)*PSTHJ(I)
  150 CONTINUE

C     ----------------------------------------------------------------- 
C     CALCULATE OMSPJ (OMEGA SUR P) AND DIAGNOSTIC W
C     ----------------------------------------------------------------- 
  
      DO 200 K=1,ILEV-1
      DO 200 I = IL1,IL2
         DRJKI      =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K) 
         DRJKPI     =  DJ(I,K+1) + VGRPSJ(I,K+1)*DB(K+1)/DSGJ(I,K+1) 
  
         OMSPJ      = ( - SSDRJK(I)*DLNSGJ(I,K)
     1                  - DRJKI*A1SGJ(I,K) - DRJKPI*A2SGJ(I,K+1) 
     2                  + VGRPSJ(I,K)*B1SGJ(I,K) 
     3                  + VGRPSJ(I,K+1)*B2SGJ(I,K+1) )/DSHJ(I,K) 
         OMEGJ(I,K) = OMSPJ * PSJ(I) * SGJ(I,K)
         WJ(I,K)    = -1. * (RGAS*TJ(I,K)/GRAV) * OMSPJ
         SSDRJK(I)  = SSDRJK(I) +DRJKI*DSGJ(I,K) 
  200 CONTINUE
  
C     ----------------------------------------------------------------- 
  
      K=ILEV
      DO 250 I = IL1,IL2
         DRJKI      =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K) 
         OMSPJ      = ( - SSDRJK(I)*DLNSGJ(I,K) - DRJKI*A1SGJ(I,K) 
     1                  + VGRPSJ(I,K)*B1SGJ(I,K) )/DSHJ(I,K) 
         OMEGJ(I,K) = OMSPJ * PSJ(I) * SGJ(I,K)
         WJ   (I,K) = -1. * (RGAS*TJ(I,K)/GRAV) * OMSPJ
  250 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 
