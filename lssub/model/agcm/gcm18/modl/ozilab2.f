      SUBROUTINE OZILAB2(LO,LEVOZ,IOZTYP)

C     * JAN 09/2017 - D.PLUMMER MODIFIED TO INCLUDE LABELS FOR
C                                CMIP6 OZONE (IOZTYP=6)
C     * JUN 28/2009 - M.LAZARE. NEW VERSION FOR GCM15I:
C     *                         - FOR RANDEL OZONE DATA (IOZTYP=4),
C     *                           USE ACTUAL PRESSURE LEVELS RATHER
C     *                           THAN {1,LEVOZ}.
C     *                           **NOTE**! EXTRA FIELD IOZTYP PASSED IN!
C     * APR 30/2003 - M.LAZARE. PREVIOUS VERSION OZILAB UP TO GCM15H.
C
C     * DEFINES LEVEL INDEX VALUES FOR INPUT OZONE FIELDS.
C
      implicit none
C
      INTEGER, INTENT(in) :: levoz, ioztyp
      INTEGER, INTENT(out) :: lo(levoz)
C
C     * PRESSURE-LEVEL DATA FOR IOZTYP=4 (RANDEL).
C
      INTEGER, PARAMETER, DIMENSION(24) :: 
     &  p4 = (/ -100, -150, -200, -300, -500,
     &          -700,   10,   15,   20,   30,
     &            50,   70,   80,  100,  150,
     &           200,  250,  300,  400,  500,
     &           600,  700,  850, 1000 /)
C
C     * PRESSURE-LEVEL DATA FOR IOZTYP=6 (CMIP6)
C
      INTEGER, PARAMETER, DIMENSION(49) :: 
     &  p6 = (/ -4500, -3100, -3200, -3400, -3700,
     &          -2100, -2200, -2400, -2700, -1100,
     &          -1200, -1400, -1700,  -100,  -150,
     &           -200,  -300,  -400,  -500,  -700,
     &             10,    15,    20,    25,    30,
     &             35,    40,    50,    60,    70,
     &             80,    90,   100,   115,   130,
     &            150,   170,   200,   250,   300,
     &            350,   400,   500,   600,   700,
     &            800,   850,   925,  1000 /)
C
      INTEGER l
C
C-----------------------------------------------------------------------
      IF(ioztyp.eq.4)     THEN
        DO l=1,levoz
          lo(l)=p4(l)
        ENDDO
      ELSEIF(ioztyp.eq.6) THEN
        DO l=1,levoz
          lo(l)=p6(l)
        ENDDO
      ELSE
        DO l=1,levoz
          lo(l)=l
        ENDDO
      ENDIF
C
      RETURN
      END
