      SUBROUTINE NAME(NAM,IC,INT)

C     * NOV 04/97 - M.HOLZER,R.HARVEY. LIKE "NOM" EXCEPT WRITES OUT IN
C     *                                FORMAT "XM01". 
C     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.)
C
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER NAM*8
      CHARACTER*1 IC(2)
C----------------------------------------------------------------------
C===  N3=INT/100
C===  NN=INT-N3*100
      N2=INT/10
      N1=INT-N2*10
      IF(INT.GT.99) CALL                           XIT('NAME',-1)
      IF(IC(2).EQ.' ') IC(2)='0'
      WRITE(NAM,1000) IC,N2,N1

 1000 FORMAT(2A1,2I1,'    ')
      RETURN
      END
