      SUBROUTINE AIRLAB(LA,LEVAIR)
C
C     * SEP 21/2009 - K.VONSALZEN. NEW ROUTINE FOR GCM15I.
C
C     * DEFINES LEVEL INDEX VALUES FOR INPUT EMISSIONS FROM AIRCRAFT.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      INTEGER LA(LEVAIR)
C-----------------------------------------------------------------------
      DO 100 L=1,LEVAIR
        LA(L)=L
  100 CONTINUE
C
      RETURN
      END
