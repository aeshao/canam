      SUBROUTINE RLNCON5(TENER,TFICQ,QSRCRAT,QSRCRM1,WPP,WP0,WPM,   
     1                   TFICQM,ITRADV,ITRVAR,DELT,FS,KOUNT,DOFILTR,
     2                   QSRC0,QSRCM,QSCL0,QSCLM,
     3                   QTOT,SCAL,SMIN) 
C
C     * APR 25/10 - M.LAZARE.    NEW VERSION FOR GCM15I:
C     *                          - CALCULATION OF QSRCRAT AND QSRC0
C     *                            ONLY DONE IF QTOT>0.
C     * MAY 05/03 - K.VONSALZEN. PREVIOUS VERSION RLNCON4 FROM GCM14
C     *                          THROUGH GCM15H:
C     *                          - CORRECT PROBLEMS WITH MASS FIXER.
C     *                          - ADD CALCULATION OF QSRC0.
C     *                          - "ITRADV" AND "ITRVAR" PASSED IN AND
C     *                            USED TO DEFINE PROPER DEFAULT
C     *                            (NON-ADVECTING) FOR QSRCRAT, DEPENDING
C     *                            ON WHETHER ITRVAR=SL3D OR ITRVAR=SLQB.
C     * SEP 30/99 - M.LAZARE. PREVIOUS VERSION RLNCON3 FOR GCM13.
C     * NOV. 18/97  - R. HARVEY/ PREVIOUS VERSION RLNCON2 FOR GCM12.
C     *               M. HOLZER. 
C
C     * SUBROUTINE QUI CALCULE L'EXCES OU LE DEFICIT DE MASSE DE CHACUN DES 
C     * TRACEURS A CHAQUE PAS DE TEMPS
  
C     *   TENER    MASSE GLOBALE DE LA QUANTITE X AU TEMPS T AVANT CORRECTION 
C     *   TFICQ    MASSE Y AJOUTEE AU SYSTEME DANS L'INTERVALLE DE TEMPS DT 
C     *            PRECEDENT, DUE A LA PRESENCE DES TERMES DE SOURCE. 
C     *     WP0    MASSE GLOBALE DE LA QUANTITE X AU TEMPS T TELLE QUE PREDIT 
C     *            AU TEMPS T-DT. 
C     *     WPP    ESTIMATION DE LA MASSE DE X AU TEMPS T+DT
C     * QSRCRAT    CORRECTION APPORTEE A LA MASSE GLOBALE AU TEMPS T
C     * QSRCRM1    CORRECTION APPORTEE A LA MASSE GLOBALE AU TEMPS T-1
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      LOGICAL DOFILTR,PNT
      DATA PNT /.FALSE./    
C---------------------------------------------------------------------- 
C     * QSRCRAT=0 FROM "INIT" COMMON DECK AT KOUNT=0,
C     * FOR GENERAL CONSERVATION DONE ON PHYSICS GRID.
C
      IF(KOUNT.EQ.0) THEN 
         WP0=TENER
         WPM=TENER
         TFICQM=TFICQ 
         IF(ITRVAR.EQ.NC4TO8("SL3D") .OR. ITRVAR.EQ.NC4TO8("SLQB"))
     1      QSRCRAT=1.
         QSRCRM1=QSRCRAT
         QSRCM=QSRC0
         QSCLM=QSCL0
         WPP=WP0 + DELT*TFICQ
      ELSE
         DOFILTR=.TRUE.                    
         TEMP=WPP 
         WPM=WP0
         WP0=TEMP 
         WPP=WPM + 2.*DELT*TFICQ 
         IF(PNT) WRITE(*,*) ' 2.*DELT*TFICQ',2.*DELT*TFICQ
C
         IF(TFICQM.EQ.0.0 .AND. TFICQ.NE.0.0) THEN
             IF(PNT) PRINT *,' INJECTION OF TRACER AT NEXT TIME STEP.'
             IF(PNT) PRINT *,' TIME FILTER NOT DONE FOR XP0.'
             DOFILTR=.FALSE.
         ENDIF
         TFICQM=TFICQ 
         QSRCRM1=QSRCRAT
         QSRCM=QSRC0
         QSCLM=QSCL0
C
         IF(PNT) WRITE(*,*) ' ****KOUNT=', KOUNT
         IF(PNT) WRITE(*,*) ' TENER=',TENER
         IF(PNT) WRITE(*,*) ' WPM,WP0,WPP = ', WPM,WP0,WPP

C===     INFLAG = 0
         IF((WP0.EQ. 0.0).AND.(TENER.GT.0.0)) THEN
             IF(PNT) WRITE(*,*) ' INJECTION AT KOUNT=', KOUNT
             WP0=WPP
             IF(PNT) WRITE(*,*) ' TIME FILTER NOT DONE FOR XP0.'
C===         INFLAG = 1
             DOFILTR=.FALSE.
         ENDIF
C
         IF(TENER .GT. 0. .AND. ITRADV.NE.0 .AND. QTOT .GT. 0.) THEN
           QSRCRAT=WPP/TENER
           QSRCRAT=MAX(QSRCRAT,0.01)
           QSRCRAT=MIN(QSRCRAT,100.)
           QSRC0=TENER/QTOT
         ELSE
           IF(PNT) WRITE(*,*) ' IN RLNCONS TENER=',TENER
           IF(PNT) WRITE(*,*) ' QSRCRAT SET TO UNITY'
           QSRCRAT=1.
           QSRC0=1.  
         ENDIF
         QSCL0=0.
         IF (QSRCRAT.LT.1.) THEN
           QSCL0=((1.-SMIN)/(1.-QSRCRAT)-QSRC0*SCAL)/(1.-QSRC0*SCAL)
           QSCL0=MAX(MIN(QSCL0,1.),0.)
         END IF
C
         IF(PNT) WRITE(*,*) ' QSRCRAT=', QSRCRAT
C 
C        * FOLLOWING LINE IS TO BE INCLUDED IF DESIRE TIME
C        * FILTER.
C 
         IF(PNT)                      THEN
           IF(DOFILTR) PRINT *,' TIME FILTER DONE FOR XP0.'
           IF(.NOT.DOFILTR) PRINT *,' TIME FILTER NOT DONE FOR XP0.'
         ENDIF
         IF(DOFILTR) THEN
             WP0=FS*WPP + (1.-2.*FS)*WP0 + FS*WPM
         ENDIF
C 
      ENDIF 
  
      RETURN
      END 
