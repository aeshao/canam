      SUBROUTINE IMVRAI2 (UG,VG,PSDLG,PSDPG, COSJ,ILG,LON,ILEV,A) 
C
C     * OCT 15/92 - M.LAZARE. COSJ NOW A PASSED ARRAY FOR MULTI-
C     *                       LATITUDE FORMULATION.   
C     * JUL 14/92 - PREVIOUS VERSION IMAVRAI.
C
C     * CONVERT WIND IMAGES AND D(PS)/DX AND /DY TO REAL VALUES.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL   UG(ILG,ILEV),VG(ILG,ILEV),PSDLG(ILG),PSDPG(ILG)
      REAL*8 COSJ(ILG)
C---------------------------------------------------------------- 
      DO 200 L=1,ILEV 
         DO 100 I=1,LON 
            UG(I,L)   = UG(I,L)     *(A/COSJ(I)) 
            VG(I,L)   = VG(I,L)     *(A/COSJ(I)) 
  100    CONTINUE 
  200 CONTINUE
C 
      DO 300 I=1,LON
            PSDLG(I)  = PSDLG(I)*(1./(A*COSJ(I)))
            PSDPG(I)  = PSDPG(I)*(1./(A*COSJ(I)))
  300 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END
