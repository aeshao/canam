      SUBROUTINE TOPLW(SHB,PTOIT,ILEV)
C
C     * APR 26/2006 - M.LAZARE. DEFINES INTEGER VARIABLE "LWTOP" WHICH
C     *                         SEPARATES FULL L/W ASSUMPTIONS (BELOW
C     *                         THIS MODEL LEVEL) FROM SIMPLIFIED ONES
C     *                         ABOVE IT.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL SHB(ILEV)
C
      COMMON /ITOPLW/ LWTOP
C
C     * THRESHOLD IN MBS.
C
      DATA THRESH/0.99/
C============================================================================
C     * PRESSURE IN MBS OF UPPER INTERFACE OF TOP LAYER.
C
C
      PFULL0=0.01*PTOIT
      LWTOP=1
C
C     * IF TOP LAYER BELOW THRESHOLD, XIT WITH LWTOP=1. OTHERWISE, CALCULATE
C     * LAYAER INDEX FOR WHICH THE TOP INTERFACE PRESSURE FIRST EXCEEDS 
C     * "THRESH".
C
      IF(PFULL0.LT.THRESH)                                     THEN
        ISTEP=0
        DO L=1,ILEV
          PFULL=SHB(L)*1000.
          IF(ISTEP.EQ.0 .AND. PFULL.GE.THRESH)          THEN
            LWTOP=L+1
            ISTEP=1        
          ENDIF
        ENDDO
      ENDIF
C
      RETURN
      END
