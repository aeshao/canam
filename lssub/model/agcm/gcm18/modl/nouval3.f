      SUBROUTINE NOUVAL3
     1                 ( ZP, DP, TP, QP, LPSP, XP, ITRAC, NTRAC,
     2                   IR1, LA, LM, LSR, NK, NM, NMAX,
     3                   RZ, RD, RT, RQ, RPS, RX, 
     4                   SSDPK, SSRTK,
     5                   OSIILA, DEL2LA,
     6                   AM, BM, CM,
     7                   PHIS,TREF,R,DT)

C     * FEB 02/94 - M.LAZARE. USE TRIANGULAR-FILLED "DEL2LA"/"OSIILA" 
C     *                       INSTEAD OF "DEL2"/"OSII" TO OPTIMIZE LOOPS
C     *                       1001-1003 AND 2002.. 
C     * AUG 14/90 - J-D.GRANDPRE. - PREVIOUS VERSION NOUVAL2.
C 
C     * MODELE SPECTRAL,SEMI-IMPLICITE,ELEMENTS FINIS CONSTANTS NOUVAL
C     * SOLUTION DU SCHEME SEMI-IMPLICITE 
C     * ON COMBINE LES COTES DROITS RD, RT ET RPS ET
C     * ON MULTIPLIE LE RESULTAT PAR OSII (CALCULE PAR MATCAL)
C     * POUR OBTENIR D+, ON SUBSTITUE ENSUITE D+ DANS L'EQN 
C     * THERMODYNAMIQUE POUR OBTENIR T+ ET DANS L'EQN DE
C     * CONTINUITE POUR OBTENIR LPS+. 
C 
C     ON CALCULE LES QUANTITES
C 
C S          ZP(MN,K)        : Z+, TOURBILLON A T+DT. 
C S          DP(MN,K)        : D+, DIVERGENCE A T+DT. 
C S          TP(MN,K)        : T+, TEMPERATURE A T+DT.
C S          QP(MN,KM)       : Q+, VARIABLE HUMIDITE A T+DT.
C S          XP(MN,K)        : X+, F(TRACEUR) A T+DT. 
C S          LPSP(MN)        : LPS+, LOG DE LA PRESSION DE SURFACE A T
C 
C    POUR CHAQUE COMPOSANTE SPECTRALE MN ET CHAQUE COUCHE K 
C 
C E          IR1             : IR1=LRS(1), LONGUEUR MAX. DE LA COLONNE M
C E          KHEM            :  0 (GLOBAL), 1 (HEMIS. N.), 2 (HEMIS. S.)
C E          LA              : NOMBRE TOTAL DE COMPOSANTES SPECTRALES 
C E          LM              : (NOMBRE D'ONDE EST-OUEST MAXIMUM+1)
C E          LRA(M)          : NOMBRE DE COMP. SPECT. ANTIS. D'ORDRE M. 
C E          LRS(M)          : NOMBRE DE COMP. SPECT. SYM. D'ORDRE M. 
C E          NK              : NOMBRE DE COUCHES. 
C E          NM              : NOMBRE DE COUCHES D'HUMIDITIE. 
C E          NMAX            : (VALEUR  MAXIMUM DE N)+1 
C 
C     A PARTIR DES DONNEES D'ENTREE 
C 
C E          RZ(MN,K)        : COTE DROIT, EQUATION DU TOURBILLON.
C E          RD(MN,K)        : COTE DROIT, EQUATION DE LA DIVERGENCE. 
C E          RT(MN,K)        : COTE DROIT, EQUATION THERMODYNAMIQUE.
C E          RQ(MN,KM)       : COTE DROIT, EQUATION D'HUMIDITE. 
C E          RX(MN,K)        : COTE DROIT, EQUATION DU TRACEUR. 
C E          RPS(MN)         : COTE DROIT, EQUATION DE CONTINUITE.
C E          PHIS(MN)        : GEOPOTENTIEL DE SURFACE
C 
C     ON SE SERT DES CHAMPS DE TRAVAIL SUIVANTS 
C     SSDPK ET SSRTK PEUVENT ETRE EQUIVALENCES DANS L'APPEL.
C 
C E          SSDPK(MN)       : SOMME DES COUCHES 1 A K DE D+*DSIGMA.
C E          SSRTK(MN)       : SOMME DE K+1 A NK DE R*DLNSIGMA*RT.
C 
C     ON SE SERT AUSSI DES PARAMETRES SUIVANTS
C 
C E          OSII(N,K,K)     : MATRICE INVERSE DANS L'EQUATION DE DIVERG
C E          FDIF(N)         : CONSTANTE DE DIFFUSION HORIZ.
C E          DEL2(N+1)         : -N(N+1)/A**2.
C E          ALFA1(K)        : ( 1.-SIGMAM*DLNSIGMA/DSIGMA).
C E          ALFA2(K)        : ( 1.-SIGMAM*DLNSIGMA/DSIGMA).
C E          DSG(K)          : DSIGMA, EPAISSEUR DE LA COUCHE K.
C E          TREF            : TREF,UNE CONSTANTE.
C E          R               : CONSTANTE DES GAZ PARFAITS.
C E          DT              : LONGEUR DU PAS DE TEMPS EN SECONDE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX ZP (LA,NK),DP (LA,NK),LPSP(LA),TP (LA,NK),QP (LA,NM)
      COMPLEX RT (LA,NK),RQ (LA,NM),RPS (LA),RD (LA,NK),RZ (LA,NK)
      COMPLEX XP(LA,NK,NTRAC), RX(LA,NK,NTRAC)
      COMPLEX PHIS(LA)
C 
C 
C            CHAMPS DE TRAVAIL. 
C 
      COMPLEX  SSDPK(LA), SSRTK(LA) 
C 
      REAL OSIILA(LA,NK,NK), DEL2LA(LA) 
      REAL AM(NK,NK), BM(NK,NK), CM(NK,NK)
C 
      INTEGER  LSR(2,LM)
C 
C-----------------------------------------------------------------------
C            ETAPE 1 :     SOLUTION DU SCHEME SEMI-IMPLICITE
C                      C)  ON FINALISE LE CALCUL DE RD EN Y AJOUTANT
C                          LES CONTRIBUTIONS DE RT ET RPS.
C     ------------------------------------------------------------------
C
      K=NK
      DO 1001 MN=1,LA
          SSRTK(MN)  = R*TREF*RPS(MN) + PHIS(MN)/DT
          RD(MN,K)   = RD(MN,K) 
     1                -DT*DEL2LA(MN)*( SSRTK(MN) + AM(K,K)*RT(MN,K)
     2                                 + AM(K-1,K)*RT(MN,K-1) )
 1001 CONTINUE
C
      DO 1002 K=NK-1,2,-1 
      DO 1002 MN=1,LA
          SSRTK(MN)  = SSRTK(MN) + AM(K+1,K)*RT(MN,K+1)
          RD(MN,K)   = RD(MN,K) 
     1                -DT*DEL2LA(MN)*( SSRTK(MN) + AM(K,K)*RT(MN,K)
     2                               + AM(K-1,K)*RT(MN,K-1) )
 1002 CONTINUE
C
      K=1 
      DO 1003 MN=1,LA
          SSRTK(MN)  = SSRTK(MN) + AM(K+1,K)*RT(MN,K+1)
          RD(MN,K)   = RD(MN,K) 
     1                -DT*DEL2LA(MN)*( SSRTK(MN) + AM(K,K)*RT(MN,K) )
 1003 CONTINUE
C
C     ------------------------------------------------------------------
C            ETAPE 2 :     SOLUTION DU SCHEME SEMI-IMPLICITE
C                      D)  ON OBTIENT LES VALEURS DE D+ 
C     ------------------------------------------------------------------
C 
      DO 2001 K=1,NK
          DO 2001 MN=1,LA 
              DP(MN,K)=(0.,0.)
 2001 CONTINUE
C 
      DO 2002 K=1,NK
      DO 2002 L=1,NK
      DO 2002 MN=1,LA
          DP(MN,K) = DP(MN,K) + OSIILA(MN,L,K)*RD(MN,L) 
 2002 CONTINUE
C
      DO 2003 K=1,NK
          DO 2003 MN=1,LA 
              DP(MN,K)=DP(MN,K)*2.*DT 
 2003 CONTINUE
C 
C     ------------------------------------------------------------------
C            ETAPE 3 :     SOLUTION DU SCHEME SEMI-IMPLICITE
C                      E)  ON SUBTITUE LES VALEURS DE RT ET RPS 
C                          POUR OBTENIR T+ ET LPS+. 
C     ------------------------------------------------------------------
C 
      K=1 
          DO 3001 MN=1,LA 
               SSDPK(MN) = (0.,0.)
               TP(MN,K) = ( 2.*RT(MN,K) 
     1                    -BM(K,K)*DP(MN,K)-BM(K+1,K)*DP(MN,K+1) )*DT 
 3001 CONTINUE
C 
      DO 3002 K=2,NK-1
          DO 3002 MN=1,LA 
               SSDPK(MN)  = SSDPK(MN) + DP(MN,K-1)*CM(K-1,K)
               TP(MN,K) = ( 2.*RT(MN,K) 
     1                   - (BM(K-1,K)/CM(K-1,K))*SSDPK(MN)
     1                    -BM(K,K)*DP(MN,K)-BM(K+1,K)*DP(MN,K+1) )*DT 
 3002 CONTINUE
C 
      K=NK
          DO 3003 MN=1,LA 
               SSDPK(MN)  = SSDPK(MN) + DP(MN,K-1)*CM(K-1,K)
               TP(MN,K) = ( 2.*RT(MN,K) 
     1                   - (BM(K-1,K)/CM(K-1,K))*SSDPK(MN)
     1                    -BM(K,K)*DP(MN,K) )*DT
                SSDPK(MN) = SSDPK(MN) +DP(MN,K)*CM(K,K) 
                LPSP(MN) = ( 2.*RPS(MN) - SSDPK(MN) )*DT
 3003 CONTINUE
C 
C     ------------------------------------------------------------------
C            ETAPE 4 :     ON MULTIPLIE RZ ET RQ PAR 2DT
C                          AINSI QUE RX.
C     ------------------------------------------------------------------
C 
      DO 4001 K=1,NK                                    
           DO 4001 MN=1,LA
                ZP(MN,K) = RZ(MN,K)*2.*DT 
 4001 CONTINUE
C 
      DO 4002 K=1,NM                                      
           DO 4002 MN=1,LA
                QP(MN,K) = RQ(MN,K)*2.*DT 
 4002 CONTINUE
C 
      IF(ITRAC.NE.0) THEN 
        DO 4003 N=1,NTRAC 
        DO 4003 K=1,NK
           DO 4003 MN=1,LA
                XP(MN,K,N)=RX(MN,K,N)*2.*DT 
 4003   CONTINUE
      ENDIF 

      RETURN
C-----------------------------------------------------------------------
      END 
