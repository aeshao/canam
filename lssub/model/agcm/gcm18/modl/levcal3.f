      SUBROUTINE LEVCAL3(SGBJ,SHTJ, SGJ,SHJ, DSGJ,DSHJ,
     1                   AG,BG,AH,BH, ACG,BCG,ACH,BCH, PSJ, 
     2                   NK,NK1,ILG,IL1,IL2,JLAT,PTOIT)

C     * JUN 10/03 - M.LAZARE. USE FULL SHTJ INSTEAD OF SPLITTING CALL
C     *                       TO ISOLATE MOON LAYER, SO THAT "SHTJ"
C     *                       CAN BE DECLARED AS 1-D IN COM13PA.    
C     * APR 29/03 - M.LAZARE. 1,LON -> IL1,IL2.
C     * DEC 15/92 - M.LAZARE. - PREVIOUS VERSION LEVCAL2.
C
C     * CALCUL LA POSITION DES BASES ET CENTRES DE COUCHES POUR 
C     * LA PHYSIQUE DE LA VERSION HYBRIDE A ELEMENTS FINIS CONSTANTS
C     * DU GCM, A VARIABLES INTERCALEES OU NON. 
C     * 
C     * PARAMETRES DE SORTIE: 
C     * 
C     * SGJ (I,K)    : MILIEU DES COUCHES DU VENT.
C     * SHJ (I,K)    : MILIEU DES COUCHES DE TEMPERATURE. 
C     * SGBJ(I,K)    : BASES DES COUCHES DU VENT. 
C     * SHTJ(I,K)    : BASES DES COUCHES DE TEMPERATURE.
C     * DSGJ(I,K)    : EPAISSEURS SIGMA DES COUCHES DU VENT.
C     * DSHJ(I,K)    : EPAISSEURS SIGMA DES COUCHES DE TEMPERATURE. 
C     * 
C     * PARAMETRES D'ENTREE:  
C     * 
C     * AG ,BG ,AH ,BH (K): INFO. SUR POSITION DES BASES DE COUCHES.
C     * ACG,BCG,ACH,BCH(K): INFO. SUR POSITION DES MILIEUX DE COUCHES.
C     * NK         : NOMBRE DE COUCHES. 
C     * NK1        : SIMPLEMENT  NK + 1.
C     * ILG        : DIMENSION EN LONGITUDE.
C     * LON        : NOMBRE DE LONGITUDES DISTINCTES. 
C     * ALFMOD     : POIDS AU SOMMET. 
C     * JLAT       : NUMERO DE CETTE LATITUDE (UTILISE POUR DEBUG)
C     * PTOIT      : PRESSURE WHERE LID OF MODEL APPLIED. 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
  
      REAL PSJ (   ILG) 
      REAL SGBJ(ILG,NK), SHTJ(ILG,NK1)
      REAL SGJ (ILG,NK), SHJ (ILG,NK) 
      REAL DSGJ(ILG,NK), DSHJ(ILG,NK) 
      REAL AG  (NK),BG  (NK),AH  (NK),BH  (NK)
      REAL ACG (NK),BCG (NK),ACH (NK),BCH (NK)
C-------------------------------------------------------------------- 
C     * DEFINITION DES MILIEUX ET BASES DE COUCHES.
C
*vdir novector  
      DO 100 K=1,NK 
      DO 100 I=IL1,IL2 
          SGJ (I  ,K)   = ACG(K)/PSJ(I)+BCG(K)
          SHJ (I  ,K)   = ACH(K)/PSJ(I)+BCH(K)
          SGBJ(I  ,K)   = AG (K)/PSJ(I)+BG (K) 
          SHTJ(I  ,K+1) = AH (K)/PSJ(I)+BH (K) 
  100 CONTINUE
  
C     * CALCUL DES EPAISSEURS DES COUCHES.
C     * STOIT IS THE LID OF THE MODEL. 

      DO 250 I=IL1,IL2
          STOIT     = PTOIT/PSJ(I)  
          DSGJ(I,1) = SGBJ(I,1) - STOIT 
          DSHJ(I,1) = SHTJ(I,2) - STOIT 
          SHTJ(I,1) = STOIT 
 250  CONTINUE  
  
      DO 300 K=2,NK 
      DO 300 I=IL1,IL2
          DSGJ(I,K) = SGBJ(I,K)  - SGBJ(I,K-1)
          DSHJ(I,K) = SHTJ(I,K+1)- SHTJ(I,K)
  300   CONTINUE
  
      RETURN
C-------------------------------------------------------------- 
      END
