      SUBROUTINE TRACD2 (TRAC,LA,ILEV,S,GRAV,KOUNT,IEPR,ITRAC,TRACOL) 
C 
C     * SEP 03/88 - M.LAZARE.  TRACOL IS NOW WORK ARRAY INSTEAD OF
C     *                        HARD-COATED DIMENSION. 
C     * MAR 03/85 - R.LAPRISE. ORIGINAL ROUTINE TRACDIA.
C 
C     * COMPUTE AND PRINT LAYER AND GLOBAL AVERAGES OF TRACER 
C     * WHEN ITRAC.NE.0.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TRAC(LA,ILEV) 
C 
      REAL TRACOL(ILEV),S(1)
C-------------------------------------------------------------------------
      IF(ITRAC.EQ.0 .OR. IEPR.EQ.0)RETURN 
      IF(MOD(KOUNT,IEPR).NE.0)     RETURN 
C 
      TRACTOT=0.
      DO 200 L=1,ILEV 
      TRACOL(L)=REAL(TRAC(1,L))/(SQRT(2.)*GRAV) 
      TRACTOT=TRACTOT+TRACOL(L)*(S(L+1)-S(L)) 
  200 CONTINUE
C 
      WRITE(6,6000)TRACTOT,(L,TRACOL(L),L=1,ILEV) 
      RETURN
C-------------------------------------------------------------------------
 6000 FORMAT('0TOTAL TRACER=',7X,E15.7,/,(I6,2X,E15.7))
      END 
