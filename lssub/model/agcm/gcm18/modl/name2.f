      SUBROUTINE NAME2(NAM,IC,INTEG)

C     * APR 17/15 - M.LAZARE/   NEW VERSION FOR GCM18+:
C     *             J.COLE/     - RENAME "INT" AS "INTEG"
C     *             F.MAJAESS.    TO AVOID CONFLICT WITH INTRINSIC
C     *                           OF SAME NAME. THIS IS A PURELY
C     *                           COSMETIC MODIFICATION SO THE SAME
C     *                           NAME IS KEPT.    
C     * AUG 25/06 - F.MAJAESS - NEW VERSION FOR GCM15F: 
C     *                         MAKE IT WORK IN 32-BITS MODE AS WELL.
C     * NOV 04/97 - M.HOLZER,R.HARVEY. PREVIOUS VERSION NAME.
C     *                                LIKE "NOM" EXCEPT WRITES OUT IN
C     *                                FORMAT "XM01". 
C     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.)
C
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER CNAM*4
      CHARACTER*1 IC(2)
C----------------------------------------------------------------------
      N2=INTEG/10
      N1=INTEG-N2*10
      IF(INTEG.GT.99) CALL                           XIT('NAME2',-1)
      IF(IC(2).EQ.' ') IC(2)='0'
      WRITE(CNAM,1000) IC,N2,N1
      NAM=NC4TO8(CNAM)

 1000 FORMAT(2A1,2I1)
      RETURN
      END
