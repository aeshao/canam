      SUBROUTINE TF2 (X,XM,F,ISIZ)
C 
C     * OCT 26/87 - R.LAPRISE.
C     * APPLY SECOND PART OF TIME FILTER TO SPECTRAL VARIABLE X(ISIZ).
C     * F  IS THE TIME FILTER COEFFICIENT.
C     * XM IS X'(K-1) ON INPUT AND X''(K-1) ON OUTPUT.
C     * X  IS X (K).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX X(ISIZ),XM(ISIZ)
C-----------------------------------------------------------------------
      DO 100 I=1,ISIZ 
         XM(I) = XM(I) + F*X(I) 
  100 CONTINUE
C 
      RETURN
      END 
