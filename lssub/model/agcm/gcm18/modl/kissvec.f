      subroutine kissvec(seed1,seed2,seed3,seed4,ran_arr,il1,il2,ilg)

! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
!  Overall period>2^123;
! This should be threadsafe

      IMPLICIT NONE

!
! INOUT
!

      REAL(8), DIMENSION (ilg), INTENT(INOUT)   :: ran_arr

      INTEGER(4), DIMENSION(ilg), INTENT(INOUT) :: seed1,seed2,seed3,
     +                                             seed4

!
! INPUT
!

      INTEGER, INTENT(IN) :: il1, il2, ilg

!
! LOCAL
!

      INTEGER    :: i, sz, kiss
      INTEGER(4) :: m, k, n

! inline function
      m(k, n) = ieor (k, ishft (k, n) )

      sz = SIZE(ran_arr)
      DO i = il1, il2
         seed1(i)   = 69069_4 * seed1(i) + 1327217885_4
         seed2(i)   = m (m (m (seed2(i), 13_4), - 17_4), 5_4)
         seed3(i)   = 18000_4 * iand (seed3(i), 65535_4)
     1              + ishft (seed3(i), - 16_4)
         seed4(i)   = 30903_4 * iand (seed4(i), 65535_4)
     1              + ishft (seed4(i), - 16_4)
         kiss       = seed1(i) + seed2(i) 
     1              + ishft (seed3(i), 16_4) + seed4(i)
         ran_arr(i) = kiss*2.328306e-10 + 0.5
      end do ! i

      end
