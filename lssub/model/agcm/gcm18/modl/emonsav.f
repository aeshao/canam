      SUBROUTINE EMONSAV(LSEMON,IDAY,NSECS,DELT)
C
C     * DEC 12/2018 - S.KHARIN. NEW ROUTINE TO CALCULATE LOGICAL
C     *                         SWITCH FOR END OF MONTH.
C
C     * LSEMON = LOGICAL SWITCH FOR END OF A MONTH.
C     * IDAY   = DAY OF THE YEAR.
C     * NSECS  = NUMBER OF SECONDS ELAPSED IN THE CURRENT DAY. 
C     * DELT   = MODEL TIMESTEP LENGTH (SECONDS). 
C
      IMPLICIT NONE

      LOGICAL LSEMON
      INTEGER IDAY,NSECS
      REAL DELT

      INTEGER MONTHEND(12),IDT,I
      DATA MONTHEND/31,59,90,120,151,181,212,243,273,304,334,365/
C-------------------------------------------------------------------- 
      LSEMON=.FALSE.
      IDT=NINT(DELT)
      DO I=1,12
        IF((IDAY-1)*86400+NSECS.EQ.MONTHEND(I)*86400-IDT)THEN
          LSEMON=.TRUE.
          EXIT
        ENDIF
      ENDDO
      RETURN
C-------------------------------------------------------------------- 
      END 
