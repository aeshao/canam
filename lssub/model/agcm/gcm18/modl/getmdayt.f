      SUBROUTINE GETMDAYT(MDAYT,MDAYT1,IDAY,MON,MON1)
C
C     * MAY 10/12 - M.LAZARE. NEW ROUTINE TO GET "MDAYT", 
C     *                       AND PREVIOUS VALUE "MDAYT1",
C     *                       (LATTER REQUIRED AT KOUNT=0
C     *                       ONLY). "MON" AND "MON1" ARE
C     *                       ALSO OUTPUT FOR USE IN
C     *                       GETWFH2,GETACH2 WHERE MONTH
C     *                       INDEX (1-12) IS USED INSTEAD
C     *                       IN IBUF(2).
C
C     * SETS "MDAYT", IE MID-MONTH DAY OF NEXT TARGET MONTH.
C     * ALSO, FOR USE AT KOUNT=0 ONLY, DETERMINE PREVIOUS
C     * MID-MONTH VALUE.
C     * "MON" AND "MON1" ARE ALSO OUTPUT FOR USE IN
C     * GETWFH2,GETACH2 WHERE MONTH INDEX (1-12) IS USED 
C     * INSTEAD IN IBUF(2).
C
      IMPLICIT NONE
C
      INTEGER*4 MYNODE
      INTEGER MMD(12)
      INTEGER L,M,N,IDAY,MDAYT,MDAYT1,MON,MON1
      LOGICAL OK
C
      COMMON /MPINFO/ MYNODE
C
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
C----------------------------------------------------------------------
C     * GET NEW MID-MONTH DAY.
C
      M=0 
      DO 5 N=1,12 
        IF(IDAY.GE.MMD(N)) M=N+1
    5 CONTINUE
      IF(M.EQ.0 .OR. M.EQ.13) M=1 
      MDAYT=MMD(M) 
      MON=M
      IF(MYNODE.EQ.0) WRITE(6,6020) MDAYT
C
C     * NOW, GET PREVIOUS MID-MONTH TARGET FOR INITIALIZATION AT KOUNT=0.
C
      L=M-1
      IF(L.EQ.0) L=12
      MDAYT1=MMD(L)
      MON1=L
C---------------------------------------------------------------------
 6020 FORMAT('0',3X,' MDAYT RESET TO',I6) 
      RETURN
      END
