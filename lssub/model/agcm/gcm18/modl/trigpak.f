      SUBROUTINE TRIGPAK(ILSLPAK,JLPAK,WJPAK,RADJPAK,DLONPAK,LATUPAK,
     +                   LATDPAK,WL,RADL,RADBU,RADBD,ILAT,LONSL,IJPAK)
C
C     * MAR 22/19 - J. COLE   ADD CODE TO COMPUTE BOUNDARIES.
C     * APR 10/03 - M.LAZARE. NEW VERSION WHICH CREATES FULLY 2-D
C     *                       TRIG FIELDS IN NEW CHAINED S-N PAIRS,
C     *                       FROM ZONAL VALUES.
C
C     * GETS PAK FIELDS OF SELECTED TRIG FIELDS FROM ZONAL VALUES.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * OUTPUT PAK FIELDS.
C
      REAL*8 WJPAK  (IJPAK), RADJPAK(IJPAK)
      REAL   DLONPAK(IJPAK), ILSLPAK(IJPAK), JLPAK  (IJPAK)
      REAL LATUPAK  (IJPAK),LATDPAK  (IJPAK)
C
C     * INPUT ZONAL ARRAYS.
C
      REAL*8 WL(ILAT), RADL(ILAT)
      REAL*8 RADBU  (ILAT),RADBD  (ILAT)
C
      INTEGER*4 MYNODE
C
      COMMON /SIZES/  ILG,NLAT,ILEV,LEVS,LRLMT,ICOORD,LAY,PTOIT,MOIST
      COMMON /MPINFO/ MYNODE
C---------------------------------------------------------------------
C     * MPI HOOK.
C
      JSTART=MYNODE*ILAT+1
      JEND  =MYNODE*ILAT+ILAT
C
C     * FIRST CALCULATE "TRUE" LATITUDE INDEX ARRAY.
C
      IF(MOD(NLAT,4).NE.0)          CALL XIT('TRIGPAK',-1)
      NLATH=NLAT/2
      NLATQ=NLAT/4
      NI=0
      J=0
      DO JQ=1,NLATQ
C
        JSP = JQ
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JSP)
          ENDDO
        ENDIF
C
        JNP = NLAT-JQ+1
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JNP)
          ENDDO
        ENDIF
C
        JSE = NLATH-JQ+1
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JSE)
          ENDDO
        ENDIF
C
        JNE = NLATH+JQ
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JNE)
          ENDDO
        ENDIF
      ENDDO
C
C     * NOW THE REMAINING ARRAYS.
C
      NI=0
      DO J=1,ILAT
         DO I=1,LONSL
            NI=NI+1
            RADJPAK(NI) = RADL (J)
            WJPAK  (NI) = WL   (J)
            ILSLPAK(NI) = REAL(I)
            DLONPAK(NI) = 360.*((ILSLPAK(NI)-1.)/REAL(LONSL))
            LATUPAK(NI) = RADBU(J)
            LATDPAK(NI) = RADBD(J)
         ENDDO
      ENDDO
C---------------------------------------------------------------------
      RETURN
      END
