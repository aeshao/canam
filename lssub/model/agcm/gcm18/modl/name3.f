      SUBROUTINE NAME3(NAM,IC,INTEG)

C     * APR 17/15 - M.LAZARE/   NEW VERSION FOR GCM18+:
C     *             J.COLE/     - RENAME "INT" AS "INTEG"
C     *             F.MAJAESS.    TO AVOID CONFLICT WITH INTRINSIC
C     *                           OF SAME NAME. THIS IS A PURELY
C     *                           COSMETIC MODIFICATION SO THE SAME
C     *                           NAME IS KEPT.    
C     * DEC 30/09 - K.VONSALZEN - NEW, BASED ON "NAME2", SIMILAR TO "NOM".
C
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER CNAM*4
      CHARACTER*1 IC(1)
C----------------------------------------------------------------------
      N3=INTEG/100
      NN=INTEG-N3*100
      N2=NN/10
      N1=NN-N2*10
      IF(INTEG.GT.999) CALL                          XIT('NAME3',-1)
      WRITE(CNAM,1000) IC,N3,N2,N1
      NAM=NC4TO8(CNAM)

 1000 FORMAT(1A1,3I1)
      RETURN
      END
