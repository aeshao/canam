      SUBROUTINE FILLNAF(WTR,TRANCHE,TRACNA,IOFF,
     1                   NTRAC,NTRACN,NTRSPEC,ITRADV,
     2                   ILEV,IJPAK,ILG,IL1,IL2)

C     * DEC 10/03 - M.LAZARE.   CONSISTENT WITH NEW METHODOLOGY FOR
C     *                         IBM PORT.
C     * OCT 29/02 - J.SCINOCCA. PREVIOUS VERSION FILNAF.

C     * FILLS "TRANCHE" WITH "TRACNA" FOR NON-ADVECTED
C     * TRACERS. OTHERWISE, "TRANCHE" IS FILLED WITH "WTR".
C     * "IOFF" IS OFFSET INTO TRACNA FOR THE PARTICULAR TASK.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL TRACNA (IJPAK,ILEV,NTRACN)
      REAL TRANCHE (ILG,ILEV,NTRAC),WTR(ILG,ILEV,NTRSPEC)
      INTEGER ITRADV(NTRAC)
C--------------------------------------------------------------------
      NA=0
      NNA=0
      DO 300 N=1,NTRAC
         IF(ITRADV(N).EQ.1) THEN
            NA=NA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  TRANCHE(I,L,N)=WTR(I,L,NA)
               ENDDO
            ENDDO
         ELSE
            NNA=NNA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  TRANCHE(I,L,N) = TRACNA(IOFF+I,L,NNA)
               ENDDO
            ENDDO
         ENDIF
 300     CONTINUE

      RETURN
      END
