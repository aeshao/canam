      SUBROUTINE UFILTRE(P,C,T,Q,PS,PM,CM,TM,QM,PSM,
     1                   FP,FC,FT,FQ,FPS, ILEV,LEVS,LA) 
C
C     * FEB 26/96 - M.LAZARE. BRACKET CALLS FOR MOISTURE SO NOT
C     *                       CALLED IF LEVS PASSED IN AS ZERO (TO
C     *                       SUPPORT SEMILAGRANGIAN CODE).  
C     * SEP 26/88 - M.LAZARE. CORRECT DIMENSIONING OF Q AND QM. 
C     * OCT 26/87 - R.LAPRISE.
C
C     * UNDO THE EFFECT OF TIME FILTER TF1, AS TO RESTORE X VARIABLES.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX P (LA,ILEV),C (LA,ILEV),T (LA,ILEV),Q (LA,LEVS),PS (LA) 
      COMPLEX PM(LA,ILEV),CM(LA,ILEV),TM(LA,ILEV),QM(LA,LEVS),PSM(LA) 
C-----------------------------------------------------------------------
      CALL UNDOTF1( P, PM, FP, LA*ILEV) 
      CALL UNDOTF1( C, CM, FC, LA*ILEV) 
      CALL UNDOTF1( T, TM, FT, LA*ILEV) 
      IF(LEVS.GT.0)                  THEN
         CALL UNDOTF1( Q, QM, FQ, LA*LEVS) 
      ENDIF
      CALL UNDOTF1(PS,PSM,FPS, LA)
C 
      RETURN
      END 
