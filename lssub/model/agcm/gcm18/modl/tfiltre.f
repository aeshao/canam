      SUBROUTINE TFILTRE(P,C,T,Q,PS, PM,CM,TM,QM,PSM, 
     1                   FP,FC,FT,FQ,FPS, ILEV,LEVS,LA, 
     2                   LTF2)
C
C     * FEB 26/96 - M.LAZARE. BRACKET CALLS FOR MOISTURE SO NOT
C     *                       CALLED IF LEVS PASSED IN AS ZERO (TO
C     *                       SUPPORT SEMILAGRANGIAN CODE). 
C     * OCT 26/87 - R.LAPRISE.
C
C     * APPLY A ROBERT TIME FILTER ON MODEL VARIABLES IN TWO STEPS AS...
C     * TF1: X' (K  ) = (1-2F) X (K  ) + F X''(K-1),
C     * TF2: X''(K-1) =        X'(K-1) + F X  (K  ).
C     * 
C     * TF2 SHOULD BE ACTIVATED 2 STEPS FOLLOWING A FORWARD TIMESTEP, 
C     * TF1 SHOULD BE ACTIVATED 1 STEP  AFTER     A FORWARD TIMESTEP. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL LTF2
      COMPLEX P (LA,ILEV),C (LA,ILEV),T (LA,ILEV),Q (LA,LEVS),PS (LA) 
      COMPLEX PM(LA,ILEV),CM(LA,ILEV),TM(LA,ILEV),QM(LA,LEVS),PSM(LA) 
C-----------------------------------------------------------------------
      IF(LTF2)THEN
         CALL TF2 (  P,  PM,  FP, LA*ILEV ) 
         CALL TF2 (  C,  CM,  FC, LA*ILEV ) 
         CALL TF2 (  T,  TM,  FT, LA*ILEV ) 
         IF(LEVS.GT.0)                  THEN
            CALL TF2 (  Q,  QM,  FQ, LA*LEVS ) 
         ENDIF
         CALL TF2 ( PS, PSM, FPS, LA      ) 
      ENDIF 
C 
      CALL TF1 (  P,  PM,  FP, LA*ILEV )
      CALL TF1 (  C,  CM,  FC, LA*ILEV )
      CALL TF1 (  T,  TM,  FT, LA*ILEV )
      IF(LEVS.GT.0)                  THEN
         CALL TF1 (  Q,  QM,  FQ, LA*LEVS )
      ENDIF
      CALL TF1 ( PS, PSM, FPS, LA      )
C 
      RETURN
      END 
