      function timer(date_time)
c
c     * timing function called by gcm driver.
c
c     * sep 06/2006 - m.lazare.
c
      implicit none

      real*8 timer
      integer date_time(8)
      real*8 dp001,done,dsim,dmih,dhid

      data dp001,done,dsim,dmih,dhid  /0.001, 1., 60., 3600., 86400./
C====================================================================
      timer = date_time(8)*dp001 + date_time(7)*done 
     &      + date_time(6)*dsim  + date_time(5)*dmih
     &      + date_time(4)*dhid
      return
      end
