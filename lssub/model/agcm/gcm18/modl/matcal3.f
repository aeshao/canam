      SUBROUTINE MATCAL3 (
     1                   OSIILA,DEL2LA,OSII,DEL2,AM,BM,CM,NK,NMAX,LM,LA,
     2                   AG,BG,AH,BH,LSR,MINDEX,PTOIT, 
     3                   CI,TRAV, 
     4                   DSG,DSH,DLNSG,A1SG,A2SG, 
     5                   A,TREF,PSREF,R,RSCP,DT,ALFMOD) 

C     * NOV 06/03 - M.LAZARE. PASS IN MINDEX AND USE TO DETERMINE
C     *                       CORRECT N-VALUE FOR LOCAL OSIILA AND DEL2LA. 
C     * FEB 02/94 - M.LAZARE. PREVIOUS VERSION MATCAL3.
C     * APR 10/89 - F.MAJAESS. PREVIOUS VERSION MATCALH.
C
C     * MODELE SPECTRAL,SEMI-IMPLICITE,ELEMENTS FINIS CONSTANT
C     * CALCUL DES MATRICES AM, BM, CM, 
C     * DE LA MATRICE  OSII ET DE DEL2
C     * UTILISATION FAITE PAR LINCAL ET NOUVAL
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL OSIILA(LA,NK,NK),DEL2LA(LA)
      REAL OSII(NMAX,NK,NK),DEL2(NMAX)
      REAL CI  (NK,NK),     TRAV(NK,NK) 
      REAL AM  (NK,NK),     BM  (NK,NK), CM   (NK,NK) 
      REAL AG  (NK),        BG  (NK),    AH   (NK),   BH(NK)
      REAL DSG (NK),        DSH (NK),    DLNSG(NK)
      REAL A1SG(NK),        A2SG(NK)

      INTEGER LSR(2,LM+1)
      INTEGER MINDEX(LM)
C-----------------------------------------------------------------------
      STOIT    =PTOIT/PSREF 
      SGB1     =AG(1)/PSREF+BG(1) 
      SHB1     =AH(1)/PSREF+BH(1) 
      DSG(1)   =SGB1-STOIT
      DSH(1)   =SHB1-STOIT
      DLNSG(1) =LOG(SHB1/STOIT)
      D1SG1    =SGB1-STOIT
      D2SG1    =0.
      A1SG(1)  =ALFMOD*D1SG1+SGB1*LOG(SHB1/SGB1)-STOIT*DLNSG(1)
      A2SG(1)  =0.
  
      DO 10 K=2,NK
          SGBK    =AG(K)  /PSREF+BG(K)
          SGBKM   =AG(K-1)/PSREF+BG(K-1)
          SHBK    =AH(K)  /PSREF+BH(K)
          SHBKM   =AH(K-1)/PSREF+BH(K-1)
          DSG(K)  =SGBK -SGBKM
          DSH(K)  =SHBK -SHBKM
          D1SGK   =SGBK -SHBKM
          D2SGK   =SHBKM-SGBKM
          DLNSG(K)=LOG(SHBK/SHBKM) 
          A1SG(K) =D1SGK +SGBK *LOG(SHBK /SGBK) -SGBKM*DLNSG(K)
          A2SG(K) =D2SGK -SGBKM*LOG(SHBKM/SGBKM) 
   10 CONTINUE
  
C     * PARTIE TRIANGULAIRE SUPERIEURE DE AM, BM, CM
  
      DO 11 K=1,NK-1
        KP=K+1
        DO 11 L=KP,NK 
          AM(L,K) =R*DLNSG(L) 
          BM(L,K) =0. 
          BM(KP,K)=RSCP*TREF*A2SG(KP)/DSH(K)
          CM(L,K) =DSG(L) 
   11 CONTINUE
  
C     * PARTIE TRIANGULAIRE INFERIEURE DE AM, BM, CM
  
      DO 12 K=2,NK
        KM=K-1
        DO 12 L=1,KM
          AM(L,K) =0. 
          AM(KM,K)=R*A2SG(K)/DSG(K) 
          BM(L,K) =RSCP*DLNSG(K)/DSH(K)*TREF*DSG(L) 
          CM(L,K) =DSG(L) 
   12 CONTINUE
  
C     * PARTIE DIAGONALE AM, BM, CM 
  
      DO 13 K=1,NK
        AM(K,K)=R*A1SG(K)/DSG(K)
        BM(K,K)=RSCP*TREF*A1SG(K)/DSH(K)
        CM(K,K)=DSG(K)
   13 CONTINUE
C 
C      DO 101 K=1,NK
C  101 PRINT*," AM(L,",K,")= ",(AM(L,K),L=1,NK) 
C      DO 102 K=1,NK
C  102 PRINT*," BM(L,",K,")= ",(BM(L,K),L=1,NK) 
C      DO 103 K=1,NK
C  103 PRINT*," CM(L,",K,")= ",(CM(L,K),L=1,NK) 
C 
      AM2=1./(A*A)
      DO 20 N=1,NMAX
C 
        DEL2(N)=-FLOAT(N*(N-1))*AM2 
  
C       LA MATRICE UNITAIRE 
  
        DO 14 K=1,NK
        DO 14 L=1,NK
   14   CI(L,K)=0.
        DO 15 K=1,NK
   15   CI(K,K)=1.
  
C     * A MATRICE DU SCHEME SEMI-IMPLICITE
  
      DO 16 K=1,NK
      DO 16 L=1,NK
      CI(L,K)=CI(L,K)-DT*DT*DEL2(N)*R*TREF*CM(L,K)
      DO 16 M=1,NK
      CI(L,K)=CI(L,K)-DT*DT*DEL2(N)*AM(M,K)*BM(L,M) 
   16 CONTINUE
C 
C      PRINT*," NOMBRE D'ONDE= ",N-1
C      DO 104 K=1,NK
C  104 PRINT*," CI(L,",K,")= ",(CI(L,K),L=1,NK) 
  
C     * LA MATRICE INVERSE DU SCHEME SEMI-IMPLICITE 
  
C     EPS = SMACH(2)
C     CALL MINV(CI,NK,NK,TRAV,DET,EPS,0,1)
      DET1=0.0
      CALL LINV3F(CI,TRAV,1,NK,NK,DET1,DET2,TRAV,IER) 
      IF(IER.EQ.130)THEN
        PRINT *,' MATRIX CI IS ALGORITHMICALLY SINGULAR ' 
        CALL XIT('MATCALH',-1)
      ENDIF 
      DET=DET1*(2**DET2)
      IF (DET.LT.1.E-15) PRINT*,' MATRIX CI, DETERMINANT = ',DET
C 
      DO 17 L=1,NK
      DO 17 K=1,NK
      OSII(N,L,K)=CI(L,K) 
   17 CONTINUE
C 
   20 CONTINUE
C
C     * FILL IN FULL-TRIANGLE ARRAY EQUIVILENT FOR FUTURE US IN LINCAL3/
C     * NOUVAL3.
C
      DO 50 M=1,LM
        MS=MINDEX(M)
        MNL=LSR(1,M)
        MNR=LSR(1,M+1)-1
        DO 40 MN=MNL,MNR
          NS = MS + (MN-MNL)
          DEL2LA(MN) = DEL2(NS)
          DO 30 L=1,NK
          DO 30 K=1,NK
            OSIILA(MN,L,K) = OSII(NS,L,K)
   30     CONTINUE
   40   CONTINUE 
   50 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END 
