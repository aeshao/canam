      SUBROUTINE NOUVALP
     1                 ( ZP, DP, TP, QP, XP, 
     2                   LA, NK, NM, DT, ITRAC, NTRAC,
     3                   RZ, RD, RT, RQ, RX) 
C 
C     * FEB 28/94 - M.LAZARE.
C 
C     * MODELE SPECTRAL,SEMI-IMPLICITE,ELEMENTS FINIS CONSTANTS NOUVAL
C     * SOLUTION DU SCHEME EXPLICITE POUR LES TERMES PHYSICS.
C     * ADAPTE DE ROUTINE NOUVAL2.
C 
C     ON CALCULE LES QUANTITES
C 
C S          ZP(MN,K)        : Z+, TOURBILLON A T+DT. 
C S          DP(MN,K)        : D+, DIVERGENCE A T+DT. 
C S          TP(MN,K)        : T+, TEMPERATURE A T+DT.
C S          QP(MN,KM)       : Q+, VARIABLE HUMIDITE A T+DT.
C S          XP(MN,K)        : X+, F(TRACEUR) A T+DT. 
C 
C    POUR CHAQUE COMPOSANTE SPECTRALE MN ET CHAQUE COUCHE K 
C 
C E          LA              : NOMBRE TOTAL DE COMPOSANTES SPECTRALES 
C E          NK              : NOMBRE DE COUCHES. 
C E          NM              : NOMBRE DE COUCHES D'HUMIDITIE. 
C 
C     A PARTIR DES DONNEES D'ENTREE 
C 
C E          RZ(MN,K)        : COTE DROIT, EQUATION DU TOURBILLON.
C E          RD(MN,K)        : COTE DROIT, EQUATION DE LA DIVERGENCE. 
C E          RT(MN,K)        : COTE DROIT, EQUATION THERMODYNAMIQUE.
C E          RQ(MN,KM)       : COTE DROIT, EQUATION D'HUMIDITE. 
C E          RX(MN,K)        : COTE DROIT, EQUATION DU TRACEUR. 
C
C E          DT              : LONGEUR DU PAS DE TEMPS EN SECONDE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX ZP (LA,NK),DP (LA,NK),TP (LA,NK),QP (LA,NM)
      COMPLEX RZ (LA,NK),RD (LA,NM),RT (LA,NK),RQ (LA,NM)
      COMPLEX XP(LA,NK,NTRAC), RX(LA,NK,NTRAC)
C-----------------------------------------------------------------------
C     * PERFORM AN EXPLICIT TIMESTEP.  
C     * (T-1) TENDENCIES HAVE ALREADY BEEN INCLUDED IN R.H.S.'S.
C     ------------------------------------------------------------------
C 
      DO 4001 K=1,NK                                                    
           DO 4001 MN=1,LA
C 
                ZP(MN,K) = RZ(MN,K)*2.*DT 
                DP(MN,K) = RD(MN,K)*2.*DT 
                TP(MN,K) = RT(MN,K)*2.*DT 
C 
 4001 CONTINUE
C 
      DO 4002 K=1,NM                                                    
           DO 4002 MN=1,LA
C 
                QP(MN,K) = RQ(MN,K)*2.*DT 
C 
 4002 CONTINUE
C 
C 
      IF(ITRAC.NE.0) THEN 
  
      DO 4003 N=1,NTRAC 
      DO 4003 K=1,NK
           DO 4003 MN=1,LA
C 
                XP(MN,K,N)=RX(MN,K,N)*2.*DT 
C 
 4003 CONTINUE
  
      ENDIF 
C 
      RETURN
C-----------------------------------------------------------------------
      END 
