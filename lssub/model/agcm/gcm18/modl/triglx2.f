      SUBROUTINE TRIGLX2(NLATH,ILAT,SR,WR,CR,RADR,WOSQ,
     1                   SRX,WRX,CRX,RADRX,WOSQX,PI) 
C
C     * JUN 12/06 - M. LAZARE. NEW VERSION FOR GCM15F:
C     *                        - PASSES IN PI AND REMOVES
C     *                          "PARAMS" COMMON BLOCK.
C     * JUN 30/03 - M. LAZARE. PREVIOUS VERSION TRIGLX:
C     *                        LIKE PREVIOUS VERSION TRIGL, EXCEPT
C     *                        ONLY GENERATES FIELDS FOR A GIVEN
C     *                        NODE, BASED ON "MPINFO".
C     * JUL 14/92 - E. CHAN. PREVIOUS VERSION TRIGL.
C
C     * THE ARGUMENT LIST IS THE SAME AS FOR GAUSSG.
C     * GAUSSG FILLS ONLY THE N HEM ORDERED N TO S. 
C     * THIS ROUTINE MAKES THE ARRAYS GLOBAL AND ORDERED FROM S TO N. 
C     *      SR=SIN(LAT),  CR=COS(LAT),  RADR=LATITUDE IN RADIANS.
C     *      WR = GAUSSIAN WEIGHTS,  WOSQ = WR/(SR**2). 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
      REAL*8 SR(1),WR(1),CR(1),RADR(1),WOSQ(1)
      REAL*8 SRX(1),WRX(1),CRX(1),RADRX(1),WOSQX(1)
C
      INTEGER*4 MYNODE 
C
      COMMON /MPINFO/ MYNODE
C-------------------------------------------------------------------- 
C     * FIRST MAKE S-N COMPLETE GRID.
C     * CRX,WRX,WOSQX ARE SYMMETRIC ABOUT THE EQUATOR. 
C     * SRX AND RADRX ARE ANTISYMMETRIC. 
C 
      PIH=3.14159265/2. 
      NLAT=NLATH*2
C
      DO 150 J=1,NLATH
        K=NLAT+1-J
        CRX(K)   = CRX(J) 
        WRX(K)   = WRX(J) 
        WOSQX(K) = WOSQX(J) 
        SRX(K)   = SRX(J) 
        SRX(J)   =-SRX(J)
        RADRX(K) = PIH-RADRX(J) 
        RADRX(J) =-RADRX(K)
  150 CONTINUE
C
C     * NOTE THAT ONLY THE VALUES RELEVANT TO THE NODE ARE OBTAINED!
C     * MPI HOOK.
C
      JSTART=MYNODE*ILAT+1
      JEND  =MYNODE*ILAT+ILAT
      NL=0       
C
      DO 200 J=1,NLAT
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN  
          NL       = NL+1
          CR(NL)   = CRX(J) 
          WR(NL)   = WRX(J) 
          WOSQ(NL) = WOSQX(J) 
          SR(NL)   = SRX(J) 
          RADR(NL) = RADRX(J) 
        ENDIF
  200 CONTINUE
C
      RETURN
      END 
