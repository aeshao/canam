      SUBROUTINE GETLDAY(LDAY,IDAY)
C
C     * JUL 29/07 - M.LAZARE. NEW ROUTINE TO GET "LDAY", BASED
C     *                       ON "GETZON9" WITHOUT THE OZONE.
C
C     * SETS "LDAY", IE FIRST DAY OFNEXT TARGET MONTH.
C
      IMPLICIT NONE
C
      INTEGER*4 MYNODE
      INTEGER NFDM(12)
      INTEGER L,N,IDAY,LDAY
      LOGICAL OK
C
      COMMON /MPINFO/ MYNODE
C
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/ 
C---------------------------------------------------------------------
C     * RESET LDAY TO FIRST DAY OF THE NEXT MONTH.
C 
      L=0 
      DO 150 N=1,12 
  150 IF(IDAY.GE.NFDM(N)) L=N+1 
      IF(L.EQ.13) L=1 
      LDAY=NFDM(L)
      IF(MYNODE.EQ.0) WRITE(6,6020) LDAY
C---------------------------------------------------------------------
 6020 FORMAT('0',3X,' LDAY RESET TO',I6) 
      RETURN
      END
