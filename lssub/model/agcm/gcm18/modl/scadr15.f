      SUBROUTINE SCADR15 (IR,IE,IXD,IXP,ID,
     1                    LEN_IR, LEN_IE, LEN_MAXD, LEN_MAXP,
     2                    NLATJD,ILGSLD,ILGD,ILEV,LEVS,LA,NTRSPEC,
     3                    NLATJP,ILGSLP,ILATP,ILGP,NTRAC,LENGTH)
C
C     * DEC 21/2007 - M.LAZARE. NEW VERSION FOR GCM15G WITH ONLY
C     *                         USED POINTERS KEPT (CLEANUP).
C     * FEB 04/2004 - M.LAZARE. PREVIOUS VERSION FOR GCM13D ONWARD (SCADR15)
C     *                         WHICH HAS THREE ADDITIONAL 2-D SLICE  
C     *                         WORKSPACE DEFINED FOR "DBAR", "UBAR"
C     *                         AND "VBAR" WORKFIELDS PASSED AND USED
C     *                         IN NEW DYNCAL4D.
C     * DEC 11/2003 - M.LAZARE. NEW VERSION FOR GCM13C (SCADR13):
C     *                         POINTERS REVISED FOR NEW TRANSFORM
C     *                         STRATEGY FOR IBM-CONVERTED MODEL:
C     *                         - UNUSED "IT" POINTER SET REMOVED.
C     *                         - ONLY POINTERS FOR "WRKS" LEFT FOR
C     *                           FORWARD/BACKWARD FOURIER TRANSFORMS
C     *                           ALLOCATED AS PART OF "SC" WITHIN
C     *                           CORE ROUTINES, SINCE LEGENDRE
C     *                           TRANSFORMS MOVED OUT TO SEPARATE
C     *                           PARALLEL LOOP OVER M-SPACE.
C     *                         - NADVSL,ILATD,IP,NTOTSL,LEV ALL
C     *                           REMOVED (NOT USED).
C     * DEC 20/2002 J.SCINOCCA. PREVIOUS VERSION (SCADR12) FOR GCM15.
C     * NOV  9/99 - J.SCINOCCA. PREVIOUS VERSION (SCADR11) FOR GCM13.
C     * NOV 23/98 - M.LAZARE. - PREVIOUS VERSION (SCADR10) FOR GCM12.
C     * FEB 29/96 - M.LAZARE. - PREVIOUS VERSION (SCADDR9) FOR GCM11.
C     * DEC 31/93 - M.LAZARE. - PREVIOUS VERSION (SCADDR8) FOR GCM9/GCM10.
C     * DEC 11/92 - M.LAZARE. - PREVIOUS VERSION (SCADDR7) FOR GCM8.
C 
C     * RETURNS THE STARTING ADDRESSES OF SCRATCH ARRAYS USED 
C     * TO VECTORIZE THE HYBRID MODEL. IT ALSO RETURNS LENGTHS (SIZE)
C     * USED FOR THE SCRATCH ARRAY AT DIFFERENT POINT IN THE CODE.
C 
      INTEGER IR (*), IE (*), ID (*) 
      INTEGER IXD(*), IXP(*)
C
      INTEGER  LEN_IR, LEN_IE, LEN_MAXD, LEN_MAXP
C-------------------------------------------------------------------- 
C     * ADDRESSES FOR RESTART WORK SPACE. 
C     * --------------------------------
C 
      IR(1 )= 1 
      IR(2 )=IR(1 ) + ILEV
      LAST  =IR(2 ) + ILEV
      LEN_IR=LAST
C
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR15',-1) 
      ENDIF 
C 
C     * ADDRESSES FOR ENERSV_ WORK SPACE. 
C     * --------------------------------
C 
      IE(1 )= 1 
      IE(2 )=IE(1 ) + ILEV
      IE(3 )=IE(2 ) + ILEV
      IE(4 )=IE(3 ) + (7+NTRAC) 
      LAST  =IE(4 ) + ILEV
      LEN_IE=LAST

      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR15',-2) 
      ENDIF 
C
C     * THE FOLLOWING ARE REQUIRED FOR MULTIPLE-LATITUDES METHODOLOGY.
C
      NLEV1E = 3*ILEV+LEVS+NTRSPEC*ILEV+1
      NLEV2E = 2*ILEV+1                   
      NLEV3E = 2*ILEV                     
      NLEVE  = NLEV1E+NLEV2E+1             
C
C     * SPACE INTERNAL TO THE DYNAMICS "CORE" ROUTINE.
C     * ---------------------------------------------

      NILHD=ILGSLD/2
C
C     * INITIALIZE THE MAX LENGTH FOR THE SC ARRAY IN THE DYNAMICS
C
      LEN_MAXD=0
C 
C     * ADDRESSES FOR NON FFT-RELATED WORK SPACE.
C 
      ID(1 )= 1 
      ID(2 )=ID(1 )+ ILGD*ILEV 
      ID(3 )=ID(2 )+ ILGD
      ID(4 )=ID(3 )+ ILGD
      ID(5 )=ID(4 )+ ILGD
      ID(6 )=ID(5 )+ ILGD*ILEV 
      ID(7 )=ID(6 )+ ILEV**2
      ID(8 )=ID(7 )+ ILEV**2
      ID(9 )=ID(8 )+ ILEV 
      ID(10)=ID(9 )+ ILEV 
      ID(11)=ID(10)+ ILEV 
      ID(12)=ID(11)+ ILEV 
      ID(13)=ID(12)+ ILEV 
      ID(14)=ID(13)+ 2*LA*ILEV
      ID(15)=ID(14)+ 2*LA*ILEV
      ID(16)=ID(15)+ 2*LA 
      ID(17)=ID(16)+ ILGD*ILEV
      ID(18)=ID(17)+ ILGD*ILEV
      ID(19)=ID(18)+ ILGD*ILEV
      LAST  =ID(19)+ ILGD*ILEV
      LEN_MAXD=MAX(LAST,LEN_MAXD)
 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR15',-3) 
      ENDIF 
C
C     * ADDRESSES FOR DYNAMICS FFT WORK SPACE (WRKS).
C
      IXD(1 )= 1 
      IXD(2) =IXD(1 ) + 2*NLEVE*NLATJD*NILHD
      LAST   =IXD(2 ) + 2*NLEVE*NLATJD*NILHD
      LEN_MAXD=MAX(LAST,LEN_MAXD)
 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR15',-4) 
      ENDIF 
C
C     * SPACE INTERNAL TO THE PHYSICS "CORE" ROUTINE.
C     * ---------------------------------------------
C
      NILHP=ILGSLP/2
C
C     * INITIALIZE THE MAX LENGTH FOR THE SC ARRAY IN THE PHYSICS.
C
      LEN_MAXP=0
C
C     * ADDRESSES FOR PHYSICS FFT WORK SPACE (WRKS).
C
      IXP(1 )= 1 
      IXP(2 )=IXP(1 ) + 2*NLEVE*NLATJP*NILHP
      LAST   =IXP(2 ) + 2*NLEVE*NLATJP*NILHP
      LEN_MAXP=MAX(LAST,LEN_MAXP)
 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR15',-5) 
      ENDIF 
C 
      RETURN
C-----------------------------------------------------------------------
 6000 FORMAT('0SIZE NEEDED=',I10,' DIMENSION=',I10) 
      END
