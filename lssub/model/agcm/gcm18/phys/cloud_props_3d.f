       SUBROUTINE CLOUD_PROPS_3D(RELIQ,REICE,CLDLIQ,                ! OUTPUT
     1                           CLDICE,LIQ_NC,
     2                           CLW_SUB, CIC_SUB, REL_SUB,         ! INPUT
     3                           REI_SUB, CDD,
     4                           CLDWATMIN,ILG,IL1,IL2,ILEV,NXLOC)

! June 02, 2013 - Jason Cole.
! This subroutine computes the 3D effective radius for liquid and ice as
! well as the particle number concentrations.

      IMPLICIT NONE

!
! INPUT DATA
!

      REAL, INTENT(IN), DIMENSION(ILG,ILEV,NXLOC) :: CLW_SUB,CIC_SUB, 
     1                                               REL_SUB, REI_SUB

      REAL, INTENT(IN), DIMENSION(ILG,ILEV)       :: CDD
      
      REAL, INTENT(IN)                            :: CLDWATMIN
      INTEGER, INTENT(IN)                         :: ILG,IL1,IL2,ILEV,
     1                                               NXLOC

!
! OUTPUT DATA
!

      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: RELIQ,REICE,
     1                                          CLDLIQ,CLDICE,
     2                                          LIQ_NC

!
! LOCAL DATA
!
      
      INTEGER :: IL,L,ICOL
      REAL    :: CWT,R_NXLOC

!
! UNITS
!

      R_NXLOC = 1.0/REAL(NXLOC)

! INITIALIZE FIELDS

      DO L = 1, ILEV
         DO IL = IL1, IL2
            RELIQ(IL,L)  = 0.0
            REICE(IL,L)  = 0.0
            CLDLIQ(IL,L) = 0.0
            CLDICE(IL,L) = 0.0
            LIQ_NC(IL,L) = 0.0
         END DO ! IL
      END DO ! L

! LOOP OVER THE SUBCOLUMN AND ASSIGN TO EACH GRIDBOX A PHASE OF
! LIQUID OR ICE BASED ON THE MASS OF WATER IN A PARTICULAR PHASE.
! ONCE PHASE IS DECIDED ACCUMULATE PHYSICAL PROPERTIES.

      DO ICOL = 1, NXLOC
         DO L = 1,ILEV
            DO IL = IL1, IL2
               CWT = CLW_SUB(IL,L,ICOL)+CIC_SUB(IL,L,ICOL)
               IF (CWT .GT. CLDWATMIN) THEN
                  IF ((CIC_SUB(IL,L,ICOL) .LE. 0.0) .OR. 
     1                (CLW_SUB(IL,L,ICOL) .GE. CIC_SUB(IL,L,ICOL))) THEN ! LIQUID
                     RELIQ(IL,L)  = RELIQ(IL,L) + REL_SUB(IL,L,ICOL)
                     LIQ_NC(IL,L) = LIQ_NC(IL,L) + CDD(IL,L)
                     CLDLIQ(IL,L) = CLDLIQ(IL,L) + 1.0
                  ELSEIF ((CLW_SUB(IL,L,ICOL) .LE. 0.0) .OR. 
     1                (CIC_SUB(IL,L,ICOL) .GT. CLW_SUB(IL,L,ICOL))) THEN ! ICE
                     REICE(IL,L)  = REICE(IL,L) + REI_SUB(IL,L,ICOL)
                     CLDICE(IL,L) = CLDICE(IL,L) + 1.0
                  END IF
               END IF	       
            END DO ! IL
         END DO ! L
      END DO ! ICOL
                     
! COMPUTE THE APPROPRIATE MEANS (RADIUS IS CLOUD-MEAN, CLOUD-FRACTION IS GRID-MEAN)

      DO L = 1, ILEV
         DO IL = IL1, IL2
            IF (CLDLIQ(IL,L) .GT. 0.0) THEN
               RELIQ(IL,L)  = RELIQ(IL,L)/CLDLIQ(IL,L)
               LIQ_NC(IL,L) = LIQ_NC(IL,L)/CLDLIQ(IL,L)
               CLDLIQ(IL,L) = CLDLIQ(IL,L)*R_NXLOC
            ELSE
               RELIQ(IL,L)  = 0.0
               LIQ_NC(IL,L) = 0.0
               CLDLIQ(IL,L) = 0.0
            END IF
            IF (CLDICE(IL,L) .GT. 0.0) THEN
               REICE(IL,L)  = REICE(IL,L)/CLDICE(IL,L)
               CLDICE(IL,L) = CLDICE(IL,L)*R_NXLOC
            ELSE
               REICE(IL,L)  = 0.0
               CLDICE(IL,L) = 0.0
            END IF
         END DO ! IL
      END DO !L

      RETURN
      END
