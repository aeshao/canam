      SUBROUTINE SIGXK2X(SHTXKJ,SHXKJ, SHJ,SHTJ, NK,NK1,ILG,IL1,IL2,
     1                   RGOCP)
C
C     * JUN 10/03 - MLAZARE.  CHANGE 1,LON TO IL1,IL2 (PASSED IN).  
C     * JUL 15/88 - M.LAZARE. PREVIOUS VERSION SIGXK2.
C
C     * DEFINE SIGMA**(RGAS/CPRES) AT LAYERS CENTRES AND TOPS.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
  
      REAL   SHTXKJ (ILG,NK ), SHXKJ (ILG,NK )
      REAL   SHTJ   (ILG,NK1), SHJ   (ILG,NK )
C-----------------------------------------------------------------------
      DO 500  K=1,NK
         DO 100 I=IL1,IL2
            SHXKJ (I,K)=SHJ (I,K)**RGOCP
            SHTXKJ(I,K)=SHTJ(I,K)**RGOCP
  100    CONTINUE 
  500 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 
