      SUBROUTINE CLD_GEN_DRIVER3(NCLDY, CLW_SUB,CIC_SUB,                ! OUTPUT
     1                           RLC_CF, RLC_CW, SIGMA_QCW, IPPH,       ! INPUT
     2                           IOVERLAP, CLD, SHJ, SHTJ, CLW, CIC, T,                                  
     3                           PS, ISEEDROW,
     4                           IL1, IL2, ILG, LAY, LEV, NX_LOC, MTOP )

C      * APR 30/2012 - JASON COLE.   NEW VERSION FOR GCM16:
C      *                             REMOVE UNUSED INTERNAL ARRAYS
C      *                             {X,X1,X2,Y,Y1,Y2}, INCLUDING CALL
C      *                             IN NEW CLD_GENERATOR3.
C      * DEC 12/2007 - JASON COLE.   PREVIOUS VERSION CLD_GEN_DRIVER2 FOR 
C      *                             GCM15G/H/I:
C      *                             - HANDLE 3 DIFFERENT OVERLAP
C      *                               METHODS (DEFINED USING IOVERLAP).
C      *                             - REMOVE CODE RELATED TO SETTING
C      *                               LPPH AND IMAXRAN SINCE NOW USING
C      *                               IPPH AND IOVERLAP.
C      *                             - CALLS NEW VERSION CLD_GENERATOR2.
C      * JAN 09/2007 - JASON COLE.   PREVIOUS VERSION CLD_GEN_DRIVER FOR GCM15F.
! --------------------------------------------------------------------
! Driver to call stochastic cloud generator
! --------------------------------------------------------------------   
! JNSC GCM15D NOW CARRIES EXTRA FIELDS FOR THE SHALLOW CONVECTION CLOUD AMOUNT,
! JNSC CLOUD WATER CONTENT AND CLOUD DROPLET NUMBER CONCENTRATION.  RIGHT NOW
! JNSC I AM JUST USING THE DEFACTO METHOD FROM GCM15D.  THIS METHOD CAN LIKELY 
! JNSC BE IMPROVED USING MCICA AND SAMPLING

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

! Note: LAY    => Number of layers
! Note: ILG    => Number of GCM columns
! Note: NX_LOC => Number of subcolumns to generate

!
! PARAMETER
!

      REAL, PARAMETER :: 
     1 M2KM = 1.0/1000.0, ! Convert meters to kilometers
     2 CUT  = 0.001

!     
! INPUT DATA
!

      REAL, INTENT(IN) ::
     1 SHJ(ILG, LAY), 
     2 T(ILG,LAY),
     3 CLD(ILG,LAY),
     4 PS(ILG),
     5 RLC_CF(ILG,LAY),
     6 RLC_CW(ILG,LAY),
     7 SIGMA_QCW(ILG,LAY),
     8 CLW(ILG,LAY),
     9 CIC(ILG,LAY),
     A SHTJ(ILG,LEV)

      INTEGER, INTENT(IN) :: ! Counters and array sizes
     1 ILG,
     2 IL1,
     3 IL2,
     4 LAY,
     5 LEV,
     6 NX_LOC,
     7 MTOP

       INTEGER, INTENT(IN) ::
     1 IOVERLAP,            ! Overlap flag
     2 IPPH                 ! Plane-parallel homogeneous flag

!
! OUTPUT DATA
!

      REAL, INTENT(OUT) ::  
     1 CLW_SUB(ILG,LAY,NX_LOC),   ! Subcolumn of cloud liquid water contents       (g/m^3)
     2 CIC_SUB(ILG,LAY,NX_LOC)    ! Subcolumn of cloud ice water contents          (g/m^3)   

      INTEGER,INTENT(OUT) ::
     1 NCLDY(ILG)                 ! Number of cloudy subcolumns

!
! WORK ARRAYS
!

      REAL ::
     1 QI_PROF(ILG,LAY),
     2 QC_PROF(ILG,LAY),
     3 ZM(ILG,LAY)

      REAL ::
     1 ALPHA(ILG,LAY), ! Fraction of maximum/random cloud overlap 
     2 RCORR(ILG,LAY)  ! Fraction of maximum/random cloud condensate overlap

      INTEGER ::
     1 I_LOC(ILG) ! Place the new subcolumns into the arrays starting from the front

      LOGICAL ::
     1 L_CLD(ILG)

      INTEGER(4) :: ISEEDROW(ILG,4) ! Seed for kissvec RNG

!
! LOCAL DATA (SCALAR)
!

      INTEGER ::
     1 IL,       ! Counter over GCM columns
     2 II,       ! Counter over NX subcolumns
     3 KK        ! Counter over lay vertical layers

      REAL ::
     1 RHO,      ! Density of air                               (g/m^3)           
     2 P,        ! GCM column pressure at layer midpoint        (Pa)
     3 ROG,      ! Total gas constant/gravity
     4 DMULT

      COMMON /PARAMS / WW, TWW, RAYON, ASQ, GRAV, RGAS, RGOCP, 
     1                 RGOASQ, CPRES, RGASV, CPRESV
      COMMON /EPS    / A, B, EPS1, EPS2 
      
! Zero out fields
      DO IL = IL1,IL2
         NCLDY(IL)  = 0
      END DO ! IL

      DO KK = 1, LAY 
         DO IL = IL1,IL2
            QC_PROF(IL,KK) = 0.0
            QI_PROF(IL,KK) = 0.0
            ALPHA(IL,KK)   = 0.0
            RCORR(IL,KK)   = 0.0
         END DO ! IL
      END DO ! KK

      DO II = 1, NX_LOC
         DO KK = 1 , LAY
            DO IL = IL1, IL2
               CIC_SUB(IL,KK,II) = 0.0
               CLW_SUB(IL,KK,II) = 0.0
            END DO
         END DO
      END DO

! Compute the heights of mid-layers
! Needed if decorrelation lengths are used
      ROG=RGAS/GRAV    

      DO IL = IL1, IL2
         P          = SHJ(IL,LAY)*PS(IL) ! Since using ratios
         ZM(IL,LAY) = ROG*T(IL,LAY)*LOG(PS(IL)/P)*M2KM
      END DO ! IL

      DO KK = LAY-1, 1, -1
         DO IL = IL1, IL2
            P1 = SHJ(IL,KK)*PS(IL)
            P2 = SHJ(IL,KK+1)*PS(IL)
            ZM(IL,KK) = ZM(IL,KK+1) + ROG*T(IL,KK)*LOG(P2/P1)*M2KM
         END DO ! IL
      END DO ! KK

! Compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
! convection

! Convert the cloud condensate from kg/kg to g/m^3 since these are the units used to generate
! cloud optical properties
!
! Compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
! convection
      DO KK = 1, LAY
         DO IL = IL1, IL2
            P = SHJ(IL,KK)*PS(IL)
            IF (CLD(IL,KK) .GE. CUT) THEN
! RHO in g/m^3                  
               RHO            = 1000.0*P/(RGAS*T(IL,KK))
               DMULT          = RHO /CLD(IL,KK)
               QI_PROF(IL,KK) = CIC(IL,KK)*DMULT
               QC_PROF(IL,KK) = CLW(IL,KK)*DMULT
            END IF
         END DO ! IL
      END DO ! KK

! Call cloud generator
      CALL CLD_GENERATOR3(CIC_SUB, CLW_SUB, NCLDY,
     1                    ZM, CLD, QC_PROF, QI_PROF,
     2                    RLC_CF, RLC_CW, SIGMA_QCW,
     3                    ILG, IL1, IL2, LAY, NX_LOC,
     4                    ALPHA, RCORR, ISEEDROW,
     5                    I_LOC, L_CLD, IPPH, IOVERLAP)

      RETURN
      END
