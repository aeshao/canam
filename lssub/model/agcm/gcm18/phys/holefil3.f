      SUBROUTINE HOLEFIL3(QROW,QADDROL,QMIN,ILG,LONSL,IL1,IL2)
C
C     * JUN 27/2006 - M.LAZARE.  NEW VERSION FOR GCM15F:
C     *                          - QADDROL AND QMIN ARE NATIVE REAL.
C     *                          - USE VARIABLE INSTEAD OF CONSTANT
C     *                            IN INTRINSICS SUCH AS "MAX", SO
C     *                            THAT CAN COMPILE IN 32-BIT MODE
C     *                            WITH REAL*8.  
C     * NOV 01/04 - M.LAZARE.    PREVIOUS VERSION HOLEFIL2.
C
C     * SUBROUTINE TO FILL HOLES FOR A GIVEN LEVEL, ALONG A
C     * SINGLE LONGITUDE PASS (CHAINED LATITUDES ARE TAKEN INTO
C     * ACCOUNT).
C
C     * IF THERE IS SUFFICIENT MOISTURE TO BORROW FROM OTHER POINTS
C     * IN THE LONGITUDE PASS, THIS IS DONE TO MINIMIZE GLOBAL
C     * CORRECTION SUBSEQUENTLY DONE IN SPECTRAL SPACE.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C  
      REAL   QROW(ILG)
      REAL   QADDROL(ILG)
      REAL   QMIN
C
      DATA ZERO /0./
C----------------------------------------------------------------
C     * CALCULATE NUMBER OF CHAINED LATITUDES.
C
      NLATJ = IL2/LONSL
C
C     * HOLEFILLING CORRECTION FOR EACH LATITUDE.
C
      DO J=1,NLATJ 
        SUMQEX=0.0
        SUMQDEF=0.0
C
        DO K=1,LONSL
          I       = (J-1)*LONSL+K
          QD1     = QROW(I)-QMIN
          QD2     = QMIN-QROW(I)
          QEXC    = MAX(QD1,ZERO)
          QDEF    = MAX(QD2,ZERO)
          SUMQEX  = SUMQEX  + QEXC
          SUMQDEF = SUMQDEF + QDEF
        ENDDO
C
        IF(SUMQEX.EQ.0.)                                   THEN
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QROW(I)    = QMIN
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ELSE IF(SUMQEX.GT.SUMQDEF)                         THEN
          RATIO=MAX(SUMQEX-SUMQDEF,ZERO)/SUMQEX
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QDIF       = QROW(I)-QMIN
            QROW(I)    = QMIN+RATIO*MAX(QDIF,ZERO)
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ELSE IF(SUMQEX.LE.SUMQDEF)                         THEN
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QNEW       = QMIN
            QROW(I)    = MAX(QROW(I),QNEW)
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ENDIF
      ENDDO
C
      RETURN
      END
