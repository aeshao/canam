      SUBROUTINE CLD_PARTICLE_SIZE_CLD_GEN2(REL_SUB, REI_SUB,      ! OUTPUT
     1                                      CDD, RADEQVW, RADEQVI, ! INPUT
     3                                      CLW_SUB, CIC_SUB,
     4                                      IL1, IL2, ILG, LAY, LEV, 
     5                                      NX_LOC, IPPH_RE)

!      * FEB 12/2009 - JASON COLE.  NEW VERSION FOR GCM15H:
!      *                            - HARD-CODED VALUE OF 75.46 FOR
!      *                              LIQUID EQUIVILENT RADIUS IS
!      *                              NOW PROPERLY DECOMPOSED INTO
!      *                              PIFAC=62.035 AND THE "TUNABLE"
!      *                              FACTOR BETA (INCREASED TO 1.3
!      *                              TO GIVE BETTER AGREEMENT WITH
!      *                              OBSERVATIONS OF LIQUID EQUIVILENT
!      *                              RADIUS). 
!      * DEC 12/2007 - JASON COLE.  PREVIOUS VERSION 
!      *                            CLD_PARTICLE_SIZE_CLD_GEN FOR GCM15G:
!      *                            - INITIAL VERSION OF CLOUD GENERATOR
C      *                              FOR MCICA.

      IMPLICIT NONE

!
! INPUT DATA
!

      REAL, INTENT(IN) ::
     1 CLW_SUB(ILG,LAY,NX_LOC), ! Subgrid cloud liquid water 
     2 CIC_SUB(ILG,LAY,NX_LOC), ! Subgrid cloud ice water
     3 CDD(ILG,LAY),            ! Cloud liquid water droplet number concentration
     4 RADEQVW(ILG,LAY),        ! Liquid cloud particle size
     5 RADEQVI(ILG,LAY)         ! Ice cloud particle size

      INTEGER, INTENT(IN) ::
     1 IL1,
     2 IL2,
     3 ILG,
     4 LAY,
     5 LEV,
     6 NX_LOC

      INTEGER, INTENT(IN) ::
     1 IPPH_RE

!
! OUTPUT DATA
!

      REAL, INTENT(OUT) ::
     1 REL_SUB(ILG,LAY,NX_LOC), 
     2 REI_SUB(ILG,LAY,NX_LOC)


!
! LOCAL DATA
!

      REAL :: 
     1 WCDW,
     2 WCDI,
     3 REI_LOC,
     4 REL_LOC,
     5 CIC_LOC,
     6 PCDNC,
     7 ROG

      INTEGER ::
     1 IL,
     2 ILAY,
     3 ICOL

!
! PARAMETERS
!

      REAL, PARAMETER :: THIRD    = 0.3333333333333

! THE FOLLOWING PARAMETERS ARE DEFINED IN CLOUDS16. MAKE SURE THESE ARE
! CONSISTENT!

      REAL, PARAMETER :: CICMIN   = 0.00001  
      REAL, PARAMETER :: CLWMIN   = 0.00001         
      REAL, PARAMETER :: RIEFFMAX = 50.0
      REAL, PARAMETER :: RWEFFMIN = 2.0
      REAL, PARAMETER :: RWEFFMAX = 30.0 
      REAL, PARAMETER :: PIFAC   = 62.035
      REAL, PARAMETER :: BETA    = 1.3   
!-------------------------------------------------------------------------
      IF (IPPH_RE .EQ. 0) THEN  ! HORIZONTALLY HOMOGENEOUS CLOUD PARTICLE EFFECTIVE SIZE

! ASSIGN THE HORIZONTALLY CONSTANT PARTICLE SIZES TO EACH CLOUDY CELL
! IN THE GENERATED SUBCOLUMNS

         DO ICOL = 1, NX_LOC
            DO ILAY = 1, LAY
               DO IL = IL1, IL2
                  REL_SUB(IL,ILAY,ICOL) = RADEQVW(IL,ILAY)
                  REI_SUB(IL,ILAY,ICOL) = RADEQVI(IL,ILAY)
               END DO ! IL
            END DO ! ILAY
         END DO ! ICOL

      ELSEIF (IPPH_RE .EQ. 1) THEN !HORIZONTALLY INHOMOGENEOUS CLOUD PARTICLE SIZES

         DO ICOL = 1, NX_LOC
            DO ILAY = 1, LAY
               DO IL = IL1, IL2
                     REL_LOC = BETA*PIFAC
     1                    * (CLW_SUB(IL,ILAY,ICOL)/CDD(IL,ILAY))**THIRD
                     REL_SUB(IL,ILAY,ICOL) = 
     1                             MAX(MIN(REL_LOC,RWEFFMAX),RWEFFMIN)
                     CIC_LOC = MAX(CIC_SUB(IL,ILAY,ICOL),CICMIN)
                     REI_LOC = 83.8*CIC_LOC**0.216
                     REI_SUB(IL,ILAY,ICOL) = MIN(REI_LOC,RIEFFMAX)
               END DO ! IL
            END DO ! ILAY
         END DO ! ICOL
      END IF
      
      RETURN
      END 
