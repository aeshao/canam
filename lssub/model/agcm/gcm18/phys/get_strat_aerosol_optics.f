      subroutine GET_STRAT_AEROSOL_OPTICS(sw_ext_sa_pak, ! OUTPUT
     &                                    sw_ext_sa_pal,
     &                                    sw_ssa_sa_pak,
     &                                    sw_ssa_sa_pal,
     &                                    sw_g_sa_pak,
     &                                    sw_g_sa_pal,
     &                                    lw_ext_sa_pak,
     &                                    lw_ext_sa_pal,
     &                                    lw_ssa_sa_pak,
     &                                    lw_ssa_sa_pal,
     &                                    lw_g_sa_pak,
     &                                    lw_g_sa_pal,
     &                                    w055_ext_sa_pak,
     &                                    w055_ext_sa_pal,
     &                                    w110_ext_sa_pak,
     &                                    w110_ext_sa_pal,
     &                                    pressure_sa_pak,
     &                                    pressure_sa_pal,
     &                                    incd,           ! INPUT
     &                                    kount,
     &                                    irefyr,
     &                                    irefmn,
     &                                    ioffyr,
     &                                    iday,
     &                                    ilat_in,
     &                                    nuan,
     &                                    ijpak,
     &                                    nlat,
     &                                    nlon,
     &                                    levsa,
     &                                    nbs,
     &                                    nbl)
C
C     * Dec 20, 2016 - J Cole      New routine to read in volcanic aerosol
C                                  dataset from ETHZ which are height and 
C                                  latitude resolved optical properties.

      IMPLICIT NONE

! PARAMETERS
      INTEGER, PARAMETER ::
     & MON_PER_YR = 12,
     & DAY_PER_YR = 365,
     & N_OPT_VAR = 3,
     & N_EXT_DIAG = 3

      !--- A list containing mid month days of the year
      INTEGER, PARAMETER, DIMENSION(MON_PER_YR) ::
     &  mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

      !--- A list containing mid month days for each month
      INTEGER, PARAMETER, DIMENSION(MON_PER_YR) ::
     &  mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)

      CHARACTER(LEN=3), PARAMETER ::
     & sw_opt_vars(N_OPT_VAR) = (/"SWE","SWS","SWG"/),
     & lw_opt_vars(N_OPT_VAR) = (/"LWE","LWS","LWG"/)

      CHARACTER(LEN=4), PARAMETER ::
     & diag_opt_vars(N_EXT_DIAG) = (/"E055","E110","PRES"/)


! OUTPUT DATA

      REAL, INTENT(OUT) :: 
     & sw_ext_sa_pak(ijpak,levsa,nbs),
     & sw_ext_sa_pal(ijpak,levsa,nbs),
     & sw_ssa_sa_pak(ijpak,levsa,nbs),
     & sw_ssa_sa_pal(ijpak,levsa,nbs),
     & sw_g_sa_pak(ijpak,levsa,nbs),
     & sw_g_sa_pal(ijpak,levsa,nbs),
     & lw_ext_sa_pak(ijpak,levsa,nbl),
     & lw_ext_sa_pal(ijpak,levsa,nbl),
     & lw_ssa_sa_pak(ijpak,levsa,nbl),
     & lw_ssa_sa_pal(ijpak,levsa,nbl),
     & lw_g_sa_pak(ijpak,levsa,nbl),
     & lw_g_sa_pal(ijpak,levsa,nbl),
     & w055_ext_sa_pak(ijpak,levsa),
     & w055_ext_sa_pal(ijpak,levsa),
     & w110_ext_sa_pak(ijpak,levsa),
     & w110_ext_sa_pal(ijpak,levsa),
     & pressure_sa_pak(ijpak,levsa),
     & pressure_sa_pal(ijpak,levsa)

! INPUT DATA

      INTEGER, INTENT(IN) :: 
     & incd,
     & kount,
     & irefyr,
     & irefmn,
     & ioffyr,
     & iday,
     & ilat_in,
     & nuan, 
     & ijpak,
     & nlat,
     & nlon,
     & levsa,
     & nbs,
     & nbl

! LOCAL DATA

      INTEGER :: 
     & year_prev, 
     & year_next, 
     & mon_prev, 
     & mon_next,
     & ib2p, 
     & ib2n, 
     & curr_year,
     & i,
     & iz,
     & ib,
     & ilat,
     & iyear,
     & ivar,
     & imdh,
     & rssti,
     & isavdts,
     & ijpak_loc

      INTEGER*4 ::
     & mynode

      REAL ::
     & day1,
     & day2,
     & day3,
     & w1,
     & w2,
     & e,
     & e1,
     & e2,
     & s,
     & s1,
     & s2,
     & g,
     & g1,
     & g2

      REAL ::
     & idata_pak(ijpak,levsa),
     & idata_pal(ijpak,levsa)

      CHARACTER(LEN=4) :: 
     & name

! COMMON DATA
      !--- mynode used to control i/o to stdout
      COMMON /mpinfo/ mynode 

      !--- keeptime is required for iyear
      COMMON /keeptim/ iyear,imdh,rssti,isavdts

C----------------------------------------------------------------------
C
C This program reads in the stratospheric/volcanic aerosol datasets to be used 
C by the GCM.
C
C It basically reads in datasets provided by ETHZ for CMIP6, which are 
C height resolved optical properties for the radiative transfer wavelength 
C intervals.
C
C The data can be read in three different ways,
C
C 1. Fully transient mode - time varying data is interpolated for each month
C                           in a time-series.
C
C 2. Repeated annual cycle - the data is time varying but repeats a particular
C                            year from a time-series.
C
C 3. Single month - data from a single month in the time-series is used.  This
C                   seems odd but has been requested for some studies (GEOMIP).
C
C----------------------------------------------------------------------

      if (mynode.eq.0) then
        write(6,*)'GET_STRAT_AEROSOL_OPTICS: NEW STRATOSPHERIC ',
     &            'AEROSOL REQUIRED'
      endif

C
C Figure out the year and month to use for the interpolation in time.
C

C.....Determine the year for which data is required

      if (irefyr .gt. 0) then
        !--- When irefyr > 0 read emissions from the data file for
        !--- a repeated annual cycle of year irefyr
        curr_year=irefyr
      else
        !--- Otherwise read time dependent emissions from the data
        !--- file using iyear to determine the current year
        curr_year=iyear+ioffyr
      endif

C.....Check on iday

      if (iday .lt. 1 .or. iday .gt. DAY_PER_YR) then
         if (mynode.eq.0) then
            write(6,*)'GET_STRAT_AEROSOL_OPTICS: IDAY is out of range.',
     &                'IDAY=',iday
        endif
        call xit("GET_STRAT_AEROSOL_OPTICS",-1)
      endif

C.....Determine previous and next year/month, relative to iday

      mon_prev=0
      if (irefmn .lt. 0) then
         !--- The month is transient.
         if (iday.ge.1 .and. iday.lt.mm_doy(1)) then
            year_prev=curr_year-1
            year_next=curr_year
            mon_prev=12
            mon_next=1
         else if (iday.ge.mm_doy(MON_PER_YR) 
     &            .and. iday.le.DAY_PER_YR) then
            year_prev=curr_year
            year_next=curr_year+1
            mon_prev=MON_PER_YR
            mon_next=1
         else
            do i=2,MON_PER_YR
               if (iday .ge. mm_doy(i-1) .and. 
     &             iday .lt. mm_doy(i)) then
                  year_prev=curr_year
                  year_next=curr_year
                  mon_prev=i-1
                  mon_next=i
                  exit
               endif
            enddo
         endif
      else
         !--- The month is not changing.
         mon_prev = irefmn
         mon_next = irefmn
      end if

      if (mon_prev.eq.0) then
         if (mynode.eq.0) then
            write(6,*)'GET_STRAT_AEROSOL_OPTICS: Problem setting year',
     &                '/month on IDAY=',iday
        endif
        call xit("GET_STRAT_AEROSOL_OPTICS",-2)
      endif

C.... Determine time stamps (ibuf2 values) to be used to read
C.... appropriate data from the file

      ib2p=year_prev*100+mon_prev
      ib2n=year_next*100+mon_next

      if (mynode.eq.0) then
        write(6,*)'GET_STRAT_AEROSOL_OPTICS: year_prev,mon_prev,',
     &            'year_next,mon_next: ',
     &             year_prev,mon_prev,year_next,mon_next
        write(6,*)'GET_STRAT_AEROSOL_OPTICS: ib2p,ib2n: ',ib2p,ib2n
      endif

C
C     * Get new boundary fields for next mid-month day. 
C
      IF(incd .EQ. 0) THEN
C 
C       * Model is stationary.
C
        IF(kount .EQ. 0) THEN 
C         
C         * Start-up time. read-in fields for average of month. 
C         * initialize target fields as well, although not used.
C
C         * Data for each band is saved in a separate variable but
C         * are read into a single variable here.

           DO ib = 1, nbs
              DO ivar = 1, N_OPT_VAR
                 WRITE(name,101) sw_opt_vars(ivar),ib
                 CALL GET_ZONAL(idata_pak,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)              

                 SELECT CASE (ivar)
                    CASE (1)
                       sw_ext_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       sw_ssa_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       sw_g_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-3)
                 END SELECT
              END DO ! ivar
           END DO ! ib

           DO ib = 1, nbl
              DO ivar = 1, N_OPT_VAR
                 WRITE(name,101) lw_opt_vars(ivar),ib
                 CALL GET_ZONAL(idata_pak,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)              

                 SELECT CASE (ivar)
                    CASE (1)
                       lw_ext_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       lw_ssa_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       lw_g_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-4)
                 END SELECT

              END DO ! ivar
           END DO ! ib

           DO ivar = 1, N_EXT_DIAG
              name = diag_opt_vars(ivar)

              CALL GET_ZONAL(idata_pak,
     &                       nlat,
     &                       nlon,
     &                       ilat_in,
     &                       ijpak,
     &                       ib2n,
     &                       nuan,
     &                       levsa,
     &                       name)

              CALL GET_ZONAL(idata_pal,
     &                       nlat,
     &                       nlon,
     &                       ilat_in,
     &                       ijpak,
     &                       ib2n,
     &                       nuan,
     &                       levsa,
     &                       name)              

              SELECT CASE (ivar)
                 CASE (1)
                    w055_ext_sa_pak(:,:) = idata_pak(:,:)
                    w055_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (2)
                    w110_ext_sa_pak(:,:) = idata_pak(:,:)
                    w110_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (3)
                    pressure_sa_pak(:,:) = idata_pak(:,:)
                    pressure_sa_pal(:,:) = idata_pal(:,:)
                 CASE DEFAULT
                    call xit("GET_STRAT_AEROSOL_OPTICS",-3)
              END SELECT

           END DO ! ivar

        ENDIF

      ELSE

C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
           DO ib = 1, nbs
              DO ivar = 1, N_OPT_VAR

                 WRITE(name,101) sw_opt_vars(ivar),ib
                 CALL GET_ZONAL(idata_pak,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2p,
     &                          nuan,
     &                          levsa,
     &                          name)

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 SELECT CASE (ivar)
                    CASE (1)
                       sw_ext_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       sw_ssa_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       sw_g_sa_pak(:,:,ib) = idata_pak(:,:)
                       sw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-5)
                 END SELECT
              END DO ! ivar
           END DO ! ib

           DO ib = 1, nbl
              DO ivar = 1, N_OPT_VAR
                 WRITE(name,101) lw_opt_vars(ivar),ib
                 CALL GET_ZONAL(idata_pak,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2p,
     &                          nuan,
     &                          levsa,
     &                          name)

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 SELECT CASE (ivar)
                    CASE (1)
                       lw_ext_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       lw_ssa_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       lw_g_sa_pak(:,:,ib) = idata_pak(:,:)
                       lw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-6)
                 END SELECT                 

              END DO ! ivar
           END DO ! ib

           DO ivar = 1, N_EXT_DIAG
              name = diag_opt_vars(ivar)

              CALL GET_ZONAL(idata_pak,
     &                       nlat,
     &                       nlon,
     &                       ilat_in,
     &                       ijpak,
     &                       ib2n,
     &                       nuan,
     &                       levsa,
     &                       name)

              CALL GET_ZONAL(idata_pal,
     &                       nlat,
     &                       nlon,
     &                       ilat_in,
     &                       ijpak,
     &                       ib2n,
     &                       nuan,
     &                       levsa,
     &                       name)              

              SELECT CASE (ivar)
                 CASE (1)
                    w055_ext_sa_pak(:,:) = idata_pak(:,:)
                    w055_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (2)
                    w110_ext_sa_pak(:,:) = idata_pak(:,:)
                    w110_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (3)
                    pressure_sa_pak(:,:) = idata_pak(:,:)
                    pressure_sa_pal(:,:) = idata_pal(:,:)
                 CASE DEFAULT
                    call xit("GET_STRAT_AEROSOL_OPTICS",-6)
              END SELECT

           END DO ! ivar

          !--- INTERPOLATE TO CURRENT DAY
          day1=REAL(mm_doy(mon_prev)) 
          day2=REAL(iday)
          day3=REAL(mm_doy(mon_next))

          IF(day2.lt.day1) day2=day2+365. 
          IF(day3.lt.day2) day3=day3+365. 

          w1=(day2-day1)/(day3-day1)
          w2=(day3-day2)/(day3-day1)

          IF (mynode .EQ. 0) THEN
            WRITE(6,*)
     &        'GET_STRAT_AEROSOL_OPTICS: Interpolating at ',curr_year,
     &        'day',iday,' between ',year_prev,' day',mm_doy(mon_prev),
     &        ' and ',year_next,' day',mm_doy(mon_next),
     &        ' using weights ',w1,w2
          ENDIF

          ijpak_loc = ijpak-1

          DO ib = 1, nbs
             DO iz=1,levsa  
                DO i=1,ijpak_loc
                   e1 = sw_ext_sa_pal(i,iz,ib)
                   e2 = sw_ext_sa_pak(i,iz,ib)
                   s1 = sw_ssa_sa_pal(i,iz,ib)
                   s2 = sw_ssa_sa_pak(i,iz,ib)
                   g1 = sw_g_sa_pal(i,iz,ib)
                   g2 = sw_g_sa_pak(i,iz,ib)
                   
                   g = w1*e1*s1*g1 + w2*e2*s2*g2
                   s = w1*e1*s1 + w2*e2*s2
                   e = w1*e1 + w2*e2

                   sw_g_sa_pak(i,iz,ib)   = g/s
                   sw_ssa_sa_pak(i,iz,ib) = s/e
                   sw_ext_sa_pak(i,iz,ib) = e

                ENDDO ! i
             ENDDO ! iz
          END DO ! ib

          DO ib = 1, nbl
             DO iz=1,levsa  
                DO i=1,ijpak_loc

                   e1 = lw_ext_sa_pal(i,iz,ib)
                   e2 = lw_ext_sa_pak(i,iz,ib)
                   s1 = lw_ssa_sa_pal(i,iz,ib)
                   s2 = lw_ssa_sa_pak(i,iz,ib)
                   g1 = lw_g_sa_pal(i,iz,ib)
                   g2 = lw_g_sa_pak(i,iz,ib)
                   
                   g = w1*e1*s1*g1 + w2*e2*s2*g2
                   s = w1*e1*s1 + w2*e2*s2
                   e = w1*e1 + w2*e2

                   lw_g_sa_pak(i,iz,ib)   = g/s
                   lw_ssa_sa_pak(i,iz,ib) = s/e
                   lw_ext_sa_pak(i,iz,ib) = e

                ENDDO ! i
             ENDDO ! iz
          END DO ! ib

          DO iz=1,levsa  
             DO i=1,ijpak_loc

                w055_ext_sa_pak(i,iz)= w1*w055_ext_sa_pal(i,iz)
     &                               + w2*w055_ext_sa_pak(i,iz)

                w110_ext_sa_pak(i,iz)= w1*w110_ext_sa_pal(i,iz)
     &                               + w2*w110_ext_sa_pak(i,iz)

                pressure_sa_pak(i,iz)= w1*pressure_sa_pal(i,iz)
     &                               + w2*pressure_sa_pak(i,iz)
             ENDDO ! i
          ENDDO ! iz

       ELSE
C 
C         * THIS IS IN THE MIDDLE OF A RUN. 
C

           DO ib = 1, nbs
              DO ivar = 1, N_OPT_VAR
                 WRITE(name,101) sw_opt_vars(ivar),ib

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 SELECT CASE (ivar)
                    CASE (1)
                       sw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       sw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       sw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-7)
                 END SELECT

              END DO ! ivar
           END DO ! ib

           DO ib = 1, nbl
              DO ivar = 1, N_OPT_VAR
                 WRITE(name,101) lw_opt_vars(ivar),ib

                 CALL GET_ZONAL(idata_pal,
     &                          nlat,
     &                          nlon,
     &                          ilat_in,
     &                          ijpak,
     &                          ib2n,
     &                          nuan,
     &                          levsa,
     &                          name)

                 SELECT CASE (ivar)
                    CASE (1)
                       lw_ext_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (2)
                       lw_ssa_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE (3)
                       lw_g_sa_pal(:,:,ib) = idata_pal(:,:)
                    CASE DEFAULT
                       call xit("GET_STRAT_AEROSOL_OPTICS",-8)
                 END SELECT

              END DO ! ivar
           END DO ! ib

           DO ivar = 1, N_EXT_DIAG
              name = diag_opt_vars(ivar)

              CALL GET_ZONAL(idata_pal,
     &                       nlat,
     &                       nlon,
     &                       ilat_in,
     &                       ijpak,
     &                       ib2n,
     &                       nuan,
     &                       levsa,
     &                       name)              

              SELECT CASE (ivar)
                 CASE (1)
                    w055_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (2)
                    w110_ext_sa_pal(:,:) = idata_pal(:,:)
                 CASE (3)
                    pressure_sa_pal(:,:) = idata_pal(:,:)
                 CASE DEFAULT
                    call xit("GET_STRAT_AEROSOL_OPTICS",-8)
              END SELECT

           END DO ! ivar

        ENDIF
      ENDIF 

 101  FORMAT(A3,I1)

      RETURN
      END
