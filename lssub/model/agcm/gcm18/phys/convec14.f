      SUBROUTINE CONVEC14(TH,PRESSG,DEL,SHJ,SHTJ,
     1                   ILEV,ILEVM,LEV,ILG,IL1,IL2,RGOCP)
C
C     * FEB 25/2008 - M.LAZARE.    COSMETIC BUGFIX: SHTJ HAS
C     *                            PROPER DECLARATION FOR
C     *                            NUMBER OF LEVELS (LEV, NOT
C     *                            ILEV).
C     * SEP 11/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - CALLS NEW STAWCL4 INSTEAD
C     *                              OF STAWCL2.       
C     *                            - WORK ARRAYS NOW LOCAL.
C     * MAY 01/2006 - M.LAZARE/    PREVIOUS VERSION CONVEC13 FOR
C     *               K.VONSALZEN. GCM15E:
C     *                            - NO MOISTURE PROCESSES (DRY
C     *                              ADJUSTMENT ONLY), HENCE
C     *                              ALL CALCULATIONS, I/O AND WORK
C     *                              FIELDS RELATED TO MOISTURE
C     *                              REMOVED. 
C     * DEC 10/2004 - M.LAZARE. PREVIOUS VERSION CONVEC12 FOR GCM15D.
C  
C     * PERFORMS DRY AND MOIST CONVECTIVE ADJUSTMENTS.
  
C     **************** INDEX OF VARIABLES *********************** 
C     *  I    => INPUT ARRAYS.
C     * I/O   => INPUT/OUTPUT ARRAYS. 
C     *  W    => WORK ARRAYS. 
C     * IC    => INPUT DATA CONSTANTS.
C     *  C    => DATA CONSTANTS PERTAINING TO SUBROUTINE ITSELF.
  
C  I  * DEL      LOCAL SIGMA HALF-LEVEL THICKNESS (I.E. DSHJ).
C  C  * DTDSCR   ASYMPTOTIC VALUE FOR MULTIPLE ADJUSTMENT CRITERIA. 
C  W  * DTF      TEMPERATURE CHANGE CAUSED BY A CONVECTIVE ADJUSTMENT.
C  W  * DTFMAX   MAXIMUM MASS-WEIGHTED DTF IN A COLUMN. 
C  W  * GAM,GAC  STABILITY FACTOR AND ITS CRITICAL VALUE. 
C  W  * IADJ     INDICATES WHETHER FURTHER ADJUSTMENT REQUIRED(=1) OR NOT.
C  W  * ICON     HOLDS POSITION OF GATHERED POINTS VS LONGITUDE INDEX.
C IC  * ILEV     NUMBER OF MODEL LEVELS 
C IC  * ILEVM    ILEV-1 
C IC  * ILG      LON+2 = SIZE OF GRID SLICE.
C IC  * IL1,IL2  START AND END LONGITUDE INDICES FOR LATITUDE CIRCLE. 
C  W  * ITER     NUMBER OF ITERATIONS AT EACH LONGITUDE POINT.
C IC  * LEV      ILEV+1 
C  C  * MAD      LOGICAL SWITCH TO INDICATE WHETHER TO MULTIPLE ADJUST. 
C  C  * NUPS     NUMBER OF DRY UNSTABLE POINTS. 
C  W  * P        GRID SLICE OF AMBIENT PRESSURE IN MBS. 
C  I  * PRESSG   ROW OF SURFACE PRESSURE IN PA. 
C IC  * RGOCP    RGAS/CP = "KAPA".
C  I  * SHJ      GRID SLICE OF LOCAL HALF-LEVEL SIGMA VALUES. 
C  I  * SHTJ     GRID SLICE OF LOCAL HALF-LEVEL SIGMA INTERFACE VALUES. 
C  W  * ST       STABILITY MATRICES.
C  W  * T        GATHERED GRID SLICE OF TEMPERATURE.
C I/O * TH       GRID SLICE OF TEMPERATURE. 
  
C     *********************************************************** 
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
  
C     * I/O FIELDS: 

      REAL  ,  INTENT(INOUT), DIMENSION(ILG,ILEV) :: TH
      REAL  ,  INTENT(IN),    DIMENSION(ILG,ILEV) :: DEL,SHJ
      REAL  ,  INTENT(IN),    DIMENSION(ILG,LEV)  :: SHTJ
      REAL  ,  INTENT(IN),    DIMENSION(ILG)      :: PRESSG
  
C     * WORK FIELDS:  

      REAL  ,  DIMENSION(ILG,ILEVM,6) :: ST
      REAL  ,  DIMENSION(ILG,ILEV) :: T,P
      REAL  ,  DIMENSION(ILG) :: GAC,GAM,DTF,DTFMAX
      INTEGER, DIMENSION(ILG) :: ICON,ITER,IADJ  
  
      LOGICAL MAD 
C
      DATA MAD   /.TRUE./ 
      DATA DTDSCR/0.002 / 
C======================================================================
C     * CALCULATE ROW OF STABILITY MATRICES.
C 
      CALL STAWCL4 (ST,SHTJ(1,2),SHTJ,SHJ,
     1              ILEVM,ILEV,ILEV+1,IL1,IL2,ILG,RGOCP)
C 
C     * INITIALIZE NECESSARY ARRAYS.
C 
      DO 50 IL=IL1,IL2
         ICON(IL)  = IL 
         IADJ(IL)  = 0
         ITER(IL)  = 0
   50 CONTINUE
C
      DO 60 L=ILEV,1,-1
      DO 60 IL=IL1,IL2
         P(IL,L)   = SHJ(IL,L) * PRESSG(IL) * 0.01
   60 CONTINUE
C
      LEN     = IL2 - IL1 + 1 
      LENGATH = LEN 
C 
C     ****************************************************************
C     * BEGIN ITERATION LOOP *****************************************
C     ****************************************************************
  
  100 CONTINUE
C 
C     * CALCULATE GATHERED TEMPERATURE AND SPECIFIC HUMIDITY GRID 
C     * SLICES. DO-LOOP INDEX "K" REFERS TO GATHERED LONGITUDE POINTS 
C     * FROM NOW ON, WHILE INDEX "IL" REFERS TO SCATTERED OUT VALUES. 
C     * THE ARRAY "ICON" CONTAINS THE GATHERED POINTS' LOCATIONS. 
C 
      DO 110 L=1,ILEV 
      DO 110 K=1,LENGATH
          T(K,L) = TH(ICON(K),L)
  110 CONTINUE
C 
C     * UPDATE ITERATION COUNTER ARRAY. 
C     * INITIALIZE THRESHOLD FOR DRY ADJUSTMENT.
C 
      DO 120 K=1,LENGATH
          ITER(ICON(K)) = ITER(ICON(K)) + 1 
          DTFMAX(K)     = 0.0 
  120 CONTINUE
C 
C     * CONVECTIVE ADJUSTMENT "BOOTSTRAPPING" LOOP. 
C     * INDEX "J" REFERS TO LOWER LEVEL AND INDEX "I" TO UPPER LEVEL. 
C 
      DO 200 J=ILEV,2,-1
          I    = J-1
C
C         * FIRST DRY ADJUSTMENT LOOP WHICH DOES NOT CARE WHERE THINGS
C         * MIGHT GET UNPHYSICAL WITH EST.GE.P. THIS INCLUDES THE
C         * INITIALIZATION OF CERTAIN ARRAYS.
C
          DO 125 K=1,LENGATH
              GAC(K)     = 0.0
              DTF(K)     = 0.0
              TT         = ST(ICON(K),I,1)*T(K,I)+ST(ICON(K),I,2)*T(K,J)
              GAM(K)     = TT+ST(ICON(K),I,3)*(T(K,I)-T(K,J)) 
C 
C             * NOW DRY ADJUST. 
C 
              IF (GAM(K).LT.GAC(K))                              THEN 
                  DTF(K)    = ST(ICON(K),I,4)*(GAC(K)-GAM(K)) 
                  DTFMAX(K) = MAX( DTFMAX(K), 
     1                               ABS(DTF(K)) * DEL(ICON(K),J))
                  T(K,I)    = T(K,I) + DTF(K) * ST(ICON(K),I,5) 
                  T(K,J)    = T(K,J) + DTF(K) 
              ENDIF 
C 
C             * UPDATE WHETHER GATHERED LONGITUDE POINT NEEDS FURTHER 
C             * ADJUSTING.
C
              IF (DTFMAX(K).GT.DTDSCR)    IADJ(ICON(K)) = 1 
  125     CONTINUE
  200 CONTINUE
C 
C     ****************************************************************
C     * END OF ITERATION LOOP ****************************************
C     ****************************************************************
C 
C     * GATHER BACK TEMPERATURE.
C 
      DO 230 L=1,ILEV 
      DO 230 K=1,LENGATH
          TH(ICON(K),L) = T(K,L)
  230 CONTINUE
C 
      IF(MAD)                                                    THEN 
C 
C         * RECALCULATE GATHERED INDICES. 
C 
          JYES = 0
          JNO  = LEN + 1
          DO 240 IL=IL1,IL2 
              IF(IADJ(IL).NE.0)                                  THEN 
                  JYES       = JYES + 1 
                  ICON(JYES) = IL 
              ELSE
                  JNO        = JNO  - 1 
                  ICON(JNO)  = IL 
              ENDIF 
  240     CONTINUE
          LENGATH = JYES
          IF(LENGATH.GT.0)                                       THEN 
C 
C             * RE-INITIALIZE ADJUSTMENT INDICATOR ARRAY AND START OVER.
C 
              DO 245 IL=IL1,IL2 
                  IADJ(IL) = 0. 
  245         CONTINUE
              GO TO 100 
          ENDIF 
      ENDIF 
C 
C     * CALCULATE NUMBER OF WET/DRY UNSTABLE POINTS IN THIS LATITUDE. 
C 
      DO 250 IL=IL1,IL2 
          IF(ITER(IL).NE.1)                     NUPS  = NUPS + 1
  250 CONTINUE
  
      RETURN
      END 
