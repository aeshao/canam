      SUBROUTINE TGCAL2 (TSG,TSGB, TSH, SG,SGB,SH,
     1                    ILEV,LEV, ILG,IS,IF)
C 
C     * JUL 15/88 - M.LAZARE - CHANGE (NK,I) ARRAYS TO PROPER (I,NK). 
C     * JAN 29/88 - R.LAPRISE.
C     * SET TEMPERATURE ARRAYS FOR PHYSICS ON MOMENTUM LEVELS 
C     * FOR HYBRID VERSION OF MODEL.
C     * SGB ARE MOMENTUM LAYER INTERFACES,
C     * SG  ARE MOMENTUM MID LAYER POSITIONS. 
C     * SH  ARE THERMODYNAMICS MID LAYER POSITIONS. 
C     * LEV = ILEV+1. 
C     * 
C     *     MOMENTUM            THERMODYNAMICS
C     * MID LAYER  INTERFACE  MID LAYER   TSH(TH) 
C     * 
C     */////////////////////////////////////////////////////////////////
C     *                         MOON LAYER TSH(1) 
C     *                      ---------------------
C     *......SG(1)..........
C     *                      ...SH(1)......TSH(2) 
C     *-------------- SGB(1)
C     *                      ---------------------
C     *......SG(2)..........
C     *                      ...SH(2)......TSH(3) 
C     *-------------- SGB(2)
C     *                      ---------------------
C     *......SG(3)..........
C     *                      ...SH(3)......TSH(4) 
C     *-------------- SGB(3)
C     *                      ---------------------
C     *......SG(4)..........
C     *                      ...SH(4)......TSH(5) 
C     *-------------- SGB(4)
C     *                      ---------------------
C     *......SG(5)..........
C     *                      ...SH(5)......TSH(6) 
C     * 
C     *////////////// SGB(5)=1. ////////////////////////////////////////
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   TSG(ILG,ILEV), TSGB(ILG,ILEV), TSH(ILG, LEV) 
      REAL    SG(ILG,ILEV),  SGB(ILG,ILEV),  SH(ILG,ILEV) 
C-----------------------------------------------------------------------
C     * MOMENTUM MID LAYER TEMPERATURES.
C 
      DO 400 I=IS,IF
  400 TSG(I,1)=TSH(I,2) 
C 
      DO 450 L=2,ILEV 
      DO 450 I=IS,IF
  450 TSG(I,L)   = (TSH(I,L  )*LOG(SH(I  ,L)/SG(I  ,L))
     1             +TSH(I,L+1)*LOG(SG(I  ,L)/SH(I,L-1))) 
     2                        /LOG(SH(I  ,L)/SH(I,L-1))
C 
C     * MOMENTUM LAYER INTERFACE TEMPERATURES.
C 
      DO 500 L=1,ILEV-1 
      DO 500 I=IS,IF
  500 TSGB(I,L) = (TSH(I,L+1)*LOG(SH (I,L+1)/SGB(I  ,L)) 
     1            +TSH(I,L+2)*LOG(SGB(I  ,L)/SH (I  ,L)))
     2                       /LOG(SH (I,L+1)/SH (I  ,L)) 
C 
C     * FOR COMPATIBILITY WITH FORMER STAGGERED MODEL...
C 
      DO 550 I=IS,IF
  550 TSGB(I,ILEV) = TSH(I,ILEV+1)
C 
      RETURN
C-----------------------------------------------------------------------
      END 
