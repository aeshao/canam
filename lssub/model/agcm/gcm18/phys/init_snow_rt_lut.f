      SUBROUTINE INIT_SNOW_RT_LUT()

!
!     * Feb 10/2015 - J.Cole. New version for gcm18:
!                             - NBC increased from 12 to 20.
!     * JAN 24/2013 - J.COLE. Previous version for gcm17:
!                    - READ IN SNOW ALBEDO AND TRANSMISSIVITY LOOK UP TABLE
!                      REQUIRED FOR NEW ALBEDO PARAMETERIZATIONS.

      IMPLICIT NONE

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! THIS SUBROUTINE READS IN A SINGLE CCCMA FORMATTED FILE THAT CONTAINS
! LOOKUP TABLES FOR EACH BAND IN THE CCCMA SOLAR RADIATIVE TRANSFER MODEL 
! FOR SNOW ALBEDO AND TRANMISSIVITY.  FOR BOTH VARIABLES THERE ARE TWO TABLES
! ONE FOR INCIDENT DIRECT BEAM RADIATION AND DIFFUSE RADIATION.  THE
! TABLES ARE A FUNCTION OF THE COSINE OF SOLAR ZENITH ANGLE, UNDERLYING
! SURFACE ALBEDO, SNOW WATER EQUIVALENT, SNOW GRAIN SIZE AND BLACK CARBON
! CONCENTRATION.  THE DATA IN THE FILE HAS BEEN UNROLLED INTO A 1XN VECTOR
! OF VALUES WHICH NEEDS TO BE RESHAPED INTO FIVE DIMENSIONAL ARRAYS.
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      INTEGER :: IU, ISBUF, ISDAT, I, IVAR
      INTEGER :: NEWUNIT, NC4TO8

      INTEGER :: ismu,isalb,ibc,isgs,iswe,ipnt
      
!--- table dimensions
! nsmu ... number of cosine of the solar zenith angle
! nsfa ... number of surface albedos
! nbc  ... number of black carbon concentrations
! nsgs ... number of snow grain sizes
! nswe ... number of snow water equivalents
! nbnd ... number of solar radiation bands

      INTEGER, PARAMETER :: 
     1 nsmu = 10,
     2 nsfa = 11,
     3 nbc  = 20,
     4 nsgs = 10,
     5 nswe = 11,
     6 nbnd = 4,
     7 irec = nsmu*nsfa*nbc*nsgs*nswe,
     8 lrec = irec*2

      REAL :: datat(irec)

      LOGICAL :: OK

      COMMON /ISCOM/ ISBUF(8), ISDAT(lrec)
      
      INTEGER MACHINE, INTSIZE
      COMMON /MACHTYP/ MACHINE, INTSIZE

      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE

      INTEGER :: snow_alb_lut_init,snow_tran_lut_init 
      
      CHARACTER(LEN=4) :: CNAM

! Need four look up tables.
      REAL, DIMENSION(nbc,nswe,nsgs,nsmu,nsfa,nbnd) ::
     1 albdif,
     2 albdir,
     3 trandif,
     4 trandir

      COMMON /SNOWALBLUT/ albdif,albdir,snow_alb_lut_init
      COMMON /SNOWTRANLUT/ trandif,trandir,snow_tran_lut_init

      snow_alb_lut_init  = 0
      snow_tran_lut_init = 0

!.........Get an available unit number
      iu=newunit(80)

!.........Open the file containing the lookup tables
      INQUIRE(FILE="SNOWRTLUT",EXIST=OK)
      IF (.NOT.OK) THEN
         WRITE(6,*)'INIT_SNOW_RT_LUT: ',
     1                  'Error accessing SNOW_ALB_TRAN_LUT'
         CALL XIT('INIT_SNOW_RT_LUT',-1)
      ENDIF
      OPEN(IU,FILE='SNOWRTLUT',FORM='UNFORMATTED')
      
      IVAR = 1

!.........Read the albedo lookup tables

      DO I=1,nbnd
         WRITE(CNAM,1001) "ADF",I
         CALL GETFLD2(IU,datat,
     &           NC4TO8("GRID"),IVAR,NC4TO8(CNAM),1,ISBUF,LREC,OK)
         IF (.NOT.OK) CALL XIT('INIT_SNOW_RT_LUT',-2)
         ipnt = 1
         DO isalb = 1, nsfa
            DO ismu = 1, nsmu
               DO isgs = 1, nsgs
                  DO iswe = 1, nswe
                     DO ibc = 1, nbc
                        albdif(ibc,iswe,isgs,ismu,isalb,i)=datat(ipnt)
                        ipnt = ipnt + 1
                     END DO ! ibc
                  END DO ! iswe
               END DO ! isgs
            END DO ! ismu
         END DO ! isalb
      ENDDO

      DO I=1,nbnd
         WRITE(CNAM,1001) "ADR",I
         CALL GETFLD2(IU,datat,
     &           NC4TO8("GRID"),IVAR,NC4TO8(CNAM),1,ISBUF,LREC,OK)
         IF (.NOT.OK) CALL XIT('INIT_SNOW_RT_LUT',-3)
         ipnt = 1
         DO isalb = 1, nsfa
            DO ismu = 1, nsmu
               DO isgs = 1, nsgs
                  DO iswe = 1, nswe
                     DO ibc = 1, nbc
                        albdir(ibc,iswe,isgs,ismu,isalb,i)=datat(ipnt)
                        ipnt = ipnt + 1
                     END DO ! ibc
                  END DO ! iswe
               END DO ! isgs
            END DO ! ismu
         END DO ! isalb
      ENDDO

!.........Read the transmissivity lookup tables
      DO I=1,nbnd
         WRITE(CNAM,1001) "TDF",I
         CALL GETFLD2(IU,datat,
     &           NC4TO8("GRID"),IVAR,NC4TO8(CNAM),1,ISBUF,LREC,OK)
         IF (.NOT.OK) CALL XIT('INIT_SNOW_RT_LUT',-4)
         ipnt = 1
         DO isalb = 1, nsfa
            DO ismu = 1, nsmu
               DO isgs = 1, nsgs
                  DO iswe = 1, nswe
                     DO ibc = 1, nbc
                        trandif(ibc,iswe,isgs,ismu,isalb,i)=datat(ipnt)
                        ipnt = ipnt + 1
                     END DO ! ibc
                  END DO ! iswe
               END DO ! isgs
            END DO ! ismu
         END DO ! isalb
      ENDDO

      DO I=1,nbnd
         WRITE(CNAM,1001) "TDR",I
         CALL GETFLD2(IU,datat,
     &           NC4TO8("GRID"),IVAR,NC4TO8(CNAM),1,ISBUF,LREC,OK)
         IF (.NOT.OK) CALL XIT('INIT_SNOW_RT_LUT',-5)
         ipnt = 1
         DO isalb = 1, nsfa
            DO ismu = 1, nsmu
               DO isgs = 1, nsgs
                  DO iswe = 1, nswe
                     DO ibc = 1, nbc
                        trandir(ibc,iswe,isgs,ismu,isalb,i)=datat(ipnt)
                        ipnt = ipnt + 1
                     END DO ! ibc
                  END DO ! iswe
               END DO ! isgs
            END DO ! ismu
         END DO ! isalb
      ENDDO

      snow_alb_lut_init  = 1
      snow_tran_lut_init = 1

      CLOSE(IU)

      RETURN
 1001 FORMAT(A,I1)

      END 
