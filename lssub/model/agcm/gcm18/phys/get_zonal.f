      SUBROUTINE GET_ZONAL(dataout, ! OUTPUT
     &                     nlat,    ! INPUT
     &                     nlon,
     &                     ilat,
     &                     igrid,
     &                     iday,
     &                     nf,
     &                     nlev,
     &                     cname)

C
C     * DEC 21/16 - J. COLE   
C
C     * GETS ZONAL CROSS-SECTIONS OF DATA FROM FILE NF AND
C     * CREATE 3-D FIELDS.
C     * SUBROUTINE HEAVILY BASED ON GETZON9.
C

      IMPLICIT NONE

!
! PARAMETERS
!

!
! INPUT DATA
!

      INTEGER,INTENT(IN) :: 
     & nlat,
     & nlon,
     & ilat,
     & igrid,
     & iday,
     & nf,
     & nlev

      CHARACTER(LEN=4), INTENT(IN) :: 
     & cname

!
! OUTPUT DATA
!

      REAL, INTENT(OUT) ::
     & dataout(igrid,nlev)

!
! LOCAL DATA
!
      LOGICAL :: 
     & ok

      REAL :: 
     & dataloc(nlat,nlev)
      
      INTEGER :: 
     & maxx,
     & iz,
     & i,
     & j,
     & jstart,
     & jend,
     & nlath,
     & nlatq,
     & ni,
     & nl,
     & jsp,
     & jnp,
     & jse,
     & jne,
     & nc4to8,
     & ibuf,
     & idat,
     & mynode,
     & machine, 
     & intsize,
     & igridm1


!
! COMMON BLOCKS
!

      COMMON /icom/ ibuf(8),idat(1) 
      COMMON /machtyp/ machine, intsize
      COMMON /mpinfo/ mynode

!---------------------------------------------------------------------
! This subroutine reads in a zonal variable for a given timestep and
! "reorders" it into an array used by the AGCM.  The array is a zonal,
! i.e., latitude and height/pressure.
!---------------------------------------------------------------------

!
!     * Allocate the array needed to read in the data.
!
!
!     * Find and read cross-section.
!
      maxx=(nlat*nlev + 8 )*machine 

      REWIND nf 

      CALL FIND(nf,nc4to8("GRID"),iday,nc4to8(cname),1,ok) 
      IF(.NOT .ok) CALL XIT('GET_ZONAL',-1)

      iz = 1
      CALL GETFLD2(nf,dataloc,nc4to8("GRID"),iday,nc4to8(cname),
     1             iz,ibuf,maxx,ok)

      IF(.NOT. ok) THEN
         PRINT *, '0PROBLEM WITH DATA'
         CALL XIT('GET_ZONAL',-2)
      ENDIF

!
!     * Now "pack" the cross-section into a 3D array.
!

      jstart=mynode*ilat+1
      jend  =mynode*ilat+ilat

!
!     * Process each level one at a time.
!
      IF(MOD(nlat,4) .NE. 0) CALL XIT('GET_ZONAL',-5)

      nlath=nlat/2 
      nlatq=nlat/4
      
      DO iz=1,nlev
         
         ni=0
         nl=0

!
!       * CREATE REORDERED FIELDS.
!
         DO j=1,nlatq
           
            jsp=j
            nl=nl+1
            IF(nl .ge. jstart .and. nl .le. jend) THEN  
               DO i=1,nlon
                  ni=ni+1
                  dataout(ni,iz)= dataloc (jsp,iz)
               END DO ! I
            ENDIF
            
            jnp=nlat-j+1 
            nl=nl+1
            IF(nl .ge. jstart .and. nl .le. jend) THEN  
               DO i=1,nlon
                  ni=ni+1
                  dataout(ni,iz) = dataloc(jnp,iz)
               END DO ! I
            ENDIF
            
            jse=nlath-j+1
            nl=nl+1
            IF(nl .ge. jstart .and. nl .le. jend) THEN  
               DO i=1,nlon
                  ni=ni+1
                  dataout(ni,iz) = dataloc(jse,iz)
               END DO ! I
            ENDIF
            
            jne=nlath+j
            nl=nl+1
            IF(nl .ge. jstart .and. nl .le. jend) THEN  
               DO i=1,nlon
                  ni=ni+1
                  dataout(ni,iz) = dataloc(jne,iz)
               END DO ! I
            ENDIF

         END DO ! J
      END DO ! IZ

      RETURN
      END
