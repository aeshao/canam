      SUBROUTINE TOTAL_CLD_FRAC2(CLDT,CLDO,CLWT,CICT,                 ! OUTPUT
     1                          CLW, CIC, REL, REI, DZ,               ! INPUT  
     2                          CLDWATMIN, ILG, IL1, IL2, ILEV, NXLOC) 

!     APR 29/2012 - M. LAZARE.  SEPARATE CLOUD WATER/ICE CALCULATION.
!     FEB 5, 2009 - JASON COLE. ORIGINAL VERSION TOTAL_CLD_FRAC FOR GCM15G/H/I.
!
! SUBROUTINE TO COMPUTE THE TOTAL CLOUD FRACTION USING SUBCOLUMNS FROM 
! THE STOCHASTIC CLOUD GENERATOR (CLD_GEN_DRIVER2).  DONE IN TWO WAYS:
! USING ALL CLOUDY SUBCOLUMNS CONTAINING AT LEAST CLDWATMIN CLOUD WATER (CLDT)
! AND USING ONLY CLOUDY SUBCOLUMNS WITH VERTICALLY INTEGRATED VISIBLE OPTICAL 
! THICKNESS OF 0.2 (CLDO) WHICH IS A BIT MORE CONSISTENT WITH ISCCP AND CERES
! OBSERVATIONS

      IMPLICIT NONE

!
! OUTPUT 
!

      REAL, DIMENSION(ILG),INTENT(OUT) ::
     1 CLDT,                             ! TOTAL CLOUD FRACTION (USING CLDWATMIN THRESHOLD)
     2 CLDO,                             ! TOTAL CLOUD FRACTION (USING TAU > 0.2 THRESHOLD)
     3 CLWT,                             ! TOTAL CLOUD LIQUID WATER PATH (KG/M2)
     4 CICT                             ! TOTAL CLOUD ICE PATH (KG/M2)

!
! INPUT 
!
      
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) ::
     1 CLW,                                         ! CLOUD LIQUID WATER                  (UNITS)
     2 CIC,                                         ! CLOUD ICE WATER                     (UNITS)
     3 REL,                                         ! CLOUD LIQUID WATER EFFECTIVE RADIUS (MICRONS) 
     4 REI                                          ! CLOUD ICE WATER EFFECTIVE DIAMETER  (MICRONS)

      REAL, DIMENSION(ILG,ILEV),INTENT(IN) ::
     1 DZ                                    ! LAYER THICKNESS (M)
 
      REAL, INTENT(IN) ::
     1 CLDWATMIN          ! THRESHOLD TO COMPUTE CLDT
      
      INTEGER, INTENT(IN) ::
     1 ILG,                 ! NUMBER OF GCM COLUMNS
     2 IL1,                 ! STARTING GCM COLUMN 
     3 IL2,                 ! ENDING GCM COLUMN
     4 ILEV,                ! NUMBER OF VERTICAL LAYERS 
     5 NXLOC                ! NUMBER OF SUBCOLUMNS

!
! LOCAL
!
      
      REAL, PARAMETER :: 
     1 MIN_TAU = 0.2,    ! THRESHOLD TO COMPUTE CLDO
     2 R_ONE   = 1.0,
     3 R_ZERO  = 0.0
 
      REAL, DIMENSION(ILG) ::
     1 CUMTAU,                ! ACCUMULATE CLOUD OPTICAL THICKNESS
     2 COUNT_CLDY
 
      REAL ::
     1 INVREL,     
     2 REI_LOC,        
     3 INVREI,
     4 TAU_VIS,
     5 TAULIQVIS,
     6 TAUICEVIS,
     7 R_NXLOC,
     8 CLW_PATH,
     9 CIC_PATH 

      INTEGER ::
     1 I,
     2 L,
     3 ICOL

! INITIALIZE VARIABLES
 
      DO I = IL1, IL2
         CLDT(I) = R_ZERO
         CLDO(I) = R_ZERO
         CLWT(I) = R_ZERO
         CICT(I) = R_ZERO 
      END DO

      R_NXLOC = REAL(NXLOC)

      DO ICOL = 1, NXLOC

        DO I = IL1, IL2
           COUNT_CLDY(I) = R_ZERO
           CUMTAU(I)     = R_ZERO
        END DO

        DO L = 1, ILEV
           DO I = IL1, IL2

              TAU_VIS  = R_ZERO
              CLW_PATH = CLW(I,L,ICOL)*DZ(I,L)
              CIC_PATH = CIC(I,L,ICOL)*DZ(I,L)

! CALCULATION FOR CLDT
              IF (CLW_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
                 CLWT(I) = CLWT(I) + CLW_PATH/1000.
              END IF
              IF (CIC_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
                 CICT(I) = CICT(I) + CIC_PATH/1000.
              END IF

! CALCULATION FOR CLDO

              IF ((CLW_PATH .GT. CLDWATMIN  .OR.
     1             CIC_PATH .GT. CLDWATMIN) .AND.
     2             CUMTAU(I) .LT. MIN_TAU) THEN

! COMPUTE CLOUD OPTICAL THICKNESS FOR THIS VOLUME

                 INVREL = R_ONE/REL(I,L,ICOL)

                 REI_LOC = 1.5396*REI(I,L,ICOL) ! CONVERT TO GENERALIZED DIMENSION
                 INVREI  = R_ONE/REI_LOC

                 IF (CLW_PATH .GT. CLDWATMIN) THEN
                    TAULIQVIS = CLW(I,L,ICOL)
     1                        * (4.483e-04 + INVREL * (1.501 + INVREL
     2                        * (7.441e-01 - INVREL * 9.620e-01)))
                 ELSE
                    TAULIQVIS = R_ZERO
                 END IF
                 
                 IF (CIC_PATH .GT. CLDWATMIN) THEN
                    TAUICEVIS = CIC(I,L,ICOL) 
     1                        * (-0.303108e-04 + 0.251805e+01 * INVREI)  
                 ELSE
                    TAUICEVIS = R_ZERO
                 END IF

                 TAU_VIS = (TAULIQVIS+TAUICEVIS)*DZ(I,L)
              END IF

              CUMTAU(I) = CUMTAU(I) + TAU_VIS

           END DO ! I
        END DO ! L

        DO I = IL1, IL2
           CLDT(I) = CLDT(I) + COUNT_CLDY(I)

           IF (CUMTAU(I) .GT. MIN_TAU) THEN
              CLDO(I) = CLDO(I) + R_ONE
           END IF
        END DO ! I

      END DO ! ICOL

      DO I = IL1, IL2
         CLDT(I) = CLDT(I)/R_NXLOC
         CLDO(I) = CLDO(I)/R_NXLOC
         CLWT(I) = CLWT(I)/R_NXLOC
         CICT(I) = CICT(I)/R_NXLOC
      END DO
      
      RETURN
      END
