      SUBROUTINE INT_TIME_STRAT_AEROSOL_ETH(sw_ext_sa_row,  ! OUTPUT
     &                                      sw_ssa_sa_row,
     &                                      sw_g_sa_row,
     &                                      lw_ext_sa_row,
     &                                      lw_ssa_sa_row,
     &                                      w055_ext_sa_row,
     &                                      w110_ext_sa_row,
     &                                      pressure_sa_row,
     &                                      sw_ext_sa_rol,  ! INPUT
     &                                      sw_ssa_sa_rol,
     &                                      sw_g_sa_rol,
     &                                      lw_ext_sa_rol,
     &                                      lw_ssa_sa_rol,
     &                                      w055_ext_sa_rol,
     &                                      w110_ext_sa_rol,
     &                                      pressure_sa_rol,
     &                                      ilg,
     &                                      il1,
     &                                      il2,
     &                                      nlev,
     &                                      nbs,
     &                                      nbl,
     &                                      delt,
     &                                      gmt,
     &                                      iday,
     &                                      mday)

C
C     * JAN 10/17 - J. COLE   
C
C     * INTERPOLATE STRATOSPHERIC AEROSOL DATA FROM ETHZ DOWN TO 
C     * CURRENT TIMESTEP.
C
C     METHOD:
C
C     BASICALLY A SIMPLE LINEAR AVERAGE IN TIME.  FOR THE OPTICAL PROPERTIES
C     NEED TO AVERAGE USING THE APPROACH OF XXX TO MAINTAIN SIMILARITY OF 
C     RADIATIVE TRANSFER SOLUTIONS.

      IMPLICIT NONE
      
! PARAMETERS
      REAL, PARAMETER ::
     & SEC_PER_DAY = 86400.0,
     & DAY_PER_YEAR = 365.0

! INPUT

      REAL, INTENT(IN) :: 
     & sw_ext_sa_rol(ilg,nlev,nbs),
     & sw_ssa_sa_rol(ilg,nlev,nbs),
     & sw_g_sa_rol(ilg,nlev,nbs),
     & lw_ext_sa_rol(ilg,nlev,nbl),
     & lw_ssa_sa_rol(ilg,nlev,nbl),
     & w055_ext_sa_rol(ilg,nlev),
     & w110_ext_sa_rol(ilg,nlev),
     & pressure_sa_rol(ilg,nlev)

      INTEGER, INTENT(IN) ::
     & ilg,
     & il1,
     & il2,
     & nlev,
     & nbs,
     & nbl,
     & iday,
     & mday

      REAL, INTENT(IN) ::
     & delt,
     & gmt

! OUTPUT

      REAL, INTENT(OUT) :: 
     & sw_ext_sa_row(ilg,nlev,nbs),
     & sw_ssa_sa_row(ilg,nlev,nbs),
     & sw_g_sa_row(ilg,nlev,nbs),
     & lw_ext_sa_row(ilg,nlev,nbl),
     & lw_ssa_sa_row(ilg,nlev,nbl),
     & w055_ext_sa_row(ilg,nlev),
     & w110_ext_sa_row(ilg,nlev),
     & pressure_sa_row(ilg,nlev)
      
! LOCAL

      INTEGER ::
     & il,
     & ilev,
     & ib

      REAL ::
     & e,
     & e1,
     & e2,
     & s,
     & s1,
     & s2,
     & g,
     & g1,
     & g2,
     & w1,
     & w2,
     & sec0,
     & fmsec,
     & secsm

!
! Compute the number of timestesps from here to mday
!
      
      sec0  = real(iday)*sec_per_day + gmt
      fmsec = real(mday)*sec_per_day

      IF(fmsec .LT. sec0) fmsec=fmsec+day_per_year*sec_per_day

      secsm=fmsec-sec0
      
      w1 = (secsm-delt)/secsm
      w2 = delt/secsm
!
! Do the interpolation
!

! Solar

      DO ib = 1, nbs
         DO ilev = 1, nlev
            DO il = il1, il2

               e1 = sw_ext_sa_row(il,ilev,ib)
               e2 = sw_ext_sa_rol(il,ilev,ib)
               s1 = sw_ssa_sa_row(il,ilev,ib)
               s2 = sw_ssa_sa_rol(il,ilev,ib)
               g1 = sw_g_sa_row(il,ilev,ib)
               g2 = sw_g_sa_rol(il,ilev,ib)
               
               g = w1*e1*s1*g1 + w2*e2*s2*g2
               s = w1*e1*s1 + w2*e2*s2
               e = w1*e1 + w2*e2
                
               sw_g_sa_row(il,ilev,ib)   = g/s
               sw_ssa_sa_row(il,ilev,ib) = s/e
               sw_ext_sa_row(il,ilev,ib) = e

            END DO ! il
         END DO ! ilev
      END DO ! ib

! Thermal

      DO ib = 1, nbl
         DO ilev = 1, nlev
            DO il = il1, il2

               e1 = lw_ext_sa_row(il,ilev,ib)
               e2 = lw_ext_sa_rol(il,ilev,ib)
               s1 = lw_ssa_sa_row(il,ilev,ib)
               s2 = lw_ssa_sa_rol(il,ilev,ib)

               s = w1*e1*s1 + w2*e2*s2
               e = w1*e1 + w2*e2
                
               lw_ssa_sa_row(il,ilev,ib) = s/e
               lw_ext_sa_row(il,ilev,ib) = e

            END DO ! il
         END DO ! ilev
      END DO ! ib

! Diagnostics
      
      DO ilev = 1, nlev
         DO il = il1, il2

            w055_ext_sa_row(il,ilev) = w1*w055_ext_sa_row(il,ilev)
     &                               + w2*w055_ext_sa_rol(il,ilev)

            w110_ext_sa_row(il,ilev) = w1*w110_ext_sa_row(il,ilev)
     &                               + w2*w110_ext_sa_rol(il,ilev)

            pressure_sa_row(il,ilev) = w1*pressure_sa_row(il,ilev)
     &                               + w2*pressure_sa_rol(il,ilev)

         END DO ! il
      END DO ! ilev

      RETURN

      END
