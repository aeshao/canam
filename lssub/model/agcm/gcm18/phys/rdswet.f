      SUBROUTINE RDSWET(SNOW,WSNOW,REFF,GCROW,REFF0,DELT,ILG,IL1,IL2)
C
C     * M. Namazi & K. VON SALZEN, SNOW GRAIN DUE TO WET AGING.
C     * ONLY OVER LAND HAPPENS. 
C     * FOR THE REST OF REGIONS KEEP THE SAME VALUE,
C
      IMPLICIT NONE

C     * INTEGER AND REAL CONSTANTS.
      INTEGER ILG,IL1,IL2,I
      REAL :: REFF0
      REAL, PARAMETER :: PI=3.1415927,C1=4.22E-13

C     * INPUT ARRAYS.
      REAL, DIMENSION(ILG) :: SNOW, WSNOW,GCROW
      REAL  DELT

C     * INPUT/OUTPUT ARRAYS.
      REAL, DIMENSION(ILG) :: REFF
                                                                        
C     * INTERNAL WORK ARRAYS.
      REAL, DIMENSION(ILG) :: FLIQ
C----------------------------------------------------------------------
      DO 100 I=IL1,IL2
          IF(GCROW(I).LT.-0.5.AND.SNOW(I).GT.0.)                THEN
C           IF(REFF(I).EQ.0)  REFF(I)=REFF0
            REFF(I)=MAX(REFF0,REFF(I))
            REFF(I)=REFF(I)*1.E+6
            FLIQ(I)=WSNOW(I)/(SNOW(I)+WSNOW(I))
            REFF(I)=((0.75E+18*C1/PI)*(FLIQ(I)**3)*DELT
     1             + REFF(I)**3)**(1./3.)
            REFF(I)=REFF(I)*1.E-6 
          ENDIF
  100 CONTINUE  
 
      RETURN
      END 
