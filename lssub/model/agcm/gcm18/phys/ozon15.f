      SUBROUTINE OZON15(OZ, O3TOP, OZZXJ, PRESSG, SHTJ, SHJ,
     1                  LEVOZ, LEV, IL1, IL2, ILG, IOZTYP)
C
C     * THIS ROUTINE INTERPOLATES THE INPUT OZONE VOLUME MIXING RATIO
C     * FOR THE CURRENT LATITUDE (OZZXJ CORRESPONDING TO PREF GRID) TO
C     * THE LOCAL PRESSURE GRID.
C
C     * JAN 26/2018 - D.PLUMMER      NEW VERSION FOR GCM18+:
C     *                               - ADDED CMIP6 OZONE (IOZTYP=6)
C     *                               - IMPLICIT NONE AND REMOVED ILAT AND JL AS
C     *                                    THEY WERE NOT BEING USED
C     * JUN 22/2013 - M.LAZARE.      NEW VERSION FOR GCM17+:
C     *                              - ADD NEW SWITCH "IOZTGRD" WITH
C     *                                DEFAULT SET TO ZERO SO THAT
C     *                                THERE IS NO VERTICAL GRADIENT
C     *                                INTO MOON LAYER, FOR LACK OF
C     *                                A MORE ACCURATE REPRESENTATION.   
C     * MAY 13/2011 - M.LAZARE.      PREVIOUS VERSION OZON14 FOR GCM16:
C     *                              - UPDATED TO ADD IOZTYP=5 FOR COMBINED
C     *                                RANDEL/CMAM OZONE.
C     * JUL 21/2009 - M.LAZARE.      PREVIOUS VERSION OZON13 FOR GCM15I:
C     *                              - REVISE CALCULATION OF OZONE ABOVE
C     *                                MODEL TOP ("O3TOP") TO HANDLE
C     *                                CASE WHERE OZONE DATA DOESN'T
C     *                                EXIST ABOVE MODEL TOP (IE RANDEL). 
C     *                              - BUGFIX WHEN PLOC>PREF(LEVOZ).
C     *                              - UPDATED TO ADD IOZTYP=4 FOR RANDEL
C     *                                OZONE.
C     * JAN 31/2007 - M.LAZARE.      PREVIOUS VERSION OZON12 FOR GCM15G/H.
C     *                              UPDATED TO ADD IOZTYP=3 FOR FORTUIN
C     *                              AND KELDER (WITH HALOE ABOVE 
C     *                              0.3 MBS).
C     * MAY 22/2006 - M.LAZARE.      NEW VERSION FOR GCM15F:
C     *                              - WORK ARRAYS NOW LOCAL.
C     * MAY 18/2002. - M.LAZARE, J.LI. PREVIOUS VERSION OZON11 FOR GCM15C->GCM15E.
C     *                                CALCULATES NEW OUTPUT ARRAY "O3TOP",
C     *                                REPRESENTING CUMULATIVE OZONE IN
C     *                                THE MOON LAYER, USED IN ATTENUATING
C     *                                SOLAR BEAM BEFORE REACHING FIRST
C     *                                MODEL LAYER IN NEW RADIATION.

      implicit none
C
C     * OUTPUT ARRAY...

      REAL, INTENT(out) :: oz(ilg,lev)
      REAL, INTENT(out) :: o3top(ilg)

C     * INPUT

      INTEGER, INTENT(in) :: levoz, lev, il1, il2, ilg, ioztyp
      REAL, INTENT(in) :: ozzxj(ilg,levoz)
      REAL, INTENT(in) :: pressg(ilg)
      REAL, INTENT(in) :: shtj(ilg,lev), shj(ilg,lev-1)

C     * LOCAL WORK SPACE

      INTEGER :: i, k, l, lstart
      REAL :: ptop, o3moon, slope, tercep, dp
      REAL, DIMENSION(ILG) :: xa, xm, ya, ym, ploc, ris
      REAL :: pref(200)
C
C     * PRESSURE LEVELS OF INPUT OZZX FIELD USED FOR INTERPOLATION.
C
      REAL :: PREF1(37)
      DATA PREF1 /.3, .4, .5, .7, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 
     1     7.0, 10., 15., 20., 30., 40., 50., 70., 100., 150.,
     2     200., 300., 400., 500., 700., 1000., 1500., 2000., 
     3     3100., 4400., 8800., 17600., 26000., 37800., 53700., 
     4     75000., 101325./ 
C
      REAL :: PREF2(59)
      DATA PREF2/  
     * 2.84191E+01, 3.25350E+01, 3.71936E+01, 4.24615E+01, 4.84153E+01,
     * 5.51432E+01, 6.27490E+01, 7.13542E+01, 8.11039E+01, 9.21699E+01,
     * 1.04757E+02, 1.19108E+02, 1.35511E+02, 1.54305E+02, 1.75891E+02,
     * 2.00739E+02, 2.29403E+02, 2.62529E+02, 3.00884E+02, 3.45369E+02,
     * 3.97050E+02, 4.57190E+02, 5.27278E+02, 6.09073E+02, 7.04636E+02,
     * 8.16371E+02, 9.47075E+02, 1.10000E+03, 1.27894E+03, 1.48832E+03,
     * 1.73342E+03, 2.02039E+03, 2.35659E+03, 2.75066E+03, 3.21283E+03,
     * 3.75509E+03, 4.39150E+03, 5.13840E+03, 6.01483E+03, 7.04289E+03,
     * 8.24830E+03, 9.66096E+03, 1.13155E+04, 1.32514E+04, 1.55122E+04,
     * 1.81445E+04, 2.11947E+04, 2.47079E+04, 2.87273E+04, 3.32956E+04,
     * 3.84573E+04, 4.42610E+04, 5.07601E+04, 5.80132E+04, 6.60837E+04, 
     * 7.50393E+04, 8.49521E+04, 9.58981E+04, 1.00369E+05            /
C
      REAL :: PREF3(28)
      DATA PREF3/  
     &     1.,     1.5,     2.2,     3.2,     4.6,     6.8,      10.,  
     &    15.,     20.,     30.,     50.,    100.,    200.,     300.,
     &   500.,    700.,   1000.,   2000.,   3000.,   5000.,    7000.,  
     & 10000.,  15000.,  20000.,  30000.,  50000.,  70000.,  100000.  /
C
      REAL :: PREF4(24)
      DATA PREF4/
     &   100.,    150.,    200.,    300.,    500.,    700.,    1000.,
     &  1500.,   2000.,   3000.,   5000.,   7000.,   8000.,   10000.,  
     & 15000.,  20000.,  25000.,  30000.,  40000.,  50000.,   60000.,  
     & 70000.,  85000., 100000.                                     /
C
      REAL :: PREF5(45)
      DATA PREF5/
     &     0.10,    0.15,    0.20,    0.30,    0.40,
     &     0.50,    0.70,    1.00,    1.50,    2.00,
     &     3.00,    4.00,    5.00,    7.00,    10.0,
     &     15.0,    20.0,    30.0,    40.0,    50.0,
     &     70.0,    100.,    150.,    200.,    300., 
     &     500.,    700.,   1000.,   1500.,   2000.,
     &    3000.,   5000.,   7000.,   8000.,  10000.,  
     &   15000.,  20000.,  25000.,  30000.,  40000.,
     &   50000.,  60000.,  70000.,  85000., 100000.                /
C
      REAL :: PREF6(49)
      DATA PREF6/
     &     0.05,     0.10,    0.20,    0.40,    0.70,
     &     1.00,     2.00,    4.00,    7.00,    10.0,
     &     20.0,     40.0,    70.0,   100.0,   150.0,
     &    200.0,    300.0,   400.0,   500.0,   700.0,
     &   1000.0,   1500.0,  2000.0,  2500.0,  3000.0,
     &   3500.0,   4000.0,  5000.0,  6000.0,  7000.0,
     &   8000.0,   9000.0, 10000.0, 11500.0, 13000.0,
     &  15000.0,  17000.0, 20000.0, 25000.0, 30000.0,
     &  35000.0,  40000.0, 50000.0, 60000.0, 70000.0,
     &  80000.0,  85000.0, 92500.0,100000.0                 /
C
      INTEGER :: ioztgrd
      DATA IOZTGRD /0/
C----------------------------------------------------------------------
C
C     * DEFINE WORKING PRESSURE LEVEL REFERENCE VERTICAL GRID, BASED ON
C     * CHOICE OF IOZTYP.
C
      IF(IOZTYP.EQ.1)    THEN
        IF(LEVOZ.NE.37) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-1)
        ENDIF
C
        DO K=1,37
          PREF(K)=PREF1(K)
        ENDDO
      ELSE IF(IOZTYP.EQ.2) THEN
        IF(LEVOZ.NE.59) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-2)
        ENDIF
C
        DO K=1,59
          PREF(K)=PREF2(K)
        ENDDO
      ELSE IF(IOZTYP.EQ.3) THEN
        IF(LEVOZ.NE.28) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-3)
        ENDIF
C
        DO K=1,28
          PREF(K)=PREF3(K)
        ENDDO
      ELSE IF(IOZTYP.EQ.4) THEN
        IF(LEVOZ.NE.24) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-4)
        ENDIF
C
        DO K=1,24
          PREF(K)=PREF4(K)
        ENDDO
      ELSE IF(IOZTYP.EQ.5) THEN
        IF(LEVOZ.NE.45) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-5)
        ENDIF
C
        DO K=1,45
          PREF(K)=PREF5(K)
        ENDDO
      ELSE IF(IOZTYP.EQ.6) THEN
        IF(LEVOZ.NE.49) THEN
          PRINT *, '0WRONG VALUE FOR LEVOZ!: IOZTYP,LEVOZ = ',IOZTYP,LEVOZ
          CALL XIT('OZON15',-6)
        ENDIF
C
        DO K=1,49
          PREF(K)=PREF6(K)
        ENDDO
      ELSE
        CALL XIT('OZON15',-7)
      ENDIF
C
C     * LOCATE INDICES FOR INTERPOLATION AT FULL PRESSURE LEVELS. 
C
      LSTART = 1
      DO 180 L = 1, LEV 
          DO 120 I = IL1, IL2 
              RIS(I) = 0.
  120     CONTINUE
  
          DO 140 K = LSTART, LEVOZ
              DO 130 I = IL1, IL2
                 IF (L.EQ.1) THEN
                    PLOC(I)=0.5*SHTJ(I,1)*PRESSG(I)
                 ELSE
                    PLOC(I)=SHJ(I,L-1)*PRESSG(I)
                 ENDIF

                 IF ( PREF(1).GE.PLOC(I).AND.RIS(I).EQ.0.) THEN
                    RIS(I)=1.
                    XA (I)=PREF(2)
                    XM (I)=PREF(1)
                    YA (I)=OZZXJ(I,1)
                    YM (I)=OZZXJ(I,1)
                 ENDIF
                 IF ( PREF(K).GE.PLOC(I).AND.RIS(I).EQ.0. )THEN
                    RIS(I)=REAL(K)
                    XA (I)=PREF(K)
                    XM (I)=PREF(K-1)
                    YA (I)=OZZXJ(I,K)
                    YM (I)=OZZXJ(I,K-1)
                 ENDIF
                 IF ( PREF(LEVOZ).LE.PLOC(I).AND.RIS(I).EQ.0. ) THEN
                    RIS(I)=REAL(LEVOZ)
                    XA (I)=PREF(LEVOZ)
                    XM (I)=PREF(LEVOZ-1)
                    YA (I)=OZZXJ(I,LEVOZ)
                    YM (I)=OZZXJ(I,LEVOZ)
                 ENDIF
  130         CONTINUE
  140     CONTINUE
C
C         * LINEAR INTERPOLATION (VOLUME MIXING RATIO)
C
          LSTART=LEVOZ
          DO 160 I=IL1,IL2
              LSTART = MIN(LSTART, NINT(RIS(I)))
              SLOPE   = (YA(I) - YM(I)) / (XA(I) - XM(I))
              TERCEP  = YM(I) - SLOPE * XM(I)
              OZ(I,L) = 48. / 28.97 * (SLOPE * PLOC(I) + TERCEP)
  160     CONTINUE
  180 CONTINUE
C
C     * CALCULATING THE O3 AVERAGED MIXING RATIO ABOVE MODEL TOP LAYER.
C
      IF(IOZTGRD.NE.0) THEN
C
C       * USUAL CALCULATION.
C
        DO 220 I = IL1, IL2
          PTOP = SHTJ(I,1) * PRESSG(I)
C
          IF(PREF(1) .LE. PTOP)                                     THEN
            DO 210 K = 1, LEVOZ
              IF(PREF(K) .LE. PTOP)                               THEN
                IF(K .EQ. 1)                                   THEN
                  O3MOON =  OZZXJ(I,K) * PREF(K)
                ELSE
                  O3MOON =  O3MOON + 0.5 * (OZZXJ(I,K-1) +
     1                      OZZXJ(I,K)) * (PREF(K) - PREF(K-1))
                ENDIF
                DP       =  PREF(K)
              ENDIF
              O3TOP(I)   = 48. / 28.97 * O3MOON / DP
  210       CONTINUE
          ELSE
C
C           * ASSUMED LINEAR INTERPOLATION TO SMALL REPRESENTATIVE VALUE AT P=0,
C           * OBTAINED FROM OUR INITIAL DATA).
C
            O3MOON   = 0.5*(OZZXJ(I,1) + 1.E-8)
            O3TOP(I) = 48. / 28.97 * O3MOON
          ENDIF
  220   CONTINUE

      ELSE
C
C       * ASSUME ZERO VERTICAL GRADIENT FOR LACK OF DEFINITE VERTICAL STRUCTURE
C       * (NEW DEFAULT).
C
        DO 230 I = IL1, IL2
          O3TOP(I) = OZ(I,2)
  230   CONTINUE

      ENDIF
C
      RETURN
      END
