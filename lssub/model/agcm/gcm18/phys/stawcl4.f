      SUBROUTINE STAWCL4 (STAWSIJ, SHBJ,SHBJ1,SHJ,
     1                    NKM1,NK,NKP1,IL1,IL2,ILG,RGOCP) 
 
C     * SEP 06/07 - M.LAZARE.  NEW VERSION FOR GCM15F ONWARD:
C     *                        ARRAYS REAL*8 INSTEAD OF REAL TO
C     *                        SUPPORT RUNNING 64-BIT PHYSICS IN
C     *                        32-BIT DRIVER. NOTE THAT STAWCL3
C     *                        WAS IN "COMM" AND THIS VERSION MUST
C     *                        BE ACCESSIBLE IN "MODEL".        
C     * DEC 05/88 - M.LAZARE - PREVIOUS VERSION STAWCL2 UP TO GCM15E.
C  
C     * DEFINE A COLUMN OF MATRIX STAWS(NK-1,6) USED IN 
C     * MOIST CONVECTIVE ADJUSTMENT.
C     * I   = THIS LONGITUDE
C     * ILG = DIMENSION IN LONGITUDE
C     * LON = NUMBER OF DISTINCT LONGITUDES.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   SHBJ(ILG,NK), SHJ(ILG,NK), SHBJ1(ILG)
      REAL   STAWSIJ(ILG,NKM1,6)
C-----------------------------------------------------------------------
      IF(NKM1.NE.NK-1 .OR. NKP1.NE.NK+1) CALL XIT('STAWCL4',-1) 
  
      DO 10 K=2,NK-1
      DO 10 I=IL1,IL2 
        SFK        =LOG(SHBJ(I  ,K)/SHBJ(I,K-1)) 
        SFKP1      =LOG(SHBJ(I,K+1)/SHBJ(I  ,K)) 
        DELK       =     SHBJ(I  ,K)-SHBJ(I,K-1)
        DELKP1     =     SHBJ(I,K+1)-SHBJ(I  ,K)
        X          = 1.E0/(SFK  + SFKP1)
        STAWSIJ(I,K,1) = X*(SFKP1 + 2.E0*SFK  )/3.E0
        STAWSIJ(I,K,2) = X*(SFK   + 2.E0*SFKP1)/3.E0
        STAWSIJ(I,K,3) = 2.E0*X/RGOCP 
        STAWSIJ(I,K,5) = -DELKP1/DELK 
        STAWSIJ(I,K,6) =(SHBJ(I,K-1)/SHJ(I,K+1))**(1.E0/3.E0) 
   10 CONTINUE
C 
C     * FOR COMPATIBILITY WITH EARLIER STAGGERED MODEL. 
C 
      K=1 
      DO 15 I=IL1,IL2 
        SFK        =LOG(SHBJ(I  ,K)/SHBJ1(I))
        SFKP1      =LOG(SHBJ(I,K+1)/SHBJ(I  ,K)) 
        DELK       =     SHBJ(I  ,K)-SHBJ1(I) 
        DELKP1     =     SHBJ(I,K+1)-SHBJ(I  ,K)
        X          = 1.E0/(SFK  + SFKP1)
        STAWSIJ(I,K,1) = X*(SFKP1 + 2.E0*SFK  )/3.E0
        STAWSIJ(I,K,2) = X*(SFK   + 2.E0*SFKP1)/3.E0
        STAWSIJ(I,K,3) = 2.E0*X/RGOCP 
        STAWSIJ(I,K,5) = -DELKP1/DELK 
        STAWSIJ(I,K,6) =(SHBJ1(I)/SHJ(I,K+1))**(1.E0/3.E0)
   15 CONTINUE
C 
      DO 20 K=1,NK-1
      DO 20 I=IL1,IL2 
        STAWSIJ(I,K,4)=1.E0/( STAWSIJ(I,K,2) - STAWSIJ(I,K,3) 
     1                + STAWSIJ(I,K,5) *(STAWSIJ(I,K,1)+STAWSIJ(I,K,3)))
   20 CONTINUE
C 
      RETURN
C-----------------------------------------------------------------------
      END 
