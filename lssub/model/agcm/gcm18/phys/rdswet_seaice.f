      SUBROUTINE RDSWET_SEAICE(SNOW,DENS,REFF,GC,GT,
     +                         REFF0,DELT,ILG,IL1,IL2,GC_MIN,GC_MAX)

C
C     * 26 April 2018 - J. Cole -> Modified version of rdswet for sea-ice
C     *                            that estimate amount of liquid in snow.
      
C     
      IMPLICIT NONE
      
C     * INPUT SCALARS
      INTEGER ILG, IL1, IL2
      REAL DELT, GC_MIN, GC_MAX, REFF0
      
C     * INPUT ARRAYS.
      REAL, DIMENSION(ILG) :: SNOW, DENS, GC, GT

C     * INPUT/OUTPUT ARRAYS.
      REAL, DIMENSION(ILG) :: REFF

C     * INTERNAL SCALAR VARIABLES
      REAL :: FLIQ, FLIQ_MAX, MELT, HF, HC
      INTEGER :: I

C     * PARAMETERS 
      REAL, PARAMETER :: C1=4.22E-13
      REAL, PARAMETER :: CR_MIN=0.03, CR_MAX=0.1, RHO_T=200.0 ! kg/m^3
      REAL, PARAMETER :: TFREZ_LIM=273.15 ! K
      REAL, PARAMETER :: EPS=1.0E-7

      REAL :: PI, RVORD, TFREZ, HS, HV, DAYLNT
      REAL :: CSNO, CPACK, GTFSW, RKHI, SBC, SNOMAX

      COMMON /PARAM1/PI,RVORD,TFREZ,HS,HV,DAYLNT
      COMMON /PARAM3/CSNO,CPACK,GTFSW,RKHI,SBC,SNOMAX

!----------------------------------------------------------------------

! Latent heat of fusion (J kg-1) of snow or sea ice = HS-HV
      HF = HS-HV

! Heat capacity of pack ice (normalized by sqrt(2)).
! 
      HC = CPACK*SQRT(2.) 
      
      DO I=IL1,IL2

         IF(GC(I) > GC_MIN .AND. GC(I) < GC_MAX) THEN
            
            IF (SNOW(I) > 0.0) THEN

               IF (GT(I) >= (TFREZ_LIM-EPS)) THEN ! The snow is melting

! Reuse some code from oifpst10 to compute the amount of melt that can occur.

                  MELT = (GT(I)-TFREZ_LIM)*HC/HF

! Andersen, 1975 provides a simple parameterization of maximum liquid water 
! retention for a snow pack as a function of snow density.

                  IF (DENS(I) >= RHO_T) THEN
                     FLIQ_MAX = CR_MIN
                  ELSE
                     FLIQ_MAX = CR_MIN 
     +                        + (CR_MAX-CR_MIN)*(RHO_T-DENS(I))/RHO_T
                  END IF

                  FLIQ = MELT/SNOW(I)

!               IF (FLIQ > FLIQ_MAX) FLIQ=FLIQ_MAX

! As a sensitivity test set FLIQ=FLIQ_MAX since the max GT=TREZ_LIM
                  FLIQ=FLIQ_MAX

! Compute the change in grain size due to wet growth. 
                  REFF(I)=MAX(REFF0,REFF(I))
                  
                  REFF(I)=REFF(I)*1.E+6
                  
                  REFF(I)=((0.75E+18*C1/PI)*(FLIQ**3)*DELT
     1                   + REFF(I)**3)**(1./3.)
                  
                  REFF(I)=REFF(I)*1.E-6 
                  
               ENDIF  ! GT
            
            ENDIF ! SNOW

         ENDIF ! GC
         
      END DO !I
      
      RETURN
      END
