      SUBROUTINE BCCONC(ZSNOW,GCROW,BCSNO,DEPB,SPCP,ROFN,RHOSNO,
     1                  DELT,ZSNMIN,ZSNMAX,ILG,IL1,IL2,GCMIN,GCMAX)

C     * Feb 10/2015 - J.Cole.   New version for gcm18:
C                               - Truncate BC concentrations to
C                                 avoid excessively large values
C                                 that may occur under special
C                                 circumstances given the simplicity 
C                                 of the current bc/snow parameterization.
C                                 This will ensure that subsequent 
C                                 albedo calculations will be
C                                 numerically robust. 
C     * Nov 15/2013 - M.Lazare. Previous version for gcm17.
C     *                         - no SNOW (wasn't being used).
C     *                         - two IF branches combined into one
C     *                           for ZSNOW>ZSNMIN.
C     *                         - K decreased from 0.5 to 0.1 (tuning).
C     *                    
C     * Aug 06/2013 - M. Namazi &  K. V. SALZEN.
C     *              BC CONCENTRATION IN SNOW DUE TO DEPOSITION
C     *              OF BC AND SCAVENGING THROUGH MELTING.

      IMPLICIT NONE                                                             

      INTEGER ILG,IL1,IL2,I,J
      REAL, DIMENSION(ILG) :: BCSNO,GCROW,DEPB,SPCP,RHOSNO,ZSNOW,ROFN
C
      REAL  DELT,COEFF1,COEFF2,ZSNMIN,ZSNMAX,GCMIN,GCMAX,ZSN
C
      REAL, PARAMETER :: K=1.E-1, EPS1=1.E-5, DELM=1. 
C--------------------------------------------------------------------
      DO 100 I=IL1,IL2
        IF(GCROW(I).GT.GCMIN.AND.GCROW(I).LT.GCMAX)            THEN 
           IF(ZSNOW(I).GT.ZSNMIN)                            THEN
              ZSN=MIN(ZSNOW(I),ZSNMAX)
              COEFF1=ZSN+DELT*SPCP(I)/RHOSNO(I)
              BCSNO(I)=BCSNO(I)*ZSN+DELT*DEPB(I)
              BCSNO(I)=BCSNO(I)/COEFF1
              COEFF2=RHOSNO(I)*ZSN-(1-K)*DELT*DELM*ROFN(I)
              BCSNO(I)=RHOSNO(I)*ZSN*BCSNO(I)/COEFF2
C
C             * TRUNCATE BC CONCENTRATIONS TO AVOID EXCESSIVELY LARGE VALUES
C             * THAT MAY OCCUR UNDER SPECIAL CIRCUMSTANCES GIVEN THE SIMPLICITY 
C             * OF THE CURRENT BC/SNOW PARAMETERIZATION.
C             * THIS WILL ENSURE THAT SUBSEQUENT ALBEDO CALCULATIONS WILL BE
C             * NUMERICALLY ROBUST. 
C
              BCSNO(I)=MIN(BCSNO(I),1.)
           ELSE
              BCSNO(I)=0.
           ENDIF
        ENDIF
  100 CONTINUE

      RETURN
      END
