      SUBROUTINE SPWCON11 (FVORT,PII) 
C
C     * JUN 05/2013 - J.LI/     NEW VERSION FOR GCM17+:
C     *               M.LAZARE. - "ACTFRC" VARIABLE ADDED FOR SEMI-
C     *                           DIRECT EFFECT.
C     *                         - "HS" CHANGED TO BE CONSISTENT WITH
C     *                           WHAT IS USED ELSEWHERE IN CODE.
C     * NOV 14/2006 - M.LAZARE. PREVIOUS VERSION SPWCON10 FOR GCM15F+:
C     *                         - SCOPE VARIABLES USING
C     *                           IMPLICIT NONE.
C     *                         - EPSICE REMOVED (ONLY
C     *                           ONE ACTIVE WAS QMIN AND
C     *                           IS NOW DEFINED IN TRINFO4). 
C     * DEC 20/2002 - M.LAZARE. PREVIOUS VERSION SPWCON9.
C     * MAY 17/2000 - M.LAZARE. PREVIOUS VERSION SPWCON8.
  
C     * SETS CONSTANTS FOR GCM17.
C
C     * ILEVM = JUST ILEV-1.
C     *  LEVS = THE NUMBER OF PROGNOSTIC MOISTURE LEVELS. 
C     *    SH = SIGMA VALUE AT MID POINT OF THERMO. LAYERS. 
C     *   SHB = SIGMA VALUE AT BASE OF THERMO. LAYERS.
C     *    WW = EARTH ROTATION RATE (1/SEC).
C     *     A = EARTH RADIUS (M). 
C     *  GRAV = GRAVITY ACCELERATION (M/SEC**2).
C     *  RGAS = DRY AIR GAS CONSTANT (JOULE/(KG*DEG)).
C     * RGOCP = RGAS/(DRY AIR SPECIFIC HEAT)
C     * FVORT = VORTICITY OF EARTH ROTATION.
C 
      IMPLICIT NONE
C     
C     * OUTPUT PARAMETERS FOR TRANSFORMS.
C
      REAL FVORT,PII
C
C     * PARAMETERS USED IN PHYSICS (NOTE THAT PARAMS ALSO USED IN
C     * DYNAMICS!).
C
      REAL            WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1                RGASV,CPRESV
      REAL            PI,RVORD,TFREZ,HS,HV,DAYLNT
      REAL            CSNO,CPACK,GTFSW,RKHI,SBC,SNOMAX
      REAL            CONI,DENI,SICMIN,DFSIC,CONF,ACTFRC
      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1                RGASV,CPRESV
      COMMON /PARAM1/ PI,     RVORD, TFREZ, HS,   HV,   DAYLNT
      COMMON /PARAM3/ CSNO,   CPACK, GTFSW, RKHI, SBC,  SNOMAX
      COMMON /PARAM4/ ACTFRC
      COMMON /PARAM5/ CONI,   DENI,  SICMIN,DFSIC,CONF
C 
C     * PARAMETERS USED BY  FUNCTION  HTVOCP. 
C 
      REAL          T1S,T2S,AI,BI,AW,BW,SLP 
      COMMON /HTCP/ T1S,T2S,AI,BI,AW,BW,SLP 
C 
C     * PARAMETERS USED BY  FUNCTIONS  DEWPNT,  SPCHUM,  DELTAQ.
C 
      REAL         A,B,EPS1,EPS2
      COMMON /EPS/ A,B,EPS1,EPS2 
C 
C     * PARAMETERS USED BY  FUNCTION  GAMSAT. 
C 
      REAL          EPSS,CAPA
      COMMON /GAMS/ EPSS,CAPA 
C 
C     * PARAMETERS USED IN MOIST CONVECTIVE ADJUSTMENT. 
C
      REAL            HC,HF,HM,AA,DEPTH
      INTEGER         LHEAT,MOIADJ,MOIFLX  
      COMMON /ADJPCP/ HC,HF,HM,AA,DEPTH,LHEAT,MOIADJ,MOIFLX 
C
C     * PARAMETERS USED IN NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      REAL           RW1,RW2,RW3,RI1,RI2,RI3
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * INTERNAL PARAMETERS.
C
      REAL RAUW,CP
C
C     * DATA STATEMENTS FOR PARAM1,PARAM3,PARAM5 ORIGINALLY IN THE
C     * PHYSICS.
C
      DATA PI / 3.1415926535898 /

      DATA    RVORD      ,TFREZ    ,HS       ,HV       ,DAYLNT
     1/       0.622      ,273.16   ,2.835E6  ,2.501E6  ,86400.   /

      DATA    CSNO       ,CPACK    ,GTFSW    ,RKHI
     1/       9.6E4      ,2.1E5    ,271.2    ,0.7      /

      DATA    SBC        ,SNOMAX   
     1/       5.66796E-8 ,10.      /

      DATA    CONI       ,DENI     ,SICMIN   ,DFSIC
     1/       2.25       ,913.     ,45.0     ,0.05     /

      DATA    CONF
     1/       1.00       /

      DATA    ACTFRC / 0.9 /
C-----------------------------------------------------------------------
      PII    =PI
      WW     =7.292E-5
      TW     =WW+WW 
      RAYON  =6.37122E06
      ASQ    =RAYON*RAYON 
      GRAV   =9.80616 
      RGAS   =287.04
      RGOCP  =2./7. 
      RGOASQ =RGAS/ASQ
      CPRES  =RGAS/RGOCP
      CPRESV =1870. 
      RGASV  =461.50
      FVORT  =TW*SQRT(2./3.)
C 
      RAUW   =1.E+3 
      GRAV   =9.80616 
      DEPTH  = 1./(RAUW*GRAV) 
      CP     =1004.5
      CAPA   =RGOCP 
      T1S    =273.16
      T2S    =233.16
      AW     =3.15213E+6/CP 
      BW     =2.38E+3/CP
      AI     =2.88053E+6/CP 
      BI     =0.167E+3/CP 
      A      =21.656
      B      =5418. 
C     AICE   =24.292
C     BICE   =6141. 
C     TICE   =233.16
C     QMIN   =1.E-40
      SLP    =1./(T1S-T2S)
      EPS1   =0.622 
      EPS2   =0.378 
      EPSS   =EPS1
C 
      HC     =.95 
      HF     =.95 
      HM     =.95 
      LHEAT  =1 
      MOIADJ =1 
      MOIFLX =1 
      AA     =0.0 
      IF(HM.LT.1.)  AA=1./(6.*(1.-HM)**2) 
C
C     * ADDED CONSTANTS FOR KNUT'S NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      RW1 = 53.67957
      RW2 = -6743.769
      RW3 = -4.8451
      RI1 = 23.33086
      RI2 = -6111.72784
      RI3 = 0.15215
C      
      RETURN
C-----------------------------------------------------------------------
      END
