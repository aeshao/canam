      SUBROUTINE RDSDPS(SNOW,ZSNOW,SPCP,RHOSNO,REFF,GCROW,
     1                  REFF0,DELT,ZSNMIN,ZSNMAX,ILG,IL1,IL2,GCMIN,
     2                  GCMAX) 
C
C     * M. Namazi &  K. V. SALZEN,SNOW GRAIN DUE TO DEPOSITION.
C 
      IMPLICIT NONE

      REAL :: REFF0
      INTEGER ILG,IL1,IL2,I
C
C     * INPUT/OUTPUT ARRAYS.
C
      REAL, DIMENSION(ILG) :: SNOW,ZSNOW,SPCP,RHOSNO,REFF,GCROW,TAUD
      REAL, PARAMETER :: SPCMIN=1.157407407E-8,
     1                   TAUINF=1.E+20      
      REAL  DELT,ZSNMIN,ZSNMAX,GCMIN,GCMAX 
C------------------------------------------------------------------------------
      DO 100 I=IL1,IL2
          IF(GCROW(I).GT.GCMIN.AND.GCROW(I).LT.GCMAX)              THEN 
             IF(SNOW(I).GT.0.)                                    THEN
                REFF(I)=MAX(REFF0,REFF(I))
                IF(SPCP(I).GT.SPCMIN)                   THEN
                  IF(ZSNOW(I).LT.ZSNMAX.AND.ZSNOW(I).GT.ZSNMIN)   THEN
                     TAUD(I)=(ZSNOW(I)*RHOSNO(I))/SPCP(I)
                     REFF(I)=REFF0+(REFF(I)-REFF0)*EXP(-DELT/TAUD(I))
                  ELSEIF(ZSNOW(I).GT.ZSNMAX)                      THEN
                     TAUD(I)=(ZSNMAX*RHOSNO(I))/SPCP(I)
                     REFF(I)=REFF0+(REFF(I)-REFF0)*EXP(-DELT/TAUD(I))
                  ELSE
                     TAUD(I)=0.
                     REFF(I)=0.  
                  ENDIF
                ELSE
                  TAUD(I)=TAUINF 
                ENDIF
             ELSE
                TAUD(I)=0. 
                REFF(I)=0.
             ENDIF
          ENDIF
  100 CONTINUE 

      RETURN
      END 
