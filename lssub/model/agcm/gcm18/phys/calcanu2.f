      SUBROUTINE CALCANU2(ANU,                                  ! OUTPUT
     1                    CLD, QCWVAR, IL1, IL2, ILG, LAY, LEV, ! INPUT
     2                    MCICA)                                ! INPUT

!     * FEB 13/2008 - JASON COLE. NEW VERSION FOR GCM15H:
!     *                           NO CLOUD BLOCK MINIMUM ANU IF MCICA.
!     * DEC 05/2007 - JASON COLE. PREVIOUS VERSION CALCANU FOR GCM15G:
!     *                           CALCULATES CLOUD INHOMOGENEITY
!     *                           FACTOR "ANU".
!     *

      IMPLICIT NONE
!
!     * OUTPUT DATA
!
      REAL, DIMENSION(ILG,LAY), INTENT(OUT) ::  
     1 ANU(ILG,LAY) 
!
!     * INPUT DATA
!
      REAL, DIMENSION(ILG,LAY), INTENT(IN) :: 
     1 CLD,    ! Cloud amount
     2 QCWVAR  ! Variance of total cloud water

      INTEGER, INTENT(IN) ::
     1 IL1,
     2 IL2,
     3 ILG,
     4 LAY,
     5 LEV,
     6 MCICA
!
! LOCAL DATA
!
      INTEGER :: ! Loop counters
     1 IL,
     2 K,
     3 KM1,
     4 KP1,
     5 LAYM1
!
! PARAMETERS/DATA
!
      REAL, PARAMETER ::
     1 CUT    = 0.001 ! MUST BE CONSISTENT WITH VALUE IN RADIATION
!--------------------------------------------------------------------
!     * COMPUTE THE LAYER BY LAYER VALUES OF ANU
!
      DO K = 1, LAY
         DO IL = IL1, IL2
            IF (CLD(IL,K) .GE. CUT )                                THEN
               ANU(IL,K)=1.0/MIN(MAX(0.25,QCWVAR(IL,K)),2.0)
            ELSE
               ANU(IL,K) = 1000.0
            ENDIF            
         END DO ! IL
      END DO ! K

      IF (MCICA .EQ. 0) THEN      
!===================================================================
!       * FOLLOWING NEEDED FOR THE 1D RADIATION CODE OF LI (NON_MCICA!)
!       * MINIMUM ANU, IT IS EXTREMELY IMPORTANT TO ENSURE CONSISTENCY
!       * BETWEEN THE DEFINITIONS OF ANU HERE AND THEIR SUBSEQUENT USE IN
!       * LWTRAN4
!===================================================================

!       * COMPUTE THE MINIMUM VALUE OF ANU FOR A CLOUD BLOCK
!       * GOING FROM TOP TO BOTTOM

        DO K = 1, LAY
          KM1 = K - 1
          DO IL = IL1, IL2
            IF (CLD(IL,K) .LT. CUT)                                 THEN
               ANU(IL,K) =  1000.0
            ELSE
               IF (K .GT. 1)                                        THEN 
                  ANU(IL,K)  =  MIN (ANU(IL,KM1), ANU(IL,K))
               ENDIF
            END IF
          END DO ! IL
        END DO ! K
      
!       * COMPUTE THE MINIMUM VALUE OF ANU FOR A CLOUD BLOCK
!       * GOING FROM BOTTOM TO TOP

        LAYM1=LAY-1
        DO K = LAYM1, 1, -1
          KP1 = K + 1
          DO IL = IL1, IL2
            IF (CLD(IL,K) .GE. CUT)                                 THEN
               ANU(IL,K)  =  MIN (ANU(IL,KP1), ANU (IL,K))
            ENDIF
          END DO ! IL
        END DO ! K
      END IF ! MCICA 

      RETURN
      END
