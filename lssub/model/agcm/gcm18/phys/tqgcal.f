      SUBROUTINE TQGCAL (TSG,QSG,TSH,QSH,SG,SH,
     1                   ILEV,ILG,IS,IF)
C 
C     * JUN 24/2014 - M.LAZARE. DERIVED FROM TGCAL2.
C
C     * SET TEMPERATURE AND MOISTURE ARRAYS FOR PHYSICS ON MOMENTUM LEVELS. 
C     * FOR HYBRID VERSION OF MODEL.
C     * SGB ARE MOMENTUM LAYER INTERFACES,
C     * SG  ARE MOMENTUM MID LAYER POSITIONS. 
C     * SH  ARE THERMODYNAMICS MID LAYER POSITIONS. 
C
      IMPLICIT NONE
C
C     * CONTROL PARAMETERS.
C
      INTEGER IS,IF,ILG,ILEV,I,L
C
C     * OUTPUT.
C
      REAL   TSG(ILG,ILEV),  QSG(ILG,ILEV) 
C
C     * INPUT.
C
      REAL   TSH(ILG,ILEV),  QSH(ILG,ILEV)
      REAL    SG(ILG,ILEV),   SH(ILG,ILEV) 
C-----------------------------------------------------------------------
      DO I=IS,IF
        TSG(I,1)=TSH(I,1) 
        QSG(I,1)=QSH(I,1)
      ENDDO
C 
      DO L=2,ILEV 
      DO I=IS,IF
        TSG(I,L)   = (TSH(I,L-1)*LOG(SH(I  ,L)/SG(I  ,L))
     1               +TSH(I,L  )*LOG(SG(I  ,L)/SH(I,L-1))) 
     2                          /LOG(SH(I  ,L)/SH(I,L-1))
C
        QSG(I,L)   = (QSH(I,L-1)*LOG(SH(I  ,L)/SG(I  ,L))
     1               +QSH(I,L  )*LOG(SG(I  ,L)/SH(I,L-1))) 
     2                          /LOG(SH(I  ,L)/SH(I,L-1))
      ENDDO
      ENDDO
C 
      RETURN
C-----------------------------------------------------------------------
      END 
