      FUNCTION KF3(RK,ARG)
C
C     * JUN 15/2013 - M.LAZARE.   NEW VERSION FOR GCM17+:
C     *                           - USES {RF3,RD3} INSTEAD OF {RF2,RD2}.
C     * SEP 11/2006 - F.MAJAESS.  PREVIOUS VERSION KF2, HARD-CODING
C     *                           REAL*8.
C     * MAR 24/1999 - J SCINOCCA. ORIGINAL VERSION KF IN "COMM".
C
C     * FUNCTION CALLED BY GWLOOKU 
C     * USED TO EVALUATE INCOMPLETE ELLIPTIC INTEGRALS
C     * BASED ON ELLF OF NUMERICAL RECIPES
C
      IMPLICIT NONE
      REAL*8 RK,ARG,KF3
      REAL*8 X,Y,Z,RKSQ

      REAL*8 RF3
      EXTERNAL RF3
C==============================================================
      RKSQ=RK**2

      X=COS(ARG)**2
      Y=1.E0-RKSQ*SIN(ARG)**2
      Z=1.E0

      KF3=SIN(ARG)*RF3(X,Y,Z)

      RETURN
      END
