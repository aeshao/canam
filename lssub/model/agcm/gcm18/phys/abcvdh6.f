      SUBROUTINE ABCVDH6 (A,B,C,CL, CDVLH,GRAV,IL1,IL2,ILG,ILEV,LEV,
     1                    RGAS,RKH,SHTJ,SHTXKJ,SHJ,SHXKJ,DSHJ,
     2                    THL,TF,TODT)
  
C     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS. 
C     * MAR 14/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H. 
  
C     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX FOR
C     * THE IMPLICIT VERTICAL DIFFUSION OF HEAT IN HYBRID VERSION OF
C     * MODEL. A IS THE LOWER DIAGONAL, B IS THE MAIN DIAGONAL, 
C     * AND C IS THE UPPER DIAGONAL.
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      REAL   A(ILG,ILEV),B(ILG,ILEV),C(ILG,ILEV),RKH(ILG,ILEV)
      REAL   TF(ILG,ILEV),THL(ILG),CDVLH(ILG),CL(ILG) 
      REAL   SHTJ (ILG,LEV), SHJ   (ILG,ILEV),DSHJ(ILG,ILEV)
      REAL   SHXKJ(ILG,ILEV),SHTXKJ(ILG,ILEV) 
C-----------------------------------------------------------------------
      DO 50 I=IL1,IL2 
         OVDS  = SHTXKJ(I,2)*(GRAV*SHTJ(I,2)/RGAS)**2 
     1          /( DSHJ(I,1) * (SHJ(I,2)-SHJ(I,1)) )
         C(I,1)= OVDS*RKH(I,2)*(1./TF(I,2))**2
   50 CONTINUE
  
      DO 75 I=IL1,IL2 
         B(I,1) = -C(I,1)/SHXKJ(I,1)
         C(I,1) =  C(I,1)/SHXKJ(I,2)
   75 CONTINUE
  
      DO 100 L=2,ILEV 
      DO 100 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,L)/RGAS)**2
     1           /( (SHJ(I,L)-SHJ(I,L-1)) * DSHJ(I,L) ) 
         A(I,L) = OVDS*(1./TF(I,L))**2*RKH(I,L) 
  100 CONTINUE
  
      DO 150 L=2,ILEV-1 
      DO 150 I=IL1,IL2
         D     = DSHJ(I,L+1) / DSHJ(I,L)
         C(I,L)= A(I,L+1)*D 
  150 CONTINUE
  
      DO 200 L=2,ILEV-1 
      DO 200 I=IL1,IL2
         B(I,L) =-( C(I,L)*SHTXKJ(I,L+1)
     1             +A(I,L)*SHTXKJ(I,  L)) / SHXKJ(I,  L)
         A(I,L) =   A(I,L)*SHTXKJ(I,  L)  / SHXKJ(I,L-1)
         C(I,L) =   C(I,L)*SHTXKJ(I,L+1)  / SHXKJ(I,L+1)
  200 CONTINUE
  
      L=ILEV
      DO 250 I=IL1,IL2
         A(I,L) = A(I,L)*SHTXKJ(I,L)
         CL(I)  = GRAV*SHJ(I,L)*CDVLH(I)/(RGAS*THL(I)*DSHJ(I,L))
  250 CONTINUE
  
      DO 300 I=IL1,IL2
         B(I,L) = -(A(I,L)+CL(I)) / SHXKJ(I,  L)
         A(I,L) =   A(I,L)        / SHXKJ(I,L-1)
  300 CONTINUE
  
C     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).
  
      DO 500 L=1,ILEV-1 
      DO 500 I=IL1,IL2
         A(I,L+1) = -TODT*A(I,L+1)
         C(I,L  ) = -TODT*C(I,L  )
  500 CONTINUE
  
      DO 550 L=1,ILEV 
      DO 550 I=IL1,IL2
         B(I,L  ) = 1. -TODT*B(I,L  ) 
  550 CONTINUE
  
      RETURN
      END 
