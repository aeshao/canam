      SUBROUTINE CLD_GENERATOR3(CIC_SUB, CLW_SUB, NCLDY,
     1                          ZF, AVG_CF, AVG_QLW, AVG_QIW,
     2                          RLC_CF, RLC_CW, SIGMA_QCW,
     3                          ILG, IL1, IL2, LAY, NX_LOC,
     4                          ALPHA, RCORR, ISEEDROW,
     5                          I_LOC, L_CLD, IPPH, IOVERLAP)

C      * APR 30/2012 - JASON COLE.   NEW VERSION FOR GCM16:
C      *                             - REMOVE {X,X1,X2,Y,Y1,Y2} IN
C      *                               CALL (FROM CLD_GEN_DRIVER_)
C      *                               SINCE ONLY USED INTERNALLY
C      *                               AS ALREADY DECLARED.
C      * DEC 12/2007 - JASON COLE.   PREVIOUS VERSION CLD_GENERATOR2
C      *                             FOR GCM15G/H/I:
C      *                             - HANDLE 3 DIFFERENT OVERLAP
C      *                               METHODS (DEFINED USING IOVERLAP).
C      *                             - REMOVE CODE RELATED TO SETTING
C      *                               LPPH AND IMAXRAN SINCE NOW USING
C      *                               IPPH AND IOVERLAP.
C      * JAN 09/2007 - JASON COLE.   PREVIOUS VERSION CLD_GENERATOR FOR GCM15F.
! --------------------------------------------------------------------
! --------------------------------------------------------------------
! Generate a NX column by LAY layer cloud field.  A
! Method and model evaluation is described in:
! Raisanen et al, 2004, Stochastic generation of subgrid-scale cloudy 
! columns for large-scale models,
! Quart. J. Roy. Meteorol. Soc., 2004 , 130 , 2047-2068
! Input profiles must be from top of the model to the bottom as is the output.
! --------------------------------------------------------------------   
! This version only generates the cloud overlap using the MRO used in the CCCma
! shortwave code

      IMPLICIT NONE

! Note: LAY    => Number of layers in GCM
!       ILG    => Number of GCM columns
!       NX_LOC => Number of columns to generate

!
! PARAMETERS
!

      REAL, PARAMETER ::
     1 CUT = 0.001
      
      INTEGER, PARAMETER ::
     1 N1 = 1000,
     2 N2 = 140

!
! INPUT DATA
!

      REAL, INTENT(IN) :: 
     1 ZF(ILG,LAY),            ! Full-level (layer midpoint) altitude (km)
     2 AVG_CF(ILG,LAY),        ! Cloud amount for each layer
     3 AVG_QLW(ILG,LAY),       ! Cloud mean liquid water mixing ratio (g/m^3)
     4 AVG_QIW(ILG,LAY),       ! Cloud mean ice mixing ratio          (g/m^3)
     5 SIGMA_QCW(ILG,LAY),     ! Normalized cloud condensate std. dev.
     6 RLC_CF(ILG,LAY),        ! Cloud fraction decorrelation length
     7 RLC_CW(ILG,LAY)         ! Cloud condensate decorrelation length

      INTEGER, INTENT(IN) :: ! Counters and array sizes
     1 ILG,
     2 IL1,
     3 IL2,
     4 LAY,
     5 NX_LOC

       INTEGER, INTENT(IN) ::  
     1 IOVERLAP,               ! Overlap flag
     2 IPPH                    ! Plane-parallel homogeneous flag

! 
! OUTPUT DATA
!

      REAL,INTENT(OUT) ::
     1 CIC_SUB(ILG,LAY,NX_LOC),     ! Column ice water mixing ratio profile    (g/m^3)
     2 CLW_SUB(ILG,LAY,NX_LOC)      ! Column liquid water mixing ratio profile (g/m^3)

      INTEGER, INTENT(OUT) ::
     1 NCLDY(ILG)                   ! Number of cloudy subcolumns

!
! WORK ARRAYS
!
      
      REAL(8) :: ! Random number vectors
     1 X(ILG),
     2 Y(ILG),
     3 X1(ILG),
     4 Y1(ILG),
     5 X2(ILG),
     6 Y2(ILG)

      REAL :: 
     1 ALPHA(ILG,LAY),         ! Fraction of maximum/random cloud overlap 
     2 RCORR(ILG,LAY)          ! Fraction of maximum/random cloud condensate overlap

      INTEGER(4) :: 
     1 ISEEDROW(ILG,4) ! Seed for kissvec RNG

      INTEGER ::
     1 I_LOC(ILG) ! Place the new subcolumns into the arrays starting from the front

      LOGICAL ::
     1 L_CLD(ILG)

!
! LOCAL DATA (SCALARS)
!

      INTEGER ::
     1 IL,         ! Counter over ILG GCM columns
     2 I,          ! Counter
     2 K,          ! Counter over LAY vertical layers
     3 K_TOP,      ! Index of top most cloud layer
     4 K_BASE,     ! Index of lowest cloud layer
     5 IND1,       ! Index in variability calculation 
     6 IND2        ! Index in variability calculation

      REAL ::      
     1 RIND1,      ! Real index in variability calculation 
     2 RIND2,      ! Real index in variability calculation
     3 ZCW         ! Ratio of cloud condensate miximg ratio for 
                   ! this cell to its layer cloud-mean value

      LOGICAL ::
     1 L_TOP,
     2 L_BOT

!
! COMMON DATA
!
      REAL ::
     1 XCW(N1,N2)

      COMMON /XCWDATA/ XCW

! Initialize the arrays

      DO IL = IL1, IL2
         I_LOC(IL) = 1
         L_CLD(IL) = .FALSE.
      END DO

! Find uppermost cloudy layer

      L_TOP = .FALSE.
      K_TOP = 0
      DO K=1,LAY
         DO IL = IL1, IL2
            K_TOP = K
            IF (AVG_CF(IL,K) .GT. CUT) L_TOP = .TRUE.
         END DO ! IL
         IF (L_TOP) EXIT
      END DO ! K

! If no cloudy layers in any GCM column in this group, exit

      IF (K_TOP .EQ. 0) THEN
         RETURN
      END IF

! Find lowermost cloudy layer

      L_BOT = .FALSE.

      K_BASE = 0
      DO K=LAY,1,-1
         DO IL = IL1, IL2
            K_BASE = K
            IF (AVG_CF(IL,K) .GE. CUT) L_BOT = .TRUE.
         END DO ! IL
         IF (L_BOT) EXIT
      END DO ! K

! Calculate overlap factors ALPHA for cloud fraction and RCORR for cloud
! condensate based on layer midpoint distances and decorrelation depths
    
      IF (IOVERLAP .EQ. 2) THEN   ! Using generalized overlap
       DO K = K_TOP, K_BASE-1
          DO IL = IL1, IL2
             IF (RLC_CF(IL,K) .GT. 0.0) THEN
                ALPHA(IL,K) = EXP(-(ZF(IL,K) - ZF(IL,K+1)) 
     1                      / RLC_CF(IL,K))
             ELSE
                ALPHA(IL,K) = 0.0
             END IF
             IF (RLC_CW(IL,K) .GT. 0.0) THEN
                RCORR(IL,K) = EXP(-(ZF(IL,K) - ZF(IL,K+1)) 
     1                      / RLC_CW(IL,K))
             ELSE
                RCORR(IL,K) = 0.0
             END IF
          END DO ! IL
       END DO ! K
      ENDIF

! Although it increases the size of the code I will use an if-then-else appraoch to 
! do the different version of cloud overlap 
! It should make it easier for people to follow what is going on.
! IOVERLAP = 0, the overlap is maximum-random
! IOVERLAP = 1, the overlap is maximum-random following the definition given in Geleyn and Hollingsworth
! IOVERLAP = 2, the overlap is generalized overlap

      IF (IOVERLAP .EQ. 0) THEN

       DO I=1,NX_LOC

! Generate all subcolumns for latitude chain

          DO K = K_TOP, K_BASE
               
             IF (K .EQ. K_TOP) THEN
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       X,IL1,IL2,ILG)
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       Y,IL1,IL2,ILG)

             END IF

             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    X1,IL1,IL2,ILG)
             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    Y1,IL1,IL2,ILG)
             
             DO IL = IL1, IL2
                
                IF (AVG_CF(IL,K-1) .EQ. 0.0) THEN !It is clear above
                   X(IL) = X1(IL)
                   Y(IL) = Y1(IL)
                END IF
! Treatment of cloudy cells

                IF (X(IL) .LT. AVG_CF(IL,K)) THEN ! Generate cloud in this layer
                   
                  IF (IPPH .EQ. 0) THEN ! Homogeneous clouds
                     ZCW = 1.0
                  ELSE
! Horizontally variable clouds:
! Determine ZCW = ratio of cloud condensate miximg ratio QC for this cell to
! its mean value for all cloudy cells in this layer.
! Use bilinear interpolation of ZCW tabulated in array XCW as a function of
!    * cumulative probability Y
!    * relative standard deviation SIGMA
! Take care that the definition of RIND2 is consistent with subroutine
! TABULATE_XCW

                     RIND1 = Y(IL) * (N1 - 1) + 1.0
                     IND1  = MAX(1, MIN(INT(RIND1), N1-1))
                     RIND1 = RIND1 - IND1
                     RIND2 = 40.0 * SIGMA_QCW(IL,K) - 3.0
                     IND2  = MAX(1, MIN(INT(RIND2), N2-1))
                     RIND2 = RIND2 - IND2
                     
                   ZCW = (1.0-RIND1) * (1.0-RIND2) * XCW(IND1,IND2)
     1                 + (1.0-RIND1) * RIND2       * XCW(IND1,IND2+1)
     2                 + RIND1 * (1.0-RIND2)       * XCW(IND1+1,IND2)
     3                 + RIND1 * RIND2             * XCW(IND1+1,IND2+1)
                  END IF

! A horizontally constant IWC/LWC ratio is assumed for each layer so far
                   L_CLD(IL)             = .TRUE.
                   CLW_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QLW(IL,K)
                   CIC_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QIW(IL,K)
                END IF
                
             END DO           ! IL
          END DO              ! K
          
! Need to check if a cloudy subcolumn was generated
          DO IL = IL1, IL2
             IF (L_CLD(IL)) THEN
                I_LOC(IL) = I_LOC(IL) + 1
                L_CLD(IL) = .FALSE.
             END IF
          END DO
       END DO                 ! I
      ELSEIF (IOVERLAP .EQ. 1) THEN
       DO I=1,NX_LOC

! Generate all subcolumns for latitude chain

          DO K = K_TOP, K_BASE
               
             IF (K .EQ. K_TOP) THEN
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       X,IL1,IL2,ILG)
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       Y,IL1,IL2,ILG)
             END IF

             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    X1,IL1,IL2,ILG)
             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    Y1,IL1,IL2,ILG)             
             
             DO IL = IL1, IL2
                IF (X(IL) .LT. 1.0-AVG_CF(IL,K-1)) THEN !It is clear above
                   X(IL) = X1(IL) * (1.0 - AVG_CF(IL,K-1))
                   Y(IL) = Y1(IL)
                END IF
                  
! Treatment of cloudy cells

                IF (X(IL) .LT. AVG_CF(IL,K)) THEN ! Generate cloud in this layer
                   
                  IF (IPPH .EQ. 0) THEN ! Homogeneous clouds
                     ZCW = 1.0
                  ELSE
! Horizontally variable clouds:
! Determine ZCW = ratio of cloud condensate miximg ratio QC for this cell to
! its mean value for all cloudy cells in this layer.
! Use bilinear interpolation of ZCW tabulated in array XCW as a function of
!    * cumulative probability Y
!    * relative standard deviation SIGMA
! Take care that the definition of RIND2 is consistent with subroutine
! TABULATE_XCW

                     RIND1 = Y(IL) * (N1 - 1) + 1.0
                     IND1  = MAX(1, MIN(INT(RIND1), N1-1))
                     RIND1 = RIND1 - IND1
                     RIND2 = 40.0 * SIGMA_QCW(IL,K) - 3.0
                     IND2  = MAX(1, MIN(INT(RIND2), N2-1))
                     RIND2 = RIND2 - IND2

                   ZCW = (1.0-RIND1) * (1.0-RIND2) * XCW(IND1,IND2)
     1                 + (1.0-RIND1) * RIND2       * XCW(IND1,IND2+1)
     2                 + RIND1 * (1.0-RIND2)       * XCW(IND1+1,IND2)
     3                 + RIND1 * RIND2             * XCW(IND1+1,IND2+1)
                  END IF
                   
! A horizontally constant IWC/LWC ratio is assumed for each layer so far
                   L_CLD(IL)             = .TRUE.
                   CLW_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QLW(IL,K)
                   CIC_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QIW(IL,K)
                END IF
                
             END DO           ! IL
          END DO              ! K

! Need to check if a cloudy subcolumn was generated
          DO IL = IL1, IL2
             IF (L_CLD(IL)) THEN
                I_LOC(IL) = I_LOC(IL) + 1
                L_CLD(IL) = .FALSE.
             END IF
          END DO
       END DO                 ! I
      ELSEIF (IOVERLAP .EQ. 2) THEN
       DO I=1,NX_LOC

! Generate all subcolumns for latitude chain

          DO K = K_TOP, K_BASE
             
             IF (K .EQ. K_TOP) THEN
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       X,IL1,IL2,ILG)
                CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                       ISEEDROW(1,3),ISEEDROW(1,4),
     2                       Y,IL1,IL2,ILG)
             END IF
             
             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    X1,IL1,IL2,ILG)
             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    Y1,IL1,IL2,ILG)

             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    X2,IL1,IL2,ILG)
             CALL KISSVEC(ISEEDROW(1,1),ISEEDROW(1,2),
     1                    ISEEDROW(1,3),ISEEDROW(1,4),
     2                    Y2,IL1,IL2,ILG)

             DO IL = IL1, IL2
                
                IF (X1(IL) .GT. ALPHA(IL,K-1)) X(IL) = X2(IL)
                IF (Y1(IL) .GT. RCORR(IL,K-1)) Y(IL) = Y2(IL)
                  
! Treatment of cloudy cells

                IF (X(IL) .LT. AVG_CF(IL,K)) THEN ! Generate cloud in this layer
                   
                  IF (IPPH .EQ. 0) THEN ! Homogeneous clouds
                     ZCW = 1.0
                  ELSE
! Horizontally variable clouds:
! Determine ZCW = ratio of cloud condensate miximg ratio QC for this cell to
! its mean value for all cloudy cells in this layer.
! Use bilinear interpolation of ZCW tabulated in array XCW as a function of
!    * cumulative probability Y
!    * relative standard deviation SIGMA
! Take care that the definition of RIND2 is consistent with subroutine
! TABULATE_XCW

                    RIND1 = Y(IL) * (N1 - 1) + 1.0
                    IND1  = MAX(1, MIN(INT(RIND1), N1-1))
                    RIND1 = RIND1 - IND1
                    RIND2 = 40.0 * SIGMA_QCW(IL,K) - 3.0
                    IND2  = MAX(1, MIN(INT(RIND2), N2-1))
                    RIND2 = RIND2 - IND2
                      
                    ZCW = (1.0-RIND1) * (1.0-RIND2) * XCW(IND1,IND2)
     1                  + (1.0-RIND1) * RIND2       * XCW(IND1,IND2+1)
     2                  + RIND1 * (1.0-RIND2)       * XCW(IND1+1,IND2)
     3                  + RIND1 * RIND2             * XCW(IND1+1,IND2+1)
                 END IF
                   
! A horizontally constant IWC/LWC ratio is assumed for each layer so far
                   L_CLD(IL)             = .TRUE.
                   CLW_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QLW(IL,K)
                   CIC_SUB(IL,K,I_LOC(IL)) = ZCW * AVG_QIW(IL,K)
                END IF
                
             END DO           ! IL
          END DO              ! K

! Need to check if a cloudy subcolumn was generated
          DO IL = IL1, IL2
             IF (L_CLD(IL)) THEN
                I_LOC(IL) = I_LOC(IL) + 1
                L_CLD(IL) = .FALSE.
             END IF
          END DO
       END DO                 ! I
      ELSE
       WRITE(*,*) "Invalid value for IOVERLAP = ",IOVERLAP,"."
       WRITE(*,*) "Valid values are 0, 1, and 2."
       WRITE(*,*) "Stopping program in CLD_GENERATOR."
       STOP
      END IF ! IOVERLAP

! Record the number of cloudy subcolumns generated
      DO IL = IL1, IL2
         NCLDY(IL) = I_LOC(IL)-1
      END DO ! IL

      RETURN
      END 
