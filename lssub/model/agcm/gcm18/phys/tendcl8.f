      SUBROUTINE TENDCL8 (ESTG,ESG,RMROW,
     1                    ILG,IL1,IL2,ILEV,LEVS,
     2                    ZTMST,MOIST,SREF,SPOW)

C     * MAY 24/2017 - M.LAZARE. FOR CONVERSION TO NEW XC40:
C     *                         - REPLACE IBM INTRINSICS BY GENERIC
C     *                           FOR PORTABILITY.
C     * APR 27/2012 - M.LAZARE. NEW VERSION FOR GCM16:
C     *                         ENSURE MIXING RATIO ("RMROW") IS
C     *                         POSIIVE-DEFINITE BEFORE CONVERSION
C     *                         TO HYBRID, BY IMPOSING "TINY" VALUE.
C     * OCT 13/2007 - M.LAZARE. PREVIOUS VERSION TENDCL7 FOR GCM15G/H/I:
C     *                         - ZTMST (2.*DELT OR DELT)
C     *                           PASSED IN INSTEAD OF
C     *                           DELT AND USED ACCORDINGLY. 
C     *                         - TEMPERATURE REMOVED.
C     *                         - "RMOON" REMOVED.
C     * SEP 13/2006 - M.LAZARE/ PREVIOUS VERSION TENDCL6 FOR GCM15F:
C     *               F.MAJAESS.- WORK ARRAYS NOW LOCAL.
C     *                         - UNUSED AH,BH VARIABLES AND
C     *                           EPS COMMON BLOCK REMOVED.  
C     *                         - UNUSED PRESPA REMOVED.
C  
C     * CALCULATE TENDENCIES ESTG FROM ADJUSTED RMROW
C     * AND ORIGINAL VALUES ESG OF MODEL MOISTURE VARIABLE. 
C     * ALSO UPDATE ORIGINAL COPIES.
C     * MOIST CONTROLS THE TYPE OF MOISTURE VARIABLE. 
C     * RMROW CORRESPONDS TO SPECIFIC HUMIDITY. 
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL,    INTENT(OUT),   DIMENSION(ILG,LEVS) :: ESTG
      REAL,    INTENT(IN),    DIMENSION(ILG,LEVS) :: ESG
      REAL  ,  INTENT(IN),    DIMENSION(ILG,ILEV) :: RMROW
      REAL     ZTMST,RZTMST
C  
C     * WORK ARRAYS.
C
      REAL,                   DIMENSION(ILG,ILEV) :: FACT,EFACT  
      REAL R,RLIM
C
      INTEGER*4 LEN
      INTEGER MACHINE,INTSIZE
C
C     * COMMON BLOCK TO HOLD WORD SIZE.
C
      COMMON /MACHTYP/ MACHINE,INTSIZE
C-----------------------------------------------------------------------
      IF(LEVS.GT.ILEV)        CALL XIT('TENDCL8',-1) 
      RZTMST=1./ZTMST
C  
C     * PROCESS MOISTURE UNDER CONTROL OF MOIST.
C
      MSG=ILEV-LEVS  
C
C     * EVALUATE SMALLEST POSSIBLE NUMBER WHICH CAN EXIST IN THIS
C     * (32-BIT OR 64-BIT) MODE.
C
      RLIM=TINY(R)
  
      IF(MOIST.EQ.NC4TO8("   Q") .OR. MOIST.EQ.NC4TO8("SL3D")) THEN 
C
        DO L=1,LEVS 
          M = L+MSG
          DO I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            ESNEW     = HUMSP 
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*RZTMST
          ENDDO
        ENDDO
  
      ELSEIF(MOIST.EQ.NC4TO8("QHYB") .OR. MOIST.EQ.NC4TO8("SLQB"))THEN 
C
        FACT (1:ILG, 1:LEVS)=1. 
        EFACT(1:ILG, 1:LEVS)=0.
        DO L=1,LEVS 
          M = L+MSG
          DO I=IL1,IL2
            RMNEW = RMROW(I,M)
            IF(RMNEW.LE.0.) RMNEW = 10.*RLIM
            FACT (I,L)=SREF/RMNEW
            EFACT(I,L)=LOG(FACT(I,L))
          ENDDO
        ENDDO
C
        IF(SPOW.EQ.1.) THEN
          DO L=1,LEVS 
            M = L+MSG
            DO I=IL1,IL2
              RMNEW = RMROW(I,M)
              IF(RMNEW.GE.SREF) THEN
                ESNEWX = RMNEW
              ELSE
                ESNEWX = SREF/(1.+EFACT(I,L)) 
              ENDIF 
              ESTG(I,L) = ESTG(I,L)+(ESNEWX-ESG(I,L))*RZTMST
            ENDDO
          ENDDO 
        ELSE
          PINV=1./SPOW
          DO L=1,LEVS 
          M = L+MSG
            DO I=IL1,IL2
              RMNEW = RMROW(I,M)
              IF(RMNEW.GE.SREF) THEN
                ESNEWX = RMNEW
              ELSE
                ESNEWX = SREF/((1.+SPOW*EFACT(I,L))**PINV) 
              ENDIF 
              ESTG(I,L) = ESTG(I,L)+(ESNEWX-ESG(I,L))*RZTMST
            ENDDO
          ENDDO 
        ENDIF
  
      ELSE
                                                 CALL XIT('TENDCL8',-2) 
      ENDIF 
  
      RETURN
      END 
