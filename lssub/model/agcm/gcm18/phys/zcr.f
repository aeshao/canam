      FUNCTION ZCR(Q1,IMOD)
C
C     * FEB 25/2018 - S. KHARIN, K. VONSALZEN
C     * ADJUSTED CODE SO IT HANDLES CASE OF Q1=Q1MIN OR Q1=Q1MAX
C
C     * JUN 26/2013 - K.VONSALZEN.
C
C     * CALLED BY STATCLD5.
C
C     * RATIO OF MOMENTS OF CLOUD LIQUID WATER CONTENT. THE RATIO
C     * <QLWC**COEFF>/<QLWC>**COEFF IS CALCULATED BY INTEGRATING
C     * OVER A GAUSSIAN DISTRIBUTION USING THE STATISTICAL CLOUD
C     * SCHEME BY MELLOR (1977). FOR IMOD=0 -> COEFF=2.47 AND
C     * FOR IMOD=1 -> COEFF=3.
C
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
C
      REAL :: ZCR
      REAL, PARAMETER :: Q1MIN=-2., Q1MAX=2.
      INTEGER, PARAMETER :: INUM=61
      REAL, PARAMETER :: Q1DELT=(Q1MAX-Q1MIN)/REAL(INUM-1)
      REAL, DIMENSION(INUM,2) :: ZCRRR
      DATA ZCRRR /
C     IMOD=0
     &              708.636,  560.461,  445.761,  356.524,  286.721,
     1              231.848,  188.502,  154.094,  126.637,  104.629,
     2               86.902,   72.556,   60.895,   51.368,   43.552,
     3               37.112,   31.781,   27.350,   23.650,   20.549,
     4               17.939,   15.733,   13.862,   12.269,   10.908,
     5                9.739,    8.733,    7.864,    7.111,    6.456,
     6                5.885,    5.385,    4.946,    4.559,    4.218,
     7                3.916,    3.649,    3.410,    3.198,    3.008,
     8                2.838,    2.685,    2.548,    2.424,    2.312,
     9                2.211,    2.119,    2.036,    1.961,    1.892,
     1                1.829,    1.772,    1.720,    1.672,    1.628,
     2                1.588,    1.551,    1.517,    1.485,    1.457,
     3                1.430,
C     IMOD=1
     &             8889.942, 6454.475, 4721.926, 3480.747, 2584.975,
     1             1934.007, 1457.707, 1106.815,  846.462,  652.041,
     2              505.868,  395.250,  311.001,  246.407,  196.574,
     3              157.888,  127.668,  103.925,   85.150,   70.220,
     4               58.278,   48.672,   40.903,   34.586,   29.420,
     5               25.173,   21.665,   18.752,   16.322,   14.284,
     6               12.568,   11.117,    9.883,    8.830,    7.928,
     7                7.152,    6.482,    5.900,    5.395,    4.953,
     8                4.567,    4.227,    3.927,    3.662,    3.427,
     9                3.218,    3.032,    2.866,    2.716,    2.582,
     1                2.462,    2.353,    2.254,    2.165,    2.084,
     2                2.011,    1.944,    1.883,    1.827,    1.776,
     3                1.729 /
C
      IF ( Q1 <= Q1MIN ) THEN
        ZCR=ZCRRR(1,IMOD+1)
      ELSE IF ( Q1 >= Q1MAX ) THEN
        ZCR=ZCRRR(INUM,IMOD+1)
      ELSE
        Q1V=(Q1-Q1MIN)/Q1DELT+1.
        IND=INT(Q1V)
        WGT=Q1V-REAL(IND)
        ZCR=WGT*ZCRRR(IND+1,IMOD+1)+(1.-WGT)*ZCRRR(IND,IMOD+1)
      ENDIF
C
      END FUNCTION ZCR
