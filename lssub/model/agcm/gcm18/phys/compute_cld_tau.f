      SUBROUTINE COMPUTE_CLD_TAU(WRKB, CLDA,                           ! OUTPUT
     1                           CLW, CIC, REL, REI, DZ,               ! INPUT  
     2                           WRKA, CLDWATMIN, ILG, IL1, IL2, ILEV, 
     3                           NXLOC,NBS) 

!     18 APR 2018 - JASON COLE  INITIAL VERSION
!
! COMPUTE THE CLOUD-MEAN CLOUD OPTICAL THICKNESS AT 550 NM AND TO IT ADD
! THE AEROSOL OPTICAL THICKNESS FROM THE FIRST (VISIBLE) BAND, AKA 
! WRKA.

      IMPLICIT NONE

!
! OUTPUT 
!

      REAL, DIMENSION(ILG,NBS),INTENT(OUT) ::
     1 WRKB                                  ! CLOUD PLUS AEROSOL VISIBLE OPITCAL THICKNESS

      REAL, DIMENSION(ILG),INTENT(OUT) ::
     1 CLDA                              ! TOTAL CLOUD FRACTION THAT IS CONSISTENT WITH WRKB

!
! INPUT 
!
      
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) ::
     1 CLW,                                         ! CLOUD LIQUID WATER                  (UNITS)
     2 CIC,                                         ! CLOUD ICE WATER                     (UNITS)
     3 REL,                                         ! CLOUD LIQUID WATER EFFECTIVE RADIUS (MICRONS) 
     4 REI                                          ! CLOUD ICE WATER EFFECTIVE DIAMETER  (MICRONS)

      REAL, DIMENSION(ILG,NBS),INTENT(IN) ::
     1 WRKA                                 ! AEROSOL OPTICAL THICKNESS FOR EACH BAND

      REAL, DIMENSION(ILG,ILEV),INTENT(IN) ::
     1 DZ                                    ! LAYER THICKNESS (M)
 
      REAL, INTENT(IN) ::
     1 CLDWATMIN          ! THRESHOLD WATER CONTENT
      
      INTEGER, INTENT(IN) ::
     1 ILG,                 ! NUMBER OF GCM COLUMNS
     2 IL1,                 ! STARTING GCM COLUMN 
     3 IL2,                 ! ENDING GCM COLUMN
     4 ILEV,                ! NUMBER OF VERTICAL LAYERS 
     5 NXLOC,               ! NUMBER OF SUBCOLUMNS
     6 NBS                  ! NUMBER OF SOLAR BANDS

!
! LOCAL
!
      
      REAL, PARAMETER :: 
     1 R_ONE   = 1.0,
     2 R_ZERO  = 0.0
 
      REAL, DIMENSION(ILG) ::
     1 CUMTAU,                ! ACCUMULATE CLOUD OPTICAL THICKNESS IN VERTICAL
     2 SUMTAU,                ! ACCUMULATE CLOUD OPTICAL THICKNESS FROM EACH SUBCOLUMN
     2 COUNT_CLDY,
     3 CLDT
 
      REAL ::
     1 INVREL,     
     2 REI_LOC,        
     3 INVREI,
     4 TAU_VIS,
     5 TAULIQVIS,
     6 TAUICEVIS,
     7 CLW_PATH,
     8 CIC_PATH,
     9 R_NXLOC

      INTEGER ::
     1 I,
     2 L,
     3 IB,
     3 ICOL

! INITIALIZE VARIABLES

      R_NXLOC = REAL(NXLOC)

      DO IB = 1, NBS
         DO I = IL1, IL2
            WRKB(I,IB) = R_ZERO
         END DO ! I
      END DO ! IB

      DO I = IL1, IL2
         CLDT(I)   = R_ZERO
         CLDA(I)   = R_ZERO
         SUMTAU(I) = R_ZERO
      END DO

      DO ICOL = 1, NXLOC

        DO I = IL1, IL2
           COUNT_CLDY(I) = R_ZERO
           CUMTAU(I)     = R_ZERO
        END DO

        DO L = 1, ILEV
           DO I = IL1, IL2

              TAU_VIS  = R_ZERO
              CLW_PATH = CLW(I,L,ICOL)*DZ(I,L)
              CIC_PATH = CIC(I,L,ICOL)*DZ(I,L)

! CALCULATION FOR CLDT
              IF (CLW_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
              END IF
              IF (CIC_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
              END IF

! CALCULATION OF CLOUD OPTICAL THICKNESS

              IF ((CLW_PATH .GT. CLDWATMIN  .OR.
     1             CIC_PATH .GT. CLDWATMIN)) THEN

! COMPUTE CLOUD OPTICAL THICKNESS FOR THIS VOLUME

                 INVREL = R_ONE/REL(I,L,ICOL)

                 REI_LOC = 1.5396*REI(I,L,ICOL) ! CONVERT TO GENERALIZED DIMENSION
                 INVREI  = R_ONE/REI_LOC

                 IF (CLW_PATH .GT. CLDWATMIN) THEN
                    TAULIQVIS = CLW(I,L,ICOL)
     1                        * (4.483e-04 + INVREL * (1.501 + INVREL
     2                        * (7.441e-01 - INVREL * 9.620e-01)))
                 ELSE
                    TAULIQVIS = R_ZERO
                 END IF
                 
                 IF (CIC_PATH .GT. CLDWATMIN) THEN
                    TAUICEVIS = CIC(I,L,ICOL) 
     1                        * (-0.303108e-04 + 0.251805e+01 * INVREI)  
                 ELSE
                    TAUICEVIS = R_ZERO
                 END IF

                 TAU_VIS = (TAULIQVIS+TAUICEVIS)*DZ(I,L)
              END IF

              CUMTAU(I) = CUMTAU(I) + TAU_VIS

           END DO ! I
        END DO ! L

        DO I = IL1, IL2
           CLDT(I)   = CLDT(I) + COUNT_CLDY(I)
           SUMTAU(I) = SUMTAU(I) + CUMTAU(I)
        END DO ! I

      END DO ! ICOL

      DO IB = 1, NBS
         DO I = IL1, IL2
            IF (CLDT(I) > R_ZERO) THEN
               WRKB(I,IB) = SUMTAU(I)/CLDT(I)
            ELSE
               WRKB(I,IB) = R_ZERO
            END IF
            
            CLDA(I) = CLDT(I)/R_NXLOC

! ADD IN THE AEROSOLS FROM BAND 1 (VISIBLE)
            WRKB(I,IB) = WRKB(I,IB) + WRKA(I,1)
         END DO ! I
      END DO ! IB
      
      RETURN
      END
