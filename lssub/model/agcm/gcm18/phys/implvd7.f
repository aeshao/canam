      SUBROUTINE IMPLVD7 (A,B,C, CL,X,XG, IL1,IL2,ILG,ILEV,TODT,
     1                    TEND,DELSIG,RAUS,WORK,VINT) 
  
C     * DEC 07/89 - D.VERSEGHY: SAME AS PREVIOUS VERSION IMPLVD6
C     *                         EXCEPT FOR REMOVAL OF UPDATE OF 
C     *                         SURFACE FLUXES. 
  
C     * CALCULATE TENDENCIES DUE TO VERTICAL DIFFUSION IN HYBRID MODEL. 
C     * THE SCHEME IS IMPLICIT BACKWARD.
C     * A,B,C ARE RESPECTIVELY THE LOWER, MAIN AND UPPER DIAG.
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   A     (ILG,ILEV),B     (ILG,ILEV),C     (ILG,ILEV) 
      REAL   X     (ILG,ILEV),WORK  (ILG,ILEV)
      REAL   CL    (ILG),     XG    (ILG),     VINT(ILG)
      REAL   TEND  (ILG,ILEV),DELSIG(ILG,ILEV),RAUS  (ILG)
C-----------------------------------------------------------------------
C     * GET X+ (IN ARRAY TEND) FROM X-. SAVE X(I,ILEV) IN VINT(I).
  
      DO 100 I=IL1,IL2
         VINT(I)   = X(I,ILEV)
         X(I,ILEV) = X(I,ILEV) +TODT*CL(I)*XG(I)
  100 CONTINUE
  
      CALL VROSSR(TEND, A,B,C,X,WORK,ILG,IL1,IL2,ILEV)
  
      DO 150 I=IL1,IL2
         X(I,ILEV) = VINT(I)
  150 CONTINUE
  
C     * GET TENDENCY FROM X- AND X+.
  
      DO 200 L=1,ILEV 
      DO 200 I=IL1,IL2
         TEND(I,L) = (TEND(I,L)-X(I,L))*(1./TODT) 
  200 CONTINUE
  
C     * CALCULATE VERTICAL INTEGRAL VINT. 
  
      DO 300 I=IL1,IL2
         VINT(I) = 0. 
  300 CONTINUE
  
      DO 400 L=1,ILEV 
      DO 400 I=IL1,IL2
         VINT(I) = VINT(I) +TEND(I,L)*DELSIG(I,L) 
  400 CONTINUE
  
      RETURN
      END 
