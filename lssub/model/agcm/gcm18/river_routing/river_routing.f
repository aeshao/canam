      SUBROUTINE RIVER_ROUTING(FOUT,FOUT_LAND,VELOCITY,
     1    PREVIOUS_GW_STORE,PREVIOUS_SW_STORE,PREVIOUS_SW_OUTFLOW,
     2    PREVIOUS_SW_INFLOW,PREVIOUS_DEPTH,PREVIOUS_GW_OUTFLOW,
     3    ROF,ROFO,FLND,FLAK,SLOPE,WIDTH,LENGTH,DIRECTION,GW_DELAY,
     4    LAT1,LON1,LAT2,LON2,LAT3,LON3,LAT4,LON4,LAT5,LON5,
     5    LAT6,LON6,LAT7,LON7,LANDMASK,
     6    DELT,ISBEG,LONSL,LON,LAT)
C=======================================================================
C 
C     * RIVER ROUTING ALGORITHM - VIVEK ARORA
C
C     * Aug 2/2016   - M. Lazare - LONSL also passed in and used to dimension
C                                  all arrays except {ROF,ROFO,FLND,FLAK}.
C                                - All references to "LON-1" (including do-loops)
C                                  changed to "LONSL".
C                                - Re-ordering of subroutine statement to
C                                  better reflect what is output only, input
C                                  only, I/O and internal.
C                                - FLAK used as well as FLND to define what is
C                                  ocean fraction (FOCN=1.-FLND-FLAK) and this is now
C                                  used instead of "1.-FLND".
C                                - DELT and ISBEG passed in and used to calculate
C                                  DTCOUP, which replaces hardcoded 86400. (ie one day).
C                                  This is also passed to a new version of 
C                                  DO_SURFACE_ROUTING.
C                                - "YESTERDAYS" -> "PREVIOUS".
C     * AUG 2016     - V. ARORA  Changes made by Vivek for switch over to CMIP6
C                                 setup with NEMO ocean and new coupler.
C                                 Take out all flinging stuff from closed
C                                 oceans and lakes. Plus account for fractional
C                                 land. This code, therefore, now only routes 
C                                 runoff to produce routed runoff that is dumped
C                                 into grid cells with land fraction less than 0.49.
C                                 
C                                 This routed runoff called FOUT is what will needed
C                                 to be passed to the coupler. 
C
C     * NOV 10/2015 - M.Lazare. Adapted from coupler routine for AGCM:
C     *                         - no CPP directives.
C     *                         - LON,LAT,MAXX passed in instead of
C     *                           PARAMETER statement.
C
C     * 19/07/00 DY.Robitaille/V.Arora
C         Modified Great Lakes/St. Lawrence River code
C         Solved previous/current time step variable mix-up bug
C             
C     * S.Tinis
C         Modified for CGCM3 use
C 
C     * INPUT ROF TOTAL RUNOFF IN KG/M^2/S OVER LAND
C     * INPUT ROFO - OVERLAND RUNOFF IN KG/M^2/S
C     * INPUT FLND - FRACTION OF LAND IN EACH GRID CELL
C     * INPUT FLAK - FRACTION OF INLAND LAKE FRACTION

C     * OUTPUT FOUT FLUX IN KGM/M^2/S, AT THE MOUTH OF RIVERS ALL ALONG
C     * THE CONTINENTAL EDGES AND OVER INLAND WATER BODIES.
C
C     * FOUT_LAND - OUTPUT FLUX IN  M^3/S OVER LAND, USING THIS WE
C     * CAN SEE WHAT DOES THE RIVER DISCHARGE LOOKS LIKE AT ANY POINT WITHIN
C     * A RIVER BASIN, AS WELL AS FIND THE OUTFLOW INTO THE OCEAN CELLS
C     * ALONG THE CONTINENTAL EDGES, BUT FOUT_LAND CANNOT BE USED TO FIND
C     * FRESH WATER FLUX (P-E) OVER OCEAN CELLS. THUS THIS QUANTITY IS
C     * ESSENTIALLY FOR DIAGNOSTICS. THE ACTUAL FRESHWATER FLUX WHICH
C     * AFFECTS SALINITY IS GIVEN BY FOUT        
C
C     * DETAILS OF RIVER ROUTING PARAMETERS
C
C     * SLOPE - THE SLOPE BETWEEN THE GRID CELLS FOUND USING THE MEAN
C     *         ELEVATIONS
C
C     * WIDTH - WIDTH OF THE RIVER ALONG THE VARIOUS SECTIONS OF THE
C     *          DIGITAL RIVER NETWORK AT THE GCM RESOLUTION PARAMETERIZED
C     *          USING A WIDTH-MEAN ANNUAL DISCHARGE RELATIONSHIP
C
C     * LENGTH - DISTANCE BETWEEN THE UPSTREAM AND DOWNSTREAM CELL IN KM.
C
C     * DIRECTION - DIRECTION OF RIVER FLOW
C     *             1 - NORTH, 2 - NE, 3 - EAST, 4 - SE, 
C     *             5 - SOUTH, 6 - SW, 7 - WEST, 8 - NW
C     *             0 - INTERNALLY DRAINED CELL
C     *             9 - INTERNALLY DRAINED RIVER MOUTH
C
C     * GW_DELAY - GROUND WATER DELAY FACTOR FOR THE GCM CELL, FUNCTION
C     *            OF MAJOR SOIL TYPE IN THE GRID CELL
C
C     * LAT1, LON1 - LATITUDE AND LONGITUDE OF NEIGHBOURING CELL WHICH
C     *                DRAINS ITS WATER INTO THE GIVEN GCM CELL
C     *               THERE ARE UPTO 7 NEIGHBOURING UPSTREAM CELLS AT 96X48
C     *               RESOLUTION, THAT IS WHY WE GO UPTO LAT7 & LON7
C
C     * LANDMASK - GROUND COVER EXTRACTED FROM MY ROUTING PARAMETER FILE, BASED
C     *      ON WHICH ROUTING IS PERFORMED. THIS IS DIFFERENT FROM ANY GC 
C     *      IN THE AGCM AND BASED ON 0.49 FRACTIONAL LAND THRESHOLD.
C     *      ALL GRID CELLS WITH LESS THAN 49% LAND ARE DEEMED WATER/OCEAN
C     *      AS FAR AS RIVER ROUTING IS CONCERNED.
C
C=======================================================================

      IMPLICIT NONE 

      INTEGER ISBEG, LONSL, LON, LAT
C
C     * OUTPUT.
C
      REAL FOUT      (LON,LAT)
      REAL FOUT_LAND (LON,LAT)
      REAL VELOCITY  (LON,LAT)
C
C     * INPUT/OUTPUT.
C
      REAL PREVIOUS_SW_INFLOW   (LON,LAT)
      REAL PREVIOUS_DEPTH       (LON,LAT)
      REAL PREVIOUS_SW_STORE    (LON,LAT)
      REAL PREVIOUS_SW_OUTFLOW  (LON,LAT)
      REAL PREVIOUS_GW_OUTFLOW  (LON,LAT)
      REAL PREVIOUS_GW_STORE    (LON,LAT)
C
C     * INPUT.
C
      REAL ROF       (LON,LAT)
      REAL ROFO      (LON,LAT)
      REAL FLND      (LON,LAT)
      REAL FLAK      (LON,LAT)

      REAL SLOPE     (LONSL,LAT)
      REAL WIDTH     (LONSL,LAT)
      REAL LENGTH    (LONSL,LAT)
      REAL DIRECTION (LONSL,LAT)
      REAL GW_DELAY  (LONSL,LAT)
      REAL LANDMASK  (LONSL,LAT)

      REAL LAT1      (LONSL,LAT)
      REAL LAT2      (LONSL,LAT)
      REAL LAT3      (LONSL,LAT)
      REAL LAT4      (LONSL,LAT)
      REAL LAT5      (LONSL,LAT)
      REAL LAT6      (LONSL,LAT)
      REAL LAT7      (LONSL,LAT)

      REAL LON1      (LONSL,LAT)
      REAL LON2      (LONSL,LAT)
      REAL LON3      (LONSL,LAT)
      REAL LON4      (LONSL,LAT)
      REAL LON5      (LONSL,LAT)
      REAL LON6      (LONSL,LAT)
      REAL LON7      (LONSL,LAT)
C
C     * INTERNAL.
C
      REAL GCM_CELL_AREA   (LONSL,LAT)
      REAL PERCOLATION     (LONSL,LAT)
      REAL SURFACE_RUNOFF  (LONSL,LAT)
      REAL NEIGHBOUR_INFLOW(LONSL,LAT)
     
      REAL SW_INFLOW   (LONSL,LAT)
      REAL DEPTH       (LONSL,LAT)
      REAL SW_STORE    (LONSL,LAT)
      REAL SW_OUTFLOW  (LONSL,LAT)
      REAL GW_OUTFLOW  (LONSL,LAT)
      REAL GW_STORE    (LONSL,LAT)
      REAL GW_INFLOW   (LONSL,LAT)

      INTEGER NEIGHBOUR_LAT(7,LONSL,LAT)
      INTEGER NEIGHBOUR_LON(7,LONSL,LAT)

      REAL*8 PI
      REAL*8 WL(LAT),RADL(LAT), WOSSL(LAT)
      REAL*8 SL(LAT)
      REAL*8 CL(LAT)
      REAL*8 ML(LONSL)
     
      REAL EARTH_RADIUS
      REAL CC
      REAL DELT
     
      REAL PREV_DEPTH
      REAL PREV_GOING_IN
      REAL CUR_SLOPE
      REAL CUR_DISTANCE
      REAL CUR_WIDTH
      REAL VEL
      REAL CUR_DEPTH
      REAL GOING_IN
      REAL COMING_OUT
      REAL FOCN
      REAL DTCOUP     
      REAL FWATER

      INTEGER LATH,I,J,DIR,N,N_LAT,N_LON
      INTEGER NWDMAX

      PARAMETER(NWDMAX=18528)
      PARAMETER(PI=3.1415926535898d0)
      PARAMETER(EARTH_RADIUS=6371.22)                              !KM
C-----------------------------------------------------------------------
C     CALCULATE COUPLING TIME FREQUENCY IN SECONDS

      DTCOUP=DELT*REAL(ISBEG)

C     FIRST, INITIALIZE VELOCITY, FOUT AND FOUT_LAND TO ZERO

      DO J = 1,LAT
       DO I = 1,LONSL
         FOUT     (I,J)=0.0
         FOUT_LAND(I,J)=0.0
         VELOCITY (I,J)=0.0
       ENDDO
      ENDDO

C     GET THE GAUSSIAN WEIGHTS INTO WL SO THAT WE CAN FIND THE
C     AREAS OF THE GCM CELLS, WE NEED THESE AREAS BECAUSE THE ROUTING
C     ALGORITHM FINDS DISCHARGE IN M^3/S WHICH WE WILL LATER CONVERT TO
C     KG/M^2/S

      LATH = LAT/2
      CALL GAUSSG(LATH,SL,WL,CL,RADL,WOSSL)
      CALL TRIGL(LATH,SL,WL,CL,RADL,WOSSL)

C     WL CONTAINS ZONAL WEIGHTS, LETS FIND MERIDIONAL WEIGHTS

      DO I = 1,LONSL
       ML(I) = 1.0/(REAL(LONSL))
      ENDDO

C     FIND THE AREAS OF GCM CELLS IN KM^2

      DO J = 1,LAT
       DO I = 1,LONSL

        GCM_CELL_AREA(I,J) = 4*PI*(EARTH_RADIUS**2)*WL(J)*ML(I)/2.0

C       DIVIDING BY 2.0 BECAUSE WL(1 TO LAT) ADD TO 2.0 NOT 1.0
         
       ENDDO
      ENDDO

C     OKAY NOW WE KNOW THE AREA OF GCM CELLS
C     ARRANGE LATS AND LONS OF NEIGHBOURING CELLS IN AN ARRAY

      DO J = 1,LAT
       DO I = 1,LONSL
        NEIGHBOUR_LAT(1,I,J)=NINT(LAT1(I,J))
        NEIGHBOUR_LAT(2,I,J)=NINT(LAT2(I,J))
        NEIGHBOUR_LAT(3,I,J)=NINT(LAT3(I,J))
        NEIGHBOUR_LAT(4,I,J)=NINT(LAT4(I,J))
        NEIGHBOUR_LAT(5,I,J)=NINT(LAT5(I,J))
        NEIGHBOUR_LAT(6,I,J)=NINT(LAT6(I,J))
        NEIGHBOUR_LAT(7,I,J)=NINT(LAT7(I,J))

        NEIGHBOUR_LON(1,I,J)=NINT(LON1(I,J))
        NEIGHBOUR_LON(2,I,J)=NINT(LON2(I,J))
        NEIGHBOUR_LON(3,I,J)=NINT(LON3(I,J))
        NEIGHBOUR_LON(4,I,J)=NINT(LON4(I,J))
        NEIGHBOUR_LON(5,I,J)=NINT(LON5(I,J))
        NEIGHBOUR_LON(6,I,J)=NINT(LON6(I,J))
        NEIGHBOUR_LON(7,I,J)=NINT(LON7(I,J))

       ENDDO
      ENDDO

C     THE LAND SURFACE SCHEME SAVES ROF - TOTAL RUNOFF AND ROFO - THE
C     OVERLAND RUNOFF. SO WE HAVE TO FIND DEEP SOIL PERCOLATION/BASE FLOW
C     OURSELVES FOR LAND CELLS ONLY

      DO J = 1,LAT
       DO I = 1,LONSL
        IF(LANDMASK(I,J).LT.-0.5)THEN
         PERCOLATION(I,J) = ROF(I,J) - ROFO(I,J)
         IF(PERCOLATION(I,J).LT.0.0)THEN
          PERCOLATION(I,J) = 0.0
         ENDIF
C        TO DEAL WITH GREAT LAKES, AND ANY LARGE LAKES FOR THAT MATTER,
C        WHICH AFFECT RIVER FLOW WE NEED A SIMPLE MECHANISM TO INCREASE
C        THE RESIDENCE TIME OF WATER. E.G. THE RESIDENCE TIME OF WATER IN
C        LAKE SUPERIOR (THE LARGEST GREAT LAKE IS ~200 YRS) AND IN LAKE
C        ERIE IS AROUND 2-3 YRS. RESIDENCE TIME IN INLAND LAKES DEPENDS
C        ON LAKE VOLUME (WHICH, OF COURSE, DEPENDS ON LAKE DEPTH AND AREA).
C        WE DO HAVE LAKE DEPTH, BUT AREA IS TRICKY BECAUSE A LAKE MAYBE SPREAD 
C        ACROSS MULTIPLE GRID CELLS.
C
C        SO FOR NOW WE JUST ASSUME IF LAKE FRACTION IS GREATER THAN SOME
C        THRESHOLD THEN WE INCREASE THE GW_DELAY TO SOME RELATIVELY LARGER
C        NUMBER THAN OUR MAX. VALUE OF 60 DAYS
C
         IF (FLAK(I,J) .GT. 0.4) THEN
           GW_DELAY(I,J)=300.0
         ENDIF

        ENDIF
       ENDDO
      ENDDO

C     DO THE ROUTING BIT CELL BY CELL FOR EACH LAND CELL 
C     & ADD P-E FOR OCEAN CELLS  

      DO J = 1,LAT
       DO I = 1,LONSL

C       Set the temporary variables to zero before each new grid point calculations
C
        PREV_DEPTH = 0.
        PREV_GOING_IN = 0.
        CUR_SLOPE = 0.
        CUR_DISTANCE = 0.
        CUR_WIDTH = 0.
        VEL = 0.
        CUR_DEPTH = 0.
        GOING_IN = 0.
        COMING_OUT = 0.

        IF(LANDMASK(I,J).LT.-0.5)THEN               !DO ROUTING CALCULATION FOR LAND CELLS
C                                             !THIS MASK COMES FROM RIVER PARAMETERS FILE
C                                             !AND IS BASED ON FRACTIONAL LAND THRESHOLD    
C                                             !OF 0.49 THAT VIVEK USED. 
         DIR = NINT(DIRECTION(I,J))

         IF(DIR.GE.1.AND.DIR.LE.8)THEN        ! Ordinary land grid cells

C         CONVERT KG/M^2/S INTO M^3/S; THE CONVERSION FORMULA IS:
C           FLOW(M^3/S) = FLUX(KG/M^2/S)*AREA(KM^2)*10^6(M^2/KM^2)/DENSITY(=1000 KG/M^3)
C                         * FLND

          SURFACE_RUNOFF(I,J) = ROFO(I,J)*FLND(I,J)*
     /    GCM_CELL_AREA(I,J)*1000.0
          GW_INFLOW(I,J)=PERCOLATION(I,J)*FLND(I,J)*
     /    GCM_CELL_AREA(I,J)*1000.0

C         SO FAR WE KNOW WHAT IS THE SURFACE RUNOFF AND GW_INFLOW, NOW WE
C         FIND THE INFLOW FROM NEIGHBOURING CELLS, WHICH COULD EVEN BE A LITTLE
C         FAR LOCATED INTERNALLY DRAINED CELLS

          NEIGHBOUR_INFLOW(I,J) = 0.0
          DO N = 1,7
           N_LAT = NEIGHBOUR_LAT(N,I,J)
           N_LON = NEIGHBOUR_LON(N,I,J)
           IF(N_LAT.NE.0.AND.N_LON.NE.0)THEN
            NEIGHBOUR_INFLOW(I,J)=NEIGHBOUR_INFLOW(I,J)+
     /      PREVIOUS_SW_OUTFLOW(N_LON,N_LAT)
           ENDIF
          ENDDO


C         FIND THE GW FACTOR USING THE GW DELAY FACTOR

          IF(GW_DELAY(I,J).EQ.0.0)CALL        XIT('RIVER_ROUTING',-1)

          CC = EXP(-1.0/GW_DELAY(I,J))

C         FIND THE GW OUTFLOW AND UPDATE THE GW STORE

          GW_OUTFLOW(I,J)=CC*PREVIOUS_GW_OUTFLOW(I,J)+
     /    (1.0-CC)*GW_INFLOW(I,J)
          GW_STORE(I,J)=PREVIOUS_GW_STORE(I,J)+GW_INFLOW(I,J)*DTCOUP
     /    -GW_OUTFLOW(I,J)*DTCOUP

C         CHECK IF GW STORE GOES BELOW ZERO AND CORRECT ACCORDINGLY

          IF(GW_STORE(I,J).LT.0.0)THEN
           IF(PREVIOUS_GW_STORE(I,J).GT.0.0)THEN
            GW_OUTFLOW(I,J)=PREVIOUS_GW_STORE(I,J)/DTCOUP+
     /      GW_INFLOW(I,J)-0.05
            IF(GW_OUTFLOW(I,J).LT.0.0)GW_OUTFLOW(I,J)=0.0
            GW_STORE(I,J)=PREVIOUS_GW_STORE(I,J)+
     /      GW_INFLOW(I,J)*DTCOUP - GW_OUTFLOW(I,J)*DTCOUP
           ENDIF
          ENDIF

C         NOW WE ARE READY TO ROUTE THE WATER THRU THE RIVER CHANNEL
C         SO WE FIND WHAT GOES INTO THE RIVER CHANNEL

          SW_INFLOW(I,J) = SURFACE_RUNOFF(I,J) + GW_OUTFLOW(I,J) +
     /    NEIGHBOUR_INFLOW(I,J)


C         PREPARE TO CALL THE SURFACE ROUTING SUBROUTINE


          GOING_IN = SW_INFLOW(I,J)
          PREV_GOING_IN = PREVIOUS_SW_INFLOW(I,J)
          PREV_DEPTH = PREVIOUS_DEPTH(I,J)
          CUR_SLOPE = SLOPE(I,J)
          CUR_DISTANCE = LENGTH(I,J)*1000. ! Conversion from Km to m
          CUR_WIDTH = WIDTH(I,J)

C         CHECK FOR -VE INFLOW

          IF(GOING_IN.LT.0.0)THEN
           WRITE(*,*)'FOR CELL (LONG,LAT) [',I,',',J,']'
           WRITE(*,*)'-VE FLOW GOING IN FOR SURFACE ROUTING = ',GOING_IN
           WRITE(*,*)'SURFACE RUNOFF    = ',SURFACE_RUNOFF(I,J)
           WRITE(*,*)'GW_OUTFLOW        = ',GW_OUTFLOW(I,J)
           WRITE(*,*)'NEIGHBOUR_INFLOW  = ',NEIGHBOUR_INFLOW(I,J)
           CALL                  XIT('RIVER_ROUTING',-2)
          ENDIF

C         CALL THE SURFACE ROUTING SUBROUTINE

          CALL DO_SURFACE_ROUTING(GOING_IN,COMING_OUT,PREV_DEPTH,
     /    CUR_SLOPE,CUR_DISTANCE,PREV_GOING_IN,CUR_DEPTH,VEL,
     /    CUR_WIDTH,DTCOUP)

C         CHECK FOR -VE OUTFLOW

          IF(COMING_OUT.LT.0.0)THEN
           WRITE(*,*)'FOR CELL (LONG,LAT) [',I,',',J,']'
           WRITE(*,*)'-VE FLOW COMING OUT OF SURFACE ROUTING = '
     /     ,COMING_OUT
           WRITE(*,*)'COMING OUT= ',COMING_OUT
           CALL                  XIT('RIVER_ROUTING',-3)
          ENDIF


          SW_OUTFLOW(I,J) = COMING_OUT

          SW_STORE(I,J) = PREVIOUS_SW_STORE(I,J)+
     /    (SW_INFLOW(I,J)-SW_OUTFLOW(I,J))*DTCOUP


C         CHECK IF SW STORE GOES BELOW ZERO, CORRECT SW_OUTFLOW
C         AND ADJUST THE FLOW DEPTH ACCORDINGLY


          IF(SW_STORE(I,J).LT.0.0)THEN
           IF(PREVIOUS_SW_STORE(I,J).GT.0.0)THEN
            SW_OUTFLOW(I,J)=PREVIOUS_SW_STORE(I,J)/DTCOUP+
     /      SW_INFLOW(I,J)-0.1
            IF(SW_OUTFLOW(I,J).LT.0.0)THEN
             SW_OUTFLOW(I,J)=0.0
            ENDIF
            SW_STORE(I,J)=PREVIOUS_SW_STORE(I,J)+
     /      SW_INFLOW(I,J)*DTCOUP - SW_OUTFLOW(I,J)*DTCOUP

C           ALSO ADJUST THE FLOW DEPTH TO MATCH THE CORRECTED OUTFLOW
C           0.040 IN THE EQN. BELOW IS MANNING N
            CUR_DEPTH = ((SW_OUTFLOW(I,J)*0.040)/
     /      (WIDTH(I,J)*SLOPE(I,J)**0.5))**(3./5.)

C           SAVE THE NEW VELOCITY AS WELL
            VEL=(1./0.040)*((CUR_DEPTH)**(2./3.))*(SLOPE(I,J)**0.5)

           ENDIF
          ENDIF

          VELOCITY(I,J) = VEL
          DEPTH(I,J) = CUR_DEPTH


          FOUT_LAND(I,J) = SW_OUTFLOW(I,J)

         ENDIF                                ! IF DIR is between 1 and 8 ie ordinary cells

         IF(DIR.EQ.0)THEN                ! IF internally drained cells

C          IF INTERNALLY DRAINED CELL THEN EVERYTHING GOES INTO THE
C          GROUNDWATER STORE WHICH HAS A TIME DELAY OF 100 DAYS

          GW_INFLOW(I,J) = ROF(I,J)*FLND(I,J)*
     /    GCM_CELL_AREA(I,J)*1000.0

          NEIGHBOUR_INFLOW(I,J) = 0.0
          DO N = 1,7
           N_LAT = NEIGHBOUR_LAT(N,I,J)
           N_LON = NEIGHBOUR_LON(N,I,J)
           IF(N_LAT.NE.0.AND.N_LON.NE.0)THEN
            NEIGHBOUR_INFLOW(I,J)=NEIGHBOUR_INFLOW(I,J)+
     /      PREVIOUS_SW_OUTFLOW(N_LON,N_LAT)
           ENDIF
          ENDDO

C          ADD NEIGHBOUR INFLOW TO GW INFLOW AS WELL AND
C          FIND THE GW OUTFLOW AND UPDATE THE GW STORE

          GW_INFLOW(I,J) = GW_INFLOW(I,J) +  NEIGHBOUR_INFLOW(I,J)

C          FIND THE GW OUTFLOW AND UPDATE THE GW STORE

          CC = EXP(-1./100.) 
          GW_OUTFLOW(I,J)=CC*PREVIOUS_GW_OUTFLOW(I,J)+
     /    (1.0-CC)*GW_INFLOW(I,J)
          GW_STORE(I,J)=PREVIOUS_GW_STORE(I,J)+GW_INFLOW(I,J)*DTCOUP
     /    -GW_OUTFLOW(I,J)*DTCOUP

C         CHECK IF GW STORE GOES BELOW ZERO AND CORRECT ACCORDINGLY

          IF(GW_STORE(I,J).LT.0.0)THEN
           IF(PREVIOUS_GW_STORE(I,J).GT.0.0)THEN
            GW_OUTFLOW(I,J)=PREVIOUS_GW_STORE(I,J)/DTCOUP+
     /      GW_INFLOW(I,J)-0.05
            IF(GW_OUTFLOW(I,J).LT.0.0)GW_OUTFLOW(I,J)=0.0
            GW_STORE(I,J)=PREVIOUS_GW_STORE(I,J)+
     /      GW_INFLOW(I,J)*DTCOUP - GW_OUTFLOW(I,J)*DTCOUP
           ENDIF
          ENDIF

C          AND THUS SURFACE WATER OUTFLOW FOR A INTERNALLY DRAINED CELL IS
C          NOTHING BUT ITS GW OUTFLOW AND THIS WILL BE MOVED TO NEAREST
C         OCEAN CELL OR NEAREST RIVER BASIN AS BASEFLOW. AND WHERE DOES
C          THIS IS TAKEN ACCOUNT OF - ITS TAKEN ACCOUNT OF IN NEIGHBOURS        

          SW_OUTFLOW(I,J) = GW_OUTFLOW(I,J)

          FOUT_LAND(I,J) = SW_OUTFLOW(I,J)

         ENDIF                                ! IF DIR is 0 ie internally drained cells


         IF(DIR.EQ.9)THEN                ! Internally drained river mouth

C          NOW WE DEAL WITH INTERNALLY DRAINED RIVER MOUTHS, THESE CELLS
C          GET WATER FROM NEIGHBOURING CELLS AND THERE OWN SURFACE RUNOFF +
C          DEEP SOIL PERCOLATION BUT EVERYTHING ENDS UP IN THE GW STORE

          GW_INFLOW(I,J) = ROF(I,J)*FLND(I,J)*
     /    GCM_CELL_AREA(I,J)*1000.0

          NEIGHBOUR_INFLOW(I,J) = 0.0
          DO N = 1,7
           N_LAT = NEIGHBOUR_LAT(N,I,J)
           N_LON = NEIGHBOUR_LON(N,I,J)
           IF(N_LAT.NE.0.AND.N_LON.NE.0)THEN
            NEIGHBOUR_INFLOW(I,J)=NEIGHBOUR_INFLOW(I,J)+
     /      PREVIOUS_SW_OUTFLOW(N_LON,N_LAT)
           ENDIF
          ENDDO

C          ADD NEIGHBOUR INFLOW TO GW INFLOW AS WELL AND
C          FIND THE GW OUTFLOW AND UPDATE THE GW STORE

          GW_INFLOW(I,J) = GW_INFLOW(I,J) +  NEIGHBOUR_INFLOW(I,J)

          CC = EXP(-1./100.) 
          GW_OUTFLOW(I,J)=CC*PREVIOUS_GW_OUTFLOW(I,J)+
     /    (1.0-CC)*GW_INFLOW(I,J)
          GW_STORE(I,J)=PREVIOUS_GW_STORE(I,J)+GW_INFLOW(I,J)*DTCOUP
     /    -GW_OUTFLOW(I,J)*DTCOUP


          SW_OUTFLOW(I,J) = GW_OUTFLOW(I,J)

          FOUT_LAND(I,J) = SW_OUTFLOW(I,J)


         ENDIF                                ! IF DIR is 11 ie internally drained river mouths

        ELSE IF(LANDMASK(I,J).GT.-0.5)THEN        !Take care of freshwater flux into the oceans
C                                           !THESE ARE GRID CELLS WITH LESS THAN 0.49 LAND
C                                           !FRACTION USED BY VIVEK TO FIND RIVER PARAMETERS.
C
C       THESE GRID CELLS RECEIVE FRESHWASTER FROM ROUTED RUNOFF AT THE CONTINENTAL
C       EDGES, PLUS THEIR OWN ROF WITHOUT ANY ROUTING. E.G IF ONE OF THE HAWAIIAN 
C       ISLANDS COVERS 0.48 FRACTION OF A GRID CELL, IT'S RUNOFF CONTRIBUTES TO FRESHWATER
C       TO THE OCEAN IN THE SAME GRID CELL.

C       THESE CELLS MAY ALSO RECEIVE ROUTED RUNOFF FROM FROM NEARBY        
C       INTERNALLY DRAINED CELLS AND INTERNALLY DRAINED RIVER MOUTHS. 

C       NOTE THAT WHEN ROUTED RUNOFF (M3/S) IS MOVED TO AN OCEAN CELL
C       IT NEEDS TO BE CONVERTED TO KG/(M^2.S) BY DIVISION WITH AGCM GRID CELL
C       AREA INTO WHICH THIS ROUTED RUNOFF IS BEING DUMPED.

         NEIGHBOUR_INFLOW(I,J) = 0.0
         DO N = 1,7
          N_LAT = NEIGHBOUR_LAT(N,I,J)
          N_LON = NEIGHBOUR_LON(N,I,J)
          IF(N_LAT.NE.0.AND.N_LON.NE.0)THEN
           NEIGHBOUR_INFLOW(I,J)=NEIGHBOUR_INFLOW(I,J)+
     /     PREVIOUS_SW_OUTFLOW(N_LON,N_LAT)
          ENDIF
         ENDDO

C         THIS TAKES CARE OF EVERYTHING THAT DRAINS INTO THE GIVEN OCEAN
C         CELL, NOW CONVERT THIS INTO KG/M^2/S BY DIVIDING BY THE GCM CELL
C         AREA, AND THIS IS WHAT THE AGCM GRID CELL FINALLY NEEDS.
C         NOTE THAT THIS WATER NEEDS TO BE SPREAD OVER THE WATER AREA
C         SO WE NEED TO MULTIPLY WITH (1-FLND-FLAK)

C         THE LOGIC IS THAT WE TRY TO SPREAD THE ROUTED WATER AND ANY
C         RUNOFF FROM THE SAME GRID CELL OVER THE OCN FRACTION. IN THE
C         EVENT THERE IS NO OCEAN IN THIS GRID CELL WE SPREAD THE WATER
C         OVER THE FLAK FRACTION.

         FOCN = MAX(0.,MIN(1.0-FLND(I,J)-FLAK(I,J),1.))

         IF(FOCN.LT.1E-05)THEN ! NO OCEAN IN THIS GRID CELL
C                              ! SPREAD ROUTED RUNOFF OVER FLAK
            IF(FLAK(I,J).GT.1E-5)THEN
               FWATER=FLAK(I,J)
            ELSE
              WRITE(*,*)'FOCN(',I,',',J,')=',FOCN
              WRITE(*,*)'FLAK(',I,',',J,')=',FLAK(I,J)
              WRITE(*,*)'FLND(',I,',',J,')=',FLND(I,J)
              WRITE(*,*)'WEIRD GRID CELL WITH LAND LIKELY LESS THAN'
              WRITE(*,*)'0.49 FRACTION, AND VERY LITTLE FOCN AND'
              WRITE(*,*)'FLAK.'
              WRITE(*,*)'AN INDICATION THAT RIVER PARAMETERS FILE'
              WRITE(*,*)'IS INCONSISTENT WITH FRACTIONAL LAND MASK'
              CALL                 XIT('RIVER_ROUTING',-4)
            ENDIF
         ELSE
            FWATER=FOCN
         ENDIF

         NEIGHBOUR_INFLOW(I,J) =  NEIGHBOUR_INFLOW(I,J)/
     /   (GCM_CELL_AREA(I,J)*1000.0*FWATER)

C        ALSO ADD THIS GRID CELL'S OWN RUNOFF WITHOUT ANY ROUTING
         FOUT(I,J) = NEIGHBOUR_INFLOW(I,J) + ROF(I,J)*
     /               FLND(I,J)/FWATER

        ENDIF                                ! This is IF LANDMASK = -1.0 or 0.0 loop

       ENDDO                                ! This is the DO I = 1,LONSL loop
      ENDDO                                 ! This is the DO J = 1,LAT loop

C     NOW WE UPDATE ALL PREVIOUS STORES AND FLOWS

      DO J = 1,LAT
       DO I = 1,LONSL
        IF(LANDMASK(I,J).LT.-0.5)THEN               !FOR LAND CELLS
         DIR = NINT(DIRECTION(I,J))

         IF(DIR.GE.1.AND.DIR.LE.8)THEN        ! Ordinary land grid cells
          PREVIOUS_GW_OUTFLOW(I,J) = GW_OUTFLOW(I,J)
          PREVIOUS_GW_STORE(I,J) = GW_STORE(I,J)
          PREVIOUS_SW_STORE(I,J) = SW_STORE(I,J)
          PREVIOUS_SW_OUTFLOW(I,J) = SW_OUTFLOW(I,J)
          PREVIOUS_SW_INFLOW(I,J) = SW_INFLOW(I,J)
          PREVIOUS_DEPTH(I,J) = DEPTH(I,J)
         ENDIF                                ! Ordinary land grid cells

         IF(DIR.EQ.0)THEN                     ! IF internally drained cells
          PREVIOUS_GW_OUTFLOW(I,J) = GW_OUTFLOW(I,J)
          PREVIOUS_GW_STORE(I,J) = GW_STORE(I,J)
          PREVIOUS_SW_OUTFLOW(I,J) = SW_OUTFLOW(I,J)
         ENDIF                                ! IF DIR is 0 ie internally drained cells

         IF(DIR.EQ.9)THEN                    ! Internally drained river mouth
          PREVIOUS_GW_OUTFLOW(I,J)       = GW_OUTFLOW(I,J)
          PREVIOUS_GW_STORE(I,J) = GW_STORE(I,J)
          PREVIOUS_SW_OUTFLOW(I,J) = SW_OUTFLOW(I,J)
         ENDIF                                ! IF DIR is 11 ie internally drained river mouths

        ENDIF                                 !IF LAND 
       ENDDO                                ! This is the DO I = 1,LONSL loop
      ENDDO                                 ! This is the DO J = 1,LAT loop

      RETURN
      END
