      SUBROUTINE GETLAKES(OCEAN_TARGET_I,OCEAN_TARGET_J,OCEAN_TARGET_N,
     1                  OT_LAKES_I,       OT_LAKES_J,       OT_LAKES_N,
     2            GLOCEAN_TARGET_I, GLOCEAN_TARGET_J, GLOCEAN_TARGET_N,
     3                 OT_GLAKES_I,      OT_GLAKES_J,      OT_GLAKES_N,
     4               LAND_TARGET_I,  LAND_TARGET_J,  LAND_TARGET_N,
     5                  LT_LAKES_I,     LT_LAKES_J,     LT_LAKES_N,
     6               GREAT_LAKES_I,  GREAT_LAKES_J,  GREAT_LAKES_N,
     7                   GL_LAND_I,      GL_LAND_J,      GL_LAND_N,
     8                    GL_DELAY)


C     *  GET THE KEY-DIRECTED TARGETTING INFORMATION FROM THE STANDARD INPUT
C     *  (STANDARD INPUT OPENED BY JCLPNT IN FLUXADJ MAIN ROUTINE).

C     * Nov 07/2015 - M.Lazare. Coupler code modified for use in AGCM:
C     *                         - no CPP diretives.
C     *
C     * Additional Great Lakes Modifications
C     *       Jul19/2000     Daniel Robitaille
C     *
C     * Original Code
C     *                      Scott Tinis
C     *
C     *
       
      IMPLICIT NONE

      INTEGER I,J,K
      INTEGER IPOINT

      CHARACTER*32 TOK(32)
      INTEGER  NTOK
      CHARACTER*256 RECORD

      INTEGER IOT                    ! ACCUMULATES NUMBER OF TARGETTED OCEAN POINTS
      INTEGER IGT                    ! ACCUMULATES NUMBER OF TARGETTED OCEAN POINTS (GREAT LAKES)
      INTEGER ILT                    ! ACCUMULATES NUMBER OF TARGETTED LAND POINTS
      INTEGER IGL                    ! ACCUMULATES NUMBER OF GREAT LAKES

      INTEGER OMAX,OLMAX,LMAX,LLMAX,GMAX,GLMAX
      PARAMETER    (OMAX=30,OLMAX=10,LMAX=10,LLMAX=10,GMAX=20,GLMAX=7)
      
      INTEGER OCEAN_TARGET_I (OMAX)
      INTEGER OCEAN_TARGET_J (OMAX)
      INTEGER OCEAN_TARGET_N
      INTEGER OT_LAKES_I     (OMAX,OLMAX)
      INTEGER OT_LAKES_J     (OMAX,OLMAX)
      INTEGER OT_LAKES_N     (OMAX)
      
      INTEGER GLOCEAN_TARGET_I (OMAX)
      INTEGER GLOCEAN_TARGET_J (OMAX)
      INTEGER GLOCEAN_TARGET_N
      INTEGER OT_GLAKES_I     (OMAX,OLMAX)
      INTEGER OT_GLAKES_J     (OMAX,OLMAX)
      INTEGER OT_GLAKES_N     (OMAX)
      
      INTEGER LAND_TARGET_I  (LMAX)
      INTEGER LAND_TARGET_J  (LMAX)
      INTEGER LAND_TARGET_N
      INTEGER LT_LAKES_I     (LMAX,LLMAX)
      INTEGER LT_LAKES_J     (LMAX,LLMAX)
      INTEGER LT_LAKES_N     (LMAX)
      
      INTEGER GREAT_LAKES_I  (GMAX)
      INTEGER GREAT_LAKES_J  (GMAX)
      INTEGER GREAT_LAKES_N
      INTEGER GL_LAND_I      (GMAX,GLMAX)
      INTEGER GL_LAND_J      (GMAX,GLMAX)
      INTEGER GL_LAND_N      (GMAX)
      
      REAL    GL_DELAY       (GMAX)

      INTEGER LULK,NEWUNIT
C---------------------------------------------
      LULK=NEWUNIT(80)
      OPEN(LULK,FILE='LAKES')

C     can we do this?

      IOT = 0
      IGT = 0
      ILT = 0 
      IGL = 0

      DO I = 1, 99999

         READ (LULK, '(A256)', END=999) RECORD

         TOK(1) = '  ' 

         CALL GETSTRTOK(RECORD,NTOK,TOK)

         IF (TOK(1) .EQ. 'LTO') THEN
            
            IOT = IOT + 1

            READ (TOK(2),*) OCEAN_TARGET_I(IOT)
            READ (TOK(3),*) OCEAN_TARGET_J(IOT)
            READ (TOK(4),*) OT_LAKES_N(IOT)

            DO J = 1, OT_LAKES_N(IOT)
               IPOINT = 4 + J*2 - 1
               READ (TOK(IPOINT),*) OT_LAKES_I(IOT,J)
               READ (TOK(IPOINT+1),*) OT_LAKES_J(IOT,J)
            END DO

         END IF

         IF (TOK(1) .EQ. 'GTO') THEN

            IGT = IGT + 1

            READ (TOK(2),*) GLOCEAN_TARGET_I(IGT)
            READ (TOK(3),*) GLOCEAN_TARGET_J(IGT)
            READ (TOK(4),*) OT_GLAKES_N(IGT)

            DO J = 1, OT_GLAKES_N(IGT)
               IPOINT = 4 + J*2 - 1
               READ (TOK(IPOINT),*) OT_GLAKES_I(IGT,J)
               READ (TOK(IPOINT+1),*) OT_GLAKES_J(IGT,J)
            END DO

         END IF

         IF (TOK(1) .EQ. 'LTL') THEN
            
            ILT = ILT + 1

            READ (TOK(2),*) LAND_TARGET_I(ILT)
            READ (TOK(3),*) LAND_TARGET_J(ILT)
            READ (TOK(4),*) LT_LAKES_N(ILT)

            DO J = 1, LT_LAKES_N(ILT)
               IPOINT = 4 + J*2 - 1
               READ (TOK(IPOINT),*) LT_LAKES_I(ILT,J)
               READ (TOK(IPOINT+1),*) LT_LAKES_J(ILT,J)
            END DO

         END IF

         IF (TOK(1) .EQ. 'GL') THEN

            IGL = IGL + 1

            READ (TOK(2),*) GREAT_LAKES_I(IGL)
            READ (TOK(3),*) GREAT_LAKES_J(IGL)
            READ (TOK(4),*) GL_DELAY(IGL)
            READ (TOK(5),*) GL_LAND_N(IGL)

            DO J = 1, GL_LAND_N(IGL)
               IPOINT = 5 + J*2 - 1
               READ (TOK(IPOINT),*)   GL_LAND_I(IGL,J)
               READ (TOK(IPOINT+1),*) GL_LAND_J(IGL,J)
            END DO

         END IF

      END DO

  999 CONTINUE

      OCEAN_TARGET_N = IOT
      GLOCEAN_TARGET_N = IGT
      LAND_TARGET_N  = ILT
      GREAT_LAKES_N  = IGL

C     *
C     * WRITE OCEAN CELL TARGETTING INFORMATION TO STANDARD OUTPUT
C     *
      
      WRITE(6,'(A)')
      WRITE(6,'(A)') ' OCEAN CELLS TARGETTED BY LAKES (P-E):'
      WRITE(6,'(A)')

      DO I = 1, OCEAN_TARGET_N
         WRITE(6,1000) OCEAN_TARGET_I(I),OCEAN_TARGET_J(I),
     1        (OT_LAKES_I(I,K),OT_LAKES_J(I,K),K=1,OT_LAKES_N(I))
      END DO


      WRITE(6,'(A)')
      WRITE(6,'(A)') ' OCEAN CELLS TARGETTED BY GrLAKES (P-E):'
      WRITE(6,'(A)')

      DO I = 1, GLOCEAN_TARGET_N
         WRITE(6,1000) GLOCEAN_TARGET_I(I),GLOCEAN_TARGET_J(I),
     1        (OT_GLAKES_I(I,K),OT_GLAKES_J(I,K),K=1,OT_GLAKES_N(I))
      END DO


C     *
C     * WRITE LAND CELL TARGETTING INFORMATION TO STANDARD OUTPUT
C     *
      
      WRITE(6,'(A)')
      WRITE(6,'(A)') ' LAND CELLS TARGETTED BY LAKES (SURFACE FLOW):'
      WRITE(6,'(A)')

      DO I = 1, LAND_TARGET_N
         WRITE(6,1000) LAND_TARGET_I(I),LAND_TARGET_J(I),
     1        (LT_LAKES_I(I,K),LT_LAKES_J(I,K),K=1,LT_LAKES_N(I))
      END DO

C     *
C     * WRITE GREAT LAKES ROUTING INFORMATION
C     *

      WRITE(6,'(A)')
      WRITE(6,'(A,A)') ' GREAT LAKES CELLS TARGETTED BY LAND ', 
     1               '(SURFACE FLOW):'
      WRITE(6,'(A)')

      DO I = 1, GREAT_LAKES_N
         WRITE(6,1001) GREAT_LAKES_I(I),GREAT_LAKES_J(I),GL_DELAY(I),
     /        (GL_LAND_I(I,K),GL_LAND_J(I,K),K=1,GL_LAND_N(I))
      END DO
      CLOSE(LULK)
C      
      RETURN
C---------------------------------------------------------------------
 1000 FORMAT(I8,',',I2,3X,'<-',3X,10(I2,',',I2,3X))
 1001 FORMAT(I8,',',I2,' (AT ',F5.1,' DAYS)',3X,'<-',3X,
     /       10(I2,',',I2,3X))
      END
