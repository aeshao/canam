      SUBROUTINE CTEMB(FIELDSM,WILTSM,
     1                 SAND,CLAY,
     2                 NL,NM,IL1,IL2,IM,IG)
C
C     * NOV 15/11 - M.LAZARE. ROUTINE BASED ON CLASSB, BUT FOR 
C     *                       CTEM, TO DEFINE INVARIANT FIELDS
C     *                       WILTSM AND FIELDSM WHICH CTEM NEEDS.
C
      IMPLICIT NONE
C
C     * SCALARS.
C
      REAL    PSISAT,GRKSAT,THPOR,B
      INTEGER NL,NM,IL1,IL2,IM,IG,I,J,M
C
C     * OUTPUT ARRAYS.
C
      REAL FIELDSM(NL,NM,IG), WILTSM(NL,NM,IG)
C
C     * INPUT ARRAYS.
C
      REAL SAND  (NL,NM,IG),  CLAY  (NL,NM,IG)
C
C     * INTERNAL ARRAYS.
C
      INTEGER ISAND(NL,NM,IG)
C---------------------------------------------------------------------
      DO 100 J=1,IG
      DO 100 M=1,IM
      DO 100 I=IL1,IL2
         ISAND  (I,M,J) = NINT(SAND(I,M,J))
         WILTSM (I,M,J) = 0.
         FIELDSM(I,M,J) = 0.
  100 CONTINUE
C
      DO 200 J=1,IG
      DO 200 M=1,IM
      DO 200 I=IL1,IL2
         IF(ISAND(I,M,J).NE.-3 .AND. ISAND(I,M,J).NE.-4) THEN
           PSISAT = (10.0**(-0.0131*SAND(I,M,J)+1.88))/100.0
           GRKSAT = (10.0**(0.0153*SAND(I,M,J)-0.884))*7.0556E-6   
           THPOR  = (-0.126*SAND(I,M,J)+48.9)/100.0
           B      = 0.159*CLAY(I,M,J)+2.91
C
           WILTSM (I,M,J) = (150./PSISAT)**(-1./B)
           WILTSM (I,M,J) = THPOR * WILTSM(I,M,J)
C
           FIELDSM(I,M,J) = (1.157E-09/GRKSAT)**(1./(2.*B+3.))
           FIELDSM(I,M,J) = THPOR *  FIELDSM(I,M,J)
         ENDIF
  200 CONTINUE
C
      RETURN
      END
