      subroutine timeck(year,day,sec,itest)

c     * August 19, 2002  - W.G. Lee
c
c     * timeck checks to see if the input year, day and
c     * seconds are integers. If not, it aborts.
c
c     * If itest = 1, then the subroutine aborts  
c     * if day is outside of:   1,2,3 ...   365
c     * or sec is outside of:   0,1,2 ... 86399
c
c     * If itest = 0, then the year/day/sec
c     * values will be adjusted so that day and 
c     * sec will fit the range:
c     *
c     * day is in range:  1,2,3 ...   365
c     * sec is in range:  0,1,2 ... 86399

c     -------------------------------------------------------------
      implicit none 

      integer  itest
      real     year,  day,  sec,  test

c     -------------------------------------------------------------

c     Test and abort if non-integers are found:

      test = sec - int(sec)
      if (test.ne.0)   call                        XIT('TIMECK',-1)
      test = day  - int(day)
      if (test.ne.0)   call                        XIT('TIMECK',-2)
      test = year - int(year)
      if (test.ne.0)   call                        XIT('TIMECK',-3)

c
      if (itest.eq.1) then

         if (sec.lt.0)      call                   XIT('TIMECK',-11)
         if (sec.gt.86399)  call                   XIT('TIMECK',-12)
         if (day.lt.1)      call                   XIT('TIMECK',-13)
         if (day.gt.365)    call                   XIT('TIMECK',-14)
 
      elseif (itest.eq.0) then

   10    if (sec.lt.0) then
            sec = sec + 86400
            day = day - 1
            goto 10
         endif

   20    if (sec.gt.86399) then
            sec = sec - 86400
            day = day + 1
            goto 20
         endif

   30    if (day.lt.1) then
            day  = day  + 365  
            year = year - 1
            goto 10
         endif

   40    if (day.gt.365) then
            day  = day  - 365
            year = year + 1
            goto 40
         endif

      else

         call                                      XIT('TIMECK',-22)
                                     
      endif

      return 
      end
