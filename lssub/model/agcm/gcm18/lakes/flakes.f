      SUBROUTINE FLAKES(LTICROW,LTSNROW,LDMXROW,LTAVROW,LTMXROW,
     1                  LTWBROW,LSHPROW,LZICROW,LZSNROW,
     2                  IKMOS,JKMOS,NMK,NL,ILG,
     3                  LTICGAT,LTSNGAT,LDMXGAT,LTAVGAT,LTMXGAT,
     4                  LTWBGAT,LSHPGAT,LZICGAT,LZSNGAT        )
C
C     * DEC 07/2016 - M.LAZARE. SCATTER OPERATION FOR FLAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK,NL,ILG,K
C
C     * LAKE TILE PARAMETERS.                                    
C
      REAL LTICROW(NL),  LTSNROW(NL),  LDMXROW(NL),  LTAVROW(NL),
     1     LTMXROW(NL),  LTWBROW(NL),  LSHPROW(NL),  LZICROW(NL),
     2     LZSNROW(NL)
C
      REAL LTICGAT(ILG), LTSNGAT(ILG), LDMXGAT(ILG), LTAVGAT(ILG),
     1     LTMXGAT(ILG), LTWBGAT(ILG), LSHPGAT(ILG), LZICGAT(ILG),
     2     LZSNGAT(ILG)
C
      INTEGER  IKMOS(ILG),   JKMOS(ILG) 
C----------------------------------------------------------------------
C     * SCATTER LAKE RELEVANT VARIABLES OUT OF
C     * WATER-TILE GAT ARRAYS                               
C                                                           
      DO K=1,NMK                                        
        LTICROW (IKMOS(K))=LTICGAT (K) 
        LTSNROW (IKMOS(K))=LTSNGAT (K) 
        LDMXROW (IKMOS(K))=LDMXGAT (K) 
        LTAVROW (IKMOS(K))=LTAVGAT (K) 
        LTMXROW (IKMOS(K))=LTMXGAT (K) 
        LTWBROW (IKMOS(K))=LTWBGAT (K) 
        LSHPROW (IKMOS(K))=LSHPGAT (K)
        LZICROW (IKMOS(K))=LZICGAT (K)
        LZSNROW (IKMOS(K))=LZSNGAT (K)     
      ENDDO
C
      RETURN
      END  
