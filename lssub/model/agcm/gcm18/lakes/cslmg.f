      SUBROUTINE CSLMG (HLAKGAT,LLAKGAT,BLAKGAT,NLAKGAT,
     +                  TLAKGAT,T0LAKGAT,HICELGAT,HDPTHGAT,
     +                  EXPWGAT,DTMPGAT,TKELGAT,DELUGAT,GREDGAT,RHOMGAT,
     +                  SNOGAT,RHONGAT,TNGAT,ANGAT,WSNOGAT,
     +                  FSVGAT,FSIGAT,FDLGAT,FSGGAT,FLGGAT,CSZGAT,
     +                  ULGAT,VLGAT,TAGAT,QAGAT,RHOAGAT,PADRGAT,PRESGAT,
     +                  ZRFMGAT,ZRFHGAT,ZDMGAT,ZDHGAT,RPCPGAT,
     +                  TRPCGAT,SPCPGAT,TSPCGAT,RHSIGAT,RADJGAT,
     +                  ASVDGAT,ASIDGAT,FSDBGAT,FSFBGAT,FSSBGAT,
     +                  REFGAT,BCSNGAT,DEPBGAT,
     +                  IKMOS,JKMOS,NLAKMAX,NMK,NL,NM,ILG,NBS,
     +                  HLAKROW,LLAKROW,BLAKROW,NLAKROW,
     +                  TLAKROW,T0LAKROW,HICELROW,HDPTHROW,
     +                  EXPWROW,DTMPROW,TKELROW,DELUROW,GREDROW,RHOMROW,
     +                  SNOROT,RHONROT,TNROT,ANROT,WSNOROT,
     +                  FSVROT,FSIROT,FDLROT,FSGROT,FLGROT,CSZROW,
     +                  ULROW,VLROW,TAROW,QAROW,RHOAROW,PADRROW,PRESROW,
     +                  ZRFMROW,ZRFHROW,ZDMROW,ZDHROW,RPCPROW,
     +                  TRPCROW,SPCPROW,TSPCROW,RHSIROW,RADJROW,
     +                  ASVDROT,ASIDROT,FSDBROT,FSFBROT,FSSBROT,
     +                  REFROT,BCSNROT,DEPBROW  )
C
C     * DEC 04/16 - M.LAZARE. - REMOVE TILE DIMENSION FOR SEVERAL LAKE FIELDS.
C     *                       - CHANGE "ROT" -> "ROW" FOR NON-TILED FIELDS.
C     *                       - RENAMED LAKEG-> CSLMG.
C     *                       - ADD "DEPB".
C     * APR 13/15 - D.VERSEGHY. LOCAL VERSION FOR OFFLINE TESTING.
C     * JAN 18/12 - M.MACKAY.  GATHER OPERATION FOR LAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK,NL,NM,ILG,K,L,NLAKMAX,NBS
C
      INTEGER  IKMOS (ILG),  JKMOS  (ILG)                           
C
C     * LAKE AND SNOW FIELDS.
C
      REAL HLAKROW (NL), LLAKROW (NL), BLAKROW (NL),      
     1     T0LAKROW(NL), HICELROW(NL), HDPTHROW(NL),
     2     EXPWROW (NL), DTMPROW (NL), TKELROW (NL), DELUROW(NL),
     3     GREDROW (NL), RHOMROW (NL), DEPBROW (NL)
      REAL NLAKROW(NL)
      REAL TLAKROW(NL,NLAKMAX)                               

      REAL SNOROT  (NL,NM), RHONROT(NL,NM), TNROT  (NL,NM),
     1     ANROT   (NL,NM), WSNOROT(NL,NM), FSVROT (NL,NM),
     2     FSIROT  (NL,NM), FDLROT (NL,NM), FSGROT (NL,NM), 
     3     FLGROT  (NL,NM), ASVDROT(NL,NM), ASIDROT(NL,NM),
     3     REFROT  (NL,NM), BCSNROT(NL,NM)
C
      REAL HLAKGAT (ILG),  LLAKGAT (ILG),  BLAKGAT (ILG),           
     1     T0LAKGAT(ILG),  HICELGAT(ILG),  HDPTHGAT(ILG),
     2     EXPWGAT (ILG),  DTMPGAT (ILG),  TKELGAT (ILG), DELUGAT(ILG),
     3     GREDGAT (ILG),  RHOMGAT (ILG),  DEPBGAT (ILG)
      INTEGER NLAKGAT(ILG)
      REAL TLAKGAT(ILG,NLAKMAX)                               

      REAL SNOGAT  (ILG),  RHONGAT (ILG),  TNGAT   (ILG),
     1     ANGAT   (ILG),  WSNOGAT (ILG),  FSVGAT  (ILG),
     2     FSIGAT  (ILG),  FDLGAT  (ILG),  FSGGAT  (ILG),
     5     FLGGAT  (ILG),  ASVDGAT (ILG),  ASIDGAT (ILG),
     6     REFGAT  (ILG),  BCSNGAT (ILG)
C
C     * ATMOSPHERIC AND GRID-CONSTANT INPUT VARIABLES.
C
      REAL  CSZROW ( NL), ULROW  ( NL), VLROW  ( NL), TAROW  ( NL), 
     1      QAROW  ( NL), PRESROW( NL), RHOAROW( NL), PADRROW( NL), 
     3      ZRFMROW( NL), ZRFHROW( NL), ZDMROW ( NL), ZDHROW ( NL),
     4      RPCPROW( NL), TRPCROW( NL), SPCPROW( NL), TSPCROW( NL),
     5      RHSIROW( NL), RADJROW( NL)
C
      REAL  FSDBROT(NL,NM,NBS), FSFBROT(NL,NM,NBS), FSSBROT(NL,NM,NBS)
C
      REAL  CSZGAT (ILG), ULGAT  (ILG), VLGAT  (ILG), TAGAT  (ILG),
     1      QAGAT  (ILG), PRESGAT(ILG), RHOAGAT(ILG), PADRGAT(ILG),
     2      ZRFMGAT(ILG), ZRFHGAT(ILG), ZDMGAT (ILG), ZDHGAT (ILG),
     3      RPCPGAT(ILG), TRPCGAT(ILG), SPCPGAT(ILG), TSPCGAT(ILG),
     4      RHSIGAT(ILG), RADJGAT(ILG)
C
      REAL  FSDBGAT(ILG,NBS), FSFBGAT(ILG,NBS), FSSBGAT(ILG,NBS)
C----------------------------------------------------------------------
      IF(NMK.GT.0) THEN
C
C       * GATHER LAKE RELEVANT VARIABLES INTO                    
C       * LAKE-TILE GAT ARRAYS                                     
C                                                                
        DO 100 K=1,NMK                                             
          SNOGAT (K)=SNOROT (IKMOS(K),JKMOS(K))
          RHONGAT(K)=RHONROT(IKMOS(K),JKMOS(K))
          TNGAT  (K)=TNROT  (IKMOS(K),JKMOS(K))
          ANGAT  (K)=ANROT  (IKMOS(K),JKMOS(K))
          ASVDGAT(K)=ASVDROT(IKMOS(K),JKMOS(K))
          ASIDGAT(K)=ASIDROT(IKMOS(K),JKMOS(K))
          REFGAT (K)=REFROT (IKMOS(K),JKMOS(K))
          BCSNGAT(K)=BCSNROT(IKMOS(K),JKMOS(K))
          WSNOGAT(K)=WSNOROT(IKMOS(K),JKMOS(K))
          FSVGAT (K)=FSVROT (IKMOS(K),JKMOS(K))
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))
          FDLGAT (K)=FDLROT (IKMOS(K),JKMOS(K))
          FSGGAT (K)=FSGROT (IKMOS(K),JKMOS(K))
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))
  100   CONTINUE
C
        DO 200 K=1,NMK                                             
          HLAKGAT (K)=HLAKROW (IKMOS(K))              
          LLAKGAT (K)=LLAKROW (IKMOS(K))              
          BLAKGAT (K)=BLAKROW (IKMOS(K))              
          NLAKGAT (K)=NINT(NLAKROW (IKMOS(K)))              
          T0LAKGAT(K)=T0LAKROW(IKMOS(K))
          HICELGAT(K)=HICELROW(IKMOS(K))
          HDPTHGAT(K)=HDPTHROW(IKMOS(K))
          EXPWGAT (K)=EXPWROW (IKMOS(K))
          DTMPGAT (K)=DTMPROW (IKMOS(K))
          TKELGAT (K)=TKELROW (IKMOS(K))
          DELUGAT (K)=DELUROW (IKMOS(K))
          GREDGAT (K)=GREDROW (IKMOS(K))
          RHOMGAT (K)=RHOMROW (IKMOS(K))
          DEPBGAT (K)=DEPBROW (IKMOS(K))
          DO L=1,NLAKGAT(K)                              
            TLAKGAT(K,L)=TLAKROW(IKMOS(K),L)        
          ENDDO
  200   CONTINUE                                                   

C       * ATMOSPHERIC FORCING VARIABLES NEEDED FOR LAKE TILES     
C
        DO 300 K=1,NMK                                           
          CSZGAT (K)=CSZROW (IKMOS(K))                    
          ULGAT  (K)=ULROW  (IKMOS(K))                    
          VLGAT  (K)=VLROW  (IKMOS(K))                    
          TAGAT  (K)=TAROW  (IKMOS(K))                
          QAGAT  (K)=QAROW  (IKMOS(K))                
          RHOAGAT(K)=RHOAROW(IKMOS(K))                
          PADRGAT(K)=PADRROW(IKMOS(K))                
          PRESGAT(K)=PRESROW(IKMOS(K))                
          ZRFMGAT(K)=ZRFMROW(IKMOS(K))
          ZRFHGAT(K)=ZRFHROW(IKMOS(K))
          ZDMGAT (K)=ZDMROW (IKMOS(K))
          ZDHGAT (K)=ZDHROW (IKMOS(K))
          RPCPGAT(K)=RPCPROW(IKMOS(K))
          TRPCGAT(K)=TRPCROW(IKMOS(K))
          SPCPGAT(K)=SPCPROW(IKMOS(K))
          TSPCGAT(K)=TSPCROW(IKMOS(K))
          RHSIGAT(K)=RHSIROW(IKMOS(K))
          RADJGAT(K)=RADJROW(IKMOS(K))
          DO L=1,NBS
            FSDBGAT(K,L)=FSDBROT(IKMOS(K),JKMOS(K),L)        
            FSFBGAT(K,L)=FSFBROT(IKMOS(K),JKMOS(K),L)        
            FSSBGAT(K,L)=FSSBROT(IKMOS(K),JKMOS(K),L)        
          ENDDO
  300   CONTINUE                                           
      ENDIF

      RETURN
      END
