      SUBROUTINE LAKEZ (ISTEP,  CTLSTP, CTSSTP, WTSSTP,
     +                  FSGL,   FLGL,   HFSL,   HEVL,   HMFL,   HTCL,
     +                  T0,     TLAK,   LKICEH, NLAK,
     +                  FSGS,   FLGS,   HFSS,   HEVS,   HMFN,   HTCS,
     +                  PCPN,   QFN,    ROFN,   SNO,    TSNOW,  WSNOW,
     +                  IL1,    IL2,    ILG,    N,      NLAKMAX)
C
C     * JAN 21/17 - D.VERSEGHY. FINALIZED FOR CSLM2. FOR NOW, COMMENT
C     *                         OUT LAKE ENERGY BALANCE CHECK; CLOSURE
C     *                         DIFFICULT DUE TO LARGE LAKE MASS.
C     * SEP 01/15 - D.VERSEGHY. ADD HTCL TO LAKE ENERGY BALANCE.
C     * AUG 31/15 - D.VERSEGHY. ADD SNOW ENERGY AND WATER BALANCE 
C     *                         CHECKS.
C     * JAN 14/13 - M.MACKAY    ENERGY BALANCE CHECK OVER LAKE TILES. 
C
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER ISTEP,IL1,IL2,ILG,N,I,J
C
C     * DIAGNOSTIC ARRAYS.
C
      REAL CTLSTP(ILG), CTSSTP(ILG), WTSSTP(ILG) 
C
      REAL Z,ZTOP,ZBOT,HCAP,HCAP0,QSUML,QSUMS,WSUMS
C
C     * LAKE INPUT ARRAYS.                                       
C                                                                
      INTEGER NLAKMAX                                            
      REAL,DIMENSION(ILG,NLAKMAX) :: TLAK                        
      INTEGER,DIMENSION(ILG) :: NLAK                             
      REAL,DIMENSION(ILG) :: FSGL,FLGL,HFSL,HEVL,HMFL,HTCL,
     1                       LKICEH,T0,
     2                       FSGS,FLGS,HFSS,HEVS,HMFN,HTCS,
     3                       PCPN,QFN,ROFN,SNO,TSNOW,WSNOW
C
C    * CLASS COMMON BLOCK PARAMETERS
C
      REAL DELT,TFREZ
      REAL HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,SPHW,SPHICE,
     1     SPHVEG,SPHAIR,RHOW,RHOICE,TCGLAC,CLHMLT,CLHVAP
C
      COMMON /CLASS1/ DELT,TFREZ
      COMMON /CLASS4/ HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     1                SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,
     2                TCGLAC,CLHMLT,CLHVAP
C
C    * LAKE TILE COMMON BLOCK PARAMETERS                        
C    *                                                          
      REAL TKECN,TKECF,TKECE,TKECS,HDPTHMIN,                    
     1     TKEMIN,DELMAX,DELMIN,EMSW,DELZLK,DELSKIN,DHMAX,TKECL,   
     2     DUMAX                                                   
C
      COMMON /LAKECON/ TKECN,TKECF,TKECE,TKECS,HDPTHMIN,        
     2             TKEMIN,DELMAX,DELMIN,EMSW,DELZLK,DELSKIN,DHMAX, 
     3             TKECL,DUMAX                                     
C
C----------------------------------------------------------------------
C
      IF(ISTEP.EQ.0) THEN
C
C     * SET BALANCE CHECK VARIABLE FOR START OF CURRENT TIME STEP.
C
      DO 100 I=IL1,IL2
          IF (LKICEH(I) .GE. DELSKIN) THEN 
              HCAP0=HCPICE
          ELSE IF (LKICEH(I) .LE. 0.0) THEN
              HCAP0=HCPW
          ELSE 
              HCAP0=(LKICEH(I)*HCPICE+(DELSKIN-LKICEH(I))*HCPW)/DELSKIN
          ENDIF
          IF (N .EQ. 1) THEN 
              CTLSTP(I)= -HCAP0*TLAK(I,1)*DELSKIN
          ELSE
              CTLSTP(I)= -HCAP0*T0(I)*DELSKIN
          ENDIF
C
          DO 50 J=1,NLAK(I)
              ZTOP=DELSKIN + DELZLK*(J -1)
              ZBOT=DELSKIN + DELZLK*J
              IF (LKICEH(I) .GE. ZBOT) THEN 
                  HCAP=HCPICE
              ELSE IF (LKICEH(I) .LE. ZTOP) THEN
                  HCAP=HCPW
              ELSE 
                  Z=LKICEH(I)-ZTOP
                  HCAP=(Z*HCPICE + (DELZLK-Z)*HCPW)/DELZLK
              ENDIF
              CTLSTP(I)=CTLSTP(I) - HCAP*TLAK(I,J)*DELZLK
50        CONTINUE
          CTSSTP(I)=-TSNOW(I)*(HCPICE*SNO(I)/RHOICE+
     1             HCPW*WSNOW(I)/RHOW)
          WTSSTP(I)=-SNO(I)-WSNOW(I)
100   CONTINUE  
C
      ENDIF
C
      IF(ISTEP.EQ.1) THEN
C
C     * CHECK ENERGY BALANCE OVER THE CURRENT TIME STEP.
C
      DO 300 I=IL1,IL2
          IF (LKICEH(I) .GE. DELSKIN) THEN 
              HCAP0=HCPICE
          ELSE IF (LKICEH(I) .LE. 0.0) THEN
              HCAP0=HCPW
          ELSE 
              HCAP0=(LKICEH(I)*HCPICE+(DELSKIN-LKICEH(I))*HCPW)/DELSKIN
          ENDIF
          CTLSTP(I)= CTLSTP(I) + HCAP0*T0(I)*DELSKIN
          DO 200 J=1,NLAK(I)
              ZTOP=DELSKIN + DELZLK*(J -1)
              ZBOT=DELSKIN + DELZLK*J
              IF (LKICEH(I) .GE. ZBOT) THEN 
                  HCAP=HCPICE
              ELSE IF (LKICEH(I) .LE. ZTOP) THEN
                  HCAP=HCPW
              ELSE 
                  Z=LKICEH(I)-ZTOP
                  HCAP=(Z*HCPICE + (DELZLK-Z)*HCPW)/DELZLK
              ENDIF
              CTLSTP(I)=CTLSTP(I) + HCAP*TLAK(I,J)*DELZLK
200       CONTINUE
          CTLSTP(I)=CTLSTP(I)/DELT
          CTSSTP(I)=CTSSTP(I)+TSNOW(I)*(HCPICE*SNO(I)/RHOICE+
     1             HCPW*WSNOW(I)/RHOW)
          CTSSTP(I)=CTSSTP(I)/DELT
          WTSSTP(I)=WTSSTP(I)+SNO(I)+WSNOW(I)
300   CONTINUE
C
      DO 400 I=IL1,IL2
          QSUML=FSGL(I)+FLGL(I)-HFSL(I)-HEVL(I)-HMFL(I)+HTCL(I)
          QSUMS=FSGS(I)+FLGS(I)-HFSS(I)-HEVS(I)-HMFN(I)+HTCS(I)
          WSUMS=(PCPN(I)-QFN(I)-ROFN(I))*DELT
                   
C          IF(ABS(CTLSTP(I)-QSUML).GE. 20.0) THEN
C              WRITE(6,6001) N,I,CTLSTP(I),QSUML
C6001          FORMAT(2X,'LAKE ENERGY BALANCE  ',2I8,2F20.8)
C              WRITE(6,6002) NLAK(I),FSGL(I),FLGL(I),HFSL(I),
C     1             HEVL(I),HMFL(I),HTCL(I),LKICEH(I),T0(I)
C6002          FORMAT(2X,I6,8F15.6)
CC              STOP
C          ENDIF
          IF(ABS(CTSSTP(I)-QSUMS).GT.5.0) THEN
              WRITE(6,6442) N,I,CTSSTP(I),QSUMS
6442          FORMAT(2X,'LAKE SNOW ENERGY BALANCE  ',2I8,2F20.8)
              WRITE(6,6450) FSGS(I),FLGS(I),HFSS(I),
     1            HEVS(I),HMFN(I),HTCS(I)
              WRITE(6,6450) SNO(I),WSNOW(I),TSNOW(I)-TFREZ
C              STOP
          ENDIF
          IF(ABS(WTSSTP(I)-WSUMS).GT.1.0E-2) THEN
              WRITE(6,6447) N,I,WTSSTP(I),WSUMS
6447          FORMAT(2X,'LAKE SNOW WATER BALANCE  ',2I8,2F20.8)
              WRITE(6,6450) PCPN(I)*DELT,QFN(I)*DELT,
     1            ROFN(I)*DELT
              WRITE(6,6450) SNO(I),WSNOW(I),TSNOW(I)-TFREZ
C              STOP
          ENDIF
400   CONTINUE
6450  FORMAT(2X,7F15.6)
C
      ENDIF
C
      RETURN
      END
