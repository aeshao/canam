      SUBROUTINE FLAKEG (SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     1                   REFGAT,  BCSNGAT, GCGAT,
     2                   FSVGAT,  FSIGAT,  FDLGAT,  FSGGAT,  FLGGAT,
     3                   HLAKGAT, LLAKGAT, BLAKGAT, 
     4                   RADJGAT, LTICGAT, LTSNGAT, LDMXGAT, LTAVGAT,
     5                   LTMXGAT, LTWBGAT, LSHPGAT, LZICGAT, LZSNGAT,
     6                   SNOWGAT, TAGAT,   ULGAT,   VLGAT,   PRESGAT, 
     7                   QAGAT,   THLGAT,
     8                   VMODGAT, GUSTGAT, RHOAGAT, GTAGAT,  CSZGAT,
     9                   ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT,
     A                   DEPBGAT, RPREGAT, SPCPGAT, RHSIGAT, PREGAT,
     B                   FSDBGAT, FSFBGAT, CSDBGAT, CSFBGAT, 
     C                   IKMOS,   JKMOS,
     D                   NMK,NL,NM,ILG,NBS,
     E                   SNOROT,  GTROT,   ANROT,   RHONROT, TNROT, 
     F                   REFROT,  BCSNROT, GCROT,
     G                   FSVROT,  FSIROT,  FDLROT,  FSGROT,  FLGROT,
     H                   HLAKROW, LLAKROW, BLAKROW, 
     I                   RADJROW, LTICROW, LTSNROW, LDMXROW, LTAVROW,
     J                   LTMXROW, LTWBROW, LSHPROW, LZICROW, LZSNROW,
     K                   SNOWROW, TAROW,   ULROW,   VLROW,   PRESROW, 
     L                   QAROW,   THLROW,
     M                   VMODL,   GUSTROL, RHOAROW, GTAROW,  CSZROW,
     N                   ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW,
     O                   DEPBROW, RPREROW, SPCPROW, RHSIROW, PREROW,
     P                   FSDBROT, FSFBROT, CSDBROT, CSFBROT         )
C
C     * Dec 10, 2016 - M.Lazare. New version called by sfcproc2
C     *                          in new model version gcm19. This
C     *                          is modelled on "oceang", with:
C     *                          - NBS and tiled radiation removed.
C     *                          - WRKA,WRKB,CLDT,CSZ removed.
C     *                          - FSV,FSI,FDL,HLAK,LLAK,BLAK added.
C     *                          - RADJ,LTIC,LTSN,LDMX,LTAV,
C     *                            LTMX,LTWB,LSHP,LZIC,LZSN added.
C     *                          - RHOA,RPRE added.
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NMK,NL,NM,ILG,NBS,K,IB
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM) ::
     1      SNOROT,  GTROT,   ANROT,   RHONROT, TNROT,   
     2      REFROT,  BCSNROT, GCROT,   FSVROT,  FSIROT,
     3      FDLROT,  FSGROT,  FLGROT

      REAL, DIMENSION(ILG)   ::
     1      SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     2      REFGAT,  BCSNGAT, GCGAT,   FSVGAT,  FSIGAT,
     3      FDLGAT,  FSGGAT,  FLGGAT

      REAL, DIMENSION(NL) ::
     1      HLAKROW, LLAKROW, BLAKROW,
     2      RADJROW, LTICROW, LTSNROW, LDMXROW, LTAVROW,
     3      LTMXROW, LTWBROW, LSHPROW, LZICROW, LZSNROW, 
     4      SNOWROW, TAROW,   ULROW,   VLROW,   PRESROW,
     5      QAROW,   THLROW,  VMODL,   GUSTROL, 
     6      RHOAROW, GTAROW,  CSZROW,  
     7      ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW,
     8      DEPBROW, RPREROW, SPCPROW, RHSIROW, PREROW

      REAL, DIMENSION(ILG) :: 
     1      HLAKGAT, LLAKGAT, BLAKGAT,
     2      RADJGAT, LTICGAT, LTSNGAT, LDMXGAT, LTAVGAT,
     3      LTMXGAT, LTWBGAT, LSHPGAT, LZICGAT, LZSNGAT,
     4      SNOWGAT, TAGAT,   ULGAT,   VLGAT,   PRESGAT,
     5      QAGAT,   THLGAT,  VMODGAT, GUSTGAT, 
     6      RHOAGAT, GTAGAT,  CSZGAT,
     7      ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT,
     8      DEPBGAT, RPREGAT, SPCPGAT, RHSIGAT, PREGAT

      REAL, DIMENSION(NL,NM,NBS) :: FSDBROT, FSFBROT, CSDBROT, CSFBROT

      REAL, DIMENSION(ILG,NBS)   :: FSDBGAT, FSFBGAT, CSDBGAT, CSFBGAT
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IKMOS (ILG),  JKMOS  (ILG)
C--------------------------------------------------------------------
      DO 100 K=1,NMK
          SNOGAT (K)=SNOROT (IKMOS(K),JKMOS(K))  
          GTGAT  (K)=GTROT  (IKMOS(K),JKMOS(K))  
          ANGAT  (K)=ANROT  (IKMOS(K),JKMOS(K))  
          RHONGAT(K)=RHONROT(IKMOS(K),JKMOS(K))  
          TNGAT  (K)=TNROT  (IKMOS(K),JKMOS(K))  
          REFGAT (K)=REFROT (IKMOS(K),JKMOS(K))  
          BCSNGAT(K)=BCSNROT(IKMOS(K),JKMOS(K))  
          GCGAT  (K)=GCROT  (IKMOS(K),JKMOS(K))
          FSVGAT (K)=FSVROT (IKMOS(K),JKMOS(K))  
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))  
          FDLGAT (K)=FDLROT (IKMOS(K),JKMOS(K))  
          FSGGAT (K)=FSGROT (IKMOS(K),JKMOS(K))  
          FLGGAT (K)=FLGROT (IKMOS(K),JKMOS(K))  
  100 CONTINUE
C
      DO 200 K=1,NMK
          HLAKGAT(K)=HLAKROW(IKMOS(K))
          LLAKGAT(K)=LLAKROW(IKMOS(K))
          BLAKGAT(K)=BLAKROW(IKMOS(K))
          RADJGAT(K)=RADJROW(IKMOS(K))
          LTICGAT(K)=LTICROW(IKMOS(K))
          LTSNGAT(K)=LTSNROW(IKMOS(K))
          LDMXGAT(K)=LDMXROW(IKMOS(K))
          LTAVGAT(K)=LTAVROW(IKMOS(K))
          LTMXGAT(K)=LTMXROW(IKMOS(K))
          LTWBGAT(K)=LTWBROW(IKMOS(K))
          LSHPGAT(K)=LSHPROW(IKMOS(K))
          LZICGAT(K)=LZICROW(IKMOS(K))
          LZSNGAT(K)=LZSNROW(IKMOS(K))
          SNOWGAT(K)=SNOWROW(IKMOS(K))
          TAGAT  (K)=TAROW  (IKMOS(K))
          ULGAT  (K)=ULROW  (IKMOS(K))
          VLGAT  (K)=VLROW  (IKMOS(K))
          PRESGAT(K)=PRESROW(IKMOS(K))
          QAGAT  (K)=QAROW  (IKMOS(K))
          THLGAT (K)=THLROW (IKMOS(K))
          VMODGAT(K)=VMODL  (IKMOS(K))    
          GUSTGAT(K)=GUSTROL(IKMOS(K))
          RHOAGAT(K)=RHOAROW(IKMOS(K))
          GTAGAT (K)=GTAROW (IKMOS(K))
          CSZGAT (K)=CSZROW (IKMOS(K))
          ZRFMGAT(K)=ZRFMROW(IKMOS(K))
          ZRFHGAT(K)=ZRFHROW(IKMOS(K))
          ZDMGAT (K)=ZDMROW (IKMOS(K))
          ZDHGAT (K)=ZDHROW (IKMOS(K))
          DEPBGAT(K)=DEPBROW(IKMOS(K))
          RPREGAT(K)=RPREROW(IKMOS(K))
          SPCPGAT(K)=SPCPROW(IKMOS(K))
          RHSIGAT(K)=RHSIROW(IKMOS(K))
          PREGAT (K)=PREROW (IKMOS(K))
  200 CONTINUE
C
      DO 300 IB=1,NBS
      DO 300 K=1,NMK
          FSDBGAT (K,IB) = FSDBROT (IKMOS(K),JKMOS(K),IB)
          FSFBGAT (K,IB) = FSFBROT (IKMOS(K),JKMOS(K),IB)
          CSDBGAT (K,IB) = CSDBROT (IKMOS(K),JKMOS(K),IB)
          CSFBGAT (K,IB) = CSFBROT (IKMOS(K),JKMOS(K),IB)
  300 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
