      SUBROUTINE CSLMS (TLAKROW,T0LAKROW,HICELROW,HDPTHROW,EXPWROW,
     1                  DTMPROW,TKELROW,DELUROW,GREDROW,RHOMROW,
     2                  LUINROW,LUIMROW,WSNOLROT,
     3                  IKMOS,JKMOS,NLAKMAX,NMK,NL,NM,ILG,
     4                  TLAKGAT,T0LAKGAT,HICELGAT,HDPTHGAT,EXPWGAT,
     5                  DTMPGAT,TKELGAT,DELUGAT,GREDGAT,RHOMGAT,
     6                  LUINGAT,LUIMGAT,WSNOLGAT,NLAKGAT)
C
C     * DEC 01/16 - M.LAZARE. - REMOVE {SNOL,RHOSL,TSNOL,ALBSL} ALREADY DONE
C     *                         IN "OCEANS" ROUTINE.
C     *                       - REMOVE {HLAK,BLAK,LLAK,NLAK} WHICH ARE INVARIANT
C     *                         AND THUS DON'T NEED TO BE SCATTERED.
C     *                       - REMOVE TILE DIMENSION FOR ALL FIELDS
C     *                         EXCEPT WSNO.
C     *                       - {IKMOS,JKMOS}->{IKMOS,JKMOS} AND NMW->NMK.
C     *                       - "ROT"->"ROW" FOR NAMES EXCEPT WSNO.
C     *                       - RENAMED LAKEG-> CSLMG.
C     *                       - ADD "LUIN" AND "LUIM", IE LAKE ICE
C     *                         FRACTION AND MASS..
C     * JAN 19/12 - M.MACKAY. SCATTER OPERATION FOR LAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK,NL,NM,ILG,K,L,NLAKMAX         
C
C     * LAKE TILE PARAMETERS                                    
C
      REAL T0LAKROW(NL),HICELROW(NL),HDPTHROW(NL),
     1     EXPWROW(NL),DTMPROW(NL),TKELROW(NL),DELUROW(NL),
     2     GREDROW(NL),RHOMROW(NL),LUINROW(NL),LUIMROW(NL)
      REAL WSNOLROT(NL,NM)
      REAL TLAKROW(NL,NLAKMAX)                             
C
      REAL T0LAKGAT(ILG),HICELGAT(ILG),HDPTHGAT(ILG),
     2     EXPWGAT(ILG),DTMPGAT(ILG),TKELGAT(ILG),DELUGAT(ILG),
     3     GREDGAT(ILG),RHOMGAT(ILG),LUINGAT(ILG),LUIMGAT(ILG)
      REAL WSNOLGAT(ILG)
      REAL TLAKGAT(ILG,NLAKMAX)                         
      INTEGER NLAKGAT(ILG)      
C
      INTEGER  IKMOS(ILG),   JKMOS(ILG) 
C----------------------------------------------------------------------
C     * SCATTER LAKE RELEVANT VARIABLES OUT OF
C     * WATER-TILE GAT ARRAYS                               
C                                                           
      DO 200 K=1,NMK                                        
        T0LAKROW(IKMOS(K))=T0LAKGAT(K) 
        HICELROW(IKMOS(K))=HICELGAT(K) 
        HDPTHROW(IKMOS(K))=HDPTHGAT(K) 
        EXPWROW (IKMOS(K))=EXPWGAT (K) 
        DTMPROW (IKMOS(K))=DTMPGAT (K) 
        TKELROW (IKMOS(K))=TKELGAT (K) 
        DELUROW (IKMOS(K))=DELUGAT (K) 
        GREDROW (IKMOS(K))=GREDGAT (K) 
        RHOMROW (IKMOS(K))=RHOMGAT (K) 
        LUINROW (IKMOS(K))=LUINGAT (K)
        LUIMROW (IKMOS(K))=LUIMGAT (K)
        DO L=1,NLAKGAT(K)                           
          TLAKROW(IKMOS(K),L)=TLAKGAT(K,L)     
        ENDDO
  200 CONTINUE                         
C
      DO 300 K=1,NMK
        WSNOLROT(IKMOS(K),JKMOS(K))=WSNOLGAT(K)
  300 CONTINUE                     
C
      RETURN
      END  
