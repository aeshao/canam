      SUBROUTINE ROOT_LIMIT (DIST, Q, R, A, B, ALPHA, BETA,
     +                       A_PROB, C_PROB, VALUE)
      PARAMETER (XACC = 0.00001, JMAX = 10000, EPS = 1.0E-5)
C     PARAMETER (XACC = 0.005, JMAX = 1000, EPS = 1.0E-5)
      IF (DIST .EQ. 1.0) THEN
         X2   = (B - A) / (B - A)
         FMID = BETAI(Q,R,X2) - A_PROB
         X1   = (A+EPS - A) / (B - A)
         FF   = BETAI(Q,R,X1) - A_PROB
      ELSE IF (DIST .EQ. 2.0) THEN
         X2   = B * BETA
         FMID = GAMMP(ALPHA,X2) - A_PROB
         X1   = (EPS) * BETA
         FF   = GAMMP(ALPHA,X1) - A_PROB
      END IF
c      IF (FF * FMID .GE. 0.0) PAUSE
c      IF (FF .LT. 0.0)THEN
         RTBIS = X1
         DX    = X2 - X1
c      ELSE
c         RTBIS = X2
c         DX    = X1 - X2
c      ENDIF
      DO 11 J=1,JMAX
         DX   = DX * .50
         XMID = RTBIS + DX
         X    = XMID
         IF (DIST .EQ. 1.0) THEN
            FMID = BETAI(Q,R,X) - C_PROB
         ELSE IF (DIST .EQ. 2.0) THEN
            FMID = GAMMP(ALPHA,X) - C_PROB
         END IF
         IF (FMID .LT. 0.0) RTBIS = XMID
         IF (ABS(DX) .LT. XACC .OR. FMID .EQ. 0.0) go to 15
11    CONTINUE
      WRITE(*,*) 'too many bisections'
15    CONTINUE
      IF (DIST .EQ. 1.0) THEN
         VALUE = RTBIS * (B - A) + A
      ELSE IF (DIST .EQ. 2.0) THEN
         VALUE = RTBIS / BETA
      END IF
      RETURN
      END
