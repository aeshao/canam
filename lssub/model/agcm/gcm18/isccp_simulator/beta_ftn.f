      FUNCTION BETA_FTN(z,w)
      REAL beta_ftn,w,z
CU    USES gammln
      REAL gammln
      beta_ftn=exp(gammln(z)+gammln(w)-gammln(z+w))
      return
      END
