       SUBROUTINE XCW2_DATA(IDIST)

C      * DEC 12/2007 - JASON COLE.   NEW VERSION FOR GCM15G:
C      *                             OPTION "IDIST" PASSED IN TO
C      *                             PERMIT USE OF GAMMA OR BETA
C      *                             DISTRIBUTION FOR HORIZONTAL
C      *                             DISTRIBUTION OF CLOUD CONDENSATE.
C      * JAN 09/2007 - JASON COLE.   PREVIOUS VERSION XCW_DATA FOR GCM15F.
C 	
C      * SUBROUTINE TO GENERATE XCW DATA NEEDED BY STOCHASTIC CLOUD GENERATOR.

	IMPLICIT NONE

	INTEGER, PARAMETER ::
     1  N1 = 1000,
     2  N2 = 140

        REAL ::
     1 XCW(N1,N2)

        COMMON /XCWDATA/ XCW
	
      INTEGER, INTENT(IN) :: IDIST ! Distribution to use for horizontal distribution
                                   ! of cloud condensate
C Local variables
      INTEGER I,J
      REAL AVG, STD, A, B, BG, C, D, R, Q, BETA, ALPHA, PROB_MIN,
     1     PROB, DIST, A_BETA, A_GAMMA

C Loop over standard deviations
      DO 10 J=1,N2
c mean and std dev
        AVG = 1.0
        STD = 0.025*(J+3)
c
c upper and lower limits for beta dist (A and B).
c upper limit for gamma dist (BG).
c BG superficially optimized by P. Raisanen (July 2002?)
C 
        A  = 0.0
        B  = (5. + 5.*STD**2)*AVG
        BG = (5. + 5.*STD**2)*AVG
c
c using mean and std dev, determine parameters of 
c beta dist and gamma dist
c
        C = (AVG - A) / (B - AVG)
        D = ((B - A) / STD)**2
        R = C * (D - 2.0 - C) / (C * (C**2 + 3.0 * C + 3.0) + 1.0)
        Q = C * R
        BETA  = AVG / STD**2
        ALPHA = AVG * BETA
        PROB_MIN = 0.0
c - PROB = cumulative frequency
c - A_BETA and A_GAMMA = returned value given PROB
c
       IF (IDIST .EQ. 2) THEN ! Gamma distribution
          DIST = 2.0 
          DO I=1,N1
           PROB = REAL(I-1.)/REAL(N1-1.)
           CALL ROOT_LIMIT (DIST, Q, R, A, BG, ALPHA, BETA,
     1                      PROB_MIN, PROB, A_GAMMA) 
           XCW(I,J) = A_GAMMA
          END DO ! I
       ELSE IF (IDIST .EQ. 1) THEN ! Beta distribution
          DIST = 1.0
          DO I=1,N1
           PROB = REAL(I-1.)/REAL(N1-1.)
           CALL ROOT_LIMIT (DIST, Q, R, A, BG, ALPHA, BETA,
     1                      PROB_MIN, PROB, A_BETA)
           XCW(I,J) = A_BETA
           END DO ! I
       ELSE
           WRITE(*,*) "Inproper choice for IDIST in XCW_DATA!"
           WRITE(*,*) "Valid values for IDIST is 1 or 2."
           WRITE(*,*) "Current value to IDIST is: ",IDIST
           WRITE(*,*) "Stopping in XCW_DATA."
           STOP
       END IF
 10   CONTINUE

      RETURN
      END
